# Crypto

This article offers a high level explanation how crypto is used inside the Schluss app.

We use commonly used and proven crypto standards to provide a secure environment. These are:
- OpenPGP for assymetric crypto operations
- AES256 for symmetric cryptography
- PBKDF2 for key derivation 
- Keychain (IOS) or KeyStore (Android) to provide secure key storage   

## Users' key
The users' PGP 4096 bit key is generated when the user creates it's vault when first time starting the app. The key is stored in the Secure keystore and password protected by a user created pincode. At login only the right pincode unlocks the key which makes it possible to decrypt the users' stored data.

### Vault creation:
1. user generates 5 digit pincode: `pin`
2. random salt is generated: `pinSalt`
3. pincode is hashed: `pinHash = pkbdf2(pin, pinSalt)`
4. random 256 bit cipher is generated: `rndKey`
5. random 4096 PGP key pair is generated, rndKey is used as passphrase: `userKeyEnc = OpenPGP.generate(passphrase: rndKey)`
6. an on-device database is created and encrypted with rndKey
7. rndKey is symmetrically encrypted with pinHash: `rndKeyEnc = aes256_encrypt(rndKey, pinHash)`
8. following is stored inside Secure Storage: pinSalt, userKeyEnc, rndKeyEnc

### Vault unlocking:
1. user enters 5 digit pincode: pin
2. generate hash from pincode, use pinSalt as salt from Secure Storage: `pinHash =  pkbdf2(pin, pinSalt)`
3. get rndKeyEnc from Secure Storage and try decrypting it with pinHash: `rndKey =  aes256_decrypt(rndKeyEnc, pinHash)`  

Now we have rndKey which unlocks the on-device database and the users' private key.

To decrypt data with the users private key:
- get userKeyEnc from Secure Storage and you're able to decypt/encrypt attributes: `decrypted = OpenPGP_decrypt('to be decrypted', userKeyEnc, rndKey)`

### Changing the pincode
1. Follow steps 1 to 3 from Vault unlocking to get rndKey.
2. Follow steps 1, 2, 3, 7 from Vault creation to encrypt the rndKey with a the new pinSalt.
3. Overwrite in Secure Storage: pinSalt, rndKeyEnc 

## Persistent data storage

The package [Sembast](https://pub.dev/packages/sembast) offers us an encrypted NoSQL data storage solution we use in our app to store all persistent data. These are application settings, attributes, data contracts, data requests.

