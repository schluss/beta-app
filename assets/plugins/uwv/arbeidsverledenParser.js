(function () {
    let r = {};

    // EmploymentHistory
    let EmploymentHistory = getAttribute('#ctl00_DVBContentPlaceHolder_ctl00_arbeidsverleden > div > table:nth-child(2) > thead > tr > th:nth-child(1)');
    r.EmploymentHistory = EmploymentHistory.replace('jaar', '').trim();

    // YearlyEmploymentHistory[]
    var items = document.querySelectorAll('#ctl00_DVBContentPlaceHolder_ctl00_arbeidsverledenTable tr');

    var historyItems = [];

    items.forEach((row) => {

        let item = {};

        // Year
        item.Year = row.querySelector('td:nth-child(1)').innerText;

        // IncludedSvYear
        item.IncludedSvYear = row.querySelector('td:nth-child(2)').innerText == 'Ja' ? true : false

        // EmploymentDetails[]
        let checkLink = row.querySelector('td:nth-child(3) a');
        if (checkLink && checkLink.href != '') {

            // Get and parse the subpages
            let htmlString = httpGet(checkLink.href);
            let dom = new DOMParser().parseFromString(htmlString, "text/html");

            // find the 3 possible types:		
            let test1 = getAttribute('#main > div.main_body > div > div.twosections-primary > section > div > span > p', null, dom);
            let test2 = getAttribute('#main > div.main_body > div > div.twosections-primary > section > div > table > tbody > tr > td', null, dom);

            // 1: Let op! .. geen
            if (test1 != null && test1.includes('Let op!')) {
                // nothing...
            }
            // 2: 123 uren voor companyname
            else if (test2 != null && test2.includes('uren')) {

                item.EmploymentDetails = [];
                let EmploymentDetailsItem = {};

                let uurPos = test2.indexOf('uren');
                let voorPos = test2.indexOf('voor');

                EmploymentDetailsItem.SVDays = 0;
                EmploymentDetailsItem.Hours = test2.substring(0, uurPos).trim();
                EmploymentDetailsItem.Employer = test2.substring(voorPos + 4).trim();

                item.EmploymentDetails.push(EmploymentDetailsItem);
            }
            // 3: sv-Dagen
            else if (test2 != null && test2.includes('sv-dagen')) {

                item.EmploymentDetails = [];
                let EmploymentDetailsItem = {};

                let svPos = test2.indexOf('sv-dagen');
                let voorPos = test2.indexOf('voor');

                EmploymentDetailsItem.SVDays = test2.substring(0, svPos).trim();
                EmploymentDetailsItem.Hours = 0;
                EmploymentDetailsItem.Employer = test2.substring(voorPos + 4).trim();

                item.EmploymentDetails.push(EmploymentDetailsItem);
            }
        }

        historyItems.push(item);
    });

    r.YearlyEmploymentHistory = JSON.stringify(historyItems); // value as a json encoded string!

    return r;
})();

function formatDate(dateStr) {
    if (!dateStr || dateStr.trim() == '' || !dateStr.includes('-'))
        return '';

    let dobArr = dateStr.trim().split('-');
    let dt = new Date(dobArr[2], (dobArr[1] - 1), dobArr[0]);
    const offset = dt.getTimezoneOffset();
    dt = new Date(dt.getTime() - (offset * 60 * 1000));
    return dt.toISOString().split('T')[0];
}


function getAttribute(selector, attribute, dom) {
    if (attribute == null) attribute = 'innerText';
    if (dom == null) dom = document;
    let el = dom.querySelector(selector);
    if (el == null) return '';

    return dom.querySelector(selector)[attribute].trim();
}

function httpGet(url) {
    var xmlHttp = new XMLHttpRequest();
    xmlHttp.open("GET", url, false); // false for synchronous request
    xmlHttp.send(null);
    return xmlHttp.response;
}
