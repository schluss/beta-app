(function () {

    let r = {};

    // Personal details:

    // Consumenten nummer
    r.ConsumerNumber = getAttribute('#profile_consumernumber');

    // Voorletters
    r.Initials = getAttribute('#profile_initials');

    // Voornamen voluit
    r.FirstNames = getAttribute('#profile_firstname');

    // Roepnaam
    r.Nickname = getAttribute('#profile_nickname');

    // Achternaam (bij geboorte)
    r.SurnameAtBirth = getAttribute('#profile_lastname');

    // Geslacht
    r.Gender = getAttribute('#profile_gender');

    // Geboortedatum - formatted as ISO8601 date (YYYY-MM-DD)
    r.DateOfBirth = formatDate(getAttribute('#profile_birthdate'));

    // Telefoonnummer
    r.PhoneNumber = getAttribute('#profile_phonenumber');

    // E-mail
    r.Email = getAttribute('#profile_emailaddress');

    // Address:
    let address = getAttribute('#profile_currentaddress').split('\n');

    // Huisnummer (with street + housenumber i make the assumption format is like Streetname 1A, when it is like Streetname 1 a, it will go wrong)
    let housenumber = address[0].split(' ');
    r.HouseNumber = housenumber[housenumber.length - 1].trim();

    // Straatnaam
    r.Street = address[0].replace(r.HouseNumber, '').trim();

    // Woonplaats
    let city = address[1].split(' ');
    r.City = city[city.length - 1].trim();

    // Postcode
    r.PostalCode = address[1].replace(r.City, '').trim();

    // Land
    r.Country = address[2];

    console.log(r);

    return r;
})();


function formatDate(dateStr) {
    if (!dateStr || dateStr.trim() == '' || !dateStr.includes('-'))
        return '';

    let dobArr = dateStr.trim().split('-');
    let dt = new Date(dobArr[2], (dobArr[1] - 1), dobArr[0]);
    const offset = dt.getTimezoneOffset();
    dt = new Date(dt.getTime() - (offset * 60 * 1000));
    return dt.toISOString().split('T')[0];
}

function getAttribute(selector, attribute, dom) {
    if (attribute == null) attribute = 'innerText';
    if (dom == null) dom = document;
    let el = dom.querySelector(selector);
    if (el == null) return '';

    return dom.querySelector(selector)[attribute].trim();
}
