// Developed only for the MijnOverheid Test environment, to provide an Akte van Overlijden

function runParser() {

    let r = {};

    // Surname
    r.GeslachtsnaamOverledene = getAttribute('#nationaliteit-003 > div:nth-child(2) > table > tbody > tr:nth-child(1) > td:nth-child(2)');
    // FirstNames
    r.VoornamenOverledene = getAttribute('#nationaliteit-003 > div:nth-child(2) > table > tbody > tr:nth-child(2) > td:nth-child(2)');
    // PlaceOfBirth
    r.GeboorteplaatsOverledene = getAttribute('#nationaliteit-003 > div:nth-child(2) > table > tbody > tr:nth-child(3) > td:nth-child(2)');
    // DateOfBirth
    r.GeboortedatumOverledene = getAttribute('#nationaliteit-003 > div:nth-child(2) > table > tbody > tr:nth-child(4) > td:nth-child(2)');
    // Place
    r.WoonplaatsOverledene = getAttribute('#nationaliteit-003 > div:nth-child(2) > table > tbody > tr:nth-child(5) > td:nth-child(2)');
    // DateOfDeath
    r.DatumVanOverlijden = getAttribute('#nationaliteit-003 > div:nth-child(2) > table > tbody > tr:nth-child(6) > td:nth-child(2)');
    // TimeOfDeath
    r.TijdstipVanOverlijden = getAttribute('#nationaliteit-003 > div:nth-child(2) > table > tbody > tr:nth-child(7) > td:nth-child(2)');
    // DateOfInspection
    r.DatumVanSchouwing = getAttribute('#nationaliteit-003 > div:nth-child(2) > table > tbody > tr:nth-child(8) > td:nth-child(2)');
    // PlaceOfDeath 
    r.PlaatsVanOverlijden = getAttribute('#nationaliteit-003 > div:nth-child(2) > table > tbody > tr:nth-child(9) > td:nth-child(2)');
    // MarriedTo
    r.GehuwdMet = getAttribute('#nationaliteit-003 > div:nth-child(2) > table > tbody > tr:nth-child(10) > td:nth-child(2)');
    // RegisteredPartnerOf
    r.GeregistreerdePartnerVan = getAttribute('#nationaliteit-003 > div:nth-child(2) > table > tbody > tr:nth-child(11) > td:nth-child(2)');
    // Municipality 
    r.UitgevendeGemeente = getAttribute('#nationaliteit-003 > div:nth-child(2) > table > tbody > tr:nth-child(12) > td:nth-child(2)');
    // DocumentNumber
    r.Aktenummer = getAttribute('#nationaliteit-003 > div:nth-child(2) > table > tbody > tr:nth-child(13) > td:nth-child(2)');

    let result = {};
    result.AkteVanOverlijden = JSON.stringify(r);

    return result;
}

// Run this function, the return value is catched in the plugin
runParser();

function getAttribute(selector, attribute, dom) {
    if (attribute == null) attribute = 'innerText';
    if (dom == null) dom = document;
    let el = dom.querySelector(selector);
    if (el == null) return '';

    return dom.querySelector(selector)[attribute].trim();
}
