# Build Android APK with Tests
build-android:
	flutter test test/startup_tests
	flutter build apk

# RUN App with Tests
run:
	flutter test test/startup_tests
	flutter run
