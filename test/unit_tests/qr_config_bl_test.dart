import 'package:flutter_test/flutter_test.dart';
import 'package:schluss_beta_app/business/impl/qr_config_bl_impl.dart';
import 'package:schluss_beta_app/singleton_injector.dart';
import 'package:schluss_beta_app/view_model/data_request_config_view_model.dart';
import 'package:schluss_beta_app/view_model/request_view_model.dart';

void main() {
  setupSingletonInjector();
  final QRConfigurationBLImpl configBL = singletonInjector<QRConfigurationBLImpl>();

  group('QR Request Business Logic', () {
    test('Retrieve Configuration Data from configuration link ', () async {
      String configUrl = 'android-app://org.schluss.app/https/app.schluss.app/connect/6tutmZI2gCGiTOs6E8E2vZcnQM2pQ3TT/https%3A%2F%2Fservices.schluss.app%2Fvolksbank%2Fconfig.json/hypotheekaanvraag';
      String scope = 'hypotheekaanvraag';
      String settingsUrl = 'https://services.schluss.app/volksbank/config.json';

      //processConfigData(String configUrl, String scope, String settingsUrl);

      //DataRequestConfigViewModel appConfig = await _configBL.processConfigData(testLink);
      DataRequestConfigViewModel appConfig = await configBL.processConfigData(
        configUrl,
        scope,
        settingsUrl,
      );
      expect(appConfig.name, 'de Volksbank');
      expect(appConfig.dataRequester.appName, 'volksbank.nl');
      expect(appConfig.scope, 'hypotheekaanvraag');
    });

    test('Retrieve Configuration Data from request', () async {
      RequestViewModel model = RequestViewModel.fromJson({
        'id': 1,
        'name': 'de Volksbank',
        'token': '1225487836',
        'scope': 'hypotheekaanvraag',
        'settings_url': 'https://services.schluss.app/volksbank/config.json',
        'status': 'P',
        'label': 'Hypotheekaanvraag',
        'request_date': 12457852782,
      });

      DataRequestConfigViewModel appConfig = await configBL.retrieveConfigData(
        model,
      );
      expect(appConfig.name, 'de Volksbank');
      expect(appConfig.dataRequester.appName, 'volksbank.nl');
      expect(appConfig.scope, 'hypotheekaanvraag');
    });
  });
}
