import 'package:flutter_test/flutter_test.dart';
import 'package:schluss_beta_app/util/vc_util.dart';

void main() {
  group('VC Test', () {
    test('Pass VC data - String', () async {
      var decryptedData = '''
        {
        "@context": "https://www.w3.org/2018/credentials/v1",
        "id": "http://schluss.app/credentials/firstnames",
        "type": ["VerifiableCredential", "FirstNamesCredential"],
        "credentialSubject": {"id": "did:iota:5yno1ph5uBU2cXSCDzn6VvXrUuTCzQaVN5Nj1F4tA3kL", "FirstNames": "Jan"},
        "issuer": "did:iota:Bo4mkWXD436DGcuEBdKGCKR2yyNXsL3XMGUVKGeZsixz",
        "issuanceDate": "2022-05-05T04:51:34Z",
        "proof": {"type": "JcsEd25519Signature2020", "verificationMethod": "#key", "signatureValue": "66GpEUn99owPYqdyFTPqY3H1qF4NDubV29vssjUE5Zxkhkq3GFGBNnByAaRpKxUzhu4MLtiv2JeyvuUauXFna6Tr"}
      }''';

      bool status = VCUtil.validateVC(decryptedData);
      expect(status, true);
    });

    test('Pass VC data - Map', () async {
      var decryptedData = {
        '@context': 'https://www.w3.org/2018/credentials/v1',
        'id': 'http://schluss.app/credentials/firstnames',
        'type': ['VerifiableCredential', 'FirstNamesCredential'],
        'credentialSubject': {'id': 'did:iota:5yno1ph5uBU2cXSCDzn6VvXrUuTCzQaVN5Nj1F4tA3kL', 'FirstNames': 'Jan'},
        'issuer': 'did:iota:Bo4mkWXD436DGcuEBdKGCKR2yyNXsL3XMGUVKGeZsixz',
        'issuanceDate': '2022-05-05T04:51:34Z',
        'proof': {'type': 'JcsEd25519Signature2020', 'verificationMethod': '#key', 'signatureValue': '66GpEUn99owPYqdyFTPqY3H1qF4NDubV29vssjUE5Zxkhkq3GFGBNnByAaRpKxUzhu4MLtiv2JeyvuUauXFna6Tr'}
      };

      bool status = VCUtil.validateVC(decryptedData);
      expect(status, true);
    });

    test('Pass not VC data - String', () async {
      var decryptedData = 'Test Data ';

      bool status = VCUtil.validateVC(decryptedData);
      expect(status, true);
    });

    test('Pass not VC data - List', () async {
      var decryptedData = ['credentialSubject', 'issuer', 'issuanceDate', 'proof'];

      bool status = VCUtil.validateVC(decryptedData);
      expect(status, true);
    });
  });
}
