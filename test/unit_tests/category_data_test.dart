import 'package:flutter/material.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:schluss_beta_app/translations/category_data.dart';

void main() {
  group('Category Data Test', () {
    test('Retrieve by key ', () async {
      CategoryData categoryData = CategoryData();
      CategoryDTO categoryDataTo = categoryData.getCategory('identity');

      expect(categoryDataTo.translateKey, 'identityCategory');
      expect(categoryDataTo.iconPath, 'assets/images/media_icon.png');
      expect(categoryDataTo.color, const Color(0xffcd472a));
    });

    test('Retrieve directly ', () async {
      CategoryData categoryData = CategoryData();
      CategoryDTO categoryDataTo = categoryData.identity;

      expect(categoryDataTo.translateKey, 'identityCategory');
      expect(categoryDataTo.iconPath, 'assets/images/media_icon.png');
      expect(categoryDataTo.color, const Color(0xffcd472a));
    });

    test('Retrieve all ', () async {
      CategoryData categoryData = CategoryData();
      List<CategoryDTO> categoryDataTo = categoryData.getAll();

      expect(categoryDataTo.first.translateKey, 'identityCategory');
      expect(categoryDataTo.first.iconPath, 'assets/images/media_icon.png');
      expect(categoryDataTo.first.color, const Color(0xffcd472a));
      expect(categoryDataTo.length, 8);
    });
  });
}
