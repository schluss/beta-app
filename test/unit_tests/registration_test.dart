/* import 'package:flutter/widgets.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:openpgp/key_pair.dart';
import 'package:schluss_beta_app/data/interface/common_dao.dart';
import 'package:schluss_beta_app/data/models/app_data_model.dart';
import 'package:schluss_beta_app/data/models/settings_data_model.dart';
import 'package:schluss_beta_app/database_config.dart';
import 'package:schluss_beta_app/singleton_injector.dart';
import 'package:schluss_beta_app/ui/registration/registration_create_pin_code.dart';
import 'package:schluss_beta_app/util/crypto_util.dart';
import 'package:schluss_beta_app/view_model/app_session_viewmodel.dart';

void main() {
  TestWidgetsFlutterBinding.ensureInitialized();

  setupSingletonInjector();
  final CommonDAO _commonDAOImpl = singletonInjector<CommonDAO>();

  group('Registration Process.', () {
    group('PIN validation.', () {
      testWidgets('', (WidgetTester tester) async {
        await tester.pumpWidget(PinCreationPage());
        // R&D.
      });
    });

    group('Vault creation.', () {
      String randomKey;
      bool dbStatus;
      String publicKey;
      String privateKey;

      test('Generating keys.', () async {
        WidgetsFlutterBinding.ensureInitialized();

        /// Assigns a PIN here.
        String password = '12346';

        /// Creates Hash value from Password.
        String hashValue = await CryptoUtil.createHashValue(password);
        print(hashValue);
        expect(hashValue.isNotEmpty, true);

        /// Generates Random Key to secure the Database.
        randomKey = await CryptoUtil.generateRandomKey();
        print(randomKey);
        expect(randomKey.isNotEmpty, true);

        /// Stores Random Key.
        await CryptoUtil.storeRandomKey(randomKey, hashValue);

        /// Generates RSA Key Pair protected with Random Key.
        KeyPair keyPair = await CryptoUtil.createKeyPair(randomKey);
        publicKey = keyPair.publicKey;
        print(publicKey);
        expect(publicKey.isNotEmpty, true);
        privateKey = keyPair.privateKey;
        print(privateKey);
        expect(privateKey.isNotEmpty, true);
      });

      test('Initializing DB.', () async {
        WidgetsFlutterBinding.ensureInitialized();

        dbStatus = await DatabaseConfig.initiateDatabase(randomKey) != null ? true : false;
        print(dbStatus);
        expect(dbStatus, true);
      });

      test('Storing App Data.', () async {
        WidgetsFlutterBinding.ensureInitialized();

        if (dbStatus) {
          /// Stores App Data.
          AppDataModel appDataModel = AppDataModel(
            publicKey: publicKey,
            timestamp: DateTime.now().millisecondsSinceEpoch,
          );
          print(appDataModel.publicKey);
          print(appDataModel.timestamp);

          Map<String, dynamic> dataMap = await _commonDAOImpl.store(
            AppDataModel.getStoreName(),
            appDataModel.toJson(),
          );
          print(dataMap.entries);
          print(dataMap.keys);
          print(dataMap.values);
          expect(dataMap.isNotEmpty, true);

          /// Sets Private key value for AppSessionViewModel.
          AppSessionViewModel sessionModel = AppSessionViewModel.fromJson(dataMap);
          sessionModel.setPrivateKey(privateKey);
          print(sessionModel.toString());
          bool isSessionSet = sessionModel != null ? true : false;
          expect(isSessionSet, true);
        }
      });

      test('Storing default settings.', () async {
        WidgetsFlutterBinding.ensureInitialized();

        if (dbStatus) {
          SettingsDataModel settingsModel = SettingsDataModel.fromJson(
            {
              'testMode': true,
              'tourDataRequestOpen': true,
            },
          );
          print(settingsModel.toString());
          expect(settingsModel.testMode, true);
          expect(settingsModel.tourDataRequestOpen, true);

          Map<String, dynamic> model = await _commonDAOImpl.store(
            SettingsDataModel.getStoreName(),
            settingsModel.toJson(),
          );
          print(model.keys);
          print(model.values);
          expect(model.isNotEmpty, true);
        }
      });
    });
  });
}
 */
