/*
 * @author Manuja Jayawardana
 * @email mjayawardana@yukon.lk
 * @create date 2020-07-27
 */
import 'package:flutter_test/flutter_test.dart';
import 'package:logger/logger.dart';
import 'package:schluss_beta_app/singleton_injector.dart';
import 'package:schluss_beta_app/util/crypto_util.dart';
import 'package:session_next/session_next.dart';

void main() {
  TestWidgetsFlutterBinding.ensureInitialized();

  String hash = '1jIKntmudQa2XAvnq9Sb/X0mmcDfAqH6x+NSmRspR+4=';
  var privateKey = '''
-----BEGIN PGP PRIVATE KEY BLOCK-----
Version: fast-openpgp

xcaGBF8ekEABEADI+C+PcWU+sSa8JQfN4IwutBZ/+VnXFL5HYPw9uVi7qQEpP43z
OQl0NJeFpO/Jl/twXK1ujwH1n7yyZ1fT2MiXtPxTl6OdnAHswL/YrTmv8+lt+PWB
7VZWst+ZWKGi9g/GHrvdBZwgMMnG2LbEGu1Ul9TZ+IpscwRgMp4Jg+//hiiMkV30
GLwcvFN0FY9AGzcp7m8w3tw5f/gZTAyNNsL2rCtXWJbDnMBEfGImTE5hXXByE+Ed
TcylxZkOox+AWx+avSnoymxg+IFKZ2OF8aXo9HOgB1Zf6eXytgBKVLkAqvCEoeVC
CHD0QQowAFaDLB/sqV6DQ+YHGVVJ52rBfuOLEmpoU6OveiggNpA0bl17DgnVdTFl
RxZpe04r/6AJEKFjGodbyF5oDsMS1BOt5y/3yiqzvLH7gdClEHXZwnO7vVI7Rk+O
iwISJ99SdGCuLfr8yP5Zsy9qxbxXBOwgz2aVM2zZd5nhT/aH9ACGxfaTzloLj2CY
FQKu4D3fm+inbfZXgE8UVemV1XJAwLfmgTS+SaiLAqMxXpG+uBizNzrIZutjjxNr
x40M3QqThTB3yL3TDZEwkUhu/YswAtdqkhiuTo9x55DaTmqNUICarmvBcf3SsJql
2gxVfbgxdK0rRicrgP2zk6rImCFhW4fp7DF5P17nfc1nQwonJzv8dXVVeQARAQAB
/gcDCABBiwnn5VzTYLLtGqfD2rzGhLxIdE1F2jBAjwknKsuFo1umdJxna7J9DKpV
3tAPLhHCLl73NhoWrWIrF2R2DNnj3SChxAV7O8q1e1z59Q4wkGdF/FiWtT4f37wi
Tb3AG5M8X5XW2JP1yn3a8JOL7dbnZUgC3k61pMYUwiV1Be2GdMf3mdQr6Om+Ypi8
dSf2ALl307dTwzZ9miQvSte5ZDga728wbNwd+YdHk2/vhRbx+a3bunzGhkCn2FUE
C+UCgn9WQys6C77FjeoVGQ/i1gzPH+aqZo/F0HQMOyZLreCFkFnbyGJBrbf49MEH
qdlCay4cV24ekEwB1juElWgySv88r8O6aPFioJAFSmvwUHraIgPCP03KCTnUNn00
09/7Qqj4LKjAX7RZAOuI5KiOBsYCUVl3wrMz4pxH3zeTjbL0lI5lLhxi/wp8yTSR
BCAQsRLAkFhugzH+6xV6NXELzDCJP7jet+bkS8Dhml6/6TLtJ54OrunIhZQ1kTXP
ZR4GQLjQ+6qP8HafQWwap6hVE0uW9SUqp41uK/+fVudqgpHx30ZskSMs5z7CS+9j
cNmuN9TpCCGzKmOj0RoehT2C4XPQ7Xs0P7kHdZHj1zQm4byikErRoA0YEkz4PN7g
fLKTRdl7/3JRtW55tyK5EeeCWj8qU77bRHhoN37SLMKG8Dh5uCo/rRwU5Ju9RyCG
9xFnOjEL9JapK1A1JwcNOFGU6vRL1QzznnBQ1ooqsbsQDbKgpptxepkJNj7qAA5S
E4YOzrl62Jm8i2BMIgDNqawTVJSwBpgdg7ICz/jCanBqymkv5LyV4Bnbecex3u3+
07nWs2vXuuU/8d+pld2+Q40daRiQkwbQhknFvYfNvU7qK64BXN/0+37UKKLWUDXO
HI3n11A8reCYnLJQ48r9718UPYLn6+vHeVi/qoYTpTG5prV0u8R9G3C0TAhzO9th
gOGwR/B3YfWlu0EnGrADkGGA1Msc/2aFq7bXMsWYBIEaQ6ipGEqUkXWQOnYxj/cG
aAmr8hdQYnibJ2ljW8es3jPtrjfpyKA0IvvZjn2GxcKnd7z3qWpae1cw2eWAIZhM
VcBw53jDFKrUkO+EhRlQ8L+A1cdYcyglaBlj8chsjssbUSPjboE/jWTLVT2JSmTY
gEKTA+V1r8ChZkaH9OvFQOhAPohzYtb+nCh/zwgz3DsYBGnz3JldlqrNLSmo6Iki
4BiOb50SdO+Tg8/XlwgZAwwOEdbl5nLS6KEEvsTM/PfEUSgaSBV6yEe91gAcgQoG
9mB92kYCZG+lw07MJztH/J7tmCVgA0gNHNMU5im9BPzZ15SE6uGdDDPNTI8h0OdQ
JYWDYMmk4EzmtjE6H3Uut6FJzjtzcQTGnjjY1lb1bDRP3QrnUTO29/nuHYlUGQPm
mdvvkc10haIElNRRNlzYnm+Q0VhdSm1pLBpsvtsS+KNAwf6lpI0oWIj5KRVPxaRL
tEmiKo68l6Xr5+WLUtW72imwV2FDA4W0J9PraYMfPOiNRs9fjG837lK6XQjlEIXe
zxnP6Gw9hGFBOtdKL/L4kPG7BJkDeM0XKbOtnGxDebU4ehp2Ad/xBK8SWaYIBCiF
C5xM54x2KDlxikBcYq+Wyn9q8kWhTuMAcQNI8j7Vl3tlgs5Cln3uZ2TH0XFhGxt7
X0VvuVJlUpOU/jJsvpJVlSzWNtmrCunjeV1eL/+933Do/EzfGJQhEBWBzYTMlU3R
Ig84DL+G1XbgKtMBfK6Nt01XYIpRKYSOtOVeJcmO/Dd1WbOPdr85++zNFHRlc3Qg
PHRlc3RAdGVzdC5jb20+wsFoBBMBCgAcBQJfHpBACRASqv//Nd9OegIbAwIZAQIL
CQIVCgAAcP0QAKtVAAeYiNh0FuGtL5fE32GlVqVFTHK5FdoFccfX8MarkU6S3HVW
VuFZ66RgDU3zIZXiWTo6DyeorRAwSuiKMLWj1yR3X3BAnFdF3Qhmsuqd13OPc87w
ehceLIElPxWVM3ExBtevRMynLOkSl9wDkOqaHgpZrBBMUlYscNAh5cfvs20LH+3N
2kcRYeFsBAOhARqXB9PGYcUE4iM/TKPtwl4q1N6dSiqdfWbPIgfynDrKsbc3zmLy
UnRqo/x77MgKUCfv2NOxvH721SFFjCqbW8o8HtXd/8aAqrzwpHweq35SWBDbr2hn
o6L+zkzc8UQ28BYPS2nGarSe6KfvH2Eg9KOjWMq+/r9WeeBwLRtpRgiOCHA+NS+b
YsBYzQtgDAGXrCD2rolup0E6CcgTihvTwGYjmjt+KngQSyZlEsL7O+HQ68WBTtQC
qRfhxzWvox964iYisGXhAQoeYtjXaDU9bRIeS+rC1zeu4pSeqOE+RtDu2cukMOd/
aB98Kz3xqqnQszL6Vvwlf83siJ5jfza72fHLYZWkK6Sydjabo178p+8IeK1vMB9R
z1gq1Fq2x/uOZlPgsC+6pS6dRGa33GspD2ZuOnSK6JKRQ/oXD8khaMUfZMcz/KGf
TXqgriuN7JWthDAAtH0UYyipT993UQBJlvtJBQvYvzO2QDjHrMbRUhFDx8ZYBF8e
kEABEAC3pmLay5vFZlwytflXOet+J/LyB7W5lTOIVKG0QrDIHNeQcUqQiokdZ6D7
EXHdFJy69brgRBTQdcs/9LE19JRKLK9Q6C7EdWBcfoo8P07vf1F18TQ+UZjndA08
bVU3uHfU8dgsX3yRIiiHd/UUH7uvomtH2OdHJyCNoSKYjSZotiYB48sMCxutJoVW
FlQDgXjdm51N4RFLTKg756H6wfDy3nhAvxi+QBm8tQ/84q/IM2AAbT6R3UgTaTMl
i9nP9TV7Itto1Un0OqcoBKRzkNH5m7nsRAPXQnIPAbMdiQD3UQg4GzpVi8QDaOcE
ompuv64NErsKbfFSEOL4yVCUAh9mDPW7XW23IOykP/C0JwDMV1PK6mV+O7GfZysP
9BRkiYsmhSeb6IKv788Ogbn8KDDddV9Y1eHQiiL/4V2WkyCu6nOis/X9OH/dIjuS
cseG7OUGmEyjVHDmLyA8wihfl9vpUqZNOL1qUj/pKtfvB7pmw1xzB3xNjmAceQRf
Sj1IYiNiZA8qWENIWz0G1dKagCA2gCrGJWVSC5eAMAv9xQk5YmdaGSpctjAUgatS
Um8U6oI6SuMGtHArRhRYMv8VKDKZsHiaGOuw4JWnm8xpht/hpPHLMcmdqhli66v7
0EhDnrLhVOh9pBsXvIfgKbysie1IBMmx2OLAUoOWrSD1wrtmyQARAQABABAAnFNL
jocGn6+UB66TAkbRSF/lE1SwwbpZVVEFHJYwOQckD1YAbK0gLiGKDG07CT+Ecji9
zCznstKyIky9k/oPSFxlF2+sb5qiXdlw/fu9y2wLAWRCXlHC4DIPGkLO6VFi36bF
pm/ZbwkuirH3shuY/dY5j7z4kb6inSxucICmEzgJfk5TBntmIavKkA8NkLZjFfBi
pGpS+E/WXLe9T+BKo9Hvdt2zyC50Oge+BjtH+hiRPYvAWRWNRsijuvAihjxqO9jd
/4yfGP/cTdo1yhGaSujeCFmVE2UCBfgiaZ+3bM5TOr8ilrMUNPrUu0MtafxLweER
ybzwUDUDn1Zt+q8b3aZ/FBmVQOYx4oBqq09YG0DBGtdZXja8+jNhxmw1ArK/F0ty
r0UpeS3Mk51y0s2buoO1KB+B2KLCfSrwZF19yCW5PTxwRg+2W6OLkTERAcP4Bbk5
qHF/tHfQ3DSfSLFWNG2E7E/8KMO9Tp3V3ThSUpJxSUbeFDt/qXpI2usTN3FzHJys
DwXVloBuY5mRhcn1rFLaz8+rP8A8cDFX+rgDAJr84Gu4gNJsEdGb4L6fGIoEYLxx
vxjyGCuy7RK6ZOekEGrd5cpqD/VOPtvrOfLG6lfEiOXjeCzv23KFy3G0KNIS8wQ/
tjPrZ7zyvrxNsKZ88e0nNwFyZ2KpT63iyj7bWwEIAMtRBm13IihqtMlGOE01azN1
TgGzyV893faARO5qrN1IFwQzNaYM02vW6/xEtYjC6FHMaO4CpVAPFeapnVsRTaGC
bCv8dkv/Ad8Uok82gwRzWjDnm3MLfoiAgfUg3ki0GC+34H/L80jYbMT6cT0FsFH4
3F1Hv804O6A8DkbfgOf5jVYOdD4RlZxwDST0Gj0FnYtUdtNsyjxUCczvbabemtMY
kDIKRORrb+Q0THpG07ibJRhbGfNh0JOTdIHenRUDY80x6cF6kdh0KJl1YuLTVNTm
jdyYkGZweLd0ssuzmScifHDIlr5Qb20FpTiYQ0yBoRZ5s/JOP6CUmhSPTyMYGJEI
AOc8yEgJn7h6jZtotPJUaZIYKfTtaTKi+x8q0SAifFa0zW3pG3goNJJoQ6mnYt7W
X9NoAOjO2+iN45W9FyO69DLFa7NL+rG+i1m7w5PBrpg62ucZZaCw5xawhnPNx3Gw
HpTCTqINIgywimXvuRuhO6XcIfwx11hYI/vNzvsF4To33XG+1WPvnZcQughwls41
AqDvvSQfSEqh5GdspjfkvFxO8oo7NhoyPqiw4P4gh9whdczt1rJGLbLm+y4IHyZa
SEH7U2nhijueO4z0LiSCMAejyGwgJVFaZ7OEb1/wjYvbhQbXZOvlkZf1Aslc9gwC
Jl2zI+dKHJ0wL2ZFkLL6RrkIAJq11RaQxhPHrhqs1gDMAm6+mQ9uNomg5oZeFs4B
svOh/UcLARsFnUbjHHQ0i+2s+Ucjo4zLgN7S9zuGXoBp2piFXsMO0A6F7WYUaMpX
E9K/qj8kIyeyTg96Iju9dQ99irTgzBCuJ4C5D+DYQKAwU63lwEMyjqVI48CfMBAE
0oFK+e8GWvh2zsTdSucQfVvUqUNl9Vsgdls80Dr5lVV3Ew8VFlDtcXkif/eO10NN
M5yDqGuzViGLtGPf03dk1xzqjHEbyNfjhU7utnennckJ94HKCP83EsHPVxIOgiu7
JjGDLk9He0S37PufFXfVhoZEIhf7C80LTmGMsPyHfmsQSEN2s8LBXwQYAQoAEwUC
Xx6QQAkQEqr//zXfTnoCGwwAALFwEACZMnxbppz2ZLXKUL2ZZqa7PcYm+mDmqBvu
0laephpRdoqioeaXWszF51im8pEion5ML19DGguSzR9YztOS/OWKeRG27s2Lv06m
hSIUgjiAea1chlxq73XXCGb39c5E/iPjacaTAEY5LRlJTDLqmBM2zY86pQ+ZML1D
cYf9pnfZehKj2gYeTbo3560itoggX4tHU/iUcJu7kc5wqVwITmxQeW9xX08RQ4Mw
7Yzx424GjEsymmxQFVwaBKVzbZwwGpaNiuEeVqAgVQABSV6xiC66HngnVZYRzgzt
+BgSzOoBoZYN6tBFucOBYtK4fxcCmDftH6KdHzlJPmKeC+msLkFxOF4aVUlYc6Nw
+o5Tq1F2aHGor+u5qAxdaopRfniYxDwZsUUlJsSMurvcJa1SM9jBq66fvwqkzPy/
sjndztm6bIwRyfH+tSkSqppNcIJvI5qfuNV0M3RrVesruE7WeMYryXLUNQ2etZM9
TYRoPp1MmF0jRryd5R5V1QsDKI19Y7FUtYYHTYsv9Xvov47JePvlj9BI0f93eVUA
SC8V41+ZozJ0Mhlsh2YKt45Jlrn6r7Jsl3iYLz5VmrzsE+HJotSXVrUiavXdpvqO
hkMzdP6w35B1iYNfBHr1flqoEOnkjQDbbPvDzPS2R4ytJVNzF6fdTNoitK9hy8m4
addl6kgABQ==
=Hk0S
-----END PGP PRIVATE KEY BLOCK-----''';

  String publicKey = '''
-----BEGIN PGP PUBLIC KEY BLOCK-----
Version: fast-openpgp

xsFNBF8ekEABEADI+C+PcWU+sSa8JQfN4IwutBZ/+VnXFL5HYPw9uVi7qQEpP43z
OQl0NJeFpO/Jl/twXK1ujwH1n7yyZ1fT2MiXtPxTl6OdnAHswL/YrTmv8+lt+PWB
7VZWst+ZWKGi9g/GHrvdBZwgMMnG2LbEGu1Ul9TZ+IpscwRgMp4Jg+//hiiMkV30
GLwcvFN0FY9AGzcp7m8w3tw5f/gZTAyNNsL2rCtXWJbDnMBEfGImTE5hXXByE+Ed
TcylxZkOox+AWx+avSnoymxg+IFKZ2OF8aXo9HOgB1Zf6eXytgBKVLkAqvCEoeVC
CHD0QQowAFaDLB/sqV6DQ+YHGVVJ52rBfuOLEmpoU6OveiggNpA0bl17DgnVdTFl
RxZpe04r/6AJEKFjGodbyF5oDsMS1BOt5y/3yiqzvLH7gdClEHXZwnO7vVI7Rk+O
iwISJ99SdGCuLfr8yP5Zsy9qxbxXBOwgz2aVM2zZd5nhT/aH9ACGxfaTzloLj2CY
FQKu4D3fm+inbfZXgE8UVemV1XJAwLfmgTS+SaiLAqMxXpG+uBizNzrIZutjjxNr
x40M3QqThTB3yL3TDZEwkUhu/YswAtdqkhiuTo9x55DaTmqNUICarmvBcf3SsJql
2gxVfbgxdK0rRicrgP2zk6rImCFhW4fp7DF5P17nfc1nQwonJzv8dXVVeQARAQAB
zRR0ZXN0IDx0ZXN0QHRlc3QuY29tPsLBaAQTAQoAHAUCXx6QQAkQEqr//zXfTnoC
GwMCGQECCwkCFQoAAHD9EACrVQAHmIjYdBbhrS+XxN9hpValRUxyuRXaBXHH1/DG
q5FOktx1VlbhWeukYA1N8yGV4lk6Og8nqK0QMEroijC1o9ckd19wQJxXRd0IZrLq
nddzj3PO8HoXHiyBJT8VlTNxMQbXr0TMpyzpEpfcA5Dqmh4KWawQTFJWLHDQIeXH
77NtCx/tzdpHEWHhbAQDoQEalwfTxmHFBOIjP0yj7cJeKtTenUoqnX1mzyIH8pw6
yrG3N85i8lJ0aqP8e+zIClAn79jTsbx+9tUhRYwqm1vKPB7V3f/GgKq88KR8Hqt+
UlgQ269oZ6Oi/s5M3PFENvAWD0tpxmq0nuin7x9hIPSjo1jKvv6/VnngcC0baUYI
jghwPjUvm2LAWM0LYAwBl6wg9q6JbqdBOgnIE4ob08BmI5o7fip4EEsmZRLC+zvh
0OvFgU7UAqkX4cc1r6MfeuImIrBl4QEKHmLY12g1PW0SHkvqwtc3ruKUnqjhPkbQ
7tnLpDDnf2gffCs98aqp0LMy+lb8JX/N7IieY382u9nxy2GVpCuksnY2m6Ne/Kfv
CHitbzAfUc9YKtRatsf7jmZT4LAvuqUunURmt9xrKQ9mbjp0iuiSkUP6Fw/JIWjF
H2THM/yhn016oK4rjeyVrYQwALR9FGMoqU/fd1EASZb7SQUL2L8ztkA4x6zG0VIR
Q87BTQRfHpBAARAAt6Zi2subxWZcMrX5Vznrfify8ge1uZUziFShtEKwyBzXkHFK
kIqJHWeg+xFx3RScuvW64EQU0HXLP/SxNfSUSiyvUOguxHVgXH6KPD9O739RdfE0
PlGY53QNPG1VN7h31PHYLF98kSIoh3f1FB+7r6JrR9jnRycgjaEimI0maLYmAePL
DAsbrSaFVhZUA4F43ZudTeERS0yoO+eh+sHw8t54QL8YvkAZvLUP/OKvyDNgAG0+
kd1IE2kzJYvZz/U1eyLbaNVJ9DqnKASkc5DR+Zu57EQD10JyDwGzHYkA91EIOBs6
VYvEA2jnBKJqbr+uDRK7Cm3xUhDi+MlQlAIfZgz1u11ttyDspD/wtCcAzFdTyupl
fjuxn2crD/QUZImLJoUnm+iCr+/PDoG5/Cgw3XVfWNXh0Ioi/+FdlpMgrupzorP1
/Th/3SI7knLHhuzlBphMo1Rw5i8gPMIoX5fb6VKmTTi9alI/6SrX7we6ZsNccwd8
TY5gHHkEX0o9SGIjYmQPKlhDSFs9BtXSmoAgNoAqxiVlUguXgDAL/cUJOWJnWhkq
XLYwFIGrUlJvFOqCOkrjBrRwK0YUWDL/FSgymbB4mhjrsOCVp5vMaYbf4aTxyzHJ
naoZYuur+9BIQ56y4VTofaQbF7yH4Cm8rIntSATJsdjiwFKDlq0g9cK7ZskAEQEA
AcLBXwQYAQoAEwUCXx6QQAkQEqr//zXfTnoCGwwAALFwEACZMnxbppz2ZLXKUL2Z
Zqa7PcYm+mDmqBvu0laephpRdoqioeaXWszF51im8pEion5ML19DGguSzR9YztOS
/OWKeRG27s2Lv06mhSIUgjiAea1chlxq73XXCGb39c5E/iPjacaTAEY5LRlJTDLq
mBM2zY86pQ+ZML1DcYf9pnfZehKj2gYeTbo3560itoggX4tHU/iUcJu7kc5wqVwI
TmxQeW9xX08RQ4Mw7Yzx424GjEsymmxQFVwaBKVzbZwwGpaNiuEeVqAgVQABSV6x
iC66HngnVZYRzgzt+BgSzOoBoZYN6tBFucOBYtK4fxcCmDftH6KdHzlJPmKeC+ms
LkFxOF4aVUlYc6Nw+o5Tq1F2aHGor+u5qAxdaopRfniYxDwZsUUlJsSMurvcJa1S
M9jBq66fvwqkzPy/sjndztm6bIwRyfH+tSkSqppNcIJvI5qfuNV0M3RrVesruE7W
eMYryXLUNQ2etZM9TYRoPp1MmF0jRryd5R5V1QsDKI19Y7FUtYYHTYsv9Xvov47J
ePvlj9BI0f93eVUASC8V41+ZozJ0Mhlsh2YKt45Jlrn6r7Jsl3iYLz5VmrzsE+HJ
otSXVrUiavXdpvqOhkMzdP6w35B1iYNfBHr1flqoEOnkjQDbbPvDzPS2R4ytJVNz
F6fdTNoitK9hy8m4addl6kgABQ==
=m/A5
-----END PGP PUBLIC KEY BLOCK-----''';

  group('Encryption and Decryption', () {
    setUpAll(() {
      setupSingletonInjector();
    });

    test('Encryption / Decryption', () async {
      SessionNext().set<String>('privateKey', privateKey);
      SessionNext().set<String>('publicKey', publicKey);
      SessionNext().set<String>('pincodeHash', hash);

      String attributeValue = 'Test Value';
      String encryptedValue = await CryptoUtil.encrypt(attributeValue);
      String decryptedValue = await CryptoUtil.decrypt(encryptedValue);

      singletonInjector<Logger>().d('Attribute Value : $attributeValue');
      singletonInjector<Logger>().d(
        'Encrypted Attribute Value : $encryptedValue',
      );
      singletonInjector<Logger>().d(
        'Decrypted Attribute Value : $decryptedValue',
      );

      expect(attributeValue, 'Test Value');

      await expectLater(decryptedValue, 'Test Value');
    });
  });
}
