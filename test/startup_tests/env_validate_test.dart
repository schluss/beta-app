import 'package:flutter_dotenv/flutter_dotenv.dart';
import 'package:flutter_test/flutter_test.dart';

void main() async {
  setUpAll(() async {
    /// Initialize test widgets to allow access to dot env.
    TestWidgetsFlutterBinding.ensureInitialized();
    await dotenv.load(fileName: '.env');
  });

  // Validates ENV File at startup
  group('ENV Files Validating', () {
    // Check optional fields declared the ENV file. ( With 'OPTIONAL_'  keyword)
    test('Check Mandatory Fields Defined ', () async {
      Map<String, String> envMap = dotenv.env;
      List<String> requiredList = envMap.keys
          .where(
            (element) => !element.startsWith('OPTIONAL_'),
          )
          .toList();
      bool status = dotenv.isEveryDefined(requiredList);
      expect(
        status,
        true,
        reason: 'Some Required Fields are missing. Please Check "OPTIONAL_" keyword in keys to identify optional fields ',
      );
    });

    // Check boolean fields declared the ENV file. Can be only [true] or [false] ( With 'IS_'  keyword).
    test('Check Boolean Fields Defined ', () async {
      Map<String, String> envMap = dotenv.env;
      List<String> booleanList = envMap.keys
          .where(
            (element) => element.startsWith('IS_'),
          )
          .toList();

      for (String boolKey in booleanList) {
        var boolValue = dotenv.env[boolKey];
        boolValue = boolValue == 'false' ? 'true' : boolValue;

        expect(boolValue, 'true', reason: '$boolKey is not a boolean Value');
      }
    });

    // Check URL fields declared the ENV file. ( With 'URL'  keyword)
    test('Check URL Fields ', () async {
      Map<String, String> envMap = dotenv.env;
      List<String> urlList = envMap.keys
          .where(
            (element) => element.contains('URL'),
          )
          .toList();

      for (String urlKey in urlList) {
        var urlValue = dotenv.env[urlKey];
        if (urlValue!.isNotEmpty) {
          bool validURL = Uri.parse(urlValue.toString()).isAbsolute;
          expect(validURL, true, reason: '$urlValue is not a valid URL');
        }
      }
    });

    // Check date fields declared the ENV file. ( With 'DATE'  keyword)
    // test('Check Date Fields Defined ', () async {
    //   Map<String, String> envMap = dotenv.env;
    //   List<String> dateList = envMap.keys.where((element) => element.contains('DATE')).toList();

    //   for (String dateKey in dateList) {
    //     var dateValue = dotenv.env[dateKey];
    //     expect(isDate(dateValue!), true, reason: dateKey + ' is not a valid Date with the yyyy-MM-dd Format');
    //   }
    // });

    //   Check declared ENV file with example ENV file provided.
    //   Should always test at the end due loading example env file
    //   test('Compare ENV and Example File ', () async {
    //     Map<String, String> envMap = dotenv.env;
    //     List<String> actList = envMap.keys.toList();

    //     dotenv.testLoad(fileInput: File('.env').readAsStringSync());
    //     List<String> expList = envMap.keys.toList();

    //     expect(actList.length, expList.length, reason: 'Env File and Provided Example Env file keys count mismatch');
    //     expect(listEquals(actList, expList), true, reason: 'Env File and Provided Example Env file mismatch');
    //   }, skip: 'TODO: Change ENV file with new format');
  });
}

///Validates date is in valid format.
// bool isDate(String input, {String format = 'yyyy-MM-dd'}) {
//   try {
//     DateFormat(format).parseStrict(input);
//     return true;
//   } catch (e) {
//     return false;
//   }
// }
