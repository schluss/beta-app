import 'dart:io';

import 'package:dio/dio.dart';
import 'package:process_run/shell.dart';

const sheetUrl = 'https://docs.google.com/spreadsheets/d/1uoOyy5kSc0A8eFLYTodc_hWBb0oCnB-nzZrqTQ0OQ5s/gviz/tq?tqx=out:csv&sheet=Translation';

void main() async {
  /// Get spreadsheet as CSV.
  var csvString = await Dio().get(sheetUrl);

  /// Stores as a CSV file.
  await File('./assets/translation/Translation.csv').writeAsString(csvString.data);

  /// Runs the flappy_translator process.
  await Shell().run('flutter pub run flappy_translator assets/translation/Translation.csv lib/translations');

  print('Translation texts are updated!');

  exit(0);
}
