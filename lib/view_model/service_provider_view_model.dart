/// Generics implementation for service providers (like a data requester and a Data provider).
class ServiceProviderViewModel {
  String? name;
  String? version;
  String? appName;
  String? description;
  String? logoUrl;
  String? publicKeyUrl;
  String? website;

  ServiceProviderViewModel();

  factory ServiceProviderViewModel.fromJson(Map data) {
    var model = ServiceProviderViewModel();
    // TODO: config.json file change, need to correct the naming as appName.
    model.appName = model._extract(data, 'appname');
    model.version = model._extract(data, 'version');
    model.name = model._extract(data, 'name');
    model.logoUrl = model._extract(data, 'logoUrl');
    model.description = model._extract(data, 'description');
    // TODO: config.json file change, need to correct the naming as publicKeyUrl.
    model.publicKeyUrl = model._extract(data, 'pubkeyurl');
    model.website = model._extract(data, 'website');

    return model;
  }

  String? _extract(Map data, String field) {
    if (data[field] == null) {
      return '';
    } else {
      return data[field];
    }
  }
}
