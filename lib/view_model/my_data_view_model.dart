/*
 * @author Manuja Jayawardana
 * @email mjayawardana@yukon.lk
 * @create date 2020-06-29
 */

import 'package:schluss_beta_app/view_model/field_view_model.dart';

class MyDataViewModel {
  final String? category;
  final String? providerName;
  // final String? providerDescription;
  final String? logoUrl;

  /// This is set from strings class.
  /// VANDAAG / AFGELOPEN 7 DAGEN / MAANDEN GELEDEN.
  final String? timeSpan;

  final String? type;

  final String? mediaType;

  /// This checks uploading file is rather than an image or not.
  ///
  /// [false] - if image (svg,png,jpeg,.) extension  / [true] - for other extensions.
  // final bool isDocument;

  final List<FieldViewModel>? dataList;

  MyDataViewModel({
    this.category,
    this.providerName,
    this.logoUrl,
    this.timeSpan,
    this.type,
    //this.isDocument = true,
    this.mediaType,
    this.dataList,
  });

  factory MyDataViewModel.fromJson(Map<String, dynamic> json) {
    return MyDataViewModel(
        category: json['category'],
        providerName: json['providerName'],
        logoUrl: json['logoUrl'],
        dataList: json['dataList'],
        timeSpan: json['timeSpan'],
        type: json['type'],
        //isLocal: json['isLocal'] ?? false,
        //isDocument: json['isDocument'] ?? false,
        mediaType: json['mediaType']);
  }
}
