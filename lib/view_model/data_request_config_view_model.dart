import 'dart:convert';

import 'package:schluss_beta_app/view_model/provider_config_view_model.dart';
import 'package:schluss_beta_app/view_model/service_provider_view_model.dart';

/// Implementations of a [DataRequest] from a [config.json] file.
class DataRequestConfigViewModel {
  /// Information about the data requesting entity.
  late ServiceProviderViewModel dataRequester;

  String? scope;
  String? category;
  String? name;
  String? description;
  String? details;
  String? timeConstraint;

  /// Configurations file URL for data request configurations.
  String? settingsUrl;
  // TODO: need to validate this endpoints variable?.
  // Map<String, String> endpoints;
  String? statusUrl;
  String? returnUrl;

  /// Requests output format.
  String? format;

  /// Contains the [dataRequest] raw json encoded [config.json] file contents.
  String? rawData;

  List<ProviderConfigViewModel> providers = [];

  DataRequestConfigViewModel();

  /// Factories for creating a new instance of [DataRequestConfigViewModel] using a [config.json]
  /// [dataRequest] object and the scope.
  factory DataRequestConfigViewModel.fromJson(
    Map<String, dynamic> data,
    String? scope,
    String? settingsUrl,
  ) {
    var model = DataRequestConfigViewModel();
    model.rawData = json.encode(data);

    // Load import section including the providers as the [dataRequest].
    var imports = List<Map<String, dynamic>>.from(data['import']);

    for (var i = 0; i < imports.length; i++) {
      if (imports[i]['scope'] != null && imports[i]['scope'].toString().toLowerCase() == scope!.toLowerCase()) {
        model.scope = model._extract(imports[i], 'scope');
        model.name = model._extract(imports[i], 'name');
        model.category = model._extract(imports[i], 'category');
        model.description = model._extract(imports[i], 'description');
        model.details = model._extract(imports[i], 'details');
        // TODO: config.json file change, need to correct the naming as timeConstraint.
        model.timeConstraint = model._extract(imports[i], 'timeconstraint');
        model.format = model._extract(imports[i], 'format')!.toLowerCase();
        model.settingsUrl = settingsUrl;
        // model.endpoints = Map<String, String>.from(imports[i]['endpoints']);
        model.statusUrl = model._extract(imports[i]['endpoints'], 'status');
        model.returnUrl = model._extract(imports[i]['endpoints'], 'return');

        // Add all the providers for this data request.
        var providers = List<Map<String, dynamic>>.from(imports[i]['providers']);

        for (var element in providers) {
          model.providers.add(ProviderConfigViewModel.fromJson(element));
        }

        break;
      }
    }

    if (model.providers.isEmpty) {
      throw ('Invalid config.json: dataRequest for scope ${scope!}. No dataproviders found');
    }

    // Set the data requester details.
    model.dataRequester = ServiceProviderViewModel.fromJson(data);

    return model;
  }

  /// Extracts and return a String value from a map.
  String? _extract(Map data, String field, {String defaultValue = ''}) {
    if (data[field] == null || data[field] == '') {
      return defaultValue;
    } else {
      return data[field];
    }
  }
}
