// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'contract_viewmodel.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

ContractViewModel _$ContractViewModelFromJson(Map<String, dynamic> json) =>
    ContractViewModel(
      id: json['id'] as int?,
      provider: json['provider'] as String?,
      scope: json['scope'] as String?,
      date: json['date'] as int?,
      version: json['version'] as String?,
      iconColor: json['iconColor'] as String?,
      shares: (json['shares'] as List<dynamic>?)
          ?.map((e) => ContractProvider.fromJson(e as Map<String, dynamic>))
          .toList(),
      requester: json['requester'] == null
          ? null
          : Requester.fromJson(json['requester'] as Map<String, dynamic>),
      contractId: json['contractId'] as String?,
      description: json['description'] as String?,
      notification: json['notification'] as bool?,
      type: json['type'] as String?,
      clientKey: json['clientKey'] as String?,
      gatewayUrl: json['gatewayUrl'] as String?,
    );

Map<String, dynamic> _$ContractViewModelToJson(ContractViewModel instance) =>
    <String, dynamic>{
      'id': instance.id,
      'contractId': instance.contractId,
      'provider': instance.provider,
      'scope': instance.scope,
      'date': instance.date,
      'version': instance.version,
      'iconColor': instance.iconColor,
      'description': instance.description,
      'shares': instance.shares,
      'requester': instance.requester,
      'notification': instance.notification,
      'type': instance.type,
      'clientKey': instance.clientKey,
      'gatewayUrl': instance.gatewayUrl,
    };
