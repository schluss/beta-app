import 'package:json_annotation/json_annotation.dart';
import 'package:schluss_beta_app/data/models/contract/contract_provider.dart';
import 'package:schluss_beta_app/data/models/contract/contract_requester.dart';

part 'contract_viewmodel.g.dart';

@JsonSerializable()
class ContractViewModel {
  int? id;

  /// uuidv4.
  String? contractId;

  /// Schluss.
  String? provider;
  String? scope;
  int? date;
  String? version;
  String? iconColor;
  String? description;
  List<ContractProvider>? shares;
  Requester? requester;

  /// [true] - Shows notifications / [false] - Hides notifications.
  bool? notification = true;

  /// user/org.
  String? type;
  String? clientKey;
  String? gatewayUrl;

  ContractViewModel({
    this.id,
    this.provider,
    this.scope,
    this.date,
    this.version,
    this.iconColor,
    this.shares,
    this.requester,
    this.contractId,
    this.description,
    this.notification,
    this.type,
    this.clientKey,
    this.gatewayUrl,
  });

  factory ContractViewModel.fromJson(
    Map<String, dynamic> json,
  ) =>
      _$ContractViewModelFromJson(json);

  Map<String, dynamic> toJson() => _$ContractViewModelToJson(this);

  @override
  String toString() {
    return 'ContractViewModel{contractId: $id, contractId: $contractId, provider: $provider, scope: $scope, date: $date, version: $version, iconColor: $iconColor,  shares: $shares, notification: $notification, type: $type, clientKey: $clientKey, gatewayUrl: $gatewayUrl,}';
  }
}
