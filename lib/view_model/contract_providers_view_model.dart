import 'package:schluss_beta_app/view_model/contract_detail_view_model.dart';

class ContractProvidersViewModel {
  String? requesterType;
  String? requesterName;
  String? requestDate;
  String? logoUrl;
  String? iconColor;
  String? description;
  List<ContractDetailViewModel>? providerList;
  bool isReleased;

  ContractProvidersViewModel({
    this.requesterType,
    this.requesterName,
    this.requestDate,
    this.logoUrl,
    this.iconColor,
    this.description,
    this.providerList,
    this.isReleased = false,
  });
}
