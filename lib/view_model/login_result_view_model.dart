class LoginResultViewModel {
  final String privateKey;
  final String publicKey;
  final String randomKey;

  LoginResultViewModel({
    required this.privateKey,
    required this.publicKey,
    required this.randomKey,
  });
}
