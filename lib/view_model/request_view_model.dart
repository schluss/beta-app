import 'dart:convert';

import 'package:schluss_beta_app/constants/url_constants.dart';
import 'package:schluss_beta_app/view_model/data_request_config_view_model.dart';
import 'package:schluss_beta_app/view_model/provider_config_view_model.dart';

///  Passes the request model in the data process.
class RequestViewModel {
  final int? id;
  final String? name;
  final String? logoUrl;
  final String? importName;
  final String? importDescription;
  // final String importFormat;
  final String? token;
  final String? scope;
  final String? settingsUrl;

  /// P - Pending , C - Complete.
  final String? status;
  final String? label;

  /// MillisecondsSinceEpoch.
  final int? requestDate;

  /// Completes providers (Index table Id) list in table.
  String? providerIdList;

  List<Map>? loadedData;

  /// Apps config.json raw data (Json encoded).
  final String? appConfigRawData;

  /// Providers config.json raw data list (Json encoded).
  final List<dynamic>? providerConfigRawData;

  /// Sends providers with request.
  List<ProviderConfigViewModel> _dataProviderList = [];
  List<ProviderConfigViewModel> getProviderList() => _dataProviderList;
  void setProviderList(value) => _dataProviderList = value ?? [];

  /// Sends selected provider with request.
  int? _currentSelectedIndex = -1;
  int? getCurrentSelectedIndex() => _currentSelectedIndex;
  void setCurrentSelectedIndex(value) => _currentSelectedIndex = value;

  RequestViewModel({
    this.id,
    this.name,
    this.logoUrl,
    this.importName,
    this.importDescription,
    this.token,
    this.scope,
    this.settingsUrl,
    this.status,
    this.label,
    this.requestDate,
    this.providerIdList,
    this.loadedData,
    this.appConfigRawData,
    this.providerConfigRawData,
  });

  /// Generates [RequestViewModel] from configuration URL.
  factory RequestViewModel.fromUrl(
    String configUrl,
    DataRequestConfigViewModel dataRequest,
  ) {
    var pathsList = Uri.parse(configUrl).pathSegments;

    List<String?> providerConfigList = [];

    for (ProviderConfigViewModel? model in dataRequest.providers) {
      providerConfigList.add(model!.rawData);
    }

    return RequestViewModel(
      name: dataRequest.dataRequester.name,
      logoUrl: dataRequest.dataRequester.logoUrl,
      importName: dataRequest.name,
      importDescription: dataRequest.description,
      token: pathsList.elementAt(UrlConstants.dataRequestTokenIndex),
      scope: pathsList.elementAt(UrlConstants.dataRequestScopeIndex),
      settingsUrl: pathsList.elementAt(UrlConstants.dataRequestConfigUrlIndex),
      status: 'P',
      label: dataRequest.dataRequester.appName,
      requestDate: DateTime.now().millisecondsSinceEpoch,
      appConfigRawData: dataRequest.rawData,
      providerConfigRawData: providerConfigList,
    );
  }

  /// Generates [RequestViewModel] from JSON/Map.
  factory RequestViewModel.fromJson(Map<String, dynamic> map) {
    return RequestViewModel(
      id: map['id'],
      name: map['name'],
      logoUrl: map['logoUrl'],
      importName: map['importName'],
      importDescription: map['importDescription'],
      token: map['token'],
      scope: map['scope'],
      settingsUrl: map['settings_url'],
      status: map['status'],
      label: map['label'],
      requestDate: map['request_date'],
      providerIdList: map['providers'],
      loadedData: map['loadedData'],
      appConfigRawData: map['appConfigRawData'],
      providerConfigRawData: map['providerConfigRawData'],
    );
  }

  @override
  String toString() {
    return json.encode(this);
  }
}
