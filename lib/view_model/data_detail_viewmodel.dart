import 'package:schluss_beta_app/view_model/field_view_model.dart';
import 'package:schluss_beta_app/view_model/provider_config_view_model.dart';

/// Views model of data details screen.
class DataDetailViewModel {
  final String? requesterName;
  final String? logoUrl;
  String? dateRequested;
  final bool? isCompleted;
  final Map<String?, List<FieldViewModel>>? dataMap;

  DataDetailViewModel({
    this.requesterName,
    this.logoUrl,
    this.dateRequested,
    this.isCompleted,
    this.dataMap,
  });

  set setDateRequested(value) => dateRequested = value;

  /// Generates [DataDetailViewModel] from JSON/Map.
  factory DataDetailViewModel.fromProviderModel(
    ProviderConfigViewModel providerModel,
  ) {
    // Groups provider attributes.
    Map<String?, List<FieldViewModel>> dataMap = {};

    for (var field in providerModel.fields!) {
      if (dataMap.containsKey(field.category)) {
        List<FieldViewModel> containList = dataMap[field.category]!;
        containList.add(field);
        dataMap.update(field.category, (value) => containList);
      } else {
        List<FieldViewModel> newList = [];
        newList.add(field);
        dataMap.putIfAbsent(field.category, () => newList);
      }
    }

    return DataDetailViewModel(
      requesterName: providerModel.dataProvider!.name,
      logoUrl: providerModel.dataProvider!.logoUrl,
      isCompleted: providerModel.isChecked,
      dataMap: dataMap,
    );
  }

  Map toJson() => {
        'name': requesterName,
        'logo': logoUrl,
      };
}
