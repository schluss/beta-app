/*
 * @author Manuja Jayawardana
 * @email mjayawardana@yukon.lk 
 * @create date 2020-08-25
 */

/// Views model for app settings.
class SettingsViewModel {
  final int? id;

  /// These modes are for enabling and disabling test-mode of the app.
  ///
  /// For enabling, make this [true].
  /// For disabling (activating mode) make this [false].
  final bool stewardshipMode;
  final bool dataSharingMode;
  final bool testMode;

  /// This is for showing the data request guideline bottom-up sheet or not.
  ///
  /// For showing it, make this true, otherwise make it false.
  final bool tourDataRequestOpen;

  final String iconColor;

  SettingsViewModel({
    this.id,
    this.stewardshipMode = false,
    this.dataSharingMode = false,
    this.testMode = false,
    this.tourDataRequestOpen = false,
    this.iconColor = '',
  });

  /// Generates [SettingsViewModel] from a JSON/Map.
  factory SettingsViewModel.fromJson(Map<String, dynamic> json) {
    return SettingsViewModel(
      id: json['id'],
      stewardshipMode: json['stewardshipMode'],
      dataSharingMode: json['dataSharingMode'],
      testMode: json['testMode'],
      tourDataRequestOpen: json['tourDataRequestOpen'],
      iconColor: json['iconColor'],
    );
  }
}
