class ProviderDataModel {
  final String? title;
  final String? subTitle;
  final String? imagePath;
  final bool isChecked;
  final int noOfShares;
  final String? iconColor;

  ProviderDataModel(
    this.title,
    this.subTitle,
    this.imagePath,
    this.isChecked, {
    this.noOfShares = 0,
    this.iconColor,
  });
}
