import 'package:schluss_beta_app/business/impl/notification_bl_impl.dart';

class NotificationViewModel {
  final int? id;
  final int connectionId;
  final String? titleKey;
  final String? descriptionKey;
  final String? routeNameKey;
  final String? routeParams;
  final bool? isClosable;

  /// A-Active , I-Inactive , R-Removed.
  final NotificationState? status;

  /// T-Tips.
  final NotificationType? type;

  /// MillisecondsSinceEpoch.
  final int? timestamp;

  NotificationViewModel(
    this.connectionId, {
    this.id,
    this.titleKey,
    this.descriptionKey,
    this.routeNameKey,
    this.routeParams,
    this.isClosable,
    this.status,
    this.type,
    this.timestamp,
  });

  /// Converts [NotificationViewModel] to a JSON/Map.
  Map<String, dynamic> toJson() => {
        'id': id,
        'connectionId': connectionId,
        'title': titleKey,
        'description': descriptionKey,
        'routeName': routeNameKey,
        'routeParams': routeParams,
        'isClosable': isClosable,
        'timestamp': timestamp,
        'status': status,
        'type': type,
      };

  /// Generates [NotificationViewModel] from a JSON/Map.
  factory NotificationViewModel.fromJson(Map<String, dynamic> json) {
    return NotificationViewModel(
      json['connectionId'],
      id: json['id'],
      titleKey: json['title'],
      descriptionKey: json['description'],
      routeNameKey: json['routeName'],
      routeParams: json['routeParams'],
      isClosable: json['isClosable'],
      timestamp: json['timestamp'],
      status: json['status'] == 'A'
          ? NotificationState.active
          : json['status'] == 'I'
              ? NotificationState.inactive
              : NotificationState.removed,
      type: json['type'] == 'T' ? NotificationType.tip : null,
    );
  }
}
