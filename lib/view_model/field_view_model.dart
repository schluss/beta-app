/// Generics representation of a field, present inside the import and export section
/// in data request config.json files.
class FieldViewModel {
  /// From imports section in config.json files.
  String? match = '';
  bool? isRequired = true;
  bool isDecrypted = false;

  /// Used to define if the value is requested from the [current] vault or from the [owner], if empty, by default: [owner] is assumed
  //String? from = '';

  /// From exports section in config.json files.
  String? name;
  String? label;
  String? description;
  String? category;
  String? group;
  String? type;
  String? format;
  String? displayValue;
  String? providerName;
  String? logoUrl;

  /// Retrieves value.
  String? value = '';
  bool isVC = false;

  int? attributeId;
  int? attributeVersionId;
  int? attributeValueId;
  DateTime? retrievedTime;
  Map<String, String>? keys;
  String? attrKeyEnc;
  String? gatewayShareId;

  /// Whether to hide the value or not
  bool isHideValue = false;

  /// To validates whether local attribute or shared attribute.
  bool isIncomingShare = false;

  /// Full paths to the [gateway/linkId/shareId].
  String? gatewayPath;

  /// The required authorizations token to be able to retrieve the encrypted
  /// attribute value when accessing [gatewayPath].
  String? gatewayAuthToken;

  /// The attributes decryption key, required to be able to decrypt the retrieved
  /// attribute value from [gatewayPath].
  String? gatewayAttributeKey;

  /// Contains the UUID (filename) of the locally stored encrypted attribute value.
  String? localPath;
  int? contractId;

  FieldViewModel({
    this.name,
    this.label,
    this.description,
    this.category,
    this.group,
    this.type,
    this.format,
    this.displayValue,
    this.match,
    //this.from,
    this.isRequired,
    this.value,
    this.providerName,
    this.logoUrl,
    this.attributeId,
    this.attributeVersionId,
    this.attributeValueId,
    this.retrievedTime,
    this.keys,
    this.isVC = false,
    this.attrKeyEnc,
    this.isDecrypted = false,
    this.isHideValue = false,
    this.isIncomingShare = false,
  });

  /// Parses a single field from the [import.providers[x].fields[x]]
  /// section in the [config.json] file from a data requester.
  void fromImportJson(Map data) {
    match = _extractString(data, 'match');
    isRequired = _extractBool(data, 'required');
  }

  /// Parses a single field from the [export.fields[x]] section in the config.json.
  /// file from a data provider.
  void fromExportJson(Map data) {
    name = _extractString(data, 'name');
    label = _extractString(data, 'label');
    description = _extractString(data, 'description');
    category = _extractString(data, 'category');
    group = _extractString(data, 'group');
    format = _extractString(data, 'format');
    type = _extractString(data, 'type');
    // Note: Must match the config.json attr name: display value and not [displayValue].
    displayValue = _extractString(data, 'displayvalue');
  }

  // Parses a fields from the database tables.
  void fromDatabaseData(Map data) {
    name = _extractString(data, 'name');
    label = _extractString(data, 'label');
    type = _extractString(data, 'type');
    attributeId = _extract(data, 'attributeId');
    attributeVersionId = _extract(data, 'attributeVersionId');
    attributeValueId = _extract(data, 'attributeValueId');
    retrievedTime = _extract(data, 'retrievedTime');
    displayValue = _extractString(data, 'displayValue');
    value = _extractString(data, 'value');
    category = _extractString(data, 'category');
    group = _extractString(data, 'group');
    keys = data['keys'];
    isVC = _extractBool(data, 'isVC');
    attrKeyEnc = _extractString(data, 'attrKeyEnc');
    providerName = _extractString(data, 'providerName');
    logoUrl = _extractString(data, 'logoUrl');
    gatewayShareId = _extractString(data, 'gatewayShareId');
    isHideValue = _extractBool(data, 'isHideValue', defaultValue: true);
    isIncomingShare = _extractBool(data, 'isIncomingShare');
  }

  /// Parses a fields from the database tables.
  void fromIncomingShare(Map data) {
    name = _extractString(data, 'name');
    label = _extractString(data, 'label');
    type = _extractString(data, 'type');
    attributeId = _extract(data, 'id');
    retrievedTime = DateTime.fromMillisecondsSinceEpoch(_extract(data, 'timestamp'));
    displayValue = _extractString(data, 'displayValue');
    category = _extractString(data, 'category');
    group = _extractString(data, 'group');
    isVC = _extractBool(data, 'isVC');
    attrKeyEnc = _extractString(data, 'attrKeyEnc');
    providerName = _extractString(data, 'providerName');
    logoUrl = _extractString(data, 'logoUrl');
    gatewayShareId = _extractString(data, 'gatewayShareId');
    isHideValue = _extractBool(data, 'isHideValue', defaultValue: true);
    isIncomingShare = true;
    gatewayPath = _extractString(data, 'gatewayPath');
    gatewayAuthToken = _extractString(data, 'gatewayAuthToken');
    gatewayAttributeKey = _extractString(data, 'gatewayAttributeKey');
    localPath = _extractString(data, 'localPath');
    contractId = _extract(data, 'contractId');
  }

  /// Parses fields from the provider plugins.
  void fromPlugin(Map data) {
    name = _extractString(data, 'name');
    label = _extractString(data, 'label');
    type = _extractString(data, 'type');
    retrievedTime = _extract(data, 'retrievedTime');
    displayValue = _extractString(data, 'displayValue');
    value = _extractString(data, 'value');
    category = _extractString(data, 'category');
    group = _extractString(data, 'group');
  }

  /// Extracts and return a String value from a map.
  String? _extractString(Map data, String field, {String defaultValue = ''}) {
    if (data[field] == defaultValue) {
      return '';
    } else {
      return data[field];
    }
  }

  // Extracts and return a boolean value from a map.
  bool _extractBool(Map data, String field, {bool defaultValue = false}) {
    if (data[field] == null) return defaultValue;

    return data[field].toString().toLowerCase() == 'true';
  }

  /// Extracts and return a dynamic value from a map
  /// returns null if field not available.
  dynamic _extract(Map data, String field) {
    return data[field];
  }

  /// Copies/Updates values to new object.
  FieldViewModel copyWith({
    name,
    label,
    description,
    category,
    type,
    format,
    displayValue,
    match,
    //from,
    isRequired,
    value,
    attributeId,
    attributeVersionId,
    attributeValueId,
    retrievedTime,
    keys,
    isVC,
  }) =>
      FieldViewModel(
        name: name ?? this.name,
        label: label ?? this.label,
        description: description ?? this.description,
        category: category ?? this.category,
        type: type ?? this.type,
        format: format ?? this.format,
        displayValue: displayValue ?? this.displayValue,
        match: match ?? this.match,
        //from: from ?? this.from,
        isRequired: isRequired ?? this.isRequired,
        value: value ?? this.value,
        attributeId: attributeId ?? this.attributeId,
        attributeVersionId: attributeVersionId ?? this.attributeVersionId,
        attributeValueId: attributeValueId ?? this.attributeValueId,
        retrievedTime: retrievedTime ?? this.retrievedTime,
        keys: keys ?? this.keys,
        isVC: isVC ?? this.isVC,
      );
}
