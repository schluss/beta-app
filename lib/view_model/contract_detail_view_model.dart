import 'package:schluss_beta_app/view_model/field_view_model.dart';

class ContractDetailViewModel {
  String? providerName;
  String? providerDescription;
  String? providerLogo;
  String? providerIconColor;
  String? providerDate;
  String? requesterName;
  String? fromName;
  Map<String?, List<FieldViewModel>>? dataMap;

  ContractDetailViewModel({
    this.providerName,
    this.providerDescription,
    this.dataMap,
    this.providerDate,
    this.requesterName,
    this.providerIconColor,
    this.fromName,
  });
}
