import 'package:schluss_beta_app/data/models/contract/contract_model.dart';
import 'package:schluss_beta_app/view_model/provider_data_model.dart';

class ContractItem {
  ProviderDataModel providerDataItem;

  String? description;

  /// Contracts id.
  int? contractId;

  /// The clients type, can be 'user' when it is another Schluss user, or 'org'
  /// when it's an organization.
  ConnectionType? type;

  /// If it is a special type of connection it's defined here.
  StewardshipType? stewardshipType;

  ContractItem(
    this.providerDataItem,
    this.description,
    this.contractId, {
    this.type = ConnectionType.organization,
    this.stewardshipType,
  });
}
