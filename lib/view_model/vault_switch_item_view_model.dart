/// ViewModel required for the list items in the vault switching panel
class VaultSwitchItemViewModel {
  final int connectionId;
  final String color;
  final String name;

  VaultSwitchItemViewModel({
    required this.connectionId,
    required this.color,
    required this.name,
  });
}
