import 'dart:convert';

import 'package:schluss_beta_app/ui/dashboard/stewardship/owner_define_access_timing_page_content.dart';

/// For passing the  data in external storage gateway process.
class GatewayViewModel {
  final String? key;
  final String? secret;
  final int? status;
  final String? timestamp;
  final String? token;
  final String? objectId;
  final String? location;
  final String? firstName;
  final String? lastName;
  final String? clientKey;
  final String? clientPublicKey;
  final int? sessionId;
  final String? socketUrl;

  List<int>? sharedAttributesIds = [];
  DataAccessType? dataAccessType;
  String? triggerKey;

  GatewayViewModel({
    this.key,
    this.secret,
    this.status,
    this.timestamp,
    this.token,
    this.objectId,
    this.location,
    this.firstName,
    this.lastName,
    this.clientKey,
    this.clientPublicKey,
    this.sessionId,
    this.socketUrl,
    this.sharedAttributesIds,
    this.dataAccessType,
    this.triggerKey,
  });

  /// Generates [GatewayViewModel] from JSON/Map.
  factory GatewayViewModel.fromJson(Map<String, dynamic> map) {
    var dataAccessType = map.containsKey('dataAccessType')
        ? map['dataAccessType'] == 'afterPassed'
            ? DataAccessType.afterPassed
            : DataAccessType.immediately
        : DataAccessType.immediately;

    return GatewayViewModel(
      key: map['key'],
      secret: map['secret'],
      status: map['status'],
      timestamp: map['timestamp'],
      token: map['token'],
      objectId: map['objectId'],
      location: map['location'],
      firstName: map['firstName'],
      lastName: map['lastName'],
      clientKey: map['clientKey'],
      clientPublicKey: map['clientPublicKey'],
      sessionId: map['sessionId'],
      socketUrl: map['socketUrl'],
      triggerKey: map['triggerKey'],
      dataAccessType: dataAccessType,
    );
  }

  String get secretData {
    return json.encode({'key': key, 'secret': secret});
  }
}
