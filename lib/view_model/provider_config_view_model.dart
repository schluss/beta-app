import 'dart:convert';

import 'package:schluss_beta_app/data/models/attribute_version_model.dart';
import 'package:schluss_beta_app/view_model/field_view_model.dart';
import 'package:schluss_beta_app/view_model/service_provider_view_model.dart';

/// Representations of a single provider inside the providers section.
/// in the data requesters config.json file.
class ProviderConfigViewModel {
  /// Information about the data providing entity.
  ServiceProviderViewModel? dataProvider;

  final String? plugin;
  final String? request;
  final String? description;
  final String? src;

  /// Used to define if the value is requested from the [current] vault or from the [owner], if empty, by default: [owner] is assumed
  final String? from;

  /// When attributes need to become a VC,self signed by Schluss
  /// (as long as the data provider does not provide VC's themselves yet).
  late bool selfSignedAttributes;

  List<FieldViewModel>? fields = [];

  // TODO: find better solution for plugin settings.
  late PluginSettings pluginSettings;

  /// This checks is for keeping the track of the pending data request.
  bool isChecked = false;
  List<AttributeVersionModel> attributeVersionList = [];

  /// Contains the data provider raw json encoded config.json file contents.
  String? rawData;

  ProviderConfigViewModel({
    this.plugin,
    this.request,
    this.description,
    this.src,
    this.fields,
    this.from = 'owner',
  });

  /// Parses and injects the provider details from data providers' config.json file.
  ///
  /// Note: The provider must have been instantiated first using the factory method.
  void fromConfigJson(Map<String, dynamic> data) {
    rawData = json.encode(data);

    selfSignedAttributes = (data.keys.contains(
              'selfSignedAttributes',
            ) &&
            data['selfSignedAttributes']
        ? true
        : false);

    // Set the data provider details.
    dataProvider = ServiceProviderViewModel.fromJson(data);

    if (dataProvider == null) {
      throw ('Information about data provider in data request could not be found.');
    }

    var exports = List<Map<String, dynamic>>.from(data['export']);

    // Go through all 'import' fields whom are set already.
    for (var field in fields!) {
      // Go through all export fields.
      for (var i = 0; i < exports.length; i++) {
        // If import.match equals export.name, we have a match.
        if (field.match!.toLowerCase() == exports[i]['name'].toString().toLowerCase()) {
          // Store the additional info at the field.
          field.fromExportJson(exports[i]);

          break;
        }
      }
    }

    pluginSettings = PluginSettings();
    // TODO: config.json file change, need to correct the naming as siteUrl.
    pluginSettings.siteUrl = data['pluginsettings']['siteurl'];
    // TODO: config.json file change, need to correct the naming as pluginSettings.
    pluginSettings.trigger = data['pluginsettings']['trigger'];
  }

  /// Factories to create a new instance of [ProviderConfigViewModel]
  /// from the data requesters config.json provider section.
  factory ProviderConfigViewModel.fromJson(Map<String, dynamic> json) {
    var dataList = json['fields'];

    List<FieldViewModel> fields = [];

    dataList.forEach((val) {
      FieldViewModel model = FieldViewModel();
      model.fromImportJson(val);
      fields.add(model);
    });

    return ProviderConfigViewModel(
      plugin: json['plugin'],
      request: json['request'],
      description: json['description'],
      src: json['src'],
      from: json['from'],
      fields: fields,
    );
  }

  Map<String, dynamic> toJson() => {'title': request};
}

/// Helpers to structure the different endpoint URLs inside the plugin settings section.
class PluginSettings {
  String? siteUrl;
  String? trigger;
}
