import 'package:flutter/material.dart';
import 'package:schluss_beta_app/controller/plugin_controller.dart';
import 'package:schluss_beta_app/view_model/request_view_model.dart';

class PluginProvider extends ChangeNotifier {
  int progressValue = 0;

  /// Gets progress value from stream & notify to relevant consumers to update UI.
  void getCurrentProgress(RequestViewModel requestViewModel) async {
    PluginController().getProgressValue(requestViewModel).listen((value) async {
      progressValue = value;
      ChangeNotifier();
    });
  }
}
