/*
 * @author Manuja Jayawardana
 * @email mjayawardana@yukon.lk
 * @create date 2020-07-15
 */
import 'dart:async';

import 'package:flutter/cupertino.dart';
import 'package:flutter/foundation.dart';
import 'package:logger/logger.dart';
import 'package:schluss_beta_app/business/impl/contract_manager_bl_impl.dart';
import 'package:schluss_beta_app/controller/account_controller.dart';
import 'package:schluss_beta_app/controller/notification_controller.dart';
import 'package:schluss_beta_app/controller/request_controller.dart';
import 'package:schluss_beta_app/singleton_injector.dart';
import 'package:schluss_beta_app/view_model/contract_recent_item.dart';
import 'package:schluss_beta_app/view_model/my_data_view_model.dart';
import 'package:schluss_beta_app/view_model/notification_view_model.dart';
import 'package:schluss_beta_app/view_model/request_view_model.dart';
import 'package:session_next/session_next.dart';

class HomeProvider extends ChangeNotifier {
  final RequestController _requestController = RequestController();

  final NotificationController _notificationController = NotificationController();
  final AccountController _accountController = AccountController();

  final Logger _logger = singletonInjector<Logger>();

  final ContractManagerBLImpl _contractManagerBL = singletonInjector<ContractManagerBLImpl>();

  List<NotificationViewModel> tipsList = [];
  List<RequestViewModel> minimizedList = [];
  List<ContractItem> doneList = [];
  List<ContractItem> contractsList = [];
  List<MyDataViewModel> dataList = [];

  bool _isDisposed = false;

  @override
  void dispose() {
    _isDisposed = true;

    super.dispose();
  }

  // TODO: remove BL classes from home. Shouldn't be call BL class from UI.
  //  Should go through controller.

  /// Checks whether stewardship mode is on/off.
  Future<bool> isStewardshipModeOn() async {
    return await _accountController.isStewardshipModeOn();
  }

  /// Checks whether data sharing mode is on/off.
  Future<bool> isDataSharingModeOn() async {
    return await _accountController.isDataSharingModeOn();
  }

  /// Checks whether test mode is on/off.
  Future<bool> isTestModeOn() async {
    return await _accountController.isTestModeOn();
  }

  /// Checks whether the actual safe owner or not.
  bool isInOwnVault() {
    return _accountController.isInOwnVault();
  }

  /// Checks tips.
  void checkTips() async {
    var connectionId = SessionNext().get<int>('connectionId') ?? 0;

    tipsList = await _notificationController.getTips(connectionId: connectionId);

    if (!_isDisposed) {
      notifyListeners();
    }
  }

  /// Checks pending requests.
  void checkPendingRequests() async {
    List<RequestViewModel> pendingRequests;
    pendingRequests = await _requestController.getPendingRequests();

    if (pendingRequests.isNotEmpty) {
      _logger.d('Pending Requests Found');

      minimizedList = pendingRequests;
    } else {
      _logger.d('No Pending Requests Found');
    }

    if (!_isDisposed) {
      notifyListeners();
    }
  }

  /// Checks completed contracts.
  void checkFinishedContracts() async {
    List<ContractItem> finishedContracts;
    finishedContracts = await _contractManagerBL.getAllContracts();

    if (finishedContracts.isNotEmpty) {
      _logger.i('Finished Contracts Found ');

      doneList = finishedContracts;
    } else {
      _logger.i('No Finished Contracts Found');
    }

    if (!_isDisposed) {
      notifyListeners();
    }
  }

  /// Removes pending requests notifications.
  void removePendingRequest(RequestViewModel request) async {
    try {
      await _requestController.removeRequest(request);

      minimizedList.remove(request);
    } catch (e) {
      _logger.i(e);
    }

    if (!_isDisposed) {
      notifyListeners();
    }
  }

  /// Clears completed contracts notifications.
  void clearContractsDoneList() {
    try {
      _contractManagerBL.hideContractNotifications();
      doneList.clear();

      if (!_isDisposed) {
        notifyListeners();
      }
    } catch (e) {
      _logger.e(e);
    }
  }
}
