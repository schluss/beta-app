import 'package:flutter/material.dart';
import 'package:schluss_beta_app/business/impl/contract_manager_bl_impl.dart';
import 'package:schluss_beta_app/singleton_injector.dart';
import 'package:schluss_beta_app/view_model/contract_recent_item.dart';

class ContractDataProvider extends ChangeNotifier {
  final ContractManagerBLImpl _contractManagerBL = singletonInjector<ContractManagerBLImpl>();

  Map<String?, List<ContractItem>> contractMap = {};

  bool _isDisposed = false;
  bool isLoading = false;

  @override
  void dispose() {
    _isDisposed = true;
    super.dispose();
  }

  void getContracts() async {
    isLoading = true;

    notifyListeners();

    contractMap = await _contractManagerBL.getContracts();

    isLoading = false;

    if (!_isDisposed) {
      notifyListeners();
    }
  }
}
