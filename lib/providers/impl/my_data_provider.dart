import 'package:flutter/cupertino.dart';
import 'package:flutter/foundation.dart';
import 'package:logger/logger.dart';
import 'package:schluss_beta_app/controller/contract_controller.dart';
import 'package:schluss_beta_app/controller/my_data_controller.dart';
import 'package:schluss_beta_app/singleton_injector.dart';
import 'package:schluss_beta_app/view_model/field_view_model.dart';
import 'package:schluss_beta_app/view_model/my_data_view_model.dart';

class MyDataProvider extends ChangeNotifier {
  final MyDataController _myDataController = MyDataController();
  final ContractController _contractController = ContractController();

  final Logger _logger = singletonInjector<Logger>();

  Map<String?, List<MyDataViewModel>?> dataMap = {};

  List<FieldViewModel> attributes = [];
  bool isLoading = true;
  bool _isDisposed = false;

  @override
  void dispose() {
    _isDisposed = true;
    super.dispose();
  }

  /// Checks if a group is unselected, partially selected or fully selected
  /// using the [selectedAttrs] as input, the [group] to check and all the [attributeIdsInGroup].
  bool? isGroupSelected(
    Map<String, List<int>> selectedAttrs,
    String group,
    List<int> attributeIdsInGroup,
  ) {
    if (selectedAttrs.isEmpty) {
      return false;
    }

    if (selectedAttrs.containsKey(group)) {
      // Is all is selected inside the group?.
      if (selectedAttrs[group]!.length == attributeIdsInGroup.length) {
        return true;
      }

      // Or only a few?.
      return null;
    }

    return false;
  }

  /// Checks if attribute is selected [selectedAttrs] given the [group] and [attributeId].
  bool isAttributeSelected(
    Map<String, List<int>> selectedAttrs,
    String group,
    int attributeId,
  ) {
    if (selectedAttrs.isEmpty || !selectedAttrs.containsKey(group)) {
      return false;
    }

    // When whole group and therefore also attribute is selected.
    if (selectedAttrs.containsKey(group) && selectedAttrs[group]!.isEmpty) {
      return true;
    }

    if (selectedAttrs[group]!.contains(attributeId)) {
      return true;
    }

    return false;
  }

  /// Updates [selectedAttrs] with [selection], given the [group] and the list
  /// of all [attributeIdsInGroup] in that [group].
  /// If there are [alreadySharedAttributes], these will be taken out of the selection.
  void updateGroupSelection(
    Map<String, List<int>> selectedAttrs,
    bool? selection,
    String group,
    List<int> attributeIdsInGroup,
    List<int> alreadySharedAttributes,
  ) {
    // When partially selected.
    if (selection == null) {
      selectedAttrs.removeWhere((key, value) => key == group);
      return;
    }

    // When group fully selected.
    if (selection) {
      List<int> toAdd = [];
      for (var attrId in attributeIdsInGroup) {
        if (!alreadySharedAttributes.contains(attrId)) {
          toAdd.add(attrId);
        }
      }

      selectedAttrs[group] = toAdd;
    }
    // When deselected.
    else {
      selectedAttrs.removeWhere((key, value) => key == group);
    }
  }

  /// Updates [selectedAttrs] with [selection], given the [group] and the [attributeId].
  void updateAttributeSelection(
    Map<String, List<int>> selectedAttrs,
    bool selection,
    String group,
    int attributeId,
  ) {
    // Attribute is selected.
    if (selection) {
      // When group does not exist yet (first attribute selected in group).
      if (!selectedAttrs.containsKey(group)) {
        selectedAttrs[group] = [];
      }

      // When not in selection yet.
      if (!selectedAttrs[group]!.contains(attributeId)) {
        selectedAttrs[group]!.add(attributeId);
      }
    }
    // Attribute is deselected.
    else {
      // When group does not exist yet.
      if (!selectedAttrs.containsKey(group)) {
        return;
      }

      // Remove the attribute id from the group.
      selectedAttrs[group]!.remove(attributeId);

      // When it is the last attribute from the group, deselect the whole group.
      if (selectedAttrs[group]!.isEmpty) {
        selectedAttrs.removeWhere((key, value) => key == group);
      }
    }
  }

  /// Gets attributes inside the [provider] and [group].
  void initDataByProviderAndGroup(String provider, String group) async {
    try {
      attributes = await _myDataController.getByProviderAndGroup(
        provider,
        group,
      );

      if (!_isDisposed) {
        notifyListeners();
      }
    } catch (e) {
      _logger.e(e);
    }
  }

  /// For the summary, get a list of all selected groups.
  void initSelectedGroups(Map<String, List<int>> selectedAttrs) async {
    var dataList = await _myDataController.getRecentData();

    for (var item in dataList) {
      if (!selectedAttrs.containsKey(item.category)) {
        continue;
      }

      if (!dataMap.containsKey(item.category)) {
        dataMap[item.category] = [];
      }

      dataMap[item.category]!.add(item);
    }

    if (!_isDisposed) {
      notifyListeners();
    }
  }

  /// Gets a list of all already shared attributes for [connectionId].
  Future<List<int>> getAlreadySharedAttributeIds(int connectionId) async {
    List<int> ids = [];

    try {
      var data = await _contractController.getSharedDataFromVault(connectionId);

      for (var item in data) {
        ids.add(item.attributeId!);
      }
    } catch (e) {
      _logger.e(e);
    }

    return ids;
  }

  /// When returning the list of attributes to share, we need all the attribute ids.
  /// This method converts the [selectedAttrs] into a flat list of id's.
  Future<List<int>> getSelectedAttributeIds(Map<String, List<int>> selectedAttrs) async {
    List<int> returnList = [];

    var data = await _myDataController.getData();

    // Go through all the data.
    for (var attr in data) {
      // When the category exists.
      if (selectedAttrs.containsKey(attr.category)) {
        // Find out if the category is shared as a whole.
        if (selectedAttrs[attr.category]!.isEmpty) {
          returnList.add(attr.attributeId!);
        }
        // Or if specific id is mentioned in the attributes to share.
        else if (selectedAttrs[attr.category]!.contains(attr.attributeId)) {
          returnList.add(attr.attributeId!);
        }
      }
    }

    return returnList;
  }

  /// Initiates my data overview screen. If [selectedAttrs] is provided,
  /// only attributes matching to [selectedAttrs] are returned.
  void initRecentOverviewScreen() async {
    List<MyDataViewModel> dataList = [];

    try {
      dataList = await _myDataController.getRecentData();
    } catch (e) {
      _logger.e(e);
    }

    if (dataList.isNotEmpty) {
      _generateDataMap(dataList);
    } else {
      dataMap = {};
    }

    isLoading = false;

    if (!_isDisposed) {
      notifyListeners();
    }
  }

  /// Generates data map from [DataViewModel] list.
  void _generateDataMap(List<MyDataViewModel> dataList) {
    dataMap = {};

    for (MyDataViewModel dataModel in dataList) {
      List<MyDataViewModel>? list = [];

      if (dataMap.containsKey(dataModel.timeSpan)) {
        list = dataMap[dataModel.timeSpan];
      }

      list!.add(dataModel);
      dataMap.putIfAbsent(dataModel.timeSpan, () => list);
    }
  }
}
