import 'dart:io';

import 'package:flutter/cupertino.dart';
import 'package:flutter/foundation.dart';
import 'package:logger/logger.dart';
import 'package:schluss_beta_app/controller/account_controller.dart';
import 'package:schluss_beta_app/singleton_injector.dart';

enum ProcessingState { creating, success, error }

class DataExportProvider extends ChangeNotifier {
  final AccountController _accountController = AccountController();

  final Logger _logger = singletonInjector<Logger>();

  ProcessingState _processingStatus = ProcessingState.creating;
  ProcessingState get getProcessingStatus => _processingStatus;

  String _zipfileName = '';
  int progressValue = 0;

  String get getZipFileName => _zipfileName;

  bool _isDisposed = false;

  @override
  void dispose() {
    _isDisposed = true;
    super.dispose();
  }

  /// Initiates pending screen : data backup process.
  void initPendingScreen(
    String? password,
  ) async {
    _processingStatus = ProcessingState.creating;

    // Little delay added to show pending animation longer.
    // If needed to faster the flow can be removed delay or update the value.
    Future.delayed(
        const Duration(milliseconds: 2000),
        () => createBackup(
              password,
            ));

    if (!_isDisposed) notifyListeners();
  }

  /// Screens update function while creating backup.
  void createBackup(String? password) async {
    try {
      String fileName = await _accountController.exportVaultData(password);
      _zipfileName = fileName.split(Platform.pathSeparator).last;
      _processingStatus = ProcessingState.success;
      progressValue = 100;
    } catch (e) {
      _processingStatus = ProcessingState.error;
      _logger.e(e);
    }
    if (!_isDisposed) notifyListeners();
  }
}
