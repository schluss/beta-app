import 'package:flutter/cupertino.dart';
import 'package:flutter/foundation.dart';
import 'package:logger/logger.dart';
import 'package:schluss_beta_app/controller/config_controller.dart';
import 'package:schluss_beta_app/controller/plugin_controller.dart';
import 'package:schluss_beta_app/singleton_injector.dart';
import 'package:schluss_beta_app/view_model/provider_config_view_model.dart';
import 'package:schluss_beta_app/view_model/request_view_model.dart';

enum RequestState { loading, success, error }

class DataRequestProvider extends ChangeNotifier {
  ConfigurationController configController = ConfigurationController();
  final PluginController _pluginController = PluginController();

  final Logger _logger = singletonInjector<Logger>();

  RequestState _requestStatus = RequestState.loading;
  RequestViewModel? _requestModel;
  final List<ProviderConfigViewModel> _dataProviderList = [];
  String? _dynamicProviderIcon = '';
  String? _dynamicProviderName = '';
  String? _dynamicProviderTitle = '';

  List<ProviderConfigViewModel> get getDataProviderList => _dataProviderList;
  RequestState get getRequestStatus => _requestStatus;
  String? get getProviderLogo => _dynamicProviderIcon;
  String? get getProviderName => _dynamicProviderName;
  String? get getProviderTitle => _dynamicProviderTitle;
  RequestViewModel? get getRequestViewModel => _requestModel;

  bool _isDisposed = false;

  @override
  void dispose() {
    _isDisposed = true;
    super.dispose();
  }

  /// Initiates pending screen, data processing in plugin side.
  void initPendingScreen(
    RequestViewModel requestModel,
    String? pluginData,
  ) async {
    try {
      _requestStatus = RequestState.loading;
      int index = requestModel.getCurrentSelectedIndex()!;
      ProviderConfigViewModel providerModel = requestModel.getProviderList().elementAt(index);
      _dynamicProviderIcon = providerModel.dataProvider!.logoUrl;
      _dynamicProviderName = providerModel.dataProvider!.name;
      _dynamicProviderTitle = providerModel.request;

      // Little delay added to show pending animation longer.
      // If needed to faster the flow can be removed delay or update the value.
      Future.delayed(
        const Duration(milliseconds: 2000),
        () => loadExtractedData(
          requestModel,
          pluginData,
        ),
      );
    } catch (e) {
      _requestStatus = RequestState.error;
      _logger.e(e.toString());
    }
    if (!_isDisposed) notifyListeners();
  }

  /// Screens update function after the data extraction from plugin.
  void loadExtractedData(RequestViewModel requestModel, String? pluginData) async {
    try {
      _requestModel = await _pluginController.getExtractedContent(
        requestModel,
        pluginData,
      );
      _requestStatus = RequestState.success;
    } catch (e) {
      _requestStatus = RequestState.error;
      _logger.e(e);
    }
    if (!_isDisposed) notifyListeners();
  }
}
