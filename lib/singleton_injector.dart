import 'package:get_it/get_it.dart';
import 'package:logger/logger.dart';
import 'package:schluss_beta_app/business/impl/account_bl_impl.dart';
import 'package:schluss_beta_app/business/impl/belasting_plugin_manager_bl_impl.dart';
import 'package:schluss_beta_app/business/impl/bkr_plugin_manager_bl_impl.dart';
import 'package:schluss_beta_app/business/impl/contract_manager_bl_impl.dart';
import 'package:schluss_beta_app/business/impl/data_share_bl_impl.dart';
import 'package:schluss_beta_app/business/impl/default_payload_plugin_bl_impl.dart';
import 'package:schluss_beta_app/business/impl/external_storage_bl_impl.dart';
import 'package:schluss_beta_app/business/impl/findesk_payload_plugin_bl_impl.dart';
import 'package:schluss_beta_app/business/impl/gateway_bl_impl.dart';
import 'package:schluss_beta_app/business/impl/insurer_plugin_manager_bl_impl.dart';
import 'package:schluss_beta_app/business/impl/me_plugin_manager_bl_impl.dart';
import 'package:schluss_beta_app/business/impl/mijn_overheid_plugin_manager_bl_impl.dart';
import 'package:schluss_beta_app/business/impl/my_data_bl_impl.dart';
import 'package:schluss_beta_app/business/impl/notary_plugin_manager_bl_impl.dart';
import 'package:schluss_beta_app/business/impl/notification_bl_impl.dart';
import 'package:schluss_beta_app/business/impl/qr_config_bl_impl.dart';
import 'package:schluss_beta_app/business/impl/request_manager_bl_impl.dart';
import 'package:schluss_beta_app/business/impl/uwv_plugin_manager_bl_impl.dart';
import 'package:schluss_beta_app/business/impl/verifiedmail_plugin_manager_bl_impl.dart';
import 'package:schluss_beta_app/business/interface/payload_plugin_manager_bl.dart';
import 'package:schluss_beta_app/business/interface/plugin_manager_bl.dart';
import 'package:schluss_beta_app/data/impl/attribute_store_dao_impl.dart';
import 'package:schluss_beta_app/data/impl/common_dao_impl.dart';
import 'package:schluss_beta_app/data/impl/contract_store_dao_impl.dart';
import 'package:schluss_beta_app/data/interface/common_dao.dart';
import 'package:schluss_beta_app/database_config.dart';
import 'package:schluss_beta_app/providers/impl/contract_data_provider.dart';
import 'package:schluss_beta_app/providers/impl/data_export_provider.dart';
import 'package:schluss_beta_app/providers/impl/data_request_provider.dart';
import 'package:schluss_beta_app/providers/impl/home_provider.dart';
import 'package:schluss_beta_app/providers/impl/my_data_provider.dart';
import 'package:schluss_beta_app/providers/plugin_provider.dart';
import 'package:schluss_beta_app/services/impl/common_service_impl.dart';
import 'package:schluss_beta_app/services/impl/config_service_impl.dart';
import 'package:schluss_beta_app/services/impl/gateway_service_impl.dart';
import 'package:schluss_beta_app/services/impl/socket_service_impl.dart';
import 'package:schluss_beta_app/services/impl/task_service.dart';
import 'package:schluss_beta_app/services/interface/common_service.dart';
import 'package:schluss_beta_app/services/interface/config_service.dart';
import 'package:schluss_beta_app/services/interface/gateway_service.dart';
import 'package:schluss_beta_app/services/interface/socket_service.dart';
import 'package:schluss_beta_app/util/log_output_util.dart';
import 'package:uuid/uuid.dart';

GetIt singletonInjector = GetIt.instance;

/// Clears the memory.
///
/// Clear all singletons, making sure no accidentally in memory stuff is still
/// there and re-register and initialize the singletons.
Future<void> resetSingletonInjector() async {
  await singletonInjector.reset();

  setupSingletonInjector();
}

/// Registers and initializes (where needed) all the singletons.
void setupSingletonInjector() {
  // Logger.
  singletonInjector.registerLazySingleton<Logger>(() => Logger(
        printer: PrettyPrinter(
          methodCount: 2,
          errorMethodCount: 8,
          lineLength: 0,
          colors: true,
          printEmojis: false,
          printTime: true,
          noBoxingByDefault: true,
        ),
        output: LogOutputUtil(),
      ));

  // Business Logic.
  singletonInjector.registerLazySingleton<QRConfigurationBLImpl>(() => QRConfigurationBLImpl());
  singletonInjector.registerLazySingleton<RequestManagerBLImpl>(() => RequestManagerBLImpl());
  singletonInjector.registerLazySingleton<MyDataBLImpl>(() => MyDataBLImpl());
  singletonInjector.registerLazySingleton<ContractManagerBLImpl>(() => ContractManagerBLImpl());
  singletonInjector.registerLazySingleton<AccountBLImpl>(() => AccountBLImpl());
  singletonInjector.registerLazySingleton<ExternalStorageBLImpl>(() => ExternalStorageBLImpl());
  singletonInjector.registerLazySingleton<GatewayBLImpl>(() => GatewayBLImpl());
  singletonInjector.registerLazySingleton<NotificationBLImpl>(() => NotificationBLImpl());
  singletonInjector.registerLazySingleton<DataShareBLImpl>(() => DataShareBLImpl());

  // Data providing plugins.
  singletonInjector.registerLazySingleton<PluginManagerBL>(() => BelastingPluginManagerBLImpl(), instanceName: 'plugin-belastingdienst');
  singletonInjector.registerLazySingleton<PluginManagerBL>(() => UWVPluginManagerBLImpl(), instanceName: 'plugin-uwv');
  singletonInjector.registerLazySingleton<PluginManagerBL>(() => BKRPluginManagerBLImpl(), instanceName: 'plugin-bkr');
  singletonInjector.registerLazySingleton<PluginManagerBL>(() => MijnOverheidPluginManagerBLImpl(), instanceName: 'plugin-mijnoverheid');
  singletonInjector.registerLazySingleton<PluginManagerBL>(() => InsurerPluginManagerBLImpl(), instanceName: 'plugin-insurer');
  singletonInjector.registerLazySingleton<PluginManagerBL>(() => NotaryPluginManagerBLImpl(), instanceName: 'plugin-notary');

  // Local data addition plugin.
  singletonInjector.registerLazySingleton<PluginManagerBL>(() => MePluginManagerBLImpl(), instanceName: 'plugin-me');
  singletonInjector.registerLazySingleton<PluginManagerBL>(() => VerifiedMailPluginManagerBLImpl(), instanceName: 'plugin-verifiedmail');

  // Output processing plugins.
  singletonInjector.registerLazySingleton<PayloadPluginManagerBl>(() => DefaultPayloadPluginBlImpl(), instanceName: 'plugin-default');
  singletonInjector.registerLazySingleton<PayloadPluginManagerBl>(() => FindeskPayloadPluginBlImpl(), instanceName: 'plugin-findesk');

  // Services.
  singletonInjector.registerLazySingleton<ConfigurationService>(() => ConfigurationServiceImpl());
  singletonInjector.registerLazySingleton<CommonService>(() => CommonServiceImpl());
  singletonInjector.registerLazySingleton<GatewayService>(() => GatewayServiceImpl());
  singletonInjector.registerLazySingleton<SocketService>(() => SocketServiceImpl());
  singletonInjector.registerLazySingleton<TaskService>(() => TaskService());

  // DAO.
  singletonInjector.registerLazySingleton<AppDatabaseConfig>(() => AppDatabaseConfig());
  singletonInjector.registerLazySingleton<CommonDAO>(() => CommonDAOImpl());
  singletonInjector.registerLazySingleton<ContractStoreDAOImpl>(() => ContractStoreDAOImpl());
  singletonInjector.registerLazySingleton<AttributeStoreDAOImpl>(() => AttributeStoreDAOImpl());

  // Providers.
  singletonInjector.registerFactory<DataRequestProvider>(
    () => DataRequestProvider(),
  );
  singletonInjector.registerFactory<MyDataProvider>(() => MyDataProvider());
  singletonInjector.registerFactory<ContractDataProvider>(
    () => ContractDataProvider(),
  );
  singletonInjector.registerFactory<PluginProvider>(() => PluginProvider());
  singletonInjector.registerFactory<HomeProvider>(() => HomeProvider());
  singletonInjector.registerFactory<DataExportProvider>(() => DataExportProvider());

  // UUID.
  singletonInjector.registerFactory<Uuid>(() => const Uuid());
}
