import 'package:flutter/material.dart';
import 'package:schluss_beta_app/business/impl/plugin_manager_bl_impl.dart';
import 'package:schluss_beta_app/business/interface/plugin_manager_bl.dart';
import 'package:schluss_beta_app/plugins/insurer/insurer.dart';
import 'package:schluss_beta_app/view_model/provider_config_view_model.dart';
import 'package:schluss_beta_app/view_model/request_view_model.dart';

/// Insurer plugin business logic.
class InsurerPluginManagerBLImpl extends PluginManagerBLImpl implements PluginManagerBL {
  final PluginInsurer _pluginInsurer = PluginInsurer();
  @override
  void onExecutePlugin(
    BuildContext context,
    ProviderConfigViewModel? providerModel,
    Function callback,
  ) {
    _pluginInsurer.runPlugin(
      context,
      callback,
      providerModel!.pluginSettings.siteUrl!,
      providerModel.pluginSettings.trigger!,
      providerModel.dataProvider!.name!,
    );
  }

  @override
  Future<Map<String, dynamic>> onGetData(String attributeDataList) {
    return _pluginInsurer.getExtractedData(attributeDataList);
  }

  @override
  Stream<int> getProgressValue(RequestViewModel requestViewModel) {
    return _pluginInsurer.getProgress();
  }

  @override
  void executePluginDirect(
    context,
    Function callBack,
    String siteUrl,
    String trigger, {
    String? pluginName = 'Verzekeraar',
    Function(String userName, String password)? storeCredentialCallback,
    String? userName,
    String? password,
  }) {
    throw 'Not implemented';
  }
}
