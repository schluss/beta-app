/*
 * @author Manuja Jayawardana
 * @email mjayawardana@yukon.lk
 * @create date 2021-03-23
 */

import 'package:flutter_dotenv/flutter_dotenv.dart';
import 'package:logger/logger.dart';
import 'package:schluss_beta_app/services/interface/gateway_service.dart';
import 'package:schluss_beta_app/singleton_injector.dart';
import 'package:schluss_beta_app/util/common_util.dart';
import 'package:schluss_beta_app/util/crypto_util.dart';
import 'package:schluss_beta_app/view_model/gateway_viewmodel.dart';

class ExternalStorageBLImpl {
  final Logger _logger = singletonInjector<Logger>();
  final GatewayService _gatewayService = singletonInjector<GatewayService>();

  /// Validates and returns gateway connection if already available.
  /// If not available create the connection and returns.

  Future<bool> validateGatewayConnection() async {
    try {
      // Check gateway secret data available on keystore.
      bool isSecretDataAvailable = await validateSecretDataAvailability();

      if (!isSecretDataAvailable) {
        GatewayViewModel gatewayViewModel = await registerNewAccount();
        if (gatewayViewModel.secret == null || gatewayViewModel.key == null) {
          return false;
        }
      }
    } catch (e) {
      _logger.e(e);
      return false;
    }
    return true;
  }

  /// Registers a new account in external storage gateway.
  Future<GatewayViewModel> registerNewAccount() async {
    var gatewayUrl = dotenv.env['EGW_URL']!;
    var endpoint = dotenv.env['EGW_REGISTER_ENDPOINT']!;

    try {
      GatewayViewModel gatewayViewModel = await _gatewayService.registerNewAccount(CommonUtil.buildUrl(
        gatewayUrl,
        path: endpoint,
      ));

      _logger.d('Registering User Completed $gatewayViewModel');

      // Store secret data in the secure keystore.
      await CryptoUtil.storeExternalStorageGatewaySecretData(
        gatewayViewModel.secretData,
      );

      // Get auth token.
      await storeAuthToken(gatewayViewModel);

      return gatewayViewModel;
    } catch (e) {
      _logger.e(e);
      rethrow;
    }
  }

  /// Gets the external storage credentials (key, secret).

  Future<String?> getToken() async {
    return await CryptoUtil.getExternalStorageGatewayToken();
  }

  Future<Map<String, String>?> postAttributeValue(String attributeValue) async {
    var gatewayUrl = dotenv.env['EGW_URL']!;
    var token = await CryptoUtil.getExternalStorageGatewayToken();

    var response = await _gatewayService.executePostRequest(
        CommonUtil.buildUrl(
          gatewayUrl,
          path: '/objects',
        ),
        attributeValue,
        headers: {'Authorization': token});

    if (response == null || !response.containsKey('objectId')) {
      return null;
    }

    return {
      'objectId': response['objectId'],
      'path': '/objects/${response['objectId']}',
      'storageProvider': 'schluss',
    };
  }

  Future<bool> deleteAttributeValue(String objectLocation) async {
    var gatewayUrl = dotenv.env['EGW_URL']!;
    var token = await CryptoUtil.getExternalStorageGatewayToken();

    var response = await _gatewayService.executeDeleteRequest(
        CommonUtil.buildUrl(
          gatewayUrl,
          path: objectLocation,
        ),
        {'Authorization': token});

    if (response == null || !response.containsKey('status')) {
      return false;
    }

    return true;
  }

  /// Executes background process for retrieve auth token from external storage gateway.
  Future<void> storeAuthToken(GatewayViewModel externalStorageViewModel) async {
    var gatewayUrl = dotenv.env['EGW_URL']!;
    var endpoint = dotenv.env['EGW_AUTH_ENDPOINT']!;

    String payload = externalStorageViewModel.secretData;

    // Register the user auth token task.
    GatewayViewModel gatewayViewModel = await _gatewayService.getAuthToken(
      CommonUtil.buildUrl(gatewayUrl, path: endpoint),
      payload,
    );

    _logger.i('Retrieving User Token Completed');

    // Store token in the secure keystore.
    await CryptoUtil.storeExternalStorageGatewayToken(gatewayViewModel.token!);

    return;
  }

  /// Validates the external storage gateway secret data available status.

  Future<bool> validateSecretDataAvailability() async {
    bool isAvailable = false;

    // Retrieve secret data from secure keystore.
    Map? secretData = await CryptoUtil.getExternalStorageGatewaySecretData();

    // Check availability of any external storage gateway [secretData] in the secure store.
    if (secretData != null) {
      isAvailable = true;
    }

    return Future.value(isAvailable);
  }
}
