import 'package:flutter/cupertino.dart';
import 'package:schluss_beta_app/business/impl/plugin_manager_bl_impl.dart';
import 'package:schluss_beta_app/business/interface/plugin_manager_bl.dart';
import 'package:schluss_beta_app/plugins/uwv/uwv.dart';
import 'package:schluss_beta_app/view_model/provider_config_view_model.dart';
import 'package:schluss_beta_app/view_model/request_view_model.dart';

/// UWV plugins business logic.
class UWVPluginManagerBLImpl extends PluginManagerBLImpl implements PluginManagerBL {
  final PluginUWV _pluginUWV = PluginUWV();
  @override
  void onExecutePlugin(
    BuildContext context,
    ProviderConfigViewModel? providerModel,
    Function callback,
  ) {
    _pluginUWV.runPlugin(
      context,
      callback,
      providerModel!.pluginSettings.siteUrl!,
      providerModel.pluginSettings.trigger!,
      providerModel.dataProvider!.name!,
    );
  }

  @override
  Future<Map<String, dynamic>> onGetData(String attributeDataList) {
    return _pluginUWV.getExtractedData(attributeDataList);
  }

  @override
  Stream<int> getProgressValue(RequestViewModel requestViewModel) {
    return _pluginUWV.getProgress();
  }

  /// Executes the plugin directly.
  // TODO: need to change the naming and remove [context].
  @override
  void executePluginDirect(
    context,
    Function callBack,
    String siteUrl,
    String trigger, {
    String? pluginName = 'UWV',
    Function(String userName, String password)? storeCredentialCallback,
    String? userName,
    String? password,
  }) {
    _pluginUWV.runPlugin(
      context,
      callBack,
      siteUrl,
      trigger,
      pluginName!,
    );
  }
}
