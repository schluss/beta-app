import 'dart:convert';
import 'dart:core';
import 'dart:io';

import 'package:logger/logger.dart';
import 'package:schluss_beta_app/business/impl/gateway_bl_impl.dart';
import 'package:schluss_beta_app/data/impl/attribute_store_dao_impl.dart';
import 'package:schluss_beta_app/data/interface/common_dao.dart';
import 'package:schluss_beta_app/data/models/attribute_model.dart';
import 'package:schluss_beta_app/data/models/attribute_version_model.dart';
import 'package:schluss_beta_app/data/models/contract/contract_model.dart';
import 'package:schluss_beta_app/data/models/contract/contract_provider.dart';
import 'package:schluss_beta_app/data/models/incoming_share_model.dart';
import 'package:schluss_beta_app/data/models/request_data_model.dart';
import 'package:schluss_beta_app/singleton_injector.dart';
import 'package:schluss_beta_app/util/common_util.dart';
import 'package:schluss_beta_app/util/crypto_util.dart';
import 'package:schluss_beta_app/util/vc_util.dart';
import 'package:schluss_beta_app/view_model/contract_recent_item.dart';
import 'package:schluss_beta_app/view_model/field_view_model.dart';
import 'package:schluss_beta_app/view_model/my_data_view_model.dart';
import 'package:schluss_beta_app/view_model/provider_data_model.dart';
import 'package:schluss_beta_app/view_model/request_view_model.dart';
import 'package:session_next/session_next.dart';

class MyDataBLImpl {
  final CommonDAO _commonDAOImpl = singletonInjector<CommonDAO>();
  final AttributeStoreDAOImpl _attributeStoreDAOImpl = singletonInjector<AttributeStoreDAOImpl>();

  final Logger _logger = singletonInjector<Logger>();

  /// Gets data ( personal data and incoming data)
  Future<List<MyDataViewModel>> getStoredData() async {
    var connectionId = SessionNext().get<int>('connectionId') ?? 0;

    if (connectionId == 0) {
      return await getOwnerData();
    } else {
      return await getIncomingContractData(connectionId);
    }
  }

  /// Gets data for vault owner.
  Future<List<MyDataViewModel>> getOwnerData() async {
    List<MyDataViewModel> dataList = [];
    List<Map<String, dynamic>> attributeList;
    Map<String, bool> sortMap = {'category': true};

    // Sort descending from field [timestamp].
    attributeList = await _commonDAOImpl.getSortedData(
      AttributeModel.getStoreName(),
      sortMap,
    );

    // When no attributes found, return empty list.
    if (attributeList.isEmpty) {
      return dataList;
    }

    Map<String, dynamic> myDataMap = {};

    for (Map<String, dynamic> attr in attributeList) {
      // Cast attr to attribute model.
      AttributeModel attributeModel = AttributeModel.fromJson(attr);

      // Get the latest attribute version for this attribute.
      AttributeVersionModel? attributeVersionModel = await AttributeStoreDAOImpl().findLatestAttributeVersion(
        attributeModel.id!,
      );

      // Setting necessary values to [FieldViewModel].
      Map<String, dynamic> dataDetailMap = {
        'name': attributeModel.name,
        'label': attributeModel.label,
        'type': attributeModel.type,
        'category': attributeModel.category,
        'group': attributeModel.group,
        'retrievedTime': DateTime.fromMillisecondsSinceEpoch(attributeModel.timestamp!),
        'attributeId': attributeModel.id,
        'attributeVersionId': attributeVersionModel!.id,
        'displayValue': attributeModel.displayValue,
        'attrKeyEnc': attributeModel.attrKeyEnc,
      };

      FieldViewModel dataDetailModel = FieldViewModel();
      dataDetailModel.fromDatabaseData(dataDetailMap);

      Map<String, dynamic> dataMap = await getDataMapGroupByProvider(
        myDataMap,
        attributeModel,
        dataDetailModel,
      );
      myDataMap.putIfAbsent(
        '${attributeModel.providerName!}_${attributeModel.category!}',
        () => dataMap,
      );
    }

    for (Map<String, dynamic> dataViewMap in myDataMap.values) {
      MyDataViewModel myDataModel = MyDataViewModel.fromJson(dataViewMap);
      dataList.add(myDataModel);
    }

    return Future.value(dataList);
  }

  /// Gets steward data using the connection id.
  Future<List<MyDataViewModel>> getIncomingContractData(int contractId) async {
    List<MyDataViewModel> dataList = [];

    List<Map<String, dynamic>> incomingShareList = [];
    Map<String, bool> sortMap = {'category': true};
    Map<String, int> contractFilterMap = {'contractId': contractId};

    // Sort descending from field [timestamp].
    incomingShareList = await _commonDAOImpl.getFilteredData(
      IncomingShareModel.getStoreName(),
      contractFilterMap,
      FilterType.or,
      sortMap: sortMap,
    );

    // If incoming shares available.
    if (incomingShareList.isNotEmpty) {
      Map<String, dynamic> myDataMap = {};

      for (Map<String, dynamic> shareMap in incomingShareList) {
        // Attribute version.
        IncomingShareModel shareModel = IncomingShareModel.fromJson(
          shareMap,
        );

        DateTime attributeDate = DateTime.fromMillisecondsSinceEpoch(
          shareModel.timestamp!,
        );

        // Setting necessary values to [FieldViewModel].
        Map<String, dynamic> dataDetailMap = {
          'name': shareModel.name,
          'category': shareModel.category,
          'providerName': shareModel.providerName,
          'logourl': shareModel.logoUrl,
          'timeSpan': attributeDate,
          'type': '',
          'label': shareModel.name,
          'mediaType': shareModel.mediaType,
          'retrievedTime': attributeDate,
          'attributeId': shareModel.id,
          'displayValue': shareModel.displayValue,
          'isVC': shareModel.isVC,
          'isIncomingShare': true,
        };

        FieldViewModel dataDetailModel = FieldViewModel();
        dataDetailModel.fromIncomingShare(shareMap);

        // TODO: create generic method [getTimeSpanMap] function. Temporary assign
        //  values to [AttributeModel] class.
        AttributeModel attributeModel = AttributeModel.fromJson(dataDetailMap);

        Map<String, dynamic> dataMap = await getDataMapGroupByProvider(
          myDataMap,
          attributeModel,
          dataDetailModel,
        );
        myDataMap.putIfAbsent(
          '${attributeModel.providerName!}_${attributeModel.category!}',
          () => dataMap,
        );
      }

      // Generate list of group data.
      for (Map<String, dynamic> dataViewMap in myDataMap.values) {
        MyDataViewModel myDataModel = MyDataViewModel.fromJson(dataViewMap);
        dataList.add(myDataModel);
      }
    }

    return Future.value(dataList);
  }

  /// Gets attributes by [provider] and [group].
  Future<List<FieldViewModel>> getByProviderAndGroup(
    String provider,
    String group,
  ) async {
    List<FieldViewModel> dataList = [];
    List<Map<String, dynamic>> attributeList;

    Map<String, dynamic> filterMap = {
      'category': group,
      'providerName': provider,
    };

    attributeList = await _commonDAOImpl.getFilteredData(
      AttributeModel.getStoreName(),
      filterMap,
      FilterType.and,
    );

    for (Map<String, dynamic> attr in attributeList) {
      // Cast attr to attribute model.
      AttributeModel attributeModel = AttributeModel.fromJson(attr);

      // Get the latest attribute version for this attribute.
      AttributeVersionModel? versionModel = await AttributeStoreDAOImpl().findLatestAttributeVersion(
        attributeModel.id!,
      );

      // Setting necessary values to [FieldViewModel].
      Map<String, dynamic> dataDetailMap = {
        'name': attributeModel.name,
        'label': attributeModel.label,
        'type': attributeModel.type,
        'category': attributeModel.category,
        'group': attributeModel.group,
        'retrievedTime': DateTime.fromMillisecondsSinceEpoch(attributeModel.timestamp!),
        'attributeId': attributeModel.id,
        'attributeVersionId': versionModel!.id,
        'displayValue': attributeModel.displayValue,
        'attrKeyEnc': attributeModel.attrKeyEnc,
      };

      FieldViewModel viewModel = FieldViewModel();
      viewModel.fromDatabaseData(dataDetailMap);

      dataList.add(viewModel);
    }

    return Future.value(dataList);
  }

  /// Gets all attributes.
  Future<List<FieldViewModel>> getData() async {
    List<FieldViewModel> attributes = [];

    var dataset = await _commonDAOImpl.getAll(AttributeModel.getStoreName());

    for (var attr in dataset) {
      var model = FieldViewModel(
        attributeId: attr['id'],
        name: attr['name'],
        label: attr['label'],
        category: attr['category'],
        group: attr['group'],
        providerName: attr['providerName'],
        logoUrl: attr['logoUrl'],
      );

      attributes.add(model);
    }

    return attributes;
  }

  /// Uploads files from [MyData] screen.
  Future<bool> uploadData(String filePath) async {
    File file = await getFileFromPath(filePath);

    var filename = file.path.split('/').last;
    var mediatype = CommonUtil.getMediaTypeFromFile(file);
    var fileStr = await file.readAsString();

    var attributeModel = AttributeModel(
      name: filename,
      label: filename,
      displayValue: filename,
      category: 'general',
      group: 'files',
      providerName: 'Zelf',
      type: 'file',
      timestamp: DateTime.now().millisecondsSinceEpoch,
      mediaType: mediatype,
    );

    await AttributeStoreDAOImpl().storeAttributeFromModel(
      attributeModel,
      fileStr,
    );

    return Future.value(true);
  }

  /// Groups data by provider name and category.
  Future<Map<String, dynamic>> getDataMapGroupByProvider(
    Map<String, dynamic> myDataMap,
    AttributeModel attributeModel,
    FieldViewModel fieldViewModel,
  ) async {
    Map<String, dynamic>? providerMap = {};
    List<FieldViewModel>? dataDetailList = [];

    // Check time span group already in the map. Should not be local file uploaded.
    // if (!attributeModel.isLocal! &&
    // myDataMap.containsKey(attributeModel.providerName!
    // + '_' + attributeModel.category!)) {

    if (myDataMap.containsKey(
      '${attributeModel.providerName!}_${attributeModel.category!}',
    )) {
      providerMap = myDataMap['${attributeModel.providerName!}_${attributeModel.category!}'];
      dataDetailList = providerMap!['dataList'];
    } else {
      providerMap = {
        'category': attributeModel.category,
        'providerName': attributeModel.providerName,
        'logoUrl': attributeModel.logoUrl,
        'type': attributeModel.type,
        'mediaType': attributeModel.mediaType,
        //'isDocument': attributeModel.type == 'file' ? true : false,
      };
    }

    dataDetailList!.add(fieldViewModel);
    providerMap.putIfAbsent('dataList', () => dataDetailList);

    return Future.value(providerMap);
  }

  /// Decrypts attribute data.
  ///
  /// Decrypts data if only encrypted and generates file with decoding data from
  /// attribute data. For local files value will be [filePath] and needs to decrypt
  /// the file content in [filepath] to generate the file.
  Future<String> generateFile(FieldViewModel data) async {
    File file;
    String? dataValue;

    // Validates if the Text is already decrypted.
    if (data.isDecrypted) {
      dataValue = data.value;
    } else {
      data = await getDataDetailsDecrypted(data);

      dataValue = await CryptoUtil.extractAttributeValue(
        data.value!,
        data.attrKeyEnc!,
      );

      // Validate if the decrypted value is a file path for encrypted data file
      // [encryptedFileExtension] is the default type specified for all encrypted files in Schluss.
      if (dataValue.endsWith('.${CryptoUtil.encryptedFileExtension}')) {
        File file = await getFileFromPath(dataValue);

        // Decode the file data to string.
        List<int> fileBytes = await file.readAsBytes();
        var fileByteData = utf8.decode(fileBytes);

        // Decrypt the file content.
        dataValue = await CryptoUtil.extractAttributeValue(
          fileByteData,
          data.attrKeyEnc!,
        );
      }
    }

    // Validate file name and content available.
    if (data.displayValue!.isNotEmpty && dataValue!.isNotEmpty) {
      // Build the file from the content.
      file = await CommonUtil().buildFileFromBytes(
        dataValue,
        data.displayValue,
      );

      return Future.value(file.path);
    } else {
      _logger.e('File name or bytes empty');

      return '';
    }
  }

  /// Checks data attribute is used in contracts.
  Future<List<ContractItem>> validateDataWithContracts(
    MyDataViewModel? dataViewModel,
  ) async {
    List<Map<String, dynamic>> allContractsList = [];
    List<ContractItem> contractList = [];
    List<String?> contractNameList = [];

    allContractsList = await _commonDAOImpl.getAll(
      ContractModel.getStoreName(),
    );
    _logger.i(allContractsList.toString());

    for (Map contract in allContractsList) {
      ContractModel contractData = ContractModel.fromJson(
        contract as Map<String, dynamic>,
      );

      if (contractData.shares == null) {
        continue;
      }

      for (ContractProvider shares in contractData.shares!) {
        if (shares.attributes == null) {
          continue;
        }

        for (AttributeVersionModel attributeVersion in shares.attributes!) {
          for (FieldViewModel fieldViewModel in dataViewModel!.dataList!) {
            // Check contract attribute [id] with data attribute [id].
            if (fieldViewModel.attributeId == attributeVersion.attributeId) {
              _logger.i(
                ' Attribute exists in Contracts ${attributeVersion.attributeId}',
              );

              // Check contract requester details already added.
              if (!contractNameList.contains(contractData.requester!.name)) {
                ContractItem recentItem = ContractItem(
                  ProviderDataModel(
                    contractData.requester!.name,
                    contractData.description,
                    contractData.requester!.logoUrl,
                    false,
                  ),
                  contractData.description,
                  contractData.id,
                );

                contractList.add(recentItem);
                contractNameList.add(contractData.requester!.name);
              }
            }
          }
        }
      }
    }
    return Future.value(contractList);
  }

  /// Deletes data attributes.
  Future<bool> deleteData(MyDataViewModel? dataViewModel) async {
    List<int?> attributeIdList = [];

    try {
      var attrStore = AttributeStoreDAOImpl();

      // Loop through all attributes and delete by [id].
      for (FieldViewModel fieldViewModel in dataViewModel!.dataList!) {
        await attrStore.deleteAttribute(fieldViewModel.attributeId!);
      }

      // TODO: need to remove this section if not needed?.
      /*
      /// Deletes attribute data.
      for (FieldViewModel fieldViewModel in dataViewModel!.dataList!) {
        if (!attributeIdList.contains(fieldViewModel.attributeId)) {
          attributeIdList.add(fieldViewModel.attributeId);
        }

        Map<String, dynamic> attributeFilterMap = {
          'attributeId': fieldViewModel.attributeId,
        };

        /// Gets attribute versions from filtered attributeId list.
        List<Map<String, dynamic>> attributeVersionList = await _commonDAOImpl!.getFilteredData(
          AttributeVersionModel.getStoreName(),
          attributeFilterMap,
          FilterType.and,
        );

        // Delete all attribute versions and attribute values link to attribute id from tables.
        if (attributeVersionList.isNotEmpty) {
          for (Map<String, dynamic> versionMap in attributeVersionList) {
            AttributeVersionModel attributeVersionModel = AttributeVersionModel.fromJson(
              versionMap,
            );

            var attrStore = AttributeStoreDAOImpl();

            /// Delete the version
            await attrStore.deleteAttributeVersion(attributeVersionModel.id!);

            /// Delete the attribute value
            await attrStore.deleteAttributeValue(attributeVersionModel.localPath!);
          }
        }
      }

      /// Deletes attributes from table.
      for (int? attributeId in attributeIdList) {
        AttributeStoreDAOImpl()
        await _commonDAOImpl!.deleteById(
          AttributeModel.getStoreName(),
          attributeId,
        );
      }
      */

      // Delete attributes in pending request.
      deletePendingRequestAttributes(attributeIdList);
    } catch (e) {
      _logger.e(e);
      return Future.value(false);
    }

    return Future.value(true);
  }

  /// Pending requests.
  ///
  /// Checks with pending request contains already retrieved attributes. If any
  /// attribute contains clear all the retrieved the data for pending request.
  void deletePendingRequestAttributes(List<int?> attributeIdList) async {
    final List<Map<String, dynamic>> pendingRequests = await _commonDAOImpl.getFilteredData(
      RequestDataModel.getStoreName(),
      {'status': 'P'},
      FilterType.and,
    );

    if (pendingRequests.isNotEmpty) {
      for (Map pendingRequest in pendingRequests) {
        RequestViewModel requestViewModel = RequestViewModel.fromJson(
          pendingRequest as Map<String, dynamic>,
        );

        if (requestViewModel.providerIdList != null) {
          for (MapEntry entry in json.decode(requestViewModel.providerIdList!).entries) {
            List list = entry.value;
            var st = list.where((id) => attributeIdList.contains(id));

            if (st.isNotEmpty) {
              // Clear pending request.
              await _commonDAOImpl.update(
                RequestDataModel.getStoreName(),
                requestViewModel.id!,
                {'providers': null},
              );
              break;
            }
          }
        }
      }
    }
  }

  /// Gets decrypted data details.
  Future<FieldViewModel> getDataDetailsDecrypted(FieldViewModel fieldViewModel) async {
    try {
      if (!fieldViewModel.isDecrypted) {
        var connectionId = SessionNext().get<int>('connectionId') ?? 0;

        if (fieldViewModel.isIncomingShare || connectionId != 0) {
          fieldViewModel.value = await _attributeStoreDAOImpl.getAttributeValueFromDirectory(fieldViewModel.localPath!, fieldViewModel.attrKeyEnc!);

          // Flag this attribute to be decrypted.
          fieldViewModel.isDecrypted = true;
        } else {
          // Get the attribute value in decrypted form.
          fieldViewModel.value = await _attributeStoreDAOImpl.getAttributeValue(fieldViewModel.attributeId!);

          // Flag this attribute to be decrypted.
          fieldViewModel.isDecrypted = true;
        }
      }
    } catch (e) {
      _logger.w(e.toString());
    }

    if (VCUtil.validateVC(fieldViewModel.value)) {
      fieldViewModel.displayValue = VCUtil.getValue(fieldViewModel.name!, fieldViewModel.value!);
      fieldViewModel.isVC = true;
    }

    return Future.value(fieldViewModel);
  }

  /// Validates file exists in the path and returns the file if available.
  Future<File> getFileFromPath(String filePath) async {
    File file = File(filePath);

    // Check file exists.
    bool fileStatus = await file.exists();

    if (fileStatus) {
      return file;
    } else {
      _logger.e('file not found');
      throw Exception('File Not Found');
    }
  }

  /// Gets attribute value.
  Future<String> getPersonalInfoValue(String provider, String attributeName) async {
    List<Map<String, dynamic>> attributeList;

    try {
      Map<String, dynamic> filterMap = {
        'providerName': provider,
        'name': attributeName,
      };

      attributeList = await _commonDAOImpl.getFilteredData(
        AttributeModel.getStoreName(),
        filterMap,
        FilterType.and,
      );

      if (attributeList.isNotEmpty) {
        for (Map<String, dynamic> map in attributeList) {
          AttributeModel model = AttributeModel.fromJson(map);

          Map<String, dynamic>? attributeModel;

          attributeModel = await _commonDAOImpl.getDataFromId(
            AttributeModel.getStoreName(),
            model.id!,
          );

          String? attributeValue = await AttributeStoreDAOImpl().getAttributeValue(
            attributeModel!['id'],
            attrKeyEnc: attributeModel['attrKeyEnc'],
          );

          return attributeValue ?? '';
        }
      }
    } catch (e) {
      _logger.e(e);
    }

    return '';
  }

  final GatewayBLImpl _gatewayBL = singletonInjector<GatewayBLImpl>();

  /// Shares a list of [attributeIds] with a connection [contractId].
  Future<bool> shareAttributesWithClient(List<int> attributeIds, int contractId, bool hideValues) async {
    try {
      var shares = await _gatewayBL.postGatewayLinkShares(attributeIds, contractId);
      var multiSendResult = await _gatewayBL.sendGatewayShares(shares, contractId, hideValues);

      return multiSendResult;
    } catch (e) {
      return Future.value(false);
    }
  }
}
