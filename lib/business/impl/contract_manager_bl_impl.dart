import 'dart:convert';
import 'dart:core';

import 'package:intl/intl.dart';
import 'package:logger/logger.dart';
import 'package:schluss_beta_app/business/impl/account_bl_impl.dart';
import 'package:schluss_beta_app/business/impl/gateway_bl_impl.dart';
import 'package:schluss_beta_app/constants/helper_constants.dart';
import 'package:schluss_beta_app/controller/contract_controller.dart';
import 'package:schluss_beta_app/controller/my_data_controller.dart';
import 'package:schluss_beta_app/data/impl/attribute_store_dao_impl.dart';
import 'package:schluss_beta_app/data/impl/share_store_dao_impl.dart';
import 'package:schluss_beta_app/data/interface/common_dao.dart';
import 'package:schluss_beta_app/data/models/attribute_model.dart';
import 'package:schluss_beta_app/data/models/contract/contract_model.dart';
import 'package:schluss_beta_app/data/models/incoming_share_model.dart';
import 'package:schluss_beta_app/data/models/outgoing_share_model.dart';
import 'package:schluss_beta_app/services/interface/socket_service.dart';
import 'package:schluss_beta_app/singleton_injector.dart';
import 'package:schluss_beta_app/util/crypto_util.dart';
import 'package:schluss_beta_app/view_model/contract_detail_view_model.dart';
import 'package:schluss_beta_app/view_model/contract_providers_view_model.dart';
import 'package:schluss_beta_app/view_model/contract_recent_item.dart';
import 'package:schluss_beta_app/view_model/field_view_model.dart';
import 'package:schluss_beta_app/view_model/provider_data_model.dart';
import 'package:schluss_beta_app/view_model/vault_switch_item_view_model.dart';
import 'package:session_next/session_next.dart';

class ContractManagerBLImpl {
  final Logger _logger = singletonInjector<Logger>();
  final CommonDAO _commonDAOImpl = singletonInjector<CommonDAO>();
  final SocketService _socketService = singletonInjector<SocketService>();
  final GatewayBLImpl _gatewayBL = singletonInjector<GatewayBLImpl>();
  final MyDataController _myDataController = MyDataController();

  /// Gets the recent contracts.
  Future<Map<String?, List<ContractItem>>> getContracts() async {
    Map<String?, List<ContractItem>> contractListMap = {};
    List<Map<String, dynamic>> allContractsList = [];

    try {
      allContractsList = await _commonDAOImpl.getAll(ContractModel.getStoreName());
    } catch (e) {
      _logger.e(e);
    }

    if (allContractsList.isNotEmpty) {
      String? key;

      for (Map contract in allContractsList.reversed) {
        List<ContractItem> recentItemList = [];
        ContractModel contractData = ContractModel.fromJson(
          contract as Map<String, dynamic>,
        );

        DateTime contractDate = DateTime.fromMillisecondsSinceEpoch(
          contractData.date!,
        );
        DateTime currentDate = DateTime.now();
        int diff = currentDate.difference(contractDate).inDays;
        String? desc = '';

        if (contract['description'] != null) {
          desc = contract['description'];
        }

        // Get the number of (incoming)shares.
        var sharesCount = 0;

        // When old type of contract, the shares count is displayed.
        if (contractData.type == ConnectionType.organization) {
          sharesCount = contractData.shares != null ? contractData.shares!.length : 0;
        }
        // When user type connection, the outgoing shares count is displayed.
        else {
          // Get a count of the outgoing shares for this contract on the out going shares table.
          sharesCount = await _commonDAOImpl.getRecordCount(
            OutgoingShareModel.getStoreName(),
            filterVal: {
              'contractId': contractData.id,
            },
            filterType: FilterType.and,
          );
        }

        // Build the list of items for the recent tab.
        ContractItem recentItem = ContractItem(
          ProviderDataModel(
            contractData.requester!.name,
            getDateString(contractData.date!),
            contractData.requester!.logoUrl,
            false,
            noOfShares: sharesCount,
            iconColor: contractData.iconColor,
          ),
          desc,
          contractData.id,
          type: contractData.type,
          stewardshipType: contractData.stewardshipType,
          //iconColor: contractData.iconColor,
        );

        recentItemList.add(recentItem);

        // Check date difference and maps/groups.
        if (diff < 1) {
          key = HelperConstant.groupTodayMapKey;
        } else if (diff < 7) {
          key = HelperConstant.groupLastWeekMapKey;
        } else if (diff < 30) {
          key = HelperConstant.groupMonthAgoMapKey;
        }

        // If the key already exits.
        if (contractListMap.containsKey(key)) {
          List<ContractItem> list = contractListMap[key]!;
          list.add(recentItem);
          contractListMap.update(key, (value) => list);
        } else {
          // New category.
          contractListMap.putIfAbsent(key, () => recentItemList);
        }
      }
    }
    return contractListMap;
  }

  /// Gets the all contracts.
  Future<List<ContractItem>> getAllContracts() async {
    List<ContractItem> contractList = [];
    late List<Map<String, dynamic>> allContractsList;

    // Get the notification enabled contracts.
    try {
      allContractsList = await _commonDAOImpl.getFilteredData(
        ContractModel.getStoreName(),
        {'notification': true},
        FilterType.and,
        sortMap: {'date': true},
      );
    } catch (e) {
      _logger.e(e);
    }

    // If contract found.
    if (allContractsList.isNotEmpty) {
      for (Map contract in allContractsList.reversed) {
        String? desc = '';
        ContractModel contractData = ContractModel.fromJson(
          contract as Map<String, dynamic>,
        );

        desc = contractData.description ?? '';

        ContractItem recentItem = ContractItem(
          ProviderDataModel(
            contractData.requester!.name,
            getDateString(contract['date']),
            contractData.requester!.logoUrl,
            false,
            noOfShares: contractData.shares != null ? contractData.shares!.length : 0,
            iconColor: contractData.iconColor,
          ),
          desc,
          contractData.id,
          type: contractData.type,
          stewardshipType: contractData.stewardshipType,
        );

        contractList.add(recentItem);
      }
    }
    return contractList;
  }

  /// Changes the notification status of the finished contracts.
  Future<void> hideContractNotifications() async {
    late List<Map<String, dynamic>> allContractsList;

    try {
      allContractsList = await _commonDAOImpl.getFilteredData(
        ContractModel.getStoreName(),
        {'notification': true},
        FilterType.and,
      );
    } catch (e) {
      _logger.e(e);
    }

    // If update notification false.
    if (allContractsList.isNotEmpty) {
      for (Map contract in allContractsList) {
        await _commonDAOImpl.update(
          ContractModel.getStoreName(),
          contract['id'],
          {'notification': false},
        );
      }
    }
  }

  /// Returns date as a string.
  String getDateString(date) {
    // Format date field with Netherland locale.
    return DateFormat('d MMMM yyyy', 'nl').format(
      DateTime.fromMillisecondsSinceEpoch(date),
    );
  }

  /// Gets the contract details.
  Future<ContractProvidersViewModel?> getContractById(int? contractId) async {
    Map<String, dynamic>? map;
    ContractProvidersViewModel? contractProvidersViewModel;

    try {
      map = await _commonDAOImpl.getDataFromId(ContractModel.getStoreName(), contractId!);

      if (map != null) {
        // Build the contract providers.
        contractProvidersViewModel = ContractProvidersViewModel();
        contractProvidersViewModel.requesterType = map['type'];
        contractProvidersViewModel.requesterName = map['requester']['name'];
        contractProvidersViewModel.logoUrl = map['requester']['logoUrl'];
        contractProvidersViewModel.iconColor = map['iconColor'];
        contractProvidersViewModel.description = map['description'];
        contractProvidersViewModel.requestDate = getDateString(map['date']);
        contractProvidersViewModel.isReleased = map['isReleased'] ?? false;

        // Set the provider info.
        List<ContractDetailViewModel> contractProvidersList = [];
        List<dynamic> shares = map['shares'] ?? [];

        for (var share in shares) {
          // Set the contract details.
          ContractDetailViewModel contractProvider = ContractDetailViewModel();
          contractProvider.providerName = share['provider'];
          contractProvider.providerLogo = share['logoUrl'];
          contractProvider.providerIconColor = share['iconColor'];
          contractProvider.providerDescription = share['request'];
          contractProvider.providerDate = getDateString(map['date']);
          contractProvider.requesterName = map['requester']['name'];

          // When the shared data did come from another vault, load the name of the connection: to be shown in UI on several places
          if (share['connectionId'] != 0) {
            contractProvider.fromName = await getConnectionName(share['connectionId']);
          }

          // Build the category-vise list.
          Map<String?, List<FieldViewModel>>? categoryMap;

          // Get the extract value of attributes.
          try {
            categoryMap = await getValues(share);
          } catch (e) {
            _logger.e(e);
          }

          contractProvider.dataMap = categoryMap;
          contractProvidersList.add(contractProvider);
        }

        contractProvidersViewModel.providerList = contractProvidersList;
        return contractProvidersViewModel;
      } else {
        throw Exception('Contract Not Found');
      }
    } catch (e) {
      _logger.e(e);
    }

    return contractProvidersViewModel;
  }

  /// Returns the full name of the requested connection
  Future<String?> getConnectionName(int connectionId) async {
    var connection = await _commonDAOImpl.getDataFromId(ContractModel.getStoreName(), connectionId);

    if (connection == null) {
      return null;
    }

    var contract = ContractModel.fromJson(connection);

    return contract.requester!.name;
  }

  // TODO: method should be refactored. Not all the values are assigning properly.
  //  Set the values for the contract.
  Future<Map<String?, List<FieldViewModel>>> getValues(share) async {
    final Map<String?, List<FieldViewModel>> dataMap = {};
    List<dynamic> attributes = share['attributes'];

    for (var attribute in attributes) {
      Map<String, dynamic>? attributeModel;
      String? value;

      try {
        // When these are shared attributes from own vault
        if (share['connectionId'] == 0) {
          attributeModel = await _commonDAOImpl.getDataFromId(AttributeModel.getStoreName(), attribute['attributeId']);

          value = await AttributeStoreDAOImpl().getAttributeValue(attributeModel!['id'], attrKeyEnc: attributeModel['attrKeyEnc']);
        }
        // When these are shared attributes from someone else's vault
        else {
          attributeModel = await _commonDAOImpl.getDataFromId(IncomingShareModel.getStoreName(), attribute['id']);

          value = await AttributeStoreDAOImpl().getAttributeValueFromDirectory(attributeModel!['localPath'], attributeModel['attrKeyEnc']);
        }
      } catch (e) {
        _logger.e(e);
      }

      FieldViewModel fieldViewModel = FieldViewModel(
        label: attributeModel!['label'],
        type: attributeModel['type'],
        description: attributeModel['description'],
        displayValue: attributeModel['displayValue'],
        name: attributeModel['name'],
        attrKeyEnc: attributeModel['attrKeyEnc'],
        isVC: attributeModel['isVC'],
        value: value,
        isDecrypted: true,
      );

      if (dataMap.containsKey(attributeModel['category'])) {
        List<FieldViewModel> list = dataMap[attributeModel['category']]!;
        list.add(fieldViewModel);
        dataMap.update(attributeModel['category'], (value) => list);
      } else {
        List<FieldViewModel> attributesList = [];
        attributesList.add(fieldViewModel);
        dataMap.putIfAbsent(attributeModel['category'], () => attributesList);
      }
    }
    return dataMap;
  }

  /// Deletes contract by ID.
  Future<bool> deleteContractById(int? contractId) async {
    _logger.d('Delete contract $contractId');

    try {
      var result = await _commonDAOImpl.deleteById(ContractModel.getStoreName(), contractId);

      // refresh switch connections (can only be done from here, because the crypto keys are needed from the session)

      SessionNext().set<List<VaultSwitchItemViewModel>>('switchConnections', await ContractController().getStewardshipAssignedConnections());

      return result;
    } catch (e) {
      _logger.e(e);

      return false;
    }
  }

  /// Deletes connections by ID.
  Future<dynamic> deleteConnectionById(int? contractId) async {
    _logger.d('Delete Connection $contractId');

    try {
      Map<String, dynamic>? map = await _commonDAOImpl.getDataFromId(
        ContractModel.getStoreName(),
        contractId!,
      );

      String clientKey = map!['clientKey'];

      Map? secretData = await (CryptoUtil.getGatewaySecretData());

      if (secretData == null ||
          !secretData.containsKey('key') ||
          !secretData.containsKey(
            'secret',
          )) {
        return false;
      }

      var message = {
        'type': 'connectRemove',
        'clientKey': secretData['key'],
      };
      bool status = await _socketService.shareMessage(
        clientKey,
        json.encode(message),
      );

      if (status) {
        await deleteOutgoingShareByContact(contractId);
        await deleteIncomingShareByContact(contractId);
        return await deleteContractById(contractId);
      }
      return false;
    } catch (e) {
      _logger.e(e);
    }
  }

  /// Deletes connections by client key.
  Future<void> deleteConnectionByClientKey(clientKey) async {
    _logger.d('Delete Connection for client  $clientKey');

    try {
      Map<String, dynamic> filterMap = {
        'clientKey': clientKey!,
      };

      List<Map<String, dynamic>> dataList = await _commonDAOImpl.getFilteredData(
        ContractModel.getStoreName(),
        filterMap,
        FilterType.or,
      );

      if (dataList.isEmpty) {
        return;
      }

      var connection = ContractModel.fromJson(dataList.first);

      // If this is a stewardship connection and this is the connection owner (=sender), also remove the trigger, if it is there
      if (connection.triggerKey != null && connection.stewardshipType == StewardshipType.sender) {
        _gatewayBL.deleteTrigger(connection.triggerKey!);
      }

      await deleteOutgoingShareByContact(connection.id!);
      await deleteIncomingShareByContact(connection.id!);
      await deleteContractById(connection.id!);
    } catch (e) {
      _logger.e(e);
    }
  }

  Future<bool> deleteStewardship(int connectionId) async {
    // get connection
    var connectionMap = await _commonDAOImpl.getDataFromId(ContractModel.getStoreName(), connectionId);
    var connection = ContractModel.fromJson(connectionMap!);

    // get all outgoing shares
    var shares = await getSharedDataFromVault(connectionId);

    for (var share in shares) {
      if (!await deleteOutgoingShare(share.gatewayShareId!, connectionId, share.attributeId!)) {
        return false;
      }
    }

    // send delete request to gateway to remove the trigger
    if (!await _gatewayBL.deleteTrigger(connection.triggerKey!)) {
      _logger.i('Trigger could not be deleted, possible it is already deleted or released');
    }

    // update the contract, removing the stewardship specific things
    await _commonDAOImpl.update(
      ContractModel.getStoreName(),
      connectionId,
      {
        'stewardshipType': null,
        'dataAccessType': 'immediately', // TODO: Refactor: DataAccessType enum should be closer to the datamodel and not only in the view
        'isReleased': false,
      },
    );

    // Get own clientKey, so the other client knows which
    Map? gatewayCredentials = await CryptoUtil.getGatewaySecretData();

    // send socket 'updateConnection' message with 'stewardshipRemove' action  to the other client
    var message = {
      'type': 'updateConnection',
      'action': 'stewardshipRemove',
      'from': gatewayCredentials!['key'],
    };

    bool status = await _socketService.shareMessage(connection.clientKey!, json.encode(message));

    return status;
  }

  /// Extracts the contacts details attributes values.
  Future<ContractDetailViewModel> getContractDetailsDecrypted(ContractDetailViewModel? contractDetailViewModel) async {
    for (var importFieldsList in contractDetailViewModel!.dataMap!.entries) {
      // Loop all values.
      for (var field in importFieldsList.value) {
        // Get values.
        String? valueStr = field.value;

        try {
          if (field.type != 'file' && !field.isDecrypted) {
            // File types are ignored files are decrypt when user click download button.
            String decryptedValue = await CryptoUtil.extractAttributeValue(
              field.value!,
              field.attrKeyEnc!,
            );

            if (decryptedValue.isNotEmpty) {
              valueStr = decryptedValue;
              field.isDecrypted = true;
            }
          }
        } catch (e) {
          _logger.e(e);
        }

        field.value = valueStr;
      }
    }
    return Future.value(contractDetailViewModel);
  }

  /// Stores contracts.
  // TODO: integrate this method with other contract storing scenarios.
  Future<ContractModel> storeContract(ContractModel contractModel) async {
    Map data = await _commonDAOImpl.store(
      ContractModel.getStoreName(),
      contractModel.toJson(),
    );

    return ContractModel.fromJson(data as Map<String, dynamic>);
  }

  /// Gets shared data with the vault from other connections.
  Future<List<FieldViewModel>> getSharedDataWithVault(int contractId) async {
    _logger.d('Get Shared Data with Me');
    List<FieldViewModel> shareList = [];
    try {
      Map<String, dynamic> filterMap = {
        'contractId': contractId,
      };

      List<Map<String, dynamic>> dataList = await _commonDAOImpl.getFilteredData(
        IncomingShareModel.getStoreName(),
        filterMap,
        FilterType.and,
      );

      if (dataList.isEmpty) {
        return shareList;
      }

      for (Map<String, dynamic> map in dataList) {
        FieldViewModel model = FieldViewModel();
        model.fromIncomingShare(map);
        shareList.add(model);
      }
    } catch (e) {
      _logger.e(e);
    }
    return shareList;
  }

  /// Gets all shared data from the vault to other connections (Outgoing shares)
  /// for the given [contractId].
  Future<List<FieldViewModel>> getSharedDataFromVault(contractId) async {
    _logger.d('Get Shared Data From Me');
    List<FieldViewModel> dataReturn = [];
    try {
      List<Map<String, dynamic>> dataList = await _commonDAOImpl.getFilteredData(
        OutgoingShareModel.getStoreName(),
        {'contractId': contractId},
        FilterType.and,
      );

      if (dataList.isEmpty) {
        return dataReturn;
      }

      // var category = '';

      for (Map<String, dynamic> map in dataList) {
        FieldViewModel attribute = await getAttributeData(map['attributeId']);
        attribute.gatewayShareId = map['gatewayShareId'];
        dataReturn.add(attribute);
      }
    } catch (e) {
      _logger.e(e);
    }
    return dataReturn;
  }

  // TODO: can be used in generalized way in this class. Attribute data as [FieldViewModel].
  Future<FieldViewModel> getAttributeData(int attributeId) async {
    Map<String, dynamic>? attributeModel;

    try {
      attributeModel = await _commonDAOImpl.getDataFromId(
        AttributeModel.getStoreName(),
        attributeId,
      );
    } catch (e) {
      _logger.e(e);
      rethrow;
    }

    // var value = await AttributeStoreDAOImpl().getAttributeValue(attributeModel!['id'], attrKeyEnc: attributeModel['attrKeyEnc']);

    FieldViewModel fieldViewModel = FieldViewModel(
      attributeId: attributeModel!['id'],
      label: attributeModel['label'],
      type: attributeModel['type'],
      category: attributeModel['category'],
      description: attributeModel['description'],
      displayValue: attributeModel['displayValue'],
      name: attributeModel['name'],
      attrKeyEnc: attributeModel['attrKeyEnc'],
      isVC: attributeModel['isVC'],
      logoUrl: attributeModel['logoUrl'],
      providerName: attributeModel['providerName'],
      //value: value,
      isDecrypted: true,
    );

    return fieldViewModel;
  }

  /// Deletes outgoing share and sends socket message to shared person.
  Future<bool> deleteOutgoingShare(
    String gatewayShareId,
    int contractId,
    int attributeId,
  ) async {
    Map<String, dynamic>? map = await _commonDAOImpl.getDataFromId(
      ContractModel.getStoreName(),
      contractId,
    );

    if (map!.isNotEmpty) {
      String clientKey = map['clientKey'];

      var message = {
        'type': 'shareRemove',
        'gatewayShareId': gatewayShareId,
      };
      bool status = await _socketService.shareMessage(
          clientKey,
          json.encode(
            message,
          ));
      bool response = await _gatewayBL.removeGatewayLinkShare(
        attributeId,
        gatewayShareId,
      );

      if (status && response) {
        return await ShareStoreDAOImpl().deleteOutgoingShareAttribute(
          gatewayShareId,
          contractId,
        );
      }
    }

    return false;
  }

  /// Deletes outgoing share by filtering contract wise.
  Future<bool> deleteOutgoingShareByContact(int contractId) async {
    Map<String, dynamic> filterMap = {'contractId': contractId};

    try {
      List<Map<String, dynamic>> dataList = await _commonDAOImpl.getFilteredData(
        OutgoingShareModel.getStoreName(),
        filterMap,
        FilterType.and,
      );

      if (dataList.isNotEmpty) {
        for (Map<String, dynamic> map in dataList) {
          OutgoingShareModel outgoingShareModel = OutgoingShareModel.fromJson(
            map,
          );
          await _gatewayBL.removeGatewayLinkShare(
            outgoingShareModel.attributeId!,
            outgoingShareModel.gatewayShareId!,
          );
          await ShareStoreDAOImpl().deleteOutgoingShareAttribute(
            outgoingShareModel.gatewayShareId!,
            contractId,
          );
        }
      }
    } catch (e) {
      _logger.e(e);
      return false;
    }

    return true;
  }

  /// Deletes incoming shared attribute and removes the gateway URL from gateway.
  Future<bool> deleteIncomingShare(String gatewayShareId) async {
    _logger.d('delete Incoming Shared Attribute');
    Map<String, dynamic> filterMap = {'gatewayShareId': gatewayShareId};

    List<Map<String, dynamic>> dataList = await _commonDAOImpl.getFilteredData(
      IncomingShareModel.getStoreName(),
      filterMap,
      FilterType.and,
    );

    // TODO: need to validate what happens if multiple records found ?.
    if (dataList.isNotEmpty && dataList.length == 1) {
      IncomingShareModel incomingShareModel = IncomingShareModel.fromJson(dataList.first);
      return await ShareStoreDAOImpl().deleteIncomingShareAttribute(
        incomingShareModel,
      );
    } else {
      _logger.e('Shared Attribute not found with provided filters');
      return false;
    }
  }

  /// Deletes incoming shared attributes by filtering contract wise.
  Future<bool> deleteIncomingShareByContact(int contractId) async {
    Map<String, dynamic> filterMap = {'contractId': contractId};

    try {
      List<Map<String, dynamic>> dataList = await _commonDAOImpl.getFilteredData(
        IncomingShareModel.getStoreName(),
        filterMap,
        FilterType.and,
      );

      if (dataList.isNotEmpty) {
        for (Map<String, dynamic> map in dataList) {
          IncomingShareModel incomingShareModel = IncomingShareModel.fromJson(map);
          await deleteIncomingShare(incomingShareModel.gatewayShareId!);
        }
      }
    } catch (e) {
      _logger.e(e);
      return false;
    }

    return true;
  }

  /// Get the released stewardship connections
  Future<List<VaultSwitchItemViewModel>> getStewardshipAssignedConnections() async {
    var accountBL = singletonInjector<AccountBLImpl>();
    var items = <VaultSwitchItemViewModel>[];

    //TODO: What if empty / deleted?
    String firstName = await _myDataController.getPersonalInfoValue(
      'MijnOverheid',
      'FirstNames',
    );

    String surname = await _myDataController.getPersonalInfoValue(
      'MijnOverheid',
      'Surname',
    );

    var settings = await accountBL.getSettings();

    // add self
    items.add(VaultSwitchItemViewModel(
      connectionId: 0,
      color: settings.iconColor,
      name: '$firstName $surname',
    ));

    // get the released connections
    var connections = await _commonDAOImpl.getFilteredData(
        ContractModel.getStoreName(),
        {
          'isReleased': true,
          //'stewardshipType': describeEnum(StewardshipType.receiver),
        },
        FilterType.and);

    for (Map<String, dynamic> connection in connections) {
      items.add(VaultSwitchItemViewModel(
        connectionId: connection['id']!,
        color: connection['iconColor'],
        name: connection['requester']['name'],
      ));
    }

    return items;
  }
}
