import 'dart:async';
import 'dart:convert';

import 'package:async/async.dart';
import 'package:flutter_dotenv/flutter_dotenv.dart';
import 'package:logger/logger.dart';
import 'package:schluss_beta_app/business/impl/external_storage_bl_impl.dart';
import 'package:schluss_beta_app/constants/http_response_code_constants.dart';
import 'package:schluss_beta_app/controller/contract_controller.dart';
import 'package:schluss_beta_app/data/impl/attribute_store_dao_impl.dart';
import 'package:schluss_beta_app/data/impl/contract_store_dao_impl.dart';
import 'package:schluss_beta_app/data/impl/share_store_dao_impl.dart';
import 'package:schluss_beta_app/data/interface/common_dao.dart';
import 'package:schluss_beta_app/data/models/attribute_model.dart';
import 'package:schluss_beta_app/data/models/connect_request_model.dart';
import 'package:schluss_beta_app/data/models/outgoing_share_model.dart';
import 'package:schluss_beta_app/services/impl/task_service.dart';
import 'package:schluss_beta_app/services/interface/gateway_service.dart';
import 'package:schluss_beta_app/services/interface/socket_service.dart';
import 'package:schluss_beta_app/singleton_injector.dart';
import 'package:schluss_beta_app/util/common_util.dart';
import 'package:schluss_beta_app/util/crypto_util.dart';
import 'package:schluss_beta_app/view_model/gateway_viewmodel.dart';
import 'package:schluss_beta_app/view_model/vault_switch_item_view_model.dart';
import 'package:session_next/session_next.dart';

class GatewayBLImpl {
  final Logger _logger = singletonInjector<Logger>();
  final GatewayService _gatewayService = singletonInjector<GatewayService>();
  final CommonDAO _commonDAOImpl = singletonInjector<CommonDAO>();
  final AttributeStoreDAOImpl _attributeStore = AttributeStoreDAOImpl();

  /// Validates and returns gateway connection if already available.
  /// If not available create the connection and returns.
  Future<bool> validateGatewayConnection() async {
    try {
      // Check gateway secret data available on keystore.
      bool isSecretDataAvailable = await validateSecretDataAvailability();

      if (!isSecretDataAvailable) {
        GatewayViewModel gatewayViewModel = await registerNewAccount();
        if (gatewayViewModel.secret == null || gatewayViewModel.key == null) {
          return false;
        }
      }
    } catch (e) {
      _logger.e(e);
      return false;
    }
    return true;
  }

  /// Registers a new account in gateway.
  Future<GatewayViewModel> registerNewAccount() async {
    var gatewayUrl = dotenv.env['UGW_URL']!;
    var endpoint = dotenv.env['UGW_REGISTER_ENDPOINT']!;

    try {
      GatewayViewModel gatewayViewModel = await _gatewayService.registerNewAccount(
        CommonUtil.buildUrl(
          gatewayUrl,
          path: endpoint,
        ),
      );

      _logger.d('Registering User Completed $gatewayViewModel');

      // Store secret data in the secure keystore.
      await CryptoUtil.storeGatewaySecretData(gatewayViewModel.secretData);

      return gatewayViewModel;
    } catch (e) {
      _logger.e(e);
      rethrow;
    }
  }

  /// Validates the gateway secret data available status.
  Future<bool> validateSecretDataAvailability() async {
    bool isAvailable = false;

    // Retrieve secret data from secure keystore.
    Map? secretData = await CryptoUtil.getGatewaySecretData();

    // Check availability of any gateway [secretData] in the secure store.
    if (secretData != null) {
      isAvailable = true;
    }

    return Future.value(isAvailable);
  }

  /// Retrieves token from secure keystore.

  Future<String?> getGatewayClientToken() async {
    Map? secretData = await (CryptoUtil.getGatewaySecretData());
    if (secretData == null || !secretData.containsKey('key')) {
      throw Exception('Gateway Secret Key not found');
    }

    return secretData['key'];
  }

  /// Retrieves connection data from gateway.
  Future<GatewayViewModel> getConnectionData(String firstName, String lastName) async {
    Map? secretData = await (CryptoUtil.getGatewaySecretData());

    if (secretData == null ||
        !secretData.containsKey('key') ||
        !secretData.containsKey(
          'secret',
        )) {
      throw ('Gateway Secret Key not found');
    }

    int? sessionId = await initConnectionRequest();

    String token = secretData['key'] + '/' + secretData['secret'];
    String gatewayUrl = dotenv.env['UGW_URL']!;

    String endpoint = dotenv.env['UGW_CONNECTION_ENDPOINT']!;
    Map payloadData = {
      'type': 'connectRequest',
      'firstName': firstName,
      'lastName': lastName,
      'gatewayUrl': gatewayUrl,
      'clientKey': secretData['key'],
      'clientPublicKey': SessionNext().get<String>('publicKey'),
      'sessionId': sessionId,
    };

    GatewayViewModel gatewayViewModel = await _gatewayService.getConnectionLink(
        CommonUtil.buildUrl(gatewayUrl, path: endpoint),
        token,
        json.encode(
          payloadData,
        ));

    await updateConnectionRequest(sessionId!, gatewayViewModel);

    return gatewayViewModel;
  }

  /// Inits connection request data store.
  Future<int?> initConnectionRequest() async {
    ConnectRequestModel model = ConnectRequestModel.fromJson({
      'timestamp': DateTime.now().millisecondsSinceEpoch,
    });
    Map<String, dynamic> dataMap = await (_commonDAOImpl.store(
      ConnectRequestModel.getStoreName(),
      model.toJson(),
    ));
    if (dataMap.containsKey('id')) {
      return dataMap['id'];
    }

    return null;
  }

  /// Updates connection request data store.
  Future<ConnectRequestModel> updateConnectionRequest(int id, GatewayViewModel gatewayViewModel) async {
    ConnectRequestModel model = ConnectRequestModel.fromJson({
      'id': id,
      'token': gatewayViewModel.token,
      'objectId': gatewayViewModel.objectId,
      'location': gatewayViewModel.location,
      'timestamp': DateTime.now().millisecondsSinceEpoch,
    });
    Map<String, dynamic>? dataMap = await (_commonDAOImpl.update(
      ConnectRequestModel.getStoreName(),
      id,
      model.toJson(),
    ));
    return ConnectRequestModel.fromJson(dataMap!);
  }

  /// Requests user socket information from gateway.
  Future<GatewayViewModel> requestUserSocketData(String location, String token) async {
    return await _gatewayService.executeGetRequest(location, token: token);
  }

  /// Creates a new link to enable shares.
  ///
  /// Creates a new link for [attributeId] on the gateway which enables creating shares later on.
  /// If attribute is not synced with external storage yet, it is done on the fly
  /// because we have no real implementation yet, I've made this with the [attributeID] as input,
  /// but could also easily be changed to [attributeVersionId] or [attributeModel] for example.
  Future<bool> postGatewayLink(int attributeId) async {
    var externalStorage = singletonInjector<ExternalStorageBLImpl>();

    // Get attribute model.
    var attributeModel = await _attributeStore.getAttribute(attributeId);

    // Validate attribute model.
    if (attributeModel == null) {
      _logger.e('postGatewayLink failed: attribute not found');
      return false;
    }

    // Check if there already is a link for this attribute.
    if (attributeModel.gatewayLinkObjectId != null) {
      _logger.i(
        'postGatewayLink: Gateway link is already present, skipping creation process',
      );
      return true;
    }

    // Check if gateway is ready.
    if (!await validateGatewayConnection()) {
      _logger.e('postGatewayLink failed: no valid Gateway connection');
      return false;
    }

    // Check if external storage is ready.
    if (!await externalStorage.validateGatewayConnection()) {
      _logger.e('postGatewayLink failed: no valid External storage connection');
      return false;
    }

    Map? gatewayCredentials = await CryptoUtil.getGatewaySecretData();
    String gatewayKey = gatewayCredentials!['key'];
    String gatewaySecret = gatewayCredentials['secret'];
    var gatewayUrl = dotenv.env['UGW_URL']!;
    var extStorageUrl = dotenv.env['EGW_URL']!;
    var extStorageToken = await externalStorage.getToken();

    // Get latest attribute version model.
    var attributeVersionModel = await _attributeStore.findLatestAttributeVersion(
      attributeId,
    );

    if (attributeVersionModel == null) {
      _logger.e('postGatewayLink failed: attribute version not found');
      return false;
    }

    // Check if attrVersion is synced already, if not: do it now immediately.
    if (attributeVersionModel.onlineStoragePath == null || attributeVersionModel.onlineStorageProvider == null) {
      var syncResult = await singletonInjector<TaskService>().addWithPriority(
        'SyncAttributeTask',
        {'id': attributeVersionModel.id},
      );

      if (!syncResult) {
        _logger.e(
          'postGatewayLink failed: attribute version could not be synched with External storage',
        );
        return false;
      }

      // Get a fresh update of the [attributeVersionModel] to incorporate the
      // [onlineStoragePath] and [onlineStorageProvider].
      var attrVersionID = attributeVersionModel.id!;
      attributeVersionModel = await _attributeStore.getAttributeVersion(
        attrVersionID,
      );
    }

    // Create gateway link.
    var requestBody = {
      'name': attributeModel.name,
      'label': attributeModel.label,
      'group': attributeModel.group,
      'category': attributeModel.category,
      'type': attributeModel.type,
      'logoUrl': attributeModel.logoUrl,
      'providerName': attributeModel.providerName,
      'mediaType': attributeModel.mediaType,
      'version': attributeVersionModel!.version,
      'storageLocation': CommonUtil.buildUrl(
        extStorageUrl,
        path: attributeVersionModel.onlineStoragePath,
      ),
      'storageProvider': attributeVersionModel.onlineStorageProvider,
      'storageAuthorization': {'authorization': extStorageToken},
    };

    var response = await _gatewayService.executePostRequest(
        CommonUtil.buildUrl(
          gatewayUrl,
          path: '/links',
        ),
        requestBody,
        headers: {
          'Authorization': '$gatewayKey/$gatewaySecret',
        });

    if (response == null && !response!.containsKey('objectId')) {
      _logger.e(
        'postGatewayLink failed: Link could not be created at the Gateway',
      );
      return false;
    }

    // Store link at attribute.
    var updateResult = await _attributeStore.updateAttributeMetadata(attributeId, {
      'gatewayLinkKey': response['linkKey'],
      'gatewayLinkObjectId': response['objectId'],
      'gatewayLinkOwnerKey': response['ownerKey'],
    });

    // Next? return link details?.
    return updateResult != null ? true : false;
  }

  /// Shares an attribute using [attributeId].
  ///
  /// Creates a share for an attribute using [attributeId],
  /// to be shared with the connection which is identified by [contractId].
  /// if no gateway link is present yet, it will be created on the fly.
  Future<GatewayPostShareResult> postGatewayLinkShare(int attributeId, int contractId) async {
    var contractStore = ContractStoreDAOImpl();
    var gatewayUrl = dotenv.env['UGW_URL']!;

    // Get attribute model.
    var attributeModel = await _attributeStore.getAttribute(attributeId);

    // Validate attribute model.
    if (attributeModel == null) {
      _logger.e('postGatewayShare failed: attribute not found');
      return GatewayPostShareResult(false);
    }

    // Get the contract.
    var contractModel = await contractStore.getById(contractId);

    // Validate contract model.
    if (contractModel == null) {
      _logger.e('postGatewayShare failed: contract not found');
      return GatewayPostShareResult(false);
    }

    // If the attribute does not have a link yet, create it first.
    if (attributeModel.gatewayLinkObjectId == null) {
      if (!await postGatewayLink(attributeId)) {
        _logger.e(
          'postGatewayShare failed: attribute link could not be created',
        );
        return GatewayPostShareResult(false);
      }

      // Get a fresh update of the [attributeModel] to incorporate
      // the gatewayXYZ properties.
      attributeModel = await _attributeStore.getAttribute(attributeId);
    }

    // Check if gateway is ready.
    if (!await validateGatewayConnection()) {
      _logger.e('postGatewayShare failed: no valid Gateway connection');
      return GatewayPostShareResult(false);
    }

    // Create a hash of the [clientPubKey] in the contract.
    var pubKeyHash = await CryptoUtil.generateHash(contractModel.clientPublicKey!);

    // Create gateway share.
    var requestBody = {
      'publicKeyHash': pubKeyHash,
    };

    var response = await _gatewayService.executePostRequest(
      CommonUtil.buildUrl(
        gatewayUrl,
        path: '/links/${attributeModel!.gatewayLinkObjectId}/shares/',
      ),
      requestBody,
      headers: {
        'Authorization': '${attributeModel.gatewayLinkKey}/${attributeModel.gatewayLinkOwnerKey}',
      },
    );

    if (response == null || !response.containsKey('objectId')) {
      _logger.e(
        'postGatewayShare failed: Share could not be created at the Gateway for Link',
      );
      return GatewayPostShareResult(false);
    }

    return GatewayPostShareResult(
      true,
      attributeId: attributeId,
      objectId: response['objectId'],
      shareId: response['shareId'],
      location: response['location'],
    );
  }

  /// Shares multiple attribute using [attributeId].
  ///
  /// Creates multiple shares at once for attributes using [attributeIds],
  /// to be shared with the connection which is identified by [contractId].
  /// If no gateway link is present yet, it will be created on the fly.
  Future<List<GatewayPostShareResult>> postGatewayLinkShares(List<int> attributeIds, int contractId) async {
    var futureGroup = FutureGroup<GatewayPostShareResult>();

    for (var id in attributeIds) {
      futureGroup.add(postGatewayLinkShare(id, contractId));
    }

    futureGroup.close();

    return futureGroup.future;
  }

  /// Sends a single [share] to the other client and stores
  /// it in outgoing share when succeeds.
  Future<bool> sendGatewayShare(GatewayPostShareResult share, int contractId, bool hideValues) async {
    return sendGatewayShares([share], contractId, hideValues);
  }

  /// Sends multiple [shares] to the other client and stores
  /// it as outgoing shares when succeeds.
  Future<bool> sendGatewayShares(List<GatewayPostShareResult> shares, int contractId, bool hideValues) async {
    var contractStore = ContractStoreDAOImpl();
    Map? secretData = await CryptoUtil.getGatewaySecretData();

    // Get contract.
    var contractModel = await contractStore.getById(contractId);

    if (contractModel == null) {
      _logger.e('sendGatewayShares failed: contract was not found');
      return false;
    }

    // Create the socket message share list.
    var futureGroup = FutureGroup<Map<String, String>?>();

    for (var share in shares) {
      // When a share is not created on the gateway, for now ignore it...?.
      if (!share.succeeded) {
        continue;
      }

      futureGroup.add(_createShare(
        share,
        contractModel.clientPublicKey!,
        contractId,
      ));
    }

    futureGroup.close();

    // Wait for all share list items are generated.
    var socketMessageShares = await futureGroup.future;

    // Remove empty elements from the list, for now we do not do something
    // with failed shares.
    socketMessageShares.removeWhere((element) => element == null);

    // Send socket message.
    var socket = singletonInjector<SocketService>();

    await socket.connect();

    var socketMessage = {
      'type': 'attributeShare',
      'from': secretData!['key'],
      'shares': socketMessageShares,
      'hideValues': hideValues,
    };

    var shareResult = socket.shareMessage(
        contractModel.clientKey!,
        json.encode(
          socketMessage,
        ));

    return shareResult;
  }

  /// Generates a single share for the socket message share list and stores it
  /// in the outgoing share table.
  Future<Map<String, String>?> _createShare(
    GatewayPostShareResult share,
    String clientPublicKey,
    int contractId,
  ) async {
    var shareStore = ShareStoreDAOImpl();

    // Get attribute.
    var attributeModel = await _attributeStore.getAttribute(share.attributeId!);

    // Validate attribute model.
    if (attributeModel == null) {
      _logger.e('sendGatewayShares failed: attribute not found');
      return null;
    }

    // Get the the decryption key decrypted.
    var decAttrKey = await CryptoUtil.decrypt(attributeModel.attrKeyEnc!);

    // Encrypt the attrKey with public key of other client.
    var attrKeyEnc = await CryptoUtil.encrypt(decAttrKey, clientPublicKey);

    // Store the share in outgoing shares.
    OutgoingShareModel shareModel = OutgoingShareModel(
      attributeId: share.attributeId,
      contractId: contractId,
      gatewayShareId: share.shareId,
    );

    // Store it.
    await shareStore.addOutgoingShare(shareModel);

    // Return the share details.
    return {
      'location': share.location!,
      'gatewayShareId': share.shareId!,
      'authorization': attributeModel.gatewayLinkKey!,
      'key': attrKeyEnc,
    };
  }

  /// Removes a attribute shared link from gateway.
  Future<bool> removeGatewayLinkShare(int attributeId, String gatewayShareId) async {
    try {
      var gatewayUrl = dotenv.env['UGW_URL']!;
      // Get attribute.
      AttributeModel? attributeModel = await _attributeStore.getAttribute(
        attributeId,
      );

      var response = await _gatewayService.executeDeleteRequest(
        CommonUtil.buildUrl(
          gatewayUrl,
          path: '/links/${attributeModel!.gatewayLinkObjectId}/shares/$gatewayShareId',
        ),
        {
          'Authorization': '${attributeModel.gatewayLinkKey}/${attributeModel.gatewayLinkOwnerKey}',
        },
      );

      if (response == null || !response.containsKey('status')) {
        return true;
      }
    } catch (e) {
      _logger.e(e);
      return false;
    }

    return true;
  }

  /// Registering Trigger with Attributes
  Future<String?> registerTrigger(List<String?> attributeShareIds, String toKey, int contractId) async {
    Map? ownerSecretData = await (CryptoUtil.getGatewaySecretData());
    String ownerKey = ownerSecretData!['key'];
    String ownerSecret = ownerSecretData['secret'];
    var gatewayUrl = dotenv.env['UGW_URL']!;
    var triggerEndpoint = dotenv.env['UGW_TRIGGER']!;

    // Hardcoded body for demo purposes, will have to become real in the future, but that will probably be a totally different implementation
    var requestBody = {
      'trigger': {
        'url': 'https://aanwezigheidverklaringvanoverlijden.azurewebsites.net/api/verify',
        'method': 'POST',
        'header': {'x-functions-key': '5Oe8hTtPeO7hS2HU6jiMtsUVbJTTbFoxm4yQqvEjAL/EV5Z5MaD7Fg=='},
        'expect': {'vvo': true, 'message': 'success'},
      },
      'from': ownerKey,
      'to': toKey,
      'shares': attributeShareIds
    };

    var response = await _gatewayService.executePostRequest(
        CommonUtil.buildUrl(
          gatewayUrl,
          path: triggerEndpoint,
        ),
        requestBody,
        headers: {
          'Authorization': '$ownerKey/$ownerSecret',
        });

    // TODO: doublecheck -> response null and contains ok?
    if (response == null && HttpResponseCodeConstants.responseOk.contains(response!['statusCode'] as int)) {
      _logger.e('Trigger failed: Cannot register trigger at Gateway');
    }

    return response['trigger'];
  }

  Future<bool> releaseTrigger(int connectionId) async {
    var contractController = ContractController();

    try {
      // get connection
      var contractStore = ContractStoreDAOImpl();
      var connection = await contractStore.getById(connectionId);

      // when connection not found
      if (connection == null) {
        return false;
      }

      var gatewayUrl = dotenv.env['UGW_URL']!;
      var triggerEndpoint = dotenv.env['UGW_TRIGGER']!;

      // Hardcoded body for demo purposes, will have to become real in the future, but that will probably be a totally different implementation
      var requestBody = {
        'name': 'Jan Janssen',
        'birthdate': '1935-01-03',
      };

      var response = await _gatewayService.executePostRequest(CommonUtil.buildUrl(gatewayUrl, path: '$triggerEndpoint/release'), requestBody, headers: {
        'Authorization': connection.triggerKey,
      });

      if (response == null || !response.containsKey('status') || response['status'] != 'Trigger released') {
        return false;
      }

      // update the contract, set isRelease to true
      await contractStore.updateContractData(connectionId, {'isReleased': true});

      // update the session, refresh the list of connections
      SessionNext().set<List<VaultSwitchItemViewModel>>('switchConnections', await contractController.getStewardshipAssignedConnections());

      return true;
    } catch (e) {
      _logger.e('Trigger failed: Could not release, error:');
      _logger.e(e);

      return false;
    }
  }

  Future<bool> deleteTrigger(String triggerKey) async {
    try {
      var gatewayUrl = dotenv.env['UGW_URL']!;
      var triggerEndpoint = dotenv.env['UGW_TRIGGER']!;

      var response = await _gatewayService.executeDeleteRequest(
        CommonUtil.buildUrl(gatewayUrl, path: triggerEndpoint),
        {
          'Authorization': triggerKey,
        },
      );

      if (response == null || !response.containsKey('status') || response['status'] != 'Trigger deleted') {
        return false;
      }

      return true;
    } catch (e) {
      _logger.e('Trigger failed: Could not delete, error:');
      _logger.e(e);
      return false;
    }
  }
}

/// Helpers class, holding the result of the creation of a gateway share attempt.
class GatewayPostShareResult {
  final bool succeeded;
  final String? objectId;
  final String? shareId;
  final String? location;

  final int? attributeId;

  GatewayPostShareResult(
    this.succeeded, {
    this.attributeId,
    this.objectId,
    this.shareId,
    this.location,
  });
}
