import 'package:flutter/cupertino.dart';
import 'package:schluss_beta_app/business/impl/plugin_manager_bl_impl.dart';
import 'package:schluss_beta_app/business/interface/plugin_manager_bl.dart';
import 'package:schluss_beta_app/plugins/me/me.dart';
import 'package:schluss_beta_app/view_model/provider_config_view_model.dart';
import 'package:schluss_beta_app/view_model/request_view_model.dart';

class MePluginManagerBLImpl extends PluginManagerBLImpl implements PluginManagerBL {
  final PluginMe _pluginMe = PluginMe();

  @override
  void onExecutePlugin(
    BuildContext context,
    ProviderConfigViewModel? providerModel,
    Function callback,
  ) async {
    _pluginMe.runPlugin(
      context,
      callback,
      '',
      '',
      providerModel!.dataProvider!.name!,
    );
  }

  @override
  Future<Map<String, dynamic>> onGetData(String attributeDataList) {
    return _pluginMe.getExtractedData(attributeDataList);
  }

  @override
  Stream<int> getProgressValue(RequestViewModel requestViewModel) {
    return _pluginMe.getProgress();
  }

  /// Executes the plugin directly.
  // TODO: need to change the naming and remove [context].
  @override
  void executePluginDirect(
    context,
    Function callBack,
    String siteUrl,
    String trigger, {
    String? pluginName = 'ME',
    Function(String userName, String password)? storeCredentialCallback,
    String? userName,
    String? password,
  }) {
    throw Exception('Not Implemented');
  }
}
