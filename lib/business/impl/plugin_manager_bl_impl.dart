import 'dart:async';
import 'dart:convert';

import 'package:flutter/cupertino.dart';
import 'package:logger/logger.dart';
import 'package:schluss_beta_app/business/impl/account_bl_impl.dart';
import 'package:schluss_beta_app/data/impl/attribute_store_dao_impl.dart';
import 'package:schluss_beta_app/data/interface/common_dao.dart';
import 'package:schluss_beta_app/data/models/attribute_model.dart';
import 'package:schluss_beta_app/data/models/request_data_model.dart';
import 'package:schluss_beta_app/services/impl/iota_ledger_service.dart';
import 'package:schluss_beta_app/singleton_injector.dart';
import 'package:schluss_beta_app/view_model/provider_config_view_model.dart';
import 'package:schluss_beta_app/view_model/request_view_model.dart';

/// Generics plugin manager class.
abstract class PluginManagerBLImpl {
  final Logger _logger = singletonInjector<Logger>();
  final CommonDAO _commonDAOImpl = singletonInjector<CommonDAO>();
  final AccountBLImpl _accountBL = singletonInjector<AccountBLImpl>();

  /// Executes plugin with callback function.
  void executePlugin(BuildContext context, RequestViewModel requestModel, int providerIndex, Function callback) async {
    _logger.i('Executes Plugin - $runtimeType');

    var providerModel = requestModel.getProviderList().elementAt(providerIndex);

    // Execute the actual plugin.
    onExecutePlugin(
      context,
      providerModel,
      callback,
    );
  }

  /// Executes plugin directly calling the function.
  // TODO: need to change the naming and remove [context].
  void executePluginDirect(
    context,
    Function callBack,
    String siteUrl,
    String trigger, {
    String? pluginName,
    Function(String userName, String password)? storeCredentialCallback,
    String? userName,
    String? password,
  });

  /// Overrides this method to call the implementation of the plugin:[PluginX.runPlugin(..)].
  void onExecutePlugin(
    BuildContext context,
    ProviderConfigViewModel? providerModel,
    Function callback,
  );

  /// Gets data from plugin.
  Future<RequestViewModel> getData(
    RequestViewModel requestModel,
    String attributeDataList,
  ) async {
    try {
      Map<String, dynamic>? responseList = await onGetData(attributeDataList);

      return Future.value(processResponse(requestModel, responseList!));
    } catch (e) {
      _logger.e(
        'Error Occurred while getting data from Plugin - $this.runtimeType.toString(): ${e.toString()}',
      );

      return Future.error(e);
    }
  }

  /// Overrides this method to call the implementation of the plugin:[PluginX.getExtractedData(..)].
  Future<Map<String, dynamic>>? onGetData(String attributeDataList);

  // Processes response from plugin and set data into request.
  Future<RequestViewModel> processResponse(
    RequestViewModel requestModel,
    Map<String, dynamic> responseList,
  ) async {
    try {
      var index = requestModel.getCurrentSelectedIndex()!;
      var providerModel = requestModel.getProviderList().elementAt(index);

      var attributeIdList = <int?>[];

      // First, make sure we only have the requested attributes to be processed further.
      for (var listItem in responseList.entries) {
        for (var importField in providerModel.fields!) {
          // When no match: skip.
          if (importField.match!.toLowerCase() != listItem.key.toLowerCase()) continue;

          // Set value for current field.
          importField.value = listItem.value;
        }
      }

      // When to make vc's from received attributes (and for now, only when test-mode is on).
      if (providerModel.selfSignedAttributes && await (_accountBL.isTestModeOn())) {
        var ledger = IOTALedgerService();
        var did = await ledger.getDID();

        // Convert the list of [FieldViewModel] items to a map (as import for the createVC method).
        // var attrList = {for (var e in providerModel.fields)  e.name: e.value}; // = future, when  all fields can be processed in the same way (also binary + list).

        var attrList = <String, dynamic>{};
        for (var field in providerModel.fields!) {
          switch (field.type) {
            // When it is a list / nested structure
            case 'list':
            // When it is a file
            case 'file':
              continue;
            case 'vc':
              attrList[field.name as String] = jsonDecode(field.value as String);
              break;

            // In all other cases (single attribute of type string, date, int etc)
            default:
              attrList[field.name as String] = field.value as String;
              break;
          }

          //if (field.type == 'list' || field.type == 'file') continue; // For now, filter out lists and files

          //attrList[field.name as String] = field.value as String;
        }

        // Overwrite the list values with updated VC values.
        attrList = await (ledger.createVC(did!['didId'], attrList));

        // Store the updated VC values in the list of [FieldViewModel] items.
        for (var vcAttr in attrList.entries) {
          for (var importField in providerModel.fields!) {
            // When no match: skip.
            if (importField.match!.toLowerCase() != vcAttr.key.toLowerCase()) continue;

            // Update the value with the VC value.
            importField.value = vcAttr.value;
            importField.isVC = true;
          }
        }
      }

      // Store the attributes.
      for (var importField in providerModel.fields!) {
        // Store attribute.
        AttributeModel? model = AttributeModel(
          name: importField.name,
          category: importField.category,
          group: importField.group,
          description: importField.description,
          label: importField.label,
          displayValue: importField.displayValue,
          logoUrl: providerModel.dataProvider!.logoUrl,
          providerName: providerModel.dataProvider!.name,
          type: importField.type,
        );

        model = await AttributeStoreDAOImpl().storeAttributeFromModel(
          model,
          importField.value!,
        );

        attributeIdList.add(model!.id);
      }

      // Update the data request with the stored attribute id's.
      Map<dynamic, dynamic>? providerMap = {};

      if (requestModel.providerIdList != null) {
        var existingProviders = requestModel.providerIdList!;

        providerMap = json.decode(existingProviders);
      }

      providerMap!.putIfAbsent(
        requestModel.getCurrentSelectedIndex().toString(),
        () => attributeIdList,
      );

      // Update request with index table [id].
      await _commonDAOImpl.update(
        RequestDataModel.getStoreName(),
        requestModel.id!,
        {
          'providers': json.encode(providerMap),
        },
      );

      providerModel.isChecked = true;
      requestModel.providerIdList = json.encode(providerMap);

      return Future.value(requestModel);
    } catch (e) {
      return Future.error(
        'Error occurred while processing data from Plugin - $this.runtimeType.toString(): ${e.toString()}',
      );
    }
  }
}
