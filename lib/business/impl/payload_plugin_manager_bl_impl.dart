import 'package:json_path/json_path.dart';

abstract class PayloadPluginManagerBlImpl {
  /// Extracts a single value from a given path.
  dynamic extract(Map? jsonObject, String search) {
    JsonPath result = JsonPath(search);

    var res = result.read(jsonObject);

    if (res.isEmpty) {
      return '';
    }

    return res.first.value;
  }

  /// Maybe future helper function to get the shares form the plugin.
  Map<String, dynamic> getShares(List providerList) {
    throw 'Not implemented';
  }
}
