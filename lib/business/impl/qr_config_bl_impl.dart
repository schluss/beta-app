import 'dart:convert';

import 'package:schluss_beta_app/controller/account_controller.dart';
import 'package:schluss_beta_app/data/impl/attribute_store_dao_impl.dart';
import 'package:schluss_beta_app/data/interface/common_dao.dart';
import 'package:schluss_beta_app/data/models/attribute_model.dart';
import 'package:schluss_beta_app/data/models/attribute_version_model.dart';
import 'package:schluss_beta_app/data/models/incoming_share_model.dart';
import 'package:schluss_beta_app/data/models/request_data_model.dart';
import 'package:schluss_beta_app/services/interface/config_service.dart';
import 'package:schluss_beta_app/singleton_injector.dart';
import 'package:schluss_beta_app/util/vc_util.dart';
import 'package:schluss_beta_app/view_model/data_request_config_view_model.dart';
import 'package:schluss_beta_app/view_model/field_view_model.dart';
import 'package:schluss_beta_app/view_model/provider_config_view_model.dart';
import 'package:schluss_beta_app/view_model/request_view_model.dart';

class QRConfigurationBLImpl {
  final ConfigurationService _configService = singletonInjector<ConfigurationService>();
  final CommonDAO _commonDAOImpl = singletonInjector<CommonDAO>();

  /// Processes configuration data using configuration URL.
  /// Configuration URL template : [https://schluss.org/redirect/connect/[token]/[settingsurl]/[scope]].
  Future<DataRequestConfigViewModel> processConfigData(String configUrl, String scope, String settingsUrl) async {
    DataRequestConfigViewModel dataRequest;

    try {
      dataRequest = await _configService.retrieveAppConfigByScope(
        settingsUrl,
        scope,
      );
    } catch (e) {
      return Future.error(e);
    }

    // check if this is a datarequest that is meant to be opened from one others' vault (when there is a from=current field)
    if (AccountController().isInOwnVault()) {
      for (var provider in dataRequest.providers) {
        if (provider.from == 'current') {
          return Future.error('This datarequest needs to be started from someone else\'s vault and not your own');
        }
      }
    }

    return Future.value(dataRequest);
  }

  /// Requests configuration data using [RequestViewModel] settings URL.
  Future<DataRequestConfigViewModel> retrieveConfigData(RequestViewModel? model) async {
    try {
      DataRequestConfigViewModel configViewModel = await _configService.retrieveAppConfigByScope(model!.settingsUrl, model.scope);

      // TODO: set providers directly to the [DataRequestConfigViewModel].
      configViewModel.providers = model.getProviderList();

      return Future.value(configViewModel);
    } catch (e) {
      return Future.error(e);
    }
  }

  /// Checks and (pre)loads the attribute values, if they are already exist in the DB.
  Future<void> preLoadDataIfAlreadyExists(RequestViewModel? requestViewModel) async {
    List<ProviderConfigViewModel> providerList = requestViewModel!.getProviderList();
    Map? providerMap = {};

    for (ProviderConfigViewModel providerViewModel in providerList) {
      bool isAllAttributesAvailable = false;
      List<int?> attributeIdList = [];
      List<FieldViewModel> updatedFieldsList = [];

      for (FieldViewModel importField in providerViewModel.fields!) {
        var updateField = importField.copyWith();

        // If the attribute already has a value (en therefore is filled already), skip the attribute.
        if (importField.value != null) {
          updatedFieldsList.add(updateField);
          continue;
        }

        // When from the owners' vault (from == '' || from == 'owner')
        if (providerViewModel.from == null || providerViewModel.from == 'owner') {
          // Data filtering criteria for attribute store by attribute "Match Name" and "Provider Name".
          Map<String, dynamic> attributeFilterMap = {
            'name': updateField.match,
            'providerName': providerViewModel.dataProvider!.name,
          };

          // Get all attributes filtered by [attributeFilterMap].
          List<Map<String, dynamic>> attributeList = await _commonDAOImpl.getFilteredData(
            AttributeModel.getStoreName(),
            attributeFilterMap,
            FilterType.and,
          );

          if (attributeList.isEmpty) {
            continue;
          }

          // Use 1st attribute if there are many attributes with same name and provider.
          AttributeModel existingAttributeModel = AttributeModel.fromJson(
            attributeList.first,
          );

          // TODO: redundancy code. Validate this codes in the refactoring.
          //  Retrieve attribute versions from AttributeVersion store sorted by version.
          Map<String, dynamic> attributeVersionFilterMap = {
            'attributeId': existingAttributeModel.id,
          };

          List<Map<String, dynamic>> attributeVersionList = await _commonDAOImpl.getFilteredData(
            AttributeVersionModel.getStoreName(),
            attributeVersionFilterMap,
            FilterType.and,
            sortMap: {'version': true},
          );

          if (attributeVersionList.isEmpty) {
            continue;
          }

          // Get the latest version for attribute.
          AttributeVersionModel attributeVersionModel = AttributeVersionModel.fromJson(attributeVersionList.last);

          providerViewModel.attributeVersionList.add(attributeVersionModel);

          // Get the attribute value
          updateField.value = await AttributeStoreDAOImpl().getAttributeValue(
            existingAttributeModel.id!,
            attrKeyEnc: existingAttributeModel.attrKeyEnc,
            localPath: attributeVersionModel.localPath,
          );

          if (VCUtil.validateVC(updateField.value)) {
            updateField.displayValue = VCUtil.getValue(updateField.match!, updateField.value!);
            updateField.isVC = true;
          }

          // Add All updated fields objects to the new list.
          updatedFieldsList.add(updateField);
          // add to the list
          attributeIdList.add(existingAttributeModel.id);
        }
        // When from the current vault the user is in, get the values from the incomingshare table
        else {
          Map<String, dynamic> attributeFilterMap = {
            'name': updateField.match,
            'providerName': providerViewModel.dataProvider!.name,
          };

          // Get all attributes filtered by [attributeFilterMap].
          List<Map<String, dynamic>> attributeList = await _commonDAOImpl.getFilteredData(
            IncomingShareModel.getStoreName(),
            attributeFilterMap,
            FilterType.and,
          );

          if (attributeList.isEmpty) {
            continue;
          }

          // TODO: temporary, very dirty cast from IncomingShareModel -> AttributeVersionModel to make it fit in the current datarequest model
          // Whole datarequest component needs to be refactored and build up from scratch!
          var attributeVersionModel = AttributeVersionModel.fromJson(attributeList.first);
          var incomingShareModel = IncomingShareModel.fromJson(attributeList.first);

          providerViewModel.attributeVersionList.add(attributeVersionModel);

          // Get attribute value
          updateField.value = await AttributeStoreDAOImpl().getAttributeValueFromDirectory(attributeVersionModel.localPath!, incomingShareModel.attrKeyEnc!);

          if (VCUtil.validateVC(updateField.value)) {
            updateField.displayValue = VCUtil.getValue(updateField.match!, updateField.value!);
            updateField.isVC = true;
          }

          // Add All updated fields objects to the new list.
          updatedFieldsList.add(updateField);
          // add to the list
          attributeIdList.add(incomingShareModel.id);
        }
      }

      // Validate the size of existing provider fields list size and value updated new list size.
      // This will ensure value's are updated for all the provider import attributes.
      isAllAttributesAvailable = providerViewModel.fields!.length == updatedFieldsList.length ? true : false;

      if (isAllAttributesAvailable) {
        requestViewModel.setCurrentSelectedIndex(providerList.indexOf(providerViewModel));

        // If provider list found convert to map.
        if (requestViewModel.providerIdList != null) {
          String existingProviders = requestViewModel.providerIdList!;
          providerMap = json.decode(existingProviders);
        }

        providerMap!.putIfAbsent(
          requestViewModel.getCurrentSelectedIndex().toString(),
          () => attributeIdList,
        );

        providerViewModel.fields = updatedFieldsList;

        // Update request with index table [id].
        await _commonDAOImpl.update(
          RequestDataModel.getStoreName(),
          requestViewModel.id!,
          {
            'providers': json.encode(providerMap),
          },
        );

        providerViewModel.isChecked = isAllAttributesAvailable;

        requestViewModel.providerIdList = json.encode(providerMap);
      }
    }
  }

  Future<void> matchWithProviderConfig(DataRequestConfigViewModel dataRequest) async {
    // Get all providers from data request.
    List<ProviderConfigViewModel> providers = dataRequest.providers;
    List<ProviderConfigViewModel> matchedProviders = [];
    // Loop each providers.
    for (var provider in providers) {
      // Get main config for each provider.
      ProviderConfigViewModel? providerConfigViewModel = provider;
      // Get provider config file as map eg:belasting config.
      Map<String, dynamic> data = json.decode(provider.rawData!);
      // Get export fields list of each provider.
      var exports = List<Map<String, dynamic>>.from(data['export']);
      List<FieldViewModel> matchedFieldsList = [];
      // Get fields from config file.
      List<FieldViewModel> fieldsList = provider.fields!;
      for (var fieldViewModel in fieldsList) {
        // Go through all export fields.
        for (var i = 0; i < exports.length; i++) {
          // If import.match equals export.name, we have a match.
          if (fieldViewModel.match!.toLowerCase() == exports[i]['name'].toString().toLowerCase()) {
            // Add matching fields to new array.
            matchedFieldsList.add(fieldViewModel);
            // Stop loop if matching fields found.
            break;
          }
        }
      }
      // Replace exiting provider.
      providerConfigViewModel.fields = matchedFieldsList;
      // Add provider to list.
      matchedProviders.add(provider);
    }
    // Update matching providers.
    dataRequest.providers = matchedProviders;
  }
}
