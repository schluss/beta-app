import 'package:flutter/cupertino.dart';
import 'package:schluss_beta_app/business/impl/plugin_manager_bl_impl.dart';
import 'package:schluss_beta_app/business/interface/plugin_manager_bl.dart';
import 'package:schluss_beta_app/plugins/bkr/bkr.dart';
import 'package:schluss_beta_app/view_model/provider_config_view_model.dart';
import 'package:schluss_beta_app/view_model/request_view_model.dart';

/// BKR plugin business logic.
class BKRPluginManagerBLImpl extends PluginManagerBLImpl implements PluginManagerBL {
  final PluginBKR _pluginBKR = PluginBKR();
  @override
  void onExecutePlugin(
    BuildContext context,
    ProviderConfigViewModel? providerModel,
    Function callback,
  ) {
    _pluginBKR.runPlugin(context, callback, providerModel!.pluginSettings.siteUrl!, providerModel.pluginSettings.trigger!, providerModel.dataProvider!.name!);
  }

  @override
  Future<Map<String, dynamic>> onGetData(String attributeDataList) {
    return _pluginBKR.getExtractedData(attributeDataList);
  }

  @override
  Stream<int> getProgressValue(RequestViewModel requestViewModel) {
    return _pluginBKR.getProgress();
  }

  /// Executes the plugin directly.
  // TODO: need to change the naming and remove [context].
  @override
  void executePluginDirect(
    context,
    Function callBack,
    String siteUrl,
    String trigger, {
    String? pluginName = 'BKR',
    Function(String userName, String password)? storeCredentialCallback,
    String? userName,
    String? password,
  }) {
    throw Exception('Not Implemented');
  }
}
