import 'package:schluss_beta_app/data/interface/common_dao.dart';
import 'package:schluss_beta_app/data/models/notification_model.dart';
import 'package:schluss_beta_app/singleton_injector.dart';
import 'package:schluss_beta_app/view_model/notification_view_model.dart';

/// Notifications status enum.
enum NotificationState {
  active, // Active notifications.
  inactive, // Temporary inactive notifications.
  removed, // Permanently removed notifications.
}

/// Notifications type enum.
enum NotificationType {
  tip, // TIPS notifications.
}

class NotificationBLImpl {
  final CommonDAO _commonDAOImpl = singletonInjector<CommonDAO>();

  Future<void> deleteNotificationsByKey(String titleKey) async {
    var notifications = await getNotifications(notificationType: NotificationType.tip);

    for (var notification in notifications) {
      if (notification.titleKey == titleKey) {
        deleteNotification(notification.id!);
      }
    }

    return;
  }

  Future<bool> deleteNotification(int id) {
    return _commonDAOImpl.deleteById(NotificationModel.getStoreName(), id);
  }

  /// Creates notifications.
  Future<bool> createNotification(NotificationViewModel notificationViewModel) async {
    // To prevent a double, try to get notifications that match the current key.
    var existingItems = await getNotifications(
      notificationType: NotificationType.tip,
      notificationTitleKey: notificationViewModel.titleKey,
      connectionId: notificationViewModel.connectionId,
    );

    // It is already there, so do not create a double.
    if (existingItems.isNotEmpty) {
      return true;
    }

    NotificationModel notificationModel = NotificationModel.fromJson(
      notificationViewModel.toJson(),
    );

    Map<String, dynamic>? model = await _commonDAOImpl.store(
      NotificationModel.getStoreName(),
      notificationModel.toJson(),
    );

    return (model.isNotEmpty ? true : false);
  }

  /// Gets notifications.
  Future<List<NotificationViewModel>> getNotifications({NotificationType? notificationType, String? notificationTitleKey, int connectionId = 0}) async {
    late List<Map<String, dynamic>> list;
    List<NotificationViewModel> notificationResultList = [];

    // Get the all notifications.
    if (notificationType == null && notificationTitleKey == null && connectionId == 0) {
      list = await _commonDAOImpl.getAll(
        NotificationModel.getStoreName(),
      );

      // Get filtered result set.
    } else {
      Map<String, dynamic> filterMap = {};

      // Filter notifications by type.
      if (notificationType != null) {
        String? typeCode = notificationType == NotificationType.tip ? 'T' : null;
        if (typeCode != null) {
          filterMap['type'] = typeCode;
        }
      }

      // Filter by [notificationTitleKey].
      if (notificationTitleKey != null) {
        filterMap['title'] = notificationTitleKey;
      }

      // filter by connectionId
      filterMap['connectionId'] = connectionId;

      list = await _commonDAOImpl.getFilteredData(
        NotificationModel.getStoreName(),
        filterMap,
        FilterType.and,
      );
    }

    // When none found.
    if (list.isEmpty) {
      return notificationResultList;
    }

    // TODO: we could probably improve something in here, making it more efficient
    //  with less castings and conversions. Convert the result so it becomes a
    //  list of [NotificationViewModels].
    for (Map<String, dynamic> notificationMap in list) {
      NotificationModel notificationModel = NotificationModel.fromJson(
        notificationMap,
      );

      NotificationViewModel notificationViewModel = NotificationViewModel.fromJson(
        notificationModel.toJson(),
      );

      notificationResultList.add(notificationViewModel);
    }

    return Future.value(notificationResultList);
  }
}
