import 'dart:convert';

import 'package:schluss_beta_app/business/impl/payload_plugin_manager_bl_impl.dart';
import 'package:schluss_beta_app/business/interface/payload_plugin_manager_bl.dart';

class DefaultPayloadPluginBlImpl extends PayloadPluginManagerBlImpl implements PayloadPluginManagerBl {
  @override
  String processPayload(Map<String, dynamic> dataRequest) {
    var result = {
      'requester': dataRequest['requester'],
      'provider': dataRequest['provider'],
      'scope': dataRequest['scope'],
      'date': dataRequest['date'],
      'shares': dataRequest['shares'],
    };

    return json.encode(result);
  }
}
