import 'package:flutter/cupertino.dart';
import 'package:flutter_dotenv/flutter_dotenv.dart';
import 'package:schluss_beta_app/business/impl/plugin_manager_bl_impl.dart';
import 'package:schluss_beta_app/business/interface/plugin_manager_bl.dart';
import 'package:schluss_beta_app/plugins/verifiedmail/verifiedmail.dart';
import 'package:schluss_beta_app/util/crypto_util.dart';
import 'package:schluss_beta_app/view_model/provider_config_view_model.dart';
import 'package:schluss_beta_app/view_model/request_view_model.dart';

class VerifiedMailPluginManagerBLImpl extends PluginManagerBLImpl implements PluginManagerBL {
  final PluginVerifiedMail _plugin = PluginVerifiedMail();

  @override
  void onExecutePlugin(BuildContext context, ProviderConfigViewModel? providerModel, Function callback) async {
    // Make some functions available to the plugin.
    PluginVerifiedMail.getEnvVar = getEnvVar;
    PluginVerifiedMail.getFromSecureStore = getSecureStoreItem;
    PluginVerifiedMail.storeInSecureStore = storeSecureStoreItem;

    _plugin.runPlugin(
      context,
      callback,
      '',
      '',
      providerModel!.dataProvider!.name!,
    );
  }

  /// Gets an env variable to the plugin.
  String? getEnvVar(String key) {
    if (dotenv.env.containsKey(key)) {
      return dotenv.env[key];
    }

    return '';
  }

  /// Gets from the secure key store.
  Future<String> getSecureStoreItem(String key) {
    return CryptoUtil.getFromKeyStore('verifiedmail.$key');
  }

  /// Stores in the secure key store.
  Future<void> storeSecureStoreItem(String key, String value) {
    return CryptoUtil.storeInKeyStore('verifiedmail.$key', value);
  }

  /// Gets data.
  @override
  Future<Map<String, dynamic>> onGetData(String attributeDataList) {
    return _plugin.getExtractedData(attributeDataList);
  }

  /// Gets the progress value.
  @override
  Stream<int> getProgressValue(RequestViewModel requestViewModel) {
    return _plugin.getProgress();
  }

  /// Executes plugin directly calling the function.
  // TODO: need to change the naming and remove [context].
  @override
  void executePluginDirect(
    context,
    Function callBack,
    String siteUrl,
    String trigger, {
    String? pluginName = 'Verified Mail',
    Function(String userName, String password)? storeCredentialCallback,
    String? userName,
    String? password,
  }) {
    throw Exception('Not Implemented');
  }
}
