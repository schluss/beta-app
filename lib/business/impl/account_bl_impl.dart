import 'dart:async';
import 'dart:convert';
import 'dart:io';

import 'package:ext_storage/ext_storage.dart';
import 'package:flutter_archive/flutter_archive.dart';
import 'package:intl/intl.dart';
import 'package:logger/logger.dart';
import 'package:openpgp/openpgp.dart' as openpgp;
import 'package:path_provider/path_provider.dart';
import 'package:permission_handler/permission_handler.dart';
import 'package:schluss_beta_app/business/impl/external_storage_bl_impl.dart';
import 'package:schluss_beta_app/business/impl/gateway_bl_impl.dart';
import 'package:schluss_beta_app/business/impl/notification_bl_impl.dart';
import 'package:schluss_beta_app/constants/environment_constants.dart';
import 'package:schluss_beta_app/data/interface/common_dao.dart';
import 'package:schluss_beta_app/data/models/app_data_model.dart';
import 'package:schluss_beta_app/data/models/login_attempt_model.dart';
import 'package:schluss_beta_app/data/models/settings_data_model.dart';
import 'package:schluss_beta_app/database_config.dart';
import 'package:schluss_beta_app/services/interface/socket_service.dart';
import 'package:schluss_beta_app/singleton_injector.dart';
import 'package:schluss_beta_app/util/color_picker_util.dart';
import 'package:schluss_beta_app/util/crypto_util.dart';
import 'package:schluss_beta_app/util/env_util.dart';
import 'package:schluss_beta_app/view_model/login_result_view_model.dart';
import 'package:schluss_beta_app/view_model/notification_view_model.dart';
import 'package:schluss_beta_app/view_model/settings_view_model.dart';
import 'package:schluss_beta_app/view_model/vault_switch_item_view_model.dart';
import 'package:session_next/session_next.dart';
import 'package:shared_preferences/shared_preferences.dart';

class AccountBLImpl {
  final CommonDAO _commonDAOImpl = singletonInjector<CommonDAO>();
  final ExternalStorageBLImpl _externalStorageBL = singletonInjector<ExternalStorageBLImpl>();
  final GatewayBLImpl _gatewayBL = singletonInjector<GatewayBLImpl>();
  final NotificationBLImpl _notificationBL = singletonInjector<NotificationBLImpl>();
  final EnvUtil _envUtil = EnvUtil();
  final Logger _logger = singletonInjector<Logger>();
  final SocketService _socketService = singletonInjector<SocketService>();

  final AppDatabaseConfig _appDatabaseConfig = singletonInjector<AppDatabaseConfig>();

  /// Generates a new Schluss vault.
  Future<bool> generateSchlussVault(String pincode) async {
    // Create hash value from the pin code.
    String pincodeHash = await CryptoUtil.createPBKDF2Hash(pincode);

    // Store pin code hash in secure keystore, to be able to compare to later.
    await CryptoUtil.storePincodeHash(pincodeHash);

    // Generate a random key, used to encrypt/decrypt the database.
    String randomKey = CryptoUtil.generateRandomKey();

    // Store the random key.
    await CryptoUtil.storeRandomKey(randomKey);

    // Generate RSA hey pair protected with the random key.
    openpgp.KeyPair keyPair = await CryptoUtil.createKeyPair();

    // Initialize database with the random key.
    bool dbStatus = await _appDatabaseConfig.initiateDatabase(randomKey);

    if (!dbStatus) {
      _logger.e(
        'Schluss vault generation failed: database initialization did not succeed',
      );
      return false;
    }

    // Store the app data.
    AppDataModel appDataModel = AppDataModel(
      publicKey: keyPair.publicKey,
      timestamp: DateTime.now().millisecondsSinceEpoch,
    );

    await _commonDAOImpl.store(
      AppDataModel.getStoreName(),
      appDataModel.toJson(),
    );

    // Create and store default settings.
    bool status = await createDefaultSettings();

    if (!status) {
      _logger.e(
        'Schluss vault generation partially successful: default settings could not be stored',
      );
    }

    // Validate connection to gateway. Registers a new account in gateway,
    // if does not exist.
    await _gatewayBL.validateGatewayConnection();

    // Validate connection to external storage gateway.
    // Register a new account in gateway, if does not exist.
    await _externalStorageBL.validateGatewayConnection();

    return true;
  }

  /// Updates vault secure keys to new pin code.
  Future<bool> updateSchlussVaultKey(String? pincode, String? oldPincode) async {
    // Hash value for the old pin code.
    String oldPincodeHash = await CryptoUtil.getPBKDF2Hash(oldPincode!);

    String storedPincodeHash = await CryptoUtil.getPincodeHash();

    // When the stored hash does not match the hash of the old (just entered) pin code.
    if (oldPincodeHash.compareTo(storedPincodeHash) != 0) {
      _logger.e(
        'updateSchlussVaultKey failed because pin code hash does not match',
      );
      return false;
    }

    // Create hash for new pin code.
    String pincodeHash = await CryptoUtil.createPBKDF2Hash(pincode!);

    // Store pin code hash in secure keystore.
    await CryptoUtil.storePincodeHash(pincodeHash);

    return true;
  }

  /// Detects if the app is reinstalled (hasRunBefore = not set), then clear the Secure Keystore.
  ///
  /// If we would not do this, IOS users would not be able to create a new vault,
  /// because key chain is persisted in IOS (iCloud automatic backups),
  /// see: [https://stackoverflow.com/questions/4747404/delete-keychain-items-when-an-app-is-uninstalled].
  Future<void> clearSecureStorageOnReinstall() async {
    String key = 'hasRunBefore';
    SharedPreferences sharedPreferences = await SharedPreferences.getInstance();

    if (!sharedPreferences.containsKey(key) || !sharedPreferences.getBool(key)!) {
      await CryptoUtil.removeAllKeys();
      await sharedPreferences.setBool(key, true);
    }
  }

  /// Validates the vault available status.
  Future<bool> validateVaultAvailability() async {
    // Make sure the keystore is cleared, if this is a new install, before we
    // continue the availability check.
    await clearSecureStorageOnReinstall();

    bool isAvailable = false;

    // Retrieve private key from secure key store.
    String privateKey = await CryptoUtil.getPrivateKeyFromSecureStore();

    // Check availability of any private keys in the secure store. This will
    // assume vault can be available.
    if (privateKey.isNotEmpty) {
      isAvailable = true;
    }

    return Future.value(isAvailable);
  }

  /// Checks if the provided pin code is correct
  Future<bool> validatePin(String pincode) async {
    var pincodeHash = await CryptoUtil.getPBKDF2Hash(pincode);
    var storedPincodeHash = await CryptoUtil.getPincodeHash();

    // When hash does not match.
    if (pincodeHash.compareTo(storedPincodeHash) != 0) {
      return false;
    } else {
      return true;
    }
  }

  /// Logs in the user
  Future<LoginResultViewModel?> validateLogin(String pincode) async {
    // Validate the pincode
    if (!await validatePin(pincode)) {
      // Update the login attempt count.
      await CryptoUtil.updateLoginAttemptCount();

      return null;
    }

    // Reset login attempts.
    await CryptoUtil.removeLoginAttempt();

    // Get other data to be returned after a successful login.
    String privateKey = await CryptoUtil.getPrivateKeyFromSecureStore();
    String randomKey = await CryptoUtil.getRandomKey();

    // Initialize the database.
    await _appDatabaseConfig.initiateDatabase(randomKey);

    // Setting the vault as owner.
    // TODO: need to validate business scenario whether we need owner as the
    //  main vault or any other last saved vaults can be shown in after login.
    changeVaultOwner(0);

    // Get the all app settings.
    List<Map<String, dynamic>> list = await _commonDAOImpl.getAll(
      AppDataModel.getStoreName(),
    );
    // When no settings found, something is wrong, but try to continue.
    if (list.isEmpty || !list.last.containsKey('publicKey')) {
      throw ('AppSettings not found. We must assume the database is corrupt. Therefore the app cannot continue its normal operation');
    }

    String publicKey = list.last['publicKey'];

    // Sanity check: validates if public and private key are able to do a simple crypto operation.
    bool status = await CryptoUtil.validateKeyPair(publicKey, privateKey);

    // When key pair does not validate, login also failed.
    if (!status) {
      throw Exception('Login validation failed.');
    }

    // Validate connection to gateway. Registers a new account in gateway, if does not exist.
    _gatewayBL.validateGatewayConnection();

    // Register a new account in external storage, if does not exist.
    _externalStorageBL.validateGatewayConnection();

    // TODO: might need to verify whether socket registration flow should be here
    //  validates secret data availability for the socket registration.
    bool isSecretDataAvailable = await _gatewayBL.validateSecretDataAvailability();
    if (isSecretDataAvailable) {
      await _socketService.connect();
    }

    return Future.value(LoginResultViewModel(
      privateKey: privateKey,
      publicKey: list.last['publicKey'],
      randomKey: randomKey,
    ));
  }

  /// Removes all keys in secure keystore and database files.
  Future<bool> deleteVault() async {
    // Disconnect from socket server.
    _socketService.disconnect();

    // Remove all keys/data from the secure keystore.
    bool keyStatus = await CryptoUtil.removeAllKeys();

    // Remove stored database files.
    // TODO: remove stored files attribute values.
    bool dbStatus = await _commonDAOImpl.deleteDatabase();

    // Remove stored shared preferences.
    SharedPreferences sharedPreferences = await SharedPreferences.getInstance();
    bool sharedPrefsStatus = await sharedPreferences.clear();

    return keyStatus && dbStatus && sharedPrefsStatus ? true : false;
  }

  /// Gets settings from settings store.
  Future<SettingsViewModel> getSettings() async {
    List<Map<String, dynamic>> modelList = await _commonDAOImpl.getAll(
      SettingsDataModel.getStoreName(),
    );

    if (modelList.isEmpty) {
      _logger.w('Settings not found');

      return SettingsViewModel();
    }

    return Future.value(SettingsViewModel.fromJson(modelList.first));
  }

  /// Changes stewardship mode store in settings store.
  Future<bool> changeStewardshipMode(bool? status) async {
    var result = await _commonDAOImpl.updateFirst(SettingsDataModel.getStoreName(), {'stewardshipMode': status});

    if (!result) {
      return false;
    }

    // Load/remove the stewardship tip on dashboard.
    List<NotificationViewModel> notificationList = await _notificationBL.getNotifications(
      notificationType: NotificationType.tip,
    );

    var tip = notificationList.cast<NotificationViewModel?>().firstWhere(
          (element) => element!.titleKey == 'tipStewardshipTitleTxt',
          orElse: () => null,
        );

    if (status == true && tip == null) {
      await createAppointStewardTip();
    } else if (status == false && tip != null) {
      await _notificationBL.deleteNotificationsByKey('tipStewardshipTitleTxt');
    }

    return true;
  }

  /// Changes data sharing mode store in settings store.
  Future<bool> changeDataSharingMode(bool? status) async {
    var result = await _commonDAOImpl.updateFirst(SettingsDataModel.getStoreName(), {'dataSharingMode': status});

    if (!result) {
      return false;
    }

    // Load/remove the data sharing tip on dashboard.
    List<NotificationViewModel> notificationList = await _notificationBL.getNotifications(
      notificationType: NotificationType.tip,
    );

    var tip = notificationList.cast<NotificationViewModel?>().firstWhere(
          (element) => element!.titleKey == 'tipDataSharingTitleTxt',
          orElse: () => null,
        );

    if (status == true && tip == null) {
      await createDataSharingTip();
    } else if (status == false && tip != null) {
      await _notificationBL.deleteNotificationsByKey('tipDataSharingTitleTxt');
    }

    return true;
  }

  /// Changes test-mode store in settings store.
  Future<bool> changeTestMode(bool? status) async {
    var result = await _commonDAOImpl.updateFirst(SettingsDataModel.getStoreName(), {'testMode': status});

    return result;
  }

  /// Changes vault owner status in settings store.
  Future<bool> changeVaultOwner(int? contractId) async {
    // Store current user contract details to session.
    SessionNext().set<int?>('connectionId', contractId);

    return SessionNext().get<int?>('connectionId') == 0 ? true : false;
  }

  /// Changes tour_data-request_on_open mode store in settings store.
  Future<bool> changeTourDataRequestOpen(bool status) async {
    var result = await _commonDAOImpl.updateFirst(SettingsDataModel.getStoreName(), {'tourDataRequestOpen': status});

    return result;
  }

  /// Returns the global ENV mode setting (development or production).
  Future<String> getEnvMode() async {
    try {
      return await EnvUtil().getEnvModeValue();
    } catch (e) {
      return EnvironmentConstants.development;
    }
  }

  /// Returns whether stewardship mode is on [true] or off [false].
  Future<bool> isStewardshipModeOn() async {
    try {
      // When mode is production, this is always overridden [false].
      //if (await EnvUtil().getEnvModeValue() == EnvironmentConstants.production) {
      //  return false;
      // }

      CommonDAO dao = singletonInjector<CommonDAO>();
      List<Map<String, dynamic>> settingsList;
      SettingsViewModel settings;

      settingsList = await dao.getAll(SettingsDataModel.getStoreName());

      // No setting available, assume the default, get from .env, otherwise return [false].
      if (settingsList.isEmpty) {
        return await EnvUtil().getEnvStewardshipModeModeValue();
      }

      settings = SettingsViewModel.fromJson(settingsList.first);

      return settings.stewardshipMode;
    } catch (e) {
      singletonInjector<Logger>().e(
        'Could not retrieve stewardship mode setting, returning a default \'false\' value',
      );
      return false;
    }
  }

  /// Returns whether data sharing mode is on [true] or off [false].
  Future<bool> isDataSharingModeOn() async {
    try {
      // When mode is production, this is always overridden [false].
      if (await EnvUtil().getEnvModeValue() == EnvironmentConstants.production) {
        return false;
      }

      CommonDAO dao = singletonInjector<CommonDAO>();
      List<Map<String, dynamic>> settingsList;
      SettingsViewModel settings;

      settingsList = await dao.getAll(SettingsDataModel.getStoreName());

      // No setting available: assume the default, get from .env, otherwise return [false].
      if (settingsList.isEmpty) {
        return await EnvUtil().getEnvDataSharingModeModeValue();
      }

      settings = SettingsViewModel.fromJson(settingsList.first);

      return settings.dataSharingMode;
    } catch (e) {
      singletonInjector<Logger>().e(
        'Could not retrieve data sharing mode setting,returning a default \'false\' value',
      );
      return false;
    }
  }

  /// Returns whether test-mode is on [true] or off [false].
  Future<bool> isTestModeOn() async {
    try {
      // When mode is production, this is always overridden [false].
      if (await EnvUtil().getEnvModeValue() == EnvironmentConstants.production) {
        return false;
      }

      CommonDAO dao = singletonInjector<CommonDAO>();
      List<Map<String, dynamic>> settingsList;
      SettingsViewModel settings;

      settingsList = await dao.getAll(SettingsDataModel.getStoreName());

      // No setting available, assume the default, get from .env, otherwise return [false].
      if (settingsList.isEmpty) {
        return await EnvUtil().getEnvTestModeValue();
      }

      settings = SettingsViewModel.fromJson(settingsList.first);

      return settings.testMode;
    } catch (e) {
      singletonInjector<Logger>().e(
        'Could not retrieve TestMode setting, returning a default \'false\' value',
      );
      return false;
    }
  }

  /// Returns whether the user is in its own vault or not
  bool isInOwnVault() {
    return SessionNext().get<int?>('connectionId') == 0 ? true : false;
  }

  /// Returns the full name of the vault the user is currently in
  String getCurrentVaultName() {
    if (!SessionNext().containsKey('switchConnections') || SessionNext().get<List<VaultSwitchItemViewModel>>('switchConnections')!.isEmpty) {
      return '';
    }

    return SessionNext().get<List<VaultSwitchItemViewModel>>('switchConnections')!.firstWhere((element) => element.connectionId == SessionNext().get<int?>('connectionId')).name;
  }

  /// Returns whether the data request tour should be displayed on = [true] or not [false].
  Future<bool?> isTourDataRequestOn() async {
    try {
      CommonDAO dao = singletonInjector<CommonDAO>();
      List<Map<String, dynamic>> settingsList;
      SettingsViewModel settings;

      settingsList = await dao.getAll(SettingsDataModel.getStoreName());

      // No setting available, assume the default, get from .env, otherwise return [false].
      if (settingsList.isEmpty) {
        return true;
      }

      settings = SettingsViewModel.fromJson(settingsList.first);

      return settings.tourDataRequestOpen;
    } catch (e) {
      singletonInjector<Logger>().e(
        'Could not retrieve tourDataRequestOpen setting, returning a default \'true\' value',
      );
      return true;
    }
  }

  /// Creates and stores the default settings
  Future<bool> createDefaultSettings() async {
    // Get the given env var settings
    var stewardShipMode = await _envUtil.getEnvStewardshipModeModeValue();
    var dataSharingMode = await _envUtil.getEnvDataSharingModeModeValue();
    var testMode = await _envUtil.getEnvTestModeValue();

    var defaultSettings = SettingsDataModel(
      stewardshipMode: stewardShipMode,
      dataSharingMode: dataSharingMode,
      testMode: testMode,
      iconColor: ColorPickerUtil().pickColor().value.toString(),
      tourDataRequestOpen: true,
    );

    // process the changes so additional cards and tips and other things are set in place
    await changeStewardshipMode(stewardShipMode);
    await changeDataSharingMode(dataSharingMode);
    await changeTestMode(testMode);

    await _commonDAOImpl.store(
      SettingsDataModel.getStoreName(),
      defaultSettings.toJson(),
    );

    return true;
  }

  /// Checks previous log-in attempt.
  ///
  /// 0 => returns for enable user to login.
  /// 1 => returns no of sec  for user need to wait.
  Future<int> validateLoginAttempts() async {
    LoginAttempt loginAttempt = await CryptoUtil.getLoginAttempt();

    // Current incorrect login attempt count.
    int currentAttemptCount = loginAttempt.loginAttemptCount;

    // Time duration for the last incorrect login attempt.
    int lastTimesInt = loginAttempt.loginAttemptTimestamp!;

    // Get the date time.
    DateTime lastTimestamp = DateTime.fromMillisecondsSinceEpoch(lastTimesInt);

    // If attempt count less than 3.
    if (currentAttemptCount < 3) {
      return Future.value(0);
    }

    // Calculate waiting time.
    int currentWaitingTime = currentAttemptCount * 10;

    // Check lock period is over.
    DateTime unlockTimeStamp = lastTimestamp.add(
      Duration(seconds: currentWaitingTime),
    );

    // UnlockTimeStamp = last login time + (current attempts * 10).
    var diffToUnlockTime = DateTime.now().difference(unlockTimeStamp);
    _logger.d(
      'Different from unlock time ${diffToUnlockTime.inSeconds}',
    );

    if (diffToUnlockTime.inSeconds > 0) {
      // Get positive value means unlock time already pass.
      // User can log. Unlock time already passed.
      return Future.value(0);
    } else {
      // Negative value and 0 means you need to wait some time. User should wait.
      return Future.value(diffToUnlockTime.inSeconds);
    }
  }

  /// Exports vault data to a zip file.
  ///
  /// Including the followings:
  /// 1. Secure keystore data.
  /// 2. Database file.
  // TODO: include files downloaded during data requests.
  Future<String> exportVaultData(String? passphrase) async {
    // Check file permissions granted by user,
    // if not granted app will request permissions.
    if (await Permission.storage.request().isGranted) {
      String configFilePath = await writeConfigData(passphrase!);
      String databaseFilePath = await _appDatabaseConfig.getDatabaseFilePath();
      var internalDir = await getApplicationDocumentsDirectory();

      // File list for include into zip file.
      final files = [File(configFilePath), File(databaseFilePath)];

      // Get download directory path.
      // If android, directory will be downloads folder.
      // If IOS, directory will internal app directory.
      String? directory;

      if (Platform.isAndroid) {
        final androidDownloadDir = await ExtStorage.getExternalStoragePublicDirectory(
          ExtStorage.DIRECTORY_DOWNLOADS,
        );

        directory = androidDownloadDir;
      } else {
        directory = internalDir.path + Platform.pathSeparator;
      }

      String dateFormatted = DateFormat('yyyy-MM-dd', 'nl').format(
        DateTime.now(),
      );

      final zipFile = File(
        '${directory!}${Platform.pathSeparator}SchlussExport-$dateFormatted.zip',
      );

      await ZipFile.createFromFiles(
        sourceDir: internalDir,
        files: files,
        zipFile: zipFile,
      );

      await removeConfigFiles();

      return zipFile.path;
    } else {
      throw Exception('File Access Not Granted');
    }
  }

  /// Copies config data from secure store.
  ///
  /// JSON file including.
  /// 1. Private Key  - Private key generated during registration.
  /// 2. Public Key - Public key generated during registration.
  /// 3. Salt Key - Salt key generated for hashing.
  /// 4. Random Key - Random key used to decrypt data.
  Future<String> writeConfigData(String passphrase) async {
    String privateKey = await CryptoUtil.getPrivateKeyFromSecureStore();
    String? publicKey = SessionNext().get<String>('publicKey');
    String hashValue = await CryptoUtil.getPBKDF2Hash(passphrase);
    String? randomKey = await CryptoUtil.getRandomKey();
    String saltKey = await CryptoUtil.getSaltKeyFromSecureStore();

    var config = {
      'privateKey': privateKey,
      'publicKey': publicKey,
      'randomKey': randomKey,
      'saltKey': saltKey,
    };

    String configString = json.encode(config);

    String configEncrypted = await CryptoUtil.encryptSymmetric(
      configString,
      hashValue,
    );

    final Directory directory = await getApplicationDocumentsDirectory();
    final File file = File('${directory.path}/config');
    await file.writeAsString(configEncrypted);

    return file.path;
  }

  /// Removes config files created during exporting data.
  Future<void> removeConfigFiles() async {
    try {
      final Directory directory = await getApplicationDocumentsDirectory();
      final File file = File('${directory.path}/config');

      await file.delete();
    } catch (e) {
      throw Exception('Files cannot be deleted');
    }
  }

  /// Creates verify identity notification.
  Future<void> createVerifyIdentityTip() async {
    var connectionId = SessionNext().get<int>('connectionId') ?? 0;

    NotificationViewModel model = NotificationViewModel(
      connectionId,
      titleKey: 'tipIdentityVerifyTitleTxt',
      descriptionKey: 'tipIdentityVerifyDescTxt',
      routeNameKey: '/identity/add',
      isClosable: true,
      status: NotificationState.active,
      type: NotificationType.tip,
      timestamp: DateTime.now().millisecondsSinceEpoch,
    );

    await _notificationBL.createNotification(model);
  }

  /// Creates appoint steward notification.
  Future<void> createAppointStewardTip() async {
    var connectionId = SessionNext().get<int>('connectionId') ?? 0;

    NotificationViewModel model = NotificationViewModel(
      connectionId,
      titleKey: 'tipStewardshipTitleTxt',
      descriptionKey: 'tipStewardshipDescTxt',
      routeNameKey: '/stewardship/owner/info',
      isClosable: false,
      status: NotificationState.active,
      type: NotificationType.tip,
      timestamp: DateTime.now().millisecondsSinceEpoch,
    );

    await _notificationBL.createNotification(model);
  }

  /// Creates data sharing notification.
  Future<void> createDataSharingTip() async {
    var connectionId = SessionNext().get<int>('connectionId') ?? 0;

    NotificationViewModel model = NotificationViewModel(
      connectionId,
      titleKey: 'tipDataSharingTitleTxt',
      descriptionKey: 'tipDataSharingDescTxt',
      routeNameKey: '/data-sharing/info',
      isClosable: false,
      status: NotificationState.active,
      type: NotificationType.tip,
      timestamp: DateTime.now().millisecondsSinceEpoch,
    );

    await _notificationBL.createNotification(model);
  }
}
