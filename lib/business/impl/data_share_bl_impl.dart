import 'dart:async';
import 'dart:convert';

import 'package:flutter_dotenv/flutter_dotenv.dart';
import 'package:schluss_beta_app/business/impl/contract_manager_bl_impl.dart';
import 'package:schluss_beta_app/business/impl/gateway_bl_impl.dart';
import 'package:schluss_beta_app/business/impl/my_data_bl_impl.dart';
import 'package:schluss_beta_app/data/impl/contract_store_dao_impl.dart';
import 'package:schluss_beta_app/data/interface/common_dao.dart';
import 'package:schluss_beta_app/data/models/connect_request_model.dart';
import 'package:schluss_beta_app/data/models/contract/contract_model.dart';
import 'package:schluss_beta_app/data/models/contract/contract_requester.dart';
import 'package:schluss_beta_app/services/interface/socket_service.dart';
import 'package:schluss_beta_app/singleton_injector.dart';
import 'package:schluss_beta_app/ui/dashboard/stewardship/owner_define_access_timing_page_content.dart';
import 'package:schluss_beta_app/util/color_picker_util.dart';
import 'package:schluss_beta_app/util/crypto_util.dart';
import 'package:schluss_beta_app/view_model/gateway_viewmodel.dart';
import 'package:session_next/session_next.dart';
import 'package:uuid/uuid.dart';

class DataShareBLImpl {
  final CommonDAO _commonDAOImpl = singletonInjector<CommonDAO>();
  final GatewayBLImpl _gatewayBL = singletonInjector<GatewayBLImpl>();
  final SocketService _socketService = singletonInjector<SocketService>();
  final Uuid? uuid = singletonInjector<Uuid>();
  final ContractManagerBLImpl _contractManagerBL = singletonInjector<ContractManagerBLImpl>();

  final MyDataBLImpl _myDataBL = singletonInjector<MyDataBLImpl>();
  final ContractStoreDAOImpl _contractStore = singletonInjector<ContractStoreDAOImpl>();

  // TODO: temporary.
  Map dataSenderInfo = {};

  /// Generates QR link for connect user.

  Future<String> generateQRLink(String endpointUrl) async {
    // Get stored firstname and lastname
    var firstName = await _myDataBL.getPersonalInfoValue('MijnOverheid', 'FirstNames');
    var lastName = await _myDataBL.getPersonalInfoValue('MijnOverheid', 'Surname');

    GatewayViewModel gatewayViewModel = await _gatewayBL.getConnectionData(firstName, lastName);

    if (gatewayViewModel.location != '' && gatewayViewModel.token != '') {
      // Make sure all the params in the generated url is properly encoded.
      String locationEncoded = Uri.encodeComponent(gatewayViewModel.location!);
      String tokenEncoded = Uri.encodeComponent(gatewayViewModel.token!);

      String deeplinkURL = dotenv.env['DEEPLINK_URL']!;
      String connectUserURL = deeplinkURL + endpointUrl; // dotenv.env['DEEPLINK_CONNECT_USER_ENDPOINT']!;
      var connectionLink = '$connectUserURL/$locationEncoded/$tokenEncoded';
      return connectionLink;
    }
    throw Exception('Data Share Location cannot be generated for QR');
  }

  /// Processes QR link and share the connection waiting signal via socket.

  Future<GatewayViewModel?> processQRData(String? qrData) async {
    List<String> pathsList = Uri.parse(qrData!).pathSegments;

    // Make sure all the params retrieved from the url are properly decoded.
    String location = Uri.decodeComponent(pathsList.elementAt(2));
    String token = Uri.decodeComponent(pathsList.elementAt(3));

    GatewayViewModel gatewayViewModel = await _gatewayBL.requestUserSocketData(
      location,
      token,
    );

    var message = {
      'type': 'connectWait',
      'sessionId': gatewayViewModel.sessionId,
      'socketUrl': gatewayViewModel.socketUrl,
    };

    bool status = await _socketService.shareMessage(
      gatewayViewModel.clientKey!,
      json.encode(message),
    );
    if (status) {
      // TODO: temporary storing data sender info.
      dataSenderInfo[gatewayViewModel.sessionId] = {
        'firstName': gatewayViewModel.firstName,
        'lastName': gatewayViewModel.lastName,
        'clientKey': gatewayViewModel.clientKey,
        'clientPublicKey': gatewayViewModel.clientPublicKey,
      };

      return gatewayViewModel;
    }

    return null;
  }

  /// Validates session exists when messages receives from the socket connection.

  Future<bool> validateSessionId(int? sessionId) async {
    Map<String, dynamic>? dataMap = await _commonDAOImpl.getDataFromId(
      ConnectRequestModel.getStoreName(),
      sessionId!,
    );

    if (dataMap != null && dataMap.isNotEmpty) {
      return true;
    }

    return false;
  }

  /// Accepts data share request from other party to receive data.

  Future<bool> acceptingDataShareRequest(
    GatewayViewModel? gatewayViewModel,
  ) async {
    // Get stored firstname and lastname
    var firstName = await _myDataBL.getPersonalInfoValue('MijnOverheid', 'FirstNames');
    var lastName = await _myDataBL.getPersonalInfoValue('MijnOverheid', 'Surname');

    Map? secretData = await (CryptoUtil.getGatewaySecretData());

    if (secretData == null ||
        !secretData.containsKey('key') ||
        !secretData.containsKey(
          'secret',
        )) {
      return false;
    }

    var message = {
      'type': 'connectResponse',
      'sessionId': gatewayViewModel!.sessionId,
      'clientKey': secretData['key'],
      'firstName': firstName,
      'lastName': lastName,
      'clientPublicKey': SessionNext().get<String>('publicKey'),
    };
    bool status = await _socketService.shareMessage(
      gatewayViewModel.clientKey!,
      json.encode(message),
    );

    return status;
  }

  /// Accepts data share request from other party to send data.

  Future<bool> acceptingDataSendRequest(GatewayViewModel? gatewayViewModel) async {
    bool status;

    ContractModel contractModel = await saveContract(
      gatewayViewModel!,
      // When dataaccess type = afterpassed it is a stewardship type connection, otherwise it is a normal connection with immediate data sharing
      stewardshipType: gatewayViewModel.dataAccessType == DataAccessType.afterPassed ? StewardshipType.sender : null,
    );

    //Posting attributes to gateway to create links
    var shares = await _gatewayBL.postGatewayLinkShares(
      gatewayViewModel.sharedAttributesIds!,
      contractModel.id!,
    );

    // Get all the shareIds from the outgoingshares, required for trigger registration
    List<String?> shareIds = shares.map((share) => share.shareId).toList();

    // Creating a trigger with attributes
    String? triggerKey = await _gatewayBL.registerTrigger(shareIds, gatewayViewModel.clientKey!, contractModel.id!);
    gatewayViewModel.triggerKey = triggerKey;

    // Updating trigger id in contract store
    _contractStore.updateContractData(contractModel.id!, {'triggerKey': triggerKey});

    var message = {
      'type': 'connectAccept',
      'sessionId': gatewayViewModel.sessionId,
      'dataAccessType': gatewayViewModel.dataAccessType != null && gatewayViewModel.dataAccessType == DataAccessType.afterPassed ? DataAccessType.afterPassed.name : DataAccessType.immediately.name,
      'triggerKey': triggerKey,
    };

    status = await _socketService.shareMessage(
      gatewayViewModel.clientKey!,
      json.encode(message),
    );

    // Share attributes selected in stewardship process.
    if (gatewayViewModel.sharedAttributesIds!.isNotEmpty) {
      status = await _gatewayBL.sendGatewayShares(shares, contractModel.id!, true);
    }

    //TODO : may need to validate this line. In which scenarios
    // we should remove connect request by validating [status] field.
    status = await removeConnectRequest(gatewayViewModel.sessionId);

    return status;
  }

  /// Processes the data accepting response from data owner.

  Future<GatewayViewModel?> processDataAcceptResponse(GatewayViewModel? gatewayViewModel) async {
    if (dataSenderInfo[gatewayViewModel!.sessionId] != null) {
      //TODO : Quite ugly merging data from dataSenderInfo and gatewayViewModel
      Map<String, dynamic> map = dataSenderInfo[gatewayViewModel.sessionId];
      map['triggerKey'] = gatewayViewModel.triggerKey!;
      map['dataAccessType'] = gatewayViewModel.dataAccessType != null ? gatewayViewModel.dataAccessType!.name : DataAccessType.immediately;

      gatewayViewModel = GatewayViewModel.fromJson(
        map,
      );
      await saveContract(gatewayViewModel, stewardshipType: gatewayViewModel.dataAccessType == DataAccessType.afterPassed ? StewardshipType.receiver : null);

      return gatewayViewModel;
    }

    return null;
  }

  /// Stores the contract in the database.
  Future<ContractModel> saveContract(GatewayViewModel gatewayViewModel, {StewardshipType? stewardshipType}) async {
    // Get the users' personal gateway URL
    var gatewayUrl = dotenv.env['UGW_URL'];

    // Create the contract to store.
    ContractModel contractModel = ContractModel(
      contractId: uuid!.v4(),
      date: DateTime.now().millisecondsSinceEpoch,
      version: '0.0.1',
      requester: Requester(
        name: '${gatewayViewModel.firstName} ${gatewayViewModel.lastName}',
      ),
      notification: true,
      type: ConnectionType.user,
      stewardshipType: stewardshipType,
      clientKey: gatewayViewModel.clientKey,
      gatewayUrl: gatewayUrl,
      iconColor: ColorPickerUtil().pickColor().value.toString(),
      clientPublicKey: gatewayViewModel.clientPublicKey,
      isReleased: false,
      dataAccessType: gatewayViewModel.dataAccessType ?? DataAccessType.immediately,
      triggerKey: gatewayViewModel.triggerKey,
    );

    // Store the contract.
    return await _contractManagerBL.storeContract(contractModel);
  }

  /// Removes connect request.
  Future<bool> removeConnectRequest(int? sessionId) async {
    bool status = await _commonDAOImpl.deleteById(
      ConnectRequestModel.getStoreName(),
      sessionId,
    );

    return status;
  }
}
