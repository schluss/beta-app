import 'dart:async';
import 'dart:convert';

import 'package:logger/logger.dart';
import 'package:schluss_beta_app/business/impl/qr_config_bl_impl.dart';
import 'package:schluss_beta_app/business/interface/payload_plugin_manager_bl.dart';
import 'package:schluss_beta_app/data/interface/common_dao.dart';
import 'package:schluss_beta_app/data/models/attribute_model.dart';
import 'package:schluss_beta_app/data/models/attribute_version_model.dart';
import 'package:schluss_beta_app/data/models/contract/contract_model.dart';
import 'package:schluss_beta_app/data/models/contract/contract_provider.dart';
import 'package:schluss_beta_app/data/models/contract/contract_requester.dart';
import 'package:schluss_beta_app/data/models/incoming_share_model.dart';
import 'package:schluss_beta_app/data/models/request_data_model.dart';
import 'package:schluss_beta_app/services/impl/payload_service_impl.dart';
import 'package:schluss_beta_app/services/interface/config_service.dart';
import 'package:schluss_beta_app/singleton_injector.dart';
import 'package:schluss_beta_app/util/color_picker_util.dart';
import 'package:schluss_beta_app/util/crypto_util.dart';
import 'package:schluss_beta_app/view_model/data_request_config_view_model.dart';
import 'package:schluss_beta_app/view_model/provider_config_view_model.dart';
import 'package:schluss_beta_app/view_model/request_view_model.dart';
import 'package:session_next/session_next.dart';
import 'package:uuid/uuid.dart';

class RequestManagerBLImpl {
  final CommonDAO _commonDAOImpl = singletonInjector<CommonDAO>();
  final QRConfigurationBLImpl _configBL = singletonInjector<QRConfigurationBLImpl>();
  final ConfigurationService _configService = singletonInjector<ConfigurationService>();
  final Uuid? uuid = singletonInjector<Uuid>();
  final Logger _logger = singletonInjector<Logger>();

  /// Stores the request.
  Future<RequestViewModel> storeRequest(RequestViewModel requestModel) async {
    RequestDataModel requestDataModel;

    var connectionId = SessionNext().get<int>('connectionId') ?? 0;

    requestDataModel = RequestDataModel.fromViewModel(connectionId, requestModel);

    Map<String, dynamic>? map = await _commonDAOImpl.store(
      RequestDataModel.getStoreName(),
      requestDataModel.toJson(),
    );

    return RequestViewModel.fromJson(map);
  }

  /// Checks if token is already used, when scanning QR code.
  Future<RequestViewModel?> findDuplicateQR(String token, String scope, String settingsUrl) async {
    RequestViewModel? requestViewModel;

    var connectionId = SessionNext().get<int>('connectionId') ?? 0;

    // Check [token], [scope] & [settingUrl] to identify scanning a duplicate.
    Map<String, dynamic> filterMap = {
      'connectionId': connectionId,
      'token': token,
      'scope': scope,
      'settings_url': settingsUrl,
    };

    final matchingRequests = await _commonDAOImpl.getFilteredData(
      RequestDataModel.getStoreName(),
      filterMap,
      FilterType.and,
    );

    // When the same request is already there.
    if (matchingRequests.isNotEmpty) {
      _logger.d('QR code duplicated!');

      // Get the latest request is enough.
      requestViewModel = RequestViewModel.fromJson(matchingRequests.first);

      return requestViewModel;
    }
    return requestViewModel;
  }

  /// Removes the request.
  Future<bool> removeRequest(RequestViewModel requestModel) async {
    await _commonDAOImpl.update(
      RequestDataModel.getStoreName(),
      requestModel.id!,
      {'status': 'D'},
    );

    return Future.value(true);
  }

  /// Gets the all pending data requests.
  Future<List<RequestViewModel>> getPendingRequestList() async {
    List<RequestViewModel> list = [];

    var connectionId = SessionNext().get<int>('connectionId') ?? 0;

    final pendingRequests = await _commonDAOImpl.getFilteredData(
      RequestDataModel.getStoreName(),
      {
        'status': 'P',
        'connectionId': connectionId,
      },
      FilterType.and,
      sortMap: {'id': false},
    );

    if (pendingRequests.isEmpty) {
      return list;
    }

    for (Map<String, dynamic> pendingRequest in pendingRequests) {
      RequestViewModel model = RequestViewModel.fromJson(pendingRequest);

      // If request view model found.
      if (model.id != null) {
        await getStoredRequestProviders(model, pendingRequest['providers']);

        list.add(model);
      } else {
        _logger.e('Generating Request view model failed.');
      }
    }
    return list;
  }

  /// Gets stored request providers.
  ///
  /// Checks if there already fetched providers data exists. If exists fill data
  /// values. Otherwise, sets data providers from configuration file.
  // TODO: need to refactor and simplify this. Contains many strange castings
  //  and loops that over complice and lead nowhere.
  Future getStoredRequestProviders(RequestViewModel model, String? providerData) async {
    DataRequestConfigViewModel dataRequest;

    dataRequest = DataRequestConfigViewModel.fromJson(
      json.decode(model.appConfigRawData!),
      model.scope,
      model.settingsUrl,
    );

    // Retrieve providers configurations from DB.
    for (ProviderConfigViewModel providerModel in dataRequest.providers) {
      var index = dataRequest.providers.indexOf(providerModel);

      var providerConfigRawData = model.providerConfigRawData!.elementAt(index);

      // If exists, loads the data.
      if (providerConfigRawData != null) {
        providerModel.fromConfigJson(json.decode(providerConfigRawData));
      }
    }

    if (providerData != null) {
      var valueMap = json.decode(providerData);

      for (String key in valueMap!.keys as Iterable<String>) {
        var providerModel = dataRequest.providers.elementAt(int.parse(key));

        for (int id in valueMap[key]) {
          // When from the owners' vault (from == '' || from == 'owner')
          if (providerModel.from == '' || providerModel.from == 'owner') {
            var attributeData = await _commonDAOImpl.getDataFromId(
              AttributeModel.getStoreName(),
              id,
            );

            var attributeModel = AttributeModel.fromJson(attributeData!);

            // TODO: redundancy code. Validate this codes in the refactoring.
            //  Retrieves the attribute versions from attribute version store sorted by version.
            Map<String, dynamic> attributeVersionFilterMap = {
              'attributeId': attributeModel.id,
            };

            List<Map<String, dynamic>> attributeVersionList = await _commonDAOImpl.getFilteredData(
              AttributeVersionModel.getStoreName(),
              attributeVersionFilterMap,
              FilterType.and,
              sortMap: {'version': true},
            );

            if (attributeVersionList.isEmpty) {
              continue;
            }
            // Get the latest version for attribute.
            Map attributeVersionMap = attributeVersionList.last;

            AttributeVersionModel attributeVersionModel = AttributeVersionModel.fromJson(
              attributeVersionMap as Map<String, dynamic>,
            );

            providerModel.attributeVersionList.add(attributeVersionModel);
          }
          // When from the current vault the user is in, get the values from the incomingshare table
          else {
            var incomingShare = await _commonDAOImpl.getDataFromId(
              IncomingShareModel.getStoreName(),
              id,
            );

            if (incomingShare == null) {
              continue;
            }

            // TODO: temporary, very dirty cast from IncomingShareModel -> AttributeVersionModel to make it fit in the current datarequest model
            // Whole datarequest component needs to be refactored and build up from scratch!
            var attributeVersionModel = AttributeVersionModel.fromJson(incomingShare);

            providerModel.attributeVersionList.add(attributeVersionModel);
          }
        }

        // Check the all attributes in the provider completed or not.
        providerModel.isChecked = providerModel.fields!.length == valueMap[key].length ? true : false;
      }
    }

    model.setProviderList(dataRequest.providers);
  }

  /// Builds & sends packaged and encrypted payload to data requesting entity.
  Future<bool> submitPayload(RequestViewModel? model) async {
    DataRequestConfigViewModel dataRequest;
    dataRequest = await _configBL.retrieveConfigData(model);

    // Create an easy usable structure for the payload processing plugins, like:
    // name : 'test',
    // scope : 'test',
    // date : '...',
    // shares : {
    //   'pluginX' :  { 'firstName' : 'John', 'lastName' : 'Doe' }
    // }
    // also allows for easy access to attributes: [shares.pluginX.firstName].
    Map<String, dynamic> pluginInput = <String, dynamic>{};
    pluginInput['requester'] = dataRequest.name;
    pluginInput['scope'] = dataRequest.scope;
    pluginInput['provider'] = 'Schluss';
    pluginInput['date'] = DateTime.now().toIso8601String();
    pluginInput['shares'] = <String, dynamic>{};
    for (var provider in dataRequest.providers) {
      Map<String?, dynamic> tmpAttrs = <String?, dynamic>{};

      for (var field in provider.fields!) {
        // When field is a list, it contains a json encoded string,
        // decode it to have a clean json object further on.
        if (field.type == 'list' && field.value != null && field.value!.isNotEmpty) {
          tmpAttrs[field.name] = json.decode(field.value!);
        } else {
          tmpAttrs[field.name] = field.value;
        }
      }
      pluginInput['shares'][provider.plugin] = tmpAttrs;
    }

    // Load the right payload processing plugin.
    String processingPlugin = '';

    switch (dataRequest.format) {
      case 'findesk':
        processingPlugin = 'plugin-findesk';
        break;
      default:
        processingPlugin = 'plugin-default';
        break;
    }

    PayloadPluginManagerBl payloadPlugin = singletonInjector.get(
      instanceName: processingPlugin,
    );

    // Process the payload content.
    String payloadContent = payloadPlugin.processPayload(pluginInput);

    // Encrypt the payload content with public key of data requesting entity.
    String encryptedContent = await encryptPayload(
      payloadContent,
      dataRequest.dataRequester.publicKeyUrl,
    );

    // Make the payload package to send.
    var payloadPackage = {
      'contract': encryptedContent,
      'signingKey': SessionNext().get<String>('publicKey'),
    };

    // Send the payload.
    var payloadSendResult = await PayloadServiceImpl().submitPayloadData(
      dataRequest.returnUrl!,
      model!.token,
      json.encode(payloadPackage),
    );

    // When payload sending failed.
    if (!payloadSendResult) {
      _logger.d('Payload could not be sent');
      return Future.error('Payload could not be sent');
    }

    _logger.d('Payload is sent successfully');

    // Send the status update, if status update url is available.
    if (dataRequest.statusUrl != null && dataRequest.statusUrl!.isNotEmpty) {
      var statusUpdateResult = await PayloadServiceImpl().updateStatus(
        dataRequest.statusUrl!,
        json.encode({'state': 'done'}),
        model.token,
      );

      if (!statusUpdateResult) {
        _logger.d('Status update failed, could not update status to \'Done\'.');
      } else {
        _logger.d('Status update succeeded, status set to \'Done\'.');
      }
    }

    // Update the data request status: set to complete [C].
    await _commonDAOImpl.update(RequestDataModel.getStoreName(), model.id!, {'status': 'C'});

    // Save the finished data request as a contract.
    await saveContract(dataRequest);

    return Future.value(true);
  }

  // TODO: use contract manager class to include this method.
  //  Saves contract information.
  Future saveContract(DataRequestConfigViewModel dataRequest) async {
    ContractModel contract;
    List<ContractProvider> list = [];
    Requester requester;
    ContractProvider contractProvider;
    String json;

    contract = ContractModel(
      contractId: uuid!.v4(), // -> '110ec58a-a0f2-4ac4-8393-c866d813b8d1'
      date: DateTime.now().millisecondsSinceEpoch,
      scope: dataRequest.scope,
      version: '0.0.1',
      description: dataRequest.details,
      notification: true,
      type: ConnectionType.organization,
      iconColor: ColorPickerUtil().pickColor().value.toString(),
    );

    requester = Requester(
      configUrl: dataRequest.settingsUrl,
      configVersion: dataRequest.dataRequester.version,
      logoUrl: dataRequest.dataRequester.logoUrl,
      name: dataRequest.dataRequester.name,
    );

    contract.requester = requester;

    for (var element in dataRequest.providers) {
      var connectionId = 0;

      // When it is a shared attribute from the incomingShares table (not from own vault)
      if (element.from == 'current' && SessionNext().containsKey('connectionId')) {
        connectionId = SessionNext().get<int>('connectionId') ?? 0;
      }

      contractProvider = ContractProvider(
        configUrl: element.src,
        logoUrl: element.dataProvider!.logoUrl,
        request: element.request,
        provider: element.dataProvider!.name,
        attributes: element.attributeVersionList,
        connectionId: connectionId,
      );

      list.add(contractProvider);
    }

    contract.shares = list;

    json = jsonEncode(contract);

    await _commonDAOImpl.store(ContractModel.getStoreName(), jsonDecode(json));

    _logger.d('Contract is saved');
  }

  /// Encrypts string with provided public key.
  Future<String> encryptPayload(String content, String? publicKeyUrl) async {
    String publicKey = await _configService.getPublicKey(publicKeyUrl);

    String result = await CryptoUtil.encrypt(content, publicKey);

    _logger.d('Payload is encrypted');

    return result;
  }

  /// Completes store-only no-contracts data request.
  Future completeLocalRequest(RequestViewModel? model) async {
    // Update the data request status: set to complete [C].
    await _commonDAOImpl.update(
      RequestDataModel.getStoreName(),
      model!.id!,
      {'status': 'C'},
    );
  }
}
