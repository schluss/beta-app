import 'dart:convert';

import 'package:intl/intl.dart';
import 'package:schluss_beta_app/business/impl/payload_plugin_manager_bl_impl.dart';
import 'package:schluss_beta_app/business/interface/payload_plugin_manager_bl.dart';

class FindeskPayloadPluginBlImpl extends PayloadPluginManagerBlImpl implements PayloadPluginManagerBl {
  @override
  String processPayload(Map<String, dynamic> dataRequest) {
    var result = <String, dynamic>{};
    var shares = dataRequest['shares'];
    DateTime now = DateTime.now();
    String date = DateFormat('yyyy-MM-dd').format(now);
    // TODO: may need to remove empty // from all files.
    var klantgegevens = [
      {
        'Persoon': 'Aanvrager',
        'Persoongegeven': {
          'Adres': {
            //
            'Straat': extract(shares, r'$.plugin-bkr.Street'),
            'Huisnummer': extract(shares, r'$.plugin-bkr.HouseNumber'),
            'Postcode': extract(shares, r'$.plugin-bkr.PostalCode'),
            'Plaats': extract(shares, r'$.plugin-bkr.City'),
            'Gemeente': '', // Op te halen uit bovenste getoonde adres in MijnOverheid: [https://services.schluss.app/mijnoverheid/test/id.pers.basis.html], waar Functie adres = Woonadres, neem waarde bij 'Gemeentelijk deel'.
            'Land': extract(shares, r'$.plugin-bkr.Country'),
          },
          'Emailadres': extract(shares, r'$.plugin-bkr.Email'),
          'Telefoonnummer': extract(shares, r'$.plugin-bkr.PhoneNumber'),
          'GeboorteAchternaam': extract(shares, r'$.plugin-bkr.SurnameAtBirth'),
          'GeboorteTussenvoegsel': '', // extract(shares, r'$.plugin-bkr.InsertionAtBirth'), // Not available anymore with latest BKR website update (07-2021).
          'ExterneDataleverancierNaam': 'Schluss',
          'ExterneDataleverancierId': '', // Welke waarden worden hier en hieronder verwacht?.
          'ExterneDataleverancierPersoonId': '',
          'ExterneRelatieId': '',
          'Voorletters': extract(shares, r'$.plugin-uwv.Initials'),
          'Voornaam': extract(shares, r'$.plugin-bkr.Firstnames'), // TODO: only get first one when multiple firstnames are there.
          'Tussenvoegsel': '', // extract(shares, r'$.plugin-bkr.Insertion'), // Not available anymore with latest BKR website update (07-2021).
          'Achternaam': extract(shares, r'$.plugin-uwv.Surname'),
          'Geboortedatum': extract(shares, r'$.plugin-uwv.DateOfBirth'),

          'Geslacht': extract(shares, r'$.plugin-bkr.Gender'), // TODO: make sure it returns Man/Vrouw/Onbekend
          'Nationaliteit': '', //extract(shares, r'$.plugin-mijnOverheid.Nationality'),
          'GeboorteLand': '', //extract(shares, r'$.plugin-mijnOverheid.CountryOfBirth'),
          'GeboortePlaats': '', //extract(shares, r'$.plugin-mijnOverheid.PlaceOfBirth'),
        },
        'LegitimatieSoort': '', //extract(shares, r'$.plugin-mijnOverheid.IdType'),
        'Legitimatienummer': '', //extract(shares, r'$.plugin-mijnOverheid.IdDocumentNumber'),
        'LegitimatieAfgiftedatum': '', // waar is dit te vinden?
        'LegitimatieAfgiftePlaats': '', // waar is dit te vinden?
        'LegitimatieAfgifteLand': '', // waar is dit te vinden?
        'Rekeningnummer': extract(shares, r'$.plugin-belastingdienst.Accountnumber'),
        'BurgerlijkeStaat': '', // waar is dit te vinden?
        'BurgerlijkeStaatDatum': '', // waar is dit te vinden?
        'HuwelijkOntbonden': {
          // Waar is dit te vinden?
          //
          'HuwelijkOntbondenDatum': '', // Waar is dit te vinden?
          'SoortOntbonden': '' // Waar is dit te vinden?
        },
        'Opleidingsniveau': '', // CalculateOpleidingsniveau(shares),
        'Roker': '',
        'AOWKortingsJaren': '' // Dit dient berekend te worden, vraag is.
      }
    ];

    var kinderen = [
      {
        //
        'ExterneRelatieId': 'string',
        'Voorletters': 'string',
        'Voornaam': 'string',
        'Tussenvoegsel': 'string',
        'Achternaam': 'string',
        'Geboortedatum': date,
        'Geslacht': 'Man',
        'Nationaliteit': 'AD',
        'GeboorteLand': 'AD',
        'GeboortePlaats': 'string'
      }
    ];

    var inkomens = [
      {
        'Persoon': 'Aanvrager',
        'Loondienst': [
          {
            //
            'NaamWerkgever': 'string',
            'BrutoJaarsalaris': 0,
            'VakantieToeslag': 0,
            'InDienstSinds': date,
            'BeroepType': 'DocentenHogerOnderwijsEnHoogleraren',
            'Beroepsfunctie': 'string',
            'Vestigingsplaats': 'string',
            'FulltimeContract': true,
            'GemiddeldAantalUrenPerWeek': 0,
            'VasteAanstelling': true,
            'TijdelijkContractTot': date,
            'Intentieverklaring': true,
            'Perspectiefverklaring': true,
            'InkomenJaarMinus1': 0,
            'InkomenJaarMinus2': 0,
            'InkomenJaarMinus3': 0,
            'Onregelmatigheidstoeslag': 0,
            'Vaste13eMaand': 0,
            'Provisie': 0,
            'VasteEindejaarsuitkering': 0,
            'Overwerk': 0,
            'WerkgeversBijdrageLevensloopregeling': 0,
            'AutoVanDeZaak': true,
            'CatalogusWaarde': 0,
            'AutoVanDeZaakRegeling': 'VastPercentageFiscaleBijtelling',
            'FiscaleBijtelling': 0,
            'EigenBijdrage': 0,
            'WerknemersbijdragePensioen': 0,
            'WerknemersbijdrageAOVerzekering': 0,
            'PercentageMeetellenVoorToetsinkomen': 0,
            'LoonheffingskortingToepassen': true
          }
        ],
        'Onderneming': [
          {
            //
            'NaamBedrijf': 'string',
            'HeeftPersoneel': true,
            'RechtsvormBedrijf': 'Eenmanszaak',
            'DGA': true,
            'PercentageAandelen': 0,
            'StartdatumOnderneming': date,
            'KvKNummer': 'string',
            'RSIN': 'string',
            'SBICode': 'string',
            'NettoWinstLoonDGAJaarMinus1': 0,
            'NettoWinstLoonDGAJaarMinus2': 0,
            'NettoWinstLoonDGAJaarMinus3': 0,
            'GemiddeldeNettoJaarwinstLoonDGA': 0,
            'Toetsinkomen': 0,
            'AutoVanDeZaak': true,
            'CatalogusWaarde': 0,
            'AutoVanDeZaakRegeling': 'VastPercentageFiscaleBijtelling',
            'FiscaleBijtelling': 0,
            'EigenBijdrage': 0,
            'PercentageMeetellenVoorToetsinkomen': 0,
            'LoonheffingskortingToepassen': true
          }
        ],
        'Uitkering': [
          {
            //
            'SoortUitkering': 'IVA',
            'BrutoUitkeringPerJaar': 0,
            'Vakantietoeslag': 0,
            'Einddatum': date,
            'PercentageMeetellenVoorToetsinkomen': 0,
            'LoonheffingskortingToepassen': true
          }
        ],
        'PrePensioen': [
          {
            //
            'Uitkeringsinstantie': 'string',
            'BrutoPensioenPerJaar': 0,
            'StartDatum': date,
            'Einddatum': date,
            'PercentageMeetellenVoorToetsinkomen': 0,
            'LoonheffingskortingToepassen': true
          }
        ],
        'OverigeInkomsten': [
          {
            //
            'Bron': 'Alimentatie',
            'BrutoBedragPerJaar': 0,
            'Einddatum': date,
            'Omschrijving': 'string',
            'PercentageMeetellenVoorToetsinkomen': 0,
            'LoonheffingskortingToepassen': true
          }
        ],
        'AOWUitkering': {
          //
          'Ingangsdatum': date,
          'BrutoUitkeringPerJaar': 0,
          'Vakantietoeslag': 0,
          'PercentageMeetellenVoorToetsinkomen': 0,
          'LoonheffingskortingToepassen': true
        },
        'NettoMaandInkomen': 0,
        'SvLoon': 0,
        'ToetsinkomenLoondienstOpBasisVan': 'BrutoJaarinkomen',
        'InkomensbepalingLoondienst': {
          //
          'ToetsinkomenOpBasisVanUWVVerzekeringsbericht': 0,
          'MaandelijksePensioenbijdrage': 0
        },
        'Arbeidsmarktscan': {
          //
          'ArbeidsmarktscanScore': 0,
          'ArbeidsmarktscanVerdienCapaciteit': 0,
          'ArbeidsmarktscanRapportNummer': 'string'
        },
        'ToetsinkomenOndernemingOpBasisVan': 'DrieJaarIBAangifte',
        'VerzameldInkomenUitOnderneming': {
          //
          'ToetsinkomenOpBasisVanInkomensverklaring': 0
        },
        'InkomenBepalingPensioen': true
      }
    ];

    var vermogens = [
      {
        //
        'Persoon': 'Aanvrager',
        'Spaarrekeningen': getVermogens(shares),
        'Beleggingsrekeningen': getBeleggingen(shares),
        'SpaarloonLevensloopRegelingen': [
          {
            //
            'ActueelSaldo': 0
          }
        ],
        'OverigeBezittingen': getOverigeVermogens(shares),
        'VermogenBepalenLeencapaciteit': 0
      }
    ];

    var verplichtingen = [
      {
        //
        'Persoon': 'Aanvrager',
        'BetaalRekeningRoodstandMogelijkheid': [
          {
            //
            'Maatschappij': 'string',
            'Kredietlimiet': 0,
            'Maandbedrag': 0,
            'ActueleRoodstand': 0,
            'AfgelostVoorNieuweHypotheek': true
          }
        ],
        'DoorlopendKrediet': getDoorlopendKrediet(shares),
        'PersoonlijkeLening': getPersoonlijkeLening(shares),
        'Studievoorschot': {
          //
          'OorspronkelijkeSchuld': 0,
          'Maandbedrag': 0,
          'ExtraAfgelostBedrag': 0,
          'AfgelostVoorNieuweHypotheek': true
        },
        'Studieschuld': {
          //
          'OorspronkelijkeSchuld': 0,
          'Maandbedrag': 0,
          'ExtraAfgelostBedrag': 0,
          'AfgelostVoorNieuweHypotheek': true
        },
        'AlimentatieExPartner': [
          {
            //
            'Jaarbedrag': 0,
            'Einddatum': date
          }
        ]
      }
    ];

    var huidigeWoonsituaties = [
      {
        //
        'HuidigeWoonsituatie': 'Aanvrager',
        'Woonvorm': 'Huurwoning',
        'Huurwoning': {
          //
          'Contractnaam': 'Aanvrager',
          'KaleHuur': 0,
          'Servicekosten': 0,
          'OverigeKosten': 0,
          'HuurverhogingPercentage': 0
        },
        'Inwonend': {
          //
          'KostenInwoning': 0,
          'OverigeKosten': 0
        },
        'Koopwoning': {
          //
          'WOZWaarde': 0,
          'Marktwaarde': 0,
          'PercentageEigenaar': 0,
          'ErfpachtCanon': 0,
          'Woningtype': 'Eengezinswoning',
          'KoopwoningDoel': 'BlijvenWonen',
          'Verkoopprijs': 0,
          'Verkoopkosten': 0,
          'Leveringsdatum': date
        }
      }
    ];

    var hypotheekverledens = [
      {
        //
        'HypotheekVerledenVan': 'Aanvrager',
        'Hypotheek': {
          //
          'Maatschappij': 'None',
          'Hypotheeknummer': 'string',
          'BedragInschrijving': 0,
          'NHG': true,
          'Percentage': 0,
          'HypotheekVerledenLeningdelen': [
            {
              //
              'Leningdeelnummer': 0,
              'LeningdeelnummerAlphaNumeriek': 'string',
              'Aflossingsvorm': 'Aflossingsvrij',
              'Fiscaliteit': 'Box1',
              'Rente': 0,
              'EinddatumRentevastperiode': date,
              'OorspronkelijkeSomOpIngangsdatum': 0,
              'Ingangsdatum': date,
              'Einddatum': date,
              'Schuldrestdatum': date,
              'Schuldrest': 0,
              'MaandelijkseInleg': 0,
              'MaandelijksePremie': 0,
              'ActueleWaarde': 0,
              'LaagsteStortingEnigJaar': 0,
              'HoogsteStortingEnigJaar': 0,
              'VerwachtRendement': 0
            }
          ]
        }
      }
    ];

    var eigenwoningreserves = [
      {
        //
        'HypotheekVerledenVan': 'Aanvrager',
        'Eigenwoningreserve': 0
      }
    ];

    var hypotheekadvies = {
      //
      'Naam': 'string',
      'Doelstelling': 'BestaandeWoningKopen',
      'Toelichting': 'string',
      'AanTeKopenWoning': {
        //
        'Gemeente': 'string',
        'BestaandeBouw': {
          //
          'Koopsom': 0,
          'RoerendeGoederen': 0,
          'Verbouwingskosten': 0,
          'MarktwaardeNaVerbouwing': 0,
          'BankgarantieAanvragen': true,
          'BankgarantieViaGeldVerstrekker': true,
          'BankgarantieBedrag': 0,
          'KostenBankgarantie': 0,
          'Overdrachtsbelasting': 0,
          'LeveringsakteNotaris': 0,
          'KostenBouwkundigeKeuring': 0,
          'SoortWoning': 'Eengezinswoning',
          'Bouwjaar': 0
        },
        'Nieuwbouw': {
          //
          'KoopsomGrond': 0,
          'GrondReedsInBezit': true,
          'Aanneemsom': 0,
          'KoopAanneemsom': 0,
          'Bouwrente': 0,
          'Meerwerk': 0,
          'Zelfbouw': true,
          'SoortNieuwbouw': 'Projectbouw',
          'Bouwplannummer': 'string',
          'Situatienummer': 'string',
          'Opleverdatum': date,
          'RenteverliesTijdensDeBouw': 0,
          'SoortWoning': 'Eengezinswoning'
        },
        'Erfpacht': true,
        'ErfpachtDetails': {
          //
          'ErfpachtAfgekocht': {
            //
            'ErfpachtAfkoopsom': 0,
            'ErfpachtBegindatum': date
          },
          'ErfpachtEeuwigdurend': {
            //
            'ErfpachtCanon': 0,
            'ErfpachtBegindatum': date
          },
          'ErfpachtEindig': {
            //
            'ErfpachtEinddatum': date,
            'ErfpachtCanon': 0,
            'ErfpachtBegindatum': date
          }
        },
        'EnergiebesparendeVoorzieningen': 0,
        'EnergiebespaarBudget': 0,
        'Marktwaarde': 0,
        'WOZWaarde': 0,
        'WOZTaxatiedatum': date,
        'Passeerdatum': date,
        'KostenAankoopmakelaar': 0,
        'PremieOpstalverzekeringPerJaar': 0,
        'KopenOnderVoorwaardenType': 'MaatschappelijkGebondenEigendom',
        'OverbruggingskredietGewenst': true,
        'OverbruggingskredietBedrag': 0,
        'Rangorde': 0,
        'RegelingTypeNieuweWoning': 'EersteHypotheekNieuw',
        'Adres': {
          //
          'Straat': 'string',
          'Huisnummer': 'string',
          'Postcode': 'string',
          'Plaats': 'string',
          'Gemeente': 'string',
          'Land': 'AD'
        },
        'NHG': true,
        'NHGKosten': 0,
        'EigenBewoning': true,
        'EnergieKlasse': 'APlusPlus',
        'EnergieklasseAfgiftedatum': date,
        'NhgMutatie': 'AankoopWoning'
      },
      'BehoudenWoonsituatie': {
        //
        'WijzigingBestaandeHypotheek': {
          //
          'Verbouwen': {
            //
            'Verbouwingskosten': 0,
            'MarktwaardeNaVerbouwing': 0,
            'EnergiebesparendeVoorzieningen': 0,
            'EnergiebespaarBudget': 0,
            'VolledigHerfinancieren': true,
            'WOZWaarde': 0,
            'WOZTaxatieDatum': date
          },
          'Hoofdelijkontslag': {
            //
            'KoopsomDeelExPartner': 0,
            'VolledigHerfinancieren': true,
            'Overdrachtsbelasting': 0,
            'LeveringsAkteNotaris': 0,
            'Marktwaarde': 0,
            'WOZWaarde': 0,
            'WOZTaxatieDatum': date
          },
          'Passeerdatum': date,
          'BoeteRente': 0
        },
        'PremieOpstalverzekeringPerJaar': 0,
        'EnergieKlasse': 'APlusPlus',
        'Bouwjaar': 0,
        'EigenBewoning': true,
        'NHG': true,
        'NHGKosten': 0,
        'NhgMutatie': 'AankoopWoning',
        'RegelingType': 'EersteHypotheekNieuw',
        'SoortWoning': 'Eengezinswoning',
        'Marktwaarde': 0,
        'WOZWaarde': 0,
        'PercentageEigenaar': 0,
        'ErfpachtCanon': 0
      },
      'MaximaalGewensteNettoHypotheekLasten': 0,
      'HypotheekToetsRente': 0,
      'PercentageEigendomAanvrager': 0,
      'AanvragerHypotheekgever': true,
      'PartnerHypotheekgever': true,
      'Taxatiekosten': 0,
      'Bemiddelingskosten': 0,
      'NettoAdvieskosten': 0,
      'HypotheekaktekostenNotaris': 0,
      'HypotheekaktekostenKadaster': 0,
      'OverigeAftrekbareKosten': 0,
      'ConsumptieveBestedingen': 0,
      'HypotheekBedrag': 0,
      'HypothecaireInschrijving': 0,
      'Erfpacht': true,
      'ErfpachtDetails': {
        //
        'ErfpachtAfgekocht': {
          //
          'ErfpachtAfkoopsom': 0,
          'ErfpachtBegindatum': date
        },
        'ErfpachtEeuwigdurend': {
          //
          'ErfpachtCanon': 0,
          'ErfpachtBegindatum': date
        },
        'ErfpachtEindig': {
          //
          'ErfpachtEinddatum': date,
          'ErfpachtCanon': 0,
          'ErfpachtBegindatum': date
        }
      }
    };

    var inventarisatie = {
      'WerkRegelingen': [
        {
          //
          'PersoonType': 'Aanvrager',
          'OpgebouwdPensioen': 0,
          'TeBereikenPensioen': 0,
          'EinddatumUitkering': date,
          'OverlijdenVoorPensioendatum': 0,
          'OverlijdenNaPensioendatum': 0,
          'Halfwezenuitkering': 0,
          'HalfwezenuitkeringTotJaar': 0,
          'FactorA': 0,
          'Pensioendatum': date,
          'Uitkeringsinstantie': 'string',
          'Registratienummer': 'string',
          'Ingangsdatum': date
        }
      ],
      'PriveRegelingen': [
        {
          //
          'PersoonType': 'Aanvrager',
          'KapitaalObvVan4ProcentRendement': 0,
          'JaarlijkseUitkeringBijLeven': 0,
          'EinddatumUitkeringBijLeven': date,
          'JaarlijkseUitkeringBijOverlijden': 0,
          'EinddatumUitkeringBijOverlijden': date,
          'EinddatumRegeling': date,
          'Uitkeringsinstantie': 'string',
          'Registratienummer': 'string',
          'Ingangsdatum': date
        }
      ],
      'PensioenInventarisaties': [
        {
          //
          'PersoonType': 'Aanvrager',
          'AOWKortingsJaren': 0,
          'LevensVerzekering': [
            {
              //
              'UitkeringBijLeven': 0,
              'Premie': 0,
              'EinddatumPremie': date,
              'Begunstigde': 'Verzekerde',
              'Einddatum': date,
              'Uitkeringsinstantie': 'string',
              'Registratienummer': 'string',
              'Ingangsdatum': date
            }
          ]
        }
      ],
      'ArbeidsongeschiktInventarisaties': [
        {
          //
          'PersoonType': 'Aanvrager',
          'WerkStartJaar': 0,
          'WekenEisVoldaan': true,
          'JarenEisVoldaan': true,
          'AantalDagenSvLoon': 0,
          'EersteZiektejaarLoondoorbetalingPercentage': 0,
          'TweedeZiektejaarLoondoorbetalingPercentage': 0,
          'LoondoorbetalingGemaximeerdOpDagloon': true,
          'WoonlastenBeschermers': [
            {
              //
              'Ingangsdatum': date,
              'NettoUitkering': true,
              'MaandelijkseUitkeringWw': 0,
              'EinddatumWwDekking': date,
              'MaandelijksePremieWw': 0,
              'EinddatumWwPremie': date,
              'MaandelijkseUitkeringAo': 0,
              'EinddatumAoDekking': date,
              'MaandelijksePremieAo': 0,
              'EinddatumAoPremie': date,
              'Uitkeringsgrens': 'Procent80OfMeer',
              'Beoordeling': 'PassendeArbeid',
              'EigenRisico': 'Dagen30',
              'Uitkeringsduur': 'Basis',
              'Uitkeringshoogte': 'VolledigeUitkering',
              'Verzekeraar': 'string',
              'Polisnummer': 'string'
            }
          ],
          'WgaBasisVerzekeringen': [
            {
              //
              'GemaximeerdOpDagloon': true,
              'Ingangsdatum': date,
              'EinddatumDekking': date,
              'Verzekeraar': 'string',
              'Polisnummer': 'string'
            }
          ],
          'WgaHiaatUitgebreidVerzekeringen': [
            {
              //
              'VanafPercentageAo': 0,
              'GemaximeerdOpDagloon': true,
              'Ingangsdatum': date,
              'EinddatumDekking': date,
              'Verzekeraar': 'string',
              'Polisnummer': 'string'
            }
          ],
          'WgaAanvullingLoyalisVerzekeringen': [
            {
              //
              'VanafPercentageAo': 0,
              'GemaximeerdOpDagloon': true,
              'Ingangsdatum': date,
              'EinddatumDekking': date,
              'Verzekeraar': 'string',
              'Polisnummer': 'string'
            }
          ],
          'WgaAanvullingMetalektroVerzekeringen': [
            {
              //
              'VanafPercentageAo': 0,
              'GemaximeerdOpDagloon': true,
              'Ingangsdatum': date,
              'EinddatumDekking': date,
              'Verzekeraar': 'string',
              'Polisnummer': 'string'
            }
          ],
          'WiaAanvullendJaarbedragAovVerzekeringen': [
            {
              //
              'BrutoJaarUitkering': 0,
              'UitkeringsPeriode': 'Altijd',
              'VanafPercentageAo': 0,
              'GemaximeerdOpDagloon': true,
              'Ingangsdatum': date,
              'EinddatumDekking': date,
              'Verzekeraar': 'string',
              'Polisnummer': 'string'
            }
          ],
          'WiaAanvullingPercSalarisAovVerzekeringen': [
            {
              //
              'UitkeringPercentage': 0,
              'UitkeringsPeriode': 'Altijd',
              'VanafPercentageAo': 0,
              'GemaximeerdOpDagloon': true,
              'Ingangsdatum': date,
              'EinddatumDekking': date,
              'Verzekeraar': 'string',
              'Polisnummer': 'string'
            }
          ],
          'WiaInkomensAanvullingAovVerzekeringen': [
            {
              //
              'BrutoJaarUitkering': 0,
              'UitkeringPercentage': 0,
              'UitkeringsPeriode': 'Altijd',
              'VanafPercentageAo': 0,
              'GemaximeerdOpDagloon': true,
              'Ingangsdatum': date,
              'EinddatumDekking': date,
              'Verzekeraar': 'string',
              'Polisnummer': 'string'
            }
          ],
          'WiaBodemVerzekeringen': [
            {
              //
              'UitkeringPercentage': 0,
              'UitkeringDuur': 0,
              'GemaximeerdOpDagloon': true,
              'Ingangsdatum': date,
              'EinddatumDekking': date,
              'Verzekeraar': 'string',
              'Polisnummer': 'string'
            }
          ],
          'WiaBodemAoVerzekeringen': [
            {
              //
              'GemaximeerdOpDagloon': true,
              'Ingangsdatum': date,
              'EinddatumDekking': date,
              'Verzekeraar': 'string',
              'Polisnummer': 'string'
            }
          ],
          'WiaExcedentverzekeringen': [
            {
              //
              'UitkeringPercentage': 0,
              'Ingangsdatum': date,
              'EinddatumDekking': date,
              'Verzekeraar': 'string',
              'Polisnummer': 'string'
            }
          ],
          'AovVoorZelfstandigen': [
            {
              //
              'VanafPercentageAo': 0,
              'VerzekerdBedrag': 0,
              'ArbeidsongeschiktheidEigenRisicoType': 'Dagen14',
              'UitkeringshoogteOpBasisVanType': 'VolledigeUitkering',
              'UitkeringEindigtOpBasisVanLeeftijdType': {
                //
                'Leeftijd': 0
              },
              'UitkeringEindigtOpBasisVanPeriodeType': {
                //
                'AantalJaren': 0
              },
              'Ingangsdatum': date,
              'EinddatumDekking': date,
              'Verzekeraar': 'string',
              'Polisnummer': 'string'
            }
          ]
        }
      ],
      'OverlijdenInventarisaties': [
        {
          //
          'PersoonType': 'Aanvrager',
          'Overlijdensrisicoverzekeringen': [
            {
              //
              'VerzekerdBedrag': 0,
              'Begunstigde': 'Partner',
              'Kapitaalverloop': 'Gelijkblijvend',
              'AnnuiteitenPercentage': 0,
              'Premie': 0,
              'EinddatumPremie': date,
              'EinddatumPolis': date,
              'Uitkeringsdoel': 'Geen',
              'Uitkeringsinstantie': 'string',
              'Registratienummer': 'string',
              'Ingangsdatum': date
            }
          ]
        }
      ]
    };

    var uwv = {
      //
      'Verzekeringsbericht': extract(shares, r'$.plugin-uwv.InsuranceMessage'),
      'Inkomens': getInkomens(shares)
    };

    var belastingdienst = {
      //
      'Belastingjaar': extract(shares, r'$.plugin-belastingdienst.TaxYear')
    };

    result = {
      //
      'requester': dataRequest['requester'],
      'provider': dataRequest['provider'],
      'scope': dataRequest['scope'],
      'date': dataRequest['date'],
      'shares': {
        //
        'Findesk': {
          //
          'Klantgegevens': klantgegevens,
          'Kinderen': kinderen,
          'Inkomens': inkomens, // Inkomens, need to get data from handig API (not integrated as of yet).
          'Vermogens': vermogens,
          'Verplichtingen': verplichtingen,
          'HuidigeWoonsituaties': huidigeWoonsituaties,
          'Hypotheekverledens': hypotheekverledens,
          'Eigenwoningreserves': eigenwoningreserves,
          'HypotheekAdvies': hypotheekadvies,
          'Inventarisatie': inventarisatie,
          'BronReferentie': 'string',
          'BedragVermogenOpzijVoorOnvoorzieneUitgaven': 0,
        },
        'UWV': uwv,
        'Belastingdienst': belastingdienst
      }
    };

    return json.encode(result);
  }

  List<dynamic> getInkomens(Map? shares) {
    var t = extract(shares, r'$.plugin-uwv.Incomes');

    if (t.isEmpty) {
      return [];
    }
    return t;
  }

  List<dynamic> getDoorlopendKrediet(Map? shares) {
    var debts = [];

    var items = extract(shares, r'$.plugin-bkr.Debts');

    if (items.isEmpty) {
      return debts;
    }

    for (Map<String, dynamic> item in items) {
      // Doorlopend credit
      if (item.containsKey('Type') && item['Type'].toString().contains('Doorlopend')) {
        debts.add({
          //
          'Maatschappij': item['Supplier'],
          'Kredietlimiet': item['Amount'],
          'Maandbedrag': '',
          'ActueelSaldo': '',
          'Einddatum': '',
          'AfgelostVoorNieuweHypotheek': ''
        });
      }
    }

    return debts;
  }

  List<dynamic> getPersoonlijkeLening(Map? shares) {
    var debts = [];

    var items = extract(shares, r'$.plugin-bkr.Debts');

    if (items.isEmpty) {
      return debts;
    }

    for (Map<String, dynamic> item in items) {
      // Doorlopend credit
      if (item.containsKey('Type') && item['Type'].toString().contains('Aflopend')) {
        debts.add({
          //
          'Maatschappij': item['Supplier'],
          'Kredietlimiet': '',
          'Maandbedrag': '',
          'ActueelSaldo': item['Amount'],
          'Einddatum': item['ExpectedEndDate'],
          'AfgelostVoorNieuweHypotheek': ''
        });
      }
    }

    return debts;
  }

  List<dynamic> getVermogens(Map? shares) {
    var assets = [];

    var items = extract(shares, r'$.plugin-belastingdienst.Assets');

    if (items.isEmpty) {
      return assets;
    }

    for (Map<String, dynamic> item in items) {
      if (item.containsKey('Type') && item['Type'] == 'SavingsAccount') {
        assets.add({
          //
          'Bank': item['Description'],
          'InlegPerMaand': '',
          'ActueelSaldo': item['Value']
        });
      }
    }

    return assets;
  }

  List<dynamic> getBeleggingen(Map? shares) {
    var assets = [];

    var items = extract(shares, r'$.plugin-belastingdienst.Assets');

    if (items.isEmpty) {
      return assets;
    }

    for (Map<String, dynamic> item in items) {
      if (item.containsKey('Type') && item['Type'] == 'MutualFund' || item['Type'] == 'MutualFundGreen') {
        assets.add({
          //
          'Bank': item['Description'],
          'InlegPerMaand': '',
          'ActueelSaldo': item['Value']
        });
      }
    }

    return assets;
  }

  List<dynamic> getOverigeVermogens(Map? shares) {
    var assets = [];

    var items = extract(shares, r'$.plugin-belastingdienst.Assets');

    if (items.isEmpty) {
      return assets;
    }

    for (Map<String, dynamic> item in items) {
      if (item.containsKey('Type') && item['Type'] == 'MiscellaneousAsset') {
        assets.add({
          //
          'Bank': item['Description'],
          'InlegPerMaand': '',
          'ActueelSaldo': item['Value']
        });
      }
    }

    return assets;
  }

/*
  List<dynamic> getVerplichtingen(Map shares) {
    // belastingdienst
    var debts = [];

    var items = extract(shares, r'$.plugin-belastingdienst.Debts');

    if (items.isNotEmpty) {
      for (Map<String, dynamic> item in items) {
        if (item['Type'] == 'MiscellaneousAsset') {
          debts.add({'Bank': item['Supplier'], 'InlegPerMaand': '', 'ActueelSaldo': item['Value']});
        }
      }
    }

    // bkr

    return [];
  }
*/
  // From a List of strings containing values like 'HBO Bachelor Informatica' get the 'HBO' term
  /*
  String calculateOpleidingsniveau(Map shares) {
    String level = '';
    var t = extract(shares, r'$.plugin-mijnoverheid.Educations');

    if (t.isEmpty) {
      return level;
    }

    List<String> educations = json.decode(t);

    if (educations.isEmpty) {
      return level;
    }

    for (var item in educations) {
      item = item.toUpperCase();

      if (item.contains('WO')) {
        level = 'WO';
        break;
      } else if (item.contains('HBO')) {
        level = 'HBO';
        break;
      } else if (item.contains('VWO')) {
        level = 'VWO';
        break;
      } else if (item.contains('HAVO')) {
        level = 'HAVO';
        break;
      } else if (item.contains('MBO')) {
        level = 'MBO';
        break;
      } else if (item.contains('VMBO')) {
        level = 'VMBO';
      }
    }

    return level;
  }
  */
  /*
  List<dynamic> getLoondienst(Map shares) {
    var loondienst = [];

    var t = extract(shares, r'$.plugin-uwv.Incomes');

    if (t.isEmpty) {
      return loondienst;
    }

    List<String> incomes = json.decode(t);

    if (incomes.isEmpty) {
      return loondienst;
    }

    for (Map<String, dynamic> item in loondienst) {
      // loop through the details

      // make an item object
      var incomeItem = {
        'NaamWerkgever': item['Employer'],
      };

      loondienst.add(incomeItem);
    }

    return loondienst;
  }
  */
}
