import 'dart:convert';

import 'package:dio/dio.dart';
import 'package:flutter_dotenv/flutter_dotenv.dart';
import 'package:logger/logger.dart';
import 'package:schluss_beta_app/business/impl/external_storage_bl_impl.dart';
import 'package:schluss_beta_app/business/interface/background_task.dart';
import 'package:schluss_beta_app/data/impl/attribute_store_dao_impl.dart';
import 'package:schluss_beta_app/data/impl/contract_store_dao_impl.dart';
import 'package:schluss_beta_app/data/impl/share_store_dao_impl.dart';
import 'package:schluss_beta_app/data/models/task_model.dart';
import 'package:schluss_beta_app/services/interface/gateway_service.dart';
import 'package:schluss_beta_app/singleton_injector.dart';
import 'package:schluss_beta_app/ui/dashboard/stewardship/owner_define_access_timing_page_content.dart';
import 'package:schluss_beta_app/util/common_util.dart';
import 'package:schluss_beta_app/util/crypto_util.dart';

/// Updates the existing attribute link on the Gateway, so it links to the latest version (=value)
/// of the attribute.
/// In the case of when the attribute is shared with other clients (connections)
/// these clients receive a socket message telling the share is updated.
///
/// Register like this:
///
///```dart
/// singletonInjector<TaskService>().add('UpdateAttributeLinkTask', {
///   'id': 1,  // Attribute version id.
/// });
/// ```
class UpdateAttributeLinkTask implements BackgroundTask {
  final Logger _logger = singletonInjector<Logger>();

  @override
  Future<bool> run(TaskModel model) async {
    try {
      // Parse the parameters from the supplied model.
      Map paramMap = jsonDecode(model.params!);

      if (paramMap.isEmpty || !paramMap.containsKey('id')) {
        _logger.e('UpdateAttributeLinkTask: Supplied parameters are invalid, therefore the task is discarded');
        return Future.value(true);
      }

      var db = AttributeStoreDAOImpl();
      int attrVersionId = paramMap['id'];

      // Get the attribute version.
      var attrVersion = await db.getAttributeVersion(attrVersionId);

      // When version does not exist we cannot continue.
      if (attrVersion == null) {
        _logger.e('UpdateAttributeLinkTask: AttributeVersion does not exist, therefore the task is discarded');
        return true;
      }

      // Get the attribute.
      var attribute = await db.getAttribute(attrVersion.attributeId!);

      // When attribute does not exist we cannot continue.
      if (attribute == null) {
        _logger.e('UpdateAttributeLinkTask: Attribute does not exist, therefore the task is discarded');
        return true;
      }

      // When attribute has no existing gateway link, we cannot continue.
      if (attribute.gatewayLinkKey == null) {
        _logger.e('UpdateAttributeLinkTask: Attribute does not have a link to the Gateway, therefore the task is discarded');
        return true;
      }

      // Make the put request to the gateway to update the link.
      var gatewayService = singletonInjector<GatewayService>();
      var externalStorage = singletonInjector<ExternalStorageBLImpl>();
      var gatewayUrl = dotenv.env['UGW_URL']!;
      var extStorageUrl = dotenv.env['EGW_URL']!;
      var extStorageToken = await externalStorage.getToken();

      var requestBody = {
        'name': attribute.name,
        'label': attribute.label,
        'group': attribute.group,
        'category': attribute.category,
        'type': attribute.type,
        'logoUrl': attribute.logoUrl,
        'providerName': attribute.providerName,
        'mediaType': attribute.mediaType,
        'version': attrVersion.version,
        'storageLocation': CommonUtil.buildUrl(
          extStorageUrl,
          path: attrVersion.onlineStoragePath,
        ),
        'storageProvider': attrVersion.onlineStorageProvider,
        'storageAuthorization': {'authorization': extStorageToken},
      };

      var response = await gatewayService.executePutRequest(
        CommonUtil.buildUrl(
          gatewayUrl,
          path: '/links/${attribute.gatewayLinkObjectId!}',
        ),
        requestBody,
        headers: {
          'Authorization': '${attribute.gatewayLinkKey!}/${attribute.gatewayLinkOwnerKey!}',
        },
      );

      if (response == null && !response!.containsKey('objectId')) {
        _logger.e('UpdateAttributeLinkTask: Invalid response from the Gateway, trying again later');
        return false;
      }

      // Send a POST message to all clients gateways' which the attribute
      // is shared with, telling them the attribute value is updated.
      Dio httpClient = Dio();
      Map? gatewayCredentials = await CryptoUtil.getGatewaySecretData();

      // Get the outgoing shares for this attribute.
      var shares = await ShareStoreDAOImpl().getOutgoingByAttribute(attribute.id!);

      for (var share in shares) {
        // Get the contract.
        var contractModel = await ContractStoreDAOImpl().getById(
          share.contractId!,
        );

        // When contract not found.
        if (contractModel == null) {
          continue;
        }

        // Send the message in the form of a POST request to the gateway of the client.
        // Why a POST request? well, the client could have another
        // gateway, therefore a socket connection is not available.
        var socketMessage = {
          'to': contractModel.clientKey,
          'message': json.encode({
            'type': 'attributeShareUpdate',
            'from': gatewayCredentials!['key'],
            'shares': [share.gatewayShareId],
            'hideValues': contractModel.dataAccessType == DataAccessType.afterPassed ? true : false,
          }),
        };

        httpClient.options = BaseOptions(contentType: Headers.jsonContentType);

        var response = await httpClient.post<Map<String, dynamic>>(
          CommonUtil.buildUrl(contractModel.gatewayUrl!, path: '/messages'),
          data: socketMessage,
        );

        // When the message post request did not succeed.
        if (response.statusCode != 200 && response.statusCode != 201) {
          _logger.e('UpdateAttributeLinkTask: Invalid response from the Gateway, trying again later');
          return false;
        }
      }

      // All done.
      return true;
    } catch (e) {
      singletonInjector<Logger>().e(e);
      return false;
    }
  }
}
