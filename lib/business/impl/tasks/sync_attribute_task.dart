import 'dart:convert';

import 'package:logger/logger.dart';
import 'package:schluss_beta_app/business/impl/external_storage_bl_impl.dart';
import 'package:schluss_beta_app/business/interface/background_task.dart';
import 'package:schluss_beta_app/data/impl/attribute_store_dao_impl.dart';
import 'package:schluss_beta_app/data/models/task_model.dart';
import 'package:schluss_beta_app/services/impl/task_service.dart';
import 'package:schluss_beta_app/singleton_injector.dart';

/// Sync an attribute (value) to the registered external storage provider.
///
/// When the attribute is also shared via the gateway the link is updated
/// with the latest version so all shares will see the latest version.
///
/// Register like this:
///
/// ```dart
/// singletonInjector<TaskService>().add('SyncAttributeTask', {
///   'id': 1,  // Attribute version id.
/// });
/// ```
class SyncAttributeTask implements BackgroundTask {
  final Logger _logger = singletonInjector<Logger>();

  @override
  Future<bool> run(TaskModel model) async {
    try {
      // Parse the parameters from the supplied model.
      Map paramMap = jsonDecode(model.params!);

      if (paramMap.isEmpty || !paramMap.containsKey('id')) {
        _logger.e('UpdateConnectionTask: Supplied parameters are invalid, therefore the task is discarded');
        return Future.value(true);
      }

      var db = AttributeStoreDAOImpl();
      int attrVersionId = paramMap['id'];

      // Get the attribute version.
      var attrVersion = await db.getAttributeVersion(attrVersionId);

      // When version does not exist we cannot continue.
      if (attrVersion == null) {
        _logger.e('UpdateConnectionTask: AttributeVersion does not exist, therefore the task is discarded');
        return false;
      }

      // When is synchronized already, we don't have to continue.
      if (attrVersion.onlineStoragePath != null || attrVersion.onlineStorageProvider != null) {
        return true;
      }

      // Get the attribute.
      var attribute = await db.getAttribute(attrVersion.attributeId!);

      // When attribute does not exist we cannot continue.
      if (attribute == null) {
        _logger.e('UpdateConnectionTask: Attribute does not exist, therefore the task is discarded');
        return true;
      }

      // Get the attribute value - in un decrypted form.
      var attributeValue = await db.getAttributeValue(
        attrVersion.attributeId!,
        doNotDecrypt: true,
      );

      // Get external storage connection.
      var extStorage = singletonInjector<ExternalStorageBLImpl>();

      if (!await extStorage.validateGatewayConnection()) {
        _logger.e('UpdateConnectionTask: External storage connection not working, trying again later');
        return false;
      }

      // Sync attribute value to ext storage.
      var storeResult = await extStorage.postAttributeValue(
        attributeValue ??= '',
      );

      if (storeResult == null) {
        _logger.e('UpdateConnectionTask: Syncing attribute to External Storage failed, trying again later');
        return false;
      }

      // Update attribute.
      var versionStorageResult = await db.updateAttributeVersionMetadata(
        attrVersionId,
        {
          'onlineStoragePath': storeResult['path'],
          'onlineStorageProvider': storeResult['storageProvider'],
        },
      );

      if (versionStorageResult == null) {
        _logger.e('UpdateConnectionTask: AttributeVersion could not be updated, trying again later');
        return false;
      }

      // When the attribute has a gateway link, we also need to update the link
      // so its' shares point to the latest version.
      if (attribute.gatewayLinkKey != null) {
        singletonInjector<TaskService>().add('UpdateAttributeLinkTask', {
          'id': attrVersion.id, // Attribute version id.
        });
      }

      return true;
    } catch (e) {
      singletonInjector<Logger>().e(e);
      return false;
    }
  }
}
