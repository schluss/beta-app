import 'dart:convert';

import 'package:logger/logger.dart';
import 'package:schluss_beta_app/business/impl/external_storage_bl_impl.dart';
import 'package:schluss_beta_app/business/interface/background_task.dart';
import 'package:schluss_beta_app/data/impl/attribute_store_dao_impl.dart';
import 'package:schluss_beta_app/data/models/task_model.dart';
import 'package:schluss_beta_app/singleton_injector.dart';

/// Removes an on the external storage stored attribute value.
///
/// Register like this:
///
/// ```dart
///   singletonInjector<TaskService>().add('SyncAttributeRemoveTask', {
///        'id': 1,                                   // Attribute version id.
///        'onlineStoragePath': '/objects/123-456',   // The storage path.
///   });
/// ```
class SyncAttributeRemoveTask implements BackgroundTask {
  final Logger _logger = singletonInjector<Logger>();

  @override
  Future<bool> run(TaskModel model) async {
    try {
      // Parse the parameters from the supplied model.
      Map paramMap = jsonDecode(model.params!);

      if (paramMap.isEmpty || !paramMap.containsKey('id') || !paramMap.containsKey('onlineStoragePath')) {
        _logger.e('SyncAttributeRemoveTask: Supplied parameters are invalid, therefore the task is discarded');
        return Future.value(true);
      }

      var db = AttributeStoreDAOImpl();

      // Get external storage connection.
      var extStorage = singletonInjector<ExternalStorageBLImpl>();

      if (!await extStorage.validateGatewayConnection()) {
        _logger.e('SyncAttributeRemoveTask: External storage connection not working, trying again later');
        return false;
      }

      // Delete attribute value from ext storage.
      var storeResult = await extStorage.deleteAttributeValue(
        paramMap['onlineStoragePath'],
      );

      if (!storeResult) {
        _logger.e('SyncAttributeRemoveTask: Delete attribute value from External Storage failed, trying again later');
        return false;
      }

      // Update attribute version (if it is still in the db).
      if (await db.getAttributeVersion(paramMap['id']) != null) {
        await db.updateAttributeVersionMetadata(paramMap['id'], {
          'onlineStoragePath': '',
          'onlineStorageProvider': '',
        });
      }

      return true;
    } catch (e) {
      singletonInjector<Logger>().e(e);
      return false;
    }
  }
}
