import 'dart:convert';

import 'package:logger/logger.dart';
import 'package:schluss_beta_app/business/interface/background_task.dart';
import 'package:schluss_beta_app/data/impl/contract_store_dao_impl.dart';
import 'package:schluss_beta_app/data/interface/common_dao.dart';
import 'package:schluss_beta_app/data/models/contract/contract_model.dart';
import 'package:schluss_beta_app/data/models/task_model.dart';
import 'package:schluss_beta_app/singleton_injector.dart';

/// Processes a new or updated incoming share from another client and stores (caches)
/// the attribute value locally for quick access.
///
/// Register like this:
///
///   ```dart
///   singletonInjector<TaskService>().add('UpdateConnectionTask', {
///        'action':'stewardshipRemove',                          // Which action? Now only: stewardshipRemove
///        'from' : 'xyz'                                     // The unique clientId at the connection
///   });
///  ```
class UpdateConnectionTask implements BackgroundTask {
  final _commonDAOImpl = singletonInjector<CommonDAO>();
  final Logger _logger = singletonInjector<Logger>();
  var contractStore = ContractStoreDAOImpl();

  @override
  Future<bool> run(TaskModel model) async {
    // Parse the parameters from the supplied model.
    Map paramMap = jsonDecode(model.params!);

    if (paramMap.isEmpty || !paramMap.containsKey('action') || !paramMap.containsKey('from')) {
      _logger.e('UpdateConnectionTask: Supplied parameters are invalid, therefore the task is discarded');
      return Future.value(true);
    }

    if (paramMap['action'] == 'stewardshipRemove') {
      var contract = await contractStore.getByClientKey(paramMap['from']);

      if (contract == null) {
        _logger.e('UpdateConnectionTask: Trying to delete stewardship from an non existing connection');
        return true;
      }

      // update the contract, removing the stewardship specific things
      await _commonDAOImpl.update(
        ContractModel.getStoreName(),
        contract.id!,
        {
          'stewardshipType': null,
          'dataAccessType': 'immediately', // TODO: Refactor: DataAccessType enum should be closer to the datamodel and not only in the view
          'isReleased': false,
        },
      );

      return true;
    } else {
      _logger.d('UpdateConnectionTask: unknown action: ${paramMap['action']}');
      return false;
    }
  }
}
