import 'dart:convert';

import 'package:logger/logger.dart';
import 'package:schluss_beta_app/business/interface/background_task.dart';
import 'package:schluss_beta_app/data/impl/attribute_store_dao_impl.dart';
import 'package:schluss_beta_app/data/impl/contract_store_dao_impl.dart';
import 'package:schluss_beta_app/data/impl/share_store_dao_impl.dart';
import 'package:schluss_beta_app/data/interface/common_dao.dart';
import 'package:schluss_beta_app/data/models/incoming_share_model.dart';
import 'package:schluss_beta_app/data/models/task_model.dart';
import 'package:schluss_beta_app/services/interface/gateway_service.dart';
import 'package:schluss_beta_app/singleton_injector.dart';
import 'package:schluss_beta_app/util/crypto_util.dart';
import 'package:schluss_beta_app/util/vc_util.dart';
import 'package:session_next/session_next.dart';

/// Processes a new or updated incoming share from another client and stores (caches)
/// the attribute value locally for quick access.
///
/// Register like this:
///
///   ```dart
///   singletonInjector<TaskService>().add('ProcessIncomingShareTask', {
///
/// ALWAYS:
///        'from': '95e2f839-ef6b-40a9-b011-cc40d4d5cffb',              // Key of the client where the share is coming from.
///        'gatewayShareId':'xyz',                                      // Unique ID of the share.
///        'hideValues' : true/false                                     // Whether to show or hide the value
///
/// ONLY WHEN IT IS A NEW SHARE:
///        'location': 'https://url.to.gateway/links/abc/shares/xyz',   // Location of the share.
///        'authorization': '9bf5dc696f8379a525fd676b5bb51fa8',         // Authorization header token.
///        'key':'-----BEGIN PGP MESSAGE-----...',                      // Encrypted attribute value decryption key.

///   });
///  ```
class ProcessIncomingShareTask implements BackgroundTask {
  final Logger _logger = singletonInjector<Logger>();

  @override
  Future<bool> run(TaskModel model) async {
    var contractStore = ContractStoreDAOImpl();
    var attributeStore = AttributeStoreDAOImpl();
    var gatewayService = singletonInjector<GatewayService>();
    var commonDAO = singletonInjector<CommonDAO>();

    // Parse the parameters from the supplied model.
    Map paramMap = jsonDecode(model.params!);

    if (paramMap.isEmpty || !paramMap.containsKey('from') || !paramMap.containsKey('gatewayShareId')) {
      _logger.e('ProcessIncomingShareTask: Supplied parameters are invalid, therefore the task is discarded');
      return Future.value(true);
    }

    // When the incoming json does not define whether the attributeid's need to be shown or hidden, set the default to be shown
    if (!paramMap.containsKey('hideValues') || paramMap['hideValues'] == null) {
      paramMap['hideValues'] = false;
    }

    // Get the contract by the from param.
    var contract = await contractStore.getByClientKey(paramMap['from']);

    if (contract == null) {
      // TODO: unlikely scenario: delete the share because it has no link to a contract anymore = floating data.

      _logger.e('ProcessIncomingShareTask: Contract does not exist, therefore the task is discarded');
      return true;
    }

    // Check if contract from compares to [paramMap['from']].
    if (contract.clientKey != paramMap['from']) {
      _logger.e('ProcessIncomingShareTask: Wrong clientKey supplied, therefore the task is discarded');

      // Sender does not match the stored contracts [clientKey], so this is
      // not a trustworthy client: discard.
      return true;
    }

    // Get the share
    var share = await ShareStoreDAOImpl().getIncomingByGatewayShareId(
      paramMap['gatewayShareId'],
    );

    var dataKey = '', location = '', authorization = '';

    // When this is a new share
    if (share == null) {
      // sanity check, to see if we also have the new required params
      if (!paramMap.containsKey('location') || !paramMap.containsKey('authorization') || !paramMap.containsKey('key')) {
        _logger.e('ProcessIncomingShareTask: Invalid params to be able to store an Incoming share, therefore the task is discarded');
        return true;
      }

      // Decrypt the key with private key.
      dataKey = await CryptoUtil.decrypt(paramMap['key']);
      location = paramMap['location'];
      authorization = paramMap['authorization'];
    }

    // Or when an existing share
    else {
      dataKey = share.gatewayAttributeKey!;
      location = share.gatewayPath!;
      authorization = share.gatewayAuthToken!;
    }

    // Download the attribute value and metadata (which are sent with the headers).
    var response = await gatewayService.executeGetRequestWithHeaders(
      location,
      token: authorization,
    );

    // Decrypt the attribute value with dataKey.
    var decryptedValue = await CryptoUtil.decryptSymmetric(
      response.data,
      dataKey,
    );

    // Validate whether VC or not.
    bool isVC = VCUtil.validateVC(decryptedValue);

    // Generate a random attribute encryption key.
    var attributeRandomKey = CryptoUtil.generateRandomKey();

    // Encrypt the random attribute encryption key with the users' public key.
    String attributeKeyEncrypted = await CryptoUtil.encrypt(attributeRandomKey, SessionNext().get<String>('publicKey'));

    // Store the attribute value.
    var attrStoreLocation = await attributeStore.storeAttributeValue(
      decryptedValue,
      attributeRandomKey,
    );

    if (attrStoreLocation == null) {
      _logger.e('ProcessIncomingShareTask: storing the Attribute value failed, trying again later');
      return false;
    }

    // When this is a new share
    if (share == null) {
      // Store the incoming share.
      var incomingShare = IncomingShareModel(
        contractId: contract.id,
        name: response.headers.value('AttributeName'),
        label: response.headers.value('AttributeLabel'),
        group: response.headers.value('AttributeGroup'),
        category: response.headers.value('AttributeCategory'),
        logoUrl: response.headers.value('AttributeLogoUrl'),
        providerName: response.headers.value('AttributeProvider'),
        version: int.tryParse(response.headers.value('AttributeVersion') ?? '1'),
        mediaType: response.headers.value('AttributeMediaType'),
        type: response.headers.value('AttributeType'),
        attrKeyEnc: attributeKeyEncrypted,
        localPath: attrStoreLocation,
        gatewayShareId: paramMap['gatewayShareId'],
        gatewayPath: location,
        gatewayAuthToken: authorization,
        gatewayAttributeKey: dataKey,
        timestamp: DateTime.now().millisecondsSinceEpoch,
        isVC: isVC,
        isHideValue: paramMap['hideValues'],
      );

      await commonDAO.store(IncomingShareModel.getStoreName(), incomingShare.toJson());
    }
    // Or when an existing share
    else {
      // Update the incoming share
      await commonDAO.update(IncomingShareModel.getStoreName(), share.id!, {
        'name': response.headers.value('AttributeName'),
        'label': response.headers.value('AttributeLabel'),
        'group': response.headers.value('AttributeGroup'),
        'category': response.headers.value('AttributeCategory'),
        'logoUrl': response.headers.value('AttributeLogoUrl'),
        'providerName': response.headers.value('AttributeProvider'),
        'version': int.tryParse(response.headers.value('AttributeVersion') ?? '1'),
        'mediaType': response.headers.value('AttributeMediaType'),
        'type': response.headers.value('AttributeType'),
        'attrKeyEnc': attributeKeyEncrypted,
        'localPath': attrStoreLocation,
        'isVC': isVC,
        'isHideValue': paramMap['hideValues'],
      });

      // Finally, cleanup by deleting the old stored value from disk.
      attributeStore.deleteAttributeValue(share.localPath!);
    }

    return true;
  }
}
