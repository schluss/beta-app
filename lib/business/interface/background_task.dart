import 'package:schluss_beta_app/data/models/task_model.dart';

/// Interfaces for all background task implementations.
abstract class BackgroundTask {
  // This methods is called by task service when running the background task.
  Future<bool> run(TaskModel model);
}
