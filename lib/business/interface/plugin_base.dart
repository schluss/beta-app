abstract class PluginBase {
  void runPlugin(context, Function callBack, String siteUrl, String trigger, String pluginName);

  Future<Map<String, dynamic>> getExtractedData(String filePath);
}
