import 'package:flutter/cupertino.dart';
import 'package:schluss_beta_app/view_model/provider_config_view_model.dart';
import 'package:schluss_beta_app/view_model/request_view_model.dart';

/// Plugins manager. Help to execute the plugin dynamically.
abstract class PluginManagerBL {
  void executePlugin(
    BuildContext context,
    RequestViewModel requestModel,
    int providerIndex,
    Function callback,
  );

  void executePluginDirect(
    context,
    Function callBack,
    String siteUrl,
    String trigger, {
    String pluginName,
    Function(String userName, String password) storeCredentialCallback,
    String? userName,
    String? password,
  });

  void onExecutePlugin(
    BuildContext context,
    ProviderConfigViewModel providerModel,
    Function callback,
  );

  Future<RequestViewModel> getData(
    RequestViewModel requestModel,
    String pluginData,
  );

  /// Gets progress stream value from plugin side.
  Stream<int> getProgressValue(RequestViewModel requestViewModel);
}
