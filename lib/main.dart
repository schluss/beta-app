import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_dotenv/flutter_dotenv.dart';
import 'package:flutter_localizations/flutter_localizations.dart';
import 'package:logger/logger.dart';
import 'package:one_context/one_context.dart';
import 'package:schluss_beta_app/constants/theme.dart';
import 'package:schluss_beta_app/constants/ui_constants.dart';
import 'package:schluss_beta_app/routes.dart' as app_routes;
import 'package:schluss_beta_app/singleton_injector.dart';
import 'package:schluss_beta_app/translations/i18n.dart';
import 'package:session_next/session_next.dart';
import 'package:uni_links/uni_links.dart';

void main() async {
  WidgetsFlutterBinding.ensureInitialized();

  // Initiate singleton classes.
  setupSingletonInjector();

  // Load environmental variables.
  await dotenv.load(fileName: '.env');

  runApp(const MyApp());
}

class MyApp extends StatefulWidget {
  const MyApp({Key? key}) : super(key: key);

  @override
  MyAppState createState() => MyAppState();
}

class MyAppState extends State<MyApp> with WidgetsBindingObserver {
  final Logger logger = singletonInjector<Logger>();
  @override
  void initState() {
    WidgetsBinding.instance.addObserver(this);
    super.initState();

    initListenUniLinks();
  }

  @override
  void dispose() {
    WidgetsBinding.instance.removeObserver(this);

    super.dispose();
  }

  /// Listens to uni links for app openings.
  void initListenUniLinks() async {
    // If the widget was removed from the tree while the asynchronous platform
    // message was in flight, we want to discard the reply rather than calling
    // [setState] to update our non-existent appearance.
    linkStream.listen((String? link) {
      logger.d('Received from Uni link: $link');
    }, onError: (err) {
      logger.e('got err: $err');
    });
  }

  @override
  Widget build(BuildContext context) {
    SystemChrome.setPreferredOrientations([
      DeviceOrientation.portraitUp, // Make the orientation of the app into portrait every time.
    ]);

    SystemChrome.setSystemUIOverlayStyle(const SystemUiOverlayStyle(
      statusBarColor: Colors.transparent, // Make the status bar transparent.
    ));

    return AnnotatedRegion<SystemUiOverlayStyle>(
      value: SystemUiOverlayStyle.light.copyWith(
        statusBarColor: UIConstants.transparent, // Make the status bar transparent.
        statusBarIconBrightness: Brightness.dark, // Make the status bar icons dark. Only on android.
        statusBarBrightness: Brightness.light, // Make the status bar icons light. Only on IOS.
        systemNavigationBarColor: UIConstants.primaryColor, // Make the navigation bar white. Only on android.
        systemNavigationBarIconBrightness: Brightness.dark, // Make the navigation bar icons dark. Only on android.
      ),
      child: Listener(
        onPointerDown: _handleUserInteraction,
        child: MaterialApp(
          locale: null,
          builder: OneContext().builder,
          navigatorKey: OneContext().navigator.key,
          debugShowCheckedModeBanner: false,
          localizationsDelegates: const [
            I18nDelegate(),
            GlobalMaterialLocalizations.delegate,
            GlobalWidgetsLocalizations.delegate,
            GlobalCupertinoLocalizations.delegate,
            DefaultCupertinoLocalizations.delegate,
          ],
          supportedLocales: I18nDelegate.supportedLocals,
          title: 'Schluss',
          theme: appTheme,
          initialRoute: '/',
          onGenerateRoute: app_routes.generateRoute,
        ),
      ),
    );
  }

  /// Do required things for every interaction the user does with the app, for example session timer updating
  void _handleUserInteraction([_]) {
    SessionNext().update();
  }
}
