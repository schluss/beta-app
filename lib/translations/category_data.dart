import 'package:flutter/cupertino.dart';

/// Uses to retrieve preloaded category data.
class CategoryData {
  CategoryDTO get identity => getCategory('identity');

  CategoryDTO get finance => getCategory('finance');

  CategoryDTO get health => getCategory('health');

  CategoryDTO get work => getCategory('work');

  CategoryDTO get home => getCategory('home');

  CategoryDTO get transport => getCategory('transport');

  CategoryDTO get education => getCategory('education');

  CategoryDTO get general => getCategory('general');

  static const _categoryValues = {
    'identity': {
      'translate_key': 'identityCategory',
      'icon_path': 'assets/images/media_icon.png',
      'color': '#cd472a',
    },
    'finance': {
      'translate_key': 'financeCategory',
      'icon_path': 'assets/images/media_icon.png',
      'color': '#cd472a',
    },
    'health': {
      'translate_key': 'healthCategory',
      'icon_path': 'assets/images/media_icon.png',
      'color': '#cd472a',
    },
    'work': {
      'translate_key': 'workCategory',
      'icon_path': 'assets/images/media_icon.png',
      'color': '#cd472a',
    },
    'home': {
      'translate_key': 'homeCategory',
      'icon_path': 'assets/images/media_icon.png',
      'color': '#cd472a',
    },
    'transport': {
      'translate_key': 'transportCategory',
      'icon_path': 'assets/images/media_icon.png',
      'color': '#cd472a',
    },
    'education': {
      'translate_key': 'educationCategory',
      'icon_path': 'assets/images/media_icon.png',
      'color': '#cd472a',
    },
    'general': {
      'translate_key': 'generalCategory',
      'icon_path': 'assets/images/media_icon.png',
      'color': '#cd472a',
    },
  };

  CategoryDTO getCategory(String key) => CategoryDTO.fromCategoryData(
        _categoryValues[key]!,
      );

  List<CategoryDTO> getAll() => _categoryValues.entries
      .map(
        (e) => getCategory(e.key),
      )
      .toList();
}

class CategoryDTO {
  String? translateKey;
  String? iconPath;
  Color? color;

  CategoryDTO({
    this.translateKey,
    this.iconPath,
    this.color,
  });

  factory CategoryDTO.fromCategoryData(Map<String, String> model) {
    // TODO: can be implement to get path directly from network or local.
    String iconPath = model['icon_path']!;
    Color color = Color(_getColorFromHex(model['color']!));
    return CategoryDTO(
      translateKey: model['translate_key'],
      iconPath: iconPath,
      color: color,
    );
  }

  /// Gets color from hex color codes.
  static int _getColorFromHex(String hexColor) {
    hexColor = hexColor.toUpperCase().replaceAll('#', '');
    if (hexColor.length == 6) {
      hexColor = 'FF$hexColor';
    }
    return int.parse(hexColor, radix: 16);
  }
}
