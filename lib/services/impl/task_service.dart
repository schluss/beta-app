import 'dart:async';
import 'dart:convert';

import 'package:logger/logger.dart';
import 'package:schluss_beta_app/business/impl/tasks/process_incoming_share_task.dart';
import 'package:schluss_beta_app/business/impl/tasks/sync_attribute_remove_task.dart';
import 'package:schluss_beta_app/business/impl/tasks/sync_attribute_task.dart';
import 'package:schluss_beta_app/business/impl/tasks/update_attribute_link_task.dart';
import 'package:schluss_beta_app/business/impl/tasks/update_connection_task.dart';
import 'package:schluss_beta_app/business/interface/background_task.dart';
import 'package:schluss_beta_app/data/impl/task_store_dao_impl.dart';
import 'package:schluss_beta_app/data/models/task_model.dart';
import 'package:schluss_beta_app/singleton_injector.dart';

/// Handles automated background (async form) processing of tasks.
// TODO: add possibility of processing multiple tasks simultaneously,
//  preferably with a limit on concurrency because we need database connectivity
//  etc we cannot use isolates unfortunately. Maybe in the future we can really
//  isolate these from the app.
class TaskService {
  final Logger _logger = singletonInjector<Logger>();
  final taskStore = TaskStoreDAOImpl();

  static bool _isProcessing = false;
  static const int maxRetryCount = 2;

  /// All possible tasks registered here.
  final _tasksFactory = <String, BackgroundTask Function()>{
    'SyncAttributeTask': () => SyncAttributeTask(),
    'SyncAttributeRemoveTask': () => SyncAttributeRemoveTask(),
    'ProcessIncomingShareTask': () => ProcessIncomingShareTask(),
    'UpdateAttributeLinkTask': () => UpdateAttributeLinkTask(),
    'UpdateConnectionTask': () => UpdateConnectionTask(),
  };

  /// Tries to create an instance of the task and returns it.
  BackgroundTask? _createInstanceOf(String taskName) {
    if (!_tasksFactory.containsKey(taskName)) {
      return null;
    }

    final createInstance = _tasksFactory[taskName];
    // Same as [createInstance()] but null safety.
    return createInstance?.call();
  }

  /// Adds a task to be processed in the background and start processing
  /// other tasks (if any).
  void add(String name, Map params) async {
    // Add the task to the task db.
    TaskModel model = TaskModel(name: name, params: jsonEncode(params));

    await taskStore.add(model);

    // Initiate execution of tasks.
    processTasks();
  }

  /// Adds a task, process it immediately (you can even await it) and then
  /// start processing other tasks (if any).
  Future<bool> addWithPriority(String name, Map params) async {
    // Add the task to the task db.
    TaskModel model = TaskModel(name: name, params: jsonEncode(params));

    await taskStore.add(model);

    return await _processTask(model);
  }

  /// Resets the tasks with status [failed] to [pending] and then starts
  /// processing all tasks one by one.
  void processAndRetryFailedTasks() async {
    // Get all tasks that failed and get another retry.
    var tasks = await taskStore.getAsList(status: TaskStatus.retry.name);

    // Reset status to [pending], penalty is an increased [retryCount].
    for (var task in tasks) {
      task.retryCount++;
      task.status = TaskStatus.pending;

      await taskStore.update(task);
    }

    // Initiate execution of tasks.
    processTasks();
  }

  /// Starts processing tasks one by one and only stops when all tasks are processed.
  void processTasks() async {
    if (_isProcessing) {
      return;
    }

    _isProcessing = true;

    // Get next available task from the db.
    var nextTask = await taskStore.getNext(TaskStatus.pending.name);

    // When there are no next tasks in line - stop processing.
    if (nextTask == null) {
      _isProcessing = false;
      _logger.i('TaskService: no tasks to run');
      return;
    }

    _processTask(nextTask);
  }

  Future<bool> _processTask(TaskModel taskModel) async {
    // Update task status to [executing].
    taskModel.status = TaskStatus.executing;
    await taskStore.update(taskModel);

    // Try to instantiate the task.
    var task = _createInstanceOf(taskModel.name!);

    // Check if task exists.
    if (task == null) {
      _logger.e('TaskService: Processing task failed because task ${taskModel.name!} does not exist:\n${jsonEncode(
        taskModel.toJson(),
      )}');

      await _handleFailedTask(taskModel);

      _isProcessing = false;

      // Process next task from queue.
      processTasks();

      return false;
    }

    var result = false;

    // Run the task.
    try {
      result = await task.run(taskModel);
    } catch (e) {
      _logger.e('TaskService: exception during processing task ${taskModel.id} - ${taskModel.name}, details:');
      _logger.e(e);
      result = false;
    }

    // When success.
    if (result) {
      await taskStore.delete(taskModel.id!);

      _logger.i(
        'TaskService: Processed task ${taskModel.id} - ${taskModel.name}',
      );
    }

    // When failed.
    else {
      await _handleFailedTask(taskModel);
    }

    _isProcessing = false;

    // Process next task from queue.
    processTasks();

    return result;
  }

  /// Updates a task by setting status to retry (for another retry) or failed
  /// (where it sits 'forever') if the task if the [maxRetryCount] is reached.
  Future<void> _handleFailedTask(TaskModel model) async {
    _logger.e('TaskService: Processing task failed:');
    _logger.e(jsonEncode(model.toJson()));

    if (model.retryCount >= maxRetryCount) {
      model.status = TaskStatus.failed;
      _logger.e('Max retries reached, task failed permanently');
    } else {
      model.status = TaskStatus.retry;
    }

    await taskStore.update(model);
  }
}
