/*
 * @author Manuja Jayawardana
 * @email mjayawardana@yukon.lk
 * @create date 2020-08-26
 */
import 'package:connectivity_plus/connectivity_plus.dart';
import 'package:dio/dio.dart';
import 'package:flutter_dotenv/flutter_dotenv.dart';
import 'package:logger/logger.dart';
import 'package:schluss_beta_app/services/interface/common_service.dart';
import 'package:schluss_beta_app/singleton_injector.dart';

class CommonServiceImpl implements CommonService {
  final Logger _logger = singletonInjector<Logger>();

  /// Checks internet connectivity by pinging into known server.
  @override
  Future<bool> checkInternetConnectivity() async {
    try {
      // Check if there is a network connection.
      if ((await getNetworkConnectionType()) == ConnectivityResult.none) {
        return false;
      }

      String testingUrl = dotenv.env['CONNECTIVITY_TEST_URL']!;

      Dio httpClient = Dio();
      httpClient.options.connectTimeout = const Duration(seconds: 10);

      await httpClient.get(testingUrl);

      _logger.i('Internet connectivity check: Success');

      return Future.value(true);
    } on DioException catch (e) {
      _logger.e('Internet connectivity check: Dio Error: $e.');
    } catch (e) {
      _logger.e('Internet connectivity check  : $e.');
    }
    return false;
  }

  /// Checks whether device is connected to WiFi/mobile-data or none of them.
  Future<ConnectivityResult> getNetworkConnectionType() async {
    return Connectivity().checkConnectivity();
  }
}

/// Checks whether device is connected to WiFi/mobile-data or none of them.
Future listenToConnectivityChanges() async {
  Connectivity().onConnectivityChanged.listen((ConnectivityResult result) {
    final Logger logger = singletonInjector<Logger>();

    String connectionStatus;

    switch (result) {
      case ConnectivityResult.ethernet:
        connectionStatus = 'You\'re connected to the internet using Ethernet :)';
        break;
      case ConnectivityResult.wifi:
        connectionStatus = 'You\'re connected to the internet using WiFi :)';
        break;
      case ConnectivityResult.mobile:
        connectionStatus = 'You\'re connected to the internet using mobile data :|';
        break;
      case ConnectivityResult.none:
      default:
        connectionStatus = 'You\'re not connected to the internet :(';
        break;
    }

    logger.d(connectionStatus);
  });
}
