/*
 * @author Manuja Jayawardana
 * @email mjayawardana@yukon.lk
 * @create date 2021-03-23
 */

import 'package:dio/dio.dart';
import 'package:schluss_beta_app/constants/http_response_code_constants.dart';
import 'package:schluss_beta_app/services/interface/gateway_service.dart';
import 'package:schluss_beta_app/util/http_response_handler_util.dart';
import 'package:schluss_beta_app/view_model/gateway_viewmodel.dart';
import 'package:synchronized/synchronized.dart' as synchronized;

/// Gateways service impl.
///
/// This classes is using to communicate with gateway. (External Storage Gateway or Gateway)
/// This class can be used as normal service or background service.
/// Note :
/// Since some methods may need to executed as background tasks do not use
/// singleton classes if the method needs to used in [BackgroundExecutor] class.
/// If any errors occurred please check objects are accessible through the method.
// TODO: find a solution from get_it library to initiate instance to work with background executer.
class GatewayServiceImpl implements GatewayService {
  final Dio _httpClient = Dio();

  final HttpResponseHandlerUtil _httpResponseHandler = HttpResponseHandlerUtil();

  /// Registers new account in gateway.
  /// @param
  /// endpoint - user register API URL in gateway.
  @override
  Future<GatewayViewModel> registerNewAccount(endpoint) async {
    // Use this object to prevent concurrent access to data.
    var lock = synchronized.Lock();

    GatewayViewModel gatewayViewModel;

    try {
      _httpClient.options = BaseOptions(
        contentType: Headers.jsonContentType,
      );

      // Lock mechanism to prevent concurrent access to register method.
      var response = await lock.synchronized(() async {
        return await _httpClient.post(endpoint);
      });

      // Check if the request is success.
      if (HttpResponseCodeConstants.responseOk.contains(response.statusCode!)) {
        gatewayViewModel = GatewayViewModel.fromJson(response.data);
        return Future.value(gatewayViewModel);
      } else {
        // TODO: remove the manual throwing and make the method nullable.
        throw Exception(response.data.message);
      }
    } on DioException catch (error) {
      return await _httpResponseHandler.validateResponse(error.response!.statusCode);
    } catch (e) {
      return Future.error(e);
    }
  }

  @override
  Future<Map<String, dynamic>?> executePostRequest(
    String endpoint,
    dynamic body, {
    Map<String, dynamic>? headers,
  }) async {
    try {
      var contentType = Headers.jsonContentType;

      if (body.runtimeType is String) {
        contentType = Headers.textPlainContentType;
      }

      _httpClient.options = BaseOptions(
        contentType: contentType,
        headers: headers,
      );

      var response = await _httpClient.post<Map<String, dynamic>>(
        endpoint,
        data: body,
      );

      if (HttpResponseCodeConstants.responseOk.contains(response.statusCode!)) {
        return Future.value(response.data);
      } else {
        return Future.value(null);
      }
    } on DioException catch (error) {
      return await _httpResponseHandler.validateResponse(error.response!.statusCode);
    } catch (e) {
      return Future.error(e);
    }
  }

  @override
  Future<Map<String, dynamic>?> executePutRequest(
    String endpoint,
    dynamic body, {
    Map<String, dynamic>? headers,
  }) async {
    try {
      var contentType = Headers.jsonContentType;

      if (body.runtimeType is String) {
        contentType = Headers.textPlainContentType;
      }

      _httpClient.options = BaseOptions(
        contentType: contentType,
        headers: headers,
      );

      var response = await _httpClient.put<Map<String, dynamic>>(
        endpoint,
        data: body,
      );

      if (HttpResponseCodeConstants.responseOk.contains(response.statusCode!)) {
        return Future.value(response.data);
      } else {
        return Future.value(null);
      }
    } on DioException catch (error) {
      return await _httpResponseHandler.validateResponse(error.response!.statusCode);
    } catch (e) {
      return Future.error(e);
    }
  }

  @override
  Future<Map<String, dynamic>?> executeDeleteRequest(
    String endpoint,
    Map<String, dynamic>? headers,
  ) async {
    try {
      _httpClient.options = BaseOptions(
        contentType: Headers.jsonContentType,
        headers: headers,
      );

      var response = await _httpClient.delete<Map<String, dynamic>>(endpoint);

      if (HttpResponseCodeConstants.responseOk.contains(response.statusCode!)) {
        return Future.value(response.data);
      } else {
        return Future.value(null);
      }
    } on DioException catch (error) {
      return await _httpResponseHandler.validateResponse(error.response!.statusCode);
    } catch (e) {
      return Future.error(e);
    }
  }

  /// Gets auth token from gateway by sending secret value and key value.
  /// @param
  /// endpoint - User AUTH API URL in gateway.
  /// payload  - Secret Data value ({ secret : "" , key : ""})
  @override
  Future<GatewayViewModel> getAuthToken(String endpoint, String payload) async {
    GatewayViewModel gatewayViewModel;
    try {
      _httpClient.options = BaseOptions(
        contentType: Headers.jsonContentType,
      );
      var response = await _httpClient.post(endpoint, data: payload);
      // Check if the request is success.
      if (HttpResponseCodeConstants.responseOk.contains(response.statusCode!)) {
        gatewayViewModel = GatewayViewModel.fromJson(response.data);
        return Future.value(gatewayViewModel);
      } else {
        // TODO: remove the manual throwing and make the method nullable.
        throw Exception(response.data.message);
      }
    } on DioException catch (error) {
      return await _httpResponseHandler.validateResponse(error.response!.statusCode);
    } catch (e) {
      return Future.error(e);
    }
  }

  /// Gets connection ink from gateway.
  /// @param
  ///    endpoint - User API URL in gateway
  ///    payload  -
  ///
  /// ```dart
  ///    Data body ({
  ///                  type : 'connectRequest'
  ///                  firstName : [vc]*
  ///                  lastName : [vc]*
  ///                  gatewayUrl : [link-to-gateway of Client A]
  ///                  clientKey : [client A key]
  ///                  sessionId : [id from record in ConnectRequest table]
  ///                })
  /// ```
  @override
  Future<GatewayViewModel> getConnectionLink(
    String endpoint,
    String? token,
    String payload,
  ) async {
    GatewayViewModel gatewayViewModel;
    try {
      _httpClient.options = BaseOptions(
        contentType: Headers.jsonContentType,
        headers: {
          'Authorization': token,
        },
      );

      var response = await _httpClient.post(endpoint, data: payload);

      // Check if the request is success.
      if (HttpResponseCodeConstants.responseOk.contains(response.statusCode!)) {
        gatewayViewModel = GatewayViewModel.fromJson(response.data);
        return Future.value(gatewayViewModel);
      } else {
        // TODO: remove the manual throwing and make the method nullable.
        throw Exception(response.data.message);
      }
    } on DioException catch (error) {
      return await _httpResponseHandler.validateResponse(error.response!.statusCode);
    } catch (e) {
      return Future.error(e);
    }
  }

  // TODO: use this generic GET method in other GET requests to improve the code quality.
  //  Generic execute GET request to gateway service.
  @override
  Future<GatewayViewModel> executeGetRequest(
    String endpoint, {
    String? token,
  }) async {
    GatewayViewModel gatewayViewModel;
    try {
      _httpClient.options = BaseOptions(
        contentType: Headers.jsonContentType,
      );
      if (token != null) {
        _httpClient.options.headers = {'Authorization': token};
      }

      var response = await _httpClient.get(endpoint);

      // Check if the request is success.
      if (HttpResponseCodeConstants.responseOk.contains(response.statusCode!)) {
        gatewayViewModel = GatewayViewModel.fromJson(response.data);
        return Future.value(gatewayViewModel);
      } else {
        // TODO: remove the manual throwing and make the method nullable.
        throw Exception(response.data.message);
      }
    } on DioException catch (error) {
      return await _httpResponseHandler.validateResponse(error.response!.statusCode);
    } catch (e) {
      return Future.error(e);
    }
  }

  @override
  Future<Response> executeGetRequestWithHeaders(
    String url, {
    String? token,
  }) async {
    try {
      _httpClient.options = BaseOptions(
        contentType: Headers.jsonContentType,
      );
      if (token != null) {
        _httpClient.options.headers = {'Authorization': token};
      }

      var response = await _httpClient.get(url);
      // Check if the request is success.
      if (HttpResponseCodeConstants.responseOk.contains(response.statusCode!)) {
        return Future.value(response);
      } else {
        // TODO: remove the manual throwing and make the method nullable.
        throw Exception(response.data.message);
      }
    } on DioException catch (error) {
      return await _httpResponseHandler.validateResponse(error.response!.statusCode);
    } catch (e) {
      return Future.error(e);
    }
  }
}
