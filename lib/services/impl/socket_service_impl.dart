/*
 * @author Manuja Jayawardana
 * @email mjayawardana@yukon.lk
 * @create date 2021-09-26
 */

import 'dart:async';
import 'dart:convert';

import 'package:flutter_dotenv/flutter_dotenv.dart';
import 'package:get_it/get_it.dart';
import 'package:logger/logger.dart';
import 'package:schluss_beta_app/controller/contract_controller.dart';
import 'package:schluss_beta_app/services/impl/task_service.dart';
import 'package:schluss_beta_app/services/interface/socket_service.dart';
import 'package:schluss_beta_app/singleton_injector.dart';
import 'package:schluss_beta_app/util/crypto_util.dart';
import 'package:socket_io_client/socket_io_client.dart';

class SocketServiceImpl with Disposable implements SocketService {
  Map<String, StreamController> _subscriptions = {};
  final Logger _logger = singletonInjector<Logger>();
  final StreamController<String> _deleteResponseOnReceiveStreamController = StreamController<String>.broadcast();
  final StreamController<String> _attributeShareOnReceiveStreamController = StreamController<String>.broadcast();
  final StreamController<String> _attributeShareUpdateOnReceiveStreamController = StreamController<String>.broadcast();
  final StreamController<String> _deleteShareAttributeOnReceiveStreamController = StreamController<String>.broadcast();
  final StreamController<String> _updateConnectionOnReceiveStreamController = StreamController<String>.broadcast();

  Socket? socket;
  Map? secretData;

  // TODO: there is a 'bug' in the socketIO client, where messages are received
  //  duplicated. Check this over time to see if the bug is resolved in
  //  [https://github.com/rikulo/socket.io-client-dart] hold the last received
  //  message to be able to compare it with the current received message, so we
  //  can find out if the message is duplicated.
  dynamic lastMessage;

  @override
  Future<void> disconnect() async {
    if (socket == null) {
      return;
    }

    socket!.disconnect();
    socket!.dispose();
    return;
  }

  /// Starts and maintains an active socket connection at the gateway where the user registered.
  @override
  Future<void> connect() async {
    // If connect is called when an active connection is already there, discard and return.
    if (socket != null && socket!.connected) {
      return;
    }

    var ugwUrl = dotenv.env['UGW_URL'];

    secretData = await CryptoUtil.getGatewaySecretData();

    // Initiate socket server.
    if (secretData!.containsKey('key')) {
      socket = io(
          ugwUrl,
          OptionBuilder()
              .setTransports(['websocket'])
              .disableAutoConnect() // Disable auto-connection at instantiation.
              .enableReconnection() // Enable auto-reconnect when connection is lost to the gateway while connected.
              .enableForceNewConnection() // Fix issues at Android, where duplicated messages where received multiple times. See [https://github.com/rikulo/socket.io-client-dart/issues/258].
              .setAuth({'token': secretData!['key'], 'secret': secretData!['secret']})
              .build());

      socket!.connect();

      // Handle [onConnect] callback.
      socket!.onConnect((_) {
        onSocketConnect();
      });

      socket!.onDisconnect((_) {
        _logger.i('Socket service: Disconnected');
      });

      // Handle [onMessage] callback.
      socket!.on('message', (data) {
        // Only when current message is not a duplicate.
        if (lastMessage != data) {
          onSocketMessage(data);
          lastMessage = data;
        } else {
          // Log the bug, to know it's still happening.
          _logger.d('Socket service bug: duplicated message received:');
          _logger.d(data);
        }
      });

      // Handle [onConnection] error call back.
      socket!.on('connect_error', (data) {
        _logger.e(data);
      });

      // Handle error call back.
      socket!.on('error', (data) {
        _logger.e(data);
      });
    }
  }

  void onSocketConnect() {
    _logger.i('Socket service: Connection established');
    // Initiate background socket listening functions.
    initBackgroundRunningSocket();
  }

  /// Subscribes to incoming socket messages.
  ///
  /// Message: the type of incoming message.
  /// Func: the function that's executed when the message is received, for example:
  ///
  /// ```dart
  /// _conn.subscribe('ASpecificMessage', (data) async {
  ///   Print(data); // contains the message content
  /// })
  /// ```
  @override
  void subscribe(String message, StreamController sController) {
    // Only a non existing subscription can be added to prevent double subscriptions.
    if (!_subscriptions.containsKey(message)) {
      _subscriptions[message] = sController;
    }
  }

  /// Receives [OnMessage] from the socket server.
  void onSocketMessage(String data) {
    Map message = json.decode(data);
    _logger.d('Socket message received:');
    _logger.d(message);
    if (!message.containsKey('type')) {
      _logger.i('Socket service: incorrect message format: no type given');
      return;
    }

    // When no subscription found for incoming message.
    if (!_subscriptions.containsKey(message['type'])) {
      _logger.i(
        'Socket service: no subscription for incoming message: ${message['type']}',
      );
      return;
    }

    if (_subscriptions[message['type']] == null) {
      _logger.i('Socket service: no handler for: ${message['type']}');
      return;
    }

    // Add the message in to the [StreamController].
    _subscriptions[message['type']]!.add(data);
  }

  /// Removes the subscription of [message].
  @override
  Future<void> unsubscribe(String message) async {
    if (!_subscriptions.containsKey(message)) {
      return;
    }

    await _subscriptions[message]!.close();

    _subscriptions.remove(message);
    return;
  }

  /// Shares the token to a identified socket address.
  @override
  Future<bool> shareToken(String toSocket) async {
    if (socket != null && secretData!.containsKey('key')) {
      try {
        Map tokenData = {'token': secretData!['key']};

        if (!socket!.connected) {
          await connect();
        }

        socket!.emit('message', {
          'to': toSocket,
          'message': json.encode(
            tokenData,
          )
        });
        return Future.value(true);
      } catch (e) {
        _logger.e(e);
      }
    } else {
      _logger.e('Socket Data not initialized');
    }
    return Future.value(false);
  }

  /// Shares the token to a identified socket address.
  @override
  Future<bool> shareMessage(String toSocket, String message) async {
    if (socket != null && secretData!.containsKey('key')) {
      try {
        if (!socket!.connected) {
          await connect();
        }

        socket!.emit('message', {'to': toSocket, 'message': message});
        return Future.value(true);
      } catch (e) {
        _logger.e(e);
      }
    } else {
      _logger.e('Socket Data not initialized');
    }
    return Future.value(false);
  }

  // TODO: can be optimized this function if we use specific socket.io channel.
  void initBackgroundRunningSocket() {
    // Initialize delete callback socket.
    initDeleteSocketConnection();

    // Initialize attribute share socket.
    initAttributeShareListener();

    // Initialize listening for incoming attribute share updates.
    initAttributeShareUpdateListener();

    // Initialize listening for remove attribute share updates.
    initDeleteContractShareAttribute();

    // Initialize listening for connection updates
    initUpdateConnection();
  }

  void initUpdateConnection() {
    subscribe('updateConnection', _updateConnectionOnReceiveStreamController);
    _updateConnectionOnReceiveStreamController.stream.listen((data) async {
      Map message = json.decode(data);

      // Validate message.
      if (!message.containsKey('from') || !message.containsKey('action')) {
        return;
      }

      String action = message['action'];

      // When the stewardship needs to be removed
      if (action == 'stewardshipRemove') {
        // Register a task to process the incoming attribute.
        singletonInjector<TaskService>().add('UpdateConnectionTask', {
          'action': message['action'],
          'from': message['from'],
        });
      }
      // Other actions like updating the name or other things: future
    });
  }

  // TODO: should be implemented in nicer/generic way. Initialize delete
  //  callback socket to receive [connectRemove] messages.
  void initDeleteSocketConnection() {
    subscribe('connectRemove', _deleteResponseOnReceiveStreamController);
    _deleteResponseOnReceiveStreamController.stream.listen((data) async {
      Map message = json.decode(data);
      String clientKey = message['clientKey'];
      ContractController contractController = ContractController();
      contractController.deleteConnectionByClientKey(clientKey);
    });
  }

  // TODO: should be implemented in nicer/generic way. Listens for incoming
  //  [attributeShare] socket messages and processes them.
  void initAttributeShareListener() {
    subscribe('attributeShare', _attributeShareOnReceiveStreamController);
    _attributeShareOnReceiveStreamController.stream.listen((data) async {
      Map<String, dynamic> message = json.decode(data);

      // Validate message.
      if (!message.containsKey('from') || !message.containsKey('shares')) {
        return;
      }

      List<Map<String, dynamic>> shares = List<Map<String, dynamic>>.from(message['shares']);

      for (var share in shares) {
        // Validate share.
        if (!share.containsKey('location') || !share.containsKey('authorization') || !share.containsKey('key')) {
          continue;
        }

        // Register a task to process the incoming attribute.
        singletonInjector<TaskService>().add('ProcessIncomingShareTask', {
          'from': message['from'],
          'gatewayShareId': share['gatewayShareId'],
          'location': share['location'],
          'authorization': share['authorization'],
          'key': share['key'],
          'hideValues': message.containsKey('hideValues') ? message['hideValues'] : false,
        });
      }
    });
  }

  void initAttributeShareUpdateListener() {
    subscribe('attributeShareUpdate', _attributeShareUpdateOnReceiveStreamController);
    _attributeShareUpdateOnReceiveStreamController.stream.listen((data) async {
      Map message = json.decode(data);

      // Validate message.
      if (!message.containsKey('shares') || !message.containsKey('from')) {
        return;
      }

      // the attributeShareUpdate message contains a list of one or more shares to be updated, split them into separate update tasks
      // Loop through the gatewayShareIds:
      for (String gatewayShareId in message['shares']) {
        // Register a task to process the incoming attribute.
        singletonInjector<TaskService>().add('ProcessIncomingShareTask', {
          'from': message['from'],
          'gatewayShareId': gatewayShareId,
          'hideValues': message.containsKey('hideValues') ? message['hideValues'] : false,
        });
      }
    });
  }

  /// Initializes delete share callback socket to receive [shareRemove] messages.
  // TODO: should be implemented in nicer/generic way.
  void initDeleteContractShareAttribute() {
    subscribe('shareRemove', _deleteShareAttributeOnReceiveStreamController);
    _deleteShareAttributeOnReceiveStreamController.stream.listen((data) async {
      Map message = json.decode(data);
      String gatewayShareId = message['gatewayShareId'];
      ContractController contractController = ContractController();
      contractController.deleteIncomingSharedAttribute(gatewayShareId);
    });
  }

  @override
  FutureOr onDispose() async {
    // Disconnect from the socket server and dispose it.
    await disconnect();

    // Close all stream subscriptions before continuing disposal.
    for (var t in _subscriptions.entries) {
      await t.value.close();
    }

    _subscriptions = {};
  }
}
