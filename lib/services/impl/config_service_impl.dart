import 'package:dio/dio.dart';
import 'package:logger/logger.dart';
import 'package:schluss_beta_app/services/interface/config_service.dart';
import 'package:schluss_beta_app/singleton_injector.dart';
import 'package:schluss_beta_app/view_model/data_request_config_view_model.dart';

class ConfigurationServiceImpl implements ConfigurationService {
  final Logger _logger = singletonInjector<Logger>();

  /// Retrieves application configuration model using [configUrl] and [scope].
  @override
  Future<DataRequestConfigViewModel> retrieveAppConfigByScope(
    String? configUrl,
    String? scope,
  ) async {
    Map<dynamic, dynamic> response;
    DataRequestConfigViewModel dataRequest;

    try {
      response = await retrieveJSONFromUrl(configUrl!);

      if (response.isEmpty) {
        throw ('Url to data request is invalid: $configUrl');
      }

      dataRequest = DataRequestConfigViewModel.fromJson(
        response as Map<String, dynamic>,
        scope,
        configUrl,
      );

      // Retrieve providers configurations from URL.
      for (var provider in dataRequest.providers) {
        var providerResponse = await retrieveJSONFromUrl(provider.src!);

        if (providerResponse.isEmpty) {
          throw ('Url to data provider is invalid: ${provider.src!}');
        }

        provider.fromConfigJson(providerResponse as Map<String, dynamic>);
      }

      return Future.value(dataRequest);
    } catch (e) {
      _logger.e('Loading and parsing data request failed: $e.');

      return Future.error(Exception('Error occurred: $e'));
    }
  }

  /// Retrieves the json files using URL.
  Future<Map> retrieveJSONFromUrl(String configUrl) async {
    _logger.i('Data Requesting From : $configUrl');

    try {
      Dio httpClient = Dio();

      var response = await httpClient.get(configUrl);

      Map? configMap = response.data;

      return Future.value(configMap);
    } on DioException catch (error) {
      _logger.e('Dio error occurred: $error.');

      return Future.error(Exception(error.message));
    } catch (e) {
      _logger.e('Request failed: $e.');

      return Future.error(Exception('Error occurred: $e'));
    }
  }

  /// Gets the public key from url in configuration.
  @override
  Future<String> getPublicKey(String? publicKeyUrl) async {
    try {
      Response response = await Dio().get(publicKeyUrl!);

      return Future.value(response.data.toString());
    } catch (e) {
      _logger.e('Request failed: $e.');

      return Future.error(Exception('Error occurred: $e'));
    }
  }
}
