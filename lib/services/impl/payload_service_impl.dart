import 'package:dio/dio.dart';
import 'package:schluss_beta_app/services/interface/payload_service.dart';

class PayloadServiceImpl implements PayloadService {
  Dio httpClient = Dio();

  /// Sends payload to the endpoint.
  @override
  Future submitPayloadData(
    String endpoint,
    String? token,
    String payloadData,
  ) async {
    try {
      httpClient.options = BaseOptions(
        contentType: Headers.jsonContentType,
        headers: {'Authorization': token},
      );

      var response = await httpClient.post(endpoint, data: payloadData);

      if (response.statusCode == 200 || response.statusCode == 201) {
        return Future.value(true);
      }
      return Future.value(true);
    } on DioException catch (error) {
      return Future.error(error);
    } catch (e) {
      return Future.error(e);
    }
  }

  /// Updates the status.
  @override
  Future<bool> updateStatus(
    String endpoint,
    String payload,
    String? token,
  ) async {
    Response<dynamic> response;

    try {
      httpClient.options = BaseOptions(
        contentType: Headers.jsonContentType,
        headers: {'Authorization': token},
      );

      response = await httpClient.post(endpoint, data: payload);
    } catch (e) {
      return Future.error(e);
    }

    if (response.statusCode == 200 || response.statusCode == 201) {
      return Future.value(true);
    }
    return Future.value(false);
  }

  /// Submits the done status.
  @override
  Future<bool> submitDoneStatus(
    String endpoint,
    String payload,
    String token,
  ) async {
    httpClient.options = BaseOptions(
      contentType: Headers.jsonContentType,
      headers: {'Authorization': token},
    );

    var response = await httpClient.post(endpoint, data: payload);

    if (response.statusCode == 200 || response.statusCode == 201) {
      return Future.value(true);
    }
    return Future.value(false);
  }
}
