import 'dart:async';
import 'dart:convert';

import 'package:dio/dio.dart';
import 'package:flutter_inappwebview/flutter_inappwebview.dart';
import 'package:schluss_beta_app/services/interface/ledger_service.dart';
import 'package:schluss_beta_app/services/interface/ledger_service_interface.dart';

/// Implementations of an IOTA type ledger object.
class IOTALedgerService extends LedgerService implements LedgerServiceInterface {
  // Fix url, as long as we are not able to do a full WASM / FFI implementation
  // of the IOTA RUST identity library.
  static const String requestUrl = 'https://services.schluss.app/me/iota/index.html';

  String? _idServiceApiUrl;
  String? _idServiceApiKey;

  late HeadlessInAppWebView headlessWebView;

  IOTALedgerService() : super('iota') {
    _idServiceApiUrl = getEnvVar('IDENTITY_SERVICE_API_URL');
    _idServiceApiKey = getEnvVar('IDENTITY_SERVICE_API_KEY');
  }

  Future<LedgerKeyPair> createKey() async {
    final completer = Completer<LedgerKeyPair>();

    try {
      /* For specific debugging 
      if (Platform.isAndroid) {
        await AndroidInAppWebViewController.setWebContentsDebuggingEnabled(true);
      }
      */

      headlessWebView = HeadlessInAppWebView(
          initialUrlRequest: URLRequest(url: Uri.parse(requestUrl)),
          initialOptions: InAppWebViewGroupOptions(
              crossPlatform: InAppWebViewOptions(
            clearCache: true,
          )),
          onLoadStop: (controller, url) async {
            // Run javascript locally inside the loaded webview.
            var functionBody = '''
              var p = new Promise(async function (resolve, reject){

                await window.identity.init();

                const key = new window.identity.KeyPair(identity.KeyType.Ed25519);
               
                // return key 
                resolve ({key : key.toJSON()});

              });
              await p;
              return p;
        	  ''';

            var jsResult = await (controller.callAsyncJavaScript(
              functionBody: functionBody,
            ));

            if (jsResult == null) {
              throw 'Generating KeyPair failed';
            }

            // Validate javascript result.
            if (jsResult.value['key']['type'] == null || jsResult.value['key']['public'] == null || jsResult.value['key']['secret'] == null || jsResult.error != null) {
              logger.e('identity service: createIdentity javascript function failed: ${jsResult.error!}');
              throw ('Generating keys failed');
            }

            var keyPair = LedgerKeyPair.fromJSON(jsResult.value['key']);

            // Dispose the webview to free up memory.
            await headlessWebView.dispose();

            // Send back the result by completing the completer.
            completer.complete(keyPair);
          });

      // Run the headless webview.
      await headlessWebView.run();

      return completer.future;
    } catch (e) {
      logger.e('identity service: createIdentity exception, details:');
      logger.e(e);
      rethrow;
    }
  }

  Future<Map<String, dynamic>> createDID(LedgerKeyPair keyPair) async {
    final completer = Completer<Map<String, dynamic>>();

    try {
      /* For specific debugging 
      if (Platform.isAndroid) {
        await AndroidInAppWebViewController.setWebContentsDebuggingEnabled(true);
      }
      */

      headlessWebView = HeadlessInAppWebView(
          initialUrlRequest: URLRequest(url: Uri.parse(requestUrl)),
          initialOptions: InAppWebViewGroupOptions(
              crossPlatform: InAppWebViewOptions(
            clearCache: true,
          )),
          onLoadStop: (controller, url) async {
            // Run javascript locally inside the loaded webview.
            var functionBody = '''
              var x = new Promise(async function (resolve, reject){
                try {
                  await window.identity.init();

                  const keyObj = window.identity.KeyPair.fromJSON(JSON.parse(key)); // key is an injected param

                  const verificationMethod = new window.identity.VerificationMethod(keyObj, 'key');

                  const doc = window.identity.Document.fromAuthentication(verificationMethod);
                
                  doc.sign(keyObj);

                  // return did 
                  resolve ({did : doc.toJSON()});
                }
                catch(e){
                  reject(x);
                }
              });
              await x;
              return x;
        	  ''';

            var jsResult = await (controller.callAsyncJavaScript(
              functionBody: functionBody,
              arguments: {'key': jsonEncode(keyPair.toJSON())},
            ));

            if (jsResult == null) {
              throw 'Creating DID failed';
            }

            // Validate javascript result.
            if (jsResult.value['did']['id'] == null || jsResult.error != null) {
              logger.e(
                'identity service: createIdentity javascript function failed: ${jsResult.error!}',
              );
              throw ('Generating DID Document failed');
            }

            // Publish DID document.
            var httpClient = Dio();

            httpClient.options = BaseOptions(
              contentType: Headers.jsonContentType,
              headers: {
                'Authorization': _idServiceApiKey,
              },
            );

            var response = await httpClient.post(
              '${_idServiceApiUrl!}/identities',
              data: jsResult.value['did'],
            );

            if (response.statusCode != 200) {
              logger.e('identity service: [POST] /identities failed, details:');
              logger.e(jsonEncode(response));

              throw ('Registering identity on IOTA ledger by publishing Signed DID document failed');
            }

            // Dispose the webview to free up memory.
            await headlessWebView.dispose();

            // Send back the result by completing the completer.
            completer.complete({
              'didId': jsResult.value['did']['id'],
              'messageId': response.data['messageId'],
            });
          });

      // Run the headless webview.
      await headlessWebView.run();

      return completer.future;
    } catch (e) {
      logger.e('identity service: createIdentity exception, details:');
      logger.e(e);
      rethrow;
    }
  }

  @override
  Future<LedgerKeyPair> getKey() async {
    // Get key from secure storage.
    var key = await getKeyFromStorage();

    // When key is empty, generate it on the spot.
    if (key == null) {
      key = await createKey();

      await storeKey(key);
    }

    return key;
  }

  @override
  Future<Map<String, dynamic>?> getDID() async {
    Map<String, dynamic>? did;

    // Try to get [did] id.
    var didStr = await getSecure('did');

    // If not existing call generate did here.
    if (didStr != '') {
      did = jsonDecode(didStr);
    } else {
      // Get key.
      var keyPair = await getKey();

      // Create did document.
      did = await createDID(keyPair);

      // Store the [did].
      await storeSecure('did', jsonEncode(did));
    }

    return did;
  }

  // TODO: implement [resolveDID].
  @override
  Future<Map<String, dynamic>> resolveDID(String didId) async {
    throw UnimplementedError();
  }

  @override
  Future<Map<String, String>> createVC(String? didId, Map<String?, dynamic> attributes) async {
    try {
      // [emptyMap] is used to return instead of null value.
      Map<String, String> emptyMap = {};
      var attrList = [];
      // Make it an array of items for the request.
      attributes.forEach((k, v) => attrList.add({'name': k, 'value': v}));

      var httpClient = Dio();

      httpClient.options = BaseOptions(
        contentType: Headers.jsonContentType,
        headers: {
          'Authorization': _idServiceApiKey,
        },
      );

      // Start the VC issuing request.
      var response = await httpClient.post('${_idServiceApiUrl!}/issue', data: {
        'did': didId,
        'attributes': attrList,
      });

      if (response.statusCode != 200) {
        logger.e('identity service: [POST] /issue failed, details:');
        logger.e(jsonEncode(response));

        throw ('invalid response');
      }

      var requestId = response.data['requestId'];

      // Poll for a result.
      // Try to get a valid result within 60 attempts
      // (with a 2sec interval per attempt) = 2min in total.
      for (var i = 0; i < 60; i++) {
        var response = await httpClient.get('${_idServiceApiUrl!}/request/$requestId');

        // When there is an error.
        if (response.statusCode == 401) {
          throw ('Invalid response from Schluss Identity Service while issuering credentials');
        }

        // When there is a valid response.
        if (response.statusCode == 200 || response.statusCode == 201) {
          // When verified by user.
          if (response.data['verificationStatus'] == 'verified') {
            // Convert the [verifiableCredential] list back to a Map<String, String>().
            var vcMap = {for (var e in response.data['verifiableCredential']) e['name']: jsonEncode(e['vc'])}.cast<String, String>();

            return vcMap;
          }
          // When verification failed.
          else if (response.data['verificationStatus'] == 'failed') {
            throw ('Signature verification failed, therefore the e-mail address could not be verified');
          }
        }

        // Still waiting, go for another round.
        await Future.delayed(const Duration(seconds: 2));
      }

      return Future.value(emptyMap);
    } catch (e) {
      logger.e('identity service: startRequest exception, details:');
      logger.e(e);
      rethrow;
    }
  }

  @override
  Future<Map<String, dynamic>> createVP() {
    // TODO: implement [createVP].
    throw UnimplementedError();
  }
}
