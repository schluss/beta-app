import 'package:schluss_beta_app/services/interface/ledger_service.dart';

/// Definitions of the required structure for an ledger type object.
abstract class LedgerServiceInterface {
  Future<LedgerKeyPair> getKey();
  Future<Map<String, dynamic>?> getDID();
  Future<Map<String, dynamic>> resolveDID(String didId);

  Future<Map<String, dynamic>> createVC(
    String didId,
    Map<String, dynamic> attributes,
  );
  Future<Map<String, dynamic>> createVP();
}
