import 'package:schluss_beta_app/view_model/data_request_config_view_model.dart';

abstract class ConfigurationService {
  Future<DataRequestConfigViewModel> retrieveAppConfigByScope(
    String? configUrl,
    String? scope,
  );
  Future<String> getPublicKey(String? publicKeyUrl);
}
