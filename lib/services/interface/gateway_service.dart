import 'package:dio/dio.dart';
import 'package:schluss_beta_app/view_model/gateway_viewmodel.dart';

abstract class GatewayService {
  Future<GatewayViewModel> registerNewAccount(String api);
  Future<GatewayViewModel> getAuthToken(String endpoint, String payload);
  Future<GatewayViewModel> getConnectionLink(
    String endpoint,
    String? token,
    String payload,
  );
  Future<GatewayViewModel> executeGetRequest(String endpoint, {String? token});
  Future<Response> executeGetRequestWithHeaders(String url, {String? token});

  Future<Map<String, dynamic>?> executePostRequest(
    String endpoint,
    dynamic body, {
    Map<String, dynamic>? headers,
  });
  Future<Map<String, dynamic>?> executePutRequest(
    String endpoint,
    dynamic body, {
    Map<String, dynamic>? headers,
  });
  Future<Map<String, dynamic>?> executeDeleteRequest(
    String endpoint,
    Map<String, dynamic>? headers,
  );
}
