/*
 * @author Manuja Jayawardana
 * @email mjayawardana@yukon.lk
 * @create date 2020-08-26
 */

abstract class CommonService {
  Future<bool> checkInternetConnectivity();
}
