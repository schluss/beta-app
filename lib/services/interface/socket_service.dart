/*
 * @author Manuja Jayawardana
 * @email mjayawardana@yukon.lk
 * @create date 2021-09-26
 */

import 'dart:async';

abstract class SocketService {
  Future<void> connect();
  Future<void> disconnect();
  void subscribe(String message, StreamController sController);
  Future<void> unsubscribe(String message);
  Future<bool> shareToken(String toSocket);
  Future<bool> shareMessage(String toSocket, String message);
}
