import 'dart:convert';

import 'package:flutter/cupertino.dart';
import 'package:flutter_dotenv/flutter_dotenv.dart';
import 'package:logger/logger.dart';
import 'package:schluss_beta_app/util/crypto_util.dart';

/// Bases implementation of a ledger type object.
abstract class LedgerService {
  final String _ledgerName;
  Logger logger = Logger();
  LedgerService(this._ledgerName);

  @protected
  String? getEnvVar(String key) {
    if (dotenv.env.containsKey(key)) {
      return dotenv.env[key];
    }

    return '';
  }

  @protected
  Future<LedgerKeyPair?> getKeyFromStorage() async {
    var keyStr = await getSecure('keypair');

    if (keyStr == '') {
      return null;
    }

    return LedgerKeyPair.fromJSONString(keyStr);
  }

  @protected
  Future<bool> storeKey(LedgerKeyPair keyPair) async {
    return storeSecure('keypair', jsonEncode(keyPair.toJSON()));
  }

  @protected
  Future<bool> storeSecure(String key, String value) async {
    try {
      await CryptoUtil.storeInKeyStore('$_ledgerName.$key', value);

      return Future.value(true);
    } catch (e) {
      return Future.value(false);
    }
  }

  @protected
  Future<String> getSecure(String key) async {
    return await CryptoUtil.getFromKeyStore('$_ledgerName.$key');
  }
}

class LedgerKeyPair {
  String? type;
  String? public;
  String? secret;

  LedgerKeyPair(this.type, this.public, this.secret);

  Map<String, String?> toJSON() {
    return {'type': type, 'public': public, 'secret': secret};
  }

  factory LedgerKeyPair.fromJSON(Map<String, dynamic> json) {
    return LedgerKeyPair(json['type'], json['public'], json['secret']);
  }

  factory LedgerKeyPair.fromJSONString(String jsonStr) {
    var json = jsonDecode(jsonStr);
    return LedgerKeyPair(json['type'], json['public'], json['secret']);
  }
}

class DID {}
