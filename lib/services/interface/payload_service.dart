/*
 * @author Asanka Anthony
 * @email aanthony@yukon.lk
 * @create date 2020-06-12 09:40:08
 * @modify date 2020-06-12 09:40:08
 */

abstract class PayloadService {
  Future submitPayloadData(String endpoint, String token, String payloadData);
  Future<bool> submitDoneStatus(String endpoint, String payload, String token);
  Future<bool> updateStatus(String endpoint, String payload, String token);
}
