/*
 * @author Manuja Jayawardana
 * @email mjayawardana@yukon.lk
 * @create date 2021-03-24
 */

import 'package:schluss_beta_app/services/impl/gateway_service_impl.dart';
import 'package:schluss_beta_app/services/interface/gateway_service.dart';
import 'package:schluss_beta_app/view_model/gateway_viewmodel.dart';

/// Gateways background executor util.
///
/// Uses this class to execute gateway related tasks in the background.
/// NOTE: This methods should be top layer methods to be used in the background
/// executor. So, develop the methods outside of the class or static methods.
class GatewayExecuterUtil {
  /// Registers new account in background.
  ///
  ///  gatewayService - [GatewayService] object.
  ///  args           - Arguments for the specified method function (Endpoint url).
  static Future<GatewayViewModel> registerNewAccountInBackground(
    List args,
  ) async {
    var endpoint = args[0];
    GatewayService gatewayService = GatewayServiceImpl();
    GatewayViewModel gatewayViewModel = await gatewayService.registerNewAccount(
      endpoint,
    );

    return gatewayViewModel;
  }

  /// Gets authentication token for registered users in background.
  ///
  /// gatewayService - [GatewayService] object.
  /// Endpoint URl   - Endpoint url for auth API.
  /// Payload        - gateway secret data.
  static Future<GatewayViewModel> getAuthTokenInBackground(List args) async {
    String endpoint = args[0];
    String payload = args[1];

    GatewayService gatewayService = GatewayServiceImpl();

    GatewayViewModel gatewayViewModel = await gatewayService.getAuthToken(
      endpoint,
      payload,
    );

    return gatewayViewModel;
  }
}
