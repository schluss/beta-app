import 'package:flutter/material.dart';
import 'package:schluss_beta_app/constants/helper_constants.dart';

class SlideRouteTransition extends PageRouteBuilder {
  final Widget? widget;
  final Offset animationDirection;
  final bool isOpaque;

  SlideRouteTransition({
    this.widget,
    this.animationDirection = HelperConstant.rightDirection,
    this.isOpaque = false,
  }) : super(
          transitionDuration: const Duration(milliseconds: 400),
          reverseTransitionDuration: const Duration(milliseconds: 300),
          barrierDismissible: true,
          opaque: true, // Makes visible the previous screen.
          pageBuilder: (
            BuildContext context,
            Animation<double> animation,
            Animation<double> secondaryAnimation,
          ) =>
              widget!,
          transitionsBuilder: (
            BuildContext context,
            Animation<double> animation,
            Animation<double> secondaryAnimation,
            Widget child,
          ) =>
              SlideTransition(
            position: Tween<Offset>(
              begin: animationDirection, // Change the direction of the slide transition by changing the offset -1,0 = left to right.
              end: Offset.zero,
            ).animate(CurvedAnimation(
              parent: animation,
              curve: Curves.fastOutSlowIn,
            )),
            child: child,
          ),
        );
}
