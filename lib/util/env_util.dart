import 'package:flutter_dotenv/flutter_dotenv.dart';
import 'package:schluss_beta_app/constants/environment_constants.dart';

class EnvUtil {
  Future<String> getEnvModeValue() async {
    return dotenv.env.containsKey('ENV')
        ? EnvironmentConstants.fromString(
            dotenv.env['ENV'],
          )
        : EnvironmentConstants.development;
  }

  Future<bool> getEnvStewardshipModeModeValue() async {
    return dotenv.env.containsKey('IS_STEWARDSHIP_MODE_DEFAULT')
        ? dotenv.env['IS_STEWARDSHIP_MODE_DEFAULT'] == 'true'
            ? true
            : false
        : false;
  }

  Future<bool> getEnvDataSharingModeModeValue() async {
    return dotenv.env.containsKey('IS_DATASHARING_MODE_DEFAULT')
        ? dotenv.env['IS_DATASHARING_MODE_DEFAULT'] == 'true'
            ? true
            : false
        : false;
  }

  Future<bool> getEnvTestModeValue() async {
    return dotenv.env.containsKey('IS_TEST_MODE_DEFAULT')
        ? dotenv.env['IS_TEST_MODE_DEFAULT'] == 'true'
            ? true
            : false
        : false;
  }
}
