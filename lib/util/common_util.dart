import 'dart:convert';
import 'dart:io';

import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:mime/mime.dart';
import 'package:path_provider/path_provider.dart';
import 'package:schluss_beta_app/controller/config_controller.dart';
import 'package:schluss_beta_app/services/interface/common_service.dart';
import 'package:schluss_beta_app/singleton_injector.dart';
import 'package:schluss_beta_app/util/error_handler_util.dart';
import 'package:schluss_beta_app/view_model/request_view_model.dart';

class CommonUtil {
  final CommonService _commonService = singletonInjector<CommonService>();
  final ConfigurationController _configController = ConfigurationController();
  final ErrorHandlerUtil _errorHandler = ErrorHandlerUtil();

  RequestViewModel? _requestModel;

  /// Checks internet connectivity.
  Future<bool> checkInternetConnectivity() async {
    bool status = await _commonService.checkInternetConnectivity();
    return Future.value(status);
  }

  /// Processes data request configuration link.
  Future<void> processConfigurationLink(String configUrl, bool hasDataRequestGuidePage, BuildContext context) async {
    final navigator = Navigator.of(context);

    try {
      _requestModel = await _configController.processConfigurationLink(configUrl);
    } catch (e) {
      _errorHandler.handleError(e.toString(), false, context, 'qrCode-scan');
    }

    if (_requestModel != null) {
      navigator.popAndPushNamed(
        '/data-request',
        arguments: {
          'model': _requestModel,
          'isOpen': hasDataRequestGuidePage,
        },
      );
    }
  }

  /// Generates file from bytes stream.
  Future<File> buildFileFromBytes(String dataValue, String? fileName) async {
    File? file;

    if (dataValue.isNotEmpty) {
      Uint8List fileByte = const Base64Codec().decode(dataValue);

      // Write the file to app directory.
      String dir = (await getTemporaryDirectory()).path;

      file = File('$dir/$fileName');

      await file.writeAsBytes(fileByte);
    }
    return Future.value(file);
  }

  /// Builds a valid url from a given endpoint and optional path.
  /// For example endpoint: [https://site.com/] and path: [/path/to/object],
  /// resolve into  [https://site.com/path/to/object].
  static String buildUrl(String endpoint, {String? path}) {
    var url = endpoint;

    if (!url.endsWith('/')) {
      url += '/';
    }

    if (path != null) {
      if (path.startsWith('/')) {
        path = path.substring(1);
      }

      url = url + path;
    }

    return url;
  }

  /// Gives tap feedback to users events.
  static void tapFeedback() {
    HapticFeedback.lightImpact();
  }

  /// Determines the media type from the given file object.
  static String getMediaTypeFromFile(File file) {
    return getMediaTypeFromHeader(file.readAsBytesSync().toList());
  }

  /// Determines the media type from the given [path/to/a/file.txt].
  static String getMediaTypeFromPath(String path) {
    var fileBytes = File(path).readAsBytesSync().toList();

    return getMediaTypeFromHeader(fileBytes, path: path);
  }

  /// Determines the media type of a file from the given path and header bytes.
  static String getMediaTypeFromHeader(List<int>? headerBytes, {String path = ''}) {
    var mime = lookupMimeType(path, headerBytes: headerBytes);

    return mime ?? 'application/unknown';
  }

  static bool isImage(String? mediaType) {
    if (mediaType != null && mediaType.contains('image')) {
      return true;
    }

    return false;

    // More thorough implementation, alternatively, if we would like to scale
    // down the on the 'as image' returned mediatypes.
    // var isImage = false;

    // var imageTypes = ['jpg', 'jpeg', 'png', 'gif', 'bmp', 'svg'];

    // if (mediaType != null) {
    //   for (var type in imageTypes) {
    //     if (mediaType.contains(type)) {
    //       isImage = true;
    //       break;
    //     }
    //   }
    // }

    // return isImage;
  }
}
