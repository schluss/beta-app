import 'dart:math';

import 'package:flutter/cupertino.dart';
import 'package:schluss_beta_app/constants/ui_constants.dart';

extension RandomListItem<T> on List<Color> {
  Color randomColor() {
    return this[Random().nextInt(length)];
  }
}

class ColorPickerUtil {
  List<Color> colorList = [
    UIConstants.accentColor,
    UIConstants.orange,
    UIConstants.yellow,
    UIConstants.green,
    UIConstants.blue,
    UIConstants.turquoise,
    UIConstants.purple,
  ];

  Color pickColor() {
    return colorList.randomColor();
  }
}
