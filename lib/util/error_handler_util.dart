import 'package:dio/dio.dart';
import 'package:flutter/material.dart';
import 'package:logger/logger.dart';
import 'package:schluss_beta_app/singleton_injector.dart';
import 'package:schluss_beta_app/translations/i18n.dart';

class ErrorHandlerUtil {
  final Logger _logger = singletonInjector<Logger>();

  Future<void> handleError(
    var error,
    bool backToPreviousPage,
    BuildContext context, [
    String? scenario,
  ]) async {
    Map<String, String> errorInfo;
    String? errorMsg;
    String? errorDescription;

    _logger.e('Error: $error');

    // Get the type of the [error].
    var errorType = error.runtimeType;

    // Get error message and description.
    errorInfo = generateErrorInfo(errorType, error, scenario, context);
    _logger.i('Error info: $errorInfo');

    // Pass error message and description.
    errorMsg = errorInfo['errorMsg'];
    errorDescription = errorInfo['errorDesc'];

    // Navigate to previous screen from the current screen.
    if (backToPreviousPage == true) {
      // Navigate to generic [error] screen with error message and description.
      Navigator.pushNamed(
        context,
        '/error',
        arguments: {
          'errorMsg': errorMsg,
          'errorDescription': errorDescription,
        },
      );
    } else {
      Navigator.pushNamedAndRemoveUntil(
        context,
        '/error',
        ModalRoute.withName('/'),
        arguments: {
          'errorMsg': errorMsg,
          'errorDescription': errorDescription,
        },
      );
    }
  }

  /// Generates [error] message and description as the type of the error.
  Map<String, String> generateErrorInfo(
    var errorType,
    var error,
    String? scenario,
    BuildContext context,
  ) {
    Map<String, String> errorInfo;

    errorInfo = {
      'errorMsg': I18n.of(context).errHandlerCommonErrHeader, // This is the default error message.
      'errorDesc': I18n.of(context).errHandlerCommonErrTxt, // This is the default error description.
    };

    _logger.i('scenario: $scenario');

    // Developer should have to customize the error msg per the scenario separately.
    if (scenario != null) {
      switch (scenario) {
        case 'vault-creation':
          errorInfo['errorMsg'] = I18n.of(context).errHandlerVaultCreationErrHeader;
          break;
        case 'log-in':
          errorInfo['errorMsg'] = I18n.of(context).errHandlerLogInErrHeader;
          break;
        case 'log-out':
          errorInfo['errorMsg'] = I18n.of(context).errHandlerLogOutErrHeader;
          break;
        case 'password-change':
          errorInfo['errorMsg'] = I18n.of(context).errHandlerPinChangeErrHeader;
          break;
        case 'account-del':
          errorInfo['errorMsg'] = I18n.of(context).errHandlerAcctDelErrHeader;
          break;
        case 'qrCode-scan':
          if (errorType == RangeError) {
            errorInfo['errorMsg'] = I18n.of(context).errHandlerQrCodeScanErrHeader;
          }
          break;
        case 'no-internet':
          errorInfo['errorMsg'] = I18n.of(context).errHandlerNoInternetErrHeader;
          break;
        case 'logger':
          errorInfo['errorMsg'] = I18n.of(context).errHandlerLogMsgNotSentErrHeader;
          break;
        default:
          errorInfo['errorMsg'] = I18n.of(context).errHandlerCommonErrHeader;
      }
    }

    _logger.i('Error-type: $errorType');

    // Developer should have to customize the error description per the type of the error separately.
    switch (errorType) {
      case RangeError: // Error msg and description for type - [RangeError].
        errorInfo['errorDesc'] = I18n.of(context).errHandlerRangeErrTxt;
        return errorInfo;
      case TypeError: // Error msg and description for type - [NullThrownError].
        errorInfo['errorDesc'] = I18n.of(context).errHandlerNullThrownErrTxt;
        return errorInfo;
      case DioException: // Error msg and description for type - [NullThrownError].
        if (scenario == 'logger') {
          errorInfo['errorDesc'] = I18n.of(context).errHandlerLogMsgNotSentErrTxt;
        }
        return errorInfo;
      case String: // Error msg and description for type - [String].
        if (scenario == 'no-internet') {
          errorInfo['errorDesc'] = I18n.of(context).errHandlerNoInternetErrTxt;
        }
        return errorInfo;
      default:
        errorInfo['errorDesc'] = I18n.of(context).errHandlerCommonErrTxt;
        return errorInfo;
    }
  }
}
