import 'package:logger/logger.dart';
import 'package:schluss_beta_app/constants/http_response_code_constants.dart';
import 'package:schluss_beta_app/singleton_injector.dart';

class HttpResponseHandlerUtil {
  final Logger _logger = singletonInjector<Logger>();

  /// Validates the HTTP responses according to the HTTP status code.
  Future validateResponse(int? statusCode) {
    switch (statusCode) {
      case HttpResponseCodeConstants.responseBadRequest:
        return returnResponse('400 - Bad Request.');
      case HttpResponseCodeConstants.responseUnauthorized:
        return returnResponse('401 - Unauthorized.');
      case HttpResponseCodeConstants.responseForbidden:
        return returnResponse('403 - Forbidden.');
      case HttpResponseCodeConstants.responseNotFound:
        return returnResponse('404 - Not Found.');
      case HttpResponseCodeConstants.responseNotAcceptable:
        return returnResponse('406 - Not Acceptable.');
      case HttpResponseCodeConstants.responseInternalServerError:
        return returnResponse('500 - Internal Server Error.');
      case HttpResponseCodeConstants.responseNotImplemented:
        return returnResponse('501 - Not Implemented.');
      case HttpResponseCodeConstants.responseBadGateway:
        return returnResponse('502 - Bad Gateway.');
      case HttpResponseCodeConstants.responseServiceUnavailable:
        return returnResponse('503 - Service Unavailable.');
      case HttpResponseCodeConstants.responseGatewayTimeout:
        return returnResponse('504 - Gateway Timeout.');
      case HttpResponseCodeConstants.responseHttpVersionNotSupported:
        return returnResponse('505 - HTTP Version Not Supported.');
      default:
        return returnResponse('Unknown Error.');
    }
  }

  Future returnResponse(String error) {
    _logger.e(error);
    return Future.error(error);
  }
}
