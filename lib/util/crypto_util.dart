import 'dart:async';
import 'dart:convert';
import 'dart:math';

import 'package:conduit_password_hash/conduit_password_hash.dart';
import 'package:crypto/crypto.dart' as crypto;
import 'package:flutter/foundation.dart';
import 'package:flutter_secure_storage/flutter_secure_storage.dart';
import 'package:logger/logger.dart';
import 'package:openpgp/openpgp.dart';
import 'package:schluss_beta_app/data/models/login_attempt_model.dart';
import 'package:schluss_beta_app/singleton_injector.dart';
import 'package:session_next/session_next.dart';

/// Read more details information here, [beta-app/docs/crypto.md].
class CryptoUtil {
  static final Logger _logger = singletonInjector<Logger>();

  /// Extensions type for encrypted files.
  static const encryptedFileExtension = 'enc';

  static const _storage = FlutterSecureStorage();

  /// Secures key store key field to retrieve SALT value.
  static const _secureKeyStoreSaltKey = 'SALT_RANDOM';

  /// Secures key store key field to retrieve private key.
  static const _secureKeyStorePrivateKey = 'PRIVATE_KEY';

  /// Secures key store key field to retrieve random key.
  static const _secureKeyStoreRandomKey = 'RANDOM_KEY';

  /// Secures key store key field to retrieve random Key.
  static const _secureKeyStorePincodeHash = 'PIN_HASH';

  /// Secures key store key field to retrieve external storage gateway [SecretData].
  static const _secureESGSecretDataKey = 'ESG_SECRET_KEY';

  /// Secures key store key field to retrieve external storage gateway token.
  static const _secureESGTokenDataKey = 'ESG_TOKEN';

  /// Secures key store key field to retrieve gateway secret data.
  static const _secureUGWSecretDataKey = 'UGW_SECRET_KEY';

  /// Secures key store key field to retrieve gateway token.
  static const _secureUGWTokenDataKey = 'UGW_TOKEN';

  /// Read [saltKey] from secure key store.
  static Future<String> getSaltKeyFromSecureStore() async {
    String? saltKey = await _storage.read(key: _secureKeyStoreSaltKey);

    return Future.value(saltKey);
  }

  /// Encrypts values.
  static Future<String> encrypt(String data, [String? publicKey]) async {
    // When public key not given, use own internal public key.
    publicKey ??= SessionNext().get<String>('publicKey');

    // Encrypt the data using the public key.
    return compute(_computeEncrypt, {'data': data, 'publicKey': publicKey});
  }

  static Future<String> _computeEncrypt(Map data) async {
    // Sanity check.
    if (!data.containsKey('data') || !data.containsKey('publicKey')) {
      return Future.value('');
    }

    String encryptedValue = await OpenPGP.encrypt(data['data'], data['publicKey']);

    return encryptedValue;
  }

  /// Decrypts given value using the users' private key.
  static Future<String> decrypt(
    String encryptedData, {
    String? privateKey,
    String? password,
  }) async {
    String? decryptedData;

    try {
      // If no private key is given, use the one from the session.
      privateKey ??= SessionNext().get<String>('privateKey');

      return compute(_computeDecrypt, {
        'encryptedData': encryptedData,
        'privateKey': privateKey,
      });
    } catch (e) {
      _logger.e('Decryption failed: $e');
    }

    return Future.value(decryptedData);
  }

  static Future<String> _computeDecrypt(Map data) async {
    // Sanity check
    if (!data.containsKey('encryptedData') || !data.containsKey('privateKey')) {
      return Future.value('');
    }

    // Decrypts values using private key and random key.
    var result = OpenPGP.decrypt(
      data['encryptedData'],
      data['privateKey'],
      '',
    );

    return Future.value(result);
  }

  /// Encrypts values with symmetric encryption.
  static Future<String> encryptSymmetric(String data, String passphrase) async {
    return compute(_computeEncryptSymmetric, {
      'data': data,
      'passphrase': passphrase,
    });
  }

  static Future<String> _computeEncryptSymmetric(Map data) async {
    // Sanity check.
    if (!data.containsKey('data') || !data.containsKey('passphrase')) {
      return Future.value('');
    }

    // Encryption parameters.
    var keyOptions = KeyOptions();
    keyOptions.cipher = Cipher.AES256;
    keyOptions.hash = Hash.SHA512;

    String encryptedValue = await OpenPGP.encryptSymmetric(
      data['data'],
      data['passphrase'],
      options: keyOptions,
    );

    return Future.value(encryptedValue);
  }

  /// Decrypts values using symmetric encryption.
  static Future<String> decryptSymmetric(String data, String passphrase) async {
    return compute(_computeDecryptSymmetric, {
      'data': data,
      'passphrase': passphrase,
    });
  }

  static Future<String> _computeDecryptSymmetric(Map data) async {
    try {
      // Sanity check.
      if (!data.containsKey('data') || !data.containsKey('passphrase')) {
        return Future.value('');
      }

      // Encryption parameters.
      var keyOptions = KeyOptions();
      keyOptions.cipher = Cipher.AES256;
      keyOptions.hash = Hash.SHA512;

      String decryptedValue = await OpenPGP.decryptSymmetric(
        data['data'],
        data['passphrase'],
        options: keyOptions,
      );

      return Future.value(decryptedValue);
    } catch (e) {
      //_logger.e('Decryption failed, error: $e');

      return Future.value('');
    }
  }

  /// Creates a RSA key pair.
  static Future<KeyPair> createKeyPair({String? password}) async {
    // Because it is a heavy function, run it in a separate thread.
    KeyPair keyPair = await compute(_computeCreateKeyPair, password);
    _logger.d('Keypair: generated');

    // Store secure keys.
    await _storage.write(
      key: _secureKeyStorePrivateKey,
      value: keyPair.privateKey,
    );

    return Future.value(keyPair);
  }

  static Future<KeyPair> _computeCreateKeyPair(String? password) async {
    // Encryption parameters.
    var keyOptions = KeyOptions()
      ..cipher = Cipher.AES256
      ..hash = Hash.SHA512
      ..rsaBits = 4096;

    KeyPair keyPair = await OpenPGP.generate(
        options: Options()
          ..passphrase = password
          ..keyOptions = keyOptions);

    return Future.value(keyPair);
  }

  /// Gets PBKDF2 hash using password and salt value from secure key store.
  static Future<String> getPBKDF2Hash(String password) async {
    String saltValue = await getSaltKeyFromSecureStore();

    var generator = PBKDF2(hashAlgorithm: crypto.sha512);

    String hash = generator.generateBase64Key(password, saltValue, 10000, 32);

    return Future.value(hash);
  }

  /// Creates a [PBKDF2] hash from given [password] and store it in secure key store.
  static Future<String> createPBKDF2Hash(String password) async {
    var salt = generateAsBase64String(32);

    var generator = PBKDF2(hashAlgorithm: crypto.sha512);

    String hash = generator.generateBase64Key(password, salt, 10000, 32);

    // Store the salt in the secure keystore.
    await storeInKeyStore(_secureKeyStoreSaltKey, salt);

    return Future.value(hash);
  }

  /// Creates an MD5 hash.
  static Future<String> getMD5HashValue(String text) async {
    var bytes = utf8.encode(text);
    crypto.Digest digest = crypto.md5.convert(bytes);

    return Future.value(digest.toString());
  }

  /// Creates new hash without using salt.
  static Future<String> generateHash(String value) async {
    var hash = crypto.sha256.convert(utf8.encode(value)).toString();
    return Future.value(hash);
  }

  /// Generates a random key.
  static String generateRandomKey({int length = 32}) {
    var random = Random.secure();
    const availableChars = 'AaBbCcDdEeFfGgHhIiJjKkLlMmNnOoPpQqRrSsTtUuVvWwXxYyZz1234567890';
    var randomString = List.generate(
      length,
      (index) => availableChars[random.nextInt(availableChars.length)],
    ).join();

    return randomString;
  }

  /// Stores the pin code hash in the secure keystore.
  static Future<void> storePincodeHash(String pinCodeHash) async {
    return storeInKeyStore(_secureKeyStorePincodeHash, pinCodeHash);
  }

  /// Retrieves the pin code hash from the secure [keystore].
  static Future<String> getPincodeHash() async {
    return getFromKeyStore(_secureKeyStorePincodeHash);
  }

  /// Stores the random key in the secure keystore.
  static Future<void> storeRandomKey(String randomKey) async {
    return storeInKeyStore(_secureKeyStoreRandomKey, randomKey);
  }

  /// Retrieves the random key from the secure keystore.
  static Future<String> getRandomKey() async {
    return getFromKeyStore(_secureKeyStoreRandomKey);
  }

  /// Gets the private key from secure key store.
  static Future<String> getPrivateKeyFromSecureStore() async {
    String? privateKey;
    privateKey = await getFromKeyStore(_secureKeyStorePrivateKey);

    return Future.value(privateKey);
  }

  /// Validates RSA key pair by doing a simple crypto operation.
  /// Exceptions: [PlatformException] can be thrown from [OpenPGP] encrypt
  /// and decrypt methods due to using a wrong passphrase.
  static Future<bool> validateKeyPair(
    String publicKey,
    String privateKey,
  ) async {
    try {
      // The test value.
      var pgpTest = 'SchlussRocks';

      // Encrypt.
      var resultEncrypt = await encrypt(pgpTest, publicKey);

      // Decrypt again.
      var result = await decrypt(resultEncrypt, privateKey: privateKey);

      // Compare.
      return (pgpTest == result) ? true : false;
    } catch (e) {
      // [PlatformException] throws from encrypt/decrypt.
      _logger.e('KeyPair validation failed: $e');

      return false;
    }
  }

  /// Deletes All keys from the secure keystore.
  /// Exceptions: [PlatformException] can be thrown as per the description on method.
  static Future<bool> removeAllKeys() async {
    try {
      await _storage.deleteAll();

      return Future.value(true);
    } catch (e) {
      _logger.e('Error in removing all keys : $e');

      return Future.value(false);
    }
  }

  /// Deletes after log-in.
  ///
  /// Deletes all login attempt information after valid login,
  /// because we don't need old failed attempt information after user login.
  static Future<bool> removeLoginAttempt() async {
    try {
      // Delete all login attempt details.
      await _storage.delete(key: LoginAttempt.getStoreName());

      _logger.d('Login - attempts cleared');

      return Future.value(true);
    } catch (e) {
      _logger.e('Login - attempt clearing failed: $e');

      return Future.value(false);
    }
  }

  /// Gets the last login attempt, if there is none it will be created and stored on the fly.
  static Future<LoginAttempt> getLoginAttempt() async {
    // Get exiting login attempt details from keystore.
    String loginAttemptStr = await getFromKeyStore(LoginAttempt.getStoreName());

    // If there is no login attempt yet, create one on the spot.
    if (loginAttemptStr == '') {
      // Create new login attempt object.
      LoginAttempt loginAttempt = LoginAttempt(
        loginAttemptCount: 0,
        loginAttemptTimestamp: DateTime.now().millisecondsSinceEpoch,
      );

      // Store login attempt.
      await storeInKeyStore(LoginAttempt.getStoreName(), jsonEncode(loginAttempt));

      _logger.d('Login - new login attempt stored, attempt count: ${loginAttempt.loginAttemptCount}');

      return loginAttempt;
    }
    // Convert json to object.
    LoginAttempt loginAttempt = LoginAttempt.fromJson(jsonDecode(
      loginAttemptStr,
    ));

    return Future.value(loginAttempt);
  }

  /// Read from secure key store, & update failed log-in attempt count.
  static Future<LoginAttempt> updateLoginAttemptCount() async {
    LoginAttempt loginAttempt = await getLoginAttempt();
    // Increment the count by one.
    loginAttempt.loginAttemptCount = loginAttempt.loginAttemptCount + 1;

    // Update time.
    loginAttempt.loginAttemptTimestamp = DateTime.now().millisecondsSinceEpoch;

    // Update exiting login count by +1 and saves.
    await storeInKeyStore(LoginAttempt.getStoreName(), jsonEncode(loginAttempt));

    _logger.d('Login - attempt count increased to : ${loginAttempt.loginAttemptCount}');

    return Future.value(loginAttempt);
  }

  /// Gets no of failed login count in store.
  static Future<int> getFailedAttemptCount() async {
    LoginAttempt loginAttempt = await getLoginAttempt();

    return Future.value(loginAttempt.loginAttemptCount);
  }

  /// Extracts the attribute.
  static Future<String> extractAttributeValue(String value, String attrKeyEnc) async {
    String valueDecrypted;
    try {
      // String publicKey = SessionUtil.instance.publicKey!;
      String privateKey = SessionNext().get<String>('privateKey')!;
      String randomKey = SessionNext().get<String>('randomKey')!;

      // Get hash value of pub key.
      // String publicKeyHash = await generateHash(publicKey);

      // Find [attrKeyEnc] in keys by matching [fingerprint] with [publicKeyFingerprint].
      // String attributeKeyEnc = keys[publicKeyHash]!;

      // Decrypt [attrKeyEnc] to get attrKey:
      // attrKey = openpgp_decrypt(attrKeyEnc, privKey).

      String attributeKey = await decrypt(
        attrKeyEnc,
        privateKey: privateKey,
        password: randomKey,
      );

      valueDecrypted = await CryptoUtil.decryptSymmetric(value, attributeKey);
    } catch (e) {
      _logger.w('Value not decrypted, may be already decrypted..');
      rethrow;
    }
    return valueDecrypted;
  }

  /// Stores a value in the secure storage.
  static Future<void> storeInKeyStore(String key, String value) async {
    await _storage.write(key: key, value: value);
  }

  /// Retrieves a value from the secure storage.
  static Future<String> getFromKeyStore(String key) async {
    var result = await _storage.read(key: key);
    return result ?? '';
  }

  /// Stores external storage gateway secret data in secret data store.
  static Future<void> storeExternalStorageGatewaySecretData(
    String secretData,
  ) async {
    return storeInKeyStore(_secureESGSecretDataKey, secretData);
  }

  /// Retrieves previous stored external storage gateway secret
  /// data from secret data store.
  static Future<Map<String, dynamic>?> getExternalStorageGatewaySecretData() async {
    String? data = await getFromKeyStore(_secureESGSecretDataKey);

    if (data.isNotEmpty) {
      return json.decode(data);
    }
    return null;
  }

  /// Stores external storage gateway token in secret data store.
  static Future<void> storeExternalStorageGatewayToken(String token) async {
    return storeInKeyStore(_secureESGTokenDataKey, token);
  }

  /// Retrieves previous stored external storage gateway secret data
  /// from secret data store.
  static Future<String?> getExternalStorageGatewayToken() async {
    return getFromKeyStore(_secureESGTokenDataKey);
  }

  /// Stores gateway secret data in secret data store.
  static Future<void> storeGatewaySecretData(String secretData) async {
    return storeInKeyStore(_secureUGWSecretDataKey, secretData);
  }

  /// Retrieves previous stored gateway secret data from secret data store.
  static Future<Map?> getGatewaySecretData() async {
    String? data = await getFromKeyStore(_secureUGWSecretDataKey);

    if (data.isNotEmpty) {
      return json.decode(data);
    }
    return null;
  }

  /// Stores gateway token in secret data store.
  static Future<void> storeGatewayToken(String token) async {
    return storeInKeyStore(_secureUGWTokenDataKey, token);
  }

  /// Retrieves previous stored gateway secret data from secret data store.
  static Future<String?> getGatewayToken() async {
    return getFromKeyStore(_secureUGWTokenDataKey);
  }
}
