import 'package:logger/logger.dart';
import 'package:schluss_beta_app/util/logger_util.dart';

class LogOutputUtil extends LogOutput {
  final LoggerUtil _loggerUtil = LoggerUtil();

  late String _log;
  late List<String> _logList;

  @override
  Future<void> output(OutputEvent event) async {
    // Print all types of log messages on the console.
    for (var line in event.lines) {
      // ignore: avoid_print
      print(line);
    }

    writeLoggerFile(event);
  }

  /// Writes log file.
  void writeLoggerFile(OutputEvent event) {
    // If type of the log message is error:
    _log = event.lines.toString(); // List<String> => String.
    _logList = _log.split('\n'); // Split log messages by line-breaks into a list.

    var logMsg = StringBuffer(); // Define a string buffer.

    // Add log messages into string buffer.
    for (var line in _logList) {
      logMsg.write(line);
    }
    // Write error log messages onto schluss.log file.
    _loggerUtil.logError(logMsg);
  }
}
