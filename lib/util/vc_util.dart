import 'dart:convert';

class VCUtil {
  /// Validates according to the [https://www.w3.org/TR/vc-data-model/#validation].
  // TODO: need to validate the [Expiration], [Status], [Fitness for Purpose]
  //  keys also for perfect validation validating the value is VC or not.
  static bool validateVC(var decryptedValue) {
    try {
      if (decryptedValue is! Map) {
        decryptedValue = jsonDecode(decryptedValue);
      }
      // List of keys needs to be tested on the values to validate the VC.
      List keysToBeValidated = ['credentialSubject', 'issuer', 'issuanceDate', 'proof'];
      keysToBeValidated.removeWhere((element) => decryptedValue.keys.contains(element));

      return keysToBeValidated.isNotEmpty ? false : true;
    } catch (e) {
      return false;
    }
  }

  static String getValue(String name, var decryptedValue) {
    Map data = json.decode(decryptedValue);
    if (data['credentialSubject'].containsKey(name)) {
      return data['credentialSubject'][name];
    }

    return decryptedValue;
  }
}
