import 'package:package_info_plus/package_info_plus.dart';

class PackageInfoUtil {
  /// Gets version of the app package.
  static Future<String> getVersion() async {
    var packageInfo = await PackageInfo.fromPlatform();
    return packageInfo.version;
  }
}
