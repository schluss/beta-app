class InitialsPickerUtil {
  String getInitials(String fullName) {
    return fullName.isNotEmpty
        ? fullName
            .trim()
            .split(' ')
            .where(
              (data) => data != '',
            )
            .map((data) => data[0])
            .take(2)
            .join()
        : '';
  }
}
