import 'dart:convert';
import 'dart:io';

import 'package:intl/intl.dart';
import 'package:path_provider/path_provider.dart';

class LoggerUtil {
  final String _currentDateTime = DateFormat('yyyy-MM-dd HH:mm:ss').format(
    DateTime.now(),
  );

  /// Gets directory path.
  Future<String> get _localPath async {
    final directory = await getApplicationDocumentsDirectory();

    return directory.path;
  }

  /// Sets the log file.
  Future<File> get _localFile async {
    final path = await _localPath;

    return File('$path/schluss.log');
  }

  /// Checks whether the file exists or not.
  Future<bool> _checkLogFileExistence() async {
    final file = await _localFile;

    return await file.exists();
  }

  /// Gets the size of the file.
  Future<int> _getLogFileSize() async {
    final file = await _localFile;

    return await file.length();
  }

  /// Updates the file.
  Future<File> _updateLogFile(String logContents) async {
    final file = await _localFile;

    // Without the string conversion occurs for issues sometime.
    return file.writeAsString(logContents, mode: FileMode.append);
  }

  /// Read the file line-wise.
  Future<List<String>> readLogFileAsLines() async {
    final file = await _localFile;

    try {
      List<String> logMessage = await file.readAsLines(encoding: utf8);
      return logMessage;
    } catch (e) {
      return <String>[];
    }
  }

  /// Writes the file.
  Future<File> _writeLogFile(var logContents) async {
    final file = await _localFile;

    return file.writeAsString('$logContents', mode: FileMode.write);
  }

  /// Removes old half of error log lines from the file and
  /// writes latest error log lines again into file.
  Future<File> _removePreviousLogs() async {
    List<String> loggedContent;
    int halfOfLineCount;
    var lastLogs = StringBuffer();

    /// Read the file line-wise.
    loggedContent = await readLogFileAsLines();

    /// Counts the half of the lines of the file.
    halfOfLineCount = ((loggedContent.length) / 2).floor();

    /// Removes old half of error log lines of the file.
    loggedContent.removeRange(0, halfOfLineCount);

    // Converts List<String> into String:
    for (var item in loggedContent) {
      lastLogs.write('$item\n');
    }

    /// Writes Latest log files again into the file.
    return await _writeLogFile(lastLogs);
  }

  /// Logs the error.
  Future<File> logError(var log) async {
    /// This checks whether file exists or not.
    bool isFileExists = await _checkLogFileExistence();
    int fileSize;

    // If file exists.
    if (isFileExists) {
      fileSize = await _getLogFileSize(); // Gets the size of the file.

      if (fileSize < 200000) {
        /// Updates the file with the error log,
        /// if the size of the file is less than 200KB:
        return await _updateLogFile('$_currentDateTime - $log\n');
      } else {
        // Remove old half of error log lines from the file,
        // if the size of the file is greater than 200KB.
        await _removePreviousLogs();

        // Update the file with the error log.
        return await _updateLogFile('$_currentDateTime - $log\n');
      }
    } else {
      // Write the error log on the file.
      return await _writeLogFile('$_currentDateTime - $log\n');
    }
  }

  /// Deletes the file:
  Future deleteLogFile() async {
    final file = await _localFile;

    await file.delete();
  }
}
