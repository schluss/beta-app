import 'dart:io';

import 'package:device_info_plus/device_info_plus.dart';
import 'package:flutter/services.dart';

class DeviceInfoUtil {
  DeviceInfoPlugin deviceInfoPlugin = DeviceInfoPlugin();

  bool _isAndroid = false;
  bool _isIos = false;
  Map<String, dynamic>? _deviceInfo;
  bool? _isPhysicalDevice;

  /// Sets the platform of the device up.
  void _setPlatform() {
    try {
      _isAndroid = Platform.isAndroid ? true : false;
      _isIos = Platform.isIOS ? true : false;
    } on PlatformException {
      _deviceInfo = <String, dynamic>{'Error:': 'Failed to get platform version.'};
    }
  }

  /// Gets android device information.
  Map<String, dynamic> _readAndroidDeviceInfo(AndroidDeviceInfo data) {
    return <String, dynamic>{
      'version.securityPatch': data.version.securityPatch,
      'version.sdkInt': data.version.sdkInt,
      'version.release': data.version.release,
      'version.previewSdkInt': data.version.previewSdkInt,
      'version.incremental': data.version.incremental,
      'version.codename': data.version.codename,
      'version.baseOS': data.version.baseOS,
      'board': data.board,
      'bootloader': data.bootloader,
      'brand': data.brand,
      'device': data.device,
      'display': data.display,
      'fingerprint': data.fingerprint,
      'hardware': data.hardware,
      'host': data.host,
      'id': data.id,
      'manufacturer': data.manufacturer,
      'model': data.model,
      'product': data.product,
      'supported32BitAbis': data.supported32BitAbis,
      'supported64BitAbis': data.supported64BitAbis,
      'supportedAbis': data.supportedAbis,
      'tags': data.tags,
      'type': data.type,
      'isPhysicalDevice': data.isPhysicalDevice,
      'systemFeatures': data.systemFeatures,
    };
  }

  /// Gets IOS device information.
  Map<String, dynamic> _readIosDeviceInfo(IosDeviceInfo data) {
    return <String, dynamic>{
      'name': data.name,
      'systemName': data.systemName,
      'systemVersion': data.systemVersion,
      'model': data.model,
      'localizedModel': data.localizedModel,
      'isPhysicalDevice': data.isPhysicalDevice,
      'utsname.sysname:': data.utsname.sysname,
      'utsname.nodename:': data.utsname.nodename,
      'utsname.release:': data.utsname.release,
      'utsname.version:': data.utsname.version,
      'utsname.machine:': data.utsname.machine,
    };
  }

  /// Gets device information.
  Future<Map<String, dynamic>?> getDeviceInfo() async {
    _setPlatform();

    if (_isAndroid) {
      _deviceInfo = _readAndroidDeviceInfo(await deviceInfoPlugin.androidInfo);
    } else if (_isIos) {
      _deviceInfo = _readIosDeviceInfo(await deviceInfoPlugin.iosInfo);
    }
    return _deviceInfo;
  }

  /// Checks whether the device is physical or not.
  Future<bool?> checkRunOnPhysicalDevice() async {
    _setPlatform();

    if (_isAndroid) {
      _isPhysicalDevice = (await deviceInfoPlugin.androidInfo).isPhysicalDevice;
    } else if (_isIos) {
      _isPhysicalDevice = (await deviceInfoPlugin.iosInfo).isPhysicalDevice;
    }
    return _isPhysicalDevice;
  }
}
