import 'dart:async';

import 'package:flutter/material.dart';
import 'package:one_context/one_context.dart';
import 'package:schluss_beta_app/constants/helper_constants.dart';
import 'package:schluss_beta_app/constants/ui_constants.dart';
import 'package:schluss_beta_app/controller/account_controller.dart';
import 'package:schluss_beta_app/translations/i18n.dart';
import 'package:schluss_beta_app/ui/reused_widgets/keypad.dart';
import 'package:schluss_beta_app/ui/reused_widgets/matching_validation_notifier.dart';
import 'package:schluss_beta_app/ui/reused_widgets/pin_code.dart';
import 'package:schluss_beta_app/ui/reused_widgets/pin_screen_template.dart';
import 'package:schluss_beta_app/util/common_util.dart';
import 'package:schluss_beta_app/util/crypto_util.dart';
import 'package:schluss_beta_app/util/error_handler_util.dart';
import 'package:session_next/session_next.dart';

enum LoginState { preLogin, migrating, migrationFailed, error, success }

class LoginPage extends StatefulWidget {
  const LoginPage({Key? key}) : super(key: key);

  @override
  LoginPageState createState() => LoginPageState();
}

class LoginPageState extends State<LoginPage> {
  final AccountController _accountController = AccountController();
  final ErrorHandlerUtil _errorHandler = ErrorHandlerUtil();

  List<String> password = ['', '', '', '', ''];
  int inputIndex = 0;
  bool hasDigitOne = false;
  bool hasDigitTwo = false;
  bool hasDigitThree = false;
  bool hasDigitFour = false;
  bool hasDigitFive = false;
  LoginState loginState = LoginState.preLogin;
  int? attemptCount = 0;
  int? failedAttemptCount;
  late int sec;

  /// Checks number count is singular or plural for load specific translation.
  bool isPlural = true;

  @override
  void dispose() {
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    double width = MediaQuery.of(context).size.width;
    double height = MediaQuery.of(context).size.height;

    return Scaffold(
      backgroundColor: UIConstants.primaryColor,
      body: SafeArea(
        child: SizedBox(
          width: width,
          height: height,
          child: Stack(
            children: [
              /// Pin screen.
              PinScreenTemplate(
                /// Title text.
                text: I18n.of(context).loginTitle,

                /// PIN.
                pin: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceAround,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: [
                    Expanded(
                      child: hasDigitOne
                          ? buildStarImage(
                              width,
                            )
                          : buildEmptyPlaceholder(width),
                    ),
                    Expanded(
                      child: hasDigitTwo
                          ? buildStarImage(
                              width,
                            )
                          : buildEmptyPlaceholder(width),
                    ),
                    Expanded(
                      child: hasDigitThree
                          ? buildStarImage(
                              width,
                            )
                          : buildEmptyPlaceholder(width),
                    ),
                    Expanded(
                      child: hasDigitFour
                          ? buildStarImage(
                              width,
                            )
                          : buildEmptyPlaceholder(width),
                    ),
                    Expanded(
                      child: hasDigitFive
                          ? buildStarImage(
                              width,
                            )
                          : buildEmptyPlaceholder(width),
                    ),
                  ],
                ),

                /// Validations messages.
                validationNotifier: Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    MatchingValidationNotifier(
                      text: _getLoginNotificationMsg(),
                      isValid: _checkValidationStatus()!,
                    ),
                  ],
                ),

                /// Key pad.
                keyPad: KeyPad(
                  handleKeyOne: onOneClick,
                  handleKeyTwo: onTwoClick,
                  handleKeyThree: onThreeClick,
                  handleKeyFour: onFourClick,
                  handleKeyFive: onFiveClick,
                  handleKeySix: onSixClick,
                  handleKeySeven: onSevenClick,
                  handleKeyEight: onEightClick,
                  handleKeyNine: onNineClick,
                  handleKeyZero: onZeroClick,
                  handleDeleteKey: onDeleteNumber,
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }

  /// Loads validation message.
  dynamic _getLoginNotificationMsg() {
    var msg = '';
    if (loginState == LoginState.success) {
      msg = I18n.of(context).loginValidTxt;
    } else if (loginState == LoginState.migrating) {
      msg = I18n.of(context).loginMigrationProgressTxt;
    } else if (loginState == LoginState.migrationFailed) {
      msg = I18n.of(context).errHandlerCommonErrHeader;
    } else {
      if (attemptCount == 1) {
        msg = I18n.of(context).loginInvalid1stAttemptTxt;
      } else if (attemptCount == 2) {
        msg = I18n.of(context).loginInvalid2ndAttemptTxt;
      }
    }

    return msg;
  }

  /// Checks validation message status.
  /// If [true] / [false] - show the message.
  bool? _checkValidationStatus() {
    bool? status = false;

    if (loginState == LoginState.success || loginState == LoginState.migrating) {
      status = true;
    } else if (loginState == LoginState.error || loginState == LoginState.migrationFailed) {
      status = false;
    }

    return status;
  }

  /// Assigns values for keys.
  void onOneClick() => handleUserInput('1');

  void onTwoClick() => handleUserInput('2');

  void onThreeClick() => handleUserInput('3');

  void onFourClick() => handleUserInput('4');

  void onFiveClick() => handleUserInput('5');

  void onSixClick() => handleUserInput('6');

  void onSevenClick() => handleUserInput('7');

  void onEightClick() => handleUserInput('8');

  void onNineClick() => handleUserInput('9');

  void onZeroClick() => handleUserInput('0');

  /// Handles the PIN.
  void handleUserInput(String number) {
    CommonUtil.tapFeedback();

    if (inputIndex != password.length) {
      password[inputIndex] = number;

      setState(() => loginState = LoginState.preLogin);

      if (inputIndex != password.length) {
        setState(() {
          if (inputIndex == 0) hasDigitOne = true;
          if (inputIndex == 1) hasDigitTwo = true;
          if (inputIndex == 2) hasDigitThree = true;
          if (inputIndex == 3) hasDigitFour = true;
          if (inputIndex == 4) hasDigitFive = true;
        });

        inputIndex++;
      }

      Future.delayed(const Duration(milliseconds: 1), () {
        if (inputIndex == password.length) {
          checkValidity(password.join(''));
        }
      });
    }
  }

  /// Validates PIN.
  void checkValidity(String password) async {
    bool loginResult = false;

    try {
      loginResult = await _accountController.validateLogin(password);
    } on Future catch (e) {
      _errorHandler.handleError(e.toString(), false, context, 'log-in');
    }

    if (loginResult) {
      setState(() => loginState = LoginState.success);
      // TODO: need to discuss and decide should we wait 1 sec showing login completed.
      await Future.delayed(const Duration(seconds: 1));
      // Start the session.
      // TODO: future, implement migration process here for migrations right after login.
      //  setState(() => loginState = LoginState.migrating);
      //  bool migrationStatus = await doTheMigrationHere
      //  if (migrationStatus) {
      //  } else {
      //  setState(() => loginState = LoginState.migrationFailed);
      //  }

      SessionNext().start(
          sessionTimeOut: HelperConstant.focusInactiveTimeSec,
          onExpire: () => {
                // Navigate to logout screen with calling [pushNamedAndRemoveUntil]
                // method - user will not be able to navigate back to the account
                // page with [pushNamed] method the user was able to navigate back to
                // the account page which should not be possible.
                OneContext().pushNamedAndRemoveUntil(
                  '/account/login',
                  (Route<dynamic> route) => false,
                )
              });

      Future.delayed(const Duration(seconds: 1), () {
        Navigator.of(context).pushNamedAndRemoveUntil(
          '/dashboard',
          (Route<dynamic> route) => false,
          arguments: {'activeTab': 0, 'isActualVaultOwner': true},
        );
      });
    } else {
      // session model == [null] means failed login.
      failedAttemptCount = await CryptoUtil.getFailedAttemptCount();

      resetPin();

      setState(() {
        if (attemptCount == 1) {
          // Get singular count, only at 1 sec.
          // Get singular translation.
          isPlural = false;
        } else {
          // Get plural translation.
          isPlural = true;
        }
        attemptCount = failedAttemptCount;

        loginState = LoginState.error;

        password = '';
      });

      sec = await _accountController.validateLoginAttempts();
      if (sec < 0) {
        // more than 0 means should redirect to countdown screen.
        if (!mounted) return;
        Navigator.pushReplacementNamed(context, '/account/login/countdown');
      }
    }
  }

  /// Deletes all five digits.
  void resetPin() {
    for (var i = inputIndex; i >= 0; i--) {
      onDeleteNumber();
    }
  }

  /// Deletes the last digit.
  void onDeleteNumber() {
    CommonUtil.tapFeedback();
    if (inputIndex >= 0) {
      if (inputIndex != 0) {
        inputIndex--;
      }

      setState(() {
        if (inputIndex == 4) hasDigitFive = false;
        if (inputIndex == 3) hasDigitFour = false;
        if (inputIndex == 2) hasDigitThree = false;
        if (inputIndex == 1) hasDigitTwo = false;
        if (inputIndex == 0) hasDigitOne = false;
      });

      password[inputIndex] = '';

      setState(() => loginState = LoginState.preLogin);
    }
  }
}
