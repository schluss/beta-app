import 'dart:async';

import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:schluss_beta_app/constants/ui_constants.dart';
import 'package:schluss_beta_app/controller/account_controller.dart';
import 'package:schluss_beta_app/translations/i18n.dart';

class LoginCountdownPage extends StatefulWidget {
  const LoginCountdownPage({Key? key}) : super(key: key);

  @override
  LoginCountdownPageState createState() => LoginCountdownPageState();
}

class LoginCountdownPageState extends State<LoginCountdownPage> {
  Timer? timer;

  /// This check number count is singular or plural for load specific translation.
  bool isPlural = true;

  /// Countdown.
  int countDownTime = 0;
  AccountController accountController = AccountController();

  @override
  void initState() {
    super.initState();
    getLoginStatus(accountController);
    timer = Timer.periodic(
      const Duration(seconds: 1),
      (Timer t) => getLoginStatus(accountController),
    );
  }

  @override
  void dispose() {
    timer?.cancel();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    double width = MediaQuery.of(context).size.width;
    double height = MediaQuery.of(context).size.height;

    return Stack(
      children: <Widget>[
        Padding(
          padding: EdgeInsets.only(
            left: width * 0.1,
            right: width * 0.1,
            bottom: height * 0.0625,
          ),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              SvgPicture.asset(
                'assets/images/logo.svg',
                colorFilter: const ColorFilter.mode(UIConstants.accentColor, BlendMode.srcIn),
                width: width * 0.142,
              ),
              Padding(
                  padding: EdgeInsets.symmetric(
                    horizontal: width * 0.05,
                    vertical: width * 0.04,
                  ),
                  child: Text(
                    I18n.of(context).loginCountdownHeader,
                    textAlign: TextAlign.center,
                    style: const TextStyle(
                      fontSize: 17.0,
                      color: UIConstants.grayDarkest70,
                      fontWeight: FontWeight.w600,
                    ),
                  )),
              Text(
                isPlural
                    ? I18n.of(context).loginCountdownTxt(
                        count: countDownTime,
                      )
                    : I18n.of(context).loginCountdownSingularTxt(
                        count: countDownTime,
                      ),
                maxLines: 5,
                overflow: TextOverflow.ellipsis,
                textAlign: TextAlign.center,
                style: TextStyle(
                  color: UIConstants.gray100,
                  fontSize: width * 0.038,
                  fontFamily: UIConstants.subFontFamily,
                ),
              )
            ],
          ),
        ),
      ],
    );
  }

  void getLoginStatus(AccountController accountController) async {
    int wTime;
    wTime = await accountController.validateLoginAttempts();

    if (wTime == 0) {
      // Can log now.
      if (!mounted) return;
      Navigator.pushReplacementNamed(context, '/account/login');
    } else {
      setState(() {
        // Get positive number.
        countDownTime = wTime.abs();

        // If get singular translation time less than 2.
        if (wTime.abs() < 2) {
          isPlural = false;
        } else {
          isPlural = true;
        }
      });
    }
  }
}
