import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:schluss_beta_app/constants/theme.dart';
import 'package:schluss_beta_app/constants/ui_constants.dart';
import 'package:schluss_beta_app/controller/account_controller.dart';
import 'package:schluss_beta_app/controller/config_controller.dart';
import 'package:schluss_beta_app/translations/i18n.dart';
import 'package:schluss_beta_app/ui/reused_widgets/dashed_separator.dart';
import 'package:schluss_beta_app/ui/reused_widgets/data_requester_logo.dart';
import 'package:schluss_beta_app/ui/reused_widgets/data_submission_info.dart';
import 'package:schluss_beta_app/ui/reused_widgets/half_circle.dart';
import 'package:schluss_beta_app/ui/reused_widgets/main_button.dart';
import 'package:schluss_beta_app/ui/reused_widgets/providers_list_item.dart';
import 'package:schluss_beta_app/view_model/data_request_config_view_model.dart';
import 'package:schluss_beta_app/view_model/provider_config_view_model.dart';
import 'package:schluss_beta_app/view_model/request_view_model.dart';
import 'package:session_next/session_next.dart';

class DataRequestDonePage extends StatefulWidget {
  final RequestViewModel? requestViewModel;

  const DataRequestDonePage({
    Key? key,
    this.requestViewModel,
  }) : super(key: key);

  @override
  // ignore: no_logic_in_create_state
  DataRequestDonePageState createState() => DataRequestDonePageState(
        requestViewModel,
      );
}

class DataRequestDonePageState extends State<DataRequestDonePage> {
  RequestViewModel? _requestViewModel;
  late DataRequestConfigViewModel _appConfigModel;

  final ConfigurationController _configController = ConfigurationController();

  String? _dataRequesterLogo = '';
  String? _name = '';

  DataRequestDonePageState(RequestViewModel? model) {
    _requestViewModel = model;
  }

  @override
  void initState() {
    _initScreenData();

    super.initState();
  }

  /// Initializes data elements in screen.
  void _initScreenData() async {
    _appConfigModel = await _configController.retrieveConfiguration(
      widget.requestViewModel,
    );

    setState(() {
      _dataRequesterLogo = _appConfigModel.dataRequester.logoUrl;
      _name = _appConfigModel.dataRequester.name;
    });
  }

  @override
  Widget build(BuildContext context) {
    double width = MediaQuery.of(context).size.width;
    double height = MediaQuery.of(context).size.height;

    return Scaffold(
      body: Container(
        height: height,
        color: UIConstants.grayLight,
        child: Stack(
          children: <Widget>[
            Align(
              alignment: Alignment.bottomCenter,
              child: Container(
                width: width,
                height: height * 0.935,
                decoration: const BoxDecoration(
                  color: UIConstants.primaryColor,
                  borderRadius: mainBorderRadius,
                ),
                child: Column(
                  children: <Widget>[
                    Container(
                      padding: EdgeInsets.only(top: width * 0.03),
                      height: height * 0.175,
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: <Widget>[
                          Text(
                            I18n.of(context).dataReqDoneTitle,
                            style: TextStyle(
                              color: UIConstants.grayDarkest100,
                              fontSize: width * 0.055,
                              fontWeight: FontWeight.w700,
                            ),
                          ),
                          Text(
                            I18n.of(context).dataReqDoneTxt,
                            style: TextStyle(
                              color: UIConstants.slateGray100,
                              fontSize: width * 0.0425,
                            ),
                          ),
                        ],
                      ),
                    ),
                    SizedBox(
                      height: height * 0.76,
                      child: Column(
                        children: <Widget>[
                          ConstrainedBox(
                            constraints: const BoxConstraints(
                              maxHeight: 180.0,
                            ),
                            child: SizedBox(
                              height: height * 0.23,
                              child: Column(
                                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                                children: <Widget>[
                                  Padding(
                                    padding: EdgeInsets.only(
                                      left: width * 0.075,
                                      right: width * 0.075,
                                    ),
                                    child: Row(
                                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                      children: <Widget>[
                                        DataSubmissionInfo(
                                          text: I18n.of(context).dataReqDoneDateTxt,
                                          value: _getDateString(),
                                        ),
                                        DataSubmissionInfo(
                                          text: I18n.of(context).dataReqDoneTermTxt,
                                          value: I18n.of(context).dataReqDoneTermValueTxt,
                                        ),
                                      ],
                                    ),
                                  ),
                                  Padding(
                                    padding: EdgeInsets.only(
                                      left: width * 0.075,
                                      right: width * 0.04,
                                    ),
                                    child: Row(
                                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                      children: <Widget>[
                                        DataSubmissionInfo(
                                          text: I18n.of(context).dataReqDoneReceiverTxt,
                                          value: _name!,
                                        ),
                                        DataRequesterLogo(
                                          img: _dataRequesterLogo!,
                                          isAssetImg: false,
                                        ),
                                      ],
                                    ),
                                  ),
                                ],
                              ),
                            ),
                          ),
                          const Divider(height: 1),
                          SizedBox(
                            height: height * 0.53 - height / 18 - width * 0.15,
                            child: ListView.builder(
                              padding: EdgeInsets.zero,
                              itemBuilder: (BuildContext context, int index) {
                                var item = _requestViewModel!.getProviderList()[index];

                                String? from;

                                var connectionId = SessionNext().get<int>('connectionId') ?? 0;

                                if (item.from != null && item.from != 'owner' && connectionId != 0) {
                                  from = AccountController().getCurrentVaultName();
                                }

                                return Material(
                                  color: Theme.of(context).primaryColor,
                                  child: InkWell(
                                    splashColor: UIConstants.blackFaded,
                                    onTap: () => _loadProvider(item),
                                    child: Column(
                                      children: <Widget>[
                                        ProvidersListItem(
                                          logo: item.dataProvider!.logoUrl!,
                                          isChecked: item.isChecked,
                                          title: item.request!,
                                          subTitle: '${I18n.of(context).viaTxt} ${item.dataProvider!.name!}',
                                          isCheckedEnabled: false,
                                          from: from,
                                        ),
                                        const Divider(height: 1),
                                      ],
                                    ),
                                  ),
                                );
                              },
                              itemCount: _requestViewModel!.getProviderList().length,
                            ),
                          ),
                        ],
                      ),
                    )
                  ],
                ),
              ),
            ),
            _buildTicketDecoration(context),
            _buildTopCheckIcon(context),
            Padding(
              padding: EdgeInsets.only(
                right: width * 0.075,
                left: width * 0.075,
                bottom: height / 18,
              ),
              child: Align(
                alignment: Alignment.bottomCenter,
                child: MainButton(
                  type: BtnType.active,
                  text: I18n.of(context).btnDoneTxt,
                  call: () => Navigator.pushNamed(
                    context,
                    '/dashboard',
                    arguments: {'activeTab': 3, 'isActualVaultOwner': true},
                  ),
                ),
              ),
            )
          ],
        ),
      ),
    );
  }

  /// Returns date as a string.
  String _getDateString() {
    // Format date field with Netherland locale.
    return DateFormat('d MMMM yyyy', 'nl').format(
      DateTime.fromMillisecondsSinceEpoch(widget.requestViewModel!.requestDate!),
    );
  }

  /// Loads provider.
  void _loadProvider(ProviderConfigViewModel model) {
    int itemIndex;

    itemIndex = _requestViewModel!.getProviderList().indexOf(model);
    _requestViewModel!.setCurrentSelectedIndex(itemIndex);

    Navigator.pushNamed(
      context,
      '/data-request/provider',
      arguments: _requestViewModel,
    );
  }

  /// Builds separating design of dashes ends with two half circles.
  Padding _buildTicketDecoration(BuildContext context) {
    double width = MediaQuery.of(context).size.width;

    return Padding(
      padding: EdgeInsets.only(top: MediaQuery.of(context).size.height * 0.22),
      child: Stack(
        children: <Widget>[
          Padding(
            padding: EdgeInsets.only(top: width * 0.03),
            child: const DashedSeparator(),
          ),
          const Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: <Widget>[
              RotatedBox(
                quarterTurns: 1,
                child: HalfCircle(),
              ),
              RotatedBox(
                quarterTurns: 3,
                child: HalfCircle(),
              )
            ],
          ),
        ],
      ),
    );
  }

  /// Builds top [greenDark] check mark of provider logo.
  Align _buildTopCheckIcon(BuildContext context) {
    double height = MediaQuery.of(context).size.height;
    return Align(
      alignment: Alignment.topCenter,
      child: Padding(
        padding: EdgeInsets.only(
          top: MediaQuery.of(
                    context,
                  ).padding.top ==
                  0
              ? height * 0.0175
              : MediaQuery.of(
                  context,
                ).padding.top,
        ),
        child: Container(
          width: height * 0.075,
          height: height * 0.075,
          alignment: Alignment.center,
          decoration: BoxDecoration(
            shape: BoxShape.circle,
            color: UIConstants.accentColor,
            boxShadow: const [
              BoxShadow(
                color: UIConstants.rosyPinkDark,
                offset: Offset(0, 7),
                blurRadius: 17,
                spreadRadius: -7,
              ),
            ],
            border: Border.all(color: UIConstants.accentColor),
          ),
          child: Icon(
            Icons.check,
            color: UIConstants.primaryColor,
            size: height * 0.0425,
          ),
        ),
      ),
    );
  }
}
