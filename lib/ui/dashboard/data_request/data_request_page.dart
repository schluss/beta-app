import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:logger/logger.dart';
import 'package:schluss_beta_app/constants/ui_constants.dart';
import 'package:schluss_beta_app/controller/account_controller.dart';
import 'package:schluss_beta_app/controller/config_controller.dart';
import 'package:schluss_beta_app/controller/plugin_controller.dart';
import 'package:schluss_beta_app/controller/request_controller.dart';
import 'package:schluss_beta_app/singleton_injector.dart';
import 'package:schluss_beta_app/translations/i18n.dart';
import 'package:schluss_beta_app/ui/reused_widgets/data_requester_logo.dart';
import 'package:schluss_beta_app/ui/reused_widgets/group_header.dart';
import 'package:schluss_beta_app/ui/reused_widgets/loading_animation.dart';
import 'package:schluss_beta_app/ui/reused_widgets/loading_list_item.dart';
import 'package:schluss_beta_app/ui/reused_widgets/main_button.dart';
import 'package:schluss_beta_app/ui/reused_widgets/mini_header_template.dart';
import 'package:schluss_beta_app/ui/reused_widgets/pin_verification_bottom_up_sheet.dart';
import 'package:schluss_beta_app/ui/reused_widgets/progress_bar.dart';
import 'package:schluss_beta_app/ui/reused_widgets/providers_list_item.dart';
import 'package:schluss_beta_app/util/common_util.dart';
import 'package:schluss_beta_app/util/error_handler_util.dart';
import 'package:schluss_beta_app/view_model/data_request_config_view_model.dart';
import 'package:schluss_beta_app/view_model/provider_config_view_model.dart';
import 'package:schluss_beta_app/view_model/request_view_model.dart';
import 'package:schluss_beta_app/view_model/settings_view_model.dart';
import 'package:session_next/session_next.dart';

class DataRequestPage extends StatefulWidget {
  final RequestViewModel? requestModel;
  final bool? isTourDataRequestOpen;

  /// When [configUrl] is given and [requestModel] = [null], the data request is loaded from an url directly.
  final String? configUrl;

  const DataRequestPage({
    Key? key,
    this.requestModel,
    this.isTourDataRequestOpen,
    this.configUrl,
  }) : super(key: key);

  @override
  // ignore: no_logic_in_create_state
  DataRequestPageState createState() => DataRequestPageState(
        requestModel: requestModel,
        configUrl: configUrl,
      );
}

class DataRequestPageState extends State<DataRequestPage> {
  final Logger _logger = singletonInjector<Logger>();

  final ConfigurationController _configController = ConfigurationController();
  final PluginController _pluginController = PluginController();
  final AccountController _accountController = AccountController();
  final RequestController _requestController = RequestController();

  final ErrorHandlerUtil _errorHandler = ErrorHandlerUtil();
  final CommonUtil _commonUtil = CommonUtil();

  RequestViewModel? requestModel;
  final String? configUrl;

  late SettingsViewModel settings;
  late DataRequestConfigViewModel dataRequestModel;
  bool _hasContract = false;
  late bool internetStatus;
  bool _isTourDataRequestOpen = false;
  late bool _isTourDataRequestOpenOn;

  var _progressValue = 0.0;
  var _progressValueText = '0%';
  String? _mainText = '';
  String? _mainTitle = '';
  String? _mainSubTitle = '';
  String? _dataRequesterLogo;
  var _activeBtnText = '';

  /// Sets temporary to work with the flow.
  bool _isLoaded = false;
  late List providerList;
  int? itemIndex;

  DataRequestPageState({this.requestModel, this.configUrl});
  bool firstTime = true;

  @override
  void initState() {
    // Resume session, why here? It could happen a data request is canceled,
    // aborted, plugin fails after the session was set to paused.
    // This makes sure it resumes!
    SessionNext().resume();

    // BOB NIET NODIG?
    // setContractExistency();

    super.initState();

    firstTime = true;

    _isTourDataRequestOpen = widget.isTourDataRequestOpen ?? false;
  }

  // TODO: looks like this method is always calls build after a dependency changes.
  //  So this might be expensive method.
  @override
  void didChangeDependencies() {
    super.didChangeDependencies();
    // Check if this method calling first time.
    if (firstTime) {
      firstTime = false;

      // When the parent widget is not fully built
      // and this method is called in [setState()] method may occur errors.
      _initScreenData();
    }
  }

  /// Initializes data elements in screen.
  void _initScreenData() async {
    settings = await _accountController.getSettings();

    setState(
      () => _isTourDataRequestOpenOn = settings.tourDataRequestOpen == false ? false : true,
    );

    // Check internet availability.
    internetStatus = await _commonUtil.checkInternetConnectivity();
    if (!mounted) return;
    if (!internetStatus) {
      _errorHandler.handleError(
        'no-internet',
        true,
        context,
        'no-internet',
      );
      return;
    }

    // When it is a data request, loaded and to be created directly from given url.
    if (requestModel == null && configUrl != null) {
      try {
        requestModel = await _configController.processConfigurationLink(configUrl!);
      } catch (e) {
        _errorHandler.handleError(e.toString(), true, context, 'qrcode-scan');
      }
    }
    if (!mounted) return;
    if ((_isTourDataRequestOpen && _isTourDataRequestOpenOn) && _hasContract) {
      // Pop-up data request guidelines bottom sheet.

      Navigator.pushNamed(context, '/data-request/guideline');
    }

    setContractExistence();

    try {
      providerList = requestModel!.getProviderList();
    } catch (e) {
      _errorHandler.handleError(e.toString(), false, context);
    }

    if (providerList.isEmpty) {
      try {
        dataRequestModel = await _configController.retrieveConfiguration(
          requestModel,
        );

        requestModel!.setProviderList(dataRequestModel.providers);
      } catch (e) {
        _errorHandler.handleError(e.toString(), false, context);
      }
    }

    /// Checks if some attributes are stored already.
    /// If so, pre-load this data already into the data request.
    await _configController.preLoadDataIfAlreadyExists(requestModel);

    setState(() {
      _mainTitle = _hasContract ? requestModel!.name! : I18n.of(context).dataReqSelfStoreTitleTxt;
      _mainSubTitle = _hasContract ? requestModel!.importName! : I18n.of(context).dataReqSelfStoreSubtitleTxt;
      _mainText = requestModel!.importDescription;
      _dataRequesterLogo = requestModel!.logoUrl;
    });

    _updateButtonText();
    _getProgressValue();

    setState(() => _isLoaded = true);
  }

  void createDataRequestOnTheFly(String configUrl) {}

  /// Sets contract existence state.
  void setContractExistence() async {
    dataRequestModel = DataRequestConfigViewModel.fromJson(
      json.decode(requestModel!.appConfigRawData!),
      requestModel!.scope,
      requestModel!.settingsUrl,
    );

    _hasContract = dataRequestModel.returnUrl!.isNotEmpty ? true : false;
  }

  /// Updates button text as the progress.
  void _updateButtonText() {
    int completedList;
    int total;

    completedList = 0;
    total = requestModel!.getProviderList().length;

    try {
      completedList = requestModel!.getProviderList().where((element) => element.isChecked == true).length;

      if (completedList == 0) {
        _activeBtnText = I18n.of(context).btnStartTxt;
      } else if (completedList < total) {
        ProviderConfigViewModel? model;
        model = _checkNextProvider();
        _activeBtnText = '${I18n.of(context).btnNextTxt}: ${model!.request!}';
      } else if (completedList == total) {
        if (!_hasContract) {
          _activeBtnText = I18n.of(context).dataReqDoneTitle;
        } else {
          _mainText = I18n.of(context).dataReqCompletedMainTxt;
          _activeBtnText = I18n.of(context).dataReqCompletedBtnTxt;
        }
      } else {
        throw Exception('Button Status Not Calculated');
      }
    } catch (e) {
      _errorHandler.handleError(e.toString(), false, context);

      _activeBtnText = I18n.of(context).btnStartTxt;
    }
  }

  /// Checks next pending provider.
  ProviderConfigViewModel? _checkNextProvider() {
    try {
      return requestModel!.getProviderList().cast<ProviderConfigViewModel?>().firstWhere((element) => element!.isChecked == false, orElse: () => null);
    } catch (e) {
      return null;
    }
  }

  /// Updates the progress value.
  void _getProgressValue() {
    int completedCount;
    int total;

    completedCount = 0;
    total = requestModel!.getProviderList().length;

    try {
      completedCount = requestModel!
          .getProviderList()
          .where(
            (element) => element.isChecked == true,
          )
          .length;
    } catch (e) {
      _errorHandler.handleError(e.toString(), false, context);

      completedCount = 0;
    }

    _progressValue = (completedCount / total);
    _progressValueText = '${(_progressValue * 100).toInt()}%';
  }

  @override
  Widget build(BuildContext context) {
    double width = MediaQuery.of(context).size.width;
    double height = MediaQuery.of(context).size.height;

    return WillPopScope(
      onWillPop: () async {
        _onScreenCloseEvent();

        return true;
      },
      child: Scaffold(
        backgroundColor: UIConstants.primaryColor,
        body: Stack(
          children: <Widget>[
            SingleChildScrollView(
              child: Column(
                children: <Widget>[
                  Padding(
                    padding: EdgeInsets.only(
                      top: height * 0.125,
                    ),
                    child: ConstrainedBox(
                      constraints: const BoxConstraints(
                        maxHeight: 280.0,
                      ),
                      child: Container(
                        height: height * 0.35,
                        color: UIConstants.primaryColor,
                        child: Column(
                          children: <Widget>[
                            Expanded(
                              child: Padding(
                                padding: EdgeInsets.only(
                                  top: width * 0.04,
                                  right: width * 0.075,
                                  left: width * 0.075,
                                  bottom: width * 0.05,
                                ),
                                child: Container(
                                  color: UIConstants.primaryColor,
                                  width: width,
                                  height: width * 0.75,
                                  child: Stack(
                                    children: <Widget>[
                                      Column(
                                        crossAxisAlignment: CrossAxisAlignment.center,
                                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                        children: <Widget>[
                                          (_isLoaded)
                                              ? Row(
                                                  children: <Widget>[
                                                    _hasContract
                                                        ? DataRequesterLogo(
                                                            img: _dataRequesterLogo!,
                                                            isAssetImg: false,
                                                          )
                                                        : const DataRequesterLogo(
                                                            img: 'assets/images/save_box_icon.png',
                                                          ),
                                                    _buildCompanyData(width),
                                                  ],
                                                )
                                              : const LoadingListItem(
                                                  topItemHeightFactor: 0.055,
                                                  topItemWidthFactor: 0.25,
                                                  bottomItemHeightFactor: 0.035,
                                                  bottomItemWidthFactor: 0.4,
                                                ),
                                          (_isLoaded)
                                              ? Text(
                                                  _mainText!,
                                                  style: TextStyle(
                                                    height: 1.4,
                                                    fontWeight: FontWeight.w400,
                                                    color: UIConstants.slateGray100,
                                                    fontSize: width * 0.045,
                                                  ),
                                                )
                                              : const LoadingListItem(
                                                  isIconVisible: false,
                                                  topItemHeightFactor: 0.0575,
                                                  topItemWidthFactor: 0.75,
                                                  bottomItemHeightFactor: 0.0575,
                                                  bottomItemWidthFactor: 0.6,
                                                ),
                                          Align(
                                            alignment: Alignment.centerLeft,
                                            child: (_isLoaded)
                                                ? _hasContract
                                                    ? _buildCalendarItem()
                                                    : Container()
                                                : LoadingAnimation(
                                                    height: width * 0.09,
                                                    width: width * 0.45,
                                                    radius: 20,
                                                  ),
                                          ),
                                          (_isLoaded)
                                              ? Padding(
                                                  padding: EdgeInsets.only(
                                                    bottom: width * 0.0,
                                                  ),
                                                  child: _buildProgressColumn(width),
                                                )
                                              : Container(),
                                        ],
                                      ),
                                    ],
                                  ),
                                ),
                              ),
                            ),
                          ],
                        ),
                      ),
                    ),
                  ),
                  (_isLoaded)
                      ? GroupHeader(text: _hasContract ? I18n.of(context).dataReqGroupHeader : I18n.of(context).dataReqSelfGroupHeader)
                      : LoadingAnimation(
                          height: width * 0.0925,
                          width: width,
                        ),
                  Container(
                    color: Theme.of(context).primaryColor,
                    height: height * 0.30,
                    child: ListView.builder(
                      padding: EdgeInsets.only(
                        top: 0,
                        left: 0,
                        right: 0,
                        bottom: height / 18 + width * 0.15,
                      ),
                      itemBuilder: (BuildContext context, int index) {
                        var item = requestModel!.getProviderList()[index];

                        String? from;

                        var connectionId = SessionNext().get<int>('connectionId') ?? 0;

                        if (item.from != null && item.from != 'owner' && connectionId != 0) {
                          from = _accountController.getCurrentVaultName();
                        }

                        return (_isLoaded)
                            ? Material(
                                color: UIConstants.primaryColor,
                                child: InkWell(
                                  splashColor: UIConstants.blackFaded,
                                  onTap: () => _providerClicked(item),
                                  child: Column(
                                    children: <Widget>[
                                      ProvidersListItem(
                                        logo: item.dataProvider!.logoUrl!,
                                        isChecked: item.isChecked,
                                        title: item.request!,
                                        subTitle: '${I18n.of(context).viaTxt} ${item.dataProvider!.name!}',
                                        from: from,
                                      ),
                                      const Divider(height: 1),
                                    ],
                                  ),
                                ),
                              )
                            : Padding(
                                padding: EdgeInsets.only(
                                  left: width * 0.075,
                                  right: width * 0.075,
                                  top: width * 0.05,
                                  bottom: width * 0.05,
                                ),
                                child: const LoadingListItem(),
                              );
                      },
                      itemCount: _isLoaded ? requestModel!.getProviderList().length : 0,
                    ),
                  ),
                ],
              ),
            ),
            Stack(
              children: <Widget>[
                Align(
                  alignment: Alignment.topCenter,
                  child: Container(
                    height: height * 0.1,
                    color: UIConstants.paleLilac,
                  ),
                ),

                /// Mini title with close button.
                MiniHeaderTemplate(
                  text: _hasContract ? I18n.of(context).dataReqMiniTitle : I18n.of(context).dataReqRetrievalMiniTitle,
                  closeBtnCall: _onScreenCloseEvent,
                ),
              ],
            ),
            Padding(
              padding: EdgeInsets.only(
                right: width * 0.075,
                left: width * 0.075,
                bottom: height / 18,
              ),
              child: Align(
                alignment: Alignment.bottomCenter,
                child: (_isLoaded)
                    ? MainButton(
                        type: BtnType.active,
                        text: _activeBtnText,
                        call: () => _generateDataFlow(),
                      )
                    : LoadingAnimation(
                        height: width * 0.15,
                        width: width,
                        radius: 8,
                      ),
              ),
            )
          ],
        ),
      ),
    );
  }

  /// Closes the screen.
  void _onScreenCloseEvent() {
    var connectionId = SessionNext().get<int>('connectionId') ?? 0;
    Navigator.pushNamedAndRemoveUntil(context, '/dashboard', ModalRoute.withName('/'), arguments: {'activeTab': 0, 'isActualVaultOwner': connectionId == 0 ? true : false});
  }

  /// Builds company data.
  Column _buildCompanyData(double width) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: <Widget>[
        Text(
          _mainTitle!,
          style: TextStyle(
            color: UIConstants.grayDarkest100,
            fontSize: width * 0.042,
            fontWeight: FontWeight.w500,
          ),
        ),
        Text(
          _mainSubTitle!,
          style: TextStyle(
            color: UIConstants.gray100,
            fontSize: width * 0.042,
            fontWeight: FontWeight.w400,
          ),
        ),
      ],
    );
  }

  /// Builds calendar.
  Container _buildCalendarItem() {
    return Container(
      padding: const EdgeInsets.only(top: 4, bottom: 4, left: 8, right: 8),
      decoration: BoxDecoration(
        borderRadius: const BorderRadius.all(Radius.circular(20)),
        border: Border.all(color: UIConstants.paleLilac),
      ),
      child: Row(
        mainAxisSize: MainAxisSize.min,
        children: <Widget>[
          Padding(
            padding: const EdgeInsets.only(right: 6),
            child: Icon(
              Icons.calendar_today,
              color: UIConstants.gray100,
              size: MediaQuery.of(context).size.width * 0.04,
            ),
          ),
          Text(
            I18n.of(context).dataReqCalendarLabelTxt,
            style: TextStyle(
              fontFamily: UIConstants.subFontFamily,
              color: UIConstants.gray100,
              fontWeight: FontWeight.w400,
              fontSize: MediaQuery.of(context).size.width * 0.04,
            ),
          ),
        ],
      ),
    );
  }

  /// Builds progress-bar.
  Column _buildProgressColumn(double width) {
    return Column(
      children: <Widget>[
        Padding(
          padding: EdgeInsets.only(bottom: width * 0.02),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: <Widget>[
              Text(
                I18n.of(context).progBarLabelTxt,
                style: TextStyle(
                  fontFamily: UIConstants.subFontFamily,
                  fontSize: width * 0.04,
                  color: UIConstants.grayDarkest100,
                  fontWeight: FontWeight.w400,
                ),
              ),
              Text(
                _progressValueText,
                style: TextStyle(
                  fontSize: width * 0.05,
                  color: UIConstants.grayDarkest100,
                  fontWeight: FontWeight.w500,
                ),
              )
            ],
          ),
        ),
        ProgressBar(value: _progressValue),
      ],
    );
  }

  /// Opens data providers page.
  void _providerClicked(ProviderConfigViewModel model) {
    try {
      itemIndex = requestModel!.getProviderList().indexOf(model);

      requestModel!.setCurrentSelectedIndex(itemIndex);
    } catch (e) {
      _errorHandler.handleError(e.toString(), false, context);
    }

    Navigator.pushNamed(
      context,
      '/data-request/provider',
      arguments: requestModel,
    );
  }

  /// Validates providers and generates the next flow on button click.
  void _generateDataFlow() async {
    ProviderConfigViewModel? model;

    model = _checkNextProvider();

    if (model != null) {
      setState(() => _executePlugin(model!));
    } else {
      _logger.i('All Providers are Completed!');

      if (_hasContract) {
        // Open submission bottom sheet.
        Navigator.pushNamed(
          context,
          '/account/verify',
          arguments: {
            'processType': ProcessType.submitDataRequest,
            'requestModel': requestModel!,
            'pluginController': _pluginController,
          },
        );
      } else {
        // Complete store-only no-contracts data request and redirect to home screen.
        await _requestController.completeLocalRequest(requestModel);
        if (!mounted) return;
        await Navigator.pushNamed(
          context,
          '/dashboard',
          arguments: {'activeTab': 1, 'isActualVaultOwner': true},
        );
      }
    }
  }

  /// Executes plugin dynamically from plugin controller.
  void _executePlugin(ProviderConfigViewModel model) {
    int itemIndex;

    try {
      itemIndex = requestModel!.getProviderList().indexOf(model);

      requestModel!.setCurrentSelectedIndex(itemIndex);

      _pluginController.executePluginDynamic(context, requestModel!, mounted);
    } on Exception catch (exception) {
      _errorHandler.handleError(exception, false, context);
    } catch (e) {
      _errorHandler.handleError(e.toString(), false, context);
    }
  }
}
