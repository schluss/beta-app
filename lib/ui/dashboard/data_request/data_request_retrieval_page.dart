import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:flutter_widget_from_html_core/flutter_widget_from_html_core.dart';
import 'package:provider/provider.dart';
import 'package:schluss_beta_app/constants/ui_constants.dart';
import 'package:schluss_beta_app/providers/impl/data_request_provider.dart';
import 'package:schluss_beta_app/providers/plugin_provider.dart';
import 'package:schluss_beta_app/singleton_injector.dart';
import 'package:schluss_beta_app/translations/i18n.dart';
import 'package:schluss_beta_app/ui/reused_widgets/animated_circle_icon.dart';
import 'package:schluss_beta_app/ui/reused_widgets/main_button.dart';
import 'package:schluss_beta_app/ui/reused_widgets/main_title.dart';
import 'package:schluss_beta_app/ui/reused_widgets/mini_header_template.dart';
import 'package:schluss_beta_app/ui/reused_widgets/progress_button.dart';
import 'package:schluss_beta_app/util/error_handler_util.dart';
import 'package:schluss_beta_app/view_model/request_view_model.dart';

class DataRequestRetrievalPage extends StatefulWidget {
  final RequestViewModel? requestModel;
  final String? pluginData;

  const DataRequestRetrievalPage({
    Key? key,
    this.requestModel,
    this.pluginData,
  }) : super(key: key);

  @override
  // ignore: no_logic_in_create_state
  DataRequestRetrievalPageState createState() => DataRequestRetrievalPageState(
        requestModel,
        pluginData,
      );
}

class DataRequestRetrievalPageState extends State<DataRequestRetrievalPage> with TickerProviderStateMixin {
  final RequestViewModel? requestModel;
  final String? pluginData;

  final DataRequestProvider _dataRequestProvider = singletonInjector<DataRequestProvider>();
  final PluginProvider _pluginProvider = singletonInjector<PluginProvider>();
  final ErrorHandlerUtil _errorHandler = ErrorHandlerUtil();

  late AnimationController _controller;
  late Animation<Offset> _offsetAnimation;
  late AnimationController _controllerScale;

  var scaleValue = 0.9;

  DataRequestRetrievalPageState(
    this.requestModel,
    this.pluginData,
  );

  @override
  void initState() {
    super.initState();

    _pluginProvider.getCurrentProgress(requestModel!);

    try {
      _dataRequestProvider.initPendingScreen(requestModel!, pluginData);
    } catch (e) {
      _errorHandler.handleError(e.toString(), false, context);
    }

    _controllerScale = AnimationController(
      vsync: this,
      lowerBound: 0.9,
      upperBound: 1.0,
      duration: const Duration(milliseconds: 800),
    )..repeat(
        reverse: true,
      );

    _controllerScale.addListener(() {
      setState(() {
        if (_dataRequestProvider.getRequestStatus == RequestState.loading) {
          scaleValue = _controllerScale.value;
        } else {
          scaleValue = 1.0;
        }
      });
    });

    _controller = AnimationController(
      duration: const Duration(milliseconds: 500),
      vsync: this,
    )..repeat(reverse: true);
    _offsetAnimation = Tween<Offset>(
      begin: Offset.zero,
      end: const Offset(0.5, 0.0),
    ).animate(CurvedAnimation(
      parent: _controller,
      curve: Curves.linear,
    ));
  }

  @override
  void dispose() {
    super.dispose();

    _controllerScale.dispose();
    _controller.dispose();
  }

  @override
  Widget build(BuildContext context) {
    double width = MediaQuery.of(context).size.width;
    double height = MediaQuery.of(context).size.height;

    return WillPopScope(
      onWillPop: () async {
        _generateButtonEvent();

        return true;
      },
      child: ChangeNotifierProvider<DataRequestProvider?>(
        create: (context) => _dataRequestProvider,
        child: Consumer<DataRequestProvider>(
          builder: (context, model, child) => Scaffold(
            backgroundColor: UIConstants.primaryColor,
            body: Stack(
              children: <Widget>[
                Center(
                  child: Stack(
                    children: <Widget>[
                      Stack(
                        children: <Widget>[
                          Container(
                            alignment: Alignment.centerLeft,
                            child: Image(
                              image: const AssetImage(
                                'assets/images/pulse.png',
                              ),
                              width: width * 0.53,
                            ),
                          ),
                          Container(
                            alignment: Alignment.centerRight,
                            child: Image(
                              image: const AssetImage(
                                'assets/images/pulse.png',
                              ),
                              width: width * 0.53,
                            ),
                          ),
                        ],
                      ),
                      Padding(
                        padding: EdgeInsets.only(
                          left: width * 0.075,
                          right: width * 0.075,
                        ),
                        child: Center(
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                            children: <Widget>[
                              Transform.scale(
                                scale: scaleValue,
                                child: AnimatedCircleIcon(
                                  img: model.getProviderLogo!.isNotEmpty
                                      ? (model.getProviderLogo!.toLowerCase().endsWith('.svg'))
                                          ? SizedBox(
                                              width: width * 0.075,
                                              height: width * 0.075,
                                              child: SvgPicture.network(model.getProviderLogo!),
                                            )
                                          : Image(
                                              image: CachedNetworkImageProvider(
                                                model.getProviderLogo!,
                                              ),
                                              width: width * 0.075,
                                              height: width * 0.075,
                                            )
                                      : const Text(''),
                                  isError: model.getRequestStatus == RequestState.error,
                                ),
                              ),
                              SlideTransition(
                                position: _offsetAnimation,
                                child: Container(
                                  width: width * 0.085,
                                  height: width * 0.085,
                                  decoration: const BoxDecoration(
                                    shape: BoxShape.circle,
                                    color: UIConstants.primaryColor,
                                    boxShadow: [
                                      BoxShadow(
                                        color: UIConstants.gray50,
                                        offset: Offset(0, 1),
                                        blurRadius: 2,
                                        spreadRadius: 0,
                                      ),
                                    ],
                                  ),
                                  child: Align(
                                    alignment: Alignment.center,
                                    child: Image(
                                      image: AssetImage(_getCentralIconPath()),
                                      width: (model.getRequestStatus == RequestState.error) ? width * 0.035 : width * 0.045,
                                    ),
                                  ),
                                ),
                              ),
                              AnimatedCircleIcon(
                                img: Image(
                                  image: const AssetImage(
                                    'assets/images/save_box_icon.png',
                                  ),
                                  width: width * 0.125,
                                  height: width * 0.125,
                                ),
                                isShadowPink: true,
                                isError: model.getRequestStatus == RequestState.error,
                              ),
                            ],
                          ),
                        ),
                      ),
                    ],
                  ),
                ),
                Stack(
                  children: <Widget>[
                    Align(
                      alignment: Alignment.topCenter,
                      child: Container(
                        padding: EdgeInsets.only(top: height * 0.2),
                        child: Column(
                          children: <Widget>[
                            MainTitle(
                              text: (model.getRequestStatus == RequestState.error)
                                  ? I18n.of(
                                      context,
                                    ).dataReqRetrievalErrorTitle
                                  : model.getProviderTitle!,
                            ),
                            Text(
                              '${I18n.of(context).viaTxt} ${model.getProviderName!}',
                              style: TextStyle(
                                color: UIConstants.gray100,
                                fontSize: width * 0.05,
                              ),
                            ),
                          ],
                        ),
                      ),
                    ),
                    Align(
                      alignment: Alignment.bottomCenter,
                      child: Container(
                        padding: EdgeInsets.only(
                          bottom: height * 0.25,
                          left: width * 0.2,
                          right: width * 0.2,
                        ),
                        child: _loadBottomText(width),
                      ),
                    ),
                    Align(
                      alignment: Alignment.topCenter,
                      child: Container(
                        height: height * 0.1,
                        color: UIConstants.paleLilac,
                      ),
                    ),

                    /// Mini title with close button.
                    MiniHeaderTemplate(
                      text: I18n.of(context).dataReqRetrievalMiniTitle,
                      closeBtnCall: _onScreenCloseEvent,
                    ),
                  ],
                ),
                Padding(
                  padding: EdgeInsets.only(
                    right: width * 0.075,
                    left: width * 0.075,
                    bottom: height / 18,
                  ),
                  child: Align(
                    alignment: Alignment.bottomCenter,
                    child: ChangeNotifierProvider<PluginProvider?>(
                      create: (context) => _pluginProvider,
                      child: Consumer<PluginProvider>(
                        builder: (context, value, child) => _pluginProvider.progressValue != 100
                            ? ProgressButton(
                                value: _pluginProvider.progressValue / 100,
                                text: _getButtonText(),
                              )
                            : MainButton(
                                type: model.getRequestStatus == RequestState.loading ? BtnType.awaited : BtnType.active,
                                text: _getButtonText(),
                                call: () => _generateButtonEvent(),
                              ),
                      ),
                    ),
                  ),
                )
              ],
            ),
          ),
        ),
      ),
    );
  }

  /// Closes the screen.
  void _onScreenCloseEvent() {
    Navigator.popAndPushNamed(
      context,
      '/data-request',
      arguments: {
        'model': widget.requestModel,
        'isOpen': false,
      },
    );
  }

  /// Generates main button event as the [RequestState].
  void _generateButtonEvent() {
    if (_dataRequestProvider.getRequestStatus == RequestState.success) {
      Navigator.pushNamedAndRemoveUntil(
        context,
        '/data-request',
        ModalRoute.withName('/dashboard'),
        arguments: {
          'model': _dataRequestProvider.getRequestViewModel,
          'isOpen': false,
        },
      );
    } else if (_dataRequestProvider.getRequestStatus == RequestState.error) {
      _onScreenCloseEvent();
    }
  }

  /// Loads the [_path] of the center animation image and changes animation status.
  String _getCentralIconPath() {
    String path;

    if (_dataRequestProvider.getRequestStatus == RequestState.success) {
      path = 'assets/images/green_check_icon.png';

      _stopAnimation();
    } else if (_dataRequestProvider.getRequestStatus == RequestState.loading) {
      path = 'assets/images/arrow_icon.png';
    } else {
      path = 'assets/images/grey_cross_icon.png';

      _stopAnimation();
    }
    return path;
  }

  /// Stops animation.
  void _stopAnimation() {
    scaleValue = 1.0;

    _controller.reset();
    _controller.stop();
    _controllerScale.stop();
  }

  /// Loads bottom description [_text] as the [RequestState].
  Widget _loadBottomText(double width) {
    Widget text;

    if (_dataRequestProvider.getRequestStatus == RequestState.success) {
      text = _buildRichText(width);
    } else if (_dataRequestProvider.getRequestStatus == RequestState.loading) {
      text = const Text('');
    } else {
      text = Text(
        I18n.of(context).dataReqRetrievalErrorTxt,
        textAlign: TextAlign.center,
        style: TextStyle(
          color: UIConstants.gray100,
          fontSize: width * 0.045,
        ),
      );
    }
    return text;
  }

  /// Builds link text of the bottom description.
  HtmlWidget _buildRichText(double width) {
    return HtmlWidget(
      '<center>${I18n.of(context).dataReqRetrievalLinkTxt}</center>',
      onTapUrl: (url) {
        Navigator.pushNamed(
          context,
          '/data-request/provider',
          arguments: _dataRequestProvider.getRequestViewModel,
        );
        return false;
      },
      textStyle: TextStyle(
        fontSize: width * 0.045,
        color: UIConstants.gray100,
      ),
    );
  }

  /// Loads text of the button.
  String _getButtonText() {
    String text;

    if (_dataRequestProvider.getRequestStatus == RequestState.success) {
      text = I18n.of(context).btnContinueTxt;
    } else if (_dataRequestProvider.getRequestStatus == RequestState.loading) {
      text = I18n.of(context).dataReqRetrievalLoadingBtnTxt;
    } else {
      text = I18n.of(context).dataReqRetrievalErrorBtnTxt;
    }
    return text;
  }
}
