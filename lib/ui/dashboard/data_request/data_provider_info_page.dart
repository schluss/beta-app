import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:logger/logger.dart';
import 'package:schluss_beta_app/constants/ui_constants.dart';
import 'package:schluss_beta_app/controller/plugin_controller.dart';
import 'package:schluss_beta_app/singleton_injector.dart';
import 'package:schluss_beta_app/translations/i18n.dart';
import 'package:schluss_beta_app/ui/reused_widgets/data_list_item.dart';
import 'package:schluss_beta_app/ui/reused_widgets/group_header.dart';
import 'package:schluss_beta_app/ui/reused_widgets/main_button.dart';
import 'package:schluss_beta_app/ui/reused_widgets/mini_header_template.dart';
import 'package:schluss_beta_app/ui/reused_widgets/provider_logo.dart';
import 'package:schluss_beta_app/ui/reused_widgets/provider_retrieval_info.dart';
import 'package:schluss_beta_app/util/error_handler_util.dart';
import 'package:schluss_beta_app/view_model/data_detail_viewmodel.dart';
import 'package:schluss_beta_app/view_model/provider_config_view_model.dart';
import 'package:schluss_beta_app/view_model/request_view_model.dart';

class DataProviderInfoPage extends StatefulWidget {
  final RequestViewModel? requestModel;

  const DataProviderInfoPage({
    Key? key,
    this.requestModel,
  }) : super(key: key);

  @override
  // ignore: no_logic_in_create_state
  DataProviderInfoPageState createState() => DataProviderInfoPageState(
        requestModel: requestModel,
      );
}

class DataProviderInfoPageState extends State<DataProviderInfoPage> {
  final PluginController _pluginController = PluginController();
  final Logger _logger = singletonInjector<Logger>();
  final RequestViewModel? requestModel;
  final ErrorHandlerUtil _errorHandler = ErrorHandlerUtil();

  late DataDetailViewModel _dataModel;
  ProviderConfigViewModel? _providerViewModel;
  DataProviderInfoPageState({this.requestModel});

  String? _dateRequested;
  String? _title;

  @override
  void initState() {
    _initScreenData();

    super.initState();
  }

  @override
  void dispose() {
    super.dispose();
  }

  /// Initializes data elements in screen.
  void _initScreenData() async {
    int? providerIndex;
    String? dateString;

    try {
      providerIndex = requestModel!.getCurrentSelectedIndex();
      _providerViewModel = requestModel!.getProviderList().elementAt(providerIndex!);
      _dataModel = DataDetailViewModel.fromProviderModel(_providerViewModel!);
    } catch (e) {
      _errorHandler.handleError(e.toString(), false, context);
    }

    _title = _providerViewModel!.request;

    // Format date field with Netherland locale.
    try {
      dateString = DateFormat('d MMMM yyyy', 'nl').format(
        DateTime.fromMillisecondsSinceEpoch(requestModel!.requestDate!),
      );
    } catch (e) {
      _errorHandler.handleError(e.toString(), false, context);
    }

    _dataModel.setDateRequested = dateString;
    _dateRequested = (_dataModel.dateRequested == null || !_dataModel.isCompleted!) ? '-' : _dataModel.dateRequested;
  }

  @override
  Widget build(BuildContext context) {
    double width = MediaQuery.of(context).size.width;
    double height = MediaQuery.of(context).size.height;

    return SafeArea(
      child: Scaffold(
        backgroundColor: UIConstants.primaryColor,
        body: Stack(
          children: <Widget>[
            SingleChildScrollView(
              child: Container(
                height: height,
                width: width,
                color: UIConstants.primaryColor,
                child: ListView(
                  padding: EdgeInsets.only(
                    top: height * 0.08,
                    bottom: height * 0.2,
                  ),
                  children: <Widget>[
                    SizedBox(height: height * 0.02),
                    Stack(
                      children: <Widget>[
                        ProviderRetrievalInfo(
                          title: I18n.of(context).providerInfoSrcTxt,
                          subTitle: _dataModel.requesterName!,
                          heightScale: 0.06,
                          isTitleBold: true,
                          isSubTitleBold: true,
                        ),
                        Align(
                          alignment: Alignment.centerRight,
                          child: Padding(
                            padding: EdgeInsets.only(right: width * 0.05),
                            child: ProviderLogo(logoImg: _dataModel.logoUrl!),
                          ),
                        ),
                      ],
                    ),
                    ProviderRetrievalInfo(
                      title: I18n.of(context).providerInfoDateTxt,
                      subTitle: _dateRequested!,
                      isTitleBold: true,
                      isSubTitleBold: true,
                    ),
                    SizedBox(height: height * 0.005),
                    ...buildChildren(_dataModel, width, height, context)
                  ],
                ),
              ),
            ),

            /// Mini title with back button.
            MiniHeaderTemplate(
              text: _title,
              hasBackBtn: true,
            ),

            Padding(
              padding: EdgeInsets.only(
                right: width * 0.075,
                left: width * 0.075,
                bottom: height / 18,
              ),

              // TODO: need to improve this business logic. There is a dependency with plugin-me.
              child: _providerViewModel!.isChecked && _providerViewModel!.plugin != 'plugin-me'
                  ? null
                  : Align(
                      alignment: Alignment.bottomCenter,
                      child: MainButton(
                        type: BtnType.active,
                        text: I18n.of(context).providerInfoBtnTxt,
                        call: () => _executePlugin(_providerViewModel),
                      ),
                    ),
            ),
          ],
        ),
      ),
    );
  }

  /// Builds children providers.
  List<Widget> buildChildren(
    DataDetailViewModel model,
    double width,
    double height,
    BuildContext context,
  ) {
    List<Widget> children;
    children = <Widget>[];

    // Parse map without groups.
    if (model.dataMap!.keys.length == 1 && model.dataMap!.keys.first!.isEmpty) {
      children.add(const GroupHeader(text: ''));

      for (var list in model.dataMap!.values) {
        for (var element in list) {
          children.add(
            DataListItem(
              model: element,
            ),
          );
        }
      }
    } else {
      // Parse map with groups.
      model.dataMap!.forEach(
        (key, value) {
          children.add(GroupHeader(text: key!.toUpperCase()));

          for (var element in value) {
            children.add(
              DataListItem(
                model: element,
              ),
            );
          }
        },
      );
    }
    return children;
  }

  /// Executes plug-in dynamically from plugin controller.
  void _executePlugin(ProviderConfigViewModel? model) {
    int itemIndex;

    try {
      itemIndex = requestModel!.getProviderList().indexOf(model!);
      requestModel!.setCurrentSelectedIndex(itemIndex);
      _pluginController.executePluginDynamic(context, requestModel!, mounted);
    } on Exception catch (exception) {
      _errorHandler.handleError(exception, false, context);
    } catch (e) {
      _logger.e(e);
      _errorHandler.handleError(e.toString(), false, context);
    }
  }
}
