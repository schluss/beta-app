import 'package:flutter/material.dart';
import 'package:one_context/one_context.dart';
import 'package:schluss_beta_app/constants/ui_constants.dart';
import 'package:schluss_beta_app/controller/account_controller.dart';
import 'package:schluss_beta_app/translations/i18n.dart';
import 'package:schluss_beta_app/ui/reused_widgets/bottom_up_sheet_template.dart';
import 'package:schluss_beta_app/ui/reused_widgets/main_button.dart';

class DataRequestGuidelineBottomUpSheet extends StatelessWidget {
  DataRequestGuidelineBottomUpSheet({Key? key}) : super(key: key);

  final AccountController _accountController = AccountController();

  @override
  Widget build(BuildContext context) {
    double height = MediaQuery.of(context).size.height;

    return BottomUpSheetTemplate(
      page: Padding(
        padding: EdgeInsets.only(
          bottom: height * 0.005,
        ),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          crossAxisAlignment: CrossAxisAlignment.center,
          mainAxisSize: MainAxisSize.max,
          children: [
            /// Image.
            Image(
              image: const AssetImage('assets/images/intro_4.png'),
              width: height * 0.15,
            ),

            /// Title.
            Text(
              I18n.of(context).dataReqGuideBottomShtTitle,
              textAlign: TextAlign.center,
              style: TextStyle(
                color: UIConstants.grayDarkest100,
                fontSize: height * 0.035,
                fontWeight: FontWeight.w700,
              ),
            ),

            /// Description lists.
            ListTile(
              leading: const Padding(
                padding: EdgeInsets.symmetric(
                  horizontal: 0.0,
                  vertical: 6.0,
                ),
                child: Image(
                  image: AssetImage('assets/images/piggy_safe_icon.png'),
                  width: 28,
                ),
              ),
              title: Text(
                I18n.of(context).dataReqGuideBottomShtTile1Header,
                style: TextStyle(
                  color: UIConstants.grayDarkest100,
                  fontSize: height * 0.025,
                  fontWeight: FontWeight.w600,
                ),
              ),
              subtitle: Text(
                I18n.of(context).dataReqGuideBottomShtTile1Txt,
                style: TextStyle(
                  color: UIConstants.gray100,
                  fontSize: height * 0.020,
                  fontFamily: UIConstants.subFontFamily,
                ),
              ),
            ),
            ListTile(
              leading: const Padding(
                padding: EdgeInsets.symmetric(
                  horizontal: 0.0,
                  vertical: 6.0,
                ),
                child: Image(
                  image: AssetImage('assets/images/master_icon.png'),
                  width: 28,
                ),
              ),
              title: Text(
                I18n.of(context).dataReqGuideBottomShtTile2Header,
                style: TextStyle(
                  color: UIConstants.grayDarkest100,
                  fontSize: height * 0.025,
                  fontWeight: FontWeight.w600,
                ),
              ),
              subtitle: Text(
                I18n.of(context).dataReqGuideBottomShtTile2Txt,
                style: TextStyle(
                  color: UIConstants.gray100,
                  fontSize: height * 0.020,
                  fontFamily: UIConstants.subFontFamily,
                ),
              ),
            ),

            /// Ignore button.
            MainButton(
              type: BtnType.whitish,
              text: I18n.of(context).dataReqGuideBottomShtIgnoreBtnTxt,
              call: () async {
                await _accountController.changeTourDataRequestOpen(false);

                OneContext().pop();
              },
            ),

            /// Main button.
            MainButton(
              type: BtnType.active,
              text: I18n.of(context).dataReqGuideBottomShtBtnTxt,
              call: () => Navigator.pop(context),
            ),
          ],
        ),
      ),
      heightRatio: 0.02,
    );
  }
}
