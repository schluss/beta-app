/*
 * @create date: 2020-05-22
 * 
 */

import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:provider/provider.dart';
import 'package:schluss_beta_app/constants/helper_constants.dart';
import 'package:schluss_beta_app/constants/ui_constants.dart';
import 'package:schluss_beta_app/controller/account_controller.dart';
import 'package:schluss_beta_app/data/models/contract/contract_model.dart';
import 'package:schluss_beta_app/providers/impl/home_provider.dart';
import 'package:schluss_beta_app/singleton_injector.dart';
import 'package:schluss_beta_app/translations/i18n.dart';
import 'package:schluss_beta_app/ui/dashboard/base.dart';
import 'package:schluss_beta_app/ui/dashboard/contracts/contract_provider_item.dart';
import 'package:schluss_beta_app/ui/reused_widgets/empty_state_content_template.dart';
import 'package:schluss_beta_app/ui/reused_widgets/group_header.dart';
import 'package:schluss_beta_app/ui/reused_widgets/header_template.dart';
import 'package:schluss_beta_app/ui/reused_widgets/main_button.dart';
import 'package:schluss_beta_app/ui/reused_widgets/notification_card.dart';
import 'package:schluss_beta_app/ui/reused_widgets/notification_group_header.dart';
import 'package:schluss_beta_app/ui/reused_widgets/providers_list_item.dart';
import 'package:schluss_beta_app/ui/reused_widgets/user_shifting_header_template.dart';
import 'package:schluss_beta_app/util/error_handler_util.dart';
import 'package:schluss_beta_app/view_model/contract_recent_item.dart';
import 'package:schluss_beta_app/view_model/my_data_view_model.dart';
import 'package:schluss_beta_app/view_model/request_view_model.dart';
import 'package:schluss_beta_app/view_model/vault_switch_item_view_model.dart';
import 'package:session_next/session_next.dart';

class Home extends StatefulWidget {
  const Home({Key? key}) : super(key: key);

  @override
  HomeState createState() => HomeState();
}

class HomeState extends State<Home> with WidgetsBindingObserver {
  final HomeProvider _homeProvider = singletonInjector<HomeProvider>();
  final ErrorHandlerUtil _errorHandler = ErrorHandlerUtil();
  final AccountController _accountController = AccountController();

  String? firstName = '';

  /// Keeps data is available or not.
  final _isNullState = false;
  bool _isStewardshipModeOn = false;
  //bool _isDataSharingModeOn = false; // TODO: data sharing interconnect with stewardship.
  bool _isTestModeOn = false;

  bool _isActualVaultOwner = true;

  /// Manages your flutter application states.
  AppLifecycleState? _appLifecycleState;

  final key = GlobalKey();

  OverlayEntry? topDarkBanner;
  final layerLink = LayerLink();

  @override
  void initState() {
    super.initState();

    WidgetsBinding.instance.addObserver(this);

    try {
      // Set wether the current vault is the owners' one or not
      var connectionId = SessionNext().get<int>('connectionId') ?? 0;
      _isActualVaultOwner = connectionId == 0;

      // Set modes.
      _setModes();

      // Get Tips
      _homeProvider.checkTips();

      // Get the pending data request.
      _homeProvider.checkPendingRequests();

      // Get all the completed contracts.
      if (_isActualVaultOwner) {
        _homeProvider.checkFinishedContracts();
      }
    } catch (e) {
      _errorHandler.handleError(e.toString(), false, context);
    }
  }

  /// Sets modes.
  void _setModes() async {
    bool stewardshipModeStatus = await _homeProvider.isStewardshipModeOn();
    //bool _dataSharingModeStatus = await _homeProvider!.isDataSharingModeOn(); // TODO: data sharing interconnect with stewardship.
    bool testModeStatus = await _homeProvider.isTestModeOn();

    setState(() {
      _isStewardshipModeOn = stewardshipModeStatus;
      //_isDataSharingModeOn = _dataSharingModeStatus; // TODO: data sharing interconnect with stewardship.
      _isTestModeOn = testModeStatus;
      _isActualVaultOwner = _isActualVaultOwner;
    });

    _showTopDarkBanner();
  }

  @override
  void didChangeDependencies() {
    super.didChangeDependencies();
  }

  @override
  Future<void> didChangeAppLifecycleState(AppLifecycleState state) async {
    _appLifecycleState = state;

    if (_appLifecycleState == AppLifecycleState.resumed) {
      try {
        _homeProvider.checkPendingRequests();
      } catch (e) {
        _errorHandler.handleError(e.toString(), false, context);
      }
    }
  }

  @override
  void dispose() {
    WidgetsBinding.instance.removeObserver(this);

    _hideTopDarkBanner();

    super.dispose();
  }

  /// Shows top dark banner.
  void _showTopDarkBanner() {
    if (!_isActualVaultOwner && topDarkBanner == null) {
      WidgetsBinding.instance.addPostFrameCallback((_) {
        if (!mounted) return;

        final overlay = Overlay.of(context);

        final renderBox = key.currentContext?.findRenderObject() as RenderBox;

        topDarkBanner = OverlayEntry(
          maintainState: true,
          builder: (context) => _buildTopDarkBanner(context, renderBox),
        );

        overlay.insert(topDarkBanner!);
      });
    }
  }

  /// Hides top dark banner.
  void _hideTopDarkBanner() {
    topDarkBanner?.remove();
    topDarkBanner = null;
  }

  /// Top dark banner.
  Widget _buildTopDarkBanner(BuildContext context, RenderBox renderBox) {
    if (!SessionNext().containsKey('switchConnections') || SessionNext().get<List<VaultSwitchItemViewModel>>('switchConnections')!.isEmpty) {
      return Container();
    }

    final size = renderBox.size;
    final offSet = renderBox.localToGlobal(Offset.zero);

    return Positioned(
      top: offSet.dy + MediaQuery.of(context).padding.top,
      width: size.width,
      height: size.height * 0.18,
      child: CompositedTransformFollower(
        link: layerLink,
        showWhenUnlinked: false,
        child: Container(
          margin: EdgeInsets.only(top: MediaQuery.of(context).padding.top),
          color: UIConstants.grayDarkest100,
          padding: EdgeInsets.symmetric(
            horizontal: MediaQuery.of(context).size.width * 0.05,
            vertical: MediaQuery.of(context).size.width * 0.03,
          ),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              /// Note text.
              Expanded(
                flex: 5,
                child: RichText(
                  textAlign: TextAlign.start,
                  softWrap: true,
                  maxLines: 2,
                  text: TextSpan(
                    style: const TextStyle(
                      fontFamily: UIConstants.subFontFamily,
                      color: UIConstants.primaryColor,
                      fontSize: 13,
                    ),
                    children: <TextSpan>[
                      TextSpan(
                        text: I18n.of(context).vaultSwitchedTopDarkBannerTxt(
                          userForenameText: _accountController.getCurrentVaultName(),
                        ),
                        style: const TextStyle(fontWeight: FontWeight.w400),
                      )
                    ],
                  ),
                ),
              ),

              /// Button.
              Expanded(
                flex: 5,
                child: Padding(
                  padding: EdgeInsets.symmetric(vertical: MediaQuery.of(context).size.height * 0.012),
                  child: MainButton(
                    type: BtnType.outlineDark,
                    text: I18n.of(context).vaultSwitchedTopDarkBannerBtnTxt,
                    textColor: UIConstants.primaryColor,
                    call: () {
                      _accountController.changeVaultOwner(0);

                      Navigator.pushNamed(
                        context,
                        '/dashboard',
                        arguments: {'activeTab': 0, 'isActualVaultOwner': true},
                      );

                      topDarkBanner?.remove();
                      topDarkBanner = null;
                    },
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    double height = MediaQuery.of(context).size.height;
    double width = MediaQuery.of(context).size.width;
    double topPadding = MediaQuery.of(context).padding.top;

    return CompositedTransformTarget(
      link: layerLink,
      child: Scaffold(
        key: key,
        appBar: PreferredSize(
          preferredSize: const Size.fromHeight(0.0),
          child: AppBar(
            elevation: 0.0,
            backgroundColor: _isActualVaultOwner ? UIConstants.primaryColor : UIConstants.grayDarkest100,
            systemOverlayStyle: _isActualVaultOwner ? SystemUiOverlayStyle.dark : SystemUiOverlayStyle.light,
          ),
        ),
        resizeToAvoidBottomInset: false,
        body: Container(
          margin: EdgeInsets.only(top: !_isActualVaultOwner ? height * 0.12 : height * 0.0),
          child: GestureDetector(
            onTap: () {
              FocusScope.of(context).requestFocus(FocusNode());
            },
            child: ChangeNotifierProvider<HomeProvider?>(
              create: (context) => _homeProvider,
              child: Consumer<HomeProvider>(
                builder: (context, model, child) => Container(
                  color: UIConstants.primaryColor,
                  height: height,
                  child: AnimatedCrossFade(
                    crossFadeState: CrossFadeState.showFirst,
                    firstCurve: Curves.ease,
                    secondCurve: Curves.easeInExpo,
                    duration: const Duration(milliseconds: 1000),
                    firstChild: Stack(
                      children: <Widget>[
                        SizedBox(
                          height: height,
                          child: NestedScrollView(
                            headerSliverBuilder: (
                              BuildContext context,
                              bool innerBoxIsScrolled,
                            ) {
                              return [buildSwitchingHeader()];
                            },
                            body: buildNotificationBody(height, width, context),
                          ),
                        ),
                        ...buildConstraintsList(height, width, topPadding),
                      ],
                    ),
                    secondChild: Container(
                      height: height,
                      color: UIConstants.paleGray,
                      child: buildBody(height, width, context, 10 + topPadding),
                    ),
                  ),
                ),
              ),
            ),
          ),
        ),
      ),
    );
  }

  Widget buildSwitchingHeader() {
    var switchConnections = SessionNext().get<List<VaultSwitchItemViewModel>>('switchConnections') ?? [];

    // If there are more then 1 switchable connections show the switching ui
    if (switchConnections.length > 1) {
      return UserShiftingHeaderTemplate(text: _accountController.getCurrentVaultName());
    }
    // If there's only one connection, just return the 'Overview' header title
    else {
      return HeaderTemplate(
        text: I18n.of(context).homeTitle,
      );
    }
  }

  /// Builds notification body.
  Column buildNotificationBody(
    double height,
    double width,
    BuildContext context,
  ) {
    return Column(
      children: <Widget>[
        Expanded(
          // Change [_isNullState] variable to set either null state or build the lists.
          // Updates this logic using business logic.
          child: _isNullUIState()
              ? EmptyStateContentTemplate(
                  img: const AssetImage('assets/images/dashboard_null.png'),
                  text: I18n.of(context).homeNoDataReqTxt,
                )
              : Container(
                  color: UIConstants.paleGray,
                  child: ListView(
                    scrollDirection: Axis.vertical,
                    shrinkWrap: true,
                    padding: EdgeInsetsDirectional.only(bottom: height * 0.02),
                    children: <Widget>[
                      ...buildTips(),
                      ...buildMinimizedChildren(
                        _homeProvider.minimizedList,
                        context,
                      ),
                      ...buildDoneChildren(_homeProvider.doneList, context),
                      Padding(
                        padding: EdgeInsets.only(
                          top: width * 0.04,
                          left: width * 0.18,
                          right: width * 0.18,
                        ),
                      ),
                    ],
                  ),
                ),
        ),
      ],
    );
  }

  /// Builds constraints list.
  List<Widget> buildConstraintsList(
    double height,
    double width,
    double topPadding,
  ) {
    List<Widget> children;
    children = <Widget>[];
    return children;
  }

  /// Builds body.
  Column buildBody(
    double height,
    double width,
    BuildContext context,
    double topPadding,
  ) {
    return Column(
      children: <Widget>[
        Padding(
          padding: EdgeInsets.only(
            left: getPadding(context),
            right: getPadding(context) / 2,
            top: topPadding,
            bottom: 10,
          ),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceAround,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              FittedBox(
                alignment: Alignment.centerRight,
                fit: BoxFit.fitWidth,
                child: GestureDetector(
                  onTap: () {},
                  child: Text(
                    I18n.of(context).btnCancelTxt,
                    style: const TextStyle(
                      fontFamily: UIConstants.subFontFamily,
                      color: UIConstants.gray100,
                      fontWeight: FontWeight.w600,
                    ),
                  ),
                ),
              ),
            ],
          ),
        ),
        Expanded(
          child: _isNullUIState() // Data not available.
              ? EmptyStateContentTemplate(
                  img: const AssetImage('assets/images/dashboard_null.png'),
                  text: I18n.of(context).homeNoDataReqTxt,
                )
              : Container(
                  color: UIConstants.paleGray,
                  child: ListView(
                    scrollDirection: Axis.vertical,
                    shrinkWrap: true,
                    padding: EdgeInsets.zero,
                    children: <Widget>[
                      ...buildDataChildren(_homeProvider.dataList, width),
                      ...buildContractsChildren(
                        _homeProvider.contractsList,
                        width,
                      ),
                    ],
                  ),
                ),
        ),
      ],
    );
  }

  /// Builds notification tips.
  List<Widget> buildTips() {
    // Tip are not showed when all mode are disabled.
    /* if (!_isStewardshipModeOn && !_isDataSharingModeOn) { */ // TODO: data sharing interconnect with stewardship.
    if (!_isStewardshipModeOn) {
      return <Widget>[];
    }

    List<Widget> children;
    children = <Widget>[];

    if (_homeProvider.tipsList.isNotEmpty) {
      children.add(
        NotificationGroupHeader(
          text: I18n.of(context).homeTipsListHeader,
        ),
      );

      for (var i = 0; i < _homeProvider.tipsList.length; i++) {
        var model = _homeProvider.tipsList[i];

        // If there are params, also include them in the route to be navigated to.
        var params = {};
        if (model.routeParams != null) {
          params = jsonDecode(model.routeParams!);
        }

        children.add(
          NotificationCard(
            title: I18n.of(context).getString(model.titleKey!),
            description: I18n.of(context).getString(model.descriptionKey!),
            activeBtnText: I18n.of(context).btnStartTxt,
            activeBtnCall: () => Navigator.pushNamed(context, model.routeNameKey!, arguments: params),
          ),
        );
      }
    }

    return children;
  }

  /// Builds in-progress data requests.
  List<Widget> buildMinimizedChildren(
    List<RequestViewModel> minimizedList,
    BuildContext context,
  ) {
    List<Widget> children;
    children = <Widget>[];

    if (minimizedList.isNotEmpty) {
      children.add(
        NotificationGroupHeader(
          text: I18n.of(context).homeMinimizedListHeader,
        ),
      );

      for (var i = 0; i < minimizedList.length; i++) {
        children.add(
          NotificationCard(
            logoImg: minimizedList[i].logoUrl!,
            title: minimizedList[i].name!,
            subtitle: minimizedList[i].importName!,
            description: minimizedList[i].importDescription!,
            activeBtnText: I18n.of(context).btnContinueTxt,
            totalValue: minimizedList[i].getProviderList().length,
            progressiveValue: minimizedList[i]
                .getProviderList()
                .where(
                  (element) => element.isChecked == true,
                )
                .length,
            closeBtnCall: () => _homeProvider.removePendingRequest(minimizedList[i]),
            activeBtnCall: () => Navigator.pushNamed(
              context,
              '/data-request',
              arguments: {
                'model': minimizedList[i],
                'isOpen': true,
              },
            ),
          ),
        );
      }
    }
    return children;
  }

  /// Builds minimized list header.
  Align buildMinimizedListHeader(BuildContext context) {
    return Align(
      alignment: Alignment.centerLeft,
      child: Padding(
        padding: EdgeInsets.only(
          left: getPadding(context),
          top: MediaQuery.of(context).size.height * 0.01,
        ),
        child: Text(
          I18n.of(context).homeMinimizedListHeader,
          style: TextStyle(
            color: UIConstants.gray100,
            fontSize: MediaQuery.of(context).size.width * 0.04,
          ),
        ),
      ),
    );
  }

  /// Builds completed contract notification.
  List<Widget> buildDoneChildren(
    List<ContractItem> doneList,
    BuildContext context,
  ) {
    List<Widget> children;
    children = <Widget>[];

    // Parse the list.
    if (doneList.isNotEmpty) {
      children.add(
        NotificationGroupHeader(
          text: I18n.of(context).homeDoneListHeader,
          closeBtnCall: _homeProvider.clearContractsDoneList,
        ),
      );

      for (var i = 0; i < doneList.length; i++) {
        children.add(
          GestureDetector(
            onTap: () {
              _hideTopDarkBanner();
              Navigator.pushNamed(
                context,
                '/contracts/contract',
                arguments: {
                  'selectedId': doneList[i].contractId,
                  'fromUI': HelperConstant.fromHomeUI,
                },
              ).then(
                (value) {
                  _showTopDarkBanner();
                },
              );
            },
            child: doneList[i].type == ConnectionType.user
                ? NotificationCard(
                    isUser: true,
                    logoColor: doneList[i].providerDataItem.iconColor!,
                    title: doneList[i].providerDataItem.title!,
                    doneDate: doneList[i].providerDataItem.subTitle!,
                  )
                : NotificationCard(
                    logoImg: doneList[i].providerDataItem.imagePath!,
                    title: doneList[i].providerDataItem.title!,
                    doneDate: doneList[i].providerDataItem.subTitle!,
                  ),
          ),
        );
      }
    }
    return children;
  }

  /// Builds completed data requests.
  Padding buildDoneListHeader(BuildContext context) {
    double width = MediaQuery.of(context).size.width;

    return Padding(
      padding: EdgeInsets.only(
        top: width * 0.05,
        left: getPadding(context),
      ),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        crossAxisAlignment: CrossAxisAlignment.end,
        children: <Widget>[
          Text(
            I18n.of(context).homeDoneListHeader,
            style: TextStyle(
              color: UIConstants.gray100,
              fontSize: width * 0.04,
            ),
          ),
        ],
      ),
    );
  }

  // TODO: refactor / remove hardcoded checks for [_isTestModeOn!] || [_isDataSharingModeOn!]
  //  || [_isStewardshipModeOn!] checks at least single data is available for showing.
  bool _isNullUIState() {
    /* if (_isTestModeOn || _isDataSharingModeOn || _isStewardshipModeOn) { */ // TODO: data sharing interconnect with stewardship.
    if (_isTestModeOn || _isStewardshipModeOn) {
      return _isNullState || (_homeProvider.doneList.isEmpty && _homeProvider.minimizedList.isEmpty && _homeProvider.tipsList.isEmpty);
    } else {
      return _isNullState || (_homeProvider.doneList.isEmpty && _homeProvider.minimizedList.isEmpty);
    }
  }

  /// Builds data notifications.
  List<Widget> buildDataChildren(List<MyDataViewModel> dataList, double width) {
    List<Widget> children;
    children = <Widget>[];

    if (dataList.isNotEmpty) {
      children.add(
        GroupHeader(text: I18n.of(context).dataTitle.toUpperCase()),
      );

      for (var i = 0; i < dataList.length; i++) {
        children.add(
          GestureDetector(
            onTap: () {
              Navigator.pushNamed(
                context,
                '/data/data',
                arguments: dataList[i],
              );
            },
            child: ProvidersListItem(
              title: dataList[i].category!,
              subTitle: dataList[i].providerName!,
              logo: dataList[i].logoUrl!,
            ),
          ),
        );
        children.add(const Divider(height: 1));
      }
    }
    return children;
  }

  /// Builds contract notification.
  List<Widget> buildContractsChildren(
    List<ContractItem> contractsList,
    double width,
  ) {
    List<Widget> children;
    children = <Widget>[];

    if (contractsList.isNotEmpty) {
      children.add(
        GroupHeader(text: I18n.of(context).contractsTitle.toUpperCase()),
      );
      for (var i = 0; i < contractsList.length; i++) {
        children.add(
          GestureDetector(
            onTap: () {
              Navigator.pushNamed(
                context,
                '/contracts/contract',
                arguments: {
                  'selectedId': contractsList[i].contractId,
                  'fromUI': HelperConstant.fromHomeUI,
                },
              );
            },
            child: ContractProviderItem(
              contractsList[i].providerDataItem,
              contractsList[i].description,
            ),
          ),
        );
        children.add(const Divider(height: 1));
      }
    }
    return children;
  }
}
