import 'package:flutter/material.dart';
import 'package:schluss_beta_app/constants/ui_constants.dart';
import 'package:schluss_beta_app/controller/contract_controller.dart';
import 'package:schluss_beta_app/translations/i18n.dart';
import 'package:schluss_beta_app/ui/reused_widgets/confirmation_bottom_up_sheet_contents_template.dart';

class OwnerRemoveAuthorizationPageContent extends StatefulWidget {
  final String? receiverName;
  final int connectionId;

  const OwnerRemoveAuthorizationPageContent({
    required this.receiverName,
    required this.connectionId,
    Key? key,
  }) : super(key: key);

  @override
  OwnerRemoveAuthorizationPageContentState createState() => OwnerRemoveAuthorizationPageContentState();
}

class OwnerRemoveAuthorizationPageContentState extends State<OwnerRemoveAuthorizationPageContent> {
  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    double width = MediaQuery.of(context).size.width;

    return ConfirmationBottomUpSheetContentsTemplate(
      title: I18n.of(context).stewardshipOwnerDeleteAuthorizationBottomShtTitle,
      mainText: I18n.of(context).stewardshipOwnerDeleteAuthorizationBottomShtTxt(
        userForenameText: widget.receiverName!,
      ),
      cardHeaderText: widget.receiverName!,
      cardContent: Row(
        children: [
          Padding(
            padding: const EdgeInsets.only(right: 8.0),
            child: Image(
              image: const AssetImage('assets/images/green_check_icon.png'),
              height: width * 0.03,
            ),
          ),
          Expanded(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Text(
                  I18n.of(context).stewardshipOwnerAuthorizeBottomCardTxt,
                  softWrap: true,
                  style: TextStyle(
                    color: UIConstants.gray100,
                    fontSize: width * 0.045,
                  ),
                ),
              ],
            ),
          ),
        ],
      ),
      activeBtnText: I18n.of(context).stewardshipOwnerDeleteAuthorizationBottomShtBtnTxt,
      activeBtnCall: () async {
        // Delete the stewardship decoration from the connection
        await ContractController().deleteStewardship(widget.connectionId);

        // Close the windows and go back to the contracts main page.
        if (!mounted) return;
        Navigator.of(context).pop();
        Navigator.of(context).pop();
        Navigator.of(context).pop();
      },
    );
  }
}
