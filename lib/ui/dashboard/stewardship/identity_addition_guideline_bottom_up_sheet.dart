import 'package:flutter/material.dart';
import 'package:schluss_beta_app/constants/ui_constants.dart';
import 'package:schluss_beta_app/translations/i18n.dart';
import 'package:schluss_beta_app/ui/reused_widgets/bottom_up_sheet_template.dart';
import 'package:schluss_beta_app/ui/reused_widgets/main_button.dart';

class IdentityAdditionGuidelineBottomUpSheet extends StatelessWidget {
  const IdentityAdditionGuidelineBottomUpSheet({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    double height = MediaQuery.of(context).size.height;

    return BottomUpSheetTemplate(
      page: Scaffold(
        body: Container(
          height: height,
          color: UIConstants.primaryColor,
          child: Column(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            mainAxisSize: MainAxisSize.min,
            children: [
              Column(
                children: [
                  /// Top image.
                  Image(
                    image: const AssetImage('assets/images/illustratie.png'),
                    width: height * 0.15,
                  ),

                  /// Title.
                  Padding(
                    padding: EdgeInsets.symmetric(vertical: height * 0.03),
                    child: Text(
                      I18n.of(context).stewardshipIdentityAdditionGuidelineBottomShtTitle,
                      textAlign: TextAlign.center,
                      style: TextStyle(
                        color: UIConstants.grayDarkest100,
                        fontSize: height * 0.035,
                        fontWeight: FontWeight.w700,
                      ),
                    ),
                  ),

                  /// Description.
                  Text(
                    I18n.of(
                      context,
                    ).stewardshipIdentityAdditionGuidelineBottomShtDescTxt,
                    style: TextStyle(
                      fontFamily: UIConstants.defaultFontFamily,
                      fontSize: height * 0.025,
                      fontWeight: FontWeight.w400,
                      color: UIConstants.slateGray100,
                    ),
                  ),
                ],
              ),

              /// Main button.
              Column(
                mainAxisAlignment: MainAxisAlignment.end,
                children: [
                  Padding(
                    padding: EdgeInsets.only(bottom: height * 0.025),
                    child: Align(
                      alignment: Alignment.bottomCenter,
                      child: MainButton(
                        type: BtnType.active,
                        text: I18n.of(context).stewardshipIdentityAdditionGuidelineBottomShtBtnTxt,
                        call: () => Navigator.pushNamed(context, '/stewardship/add-identity'),
                      ),
                    ),
                  ),
                ],
              ),
            ],
          ),
        ),
      ),
      heightRatio: 0.05,
    );
  }
}
