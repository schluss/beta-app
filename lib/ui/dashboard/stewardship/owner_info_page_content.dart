import 'package:flutter/material.dart';
import 'package:schluss_beta_app/controller/account_controller.dart';
import 'package:schluss_beta_app/controller/my_data_controller.dart';
import 'package:schluss_beta_app/translations/i18n.dart';
import 'package:schluss_beta_app/ui/reused_widgets/user_feature_page_contents_template.dart';

class OwnerInfoPageContent extends StatefulWidget {
  const OwnerInfoPageContent({Key? key}) : super(key: key);

  @override
  OwnerInfoPageContentState createState() => OwnerInfoPageContentState();
}

class OwnerInfoPageContentState extends State<OwnerInfoPageContent> {
  final MyDataController _myDataController = MyDataController();

  @override
  void initState() {
    // Try to get the stored identity, if no stored yet, initialize getting the identity
    loadIdentityAdditionGuidelineSheet();

    super.initState();
  }

  void loadIdentityAdditionGuidelineSheet() async {
    String firstName = await _myDataController.getPersonalInfoValue(
      'MijnOverheid',
      'FirstNames',
    );

    String surname = await _myDataController.getPersonalInfoValue(
      'MijnOverheid',
      'Surname',
    );

    if (firstName.isEmpty || surname.isEmpty) {
      // Create an 'Add your identity tooltip on the dashboard'
      AccountController().createIdentityTooltip();
      if (!mounted) return;
      Navigator.popAndPushNamed(
        context,
        '/stewardship/identity-addition-guideline',
      );
    }
  }

  @override
  Widget build(BuildContext context) {
    return UserFeaturePageContentsTemplate(
      miniTitle: I18n.of(context).stewardshipBottomShtMiniTitle,
      title: I18n.of(context).stewardshipInfoBottomShtTitle,
      para1TextPart1: I18n.of(context).stewardshipInfoBottomShtPara1Text,
      para2Text: I18n.of(context).stewardshipInfoBottomShtPara2Text,
      activeBtnText: I18n.of(context).btnNextTxt,
      activeBtnCall: () => Navigator.popAndPushNamed(
        context,
        '/stewardship/owner/define-access-timing',
      ),
      activeBtnDisabledOnClick: true,
    );
  }
}
