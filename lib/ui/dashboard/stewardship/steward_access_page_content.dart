import 'package:flutter/material.dart';
import 'package:schluss_beta_app/controller/account_controller.dart';
import 'package:schluss_beta_app/translations/i18n.dart';
import 'package:schluss_beta_app/ui/reused_widgets/user_feature_page_contents_template.dart';

class StewardAccessPageContent extends StatefulWidget {
  final int connectionId;
  final String? senderName;
  final bool hasAccess;

  const StewardAccessPageContent({
    required this.connectionId,
    required this.senderName,
    this.hasAccess = false,
    Key? key,
  }) : super(key: key);

  @override
  StewardAccessPageContentState createState() => StewardAccessPageContentState();
}

class StewardAccessPageContentState extends State<StewardAccessPageContent> {
  final AccountController _accountController = AccountController();

  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return UserFeaturePageContentsTemplate(
      heightRatio: 0.4,
      miniTitle: I18n.of(context).stewardshipBottomShtMiniTitle,
      title: widget.hasAccess ? I18n.of(context).stewardshipStewardAccessBottomShtTitle(userNameText: widget.senderName!) : I18n.of(context).stewardshipStewardAccessProhibitedBottomShtTitle(userNameText: widget.senderName!),
      para1TextPart1: widget.hasAccess ? I18n.of(context).stewardshipStewardAccessBottomShtTxt(userNameText: widget.senderName!) : I18n.of(context).stewardshipStewardAccessProhibitedBottomShtTxt(userNameText: widget.senderName!),
      activeBtnText: widget.hasAccess ? I18n.of(context).stewardshipStewardAccessBottomMainBtnTxt : I18n.of(context).stewardshipStewardAccessProhibitedBottomMainBtnTxt,
      activeBtnCall: () {
        if (widget.hasAccess) {
          _accountController.changeVaultOwner(widget.connectionId).then((value) => Navigator.pushNamedAndRemoveUntil(context, '/dashboard', (r) => false, arguments: {'activeTab': 0}));
        } else {
          Navigator.pop(context);
        }
      },
    );
  }
}
