import 'package:flutter/material.dart';
import 'package:schluss_beta_app/controller/account_controller.dart';
import 'package:schluss_beta_app/translations/i18n.dart';
import 'package:schluss_beta_app/ui/reused_widgets/user_feature_page_contents_template.dart';

class VaultSwitchingInfoPageContent extends StatefulWidget {
  final int? contractId;
  final String? name;

  const VaultSwitchingInfoPageContent({
    this.contractId = 0,
    this.name = '',
    Key? key,
  }) : super(key: key);

  @override
  VaultSwitchingInfoContentState createState() => VaultSwitchingInfoContentState();
}

class VaultSwitchingInfoContentState extends State<VaultSwitchingInfoPageContent> {
  final AccountController _accountController = AccountController();

  @override
  Widget build(BuildContext context) {
    return UserFeaturePageContentsTemplate(
        miniTitle: I18n.of(context).stewardshipBottomShtMiniTitle,
        title: I18n.of(context).vaultsSwitchingInfoBottomShtTitle(
          userForenameText: widget.name!,
        ),
        para1TextPart1: I18n.of(context).vaultsSwitchingInfoBottomShtParaTxtPart1,
        para1TextPart2: I18n.of(context).vaultsSwitchingInfoBottomShtParaTxtPart2,
        para1TextPart3: I18n.of(context).vaultsSwitchingInfoBottomShtParaTxtPart3,
        activeBtnText: I18n.of(context).btnContinueTxt,
        activeBtnCall: () {
          _accountController.changeVaultOwner(widget.contractId);

          Navigator.pushNamed(
            context,
            '/dashboard',
            arguments: {'activeTab': 0, 'isActualVaultOwner': false},
          );
        });
  }
}
