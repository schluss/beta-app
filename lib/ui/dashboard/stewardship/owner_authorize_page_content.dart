import 'package:flutter/material.dart';
import 'package:schluss_beta_app/constants/ui_constants.dart';
import 'package:schluss_beta_app/translations/i18n.dart';
import 'package:schluss_beta_app/ui/reused_widgets/user_feature_page_contents_template.dart';

class OwnerAuthorizePageContent extends StatefulWidget {
  final String? receiverName;
  final int connectionId;

  const OwnerAuthorizePageContent({
    required this.connectionId,
    required this.receiverName,
    Key? key,
  }) : super(key: key);

  @override
  OwnerAuthorizePageContentState createState() => OwnerAuthorizePageContentState();
}

class OwnerAuthorizePageContentState extends State<OwnerAuthorizePageContent> {
  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    double width = MediaQuery.of(context).size.width;

    return UserFeaturePageContentsTemplate(
      miniTitle: I18n.of(context).stewardshipBottomShtMiniTitle,
      title: I18n.of(context).stewardshipOwnerAuthorizeBottomTitle(
        userNameText: widget.receiverName!,
      ),
      para1TextPart1: I18n.of(context).stewardshipOwnerAuthorizeBottomParaTxt(
        userNameText: widget.receiverName!,
      ),
      cardHeaderText: widget.receiverName!,
      cardContent: Row(
        mainAxisAlignment: MainAxisAlignment.start,
        crossAxisAlignment: CrossAxisAlignment.start,
        mainAxisSize: MainAxisSize.max,
        children: [
          Padding(
            padding: const EdgeInsets.only(top: 4.0, right: 8.0),
            child: Image(
              image: const AssetImage('assets/images/green_check_icon.png'),
              height: width * 0.03,
            ),
          ),
          Flexible(
            child: Text(
              I18n.of(context).stewardshipOwnerAuthorizeBottomCardTxt,
              softWrap: true,
              style: TextStyle(
                color: UIConstants.gray100,
                fontSize: width * 0.045,
              ),
            ),
          ),
        ],
      ),
      //outlineBtnText: I18n.of(context).stewardshipOwnerAuthorizeBottomCancelBtnTxt,
      //outlineBtnCall: () {},
      activeBtnText: I18n.of(context).stewardshipOwnerAuthorizeBottomMainBtnTxt,
      activeBtnCall: () => Navigator.popAndPushNamed(
        context,
        '/stewardship/owner/remove',
        arguments: {
          'connectionId': widget.connectionId,
          'receiverName': widget.receiverName,
        },
      ),
    );
  }
}
