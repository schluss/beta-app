import 'package:flutter/material.dart';
import 'package:schluss_beta_app/constants/ui_constants.dart';
import 'package:schluss_beta_app/controller/data_share_controller.dart';
import 'package:schluss_beta_app/translations/i18n.dart';
import 'package:schluss_beta_app/ui/reused_widgets/user_feature_page_contents_template.dart';

class StewardAuthorizePageContent extends StatefulWidget {
  final String senderName;
  final int connectionId;

  const StewardAuthorizePageContent({
    required this.connectionId,
    required this.senderName,
    Key? key,
  }) : super(key: key);

  @override
  StewardAuthorizePageContentState createState() => StewardAuthorizePageContentState();
}

class StewardAuthorizePageContentState extends State<StewardAuthorizePageContent> {
  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    double width = MediaQuery.of(context).size.width;

    return UserFeaturePageContentsTemplate(
      miniTitle: I18n.of(context).stewardshipBottomShtMiniTitle,
      title: I18n.of(context).stewardshipStewardAuthorizeBottomTitle(
        userNameText: widget.senderName,
      ),
      para1TextPart1: I18n.of(context).stewardshipStewardAuthorizeBottomPara1Txt(userNameText: widget.senderName),
      para2Text: I18n.of(context).stewardshipStewardAuthorizeBottomPara2Txt,
      cardHeaderText: widget.senderName,
      cardContent: Row(
        mainAxisAlignment: MainAxisAlignment.start,
        crossAxisAlignment: CrossAxisAlignment.start,
        mainAxisSize: MainAxisSize.max,
        children: [
          Padding(
            padding: const EdgeInsets.only(top: 4.0, right: 8.0),
            child: Image(
              image: const AssetImage('assets/images/green_check_icon.png'),
              height: width * 0.03,
            ),
          ),
          Flexible(
            child: Text(
              I18n.of(context).stewardshipStewardAuthorizeBottomCardTxt(userNameText: widget.senderName),
              softWrap: true,
              style: TextStyle(
                color: UIConstants.gray100,
                fontSize: width * 0.040,
              ),
            ),
          ),
        ],
      ),
      //outlineBtnText: I18n.of(context).stewardshipOwnerAuthorizeBottomMainBtnTxt,
      //outlineBtnCall: () {},
      activeBtnText: I18n.of(context).stewardshipStewardAuthorizeBottomMainBtnTxt,
      activeBtnCall: () => processTriggerReleaseRequest(),
      activeBtnDisabledOnClick: true,
    );
  }

  /// Tries to release the trigger
  void processTriggerReleaseRequest() async {
    //TODO: When running this method, it could take a little time before the trigger is released (or not). Right now there is no feedback to the user, so it seems nothing happens.
    // Quickest fix: disable the mainBtn and show a spinner animation
    // What happens when a user closes the bottomupsheet while waiting? Does the process finish? Also something to check!

    var controller = DataShareController();

    // try to release the trigger
    var isReleased = await controller.releaseTrigger(widget.connectionId);
    if (!mounted) return;
    Navigator.popAndPushNamed(
      context,
      '/stewardship/steward/access',
      arguments: {
        'connectionId': widget.connectionId,
        'senderName': widget.senderName,
        'hasAccess': isReleased,
      },
    );
  }
}
