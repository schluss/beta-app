import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:schluss_beta_app/translations/i18n.dart';
import 'package:schluss_beta_app/ui/reused_widgets/user_feature_page_contents_template.dart';

class AddIdentityBottomUpSheet extends StatelessWidget {
  const AddIdentityBottomUpSheet({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return UserFeaturePageContentsTemplate(
      heightRatio: 0.45,
      miniTitle: '',
      title: I18n.of(context).tipIdentityVerifyTitleTxt,
      para1TextPart1: I18n.of(context).tipIdentityVerifyDescTxt,
      activeBtnText: I18n.of(context).btnStartTxt,
      activeBtnCall: () => Navigator.popAndPushNamed(context, '/identity/add'),
    );
  }
}
