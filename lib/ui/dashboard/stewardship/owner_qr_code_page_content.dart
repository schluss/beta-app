import 'dart:async';
import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:pretty_qr_code/pretty_qr_code.dart';
import 'package:schluss_beta_app/controller/data_share_controller.dart';
import 'package:schluss_beta_app/services/interface/socket_service.dart';
import 'package:schluss_beta_app/singleton_injector.dart';
import 'package:schluss_beta_app/translations/i18n.dart';
import 'package:schluss_beta_app/ui/dashboard/stewardship/owner_define_access_timing_page_content.dart';
import 'package:schluss_beta_app/ui/reused_widgets/user_feature_page_contents_template.dart';

class OwnerQrCodePageContent extends StatefulWidget {
  final String? qrCodeLink;
  final List<int>? sharedAttributesIds;
  final DataAccessType? dataAccessType;
  const OwnerQrCodePageContent({
    Key? key,
    this.qrCodeLink,
    this.sharedAttributesIds,
    this.dataAccessType,
  }) : super(key: key);

  @override
  OwnerQrCodePageContentState createState() => OwnerQrCodePageContentState();
}

class OwnerQrCodePageContentState extends State<OwnerQrCodePageContent> {
  final SocketService _socketService = singletonInjector<SocketService>();
  final DataShareController _shareController = DataShareController();
  final StreamController<String> _connectResponseOnReceiveStreamController = StreamController<String>.broadcast();

  @override
  void initState() {
    super.initState();
    checkSocketMessages();
  }

  /// Checks socket connection for [connectWait] signal from other party through socket.
  void checkSocketMessages() async {
    _socketService.subscribe(
      'connectWait',
      _connectResponseOnReceiveStreamController,
    );

    _connectResponseOnReceiveStreamController.stream.listen((data) async {
      Map message = json.decode(data);
      bool status = await _shareController.validateSessionId(
        message['sessionId'],
      );
      if (status) {
        // Remove subscription from the socket service.
        await _socketService.unsubscribe('connectWait');
        if (!mounted) return;
        Navigator.popAndPushNamed(
          context,
          '/stewardship/awaiting',
          arguments: {
            'sharedAttributesIds': widget.sharedAttributesIds,
            'dataAccessType': widget.dataAccessType,
          },
        );
      }
    });
  }

  @override
  void dispose() {
    _socketService.unsubscribe('connectWait');
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    double width = MediaQuery.of(context).size.width;

    return UserFeaturePageContentsTemplate(
      miniTitle: I18n.of(context).stewardshipBottomShtMiniTitle,
      topImage: Image(
        image: const AssetImage(
          'assets/images/qr_code_with_boarders_icon.png',
        ),
        height: width * 0.18,
      ),
      para1TextPart1: I18n.of(context).stewardshipQrCodeBottomShtParaText,
      para1TextAlign: TextAlign.center,
      middleImage: Align(
        alignment: Alignment.center,
        child: widget.qrCodeLink != null
            ? PrettyQr(
                image: const AssetImage('assets/images/schluss_logo_pink_large.png'),
                // elementColor: Colors.red,
                // typeNumber: 3,
                size: width * 0.75,
                data: widget.qrCodeLink!,
                errorCorrectLevel: QrErrorCorrectLevel.M,
                roundEdges: true,
              )
            : const Text(''),
      ),
    );
  }
}
