import 'package:flutter/material.dart';
import 'package:schluss_beta_app/constants/ui_constants.dart';
import 'package:schluss_beta_app/controller/account_controller.dart';
import 'package:schluss_beta_app/controller/data_share_controller.dart';
import 'package:schluss_beta_app/controller/my_data_controller.dart';
import 'package:schluss_beta_app/translations/i18n.dart';
import 'package:schluss_beta_app/ui/reused_widgets/user_feature_page_contents_template.dart';
import 'package:schluss_beta_app/view_model/gateway_viewmodel.dart';

class StewardAgreementPageContent extends StatefulWidget {
  final GatewayViewModel? gatewayViewModel;

  const StewardAgreementPageContent({
    required this.gatewayViewModel,
    Key? key,
  }) : super(key: key);

  @override
  StewardAgreementPageContentState createState() => StewardAgreementPageContentState();
}

class StewardAgreementPageContentState extends State<StewardAgreementPageContent> {
  final DataShareController _shareController = DataShareController();
  final MyDataController _myDataController = MyDataController();

  bool hasIdentity = false;

  @override
  void initState() {
    // Try to get the stored identity, if no stored yet, initialize getting the identity
    loadIdentityAdditionGuidelineSheet();

    super.initState();
  }

  void loadIdentityAdditionGuidelineSheet() async {
    String firstName = await _myDataController.getPersonalInfoValue(
      'MijnOverheid',
      'FirstNames',
    );

    String surname = await _myDataController.getPersonalInfoValue(
      'MijnOverheid',
      'Surname',
    );

    if (firstName.isEmpty || surname.isEmpty) {
      // Create an 'Add your identity tooltip on the dashboard'
      AccountController().createIdentityTooltip();
      if (!mounted) return;
      Navigator.popAndPushNamed(
        context,
        '/stewardship/identity-addition-guideline',
      );
    }
  }

  @override
  Widget build(BuildContext context) {
    double width = MediaQuery.of(context).size.width;

    return UserFeaturePageContentsTemplate(
      miniTitle: I18n.of(context).stewardshipBottomShtMiniTitle,
      title: I18n.of(context).stewardshipStewardAgreementBottomShtTitle(
        userForenameText: widget.gatewayViewModel!.firstName!,
        userSurnameText: widget.gatewayViewModel!.lastName!,
      ),
      para1TextPart1: I18n.of(
        context,
      ).stewardshipStewardAgreementBottomShtParaTxt,
      cardHeaderText: '${widget.gatewayViewModel!.firstName!} ${widget.gatewayViewModel!.lastName!}',
      cardContent: Row(
        children: [
          Padding(
            padding: const EdgeInsets.only(right: 8.0),
            child: Image(
              image: const AssetImage('assets/images/green_check_icon.png'),
              height: width * 0.03,
            ),
          ),
          Expanded(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Text(
                  I18n.of(context).stewardshipStewardBottomShtCardTxt,
                  style: TextStyle(
                    color: UIConstants.gray100,
                    fontSize: width * 0.045,
                  ),
                ),
              ],
            ),
          ),
        ],
      ),
      outlineBtnText: I18n.of(context).btnCancelTxt,
      outlineBtnCall: () => Navigator.pop(context),
      activeBtnText: I18n.of(context).btnAgreeTxt,
      activeBtnCall: () => acceptConnection(),
      activeBtnDisabledOnClick: true,
    );
  }

  /// Accepts the data connection request.
  void acceptConnection() async {
    bool status = await _shareController.acceptingDataShareRequest(
      widget.gatewayViewModel,
    );
    if (status) {
      if (!mounted) return;
      Navigator.popAndPushNamed(context, '/stewardship/awaiting');
    }
  }
}
