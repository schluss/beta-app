import 'dart:async';
import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:schluss_beta_app/controller/data_share_controller.dart';
import 'package:schluss_beta_app/services/interface/socket_service.dart';
import 'package:schluss_beta_app/singleton_injector.dart';
import 'package:schluss_beta_app/ui/dashboard/stewardship/owner_define_access_timing_page_content.dart';
import 'package:schluss_beta_app/ui/reused_widgets/waiting_page.dart';
import 'package:schluss_beta_app/view_model/gateway_viewmodel.dart';

class WaitingPageContent extends StatefulWidget {
  final List<int>? sharedAttributesIds;
  final DataAccessType? dataAccessType;

  const WaitingPageContent({
    this.sharedAttributesIds,
    this.dataAccessType,
    Key? key,
  }) : super(key: key);

  @override
  WaitingPageContentState createState() => WaitingPageContentState();
}

class WaitingPageContentState extends State<WaitingPageContent> {
  final SocketService _socketService = singletonInjector<SocketService>();
  final StreamController<String> _connectWaitOnReceiveStreamController = StreamController<String>();
  final StreamController<String> _connectAcceptOnReceiveStreamController = StreamController<String>();
  final DataShareController _shareController = DataShareController();

  @override
  void initState() {
    super.initState();
    checkSocketMessages();
  }

  /// Checks socket connection signals from other party through socket.
  void checkSocketMessages() async {
    _socketService.subscribe(
      'connectResponse',
      _connectWaitOnReceiveStreamController,
    );
    _socketService.subscribe(
      'connectAccept',
      _connectAcceptOnReceiveStreamController,
    );

    _connectWaitOnReceiveStreamController.stream.listen((data) async {
      GatewayViewModel gatewayViewModel = GatewayViewModel.fromJson(json.decode(data));
      gatewayViewModel.sharedAttributesIds = widget.sharedAttributesIds ?? [];
      gatewayViewModel.dataAccessType = widget.dataAccessType;

      bool status = await _shareController.validateSessionId(gatewayViewModel.sessionId);

      if (status) {
        // Remove subscription from the socket service.
        await _socketService.unsubscribe('connectResponse');
        if (!mounted) return;
        Navigator.popAndPushNamed(
          context,
          '/stewardship/owner/agreement',
          arguments: {'gatewayViewModel': gatewayViewModel},
        );
      }
    });

    _connectAcceptOnReceiveStreamController.stream.listen((data) async {
      GatewayViewModel? gatewayViewModel = GatewayViewModel.fromJson(json.decode(data));
      gatewayViewModel = await _shareController.processDataAcceptResponse(gatewayViewModel);

      if (gatewayViewModel != null) {
        // TODO: build a business logic to check whether the owner cancels the
        // connection or not on task: [https://schluss.atlassian.net/browse/BETA-2892].

        // Remove subscription from the socket service.
        await _socketService.unsubscribe('connectAccept');

        if (!mounted) return;

        Navigator.popAndPushNamed(
          context,
          '/stewardship/steward/received',
          arguments: {
            'senderForename': gatewayViewModel.firstName,
            'senderSurname': gatewayViewModel.lastName,
          },
        );
      }
    });
  }

  @override
  void dispose() {
    _socketService.unsubscribe('connectResponse');
    _socketService.unsubscribe('connectAccept');

    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return const WaitingPage();
  }
}
