import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:schluss_beta_app/translations/i18n.dart';
import 'package:schluss_beta_app/ui/reused_widgets/user_feature_page_contents_template.dart';

class CancellationPageContent extends StatefulWidget {
  final String? senderSurname;

  const CancellationPageContent({
    required this.senderSurname,
    Key? key,
  }) : super(key: key);

  @override
  CancellationPageContentState createState() => CancellationPageContentState();
}

class CancellationPageContentState extends State<CancellationPageContent> {
  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    double width = MediaQuery.of(context).size.width;

    return UserFeaturePageContentsTemplate(
      miniTitle: I18n.of(context).stewardshipBottomShtMiniTitle,
      isCentered: true,
      topImage: Image(
        image: const AssetImage('assets/images/error_icon.png'),
        height: width * 0.18,
      ),
      titleAlign: Alignment.center,
      title: I18n.of(context).stewardshipCancelBottomShtTitle,
      titleTextAlign: TextAlign.center,
      para1TextPart1: I18n.of(context).stewardshipCancelBottomShtDescTxt(
        userSurnameText: widget.senderSurname!,
      ),
      para1TextAlign: TextAlign.center,
      activeBtnText: I18n.of(
        context,
      ).stewardshipStewardAccessProhibitedBottomMainBtnTxt,
      activeBtnCall: () => Navigator.pop(context),
    );
  }
}
