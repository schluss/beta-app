import 'package:flutter/material.dart';
import 'package:schluss_beta_app/business/impl/notification_bl_impl.dart';
import 'package:schluss_beta_app/constants/ui_constants.dart';
import 'package:schluss_beta_app/singleton_injector.dart';
import 'package:schluss_beta_app/translations/i18n.dart';
import 'package:schluss_beta_app/ui/reused_widgets/user_feature_page_contents_template.dart';

class OwnershipSentPageContent extends StatefulWidget {
  final String? receiverForename;
  final String? receiverSurname;

  const OwnershipSentPageContent({
    required this.receiverForename,
    required this.receiverSurname,
    Key? key,
  }) : super(key: key);

  @override
  OwnershipSentPageContentState createState() => OwnershipSentPageContentState();
}

class OwnershipSentPageContentState extends State<OwnershipSentPageContent> {
  @override
  void initState() {
    super.initState();

    // Remove the stewardship tip from dashboard.
    singletonInjector<NotificationBLImpl>().deleteNotificationsByKey(
      'tipStewardshipTitleTxt',
    );
  }

  @override
  Widget build(BuildContext context) {
    double width = MediaQuery.of(context).size.width;

    return UserFeaturePageContentsTemplate(
      miniTitle: I18n.of(context).stewardshipBottomShtMiniTitle,
      topImage: Image(
        image: const AssetImage('assets/images/intro_4.png'),
        width: width * 0.5,
      ),
      titleAlign: Alignment.center,
      title: I18n.of(context).stewardshipSuccessBottomShtTitle,
      titleTextAlign: TextAlign.center,
      para1TextPart1: I18n.of(context).stewardshipPassedBottomShtDescTxt,
      para1TextAlign: TextAlign.center,
      cardHeaderText: '${widget.receiverForename!} ${widget.receiverSurname!}',
      cardContent: Row(
        children: [
          Padding(
            padding: const EdgeInsets.only(right: 8.0),
            child: Image(
              image: const AssetImage('assets/images/green_check_icon.png'),
              height: width * 0.03,
            ),
          ),
          Expanded(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Text(
                  I18n.of(context).stewardshipOwnerBottomShtCardTxt,
                  //'Selected data',
                  style: TextStyle(
                    color: UIConstants.gray100,
                    fontSize: width * 0.045,
                  ),
                ),
              ],
            ),
          ),
        ],
      ),
      activeBtnText: I18n.of(context).btnDoneTxt,
      activeBtnCall: () => Navigator.pushNamed(
        context,
        '/dashboard',
        arguments: {'activeTab': 0, 'isActualVaultOwner': true},
      ),
      activeBtnDisabledOnClick: true,
    );
  }
}
