import 'package:flutter/material.dart';
import 'package:schluss_beta_app/constants/ui_constants.dart';
import 'package:schluss_beta_app/controller/data_share_controller.dart';
import 'package:schluss_beta_app/translations/i18n.dart';
import 'package:schluss_beta_app/ui/reused_widgets/bottom_up_sheet_template.dart';
import 'package:schluss_beta_app/ui/reused_widgets/main_button.dart';
import 'package:schluss_beta_app/ui/reused_widgets/main_title.dart';

enum DataAccessType { immediately, afterPassed }

class OwnerDefineAccessTimingPageContent extends StatefulWidget {
  const OwnerDefineAccessTimingPageContent({Key? key}) : super(key: key);

  @override
  State<OwnerDefineAccessTimingPageContent> createState() => _OwnerDefineAccessTimingPageContentState();
}

class _OwnerDefineAccessTimingPageContentState extends State<OwnerDefineAccessTimingPageContent> {
  DataAccessType? _accessType;
  bool? _isOption1Selected = false;
  bool? _isOption2Selected = false;

  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    double width = MediaQuery.of(context).size.width;
    double height = MediaQuery.of(context).size.height;

    return BottomUpSheetTemplate(
      heightRatio: 0.0,
      miniTitle: I18n.of(context).stewardshipBottomShtMiniTitle,
      page: Stack(
        children: [
          Column(
            children: [
              Column(
                mainAxisAlignment: MainAxisAlignment.start,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  /// Title.
                  Padding(
                    padding: EdgeInsets.only(
                      top: height * 0.03,
                      bottom: height * 0.04,
                    ),
                    child: Align(
                      alignment: Alignment.topLeft,
                      child: MainTitle(
                        text: I18n.of(context).stewardshipDefineAccessTimingBottomShtTitle,
                        textHeight: 1.25,
                        textAlign: TextAlign.left,
                      ),
                    ),
                  ),

                  /// Radio option 1: immediately.
                  ListTile(
                    contentPadding: const EdgeInsets.all(0),
                    title: Transform.translate(
                      offset: const Offset(-16, 0),
                      child: Text(
                        I18n.of(context).stewardshipDefineAccessTimingBottomShtOption1Txt,
                        style: TextStyle(
                          color: UIConstants.grayDarkest100,
                          fontSize: height * 0.025,
                          fontWeight: FontWeight.w400,
                        ),
                      ),
                    ),
                    leading: GestureDetector(
                      onTap: () => setState(() {
                        _isOption1Selected = true;
                        _isOption2Selected = false;
                        _accessType = DataAccessType.immediately;
                      }),
                      child: _isOption1Selected == true
                          ? Image.asset(
                              'assets/images/radio_btn_selected_icon.png',
                              width: width * 0.08,
                            )
                          : Image.asset(
                              'assets/images/radio_btn_unselected_icon.png',
                              width: width * 0.08,
                            ),
                    ),
                  ),

                  /// Divider line.
                  const Divider(height: 1),

                  /// Radio option 2: after passed.
                  ListTile(
                    contentPadding: const EdgeInsets.all(0),
                    title: Transform.translate(
                      offset: const Offset(-16, 0),
                      child: Text(
                        I18n.of(context).stewardshipDefineAccessTimingBottomShtOption2Txt,
                        style: TextStyle(
                          color: UIConstants.grayDarkest100,
                          fontSize: height * 0.025,
                          fontWeight: FontWeight.w400,
                        ),
                      ),
                    ),
                    leading: GestureDetector(
                      onTap: () => setState(() {
                        _isOption1Selected = false;
                        _isOption2Selected = true;
                        _accessType = DataAccessType.afterPassed;
                      }),
                      child: _isOption2Selected == true
                          ? Image.asset(
                              'assets/images/radio_btn_selected_icon.png',
                              width: width * 0.08,
                            )
                          : Image.asset(
                              'assets/images/radio_btn_unselected_icon.png',
                              width: width * 0.08,
                            ),
                    ),
                  ),
                ],
              ),
            ],
          ),

          /// Main button.
          Padding(
            padding: const EdgeInsets.only(bottom: 0),
            child: Align(
              alignment: Alignment.bottomCenter,
              child: MainButton(
                type: _accessType != null ? BtnType.active : BtnType.disabled,
                isDisabled: true,
                text: I18n.of(context).btnNextTxt,
                call: () => displayDataSelectionFlow(),
              ),
            ),
          ),
        ],
      ),
    );
  }

  void displayDataSelectionFlow() {
    // To open the screens and start the data selection flow.
    Navigator.pushNamed(context, '/data/select/groups', arguments: {
      // 'connectionId': widget.contractId,
      'onCancel': onShareCancel,
      'onShare': onShareFinish,
    });
  }

  /// When the cancel button is clicked on the select data screens.
  void onShareCancel() {
    return;
  }

  /// When the share button is pushed from the select data screens,
  /// this method is fired and the selected attributes are shared.
  Future<bool> onShareFinish(List<int> attributeIds) async {
    if (attributeIds.isEmpty) {
      return false;
    }

    await shareQRInfo(attributeIds);

    return true;
  }

  /// Generates QR link and share the QR data to generate the QR image.
  Future<void> shareQRInfo(List<int> attributeIds) async {
    DataShareController shareController = DataShareController();

    String qrLink = await shareController.generateQRLink('/connectsteward');

    if (!mounted) return;
    await Navigator.popAndPushNamed(
      context,
      '/stewardship/owner/qr-code',
      arguments: {
        'qrData': qrLink,
        'sharedAttributesIds': attributeIds,
        'dataAccessType': _accessType,
      },
    );
  }
}
