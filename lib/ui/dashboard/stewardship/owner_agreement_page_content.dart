import 'package:flutter/material.dart';
import 'package:schluss_beta_app/constants/ui_constants.dart';
import 'package:schluss_beta_app/controller/data_share_controller.dart';
import 'package:schluss_beta_app/translations/i18n.dart';
import 'package:schluss_beta_app/ui/reused_widgets/pin_verification_bottom_up_sheet.dart';
import 'package:schluss_beta_app/ui/reused_widgets/user_feature_page_contents_template.dart';
import 'package:schluss_beta_app/view_model/gateway_viewmodel.dart';

class OwnerAgreementPageContent extends StatefulWidget {
  final GatewayViewModel? gatewayViewModel;

  const OwnerAgreementPageContent({
    required this.gatewayViewModel,
    Key? key,
  }) : super(key: key);

  @override
  OwnerAgreementPageContentState createState() => OwnerAgreementPageContentState();
}

class OwnerAgreementPageContentState extends State<OwnerAgreementPageContent> {
  final DataShareController _shareController = DataShareController();

  @override
  Widget build(BuildContext context) {
    double width = MediaQuery.of(context).size.width;

    return UserFeaturePageContentsTemplate(
      miniTitle: I18n.of(context).stewardshipBottomShtMiniTitle,
      title: I18n.of(context).stewardshipOwnerAgreementBottomShtTitle(
        userForenameText: widget.gatewayViewModel!.firstName!,
        userSurnameText: widget.gatewayViewModel!.lastName!,
      ),
      para1TextPart1: I18n.of(
        context,
      ).stewardshipOwnerAgreementBottomShtParaTxt,
      cardHeaderText: '${widget.gatewayViewModel!.firstName!} ${widget.gatewayViewModel!.lastName!}',
      cardContent: Row(
        children: [
          Padding(
            padding: const EdgeInsets.only(right: 8.0),
            child: Image(
              image: const AssetImage('assets/images/green_check_icon.png'),
              height: width * 0.03,
            ),
          ),
          Expanded(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Text(
                  I18n.of(context).stewardshipOwnerBottomShtCardTxt,
                  // 'Selected data',
                  style: TextStyle(
                    color: UIConstants.gray100,
                    fontSize: width * 0.045,
                  ),
                ),
              ],
            ),
          ),
        ],
      ),
      outlineBtnText: I18n.of(context).btnCancelTxt,
      outlineBtnCall: () => Navigator.pop(context),
      activeBtnText: I18n.of(context).btnAgreeTxt,
      activeBtnCall: () => acceptRequest(),
      activeBtnDisabledOnClick: true,
    );
  }

  /// Accepts the data send request.
  void acceptRequest() async {
    var result = await Navigator.pushNamed(
      context,
      '/account/verify',
      arguments: {'processType': ProcessType.grantDataSharing},
    );

    // Validate pin code verified.
    if (result == true) {
      _shareController.acceptingDataSendRequest(widget.gatewayViewModel);
      if (!mounted) return;
      Navigator.popAndPushNamed(
        context,
        '/stewardship/owner/sent',
        arguments: {
          'receiverForename': widget.gatewayViewModel!.firstName,
          'receiverSurname': widget.gatewayViewModel!.lastName,
        },
      );
    }
  }
}
