import 'package:flutter/material.dart';
import 'package:logger/logger.dart';
import 'package:schluss_beta_app/controller/my_data_controller.dart';
import 'package:schluss_beta_app/singleton_injector.dart';
import 'package:schluss_beta_app/translations/i18n.dart';
import 'package:schluss_beta_app/ui/reused_widgets/confirmation_bottom_up_sheet_contents_template.dart';
import 'package:schluss_beta_app/util/error_handler_util.dart';
import 'package:schluss_beta_app/view_model/my_data_view_model.dart';

class DataDeletionConfirmationBottomUpSheet extends StatefulWidget {
  final MyDataViewModel? dataViewModel;

  const DataDeletionConfirmationBottomUpSheet({
    this.dataViewModel,
    Key? key,
  }) : super(key: key);

  @override
  State<DataDeletionConfirmationBottomUpSheet> createState() => _DataDeletionConfirmationBottomUpSheetState();
}

class _DataDeletionConfirmationBottomUpSheetState extends State<DataDeletionConfirmationBottomUpSheet> {
  final MyDataController _myDataController = MyDataController();
  final Logger _logger = singletonInjector<Logger>();
  final ErrorHandlerUtil _errorHandler = ErrorHandlerUtil();

  @override
  Widget build(BuildContext context) {
    return ConfirmationBottomUpSheetContentsTemplate(
      title: I18n.of(context).dataInfoDelBottomShtTitle,
      mainText: I18n.of(context).dataInfoDelBottomShtTxt,
      activeBtnText: I18n.of(context).dataInfoDelBottomShtBtnTxt,
      activeBtnCall: () => _onDeleteDataConfirmed(),
    );
  }

  void _onDeleteDataConfirmed() async {
    try {
      // Close bottom-up screen.
      Navigator.pop(context);

      bool status = await _myDataController.deleteData(widget.dataViewModel);
      if (!mounted) return;
      if (!status) {
        _logger.e('Delete Failed.');
        _errorHandler.handleError(
          Exception('Delete Failed.'),
          false,
          context,
        );
      } else {
        Navigator.pop(context, true);
      }
    } catch (e) {
      _errorHandler.handleError(e.toString(), false, context);
    }
  }
}
