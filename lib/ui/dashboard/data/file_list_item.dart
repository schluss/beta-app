import 'package:flutter/material.dart';
import 'package:schluss_beta_app/constants/ui_constants.dart';
import 'package:schluss_beta_app/translations/i18n.dart';
import 'package:schluss_beta_app/util/common_util.dart';

class FileListItem extends StatelessWidget {
  final String title;
  final String subTitle;
  final String? mediaType;

  const FileListItem({
    required this.title,
    required this.subTitle,
    this.mediaType,
    Key? key,
  }) : super(key: key);
  @override
  Widget build(BuildContext context) {
    double width = MediaQuery.of(context).size.width;
    double height = MediaQuery.of(context).size.height;

    return Container(
      color: UIConstants.primaryColor,
      child: Padding(
        padding: EdgeInsets.only(
          left: width * 0.075,
          right: width * 0.075,
          top: width * 0.025,
          bottom: width * 0.025,
        ),
        child: Container(
          color: Theme.of(context).primaryColor,
          alignment: Alignment.center,
          height: height * 0.095,
          child: Row(
            children: <Widget>[
              Padding(
                padding: EdgeInsets.only(right: width * 0.04),
                child: Image(
                  image: CommonUtil.isImage(mediaType)
                      ? const AssetImage(
                          'assets/images/media_placeholder_icon.png',
                        )
                      : const AssetImage(
                          'assets/images/document_placeholder_icon.png',
                        ),
                  height: width * 0.13,
                ),
              ),
              Column(
                mainAxisAlignment: MainAxisAlignment.center,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  ConstrainedBox(
                    constraints: BoxConstraints(maxWidth: width * 0.6),
                    child: Text(
                      title,
                      softWrap: true,
                      overflow: TextOverflow.ellipsis,
                      style: TextStyle(
                        color: UIConstants.grayDarkest100,
                        fontWeight: FontWeight.w600,
                        fontStyle: FontStyle.normal,
                        letterSpacing: 0,
                        fontSize: width * 0.045,
                      ),
                    ),
                  ),
                  ConstrainedBox(
                    constraints: BoxConstraints(maxWidth: width * 0.6),
                    child: Text(
                      CommonUtil.isImage(mediaType)
                          ? '${I18n.of(
                              context,
                            ).dataUploadWaitingImgTxt} ${subTitle.toLowerCase()}'
                          : '${I18n.of(
                              context,
                            ).dataUploadOptionBottomShtDocTxt} ${subTitle.toLowerCase()}',
                      maxLines: 1,
                      softWrap: true,
                      overflow: TextOverflow.ellipsis,
                      style: TextStyle(
                        fontFamily: UIConstants.subFontFamily,
                        color: UIConstants.gray100,
                        fontWeight: FontWeight.w400,
                        fontStyle: FontStyle.normal,
                        letterSpacing: 0,
                        fontSize: width * 0.0425,
                      ),
                    ),
                  ),
                ],
              ),
              const Spacer(),
              Icon(
                Icons.keyboard_arrow_right,
                color: UIConstants.gray100,
                size: width * 0.06,
              ),
            ],
          ),
        ),
      ),
    );
  }
}
