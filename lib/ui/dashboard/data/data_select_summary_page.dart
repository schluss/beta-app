import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:schluss_beta_app/constants/ui_constants.dart';
import 'package:schluss_beta_app/controller/contract_controller.dart';
import 'package:schluss_beta_app/providers/impl/my_data_provider.dart';
import 'package:schluss_beta_app/singleton_injector.dart';
import 'package:schluss_beta_app/translations/i18n.dart';
import 'package:schluss_beta_app/ui/reused_widgets/data_list_item.dart';
import 'package:schluss_beta_app/ui/reused_widgets/forward_button.dart';
import 'package:schluss_beta_app/ui/reused_widgets/group_header.dart';
import 'package:schluss_beta_app/ui/reused_widgets/main_button.dart';
import 'package:schluss_beta_app/ui/reused_widgets/mini_header_template.dart';
import 'package:schluss_beta_app/ui/reused_widgets/provider_logo.dart';
import 'package:schluss_beta_app/ui/reused_widgets/providers_list_item.dart';
import 'package:schluss_beta_app/util/error_handler_util.dart';
import 'package:schluss_beta_app/util/initials_picker_util.dart';
import 'package:schluss_beta_app/view_model/contract_detail_view_model.dart';
import 'package:schluss_beta_app/view_model/contract_providers_view_model.dart';
import 'package:schluss_beta_app/view_model/my_data_view_model.dart';

/// Attributes selection summary page, where you see the groups wherein
/// attributes are selected to be shared.
class DataSelectSummaryPage extends StatefulWidget {
  final Map<String, List<int>>? selectedAttributes;
  final Function onCancel;
  final Function onShare;

  final int connectionId;

  const DataSelectSummaryPage({
    this.connectionId = 0,
    required this.onCancel,
    required this.onShare,
    required this.selectedAttributes,
    Key? key,
  }) : super(key: key);

  @override
  DataSelectSummaryPageState createState() => DataSelectSummaryPageState();
}

class DataSelectSummaryPageState extends State<DataSelectSummaryPage> {
  final ErrorHandlerUtil _errorHandler = ErrorHandlerUtil();
  final ContractController _contractController = ContractController();
  final InitialsPickerUtil _initialsPickerUtil = InitialsPickerUtil();
  ContractProvidersViewModel? contractProvidersViewModel;
  final MyDataProvider _myDataProvider = singletonInjector<MyDataProvider>();

  @override
  void setState(VoidCallback fn) {
    if (mounted) {
      super.setState(fn);
    }
  }

  @override
  void initState() {
    super.initState();

    initScreenData();
  }

  void initScreenData() async {
    try {
      // When connectionId given, load the connection.
      if (widget.connectionId != 0) {
        var model = await _contractController.getContractById(
          widget.connectionId,
        );

        setState(() {
          contractProvidersViewModel = model;
        });
      }

      // Get the data.
      _myDataProvider.initSelectedGroups(widget.selectedAttributes!);
    } catch (e) {
      _errorHandler.handleError(e.toString(), false, context);
    }
  }

  @override
  Widget build(BuildContext context) {
    double width = MediaQuery.of(context).size.width;
    double height = MediaQuery.of(context).size.height;
    double topPadding = MediaQuery.of(context).padding.top;

    return Scaffold(
      backgroundColor: UIConstants.primaryColor,
      body: Stack(
        children: <Widget>[
          SingleChildScrollView(
            child: ChangeNotifierProvider<MyDataProvider?>(
              create: (context) => _myDataProvider,
              child: Consumer<MyDataProvider>(builder: (
                context,
                model,
                child,
              ) {
                return Container(
                  height: height,
                  width: width,
                  color: UIConstants.primaryColor,
                  child: ListView(
                    padding: EdgeInsets.only(
                      top: height * 0.06 + topPadding,
                      bottom: height * 0.2,
                    ),
                    children: <Widget>[
                      contractProvidersViewModel != null ? buildTopContainer(height, width, 0) : const Text(''),

                      SizedBox(height: height * 0.012),

                      // Pass in the method below a [DataDetailViewModel] instance.
                      buildTabBody(model.dataMap),
                    ],
                  ),
                );
              }),
            ),
          ),

          /// Mini title with back button.
          MiniHeaderTemplate(
            text: I18n.of(context).dataSelectSummaryTxt,
            hasBackBtn: true,
            backBtnText: I18n.of(context).btnBackTxt,
            forwardBtn: ForwardButton(
              text: I18n.of(context).btnCancelTxt,
              call: () {
                // Close the open windows first.
                Navigator.of(context).pop();
                Navigator.of(context).pop();

                widget.onCancel();
              },
            ),
          ),

          Padding(
            padding: EdgeInsets.only(
              right: width * 0.075,
              left: width * 0.075,
              bottom: height / 18,
            ),
            child: Align(
              alignment: Alignment.bottomCenter,
              child: MainButton(
                type: BtnType.active,
                isDisabled: true,
                text: I18n.of(context).dataSelectFinishBtnTxt,
                call: () async {
                  // Make it a list of id's only.
                  List<int> attributeIds = await _myDataProvider.getSelectedAttributeIds(
                    widget.selectedAttributes!,
                  );

                  // Send to the callback function
                  // IF the callback function returns a Future<bool> and its result = true, then these pages will not be popped
                  // IF the callback function returns a void / Future<void>, then these page will be popped
                  var result = await widget.onShare(attributeIds);
                  if (!mounted) return;
                  if (result == null) {
                    // Close the open windows
                    Navigator.of(context).pop();
                    Navigator.of(context).pop();
                  }
                },
              ),
            ),
          )
        ],
      ),
    );
  }

  Container buildTopContainer(double height, double width, double topPadding) {
    if (contractProvidersViewModel == null) {
      return Container();
    }

    var requesterName = contractProvidersViewModel!.requesterName ?? '';
    var logoColor = contractProvidersViewModel!.iconColor != null ? Color(int.parse(contractProvidersViewModel!.iconColor!)) : null;
    var logoLetters = _initialsPickerUtil.getInitials(requesterName).toString();

    return Container(
      margin: EdgeInsets.only(top: topPadding),
      padding: EdgeInsets.symmetric(
        horizontal: width * 0.05,
        vertical: width * 0.05,
      ),
      color: UIConstants.primaryColor,
      child: Column(
        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
        children: <Widget>[
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: <Widget>[
              /// Logo.
              Expanded(
                flex: 2,
                child: ProviderLogo(
                  isOrg: false,
                  logoColor: logoColor,
                  logoLetters: logoLetters,
                ),
              ),

              /// Title.
              Expanded(
                flex: 7,
                child: Text(
                  requesterName,
                  maxLines: 1,
                  textAlign: TextAlign.start,
                  overflow: TextOverflow.ellipsis,
                  softWrap: true,
                  style: TextStyle(
                    fontWeight: FontWeight.w700,
                    color: UIConstants.grayDarkest100,
                    fontSize: width * 0.07,
                  ),
                ),
              ),
            ],
          ),
        ],
      ),
    );
  }

  Widget buildTabBody(Map<String?, List<MyDataViewModel>?> recentMap) {
    List<Widget> children = [];

    children.add(GroupHeader(text: I18n.of(context).dataSelectSelectedDataTxt));

    // Loop through the headers.
    for (var row in recentMap.entries) {
      // Loop throng the items.
      for (var item in row.value!) {
        children.add(Material(
          color: UIConstants.primaryColor,
          child: Row(
            children: [
              Expanded(
                flex: 9,
                child: InkWell(
                  splashColor: UIConstants.blackFaded,
                  onTap: () => Navigator.pushNamed(
                    context,
                    '/data/select/summary/attributes',
                    arguments: {
                      'connectionId': widget.connectionId,
                      'provider': item.providerName,
                      'group': item.category,
                      'logoUrl': item.logoUrl,
                      'selectedAttributes': widget.selectedAttributes,
                    },
                  ),
                  child: ProvidersListItem(
                    title: item.category!,
                    subTitle: item.providerName!,
                    logo: item.logoUrl!,
                  ),
                ),
              ),
            ],
          ),
        ));
      }
    }

    return Column(children: children);
  }
}

/// Builds to show list of attributes values.
List<Widget> buildAttributes(
  ContractDetailViewModel model,
  double width,
  double height,
  BuildContext context,
) {
  List<Widget> children;
  children = <Widget>[];

  if (model.dataMap != null) {
    // Parse map without groups.
    if (model.dataMap!.keys.length == 1 && model.dataMap!.keys.first!.isEmpty) {
      children.add(const GroupHeader(text: ''));
      for (var list in model.dataMap!.values) {
        {
          for (var element in list) {
            children.add(DataListItem(
              model: element,
              isDecryptionNeeded: true,
            ));
            children.add(const Divider(height: 1));
          }
        }
      }
    } else {
      // Parse map with groups.
      model.dataMap!.forEach((key, value) {
        children.add(GroupHeader(text: key!.toUpperCase()));

        for (var element in value) {
          children.add(DataListItem(
            model: element,
            isDecryptionNeeded: true,
          ));
        }
      });
    }
  }
  return children;
}
