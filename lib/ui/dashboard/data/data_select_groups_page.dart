import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:schluss_beta_app/constants/ui_constants.dart';
import 'package:schluss_beta_app/providers/impl/my_data_provider.dart';
import 'package:schluss_beta_app/singleton_injector.dart';
import 'package:schluss_beta_app/translations/i18n.dart';
import 'package:schluss_beta_app/ui/reused_widgets/forward_button.dart';
import 'package:schluss_beta_app/ui/reused_widgets/mini_header_template.dart';
import 'package:schluss_beta_app/ui/reused_widgets/providers_list_item.dart';
import 'package:schluss_beta_app/util/error_handler_util.dart';
import 'package:schluss_beta_app/view_model/contract_providers_view_model.dart';

/// Allows to select all attribute or go to more detailed selection.
///
/// Attributes sharing starting page, where the user is shown the groups of data.
/// Allows to select all attributes inside a group at once or go into the group
/// for a more detailed selection.
class DataSelectGroupsPage extends StatefulWidget {
  final Map<String, List<int>>? selectedAttributes;

  /// Methods that's called when attribute selection is canceled.
  final Function onCancel;

  /// Methods that's called after attribute selection is done. A [List<int>]
  /// attributeIds is provided with the method so you're able to process them.
  final Function onShare;

  /// Optional: when set, the UI shows which whom you're going to share the attributes
  /// and also checks if some attributes are already shared to prevent double.
  final int connectionId;

  const DataSelectGroupsPage({
    this.connectionId = 0,
    required this.onCancel,
    required this.onShare,
    this.selectedAttributes,
    Key? key,
  }) : super(key: key);

  @override
  DataSelectGroupsPageState createState() => DataSelectGroupsPageState();
}

class DataSelectGroupsPageState extends State<DataSelectGroupsPage> with SingleTickerProviderStateMixin {
  ContractProvidersViewModel? contractProvidersViewModel = ContractProvidersViewModel();
  List<int> alreadySharedAttributes = [];
  Map<String, List<int>>? selectedAttributes;
  final ErrorHandlerUtil _errorHandler = ErrorHandlerUtil();
  final MyDataProvider _myDataProvider = singletonInjector<MyDataProvider>();
  var count = 0;
  bool hasSelectedItems = false;

  @override
  void setState(VoidCallback fn) {
    if (mounted) {
      super.setState(fn);

      updateCompleteButtonVisibility();
    }
  }

  @override
  void initState() {
    super.initState();

    selectedAttributes = widget.selectedAttributes ?? {};

    initScreenData();

    updateCompleteButtonVisibility();
  }

  /// Initializes data elements in screen.
  void initScreenData() async {
    try {
      // If there is an active connection, first check if there are also already
      // shared attributes so we can grey them out.
      if (widget.connectionId != 0) {
        alreadySharedAttributes = await _myDataProvider.getAlreadySharedAttributeIds(
          widget.connectionId,
        );
      }

      _myDataProvider.initRecentOverviewScreen();
    } catch (e) {
      _errorHandler.handleError(e.toString(), false, context);
    }
  }

  @override
  Widget build(BuildContext context) {
    double width = MediaQuery.of(context).size.width;
    double height = MediaQuery.of(context).size.height;
    double topPadding = MediaQuery.of(context).padding.top;

    return Scaffold(
      body: Stack(
        children: <Widget>[
          Container(
            color: UIConstants.paleGray,
            // TODO: Remove ListView builder
            child: ListView.builder(
              physics: const NeverScrollableScrollPhysics(),
              padding: EdgeInsets.only(top: height * 0.06),
              itemCount: count + 2,
              itemBuilder: (BuildContext context, int index) {
                if (index == 0) {
                  return Container(
                    padding: EdgeInsets.only(top: topPadding),
                  );
                }
                return _buildDefaultTabController(height, width);
              },
            ),
          ),

          /// Mini header with add button, options button and back button.
          MiniHeaderTemplate(
            text: I18n.of(context).dataSelectTitle,
            hasBackBtn: true,
            backBtnText: I18n.of(context).btnCancelTxt,
            forwardBtn: hasSelectedItems
                ? ForwardButton(
                    text: I18n.of(context).dataSelectDoneBtnTxt,
                    color: UIConstants.accentColor,
                    call: () => Navigator.pushNamed(
                      context,
                      '/data/select/summary',
                      arguments: {
                        'connectionId': widget.connectionId,
                        'onCancel': widget.onCancel,
                        'onShare': widget.onShare,
                        'selectedAttributes': selectedAttributes,
                      },
                    ),
                  )
                : null,
          ),
        ],
      ),
    );
  }

  /// Builds screen body tab area.
  Widget _buildDefaultTabController(double height, double width) {
    return SizedBox(
      height: height,
      child: Column(
        children: [
          Expanded(
            child: ChangeNotifierProvider<MyDataProvider?>(
              create: (context) => _myDataProvider,
              child: Consumer<MyDataProvider>(
                builder: (context, model, child) {
                  return Container(
                    color: UIConstants.primaryColor,
                    child: Column(
                      children: [
                        // Tab body view.
                        Expanded(child: buildTabBody(model, width)),
                      ],
                    ),
                  );
                },
              ),
            ),
          ),
        ],
      ),
    );
  }

  Widget buildTabBody(MyDataProvider model, double width) {
    List<Widget> children = [];

    // Loop through the headers.
    for (var row in model.dataMap.entries) {
      // Loop through the items.
      for (var item in row.value!) {
        children.add(Material(
          color: UIConstants.primaryColor,
          child: Row(
            children: [
              Expanded(
                flex: 2,
                child: Padding(
                  padding: const EdgeInsets.only(
                    left: 0,
                    right: 0,
                  ),
                  child: Theme(
                    data: ThemeData(
                      unselectedWidgetColor: UIConstants.slateGray40,
                    ),
                    child: Checkbox(
                      checkColor: UIConstants.primaryColor,
                      activeColor: UIConstants.accentColor,
                      shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(4),
                      ),
                      tristate: true,
                      value: _myDataProvider.isGroupSelected(
                        selectedAttributes!,
                        item.category!,
                        item.dataList!.map((e) => e.attributeId!).toList(),
                      ),
                      onChanged: (bool? value) {
                        setState(() {
                          List<int> attributes = item.dataList!
                              .map(
                                (e) => e.attributeId!,
                              )
                              .toList();

                          _myDataProvider.updateGroupSelection(
                            selectedAttributes!,
                            value,
                            item.category!,
                            attributes,
                            alreadySharedAttributes,
                          );

                          // Wether to show or hide the finish button.
                          updateCompleteButtonVisibility();
                        });
                      },
                    ),
                  ),
                ),
              ),
              Expanded(
                flex: 9,
                child: InkWell(
                  splashColor: UIConstants.blackFaded,
                  onTap: () {
                    Navigator.pushNamed(
                      context,
                      '/data/select/attributes',
                      arguments: {
                        'connectionId': widget.connectionId,
                        'onCancel': widget.onCancel,
                        'onShare': widget.onShare,
                        'provider': item.providerName,
                        'group': item.category,
                        'logoUrl': item.logoUrl,
                        'selectedAttributes': selectedAttributes,
                      },
                    ).then((value) => {
                          // And when you come back, make sure we update
                          // the checkboxes state, including button visibility.
                          setState(() {})
                        });
                  },
                  child: ProvidersListItem(
                    title: item.category!,
                    subTitle: item.providerName!,
                    logo: item.logoUrl ?? '',
                    hasLeftPadding: false,
                  ),
                ),
              ),
            ],
          ),
        ));
      }
    }

    return Column(children: children);
  }

  void updateCompleteButtonVisibility() {
    hasSelectedItems = (selectedAttributes != null && selectedAttributes!.isNotEmpty);
  }
}
