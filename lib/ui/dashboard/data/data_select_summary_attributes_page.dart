import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:schluss_beta_app/constants/ui_constants.dart';
import 'package:schluss_beta_app/providers/impl/my_data_provider.dart';
import 'package:schluss_beta_app/singleton_injector.dart';
import 'package:schluss_beta_app/translations/i18n.dart';
import 'package:schluss_beta_app/ui/reused_widgets/data_list_item.dart';
import 'package:schluss_beta_app/ui/reused_widgets/group_header.dart';
import 'package:schluss_beta_app/ui/reused_widgets/mini_header_template.dart';
import 'package:schluss_beta_app/ui/reused_widgets/provider_logo.dart';
import 'package:schluss_beta_app/ui/reused_widgets/provider_retrieval_info.dart';
import 'package:schluss_beta_app/util/error_handler_util.dart';

/// Attributes selection summary page, where you see the attributes selected
/// to be shared inside the chosen group.
class DataSelectSummaryAttributesPage extends StatefulWidget {
  final Map<String, List<int>>? selectedAttributes;
  final String? group;
  final String? provider;
  final String? logoUrl;

  final int connectionId;

  const DataSelectSummaryAttributesPage({
    this.connectionId = 0,
    this.provider,
    this.group,
    this.logoUrl,
    this.selectedAttributes,
    Key? key,
  }) : super(key: key);

  @override
  DataSelectSummaryAttributesPageState createState() => DataSelectSummaryAttributesPageState();
}

class DataSelectSummaryAttributesPageState extends State<DataSelectSummaryAttributesPage> {
  Map<String, List<int>>? selectedAttributes;
  final ErrorHandlerUtil _errorHandler = ErrorHandlerUtil();
  final MyDataProvider _myDataProvider = singletonInjector<MyDataProvider>();

  @override
  void setState(VoidCallback fn) {
    if (mounted) {
      super.setState(fn);
    }
  }

  @override
  void initState() {
    super.initState();

    selectedAttributes = widget.selectedAttributes;
    initScreenData();
  }

  void initScreenData() async {
    try {
      _myDataProvider.initDataByProviderAndGroup(
        widget.provider!,
        widget.group!,
      );
    } catch (e) {
      _errorHandler.handleError(e.toString(), false, context);
    }
  }

  @override
  Widget build(BuildContext context) {
    double width = MediaQuery.of(context).size.width;
    double height = MediaQuery.of(context).size.height;
    double topPadding = MediaQuery.of(context).padding.top;

    return Scaffold(
      backgroundColor: UIConstants.primaryColor,
      body: ChangeNotifierProvider<MyDataProvider?>(
        create: (context) => _myDataProvider,
        child: Consumer<MyDataProvider>(builder: (context, model, child) {
          return Stack(
            children: <Widget>[
              SingleChildScrollView(
                child: Container(
                  height: height,
                  width: width,
                  color: UIConstants.primaryColor,
                  child: ListView(
                    padding: EdgeInsets.only(
                      top: height * 0.06 + topPadding,
                      bottom: height * 0.2,
                    ),
                    children: <Widget>[
                      buildTopContainer(width, height, context),
                      ...buildAttributes(
                        width,
                        height,
                        context,
                      ),
                    ],
                  ),
                ),
              ),

              /// Mini title with back button.
              MiniHeaderTemplate(
                text: '',
                hasBackBtn: true,
                backBtnText: I18n.of(context).btnBackTxt,
              ),
            ],
          );
        }),
      ),
    );
  }

  /// Builds top container section.
  Container buildTopContainer(
    double width,
    double height,
    BuildContext context,
  ) {
    return Container(
      height: height * 0.225,
      color: UIConstants.primaryColor,
      child: ConstrainedBox(
        constraints: const BoxConstraints(maxHeight: 180.0),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
          children: <Widget>[
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: <Widget>[
                Expanded(
                  child: Padding(
                    padding: EdgeInsets.only(
                      left: width * 0.075,
                      right: width * 0.01,
                    ),
                    child: ConstrainedBox(
                      constraints: BoxConstraints(maxWidth: width * 0.7),
                      child: Text(
                        widget.group!,
                        maxLines: 2,
                        textAlign: TextAlign.start,
                        overflow: TextOverflow.ellipsis,
                        softWrap: true,
                        style: TextStyle(
                          height: 1.2,
                          fontWeight: FontWeight.w700,
                          color: UIConstants.grayDarkest100,
                          fontSize: width * 0.07,
                        ),
                      ),
                    ),
                  ),
                ),
                ProviderLogo(logoImg: widget.logoUrl!)
              ],
            ),
            ProviderRetrievalInfo(
              title: I18n.of(context).providerInfoSrcTxt,
              subTitle: widget.provider!,
              heightScale: 0.06,
              isSubTitleBold: true,
            ),
          ],
        ),
      ),
    );
  }

  /// Builds to show list of attributes values.
  List<Widget> buildAttributes(
    double width,
    double height,
    BuildContext context,
  ) {
    List<Widget> children = [];

    children.add(GroupHeader(text: widget.group!));

    for (var attribute in _myDataProvider.attributes) {
      // When the attribute is not selected, don't show.
      if (!selectedAttributes![widget.group]!.contains(
        attribute.attributeId,
      )) {
        continue;
      }

      children.add(Material(
        color: UIConstants.primaryColor,
        child: Row(
          children: [
            Expanded(
              flex: 9,
              child: InkWell(
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.start,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    DataListItem(
                      model: attribute,
                      isDecryptionNeeded: true,
                    ),
                    const Divider(height: 1),
                  ],
                ),
              ),
            ),
          ],
        ),
      ));
    }

    return children;
  }
}
