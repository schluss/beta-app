import 'package:file_picker/file_picker.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_dotenv/flutter_dotenv.dart';
import 'package:logger/logger.dart';
import 'package:schluss_beta_app/constants/ui_constants.dart';
import 'package:schluss_beta_app/controller/account_controller.dart';
import 'package:schluss_beta_app/singleton_injector.dart';
import 'package:schluss_beta_app/translations/i18n.dart';
import 'package:schluss_beta_app/ui/dashboard/data/data_upload_waiting_page.dart';
import 'package:schluss_beta_app/ui/reused_widgets/bottom_up_action_sheet.dart';
import 'package:schluss_beta_app/ui/reused_widgets/bottom_up_sheet_template.dart';
import 'package:schluss_beta_app/ui/reused_widgets/provider_retrieval_info.dart';
import 'package:schluss_beta_app/ui/reused_widgets/providers_list_item.dart';
import 'package:schluss_beta_app/util/error_handler_util.dart';
import 'package:schluss_beta_app/view_model/settings_view_model.dart';

class DataUploadingBottomUpSheet extends StatefulWidget {
  const DataUploadingBottomUpSheet({Key? key}) : super(key: key);

  @override
  DataUploadingBottomUpSheetState createState() => DataUploadingBottomUpSheetState();
}

class DataUploadingBottomUpSheetState extends State<DataUploadingBottomUpSheet> {
  final AccountController _accountController = AccountController();

  String? belastingdienstLogoUrl = dotenv.env['BELASTINGDIENST_LOGO_URL'];
  String? uwvLogoUrl = dotenv.env['UWV_LOGO_URL'];
  String? bkrLogoUrl = dotenv.env['BKR_LOGO_URL'];

  String? meConfigUrl = dotenv.env['ME_CONFIG_URL'];
  String? verifiedMailConfigUrl = dotenv.env['VERIFIEDMAIL_CONFIG_URL'];
  String? belastingdienstConfigUrl = dotenv.env['BELASTINGDIENST_CONFIG_URL'];
  String? uwvConfigUrl = dotenv.env['UWV_CONFIG_URL'];
  String? bkrConfigUrl = dotenv.env['BKR_CONFIG_URL'];

  late SettingsViewModel settings;
  bool _isDataSharingModeOn = false;
  bool _testMode = false;

  @override
  void initState() {
    super.initState();

    setTestModeState();
  }

  /// Sets the state of the test-mode as account settings.
  Future setTestModeState() async {
    // Get account settings values.
    settings = await _accountController.getSettings();

    setState(() {
      _isDataSharingModeOn = settings.dataSharingMode;
      _testMode = settings.testMode;
    });
  }

  @override
  Widget build(BuildContext context) {
    double width = MediaQuery.of(context).size.width;
    double height = MediaQuery.of(context).size.height;

    return BottomUpSheetTemplate(
      page: Stack(
        children: [
          /// Title.
          Padding(
            padding: const EdgeInsets.only(top: 8.0),
            child: Align(
              alignment: Alignment.topCenter,
              child: Text(
                I18n.of(context).dataUploadBottomShtMiniTitle,
                style: TextStyle(
                  color: UIConstants.grayDarkest100,
                  fontSize: width * 0.06,
                  fontWeight: FontWeight.bold,
                ),
              ),
            ),
          ),

          Padding(
            padding: EdgeInsets.only(top: height * 0.09),
            child: Column(
              children: [
                const Divider(height: 1),

                /// Add address
                Material(
                  color: UIConstants.primaryColor,
                  child: InkWell(
                    splashColor: UIConstants.blackFaded,
                    onTap: () => Navigator.pushNamed(context, '/data-request', arguments: {
                      'configUrl': meConfigUrl,
                    }),
                    child: Padding(
                      padding: const EdgeInsets.symmetric(horizontal: 8.0),
                      child: ListTile(
                        title: Text(
                          I18n.of(context).dataUploadBottomShtTile2Header,
                          style: TextStyle(
                            color: UIConstants.grayDarkest100,
                            fontSize: width * 0.0475,
                            fontWeight: FontWeight.w600,
                          ),
                        ),
                        trailing: Icon(
                          Icons.keyboard_arrow_right,
                          color: UIConstants.gray100,
                          size: MediaQuery.of(context).size.width * 0.06,
                        ),
                        leading: const Padding(
                          padding: EdgeInsets.symmetric(
                            horizontal: 10.0,
                            vertical: 8.0,
                          ),
                          child: Image(
                            image: AssetImage(
                              'assets/images/address.png',
                            ),
                            width: 28,
                          ),
                        ),
                      ),
                    ),
                  ),
                ),

                const Divider(height: 1),

                /// File attachment.
                Material(
                  color: UIConstants.primaryColor,
                  child: InkWell(
                    splashColor: UIConstants.blackFaded,
                    /* onTap: () => Navigator.pushNamed(
                      context,
                      'data/upload/options',
                    ), */
                    onTap: () => BottomUpActionSheetLoader.loadActionSheet(
                      context,
                      title: 'Add file',
                      buttonList: [
                        ActionSheetButton(
                          text: I18n.of(context).dataUploadOptionBottomShtMediaTxt,
                          call: () => _openFileExplorer(FileType.media),
                        ),
                        ActionSheetButton(
                          text: I18n.of(context).dataUploadOptionBottomShtDocTxt,
                          call: () => _openFileExplorer(FileType.any),
                        ),
                      ],
                    ),
                    child: Padding(
                      padding: const EdgeInsets.symmetric(horizontal: 8.0),
                      child: ListTile(
                        title: Text(
                          I18n.of(context).dataUploadBottomShtTileHeader,
                          style: TextStyle(
                            color: UIConstants.grayDarkest100,
                            fontSize: width * 0.0475,
                            fontWeight: FontWeight.w600,
                          ),
                        ),
                        trailing: Icon(
                          Icons.keyboard_arrow_right,
                          color: UIConstants.gray100,
                          size: MediaQuery.of(context).size.width * 0.06,
                        ),
                        leading: const Padding(
                          padding: EdgeInsets.symmetric(
                            horizontal: 10.0,
                            vertical: 6.0,
                          ),
                          child: Image(
                            image: AssetImage('assets/images/attach_icon.png'),
                            width: 28,
                          ),
                        ),
                      ),
                    ),
                  ),
                ),

                _testMode ? const Divider(height: 1) : Container(),

                /// Manual verified e-mail addition.
                Material(
                  color: UIConstants.primaryColor,
                  child: InkWell(
                    splashColor: UIConstants.blackFaded,
                    onTap: () => Navigator.pushNamed(context, '/data-request', arguments: {
                      'configUrl': verifiedMailConfigUrl,
                    }),
                    child: Padding(
                      padding: const EdgeInsets.symmetric(horizontal: 8.0),
                      child: ListTile(
                        title: Text(
                          I18n.of(context).dataUploadBottomShtTile3Header,
                          style: TextStyle(
                            color: UIConstants.grayDarkest100,
                            fontSize: width * 0.0475,
                            fontWeight: FontWeight.w600,
                          ),
                        ),
                        trailing: Icon(
                          Icons.keyboard_arrow_right,
                          color: UIConstants.gray100,
                          size: MediaQuery.of(context).size.width * 0.06,
                        ),
                        leading: const Padding(
                          padding: EdgeInsets.symmetric(
                            horizontal: 10.0,
                            vertical: 8.0,
                          ),
                          child: Image(
                            image: AssetImage(
                              'assets/images/mail_icon.png',
                            ),
                            width: 28,
                          ),
                        ),
                      ),
                    ),
                  ),
                ),

                _testMode ? const Divider(height: 1) : Container(),

                /// Pro-active data requests.
                ///
                /// They are store-only no-contracts data requests.
                /// Only shows when both test-mode are data-sharing are on.
                _testMode && _isDataSharingModeOn
                    ? Column(
                        children: [
                          Padding(
                            padding: const EdgeInsets.only(
                              top: 40.0,
                            ),
                            child: ProviderRetrievalInfo(
                              title: I18n.of(context).providerInfoSrcTxt,
                              subTitle: '',
                              heightScale: 0.045,
                              isTitleBold: true,
                              isSubTitleBold: false,
                            ),
                          ),
                          Container(
                              color: Theme.of(context).primaryColor,
                              height: height * 0.36,
                              child: Column(
                                children: [
                                  InkWell(
                                    onTap: () => Navigator.pushNamed(context, '/data-request', arguments: {
                                      'configUrl': belastingdienstConfigUrl,
                                    }),
                                    child: ProvidersListItem(
                                      title: 'Belastingdienst',
                                      subTitle: '',
                                      logo: belastingdienstLogoUrl!,
                                    ),
                                  ),
                                  const Divider(height: 1),
                                  InkWell(
                                    onTap: () => Navigator.pushNamed(context, '/data-request', arguments: {
                                      'configUrl': uwvConfigUrl,
                                    }),
                                    child: ProvidersListItem(
                                      title: 'UWV',
                                      subTitle: '',
                                      logo: uwvLogoUrl!,
                                    ),
                                  ),
                                  const Divider(height: 1),
                                  InkWell(
                                    onTap: () => Navigator.pushNamed(context, '/data-request', arguments: {
                                      'configUrl': bkrConfigUrl,
                                    }),
                                    child: ProvidersListItem(
                                      title: 'BKR',
                                      subTitle: '',
                                      logo: bkrLogoUrl!,
                                    ),
                                  )
                                ],
                              )),
                        ],
                      )
                    : Container(),
              ],
            ),
          ),
        ],
      ),
      heightRatio: 0.10,
    );
  }

  /// Opens file explorer.
  void _openFileExplorer(FileType fileType) async {
    final Logger logger = singletonInjector<Logger>();
    final ErrorHandlerUtil errorHandler = ErrorHandlerUtil();

    List<PlatformFile>? file;

    try {
      file = (await FilePicker.platform.pickFiles(
        withData: false,
        withReadStream: true,
        type: fileType,
        allowMultiple: false,
      ))
          ?.files;
    } on PlatformException catch (e) {
      logger.e('Unsupported operation $e');

      errorHandler.handleError(e.toString(), false, context);
    } catch (ex) {
      logger.e(ex);
    }

    if (!mounted) return;

    if (file != null) {
      await Navigator.pushReplacement(
        context,
        MaterialPageRoute(
          builder: (context) => DataUploadWaitingPage(
            file!.first.path.toString(),
            fileType,
          ),
        ),
      );

      if (!mounted) return;

      Navigator.pushNamed(
        context,
        '/dashboard',
        arguments: {'activeTab': 1, 'isActualVaultOwner': true},
      );
    }
  }
}
