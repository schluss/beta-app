import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:schluss_beta_app/constants/ui_constants.dart';
import 'package:schluss_beta_app/ui/reused_widgets/mini_header_template.dart';
import 'package:schluss_beta_app/view_model/field_view_model.dart';

late BuildContext _ctx;

class DataDetailsListItemPage extends StatefulWidget {
  final FieldViewModel? model;

  const DataDetailsListItemPage({
    Key? key,
    this.model,
  }) : super(key: key);

  @override
  // ignore: no_logic_in_create_state
  DataDetailsListItemPageState createState() => DataDetailsListItemPageState(
        model: model,
      );
}

class DataDetailsListItemPageState extends State<DataDetailsListItemPage> {
  FieldViewModel? model;
  late ScrollController _scrollController;
  List<dynamic>? jsonValue;

  DataDetailsListItemPageState({this.model});

  @override
  void initState() {
    _scrollController = ScrollController();

    // TODO: ideally we only now load and decrypt the value.
    super.initState();
  }

  @override
  void dispose() {
    _scrollController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    _ctx = context;
    double height = MediaQuery.of(context).size.height;

    return Scaffold(
      backgroundColor: UIConstants.primaryColor,
      body: Stack(
        children: [
          SingleChildScrollView(
            padding: EdgeInsets.only(top: height * 0.12),
            child: Column(
              children: [buildExpandable(json.decode(model!.value!))],
            ),
          ),

          /// Mini title with close button.
          MiniHeaderTemplate(
            text: model!.label,
          ),
        ],
      ),
    );
  }
}

/// Builds and returns a nested expandable list from a json map.
Widget buildExpandable(dynamic jsonValue) {
  List<ExpansionTile> tiles;
  tiles = <ExpansionTile>[];

  // When it is a Map
  if (jsonValue is Map) {
    Map tmp = Map.from(jsonValue);
    List<Widget> children = [];
    tmp.forEach((key, value) {
      children.add(buildTextRow(key.toString(), value.toString()));
    });

    return Column(
      children: children,
    );
  }

  // When it is a list
  else if (jsonValue is List) {
    // Loop through the rows.
    for (var element in jsonValue) {
      if (element is Map) {
        Map<String, dynamic> cols;
        List<Widget> children;
        late String title;
        bool isFirst;

        cols = Map.from(element);
        children = [];
        isFirst = true;

        // Loop through the columns.
        cols.forEach((key, value) {
          // Every top level row will become an expandable title.
          if (isFirst) {
            title = value.toString(); // key + ': ' + value;
            isFirst = false;
            // On other rows.
          } else {
            // Check if this is a list.
            if (value is List) {
              List<dynamic> lst;
              lst = List.from(value);

              // Subtle addition: when list has a length of 1 and contains a map.
              // Do not make it as a new expandable list, but just loop through the
              // items to show them as key - value item to be displayed.
              if (lst.length == 1 && lst[0] is Map) {
                Map<dynamic, dynamic> tmpMap;

                tmpMap = Map.from(lst[0]);

                tmpMap.forEach((key, value) {
                  children.add(buildTextRow(key.toString(), value.toString()));
                });
              } else {
                // Call [buildExpandable] recursively because this is a nested expandable list.
                children.add(buildExpandable(value));
              }

              // If this is no list it is a key - value item to be displayed.
            } else {
              children.add(buildTextRow(key.toString(), value.toString()));
            }
          }
        });

        // Finally add all the child and the title to the list of [ExpansionTiles].
        var edgeInsets = const EdgeInsets.fromLTRB(20, 20, 0, 20);

        tiles.add(
          ExpansionTile(
            childrenPadding: edgeInsets,
            title: Text(
              title,
              maxLines: 2,
              style: const TextStyle(
                fontWeight: FontWeight.bold,
              ),
            ),
            children: children,
          ),
        );
      }
    }
  }

  return Column(
    children: tiles,
  );
}

/// Builds and return a key - value item row.
Widget buildTextRow(String key, String value) {
  double width = MediaQuery.of(_ctx).size.width;

  return Container(
    padding: const EdgeInsets.fromLTRB(15, 0, 0, 10),
    child: Row(
      mainAxisAlignment: MainAxisAlignment.start,
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Column(
          children: [
            Text(
              '$key: ',
              style: TextStyle(
                fontSize: width * 0.04,
                fontWeight: FontWeight.w700,
                fontStyle: FontStyle.normal,
              ),
            )
          ],
        ),
        Flexible(
          child: Text(
            value,
            style: TextStyle(
              fontSize: width * 0.04,
              fontStyle: FontStyle.normal,
            ),
            softWrap: true,
            maxLines: 10,
          ),
        )
      ],
    ),
  );
}
