import 'package:file_picker/file_picker.dart';
import 'package:flutter/material.dart';
import 'package:flutter_widget_from_html_core/flutter_widget_from_html_core.dart';
import 'package:schluss_beta_app/constants/ui_constants.dart';
import 'package:schluss_beta_app/controller/my_data_controller.dart';
import 'package:schluss_beta_app/providers/impl/data_request_provider.dart';
import 'package:schluss_beta_app/translations/i18n.dart';
import 'package:schluss_beta_app/ui/reused_widgets/animated_circle_icon.dart';
import 'package:schluss_beta_app/ui/reused_widgets/main_button.dart';
import 'package:schluss_beta_app/ui/reused_widgets/main_title.dart';
import 'package:schluss_beta_app/ui/reused_widgets/mini_header_template.dart';
import 'package:schluss_beta_app/util/error_handler_util.dart';

class DataUploadWaitingPage extends StatefulWidget {
  final String filePath;
  final FileType fileType;

  const DataUploadWaitingPage(
    this.filePath,
    this.fileType, {
    Key? key,
  }) : super(key: key);

  @override
  DataUploadWaitingPageState createState() => DataUploadWaitingPageState();
}

class DataUploadWaitingPageState extends State<DataUploadWaitingPage> with TickerProviderStateMixin {
  final MyDataController _myDataController = MyDataController();
  final ErrorHandlerUtil _errorHandler = ErrorHandlerUtil();

  late AnimationController _controller;
  late Animation<Offset> _offsetAnimation;
  late AnimationController _controllerScale;

  var scaleValue = 0.9;
  var state = RequestState.loading;
  String _fileName = '';

  @override
  void initState() {
    super.initState();

    _fileName = widget.filePath.isNotEmpty ? widget.filePath.split('/').last : '';

    _controllerScale = AnimationController(
      vsync: this,
      lowerBound: 0.9,
      upperBound: 1.0,
      duration: const Duration(milliseconds: 800),
    )..repeat(reverse: true);
    _controllerScale.addListener(() {
      setState(() {
        if (state == RequestState.loading) {
          scaleValue = _controllerScale.value;
        } else {
          scaleValue = 1.0;
        }
      });
    });
    _controller = AnimationController(
      duration: const Duration(milliseconds: 500),
      vsync: this,
    )..repeat(reverse: true);
    _offsetAnimation = Tween<Offset>(
      begin: Offset.zero,
      end: const Offset(0.5, 0.0),
    ).animate(CurvedAnimation(
      parent: _controller,
      curve: Curves.linear,
    ));
  }

  @override
  void didChangeDependencies() {
    super.didChangeDependencies();
    // When the parent widget is not fully built and we call setState method - the error may occur.
    // So, below method should be called after the UI is built not in [initState] method.
    _uploadFile();
  }

  @override
  void dispose() {
    super.dispose();
    _controllerScale.dispose();
    _controller.dispose();
  }

  /// Uploads the files to app.
  void _uploadFile() async {
    late bool status;

    try {
      status = await _myDataController.uploadData(widget.filePath);
    } catch (e) {
      _errorHandler.handleError(e.toString(), false, context);
    }

    setState(() => state = status ? RequestState.success : RequestState.error);
  }

  @override
  Widget build(BuildContext context) {
    double width = MediaQuery.of(context).size.width;
    double height = MediaQuery.of(context).size.height;

    return Scaffold(
      backgroundColor: UIConstants.primaryColor,
      body: Stack(
        children: <Widget>[
          Center(
            child: Stack(
              children: <Widget>[
                Stack(
                  children: <Widget>[
                    Container(
                      alignment: Alignment.centerLeft,
                      child: Image(
                        image: const AssetImage('assets/images/pulse.png'),
                        width: width * 0.53,
                      ),
                    ),
                    Container(
                      alignment: Alignment.centerRight,
                      child: Image(
                        image: const AssetImage('assets/images/pulse.png'),
                        width: width * 0.53,
                      ),
                    ),
                  ],
                ),
                Padding(
                  padding: EdgeInsets.only(
                    left: width * 0.075,
                    right: width * 0.075,
                  ),
                  child: Center(
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                      children: <Widget>[
                        Transform.scale(
                          scale: scaleValue,
                          child: AnimatedCircleIcon(
                            img: Image(
                              image: _loadPlaceholderImg(),
                              width: width * 0.125,
                              height: width * 0.125,
                            ),
                            isError: state == RequestState.error,
                          ),
                        ),
                        SlideTransition(
                          position: _offsetAnimation,
                          child: Container(
                            width: width * 0.085,
                            height: width * 0.085,
                            decoration: const BoxDecoration(
                              shape: BoxShape.circle,
                              color: UIConstants.primaryColor,
                              boxShadow: [
                                BoxShadow(
                                  color: UIConstants.gray50,
                                  offset: Offset(0, 1),
                                  blurRadius: 2,
                                  spreadRadius: 0,
                                ),
                              ],
                            ),
                            child: Align(
                              alignment: Alignment.center,
                              child: Image(
                                image: AssetImage(_getCentralIconPath()),
                                width: state == RequestState.error ? width * 0.035 : width * 0.045,
                              ),
                            ),
                          ),
                        ),
                        AnimatedCircleIcon(
                          img: Image(
                            image: const AssetImage('assets/images/save_box_icon.png'),
                            width: width * 0.125,
                            height: width * 0.125,
                          ),
                          isShadowPink: true,
                          isError: state == RequestState.error,
                        ),
                      ],
                    ),
                  ),
                ),
              ],
            ),
          ),
          Stack(
            children: <Widget>[
              Align(
                alignment: Alignment.topCenter,
                child: Container(
                  padding: EdgeInsets.only(top: height * 0.2),
                  child: Column(
                    children: <Widget>[
                      Padding(
                        padding: EdgeInsets.symmetric(
                          horizontal: width * 0.05,
                        ),
                        child: MainTitle(
                          text: state == RequestState.error
                              ? I18n.of(
                                  context,
                                ).dataReqRetrievalErrorTitle
                              : _fileName,
                          textHeight: 1.2,
                        ),
                      ),
                      Text(
                        widget.fileType == FileType.media
                            ? I18n.of(
                                context,
                              ).dataUploadWaitingImgTxt
                            : I18n.of(context).dataUploadOptionBottomShtMediaTxt,
                        style: TextStyle(
                          color: UIConstants.gray100,
                          fontSize: width * 0.05,
                        ),
                      )
                    ],
                  ),
                ),
              ),
              Align(
                alignment: Alignment.bottomCenter,
                child: Container(
                  padding: EdgeInsets.only(
                    bottom: height * 0.25,
                    left: width * 0.2,
                    right: width * 0.2,
                  ),
                  child: _getBottomText(width),
                ),
              ),
              Align(
                alignment: Alignment.topCenter,
                child: Container(
                  height: height * 0.1,
                  color: UIConstants.paleLilac,
                ),
              ),

              /// Mini title with close button.
              MiniHeaderTemplate(
                text: I18n.of(context).dataReqRetrievalMiniTitle,
              ),
            ],
          ),
          Padding(
            padding: EdgeInsets.only(
              right: width * 0.075,
              left: width * 0.075,
              bottom: height / 18,
            ),
            child: Align(
              alignment: Alignment.bottomCenter,
              child: MainButton(
                type: state == RequestState.loading ? BtnType.awaited : BtnType.active,
                text: _getButtonText(),
                call: () => _generateButtonEvent(),
              ),
            ),
          )
        ],
      ),
    );
  }

  /// Loads placeholder image.
  AssetImage _loadPlaceholderImg() {
    if (widget.fileType == FileType.media) {
      return const AssetImage('assets/images/media_icon.png');
    } else {
      return const AssetImage('assets/images/document_icon.png');
    }
  }

  /// Gets the path of the middle icon.
  String _getCentralIconPath() {
    String result;
    if (state == RequestState.success) {
      result = 'assets/images/green_check_icon.png';
      _stopAnimation();
    } else if (state == RequestState.loading) {
      result = 'assets/images/arrow_icon.png';
    } else {
      result = 'assets/images/grey_cross_icon.png';
      _stopAnimation();
    }
    return result;
  }

  /// Stops animation.
  void _stopAnimation() {
    scaleValue = 1.0;
    _controller.reset();
    _controller.stop();
    _controllerScale.stop();
  }

  /// Loads bottom text.
  Widget _getBottomText(double width) {
    Widget result;
    if (state == RequestState.success) {
      result = buildRichText(width);
    } else if (state == RequestState.loading) {
      result = const Text('');
    } else {
      result = Text(
        I18n.of(context).dataReqRetrievalErrorTxt,
        textAlign: TextAlign.center,
        style: TextStyle(
          color: UIConstants.gray100,
          fontSize: width * 0.045,
        ),
      );
    }
    return result;
  }

  /// Builds rich text.
  HtmlWidget buildRichText(double width) {
    return HtmlWidget(
      widget.fileType == FileType.media
          ? '<center>${I18n.of(
              context,
            ).dataUploadWaitingImgInVaultTxt}</center>'
          : '<center>${I18n.of(
              context,
            ).dataUploadWaitingDocInVaultTxt}</center>',
      onTapUrl: (url) {
        return false;
      },
      textStyle: TextStyle(
        fontSize: width * 0.045,
        color: UIConstants.gray100,
      ),
    );
  }

  /// Loads button text.
  String _getButtonText() {
    String result;
    if (state == RequestState.success) {
      result = I18n.of(context).btnContinueTxt;
    } else if (state == RequestState.loading) {
      result = I18n.of(context).dataReqRetrievalLoadingBtnTxt;
    } else {
      result = I18n.of(context).dataReqRetrievalErrorBtnTxt;
    }
    return result;
  }

  /// On clicks event of go Verde button.
  void _generateButtonEvent() {
    if (state == RequestState.success) {
      Navigator.pop(context, true);
    }
  }
}
