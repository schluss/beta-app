import 'package:flutter/material.dart';
import 'package:schluss_beta_app/constants/helper_constants.dart';
import 'package:schluss_beta_app/constants/ui_constants.dart';
import 'package:schluss_beta_app/translations/i18n.dart';
import 'package:schluss_beta_app/ui/reused_widgets/bottom_up_sheet_template.dart';
import 'package:schluss_beta_app/ui/reused_widgets/main_button.dart';
import 'package:schluss_beta_app/ui/reused_widgets/main_title.dart';
import 'package:schluss_beta_app/ui/reused_widgets/providers_list_item.dart';
import 'package:schluss_beta_app/view_model/contract_recent_item.dart';

class DataDeletionErrorBottomUpSheet extends StatefulWidget {
  final List<ContractItem>? contractList;

  const DataDeletionErrorBottomUpSheet({
    this.contractList,
    Key? key,
  }) : super(key: key);

  @override
  State<DataDeletionErrorBottomUpSheet> createState() => _DataDeletionErrorBottomUpSheetState();
}

class _DataDeletionErrorBottomUpSheetState extends State<DataDeletionErrorBottomUpSheet> {
  @override
  Widget build(BuildContext context) {
    double width = MediaQuery.of(context).size.width;
    double height = MediaQuery.of(context).size.height;

    return BottomUpSheetTemplate(
      page: Column(
        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
        crossAxisAlignment: CrossAxisAlignment.center,
        mainAxisSize: MainAxisSize.max,
        children: [
          /// Warning icon.
          Align(
            alignment: Alignment.centerLeft,
            child: Container(
              decoration: const BoxDecoration(
                shape: BoxShape.circle,
                color: UIConstants.paleGray,
              ),
              padding: EdgeInsets.all(width * 0.05),
              child: Image(
                image: const AssetImage('assets/images/warning_icon.png'),
                height: width * 0.084,
              ),
            ),
          ),

          /// Title.
          Align(
            alignment: Alignment.centerLeft,
            child: MainTitle(
              text: I18n.of(context).dataInfoDelErrBottomShtTitle,
              textHeight: 1.25,
              textAlign: TextAlign.left,
            ),
          ),

          /// Description.
          Text(
            I18n.of(context).dataInfoDelErrBottomShtTxt,
            style: TextStyle(
              color: UIConstants.slateGray100,
              fontSize: width * 0.0475,
            ),
          ),

          /// Contracts list.
          ListView(
            shrinkWrap: true,
            padding: EdgeInsets.only(
              left: 0.00,
              right: 0.00,
              bottom: height / 6,
            ),
            children: <Widget>[...buildChildren(context)],
          ),

          /// Submit button.
          MainButton(
            type: BtnType.active,
            text: I18n.of(context).btnNoGoBackTxt,
            call: () => Navigator.pop(context),
          ),
        ],
      ),
      heightRatio: 0.05,
    );
  }

  List<Widget> buildChildren(context) {
    List<Widget> children = <Widget>[];

    for (ContractItem contract in widget.contractList!) {
      children.add(const Divider(height: 1));
      children.add(
        Material(
          color: UIConstants.primaryColor,
          child: InkWell(
            onTap: () {
              Navigator.pushNamed(
                context,
                '/contracts/contract',
                arguments: {
                  'selectedId': contract.contractId,
                  'fromUI': HelperConstant.fromDataUI,
                },
              );
            },
            child: ProvidersListItem(
              title: contract.providerDataItem.title!,
              subTitle: '${I18n.of(context).viaTxt} ${contract.description!}',
              logo: contract.providerDataItem.imagePath!,
            ),
          ),
        ),
      );
    }

    children.add(const Divider(height: 1));

    return children;
  }
}
