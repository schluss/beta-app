import 'package:file_picker/file_picker.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:logger/logger.dart';
import 'package:schluss_beta_app/singleton_injector.dart';
import 'package:schluss_beta_app/translations/i18n.dart';
import 'package:schluss_beta_app/ui/dashboard/data/data_upload_waiting_page.dart';
import 'package:schluss_beta_app/ui/reused_widgets/bottom_up_sheet_template.dart';
import 'package:schluss_beta_app/ui/reused_widgets/main_button.dart';
import 'package:schluss_beta_app/util/error_handler_util.dart';

class DataUploadingOptionsBottomUpSheet extends StatefulWidget {
  const DataUploadingOptionsBottomUpSheet({Key? key}) : super(key: key);

  @override
  DataUploadingOptionsBottomUpSheetState createState() => DataUploadingOptionsBottomUpSheetState();
}

class DataUploadingOptionsBottomUpSheetState extends State<DataUploadingOptionsBottomUpSheet> {
  final Logger _logger = singletonInjector<Logger>();
  final ErrorHandlerUtil _errorHandler = ErrorHandlerUtil();

  String? _extension;
  List<PlatformFile>? _file;

  @override
  Widget build(BuildContext context) {
    double width = MediaQuery.of(context).size.width;
    double height = MediaQuery.of(context).size.height;

    return BottomUpSheetTemplate(
      page: SizedBox(
        height: height,
        child: Align(
          alignment: Alignment.bottomCenter,
          child: Padding(
            padding: EdgeInsets.all(width * 0.025),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.end,
              children: <Widget>[
                MainButton(
                  type: BtnType.bottomFlatted,
                  text: I18n.of(context).dataUploadOptionBottomShtMediaTxt,
                  call: () => _openFileExplorer(FileType.media), // Provide the correct extensions and file types here.
                ),
                Padding(
                  padding: EdgeInsets.only(bottom: width * 0.025, top: 1),
                  child: MainButton(
                    type: BtnType.topFlatted,
                    text: I18n.of(context).dataUploadOptionBottomShtDocTxt,
                    call: () => _openFileExplorer(FileType.any), // Provide the correct extensions and file types here.
                  ),
                ),
                MainButton(
                  type: BtnType.whitish,
                  text: I18n.of(context).btnCancelTxt,
                  call: () => Navigator.pop(context),
                ),
              ],
            ),
          ),
        ),
      ),
      heightRatio: Theme.of(context).platform == TargetPlatform.iOS ? 0.70 : 0.60,
    );
  }

  /// Opens file explorer.
  void _openFileExplorer(FileType fileType) async {
    try {
      _file = (await FilePicker.platform.pickFiles(
        withData: false,
        withReadStream: true,
        type: fileType,
        allowMultiple: false,
        allowedExtensions: (_extension?.isNotEmpty ?? false)
            ? _extension
                ?.replaceAll(
                  ' ',
                  '',
                )
                .split(',')
            : null,
      ))
          ?.files;
    } on PlatformException catch (e) {
      _logger.e('Unsupported operation ${e.toString()}');

      _errorHandler.handleError(e.toString(), false, context);
    } catch (ex) {
      _logger.e(ex);
    }

    if (_file != null) {
      if (!mounted) return;
      await Navigator.pushReplacement(
        context,
        MaterialPageRoute(
          builder: (context) => DataUploadWaitingPage(
            _file!.first.path.toString(),
            fileType,
          ),
        ),
      );
      if (!mounted) return;
      Navigator.pushNamed(
        context,
        '/dashboard',
        arguments: {'activeTab': 1, 'isActualVaultOwner': true},
      );
    }
  }
}
