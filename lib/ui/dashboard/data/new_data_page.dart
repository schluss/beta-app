import 'dart:math';

import 'package:flutter/material.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:provider/provider.dart';
import 'package:schluss_beta_app/constants/ui_constants.dart';
import 'package:schluss_beta_app/providers/impl/my_data_provider.dart';
import 'package:schluss_beta_app/singleton_injector.dart';
import 'package:schluss_beta_app/translations/i18n.dart';
import 'package:schluss_beta_app/ui/dashboard/base.dart';
import 'package:schluss_beta_app/ui/reused_widgets/dashboard_page_template.dart';
import 'package:schluss_beta_app/ui/reused_widgets/data_card.dart';
import 'package:schluss_beta_app/ui/reused_widgets/data_polygon_item.dart';
import 'package:schluss_beta_app/ui/reused_widgets/empty_state_content_template.dart';

class NewDataPage extends StatefulWidget {
  const NewDataPage({Key? key}) : super(key: key);

  @override
  NewDataPageState createState() => NewDataPageState();
}

class NewDataPageState extends State<NewDataPage> with SingleTickerProviderStateMixin {
  final MyDataProvider _myDataProvider = singletonInjector<MyDataProvider>();

  @override
  void initState() {
    _initOverviewScreens();

    super.initState();
  }

  /// Initializes overview screens.
  void _initOverviewScreens() {
    _myDataProvider.initRecentOverviewScreen();
  }

  @override
  Widget build(BuildContext context) {
    double width = MediaQuery.of(context).size.width;
    double height = MediaQuery.of(context).size.height;

    /// Header.
    return DashboardPageTemplate(
      text: I18n.of(context).dataTitle,
      addBtnCall: () => Navigator.pushNamed(context, 'data/upload'),
      appBody: buildBody(width, height),
    );
  }

  /// Returns page main body column.
  Column buildBody(double width, double height) {
    return Column(
      children: <Widget>[
        Expanded(
          child: ChangeNotifierProvider<MyDataProvider?>(
            create: (context) => _myDataProvider,
            child: Consumer<MyDataProvider>(
              builder: (context, model, child) {
                if (model.isLoading) {
                  return _buildWaitingArea();
                } else {
                  if (model.dataMap.isNotEmpty) {
                    return _buildDataArea(width, height, false);
                  } else {
                    return EmptyStateContentTemplate(
                      img: const AssetImage('assets/images/vault_1.png'),
                      text: I18n.of(context).dataNoDataTxt,
                    );
                  }
                }
              },
            ),
          ),
        ),
      ],
    );
  }

  /// Builds screen body waiting area.
  Container _buildWaitingArea() {
    return Container(
      color: UIConstants.paleGray,
      child: const Center(
        child: SpinKitThreeBounce(
          color: UIConstants.slateGray100,
          size: 16,
        ),
      ),
    );
  }

  // TODO : Check hasCategorizedData Field needed ( Since A-Z data page removed )
  /// Builds screen body area.
  Container _buildDataArea(
    double width,
    double height,
    bool hasCategorizedData,
  ) {
    // TODO: remove this [random] after integrating the backend process.
    Random random = Random();

    return Container(
      color: UIConstants.paleGray,
      child: ListView(
        padding: EdgeInsets.symmetric(
          horizontal: getPadding(context),
          vertical: height * 0.01,
        ),
        children: <Widget>[
          /// Recent data section.
          Column(
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Padding(
                padding: EdgeInsets.only(
                  left: width * 0.02,
                  bottom: height * 0.01,
                ),
                child: Text(
                  I18n.of(context).dataRecentlyViewedTitle,
                  textAlign: TextAlign.start,
                  style: TextStyle(
                    color: UIConstants.grayDarkest100,
                    fontWeight: FontWeight.w600,
                    fontSize: width * 0.045,
                  ),
                ),
              ),
              SizedBox(
                height: height * 0.16,
                child: SizedBox(
                  child: GridView.count(
                    padding: const EdgeInsets.all(0.0),
                    crossAxisCount: 1,
                    childAspectRatio: (1 / 0.75),
                    shrinkWrap: true,
                    scrollDirection: Axis.horizontal,
                    // TODO: use this logic to load cards data from backend.
                    // children: List.generate(10, (index) {
                    //   return const DataPolygonItem(
                    //     logoImg: '<path>',
                    //     logoColour: <colour>,
                    //     labelText: '<label>',
                    //   ),;
                    // }),

                    children: const [
                      DataPolygonItem(
                        logoImg: 'assets/images/piggy-bank.png',
                        logoColor: UIConstants.pastelGreen,
                        text: 'Verzekering',
                      ),
                      DataPolygonItem(
                        logoImg: 'assets/images/user.png',
                        logoColor: UIConstants.pastelBlue,
                        text: 'Naam',
                      ),
                      DataPolygonItem(
                        logoImg: 'assets/images/home.png',
                        logoColor: UIConstants.pastelYellow,
                        text: 'Zorgverzekering',
                      ),
                      DataPolygonItem(
                        logoImg: 'assets/images/heart.png',
                        logoColor: UIConstants.pastelPink,
                        text: 'Verklaring van Overlijden',
                      ),
                      DataPolygonItem(
                        logoImg: 'assets/images/home.png',
                        logoColor: UIConstants.pastelYellow,
                        text: 'Adres',
                      ),
                      DataPolygonItem(
                        logoImg: 'assets/images/piggy-bank.png',
                        logoColor: UIConstants.pastelGreen,
                        text: 'Verzekering',
                      ),
                      DataPolygonItem(
                        logoImg: 'assets/images/user.png',
                        logoColor: UIConstants.pastelBlue,
                        text: 'Naam',
                      ),
                      DataPolygonItem(
                        logoImg: 'assets/images/user.png',
                        logoColor: UIConstants.pastelBlue,
                        text: 'Naam',
                      ),
                      DataPolygonItem(
                        logoImg: 'assets/images/home.png',
                        logoColor: UIConstants.pastelYellow,
                        text: 'Adres',
                      ),
                      DataPolygonItem(
                        logoImg: 'assets/images/home.png',
                        logoColor: UIConstants.pastelYellow,
                        text: 'Adres',
                      ),
                    ],
                  ),
                ),
              ),
            ],
          ),

          Padding(padding: EdgeInsets.symmetric(vertical: height * 0.01)),

          /// Categorized data section.
          hasCategorizedData
              ? Column(
                  mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Padding(
                      padding: EdgeInsets.only(
                        left: width * 0.02,
                        bottom: height * 0.01,
                      ),
                      child: Text(
                        I18n.of(context).dataCategorizedTitle,
                        textAlign: TextAlign.start,
                        style: TextStyle(
                          color: UIConstants.grayDarkest100,
                          fontWeight: FontWeight.w600,
                          fontSize: width * 0.045,
                        ),
                      ),
                    ),
                    SizedBox(
                      //height: height - height * 0.5,
                      child: Container(
                        color: UIConstants.paleGray,
                        child: GridView.count(
                          primary: false,
                          padding: const EdgeInsets.all(0.0),
                          crossAxisCount: 2,
                          childAspectRatio: (1 / .8),
                          shrinkWrap: true,

                          // TODO: use this logic to load cards data from backend.
                          // children: List.generate(8, (index) {
                          //     return const DataCard(
                          //       logoImg: '<path>',
                          //       logoColour: <colour>,
                          //       title: '<title>',
                          //       count: <count>.toString(),
                          //     );
                          //   }),

                          children: [
                            DataCard(
                              logoImg: 'assets/images/user.png',
                              logoColor: UIConstants.pastelBlue,
                              title: I18n.of(context).dataIdentityCategoryTitle,
                              count: (random.nextInt(20) + 1).toString(),
                            ),
                            DataCard(
                              logoImg: 'assets/images/piggy-bank.png',
                              logoColor: UIConstants.pastelGreen,
                              title: I18n.of(context).dataFinanceCategoryTitle,
                              count: (random.nextInt(20) + 1).toString(),
                            ),
                            DataCard(
                              logoImg: 'assets/images/suitcase.png',
                              logoColor: UIConstants.pastelTurquoise,
                              title: I18n.of(context).dataWorkCategoryTitle,
                              count: (random.nextInt(20) + 1).toString(),
                            ),
                            DataCard(
                              logoImg: 'assets/images/heart.png',
                              logoColor: UIConstants.pastelPink,
                              title: I18n.of(context).dataHealthCategoryTitle,
                              count: (random.nextInt(20) + 1).toString(),
                            ),
                            DataCard(
                              logoImg: 'assets/images/home.png',
                              logoColor: UIConstants.pastelYellow,
                              title: I18n.of(context).dataHomeCategoryTitle,
                              count: (random.nextInt(20) + 1).toString(),
                            ),
                            DataCard(
                              logoImg: 'assets/images/car-alt.png',
                              logoColor: UIConstants.pastelOrange,
                              title: I18n.of(context).dataTransportCategoryTitle,
                              count: (random.nextInt(20) + 1).toString(),
                            ),
                            DataCard(
                              logoImg: 'assets/images/graduation-cap.png',
                              logoColor: UIConstants.pastelPurple,
                              title: I18n.of(context).dataEducationCategoryTitle,
                              count: (random.nextInt(20) + 1).toString(),
                            ),
                            DataCard(
                              logoImg: 'assets/images/infinity.png',
                              logoColor: UIConstants.pastelGrey,
                              title: I18n.of(context).dataGeneralCategoryTitle,
                              count: (random.nextInt(20) + 1).toString(),
                            ),
                          ],
                        ),
                      ),
                    ),
                  ],
                )
              : Container(),
        ],
      ),
    );
  }
}
