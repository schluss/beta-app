import 'package:flutter/material.dart';
import 'package:flutter_polygon/flutter_polygon.dart';
import 'package:schluss_beta_app/constants/ui_constants.dart';
import 'package:schluss_beta_app/translations/i18n.dart';
import 'package:schluss_beta_app/ui/reused_widgets/data_attribute_info_card.dart';
import 'package:schluss_beta_app/ui/reused_widgets/data_attribute_info_full_card.dart';
import 'package:schluss_beta_app/ui/reused_widgets/mini_header_template.dart';

class DataAttributeDetailPage extends StatefulWidget {
  final String logoPath;
  final Color logoColour;
  final String title;

  const DataAttributeDetailPage({
    required this.logoPath,
    required this.logoColour,
    required this.title,
    Key? key,
  }) : super(key: key);

  @override
  DataAttributeDetailPageState createState() => DataAttributeDetailPageState();
}

class DataAttributeDetailPageState extends State<DataAttributeDetailPage> {
  late ScrollController _scrollController;

  late bool _isHidden;

  @override
  void initState() {
    _scrollController = ScrollController();

    _isHidden = true;

    super.initState();
  }

  @override
  void dispose() {
    _scrollController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    double width = MediaQuery.of(context).size.width;
    double height = MediaQuery.of(context).size.height;

    return Scaffold(
      backgroundColor: UIConstants.paleGray,
      body: Stack(
        children: [
          SingleChildScrollView(
            padding: EdgeInsets.only(
              top: height * 0.22,
            ),
            child: Column(
              children: [
                DataAttributeInfoCard(isShown: _isHidden, isDefault: true),
                _isHidden
                    ? Padding(
                        padding: EdgeInsets.only(bottom: height * 0.04),
                        child: GestureDetector(
                          onTap: () => {setState(() => _isHidden = false)},
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.center,
                            crossAxisAlignment: CrossAxisAlignment.center,
                            children: [
                              Text(
                                I18n.of(context).dataShowSrcTxt,
                                textAlign: TextAlign.end,
                                style: TextStyle(
                                  color: UIConstants.gray100,
                                  fontWeight: FontWeight.w400,
                                  fontSize: width * 0.045,
                                ),
                              ),
                              Icon(
                                Icons.keyboard_arrow_down,
                                color: UIConstants.gray100,
                                size: width * 0.06,
                              ),
                            ],
                          ),
                        ),
                      )
                    : Container(),
                !_isHidden
                    ? DataAttributeInfoCard(
                        isShown: !_isHidden,
                      )
                    : Container(),
                !_isHidden
                    ? DataAttributeInfoCard(
                        isShown: !_isHidden,
                      )
                    : Container(),
                !_isHidden
                    ? Padding(
                        padding: EdgeInsets.only(bottom: height * 0.04),
                        child: GestureDetector(
                          onTap: () => {setState(() => _isHidden = true)},
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.center,
                            crossAxisAlignment: CrossAxisAlignment.center,
                            children: [
                              Text(
                                I18n.of(context).dataHideSrcTxt,
                                textAlign: TextAlign.end,
                                style: TextStyle(
                                  color: UIConstants.gray100,
                                  fontWeight: FontWeight.w400,
                                  fontSize: width * 0.045,
                                ),
                              ),
                              Icon(
                                Icons.keyboard_arrow_up,
                                color: UIConstants.gray100,
                                size: width * 0.06,
                              ),
                            ],
                          ),
                        ),
                      )
                    : Container(),
                const DataAttributeInfoFullCard(),
              ],
            ),
          ),

          /// Mini title with the back button and the option button.
          MiniHeaderTemplate(
            text: widget.title,
            hasBackBtn: true,
            optionsBtnCall: () {},
            backgroundColor: UIConstants.paleGray,
          ),

          Container(
            margin: EdgeInsets.only(top: height * 0.12),
            padding: EdgeInsets.only(
              top: height * 0.0,
              right: width * 0.05,
              bottom: height * 0.01,
              left: width * 0.05,
            ),
            color: UIConstants.paleGray,
            child: Row(
              mainAxisAlignment: MainAxisAlignment.start,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                SizedBox(
                  height: height * 0.08,
                  child: ClipPolygon(
                    sides: 6,
                    borderRadius: 8.0,
                    rotate: 0.0,
                    child: Container(
                      color: widget.logoColour,
                      alignment: Alignment.center,
                      child: Image(
                        image: AssetImage(widget.logoPath),
                        width: width * 0.06,
                      ),
                    ),
                  ),
                ),
                Padding(
                  padding: EdgeInsets.only(left: width * 0.02),
                  child: Text(
                    widget.title,
                    textAlign: TextAlign.left,
                    style: TextStyle(
                      color: UIConstants.black,
                      fontSize: height * 0.025,
                      fontWeight: FontWeight.bold,
                    ),
                  ),
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }
}
