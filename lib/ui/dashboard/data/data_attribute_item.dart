import 'package:flutter/material.dart';
import 'package:flutter_polygon/flutter_polygon.dart';
import 'package:schluss_beta_app/constants/ui_constants.dart';
import 'package:schluss_beta_app/translations/i18n.dart';

class DataAttributeItem extends StatefulWidget {
  final String logoPath;
  final Color logoColour;
  final String title;
  final int count;
  final bool hasOptionBtn;

  const DataAttributeItem({
    required this.logoPath,
    required this.logoColour,
    required this.title,
    required this.count,
    this.hasOptionBtn = false,
    Key? key,
  }) : super(key: key);

  @override
  DataAttributeItemState createState() => DataAttributeItemState();
}

class DataAttributeItemState extends State<DataAttributeItem> {
  @override
  Widget build(BuildContext context) {
    double width = MediaQuery.of(context).size.width;
    double height = MediaQuery.of(context).size.height;

    return Container(
      decoration: BoxDecoration(
        border: Border(
          top: buildBorderSide(),
          bottom: buildBorderSide(),
        ),
      ),
      padding: EdgeInsets.symmetric(
        horizontal: width * 0.05,
        vertical: width * 0.02,
      ),
      child: Row(
        crossAxisAlignment: CrossAxisAlignment.center,
        children: <Widget>[
          /// Logo.
          Expanded(
            flex: 2,
            child: SizedBox(
              height: height * 0.082,
              child: ClipPolygon(
                sides: 6,
                borderRadius: 8.0,
                rotate: 0.0,
                child: Container(
                  color: widget.logoColour,
                  alignment: Alignment.center,
                  child: Image(
                    image: AssetImage(widget.logoPath),
                    width: width * 0.05,
                  ),
                ),
              ),
            ),
          ),
          Expanded(
            flex: 7,
            child: Padding(
              padding: EdgeInsets.only(left: width * 0.03),
              child: Column(
                mainAxisAlignment: MainAxisAlignment.start,
                mainAxisSize: MainAxisSize.max,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  /// Title.
                  buildConstrainedBox(
                    context,
                    widget.title,
                    TextStyle(
                      color: UIConstants.grayDarkest100,
                      fontWeight: FontWeight.w600,
                      fontStyle: FontStyle.normal,
                      letterSpacing: 0,
                      fontSize: width * 0.045,
                    ),
                  ),

                  /// Attributes count.
                  Padding(
                    padding: EdgeInsets.only(top: width * 0.01),
                    child: Container(
                      padding: const EdgeInsets.symmetric(
                        horizontal: 8.0,
                        vertical: 5.0,
                      ),
                      decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(6.0),
                        color: UIConstants.paleGray,
                      ),
                      child: buildConstrainedBox(
                        context,
                        widget.count == 1
                            ? '${widget.count} ${I18n.of(
                                context,
                              ).attributeTxt}'
                            : '${widget.count} ${I18n.of(
                                context,
                              ).attributesTxt}',
                        TextStyle(
                          color: UIConstants.gray100,
                          fontWeight: FontWeight.w400,
                          fontStyle: FontStyle.normal,
                          letterSpacing: 0,
                          fontSize: width * 0.045,
                        ),
                      ),
                    ),
                  ),
                ],
              ),
            ),
          ),

          /// Arrow head.
          Expanded(
            flex: 1,
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.end,
              mainAxisAlignment: MainAxisAlignment.end,
              children: <Widget>[
                Container(
                  padding: EdgeInsets.only(left: width * 0.05),
                  child: Icon(
                    !widget.hasOptionBtn ? Icons.keyboard_arrow_right : Icons.more_horiz,
                    color: UIConstants.gray100,
                    size: width * 0.055,
                  ),
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }

  BorderSide buildBorderSide() => const BorderSide(
        color: UIConstants.grayLight,
        width: 0.5,
      );

  ConstrainedBox buildConstrainedBox(
    BuildContext context,
    String text,
    TextStyle textStyle,
  ) {
    double width = MediaQuery.of(context).size.width;

    return ConstrainedBox(
      constraints: BoxConstraints(maxWidth: width * 0.7),
      child: Text(
        text,
        softWrap: true,
        overflow: TextOverflow.ellipsis,
        style: textStyle,
      ),
    );
  }
}
