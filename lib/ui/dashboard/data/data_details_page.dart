import 'package:flutter/material.dart';
import 'package:schluss_beta_app/constants/ui_constants.dart';
import 'package:schluss_beta_app/controller/my_data_controller.dart';
import 'package:schluss_beta_app/translations/i18n.dart';
import 'package:schluss_beta_app/ui/reused_widgets/backward_button.dart';
import 'package:schluss_beta_app/ui/reused_widgets/data_list_item.dart';
import 'package:schluss_beta_app/ui/reused_widgets/data_submission_info.dart';
import 'package:schluss_beta_app/ui/reused_widgets/full_width_button.dart';
import 'package:schluss_beta_app/ui/reused_widgets/group_header.dart';
import 'package:schluss_beta_app/ui/reused_widgets/provider_logo.dart';
import 'package:schluss_beta_app/util/common_util.dart';
import 'package:schluss_beta_app/view_model/contract_recent_item.dart';
import 'package:schluss_beta_app/view_model/field_view_model.dart';
import 'package:schluss_beta_app/view_model/my_data_view_model.dart';
import 'package:session_next/session_next.dart';

class DataDetailsPage extends StatefulWidget {
  final MyDataViewModel dataViewModel;

  const DataDetailsPage(
    this.dataViewModel, {
    Key? key,
  }) : super(key: key);

  @override
  // ignore: no_logic_in_create_state
  DataDetailsPageState createState() => DataDetailsPageState(
        dataViewModel,
      );
}

class DataDetailsPageState extends State<DataDetailsPage> {
  MyDataViewModel dataViewModel;

  final MyDataController _myDataController = MyDataController();

  bool isDecrypted = false;

  ScrollController? _scrollController;
  double _scrollValue = 0;

  DataDetailsPageState(this.dataViewModel);

  @override
  void initState() {
    _scrollController = ScrollController();

    super.initState();
  }

  @override
  void dispose() {
    _scrollController!.dispose();

    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    double width = MediaQuery.of(context).size.width;
    double height = MediaQuery.of(context).size.height;
    double topPadding = MediaQuery.of(context).padding.top;

    var connectionId = SessionNext().get<int>('connectionId') ?? 0;

    return Scaffold(
      body: Stack(
        children: <Widget>[
          Container(
            padding: EdgeInsets.only(top: topPadding),
            color: UIConstants.primaryColor,
            child: NotificationListener(
              onNotification: (dynamic notification) {
                if (notification is ScrollUpdateNotification) {
                  setState(
                    () => _scrollValue = _scrollController!.position.pixels,
                  );
                }
                return false;
              },
              child: ListView(
                controller: _scrollController,
                padding: EdgeInsets.only(top: height * 0.06),
                children: <Widget>[
                  buildTopContainer(
                    width,
                    height,
                    context,
                  ),
                  ...buildChildren(
                    dataViewModel.dataList,
                    width,
                    height,
                  ),
                  connectionId == 0
                      ? FullWidthButton(
                          text: I18n.of(context).dataInfoDelBtnTxt,
                          call: _validateDataAttributes,
                        )
                      : const Text(''),
                ],
              ),
            ),
          ),
          Align(
            alignment: Alignment.topLeft,
            child: Container(
              color: UIConstants.primaryColor,
              height: height * 0.05 + topPadding,
              child: Padding(
                padding: EdgeInsets.only(top: topPadding),
                child: Stack(
                  children: <Widget>[
                    Align(
                      alignment: Alignment.center,
                      child: ConstrainedBox(
                        constraints: BoxConstraints(
                          maxWidth: width * 0.55,
                        ),
                        child: AnimatedCrossFade(
                          crossFadeState: _scrollValue > width * 0.15 ? CrossFadeState.showSecond : CrossFadeState.showFirst,
                          duration: const Duration(milliseconds: 150),
                          firstChild: const Text(''),
                          secondChild: Text(
                            dataViewModel.category ?? '',
                            maxLines: 1,
                            overflow: TextOverflow.ellipsis,
                            textAlign: TextAlign.center,
                            style: TextStyle(
                              color: UIConstants.grayDarkest100,
                              fontSize: width * 0.04,
                              fontWeight: FontWeight.w700,
                              fontStyle: FontStyle.normal,
                            ),
                          ),
                        ),
                      ),
                    ),
                    AnimatedCrossFade(
                      crossFadeState: _scrollValue > width * 0.15 ? CrossFadeState.showSecond : CrossFadeState.showFirst,
                      duration: const Duration(milliseconds: 0),

                      /// Back button.
                      firstChild: BackwardButton(
                        text: I18n.of(context).dataTitle,
                      ),
                      secondChild: Padding(
                        padding: EdgeInsets.only(left: height * 0.02),
                        child: Align(
                          alignment: Alignment.centerLeft,
                          child: GestureDetector(
                            onTap: () {
                              Navigator.pop(context);
                            },
                            child: dataViewModel.dataList != null
                                ? Row(
                                    children: <Widget>[
                                      Icon(
                                        Icons.keyboard_arrow_left,
                                        color: UIConstants.gray100,
                                        size: width * 0.055,
                                      ),
                                      (dataViewModel.category!.length < 15)
                                          ? Text(
                                              I18n.of(context).dataTitle,
                                              style: buildBackArrowTextStyle(width),
                                            )
                                          : ConstrainedBox(
                                              constraints: BoxConstraints(
                                                maxWidth: width * 0.2,
                                              ),
                                              child: Text(
                                                I18n.of(context).dataTitle,
                                                overflow: TextOverflow.ellipsis,
                                                style: buildBackArrowTextStyle(width),
                                              ),
                                            ),
                                    ],
                                  )
                                : const Row(),
                          ),
                        ),
                      ),
                    ),
                  ],
                ),
              ),
            ),
          ),
        ],
      ),
    );
  }

  /// Builds back arrow text style.
  TextStyle buildBackArrowTextStyle(double width) {
    return TextStyle(
      color: UIConstants.gray100,
      fontFamily: UIConstants.subFontFamily,
      fontSize: width * 0.042,
    );
  }

  /// Builds placeholder image.
  AssetImage _loadPlaceholderImg() {
    if (CommonUtil.isImage(dataViewModel.mediaType)) {
      return const AssetImage('assets/images/document_placeholder_icon.png');
    } else {
      return const AssetImage('assets/images/media_placeholder_icon.png');
    }
  }

  /// Builds top container section.
  Container buildTopContainer(double width, double height, BuildContext context) {
    return Container(
      height: (dataViewModel.timeSpan != null) ? height * 0.28 : height * 0.225,
      color: UIConstants.primaryColor,
      child: ConstrainedBox(
        constraints: const BoxConstraints(maxHeight: 180.0),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
          children: <Widget>[
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: <Widget>[
                Expanded(
                  child: Padding(
                    padding: EdgeInsets.only(
                      left: width * 0.075,
                      right: width * 0.01,
                    ),
                    child: ConstrainedBox(
                      constraints: BoxConstraints(maxWidth: width * 0.7),
                      child: Text(
                        dataViewModel.category != null
                            ? dataViewModel.category == 'dataText'
                                ? I18n.of(context).dataTitle
                                : dataViewModel.category!
                            : '',
                        maxLines: 2,
                        textAlign: TextAlign.start,
                        overflow: TextOverflow.ellipsis,
                        softWrap: true,
                        style: TextStyle(
                          height: 1.2,
                          fontWeight: FontWeight.w700,
                          color: UIConstants.grayDarkest100,
                          fontSize: width * 0.07,
                        ),
                      ),
                    ),
                  ),
                ),
                dataViewModel.type == 'file'
                    ? Padding(
                        padding: EdgeInsets.only(right: width * 0.04),
                        child: Image(
                          image: _loadPlaceholderImg(),
                          height: height * 0.06,
                        ),
                      )
                    : ProviderLogo(logoImg: dataViewModel.logoUrl ?? '')
              ],
            ),
            Padding(
              padding: EdgeInsets.only(
                left: width * 0.075,
                right: width * 0.075,
              ),
              child: buildHeaderRow(
                I18n.of(context).dataInfoRetrievedDateTxt,
                dataViewModel.timeSpan != null
                    ? I18n.of(context).getString(
                        dataViewModel.timeSpan!,
                      )
                    : '',
              ),
            ),
            Padding(
              padding: EdgeInsets.only(
                left: width * 0.075,
                right: width * 0.04,
              ),
              child: buildHeaderRow(
                I18n.of(context).dataInfoSrcTxt,
                getSource(),
              ),
            ),
          ],
        ),
      ),
    );
  }

  /// Gets source.
  String getSource() {
    String source = '';

    if (dataViewModel.type == 'file') {
      if (CommonUtil.isImage(dataViewModel.mediaType)) {
        source = I18n.of(context).dataUploadOptionBottomShtDocTxt;
      } else {
        source = I18n.of(context).dataUploadOptionBottomShtMediaTxt;
      }
    } else if (dataViewModel.providerName != null) {
      source = dataViewModel.providerName!;
    }
    return source;
  }

  /// Builds header.
  Row buildHeaderRow(String title, String value) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      children: <Widget>[
        DataSubmissionInfo(text: title.toUpperCase(), value: value, widthFactor: 0.6),
      ],
    );
  }

  /// Builds children.
  List<Widget> buildChildren(
    List<FieldViewModel>? dataList,
    double width,
    double height,
  ) {
    List<Widget> children = <Widget>[];

    var headerName = '';

    if (dataViewModel.type == 'file') {
      headerName = I18n.of(context).dataTitle.toUpperCase();
    } else {
      headerName = dataViewModel.category!.toUpperCase();
    }

    children.add(GroupHeader(text: headerName));

    for (var fieldViewModel in dataList!) {
      children.add(
        InkWell(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.start,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              DataListItem(
                model: fieldViewModel,
                isDecryptionNeeded: true,
              ),
              const Divider(height: 1),
            ],
          ),
        ),
      );
    }
    return children;
  }

  /// Validates data attributes to check if its connected with any contracts.
  void _validateDataAttributes() async {
    List<ContractItem> contractList = [];

    contractList = await _myDataController.validateDataWithContracts(
      dataViewModel,
    );
    if (!mounted) return;
    if (contractList.isNotEmpty) {
      /// Pops-up delete data error bottom-up Page.
      Navigator.pushNamed(
        context,
        '/data/data/delete/error',
        arguments: {
          'contractList': contractList,
        },
      );
    } else {
      /// Pops-up delete data bottom-up Page.
      Navigator.pushNamed(
        context,
        '/data/data/delete',
        arguments: {
          'dataViewModel': dataViewModel,
        },
      );
    }
  }
}
