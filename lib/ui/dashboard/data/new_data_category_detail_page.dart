import 'package:flutter/material.dart';
import 'package:schluss_beta_app/constants/ui_constants.dart';
import 'package:schluss_beta_app/translations/i18n.dart';
import 'package:schluss_beta_app/ui/dashboard/data/data_attribute_item.dart';
import 'package:schluss_beta_app/ui/reused_widgets/icon_header_template.dart';

class DataCategoryDetailPage extends StatefulWidget {
  final String logoPath;
  final Color logoColour;
  final String title;

  const DataCategoryDetailPage({
    required this.logoPath,
    required this.logoColour,
    required this.title,
    Key? key,
  }) : super(key: key);

  @override
  DataCategoryDetailPageState createState() => DataCategoryDetailPageState();
}

class DataCategoryDetailPageState extends State<DataCategoryDetailPage> {
  late ScrollController _scrollController;

  @override
  void initState() {
    _scrollController = ScrollController();

    super.initState();
  }

  @override
  void dispose() {
    _scrollController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    double width = MediaQuery.of(context).size.width;
    double height = MediaQuery.of(context).size.height;

    return Scaffold(
      backgroundColor: UIConstants.paleGray,
      body: Stack(
        children: [
          SingleChildScrollView(
            padding: EdgeInsets.only(top: height * 0.19),
            child: Container(
              color: UIConstants.primaryColor,
              child: Column(
                children: [
                  ListView(
                    shrinkWrap: true,
                    padding: const EdgeInsets.only(top: 0.0),
                    children: <Widget>[
                      InkWell(
                        splashColor: UIConstants.blackFaded,
                        onTap: null,
                        child: DataAttributeItem(
                          logoPath: widget.logoPath,
                          logoColour: widget.logoColour,
                          title: I18n.of(
                            context,
                          ).identityConfirmationBottomShtCardTxt1,
                          count: 3,
                          hasOptionBtn: true,
                        ),
                      ),
                      InkWell(
                        splashColor: UIConstants.blackFaded,
                        onTap: null,
                        child: DataAttributeItem(
                          logoPath: widget.logoPath,
                          logoColour: widget.logoColour,
                          title: I18n.of(
                            context,
                          ).identityConfirmationBottomShtCardTxt2,
                          count: 1,
                          hasOptionBtn: true,
                        ),
                      ),
                      InkWell(
                        splashColor: UIConstants.blackFaded,
                        onTap: null,
                        child: DataAttributeItem(
                          logoPath: widget.logoPath,
                          logoColour: widget.logoColour,
                          title: I18n.of(context).nationalityTxt,
                          count: 3,
                          hasOptionBtn: true,
                        ),
                      ),
                    ],
                  ),
                ],
              ),
            ),
          ),

          /// Icon title with the back button.
          IconHeaderTemplate(
            logo: widget.logoPath,
            text: widget.title,
            hasBackBtn: true,
          ),

          /// Sort text.
          Container(
            margin: EdgeInsets.only(top: height * 0.15),
            padding: EdgeInsets.only(left: width * 0.06, bottom: height * 0.02),
            color: UIConstants.primaryColor,
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: <Widget>[
                /// Label.
                Expanded(
                  flex: 5,
                  child: Text(
                    I18n.of(context).identityConfirmationBottomShtCardTxt1,
                    maxLines: 1,
                    textAlign: TextAlign.left,
                    overflow: TextOverflow.ellipsis,
                    softWrap: true,
                    style: TextStyle(
                      fontFamily: UIConstants.subFontFamily,
                      color: UIConstants.gray100,
                      fontWeight: FontWeight.w400,
                      fontStyle: FontStyle.normal,
                      letterSpacing: 0,
                      fontSize: width * 0.040,
                    ),
                  ),
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }
}
