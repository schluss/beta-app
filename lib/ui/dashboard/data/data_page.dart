import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:provider/provider.dart';
import 'package:schluss_beta_app/constants/ui_constants.dart';
import 'package:schluss_beta_app/controller/account_controller.dart';
import 'package:schluss_beta_app/providers/impl/my_data_provider.dart';
import 'package:schluss_beta_app/singleton_injector.dart';
import 'package:schluss_beta_app/translations/i18n.dart';
import 'package:schluss_beta_app/ui/dashboard/data/file_list_item.dart';
import 'package:schluss_beta_app/ui/reused_widgets/dashboard_page_template.dart';
import 'package:schluss_beta_app/ui/reused_widgets/empty_state_content_template.dart';
import 'package:schluss_beta_app/ui/reused_widgets/main_button.dart';
import 'package:schluss_beta_app/ui/reused_widgets/providers_list_item.dart';
import 'package:schluss_beta_app/util/error_handler_util.dart';
import 'package:session_next/session_next.dart';

class DataPage extends StatefulWidget {
  const DataPage({Key? key}) : super(key: key);

  @override
  DataPageState createState() => DataPageState();
}

class DataPageState extends State<DataPage> with SingleTickerProviderStateMixin {
  final MyDataProvider _myDataProvider = singletonInjector<MyDataProvider>();
  final ErrorHandlerUtil _errorHandler = ErrorHandlerUtil();
  final AccountController _accountController = AccountController();
  bool _isActualVaultOwner = true;

  final key = GlobalKey();
  OverlayEntry? topDarkBanner;
  final layerLink = LayerLink();

  @override
  void initState() {
    _initOverviewScreens();

    super.initState();

    _showTopDarkBanner();
  }

  @override
  void dispose() {
    _hideTopDarkBanner();

    super.dispose();
  }

  void _showTopDarkBanner() {
    if (!_isActualVaultOwner && topDarkBanner == null) {
      WidgetsBinding.instance.addPostFrameCallback((_) {
        if (!mounted) return;

        final overlay = Overlay.of(context);

        final renderBox = key.currentContext?.findRenderObject() as RenderBox;

        topDarkBanner = OverlayEntry(
          maintainState: true,
          builder: (context) => _buildTopDarkBanner(context, renderBox),
        );

        overlay.insert(topDarkBanner!);
      });
    }
  }

  /// Hides top dark banner.
  void _hideTopDarkBanner() {
    topDarkBanner?.remove();
    topDarkBanner = null;
  }

  /// Top dark banner.
  Widget _buildTopDarkBanner(BuildContext context, RenderBox renderBox) {
    final size = renderBox.size;
    final offSet = renderBox.localToGlobal(Offset.zero);

    return Positioned(
      top: offSet.dy + MediaQuery.of(context).padding.top,
      width: size.width,
      height: size.height * 0.18,
      child: CompositedTransformFollower(
        link: layerLink,
        showWhenUnlinked: false,
        child: Container(
          margin: EdgeInsets.only(top: MediaQuery.of(context).padding.top),
          color: UIConstants.grayDarkest100,
          padding: EdgeInsets.symmetric(
            horizontal: MediaQuery.of(context).size.width * 0.05,
            vertical: MediaQuery.of(context).size.width * 0.03,
          ),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              /// Note text.
              Expanded(
                flex: 5,
                child: RichText(
                  textAlign: TextAlign.start,
                  softWrap: true,
                  maxLines: 2,
                  text: TextSpan(
                    style: const TextStyle(
                      fontFamily: UIConstants.subFontFamily,
                      color: UIConstants.primaryColor,
                      fontSize: 13,
                    ),
                    children: <TextSpan>[
                      /*
                      TextSpan(
                        text: I18n.of(context).vaultSwitchedTopDarkBannerNoteTxt,
                        style: const TextStyle(fontWeight: FontWeight.w600),
                      ),
                      */
                      TextSpan(
                        text: I18n.of(context).vaultSwitchedTopDarkBannerTxt(
                          userForenameText: _accountController.getCurrentVaultName(),
                        ),
                        style: const TextStyle(fontWeight: FontWeight.w400),
                      ),
                    ],
                  ),
                ),
              ),

              /// Button.
              Expanded(
                flex: 5,
                child: Padding(
                  padding: EdgeInsets.symmetric(
                      vertical: MediaQuery.of(
                            context,
                          ).size.height *
                          0.012),
                  child: MainButton(
                    type: BtnType.outlineDark,
                    text: I18n.of(context).vaultSwitchedTopDarkBannerBtnTxt,
                    textColor: UIConstants.primaryColor,
                    call: () {
                      _accountController.changeVaultOwner(0);

                      Navigator.pushNamed(
                        context,
                        '/dashboard',
                        arguments: {'activeTab': 0, 'isActualVaultOwner': true},
                      );

                      topDarkBanner?.remove();
                      topDarkBanner = null;
                    },
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }

  /// Initializes overview screens.
  void _initOverviewScreens() {
    _myDataProvider.initRecentOverviewScreen();
    var connectionId = SessionNext().get<int>('connectionId') ?? 0;
    setState(() => _isActualVaultOwner = connectionId == 0 ? true : false);
  }

  @override
  Widget build(BuildContext context) {
    double height = MediaQuery.of(context).size.height;
    double width = MediaQuery.of(context).size.width;

    /// Header.
    return CompositedTransformTarget(
      link: layerLink,
      child: DashboardPageTemplate(
        key: key,
        text: I18n.of(context).dataTitle,
        addBtnCall: () => Navigator.pushNamed(context, 'data/upload'),
        appBar: PreferredSize(
          preferredSize: const Size.fromHeight(0.0),
          child: AppBar(
            elevation: 0.0,
            backgroundColor: _isActualVaultOwner ? UIConstants.primaryColor : UIConstants.grayDarkest100,
            systemOverlayStyle: _isActualVaultOwner ? SystemUiOverlayStyle.dark : SystemUiOverlayStyle.light,
          ),
        ),
        topPadding: _isActualVaultOwner ? 0.0 : height * 0.12,
        appBody: buildBody(width),
      ),
    );
  }

  /// Refreshes page with respectively.
  void _refreshPage(bool? status) {
    status = status == null ? false : true;
    if (status) {
      try {
        _initOverviewScreens();
      } catch (e) {
        _errorHandler.handleError(e.toString(), false, context);
      }
    }
  }

  /// Returns page main body column.
  Column buildBody(double width) {
    return Column(
      children: <Widget>[
        Expanded(
          child: ChangeNotifierProvider<MyDataProvider?>(
            create: (context) => _myDataProvider,
            child: Consumer<MyDataProvider>(
              builder: (context, model, child) {
                if (model.isLoading) {
                  return _buildWaitingArea();
                } else {
                  if (model.dataMap.isNotEmpty) {
                    return _buildDataScreen(width);
                  } else {
                    return EmptyStateContentTemplate(
                      img: const AssetImage('assets/images/vault_1.png'),
                      text: I18n.of(context).dataNoDataTxt,
                    );
                  }
                }
              },
            ),
          ),
        ),
      ],
    );
  }

  /// Builds screen body waiting area.
  Container _buildWaitingArea() {
    return Container(
      color: UIConstants.paleGray,
      child: const Center(
        child: SpinKitThreeBounce(
          color: UIConstants.slateGray100,
          size: 16,
        ),
      ),
    );
  }

  /// Builds screen body tab area.
  Column _buildDataScreen(double width) {
    return Column(
      children: <Widget>[
        Expanded(
          child: Consumer<MyDataProvider>(
            builder: (context, model, child) => Container(
              color: UIConstants.primaryColor,
              child: buildContainer(model.dataMap, width),
            ),
          ),
        ),
      ],
    );
  }

  /// Builds container of list-view.
  ListView buildContainer(Map map, double width) {
    return ListView(
      padding: EdgeInsets.zero,
      children: <Widget>[...buildChildren(map, width)],
    );
  }

  /// Builds children of list.
  List<Widget> buildChildren(Map map, double width) {
    var children = <Widget>[];
    map.forEach(
      (key, value) {
        value.forEach(
          (element) {
            children.add(
              Material(
                color: UIConstants.primaryColor,
                child: InkWell(
                  splashColor: UIConstants.blackFaded,
                  onTap: () async {
                    // Wait for close bottom up screens to refresh.
                    final status = await Navigator.pushNamed(
                      context,
                      '/data/data',
                      arguments: element,
                    );
                    _refreshPage(status as bool?);
                  },
                  child: element.type == 'file'
                      ? FileListItem(
                          title: element.providerName,
                          mediaType: element.mediaType,
                          subTitle: (element.timeSpan == null
                              ? ' '
                              : ' - ${I18n.of(
                                  context,
                                ).getString(element.timeSpan)}'),
                        )
                      : ProvidersListItem(
                          title: element.category,
                          subTitle: element.providerName,
                          logo: element.logoUrl ?? '',
                        ),
                ),
              ),
            );
            children.add(const Divider(height: 1));
          },
        );
      },
    );
    return children;
  }
}
