import 'dart:io';

import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:schluss_beta_app/constants/theme.dart';
import 'package:schluss_beta_app/constants/ui_constants.dart';
import 'package:schluss_beta_app/translations/i18n.dart';
import 'package:schluss_beta_app/ui/reused_widgets/dashed_separator.dart';
import 'package:schluss_beta_app/ui/reused_widgets/data_submission_info.dart';
import 'package:schluss_beta_app/ui/reused_widgets/half_circle.dart';
import 'package:schluss_beta_app/ui/reused_widgets/main_button.dart';

class DataExportingDonePage extends StatefulWidget {
  final String? filepath;
  const DataExportingDonePage(
    this.filepath, {
    Key? key,
  }) : super(key: key);

  @override
  DataExportingDonePageState createState() => DataExportingDonePageState();
}

class DataExportingDonePageState extends State<DataExportingDonePage> {
  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    double width = MediaQuery.of(context).size.width;
    double height = MediaQuery.of(context).size.height;

    return Scaffold(
      body: Container(
        height: height,
        color: UIConstants.grayLight,
        child: Stack(
          children: <Widget>[
            Align(
              alignment: Alignment.bottomCenter,
              child: Container(
                width: width,
                height: height * 0.935,
                decoration: const BoxDecoration(
                  color: UIConstants.primaryColor,
                  borderRadius: mainBorderRadius,
                ),
                child: Column(
                  children: <Widget>[
                    Container(
                      padding: EdgeInsets.only(top: width * 0.03),
                      height: height * 0.175,
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: <Widget>[
                          Text(
                            I18n.of(context).exportProcessCompleteTitleTxt,
                            style: TextStyle(
                              color: UIConstants.grayDarkest100,
                              fontSize: width * 0.055,
                              fontWeight: FontWeight.w700,
                            ),
                          ),
                          Text(
                            I18n.of(context).exportProcessCompleteSubTitleTxt,
                            style: TextStyle(
                              color: UIConstants.slateGray100,
                              fontSize: width * 0.0425,
                            ),
                          ),
                        ],
                      ),
                    ),
                    Column(
                      children: <Widget>[
                        ConstrainedBox(
                          constraints: const BoxConstraints(
                            maxHeight: 100.0,
                          ),
                          child: Container(
                            margin: EdgeInsets.only(top: height * 0.03),
                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              mainAxisAlignment: MainAxisAlignment.start,
                              children: <Widget>[
                                Padding(
                                  padding: EdgeInsets.only(
                                    left: width * 0.075,
                                    right: width * 0.075,
                                  ),
                                  child: Row(
                                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                    children: <Widget>[
                                      DataSubmissionInfo(
                                        text: I18n.of(context).dataReqDoneDateTxt,
                                        value: _getDateString(),
                                      ),
                                      DataSubmissionInfo(
                                        text: I18n.of(
                                          context,
                                        ).exportProcessCompleteTypeTxt,
                                        value: I18n.of(
                                          context,
                                        ).exportProcessCompleteDataBackupTxt,
                                      ),
                                    ],
                                  ),
                                ),
                              ],
                            ),
                          ),
                        ),
                        const Divider(height: 1),
                        Container(
                          padding: EdgeInsets.only(
                            left: width * 0.075,
                            right: width * 0.075,
                          ),
                          child: Text(
                            I18n.of(context).exportProcessCompleteDescTxt(
                              fileName: _getFilePath(),
                            ),
                            style: TextStyle(
                              color: UIConstants.slateGray100,
                              fontSize: width * 0.0425,
                            ),
                          ),
                        ),
                      ],
                    )
                  ],
                ),
              ),
            ),
            _buildTicketDecoration(context),
            _buildTopCheckIcon(context),
            Padding(
              padding: EdgeInsets.only(
                right: width * 0.075,
                left: width * 0.075,
                bottom: height / 18,
              ),
              child: Align(
                alignment: Alignment.bottomCenter,
                child: MainButton(
                  type: BtnType.active,
                  text: I18n.of(context).btnDoneTxt,
                  call: () => Navigator.pushNamed(
                    context,
                    '/dashboard',
                    arguments: {'activeTab': 4, 'isActualVaultOwner': true},
                  ),
                ),
              ),
            )
          ],
        ),
      ),
    );
  }

  /// Returns date as a string.
  String _getDateString() {
    // Format date field with Netherland locale.
    return DateFormat('d MMMM yyyy', 'nl').format(
      DateTime.now(),
    );
  }

  /// Returns file path.
  String _getFilePath() {
    String filePath = '';
    if (Platform.isAndroid) {
      filePath = 'Downloads -> ${widget.filepath!.split('/').last}';
    } else if (Platform.isIOS) {
      filePath = widget.filepath!.replaceAll(Platform.pathSeparator, ' -> ');
    }

    return filePath;
  }

  /// Builds separating design of dashes ends with two half circles.
  Padding _buildTicketDecoration(BuildContext context) {
    double width = MediaQuery.of(context).size.width;

    return Padding(
      padding: EdgeInsets.only(top: MediaQuery.of(context).size.height * 0.22),
      child: Stack(
        children: <Widget>[
          Padding(
            padding: EdgeInsets.only(top: width * 0.03),
            child: const DashedSeparator(),
          ),
          const Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: <Widget>[
              RotatedBox(
                quarterTurns: 1,
                child: HalfCircle(),
              ),
              RotatedBox(
                quarterTurns: 3,
                child: HalfCircle(),
              )
            ],
          ),
        ],
      ),
    );
  }

  /// Builds top greenDark check mark of provider logo.
  Align _buildTopCheckIcon(BuildContext context) {
    double height = MediaQuery.of(context).size.height;
    return Align(
      alignment: Alignment.topCenter,
      child: Padding(
        padding: EdgeInsets.only(
          top: MediaQuery.of(
                    context,
                  ).padding.top ==
                  0
              ? height * 0.0175
              : MediaQuery.of(
                  context,
                ).padding.top,
        ),
        child: Container(
          width: height * 0.075,
          height: height * 0.075,
          alignment: Alignment.center,
          decoration: BoxDecoration(
            shape: BoxShape.circle,
            color: UIConstants.accentColor,
            boxShadow: const [
              BoxShadow(
                color: UIConstants.rosyPinkDark,
                offset: Offset(0, 7),
                blurRadius: 17,
                spreadRadius: -7,
              ),
            ],
            border: Border.all(color: UIConstants.accentColor),
          ),
          child: Icon(
            Icons.check,
            color: UIConstants.primaryColor,
            size: height * 0.0425,
          ),
        ),
      ),
    );
  }
}
