import 'package:flutter/material.dart';
import 'package:schluss_beta_app/translations/i18n.dart';
import 'package:schluss_beta_app/ui/reused_widgets/main_button.dart';
import 'package:schluss_beta_app/ui/reused_widgets/plain_info_screen_template.dart';

class FeedbackDonePage extends StatefulWidget {
  const FeedbackDonePage({Key? key}) : super(key: key);

  @override
  FeedbackDonePageState createState() => FeedbackDonePageState();
}

class FeedbackDonePageState extends State<FeedbackDonePage> with SingleTickerProviderStateMixin {
  int count = 0;
  int popCountMax = 2;
  @override
  Widget build(BuildContext context) {
    return PlainInfoScreenTemplate(
      img: const AssetImage('assets/images/streamline_icon.png'),
      title: I18n.of(context).feedbackDoneHeader,
      mainText: I18n.of(context).feedbackDoneTxt,
      dynamicContent: Column(
        mainAxisAlignment: MainAxisAlignment.end,
        children: <Widget>[
          MainButton(
            type: BtnType.active,
            text: I18n.of(context).btnGoBackTxt,
            call: () => Navigator.of(context).popUntil(
              (_) => count++ >= popCountMax,
            ),
          ),
        ],
      ),
      backBtnPopCount: popCountMax,
    );
  }
}
