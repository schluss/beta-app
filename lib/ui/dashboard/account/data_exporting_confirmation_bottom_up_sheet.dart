import 'package:flutter/material.dart';
import 'package:schluss_beta_app/translations/i18n.dart';
import 'package:schluss_beta_app/ui/reused_widgets/confirmation_bottom_up_sheet_contents_template.dart';
import 'package:schluss_beta_app/ui/reused_widgets/pin_verification_bottom_up_sheet.dart';

class DataExportingConfirmationBottomUpSheet extends StatelessWidget {
  const DataExportingConfirmationBottomUpSheet({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    double width = MediaQuery.of(context).size.width;

    return ConfirmationBottomUpSheetContentsTemplate(
      title: I18n.of(context).accountExportBottomShtTitle,
      mainText: I18n.of(context).accountExportBottomShtTxt,
      activeBtnText: I18n.of(context).btnExportDataTxt,
      activeBtnCall: () => Navigator.pushNamed(
        context,
        '/account/verify',
        arguments: {'processType': ProcessType.exportData},
      ),
      topIcon: Image(
        image: const AssetImage('assets/images/backup_pink_icon.png'),
        height: width * 0.084,
      ),
    );
  }
}
