import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:schluss_beta_app/constants/ui_constants.dart';
import 'package:schluss_beta_app/providers/impl/data_export_provider.dart';
import 'package:schluss_beta_app/singleton_injector.dart';
import 'package:schluss_beta_app/translations/i18n.dart';
import 'package:schluss_beta_app/ui/reused_widgets/animated_circle_icon.dart';
import 'package:schluss_beta_app/ui/reused_widgets/main_button.dart';
import 'package:schluss_beta_app/ui/reused_widgets/main_title.dart';
import 'package:schluss_beta_app/ui/reused_widgets/mini_header_template.dart';
import 'package:schluss_beta_app/ui/reused_widgets/progress_button.dart';
import 'package:schluss_beta_app/util/error_handler_util.dart';

class DataExportingPageBottomUpSheet extends StatefulWidget {
  final String? password;

  const DataExportingPageBottomUpSheet({
    Key? key,
    this.password,
  }) : super(key: key);

  @override
  DataExportingPageState createState() => DataExportingPageState();
}

class DataExportingPageState extends State<DataExportingPageBottomUpSheet> with TickerProviderStateMixin {
  final DataExportProvider _dataExportProvider = singletonInjector<DataExportProvider>();
  final ErrorHandlerUtil _errorHandler = ErrorHandlerUtil();

  late AnimationController _controller;
  late Animation<Offset> _offsetAnimation;
  late AnimationController _controllerScale;

  var scaleValue = 0.9;

  @override
  void initState() {
    super.initState();

    try {
      _dataExportProvider.initPendingScreen(widget.password);
    } catch (e) {
      _errorHandler.handleError(e.toString(), false, context);
    }

    _controllerScale = AnimationController(
      vsync: this,
      lowerBound: 0.9,
      upperBound: 1.0,
      duration: const Duration(milliseconds: 800),
    )..repeat(
        reverse: true,
      );

    _controllerScale.addListener(() {
      setState(() {
        if (_dataExportProvider.getProcessingStatus == ProcessingState.creating) {
          scaleValue = _controllerScale.value;
        } else {
          scaleValue = 1.0;
        }
      });
    });

    _controller = AnimationController(
      duration: const Duration(milliseconds: 500),
      vsync: this,
    )..repeat(reverse: true);
    _offsetAnimation = Tween<Offset>(
      begin: Offset.zero,
      end: const Offset(0.5, 0.0),
    ).animate(CurvedAnimation(
      parent: _controller,
      curve: Curves.linear,
    ));
  }

  @override
  void dispose() {
    super.dispose();

    _controllerScale.dispose();
    _controller.dispose();
  }

  @override
  Widget build(BuildContext context) {
    double width = MediaQuery.of(context).size.width;
    double height = MediaQuery.of(context).size.height;

    return WillPopScope(
      onWillPop: () async {
        _generateButtonEvent();
        return true;
      },
      child: ChangeNotifierProvider<DataExportProvider?>(
        create: (context) => _dataExportProvider,
        child: Consumer<DataExportProvider>(
          builder: (context, model, child) => Scaffold(
            backgroundColor: UIConstants.primaryColor,
            body: Stack(
              children: <Widget>[
                Center(
                  child: Stack(
                    children: <Widget>[
                      Stack(
                        children: <Widget>[
                          Container(
                            alignment: Alignment.centerLeft,
                            child: Image(
                              image: const AssetImage(
                                'assets/images/pulse.png',
                              ),
                              width: width * 0.53,
                            ),
                          ),
                          Container(
                            alignment: Alignment.centerRight,
                            child: Image(
                              image: const AssetImage(
                                'assets/images/pulse.png',
                              ),
                              width: width * 0.53,
                            ),
                          ),
                        ],
                      ),
                      Padding(
                        padding: EdgeInsets.only(
                          left: width * 0.075,
                          right: width * 0.075,
                        ),
                        child: Center(
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                            children: <Widget>[
                              Transform.scale(
                                scale: scaleValue,
                                child: AnimatedCircleIcon(
                                  img: Image(
                                    image: const AssetImage(
                                      'assets/images/save_box_icon.png',
                                    ),
                                    width: width * 0.125,
                                    height: width * 0.125,
                                  ),
                                  isError: model.getProcessingStatus == ProcessingState.error,
                                ),
                              ),
                              SlideTransition(
                                position: _offsetAnimation,
                                child: Container(
                                  width: width * 0.085,
                                  height: width * 0.085,
                                  decoration: const BoxDecoration(
                                    shape: BoxShape.circle,
                                    color: UIConstants.primaryColor,
                                    boxShadow: [
                                      BoxShadow(
                                        color: UIConstants.gray50,
                                        offset: Offset(0, 1),
                                        blurRadius: 2,
                                        spreadRadius: 0,
                                      ),
                                    ],
                                  ),
                                  child: Align(
                                    alignment: Alignment.center,
                                    child: Image(
                                      image: AssetImage(_getCentralIconPath()),
                                      width: (model.getProcessingStatus == ProcessingState.error) ? width * 0.035 : width * 0.045,
                                    ),
                                  ),
                                ),
                              ),
                              AnimatedCircleIcon(
                                img: Image(
                                  image: const AssetImage(
                                    'assets/images/document_icon.png',
                                  ),
                                  width: width * 0.125,
                                  height: width * 0.125,
                                ),
                                isShadowPink: true,
                                isError: model.getProcessingStatus == ProcessingState.error,
                              ),
                            ],
                          ),
                        ),
                      ),
                    ],
                  ),
                ),
                Stack(
                  children: <Widget>[
                    Align(
                      alignment: Alignment.topCenter,
                      child: Container(
                        padding: EdgeInsets.only(top: height * 0.2),
                        child: Column(
                          children: <Widget>[
                            MainTitle(
                              text: (model.getProcessingStatus == ProcessingState.error)
                                  ? I18n.of(
                                      context,
                                    ).dataReqRetrievalErrorTitle
                                  : model.getZipFileName,
                            ),
                          ],
                        ),
                      ),
                    ),
                    Align(
                      alignment: Alignment.bottomCenter,
                      child: Container(
                        padding: EdgeInsets.only(
                          bottom: height * 0.25,
                          left: width * 0.2,
                          right: width * 0.2,
                        ),
                        child: _loadBottomText(width),
                      ),
                    ),
                    Align(
                      alignment: Alignment.topCenter,
                      child: Container(
                        height: height * 0.1,
                        color: UIConstants.paleLilac,
                      ),
                    ),

                    /// Mini title with close button.
                    MiniHeaderTemplate(
                      text: I18n.of(context).exportCreatingTitleTxt,
                      closeBtnCall: _onScreenCloseEvent,
                    ),
                  ],
                ),
                Padding(
                  padding: EdgeInsets.only(
                    right: width * 0.075,
                    left: width * 0.075,
                    bottom: height / 18,
                  ),
                  child: Align(
                    alignment: Alignment.bottomCenter,
                    child: model.progressValue != 100
                        ? ProgressButton(
                            value: model.progressValue / 100,
                            text: _getButtonText(),
                          )
                        : MainButton(
                            type: model.getProcessingStatus == ProcessingState.creating ? BtnType.awaited : BtnType.active,
                            text: _getButtonText(),
                            call: () => _generateButtonEvent(),
                          ),
                  ),
                )
              ],
            ),
          ),
        ),
      ),
    );
  }

  /// Generates main button event as the [RequestState].
  void _generateButtonEvent() {
    if (_dataExportProvider.getProcessingStatus == ProcessingState.success) {
      Navigator.pushNamedAndRemoveUntil(
        context,
        '/account/export-data/done',
        ModalRoute.withName('/dashboard'),
        arguments: _dataExportProvider.getZipFileName,
      );
    } else if (_dataExportProvider.getProcessingStatus == ProcessingState.error) {
      _onScreenCloseEvent();
    }
  }

  /// Closes the screen.
  void _onScreenCloseEvent() {
    Navigator.pushNamed(
      context,
      '/dashboard',
      arguments: {'activeTab': 4, 'isActualVaultOwner': true},
    );
  }

  /// Loads the [_path] of the center animation image and changes animation status.
  String _getCentralIconPath() {
    String path;

    if (_dataExportProvider.getProcessingStatus == ProcessingState.success) {
      path = 'assets/images/green_check_icon.png';
      _stopAnimation();
    } else if (_dataExportProvider.getProcessingStatus == ProcessingState.creating) {
      path = 'assets/images/arrow_icon.png';
    } else {
      path = 'assets/images/grey_cross_icon.png';
      _stopAnimation();
    }
    return path;
  }

  /// Stops animation.
  void _stopAnimation() {
    scaleValue = 1.0;

    _controller.reset();
    _controller.stop();
    _controllerScale.stop();
  }

  /// Loads bottom description Error Text.
  Text? _loadBottomText(double width) {
    Text? text;

    if (_dataExportProvider.getProcessingStatus == ProcessingState.error) {
      text = Text(
        I18n.of(context).dataReqRetrievalErrorTxt,
        textAlign: TextAlign.center,
        style: TextStyle(
          color: UIConstants.gray100,
          fontSize: width * 0.045,
        ),
      );
    } else if (_dataExportProvider.getProcessingStatus == ProcessingState.success) {
      text = Text(
        I18n.of(context).exportCreatingSuccessBottomTxt,
        textAlign: TextAlign.center,
        style: TextStyle(
          color: UIConstants.gray100,
          fontSize: width * 0.045,
        ),
      );
    }
    return text;
  }

  /// Loads text of the button.
  String _getButtonText() {
    String text;

    if (_dataExportProvider.getProcessingStatus == ProcessingState.success) {
      text = I18n.of(context).btnContinueTxt;
    } else if (_dataExportProvider.getProcessingStatus == ProcessingState.creating) {
      text = I18n.of(context).exportCreatingLoadingBtnTxt;
    } else {
      text = I18n.of(context).exportCreatingErrorBtnTxt;
    }
    return text;
  }
}
