import 'package:flutter/material.dart';
import 'package:schluss_beta_app/constants/ui_constants.dart';
import 'package:schluss_beta_app/translations/i18n.dart';
import 'package:schluss_beta_app/ui/reused_widgets/bottom_up_sheet_template.dart';
import 'package:schluss_beta_app/ui/reused_widgets/provider_logo.dart';
import 'package:schluss_beta_app/util/initials_picker_util.dart';
import 'package:schluss_beta_app/view_model/vault_switch_item_view_model.dart';
import 'package:session_next/session_next.dart';

class VaultsSwitchingBottomUpSheet extends StatefulWidget {
  const VaultsSwitchingBottomUpSheet({
    Key? key,
  }) : super(key: key);

  @override
  VaultsSwitchingBottomUpSheetState createState() => VaultsSwitchingBottomUpSheetState();
}

class VaultsSwitchingBottomUpSheetState extends State<VaultsSwitchingBottomUpSheet> {
  final InitialsPickerUtil _initialsPickerUtil = InitialsPickerUtil();

  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    double width = MediaQuery.of(context).size.width;
    double height = MediaQuery.of(context).size.height;

    return BottomUpSheetTemplate(
      page: Stack(
        children: [
          /// Title.
          Padding(
            padding: const EdgeInsets.only(top: 8.0),
            child: Align(
              alignment: Alignment.topCenter,
              child: Text(
                I18n.of(context).vaultsSwitchingBottomShtMiniTitle,
                style: TextStyle(
                  color: UIConstants.grayDarkest100,
                  fontSize: width * 0.06,
                  fontWeight: FontWeight.bold,
                ),
              ),
            ),
          ),

          Padding(
            padding: EdgeInsets.only(top: height * 0.08),
            child: Column(
              children: [
                showStewardConnections(width, height),
              ],
            ),
          ),
        ],
      ),
      heightRatio: 0.55,
    );
  }

  /// Shows stewardship connections.
  Widget showStewardConnections(double width, double height) {
    var children = <Widget>[];
    var connections = SessionNext().get<List<VaultSwitchItemViewModel>>('switchConnections') ?? [];

    for (var element in connections) {
      children.add(
        const Divider(height: 1),
      );
      children.add(Padding(
        padding: EdgeInsets.symmetric(vertical: height * 0.01),
        child: Material(
          color: UIConstants.primaryColor,
          child: InkWell(
            splashColor: UIConstants.blackFaded,
            onTap: () {
              // Update session because user selected another connection
              SessionNext().set<int>('connectionId', element.connectionId);

              // when owner selects own vault
              if (element.connectionId == 0) {
                Navigator.pushNamed(
                  context,
                  '/dashboard',
                  arguments: {'activeTab': 0},
                );
              }
              // User selected someone elses' vault to look at, so we show an info screen in between
              else {
                Navigator.pushNamed(context, '/vault/switch/info', arguments: {
                  'contractId': element.connectionId,
                  'name': element.name,
                });
              }
            },
            child: Padding(
              padding: const EdgeInsets.symmetric(horizontal: 0.0),
              child: ListTile(
                leading: ProviderLogo(
                  isOrg: false,
                  logoColor: Color(int.parse(element.color)),
                  logoLetters: _initialsPickerUtil.getInitials(element.name),
                ),
                title: Column(
                  mainAxisAlignment: MainAxisAlignment.start,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text(
                      element.name,
                      style: TextStyle(
                        fontFamily: UIConstants.defaultFontFamily,
                        fontSize: width * 0.055,
                        fontWeight: FontWeight.w700,
                      ),
                    ),
                    element.connectionId != 0
                        ? Text(
                            I18n.of(context).vaultsSwitchingBottomShtTileListSubtitle,
                            style: TextStyle(
                              color: UIConstants.slateGray100,
                              fontSize: width * 0.035,
                              fontWeight: FontWeight.w400,
                            ),
                          )
                        : Container(),
                  ],
                ),
              ),
            ),
          ),
        ),
      ));
    }

    return Column(children: children);
  }
}
