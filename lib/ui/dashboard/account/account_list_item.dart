import 'package:flutter/material.dart';
import 'package:schluss_beta_app/constants/ui_constants.dart';
import 'package:schluss_beta_app/ui/dashboard/base.dart';
import 'package:schluss_beta_app/ui/reused_widgets/divider_strap.dart';
import 'package:schluss_beta_app/ui/reused_widgets/switch_button.dart';

class AccountListItem extends StatelessWidget {
  final bool centerAligned;
  final String? imagePath;
  final String text;
  final bool hasArrow;
  final bool? isSwitchBtnOn;
  final Function? switchBtnCall;
  final String descText1;
  final String descText2;
  final Function? call;

  const AccountListItem({
    this.centerAligned = false,
    this.imagePath,
    required this.text,
    this.hasArrow = true,
    this.isSwitchBtnOn = false,
    this.switchBtnCall,
    this.descText1 = '',
    this.descText2 = '',
    this.call,
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    double width = MediaQuery.of(context).size.width;
    double height = MediaQuery.of(context).size.height;

    return Material(
      color: UIConstants.primaryColor,
      child: Column(
        children: [
          InkWell(
            splashColor: UIConstants.blackFaded,
            onTap: call as void Function()?,
            child: SizedBox(
              height: height * 0.085,
              child: Padding(
                padding: EdgeInsets.symmetric(horizontal: getPadding(context)),
                child: centerAligned
                    ? Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          Text(
                            text,
                            style: TextStyle(
                              color: UIConstants.accentColor,
                              fontSize: width * 0.045,
                            ),
                          ),
                        ],
                      )
                    : Row(
                        mainAxisAlignment: MainAxisAlignment.start,
                        children: [
                          Image(
                            image: AssetImage(imagePath!),
                            height: height * 0.03,
                            width: height * 0.03,
                          ),
                          Padding(
                            padding: EdgeInsets.only(left: getPadding(context)),
                            child: Text(
                              text,
                              style: TextStyle(
                                fontSize: width * 0.045,
                                color: UIConstants.grayDarkest100,
                              ),
                            ),
                          ),
                          const Spacer(),
                          hasArrow
                              ? Icon(
                                  Icons.keyboard_arrow_right,
                                  color: UIConstants.gray100,
                                  size: MediaQuery.of(context).size.width * 0.065,
                                )
                              : switchBtnCall != null
                                  ? Padding(
                                      padding: const EdgeInsets.only(right: 8.0),
                                      child: GestureDetector(
                                        onTap: call as void Function()?,
                                        child: SwitchButton(
                                          isSwitchOn: isSwitchBtnOn!,
                                          callback: switchBtnCall,
                                        ),
                                      ),
                                    )
                                  : Container(),
                        ],
                      ),
              ),
            ),
          ),
          if (descText1 != '')
            DividerStrap(
              hasBorder: false,
              textPart1: descText1,
              textPart2: descText2,
              textAlign: TextAlign.left,
            ),
        ],
      ),
    );
  }
}
