import 'package:flutter/material.dart';
import 'package:schluss_beta_app/constants/ui_constants.dart';
import 'package:schluss_beta_app/translations/i18n.dart';
import 'package:schluss_beta_app/ui/reused_widgets/main_button.dart';
import 'package:schluss_beta_app/ui/reused_widgets/top_logo.dart';

class PinChangingDonePage extends StatelessWidget {
  const PinChangingDonePage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    double width = MediaQuery.of(context).size.width;
    double height = MediaQuery.of(context).size.height;
    double topPadding = MediaQuery.of(context).padding.top;

    return Scaffold(
      backgroundColor: UIConstants.primaryColor,
      body: SizedBox(
        height: height,
        width: width,
        child: Stack(
          children: <Widget>[
            Padding(
              padding: EdgeInsets.only(
                bottom: height * 0.075,
                right: width * 0.08,
                left: width * 0.08,
              ),
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  /// Image.
                  Align(
                    alignment: Alignment.center,
                    child: Image(
                      image: const AssetImage('assets/images/intro_3.png'),
                      width: height * 0.3,
                    ),
                  ),

                  /// Main text.
                  Padding(
                    padding: EdgeInsets.only(
                      top: height * 0.05,
                      bottom: height * 0.0175,
                    ),
                    child: Text(
                      I18n.of(context).pinChangedDoneTitle,
                      textAlign: TextAlign.center,
                      style: TextStyle(
                        color: UIConstants.grayDarkest100,
                        fontSize: MediaQuery.of(context).size.width * 0.07,
                        fontWeight: FontWeight.bold,
                      ),
                    ),
                  ),

                  /// Description.
                  Text(
                    I18n.of(context).pinChangedDoneTxt,
                    textAlign: TextAlign.center,
                    style: TextStyle(
                      color: UIConstants.slateGray100,
                      fontSize: MediaQuery.of(context).size.width * 0.048,
                    ),
                  ),
                ],
              ),
            ),

            /// Logo.
            TopLogo(
              imgPath: 'assets/images/logo.svg',
              imgWidth: width * 0.142,
              topPadding: topPadding + height * 0.05,
            ),

            /// Main button.
            Padding(
              padding: EdgeInsets.only(
                right: width * 0.07,
                left: width * 0.07,
                bottom: height * 0.05,
              ),
              child: Align(
                alignment: Alignment.bottomCenter,
                child: MainButton(
                  type: BtnType.active,
                  text: I18n.of(context).btnContinueTxt,
                  call: () => Navigator.pushNamed(context, '/account/login'),
                ),
              ),
            )
          ],
        ),
      ),
    );
  }
}
