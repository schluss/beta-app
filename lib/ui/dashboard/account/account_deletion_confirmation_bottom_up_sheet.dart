import 'package:flutter/material.dart';
import 'package:schluss_beta_app/translations/i18n.dart';
import 'package:schluss_beta_app/ui/reused_widgets/confirmation_bottom_up_sheet_contents_template.dart';

class AccountDeletionConfirmationBottomUpSheet extends StatelessWidget {
  const AccountDeletionConfirmationBottomUpSheet({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return ConfirmationBottomUpSheetContentsTemplate(
      title: I18n.of(context).accountDelBottomShtTitle,
      mainText: I18n.of(context).accountDelBottomShtTxt,
      activeBtnText: I18n.of(context).accountDelBottomShtBtnTxt,
      activeBtnCall: () => Navigator.pushNamed(context, '/account/deletion/confirm-final'),
    );
  }
}
