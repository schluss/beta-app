import 'dart:async';

import 'package:flutter/material.dart';
import 'package:schluss_beta_app/constants/ui_constants.dart';
import 'package:schluss_beta_app/controller/user_feedback_controller.dart';
import 'package:schluss_beta_app/translations/i18n.dart';
import 'package:schluss_beta_app/ui/reused_widgets/bottom_up_sheet_template.dart';
import 'package:schluss_beta_app/ui/reused_widgets/main_button.dart';
import 'package:schluss_beta_app/ui/reused_widgets/tabs_bar.dart';
import 'package:schluss_beta_app/util/error_handler_util.dart';

enum TabType { issue, generic, genericDashboard }

class FeedbackBottomUpSheet extends StatefulWidget {
  final TabType tabType;

  const FeedbackBottomUpSheet({
    required this.tabType,
    Key? key,
  }) : super(key: key);

  @override
  FeedbackBottomUpSheetState createState() => FeedbackBottomUpSheetState();
}

class FeedbackBottomUpSheetState extends State<FeedbackBottomUpSheet> with SingleTickerProviderStateMixin {
  final UserFeedbackController _userFeedbackController = UserFeedbackController();
  final TextEditingController _feedbackTextController = TextEditingController();
  final List<Tab> dataTabs = <Tab>[];
  final ErrorHandlerUtil _errorHandler = ErrorHandlerUtil();

  TabType? tabType;
  TabController? _tabController;
  int? _currentIndex;
  String? _feedbackType;
  String? feedbackText;
  bool _isSendingInitiated = false;
  bool _isBtnEnabled = false;
  bool? response;

  @override
  void initState() {
    super.initState();

    tabType = widget.tabType;
    _tabController = TabController(vsync: this, length: 2);
    _loadsRelevantTab();
    _tabController!.animation!.addListener(_handleTabSelection);
  }

  /// Loads relevant tab.
  void _loadsRelevantTab() {
    _tabController!.index = tabType == TabType.issue ? 0 : 1;
  }

  /// Handles Tab change event.
  void _handleTabSelection() {
    if (_tabController!.indexIsChanging && _currentIndex != _tabController!.index) {
      setState(() => _currentIndex = _tabController!.index);
    }
  }

  @override
  Widget build(BuildContext context) {
    /// Sets tab names.
    dataTabs.clear();
    dataTabs.add(Tab(text: I18n.of(context).feedbackTabProbHeader));
    dataTabs.add(Tab(text: I18n.of(context).feedbackTabIdeaHeader));
    // Set width.
    double width = MediaQuery.of(context).size.width;
    // Set height.
    double height = MediaQuery.of(context).size.height;

    return BottomUpSheetTemplate(
      miniTitle: I18n.of(context).feedbackTitle,
      page: Column(
        children: [
          /// Pages tabs section.
          SizedBox(
            height: height * 0.30,
            child: Column(
              children: [
                Expanded(
                  child: Container(
                    color: UIConstants.primaryColor,
                    child: DefaultTabController(
                      length: dataTabs.length,
                      child: Column(
                        children: [
                          /// Tab bar.
                          Container(
                            decoration: const BoxDecoration(
                              color: UIConstants.primaryColor,
                              border: Border(
                                bottom: BorderSide(
                                  color: UIConstants.grayLight,
                                  width: 1,
                                ),
                              ),
                            ),
                            child: TabsBar(
                              tabs: dataTabs,
                              tabController: _tabController!,
                              align: Alignment.center,
                            ),
                          ),

                          /// Tabs body view.
                          Expanded(
                            child: TabBarView(
                              controller: _tabController,
                              children: <Widget>[
                                /// Problems tab body.
                                TextField(
                                  autofocus: true,
                                  controller: _feedbackTextController,
                                  keyboardType: TextInputType.multiline,
                                  maxLines: 11,
                                  decoration: InputDecoration.collapsed(
                                    hintText: I18n.of(context).feedbackTabProbHintTxt,
                                  ),
                                  onChanged: (text) => text.isNotEmpty ? _enableMainButton() : _disableMainButton(),
                                ),

                                /// Ideas tab body.
                                TextField(
                                  autofocus: true,
                                  controller: _feedbackTextController,
                                  keyboardType: TextInputType.multiline,
                                  maxLines: 11,
                                  decoration: InputDecoration.collapsed(
                                    hintText: I18n.of(context).feedbackTabIdeaHintTxt,
                                  ),
                                  onChanged: (text) => text.isNotEmpty ? _enableMainButton() : _disableMainButton(),
                                ),
                              ],
                            ),
                          ),
                        ],
                      ),
                    ),
                  ),
                ),
              ],
            ),
          ),

          /// Pages main button layout.
          Container(
            color: UIConstants.primaryColor,
            child: Padding(
              padding: EdgeInsets.only(
                top: width * 0.050,
              ),
              child: Align(
                alignment: Alignment.topCenter,
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.end,
                  children: <Widget>[
                    _buildMainButton()!,
                  ],
                ),
              ),
            ),
          ),
        ],
      ),
    );
  }

  /// Enables the main button.
  void _enableMainButton() => setState(() => _isBtnEnabled = true);

  /// Disables the main button.
  void _disableMainButton() => setState(() => _isBtnEnabled = false);

  /// Builds Page main button.
  MainButton? _buildMainButton() {
    MainButton? button;

    // If there is no text and the main button is not enabled.
    if (!_isBtnEnabled) {
      button = MainButton(
        type: BtnType.disabled,
        text: I18n.of(context).feedbackBtnTxt,
        call: () {},
      );
    } else {
      // If main button is enabled and the main button is not clicked.
      if (!_isSendingInitiated) {
        button = MainButton(
          type: BtnType.active,
          text: I18n.of(context).feedbackBtnTxt,
          call: () => _onMainBtnClick(),
          isDisabled: true,
        );
      } else if (_isSendingInitiated) {
        // If the main button is clicked and the response from server is not received yet.
        button = MainButton(
          type: BtnType.awaited,
          text: I18n.of(context).btnSendTxt,
          call: () {},
        );
      }
    }
    return button;
  }

  /// Handles main button function.
  void _onMainBtnClick() {
    FocusScope.of(context).unfocus();

    _tabController!.index == 0 ? _feedbackType = 'bug' : _feedbackType = 'idea';
    tabType == TabType.genericDashboard ? _feedbackType = 'dashboard improvement' : _feedbackType = _feedbackType;
    feedbackText = _feedbackTextController.text.toString();
    _sendFeedback(_feedbackType, feedbackText);

    setState(() => _isSendingInitiated = true);
  }

  /// Sends feedback to the server.
  Future _sendFeedback(String? feedbackType, String? feedbackText) async {
    try {
      response = await _userFeedbackController.sendUserFeedback(
        feedbackType,
        feedbackText,
      );
    } catch (e) {
      _errorHandler.handleError(e.toString(), true, context);
    }

    // If feedback is successfully sent to the server and the response from server is received.
    if (response == true) {
      _feedbackTextController.clear();

      if (!mounted) return;
      Navigator.pushNamed(
        context,
        '/account/feedback/done',
      );
    }
  }
}
