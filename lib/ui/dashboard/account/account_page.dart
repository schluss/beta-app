import 'package:flutter/material.dart';
import 'package:schluss_beta_app/constants/environment_constants.dart';
import 'package:schluss_beta_app/constants/ui_constants.dart';
import 'package:schluss_beta_app/controller/account_controller.dart';
import 'package:schluss_beta_app/translations/i18n.dart';
import 'package:schluss_beta_app/ui/dashboard/account/account_list_item.dart';
import 'package:schluss_beta_app/ui/reused_widgets/dashboard_page_template.dart';
import 'package:schluss_beta_app/ui/reused_widgets/divider_strap.dart';
import 'package:schluss_beta_app/ui/reused_widgets/pin_verification_bottom_up_sheet.dart';
import 'package:schluss_beta_app/util/error_handler_util.dart';
import 'package:schluss_beta_app/util/package_info_util.dart';
import 'package:schluss_beta_app/view_model/settings_view_model.dart';

class AccountPage extends StatefulWidget {
  const AccountPage({Key? key}) : super(key: key);

  @override
  AccountPageState createState() => AccountPageState();
}

class AccountPageState extends State<AccountPage> {
  final AccountController _accountController = AccountController();
  final ErrorHandlerUtil _errorHandler = ErrorHandlerUtil();

  SettingsViewModel? _settings;

  bool? _isStewardshipModeOn = false;

  bool? _isTestModeOn = false;
  String _envMode = EnvironmentConstants.development;
  String? _versionNo = '0.0.0';
  String? _version;

  @override
  void initState() {
    super.initState();

    _setSettings();

    _getVersionNo();
  }

  /// Sets test-mode settings.
  void _setSettings() async {
    _settings = await _accountController.getSettings();
    _envMode = await _accountController.getEnvMode();

    if (_settings != null) {
      setState(() {
        _isStewardshipModeOn = _settings!.stewardshipMode;
        _isTestModeOn = _settings!.testMode;
      });
    }
  }

  /// Gets the package version number.
  void _getVersionNo() async {
    _version = await PackageInfoUtil.getVersion();
    setState(() => _versionNo = _version);
  }

  @override
  Widget build(BuildContext context) {
    double height = MediaQuery.of(context).size.height;
    double width = MediaQuery.of(context).size.width;

    // Header.
    return DashboardPageTemplate(
      text: I18n.of(context).accountTitle,
      appBody: buildBody(width, height),
    );
  }

  Widget buildBody(double width, double height) {
    return Scaffold(
      body: Container(
        color: UIConstants.paleGray,
        height: height,
        width: width,
        child: SingleChildScrollView(
          child: Column(
            children: [
              /// Divider.
              const DividerStrap(),

              /// Promise raw.
              AccountListItem(
                imagePath: 'assets/images/handshake_icon.png',
                text: I18n.of(context).accountPromiseTxt,
                call: () => Navigator.pushNamed(context, '/account/promise'), // Pop-up promise bottom-up sheet.
              ),

              /// Divider.
              const DividerStrap(),

              /// Change PIN raw.
              AccountListItem(
                imagePath: 'assets/images/lock_icon.png',
                text: I18n.of(context).accountChangePINTxt,
                call: () => Navigator.pushNamed(
                  context,
                  '/account/verify',
                  arguments: {'processType': ProcessType.changePin},
                ), // Pops-up PIN bottom-up sheet.
              ),

              /// Divider line.
              const Divider(height: 1),

              /// Log out raw.
              AccountListItem(
                imagePath: 'assets/images/exit_icon.png',
                text: I18n.of(context).accountLogoutTxt,
                call: _logout,
                hasArrow: false,
              ),

              /// Divider.
              const DividerStrap(),

              /// Report problem raw.
              AccountListItem(
                imagePath: 'assets/images/issue_icon.png',
                text: I18n.of(context).accountFeedbackProbTxt,
                call: () => Navigator.pushNamed(context, '/account/feedback/problem'), // Pop-up feedback(bug/problem) bottom-up sheet.
              ),

              /// Divider line.
              const Divider(height: 1),

              /// Feedback raw.
              AccountListItem(
                imagePath: 'assets/images/light_icon.png',
                text: I18n.of(context).accountFeedbackTxt,
                call: () => Navigator.pushNamed(context, '/account/feedback/idea'), // Pops-up feedback(brilliant idea) bottom-up sheet.
              ),

              /// Divider.
              const DividerStrap(),

              /// Export data raw.
              // This raw is hidden since we have no restore option yet.
              // Other UIs and functionalities of this process are available and not commented.
              /* AccountListItem(
                imagePath: 'assets/images/backup.png',
                text: I18n.of(context).accountExportTxt,
                call: () => Navigator.pushNamed(
                  context,
                  '/account/export-data/confirm',
                ), // Pop-up export data bottom-up sheet.
              ), */

              _toggleListItems(),

              /// Removes vault raw.
              AccountListItem(
                centerAligned: true,
                text: I18n.of(context).accountDelTxt,
                call: () => Navigator.pushNamed(
                  context,
                  '/account/verify',
                  arguments: {'processType': ProcessType.deleteAccount},
                ), // Pop-up PIN bottom-up sheet.
              ),

              /// Divider line.
              const Divider(height: 1),

              /// Version raw.
              Padding(
                padding: EdgeInsets.only(bottom: height * 0.03),
                child: DividerStrap(
                  hasBorder: false,
                  textPart1: I18n.of(context).accountVersionTxt,
                  textPart2: _versionNo,
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }

  /// Logs out.
  void _logout() {
    try {
      _accountController.logout();
    } catch (e) {
      _errorHandler.handleError(e.toString(), false, context, 'log-out');
    }
  }

  /// Switches stewardship-mode on/off.
  void _switchStewardshipMode(bool isOn) async {
    try {
      setState(() => _isStewardshipModeOn = isOn);

      await _accountController.changeStewardshipMode(_isStewardshipModeOn);
    } catch (e) {
      _errorHandler.handleError(e.toString(), false, context);
    }
  }

  /// Switches data sharing-mode on/off.
  // TODO: data sharing interconnect with stewardship.
  // Functionalities of this process are available and not commented.
  /* void _switchDataSharingMode(bool isOn) async {
    try {
      setState(() => _isDataSharingModeOn = isOn);

      await _accountController.changeDataSharingMode(_isDataSharingModeOn);
    } catch (e) {
      _errorHandler.handleError(e.toString(), false, context);
    }
  } */

  /// Switches test-mode on/off.
  void _switchTestMode(bool isOn) async {
    try {
      setState(() => _isTestModeOn = isOn);

      await _accountController.changeTestMode(_isTestModeOn);
    } catch (e) {
      _errorHandler.handleError(e.toString(), false, context);
    }
  }

  /// Returns toggle items or not, depending on [development] or [production] mode.
  Widget _toggleListItems() {
    if (_envMode == 'development') {
      return Column(children: [
        /// Divider.
        const DividerStrap(),

        /// Stewardship-mode on/off raw.
        AccountListItem(
          imagePath: 'assets/images/test_mode_icon.png',
          text: I18n.of(context).accountStewardshipModeTxt,
          hasArrow: false,
          isSwitchBtnOn: _isStewardshipModeOn,
          switchBtnCall: _switchStewardshipMode,
        ),

        /// Divider line.
        const Divider(height: 1),

        /// Data sharing-mode on/off raw.
        // TODO: data sharing interconnect with stewardship.
        // This raw is hidden until interconnect with stewardship process.
        /* AccountListItem(
          imagePath: 'assets/images/test_mode_icon.png',
          text: I18n.of(context).accountDataSharingModeTxt,
          hasArrow: false,
          isSwitchBtnOn: _isDataSharingModeOn,
          switchBtnCall: _switchDataSharingMode,
        ), */

        /// Divider line.
        const Divider(height: 1),

        /// Test-mode on/off raw.
        AccountListItem(
          imagePath: 'assets/images/test_mode_icon.png',
          text: I18n.of(context).accountTestModeTxt,
          hasArrow: false,
          isSwitchBtnOn: _isTestModeOn,
          switchBtnCall: _switchTestMode,
          descText1: I18n.of(context).accountTestModeDesTxt,
        ),

        /// Divider line.
        const Divider(height: 1),
      ]);
    }

    return const Column();
  }
}
