import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:schluss_beta_app/constants/ui_constants.dart';
import 'package:schluss_beta_app/ui/intro/promise_content.dart';
import 'package:schluss_beta_app/ui/reused_widgets/bottom_up_sheet_template.dart';
import 'package:schluss_beta_app/ui/reused_widgets/closing_button.dart';

class PromiseBottomUpSheet extends StatelessWidget {
  const PromiseBottomUpSheet({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    double height = MediaQuery.of(context).size.height;

    return BottomUpSheetTemplate(
      isHeaderNeeded: false,
      page: Stack(
        children: [
          /// Close button.
          ClosingButton(
            backgroundColor: UIConstants.primaryColor,
            iconColor: UIConstants.accentColor,
            call: () => Navigator.pop(context),
          ),

          /// Logo.
          Padding(
            padding: EdgeInsets.only(top: height * 0.05),
            child: const PromiseContent(),
          ),
        ],
      ),
      heightRatio: 0.12,
    );
  }
}
