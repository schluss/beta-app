import 'package:flutter/material.dart';
import 'package:logger/logger.dart';
import 'package:schluss_beta_app/constants/ui_constants.dart';
import 'package:schluss_beta_app/controller/account_controller.dart';
import 'package:schluss_beta_app/singleton_injector.dart';
import 'package:schluss_beta_app/translations/i18n.dart';
import 'package:schluss_beta_app/ui/reused_widgets/bottom_up_sheet_template.dart';
import 'package:schluss_beta_app/ui/reused_widgets/main_button.dart';
import 'package:schluss_beta_app/ui/reused_widgets/main_title.dart';
import 'package:schluss_beta_app/util/error_handler_util.dart';

class AccountDeletionFinalConfirmationBottomUpSheet extends StatefulWidget {
  const AccountDeletionFinalConfirmationBottomUpSheet({Key? key}) : super(key: key);

  @override
  AccountDeletionFinalConfirmationBottomUpSheetState createState() => AccountDeletionFinalConfirmationBottomUpSheetState();
}

class AccountDeletionFinalConfirmationBottomUpSheetState extends State<AccountDeletionFinalConfirmationBottomUpSheet> {
  final AccountController _accountController = AccountController();
  final ErrorHandlerUtil _errorHandler = ErrorHandlerUtil();

  final _confirmController = TextEditingController();
  final Logger _logger = singletonInjector<Logger>();
  String? _confirmText;
  bool _isMatched = false;

  @override
  Widget build(BuildContext context) {
    double width = MediaQuery.of(context).size.width;
    double height = MediaQuery.of(context).size.height;

    _confirmText = I18n.of(context).accountDelConfirmBottomShtMatchTxt;

    return BottomUpSheetTemplate(
      page: Column(
        mainAxisAlignment: MainAxisAlignment.start,
        crossAxisAlignment: CrossAxisAlignment.start,
        mainAxisSize: MainAxisSize.max,
        children: [
          /// Title.
          Padding(
            padding: EdgeInsets.only(bottom: height * 0.02),
            child: MainTitle(
              text: I18n.of(context).accountDelConfirmBottomShtTitle,
              textAlign: TextAlign.left,
            ),
          ),

          /// Description.
          Padding(
            padding: EdgeInsets.only(bottom: height * 0.035),
            child: Text(
              I18n.of(context).accountDelConfirmBottomShtTxt(
                deleteText: _confirmText!,
              ),
              style: TextStyle(
                color: UIConstants.slateGray100,
                fontSize: width * 0.045,
              ),
            ),
          ),

          /// Text box.
          Padding(
            padding: EdgeInsets.only(bottom: height * 0.035),
            child: TextFormField(
              controller: _confirmController,
              onChanged: (value) {
                setState(
                  () => _isMatched = value.trim() == _confirmText ? true : false,
                );
              },
              autofocus: true,
              maxLength: 100,
              maxLines: 1,
              textCapitalization: TextCapitalization.characters,
              textAlign: TextAlign.start,
              style: TextStyle(fontSize: width * 0.05),
              cursorColor: UIConstants.gray100,
              decoration: InputDecoration(
                counterText: '',
                fillColor: UIConstants.paleGray,
                filled: true,
                contentPadding: EdgeInsets.symmetric(
                  vertical: height * 0.002,
                  horizontal: height * 0.02,
                ),
                focusedBorder: _buildOutlineInputBorder(),
                enabledBorder: _buildOutlineInputBorder(),
              ),
              keyboardType: TextInputType.text,
            ),
          ),

          /// Submit button.
          MainButton(
            type: _isMatched ? BtnType.active : BtnType.disabled,
            text: I18n.of(context).accountDelConfirmBottomShtBtnTxt,
            call: _isMatched ? _onConfirmClick : () {},
          ),
        ],
      ),
      heightRatio: Theme.of(context).platform == TargetPlatform.iOS ? 0.4 : 0.20,
    );
  }

  /// Builds outline input border.
  OutlineInputBorder _buildOutlineInputBorder() {
    return OutlineInputBorder(
      borderSide: const BorderSide(color: UIConstants.grayLight, width: 0.5),
      borderRadius: BorderRadius.circular(12.0),
    );
  }

  /// Deletes the vault.
  void _onConfirmClick() async {
    if (_confirmText == _confirmController.text.trim()) {
      late bool isDeleted;

      try {
        isDeleted = await _accountController.deleteVault();
      } catch (e) {
        _errorHandler.handleError(e.toString(), false, context, 'account-del');
      }

      if (isDeleted) {
        _logger.i('Deleting vault success!');
        if (!mounted) return;
        Navigator.pushNamedAndRemoveUntil(
          context,
          '/intro',
          (Route<dynamic> route) => false, // Clean the navigation history.
        );
      } else {
        _logger.e('Error occurred while deleting vault.');
      }
    } else {
      _logger.e('Wrong Confirm Text: ${_confirmController.text}');
    }
  }
}
