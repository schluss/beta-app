import 'package:flutter/material.dart';
import 'package:flutter_dotenv/flutter_dotenv.dart';
import 'package:schluss_beta_app/constants/ui_constants.dart';
import 'package:schluss_beta_app/translations/i18n.dart';
import 'package:schluss_beta_app/ui/reused_widgets/user_feature_page_contents_template.dart';

class AddIdentity extends StatefulWidget {
  const AddIdentity({Key? key}) : super(key: key);

  @override
  State<AddIdentity> createState() => _AddIdentityState();
}

class _AddIdentityState extends State<AddIdentity> {
  final String? mijnOverheidConfigUrl = dotenv.env['MIJNOVERHEID_CONFIG_URL'];

  @override
  void initState() {
    super.initState();

    // TODO: although this is not perfect: tip is deleted already before adding
    //  identity is done it is better than always staying there possible
    //  solution: make a possibility to redirect after finishing a data request,
    //  so then we can redirect the user to a finish screen where the tip then
    //  is deleted Remove the add identity tip from dashboard.
    //  singletonInjector<NotificationBL>().deleteNotificationsByKey('tipIdentityVerifyTitleTxt');
  }

  @override
  Widget build(BuildContext context) {
    double width = MediaQuery.of(context).size.width;
    double height = MediaQuery.of(context).size.height;

    return UserFeaturePageContentsTemplate(
      miniTitle: I18n.of(context).identityConfirmationBottomShtMiniTitle,
      title: I18n.of(context).identityConfirmationBottomShtTitle,
      para1TextPart1: I18n.of(context).identityConfirmationBottomShtPara1Text,
      para2Text: I18n.of(context).identityConfirmationBottomShtPara2Text,
      hasDivider: true,
      middleImage: Align(
        alignment: Alignment.centerLeft,
        child: Image(
          image: const AssetImage('assets/images/rijksoverheid_logo.png'),
          height: height * 0.1,
        ),
      ),
      cardHeaderText: I18n.of(context).identityConfirmationBottomShtCardHeaderTxt,
      cardContent: Row(
        children: [
          Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Text(
                I18n.of(context).identityConfirmationBottomShtCardTxt1,
                style: TextStyle(
                  color: UIConstants.gray100,
                  fontSize: width * 0.045,
                ),
              ),
              Text(
                I18n.of(context).identityConfirmationBottomShtCardTxt2,
                style: TextStyle(
                  color: UIConstants.gray100,
                  fontSize: width * 0.045,
                ),
              ),
              Text(
                I18n.of(context).identityConfirmationBottomShtCardTxt3,
                style: TextStyle(
                  color: UIConstants.gray100,
                  fontSize: width * 0.045,
                ),
              ),
            ],
          ),
        ],
      ),
      activeBtnText: I18n.of(context).identityConfirmationBottomShtMainBtnTxt,
      activeBtnCall: () => Navigator.pushNamed(
        context,
        '/data-request',
        arguments: {
          'configUrl': mijnOverheidConfigUrl,
        },
      ),
    );
  }
}
