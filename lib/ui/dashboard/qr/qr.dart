import 'package:dotted_border/dotted_border.dart';
import 'package:flutter/material.dart';
import 'package:logger/logger.dart';
import 'package:mobile_scanner/mobile_scanner.dart';
import 'package:schluss_beta_app/constants/ui_constants.dart';
import 'package:schluss_beta_app/controller/data_share_controller.dart';
import 'package:schluss_beta_app/singleton_injector.dart';
import 'package:schluss_beta_app/translations/i18n.dart';
import 'package:schluss_beta_app/ui/dashboard/qr/qr_scanner_overlay.dart';
import 'package:schluss_beta_app/util/common_util.dart';
import 'package:schluss_beta_app/view_model/gateway_viewmodel.dart';

class QRPage extends StatefulWidget {
  const QRPage({
    Key? key,
  }) : super(key: key);

  @override
  State<StatefulWidget> createState() => _QRPageState();
}

class _QRPageState extends State<QRPage> {
  final GlobalKey qrKey = GlobalKey(debugLabel: 'QR');
  final CommonUtil _commonUtil = CommonUtil();
  final Logger _logger = singletonInjector<Logger>();

  bool hasFound = false;

  @override
  void initState() {
    super.initState();
  }

  @override
  void dispose() {
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    double width = MediaQuery.of(context).size.width;
    double height = MediaQuery.of(context).size.height;

    return Scaffold(
      body: Stack(
        children: [
          SizedBox(
            width: width,
            height: height,
            child: MobileScanner(onDetect: (barcode) {
              if (barcode.barcodes.isEmpty || barcode.barcodes.first.rawValue == null) {
                throw ('Invalid QR code link');
              } else {
                _onQRViewCreated(barcode.barcodes.first.rawValue!);
              }
            }),
          ),

          /// Creates color overlay around the screen.
          Container(
              constraints: const BoxConstraints.expand(),
              decoration: ShapeDecoration(
                shape: QrScannerOverlayShape(
                  cutOutSize: MediaQuery.of(context).size.width - width * .29,
                  borderColor: UIConstants.grayDarkest48,
                  borderRadius: 10,
                ),
              )),
          Center(
            child: (hasFound)
                ? _buildDashedContainer(
                    width,
                    height,
                    0,
                    2,
                    UIConstants.greenLight,
                  )
                : _buildDashedContainer(
                    width,
                    height,
                    15.0,
                    3,
                    UIConstants.grayDarkest20,
                  ),
          ),
          _buildCloseButton(context, width, height),
          _buildMainButton(context, width, height),
        ],
      ),
    );
  }

  /// Scans QR code.
  // TODO: refactor this so there is a better switch between the different type
  //  of actions done by the different incoming QR deeplink urls, for example:
  //  [https://schluss.org/redirect/connect/].. -> data request
  //  [https://schluss.org/redirect/connectuser/].. -> users connecting vaults
  //  [https://schluss.org/redirect/connectsteward/].. -> users connecting vaults and appointing a steward.
  //  Also: this logic should not be in the UI.
  void _onQRViewCreated(String qrCode) async {
    Uri url = Uri();

    try {
      url = Uri.parse(qrCode);

      _logger.i('Validating ink: $url');
    } catch (e) {
      throw ('Invalid QR code link');
    }

    // Update state.
    setState(() => hasFound = true);

    // Data request.
    if (url.pathSegments.contains('connect')) {
      await _commonUtil.processConfigurationLink(
        qrCode,
        true,
        context,
      );
    }

    // Connecting vaults
    else if (url.pathSegments.contains('connectuser')) {
      GatewayViewModel? gatewayViewModel = await DataShareController().processQRData(
        qrCode,
      );
      if (!mounted) return;
      Navigator.popAndPushNamed(
        context,
        '/data-sharing/agreement/receiver',
        arguments: {'gatewayViewModel': gatewayViewModel},
      );
    }

    // Connecting vaults and appointing a steward.
    else if (url.pathSegments.contains('connectsteward')) {
      GatewayViewModel? gatewayViewModel = await DataShareController().processQRData(qrCode);
      if (!mounted) return;
      Navigator.popAndPushNamed(
        context,
        '/stewardship/steward/agreement',
        arguments: {'gatewayViewModel': gatewayViewModel},
      );
    } else {
      throw ('Invalid QR code link');
    }
  }

  /// Builds dashed container.
  DottedBorder _buildDashedContainer(
    double width,
    double height,
    double borderDashGap,
    double borderThickness,
    Color color,
  ) {
    return DottedBorder(
      borderType: BorderType.RRect,
      radius: const Radius.circular(10),
      dashPattern: [18, borderDashGap],
      strokeWidth: borderThickness,
      color: color,
      padding: const EdgeInsets.all(0),
      child: ClipRRect(
        borderRadius: const BorderRadius.all(Radius.circular(10)),
        child: SizedBox(width: width * 0.7, height: width * 0.7),
      ),
    );
  }

  /// Builds close-button.
  GestureDetector _buildCloseButton(
    BuildContext context,
    double width,
    double height,
  ) {
    return GestureDetector(
      onTap: () => Navigator.pop(context),
      child: Padding(
        padding: EdgeInsets.only(
          top: height * 0.075,
          right: width * 0.05,
        ),
        child: Align(
          alignment: Alignment.topRight,
          child: Container(
            decoration: const BoxDecoration(
              shape: BoxShape.circle,
              color: UIConstants.grayLight,
            ),
            padding: const EdgeInsets.all(8),
            child: Icon(
              Icons.close,
              color: UIConstants.gray100,
              size: width * 0.05,
            ),
          ),
        ),
      ),
    );
  }

  /// Builds main-button.
  GestureDetector _buildMainButton(
    BuildContext context,
    double width,
    double height,
  ) {
    return GestureDetector(
      onTap: () {},
      child: Padding(
        padding: EdgeInsets.only(
          bottom: height * 0.07,
          left: width * 0.085,
          right: width * 0.085,
        ),
        child: Align(
          alignment: Alignment.bottomCenter,
          child: Container(
            width: width,
            height: height * 0.11,
            decoration: const BoxDecoration(
              shape: BoxShape.rectangle,
              borderRadius: BorderRadius.all(Radius.circular(50)),
              color: UIConstants.grayDarkest40,
            ),
            alignment: Alignment.bottomCenter,
            padding: const EdgeInsets.all(4),
            child: Align(
              alignment: Alignment.center,
              child: (hasFound)
                  ? _getButtonText(width, I18n.of(context).qrDoneBtnTxt)
                  : Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: <Widget>[
                        Image(
                          image: const AssetImage('assets/images/qr_code_icon.png'),
                          width: width * 0.07,
                        ),
                        Padding(
                          padding: EdgeInsets.only(left: width * 0.02),
                          child: _getButtonText(
                            width,
                            I18n.of(context).qrBtnTxt,
                          ),
                        ),
                      ],
                    ),
            ),
          ),
        ),
      ),
    );
  }

  /// Returns text of main-button.
  Text _getButtonText(double width, String text) {
    return Text(
      text,
      style: TextStyle(
        color: UIConstants.primaryColor,
        fontSize: width * 0.05,
      ),
    );
  }
}
