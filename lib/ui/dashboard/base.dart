import 'package:flutter/material.dart';
import 'package:flutter_dotenv/flutter_dotenv.dart';
import 'package:schluss_beta_app/constants/ui_constants.dart';
import 'package:schluss_beta_app/translations/i18n.dart';
import 'package:schluss_beta_app/ui/dashboard/account/account_page.dart';
import 'package:schluss_beta_app/ui/dashboard/contracts/contracts_page.dart';
import 'package:schluss_beta_app/ui/dashboard/data/data_page.dart';
import 'package:schluss_beta_app/ui/dashboard/home/home.dart';
import 'package:schluss_beta_app/util/common_util.dart';
import 'package:session_next/session_next.dart';

class Base extends StatefulWidget {
  final String? title;
  final int? selectedIndexPage;

  const Base({
    Key? key,
    this.title,
    this.selectedIndexPage,
  }) : super(key: key);

  @override
  BaseState createState() => BaseState();
}

class BaseState extends State<Base> {
  final CommonUtil _commonUtil = CommonUtil();

  int? _selectedIndex = 0;
  Widget _currentPage = const Home();

  bool _isOwner = true;

  @override
  void initState() {
    super.initState();

    setState(() {
      var connectionId = SessionNext().get<int>('connectionId') ?? 0;
      // When connectionId == 0, this is the owner looking at its own vault
      _isOwner = connectionId == 0;
    });

    // Set selected nav bar item index, if it is not [null].
    if (widget.selectedIndexPage != null) {
      setState(() => _onItemTapped(widget.selectedIndexPage));
    }
  }

  void _onItemTapped(int? pageIndex) {
    setState(() {
      switch (pageIndex) {
        // Set values of currently showing.
        case 0:
          _currentPage = const Home();
          _selectedIndex = pageIndex;
          break;
        case 1:
          // TODO: remove this unwanted codes, after completely integrating the functionalities.
          //  To enable data screen: uncomment line:53 and comment line:54.
          //  To enable new data screen: uncomment line:54 and comment line:53.
          _currentPage = const DataPage();
          //_currentPage = const NewDataPage();
          _selectedIndex = pageIndex;
          break;
        case 3:
          if (_isOwner) {
            _currentPage = const ContractsPage();
            _selectedIndex = pageIndex;
          }
          break;
        case 4:
          if (_isOwner) {
            _currentPage = const AccountPage();
            _selectedIndex = pageIndex;
          }
          break;
      }
    });
  }

  @override
  Widget build(BuildContext context) {
    double width = MediaQuery.of(context).size.width;

    return Theme(
      data: ThemeData(
        splashColor: UIConstants.transparent,
        highlightColor: UIConstants.transparent,
        primaryColor: UIConstants.primaryColor,
        fontFamily: UIConstants.defaultFontFamily,
        colorScheme: ColorScheme.fromSwatch().copyWith(
          secondary: UIConstants.accentColor,
        ),
      ),
      child: Stack(
        children: <Widget>[
          Scaffold(
            bottomNavigationBar: BottomNavigationBar(
              type: BottomNavigationBarType.fixed,
              items: _buildNavigationBarItems(
                context,
                isActualVaultOwner: _isOwner,
              ), // Set list of navigation bar items.
              showUnselectedLabels: true,
              iconSize: width * 0.08,
              selectedFontSize: width * 0.032,
              unselectedFontSize: width * 0.032,
              currentIndex: _selectedIndex!,
              unselectedItemColor: UIConstants.gray100,
              selectedItemColor: UIConstants.accentColor,
              onTap: _onItemTapped,
            ),
            body: _currentPage,
          ),
          GestureDetector(
            onTap: () {
              _processLoadingDataRequest();
            },
            child: buildQRButton(context),
          )
        ],
      ),
    );
  }

  /// Processes loading data request.
  void _processLoadingDataRequest() {
    String? qrOverrideUrl = dotenv.env['OPTIONAL_QR_OVERRIDE_URL'];

    // If there is an override URL defined, use that in stead.
    if (qrOverrideUrl != null && Uri.parse(qrOverrideUrl).isAbsolute) {
      _commonUtil.processConfigurationLink(qrOverrideUrl, true, context);
    }

    // Otherwise start the QR code scanner.
    else {
      Navigator.pushNamed(context, '/qr-scan');
    }
  }

  /// Builds QR code button.
  Padding buildQRButton(BuildContext context) {
    double width = MediaQuery.of(context).size.width;

    return Padding(
      padding: EdgeInsets.only(
        bottom: MediaQuery.of(context).padding.bottom * 0.9 + width * 0.01,
      ),
      child: Align(
        alignment: Alignment.bottomCenter,
        child: Container(
          width: width * 0.1675,
          height: width * 0.1675,
          alignment: Alignment.center,
          decoration: const BoxDecoration(
            shape: BoxShape.circle,
            color: UIConstants.grayDarkest100,
          ),
          child: Image(
            image: const AssetImage('assets/images/qr_code_icon.png'),
            color: Theme.of(context).primaryColor,
            width: width * 0.08,
          ),
        ),
      ),
    );
  }
}

/// Builds bottom navigation bar items.
List<BottomNavigationBarItem> _buildNavigationBarItems(BuildContext context, {bool isActualVaultOwner = true}) {
  // List of nav bar items.
  List<BottomNavigationBarItem> list = [];

  // Home.
  list.add(
    BottomNavigationBarItem(
      label: I18n.of(context).homeTitle,
      icon: const ImageIcon(AssetImage('assets/images/home_icon.png')),
    ),
  );

  // Data.
  list.add(
    BottomNavigationBarItem(
      label: I18n.of(context).dataTitle,
      icon: const ImageIcon(AssetImage('assets/images/file_icon.png')),
    ),
  );

  // QR code.
  list.add(
    const BottomNavigationBarItem(
      icon: Icon(Icons.crop_free, size: 0),
      label: '',
    ),
  );

  // Contract.
  if (isActualVaultOwner) {
    list.add(
      BottomNavigationBarItem(
        label: I18n.of(context).contractsTitle,
        icon: const ImageIcon(AssetImage('assets/images/connection_icon.png')),
      ),
    );
  } else {
    list.add(
      BottomNavigationBarItem(
        label: I18n.of(context).contractsTitle,
        icon: const ImageIcon(
          AssetImage('assets/images/connection_icon.png'),
          color: UIConstants.paleLilac,
        ),
      ),
    );
  }

  // Account.
  if (isActualVaultOwner) {
    list.add(
      BottomNavigationBarItem(
        label: I18n.of(context).accountTitle,
        icon: const ImageIcon(AssetImage('assets/images/account_icon.png')),
      ),
    );
  } else {
    list.add(
      BottomNavigationBarItem(
        label: I18n.of(context).accountTitle,
        icon: const ImageIcon(
          AssetImage('assets/images/account_icon.png'),
          color: UIConstants.paleLilac,
        ),
      ),
    );
  }

  return list;
}

/// Gets the padding.
double getPadding(BuildContext context) {
  return (MediaQuery.of(context).size.width -
          2 *
              MediaQuery.of(
                context,
              ).size.width *
              0.44) /
      3;
}
