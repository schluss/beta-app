import 'package:flutter/material.dart';
import 'package:schluss_beta_app/translations/i18n.dart';
import 'package:schluss_beta_app/ui/dashboard/contracts/connection_delete_dialog_template.dart';

class ConnectionSharedDataDeleteBottomUpSheet extends StatefulWidget {
  final String? userName;
  final String? dataCount;

  const ConnectionSharedDataDeleteBottomUpSheet({
    this.userName,
    this.dataCount,
    Key? key,
  }) : super(key: key);

  @override
  ConnectionSharedDataDeleteBottomUpSheetState createState() => ConnectionSharedDataDeleteBottomUpSheetState();
}

class ConnectionSharedDataDeleteBottomUpSheetState extends State<ConnectionSharedDataDeleteBottomUpSheet> {
  @override
  Widget build(BuildContext context) {
    return ConnectionDeleteBottomUpSheetTemplate(
      title: widget.userName,
      delBtnTxt: I18n.of(context).connectionSharedDataDeleteBottomShtDelBtnTxt,
      whitishBtnTxt: I18n.of(context).connectionDelBottomShtCancelBtnTxt,
    );
  }
}
