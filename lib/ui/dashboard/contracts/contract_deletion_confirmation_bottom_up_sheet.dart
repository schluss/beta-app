import 'package:flutter/material.dart';
import 'package:schluss_beta_app/constants/helper_constants.dart';
import 'package:schluss_beta_app/controller/contract_controller.dart';
import 'package:schluss_beta_app/translations/i18n.dart';
import 'package:schluss_beta_app/ui/reused_widgets/confirmation_bottom_up_sheet_contents_template.dart';
import 'package:schluss_beta_app/util/error_handler_util.dart';

class ContractDeletionConfirmationBottomUpSheet extends StatefulWidget {
  final int? contractId;
  final String? fromUI;

  const ContractDeletionConfirmationBottomUpSheet({
    Key? key,
    this.contractId,
    this.fromUI,
  }) : super(key: key);

  @override
  State<ContractDeletionConfirmationBottomUpSheet> createState() => _ContractDeletionConfirmationBottomUpSheetState();
}

class _ContractDeletionConfirmationBottomUpSheetState extends State<ContractDeletionConfirmationBottomUpSheet> {
  final ContractController _contractController = ContractController();

  final ErrorHandlerUtil _errorHandler = ErrorHandlerUtil();

  @override
  Widget build(BuildContext context) {
    return ConfirmationBottomUpSheetContentsTemplate(
      title: I18n.of(context).contractInfoDelBottomShtTitle,
      mainText: I18n.of(context).contractInfoDelBottomShtTxt,
      activeBtnText: I18n.of(context).contractInfoDelBottomShtBtnTxt,
      activeBtnCall: () => _onDeleteContractConfirmed(),
    );
  }

  void _onDeleteContractConfirmed() async {
    try {
      await _contractController.deleteContractById(widget.contractId);
    } catch (e) {
      _errorHandler.handleError(e.toString(), false, context);
    }
    if (!mounted) return;
    if (widget.fromUI == HelperConstant.fromContractUI) {
      Navigator.pushNamed(
        context,
        '/dashboard',
        arguments: {'activeTab': 3, 'isActualVaultOwner': true},
      );
    } else if (widget.fromUI == HelperConstant.fromDataUI) {
      Navigator.pushNamed(
        context,
        '/dashboard',
        arguments: {'activeTab': 1, 'isActualVaultOwner': true},
      );
    } else {
      Navigator.pushNamed(
        context,
        '/dashboard',
        arguments: {'activeTab': 0, 'isActualVaultOwner': true},
      );
    }
  }
}
