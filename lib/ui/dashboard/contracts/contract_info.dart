import 'package:flutter/material.dart';
import 'package:schluss_beta_app/constants/ui_constants.dart';
import 'package:schluss_beta_app/controller/contract_controller.dart';
import 'package:schluss_beta_app/translations/i18n.dart';
import 'package:schluss_beta_app/ui/reused_widgets/data_list_item.dart';
import 'package:schluss_beta_app/ui/reused_widgets/group_header.dart';
import 'package:schluss_beta_app/ui/reused_widgets/mini_header_template.dart';
import 'package:schluss_beta_app/ui/reused_widgets/provider_logo.dart';
import 'package:schluss_beta_app/ui/reused_widgets/provider_retrieval_info.dart';
import 'package:schluss_beta_app/util/error_handler_util.dart';
import 'package:schluss_beta_app/view_model/contract_detail_view_model.dart';

class ContractInfo extends StatefulWidget {
  final ContractDetailViewModel? contractDetailViewModel;
  final int? contractId;
  final int? selectedIndex;
  const ContractInfo({
    Key? key,
    this.contractDetailViewModel,
    this.contractId,
    this.selectedIndex,
  }) : super(key: key);

  @override
  ContractInfoState createState() => ContractInfoState();
}

class ContractInfoState extends State<ContractInfo> {
  final ErrorHandlerUtil _errorHandler = ErrorHandlerUtil();
  final ContractController _contractController = ContractController();
  ContractDetailViewModel? contractDetailViewModelDecrypted = ContractDetailViewModel();

  var count = 0;

  @override
  void initState() {
    initScreenData();
    super.initState();
  }

  /// Initializes data elements in screen.
  void initScreenData() async {
    ContractDetailViewModel? contractViewmodel;

    try {
      contractViewmodel = await _contractController.getContractDetailsDecrypted(
        widget.contractDetailViewModel,
      );
    } catch (e) {
      _errorHandler.handleError(e.toString(), false, context);
    }

    setState(() {
      contractDetailViewModelDecrypted = contractViewmodel;
    });
  }

  @override
  Widget build(BuildContext context) {
    double width = MediaQuery.of(context).size.width;
    double height = MediaQuery.of(context).size.height;
    double topPadding = MediaQuery.of(context).padding.top;

    return Container(
      color: UIConstants.paleGray,
      height: height,
      width: width,
      child: Stack(
        children: <Widget>[
          SingleChildScrollView(
            child: Container(
              height: height,
              width: width,
              color: UIConstants.paleGray,
              child: ListView(
                padding: EdgeInsets.only(
                  top: height * 0.085 + topPadding,
                  bottom: height * 0.2,
                ),
                children: <Widget>[
                  Container(
                    color: UIConstants.primaryColor,
                    child: Stack(
                      children: <Widget>[
                        ProviderRetrievalInfo(
                          title: I18n.of(context).providerInfoSrcTxt,
                          subTitle: widget.contractDetailViewModel!.requesterName!,
                          heightScale: 0.08,
                          isSubTitleBold: true,
                        ),
                        Align(
                          alignment: Alignment.centerRight,
                          child: Padding(
                            padding: EdgeInsets.only(right: width * 0.05),
                            child: ProviderLogo(
                              logoImg: widget.contractDetailViewModel!.providerLogo!,
                            ),
                          ),
                        ),
                      ],
                    ),
                  ),

                  ProviderRetrievalInfo(
                    title: I18n.of(context).providerInfoDateTxt,
                    subTitle: widget.contractDetailViewModel!.providerDate!,
                    isSubTitleBold: true,
                  ),
                  SizedBox(height: height * 0.012),
                  // Pass in the method below a DataDetailViewModel instance.
                  ...buildChildren(
                    contractDetailViewModelDecrypted!,
                    width,
                    height,
                    context,
                  ),
                ],
              ),
            ),
          ),

          /// Mini title with back button.
          MiniHeaderTemplate(
            text: I18n.of(context).providerInfoTitle,
            hasBackBtn: true,
          ),
        ],
      ),
    );
  }
}

/// Builds to show list of attributes values.
List<Widget> buildChildren(
  ContractDetailViewModel model,
  double width,
  double height,
  BuildContext context,
) {
  List<Widget> children;
  children = <Widget>[];

  if (model.dataMap != null) {
    // Parse map without groups.
    if (model.dataMap!.keys.length == 1 && model.dataMap!.keys.first!.isEmpty) {
      children.add(const GroupHeader(text: ''));
      for (var list in model.dataMap!.values) {
        {
          for (var element in list) {
            children.add(DataListItem(
              model: element,
              isDecryptionNeeded: true,
            ));
            children.add(const Divider(height: 1));
          }
        }
      }
    } else {
      // Parse map with groups.
      model.dataMap!.forEach((key, value) {
        children.add(GroupHeader(text: key!.toUpperCase()));

        for (var element in value) {
          children.add(DataListItem(
            model: element,
            isDecryptionNeeded: true,
          ));
        }
      });
    }
  }
  return children;
}
