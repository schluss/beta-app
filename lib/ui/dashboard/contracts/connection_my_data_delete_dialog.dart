import 'package:flutter/material.dart';
import 'package:schluss_beta_app/translations/i18n.dart';
import 'package:schluss_beta_app/ui/dashboard/contracts/connection_delete_dialog_template.dart';

class ConnectionMyDataDeleteBottomUpSheet extends StatefulWidget {
  final String? userName;
  final String? dataCount;
  final VoidCallback? deleteCallback;

  const ConnectionMyDataDeleteBottomUpSheet({
    this.userName,
    this.dataCount,
    this.deleteCallback,
    Key? key,
  }) : super(key: key);

  @override
  ConnectionMyDataDeleteBottomUpSheetState createState() => ConnectionMyDataDeleteBottomUpSheetState();
}

class ConnectionMyDataDeleteBottomUpSheetState extends State<ConnectionMyDataDeleteBottomUpSheet> {
  @override
  Widget build(BuildContext context) {
    return ConnectionDeleteBottomUpSheetTemplate(
      title: widget.userName,
      delBtnTxt: I18n.of(context).connectionMyDataDeleteBottomShtDelBtnTxt,
      whitishBtnTxt: I18n.of(context).connectionDelBottomShtCancelBtnTxt,
      deleteCallback: widget.deleteCallback,
    );
  }
}
