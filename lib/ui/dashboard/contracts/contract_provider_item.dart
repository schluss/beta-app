import 'package:flutter/material.dart';
import 'package:schluss_beta_app/constants/ui_constants.dart';
import 'package:schluss_beta_app/ui/reused_widgets/provider_logo.dart';
import 'package:schluss_beta_app/view_model/provider_data_model.dart';

class ContractProviderItem extends StatefulWidget {
  final ProviderDataModel? providerDataItem;
  final String? description;

  const ContractProviderItem(
    this.providerDataItem,
    this.description, {
    Key? key,
  }) : super(key: key);

  @override
  ContractProviderItemState createState() => ContractProviderItemState();
}

class ContractProviderItemState extends State<ContractProviderItem> {
  @override
  Widget build(BuildContext context) {
    double width = MediaQuery.of(context).size.width;

    return Container(
      decoration: BoxDecoration(
        border: Border(
          top: buildBorderSide(),
          bottom: buildBorderSide(),
        ),
      ),
      padding: EdgeInsets.symmetric(
        horizontal: width * 0.05,
        vertical: width * 0.03,
      ),
      child: Row(
        crossAxisAlignment: CrossAxisAlignment.center,
        children: <Widget>[
          /// Logo.
          Expanded(
            flex: 2,
            child: ProviderLogo(
              logoImg: widget.providerDataItem!.imagePath!,
              isChecked: widget.providerDataItem!.isChecked,
            ),
          ),
          Expanded(
            flex: 7,
            child: Column(
              mainAxisAlignment: MainAxisAlignment.start,
              mainAxisSize: MainAxisSize.max,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                /// Title.
                buildConstrainedBox(
                  context,
                  widget.providerDataItem!.title!,
                  TextStyle(
                    color: UIConstants.grayDarkest100,
                    fontWeight: FontWeight.w600,
                    fontStyle: FontStyle.normal,
                    letterSpacing: 0,
                    fontSize: width * 0.045,
                  ),
                ),

                /// Subtitle.
                buildConstrainedBox(
                  context,
                  widget.providerDataItem!.subTitle!,
                  TextStyle(
                    fontFamily: UIConstants.subFontFamily,
                    color: UIConstants.gray100,
                    fontWeight: FontWeight.w400,
                    fontStyle: FontStyle.normal,
                    letterSpacing: 0,
                    fontSize: width * 0.042,
                  ),
                ),

                /// Description.
                widget.description!.isEmpty == false
                    ? Padding(
                        padding: EdgeInsets.only(top: width * 0.02),
                        child: buildConstrainedBox(
                          context,
                          widget.description!,
                          TextStyle(
                            color: UIConstants.grayDarkest100,
                            fontWeight: FontWeight.w400,
                            fontStyle: FontStyle.normal,
                            letterSpacing: 0,
                            fontSize: width * 0.045,
                          ),
                        ),
                      )
                    : Container(),
              ],
            ),
          ),

          /// Arrow head.
          Expanded(
            flex: 1,
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.end,
              mainAxisAlignment: MainAxisAlignment.end,
              children: <Widget>[
                Container(
                  padding: EdgeInsets.only(left: width * 0.05),
                  child: Icon(
                    Icons.keyboard_arrow_right,
                    color: UIConstants.gray100,
                    size: width * 0.055,
                  ),
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }

  BorderSide buildBorderSide() => const BorderSide(
        color: UIConstants.grayLight,
        width: 0.5,
      );

  ConstrainedBox buildConstrainedBox(
    BuildContext context,
    String text,
    TextStyle textStyle,
  ) {
    double width = MediaQuery.of(context).size.width;

    return ConstrainedBox(
      constraints: BoxConstraints(maxWidth: width * 0.7),
      child: Text(
        text,
        softWrap: true,
        overflow: TextOverflow.ellipsis,
        style: textStyle,
      ),
    );
  }
}
