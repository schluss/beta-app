import 'package:flutter/material.dart';
import 'package:schluss_beta_app/business/impl/my_data_bl_impl.dart';
import 'package:schluss_beta_app/constants/helper_constants.dart';
import 'package:schluss_beta_app/constants/ui_constants.dart';
import 'package:schluss_beta_app/controller/contract_controller.dart';
import 'package:schluss_beta_app/controller/my_data_controller.dart';
import 'package:schluss_beta_app/singleton_injector.dart';
import 'package:schluss_beta_app/translations/i18n.dart';
import 'package:schluss_beta_app/ui/dashboard/contracts/contracts_page.dart';
import 'package:schluss_beta_app/ui/reused_widgets/bottom_up_sheet_template.dart';
import 'package:schluss_beta_app/ui/reused_widgets/provider_logo.dart';
import 'package:schluss_beta_app/ui/reused_widgets/providers_list_item.dart';
import 'package:schluss_beta_app/ui/reused_widgets/tabs_bar.dart';
import 'package:schluss_beta_app/util/error_handler_util.dart';
import 'package:schluss_beta_app/util/initials_picker_util.dart';
import 'package:schluss_beta_app/view_model/contract_detail_view_model.dart';
import 'package:schluss_beta_app/view_model/contract_providers_view_model.dart';
import 'package:schluss_beta_app/view_model/field_view_model.dart';

class ConnectionPage extends StatefulWidget {
  final int? contractId;
  final String? fromUI;
  final String? userName;
  final String? sharedDatCount;
  final bool isSender;
  final bool isReceiver;

  const ConnectionPage({
    this.contractId,
    this.fromUI,
    this.userName,
    this.sharedDatCount,
    required this.isSender,
    required this.isReceiver,
    Key? key,
  }) : super(key: key);

  @override
  ConnectionPageState createState() => ConnectionPageState();
}

class ConnectionPageState extends State<ConnectionPage> with SingleTickerProviderStateMixin {
  ContractProvidersViewModel? contractProvidersViewModel = ContractProvidersViewModel();

  final ContractController _contractController = ContractController();
  final ErrorHandlerUtil _errorHandler = ErrorHandlerUtil();
  final InitialsPickerUtil _initialsPickerUtil = InitialsPickerUtil();
  final MyDataController _myDataController = MyDataController();
  final List<Tab> dataTabs = <Tab>[];
  TabController? _tabController;
  ConnectionScreenIdentifier screenId = ConnectionScreenIdentifier.myData;
  int _currentIndex = 0;
  List<FieldViewModel> sharedDataFromVault = [];
  List<FieldViewModel> sharedDataWithVault = [];

  var count = 0;
  bool isLoading = true;

  Color? logoColor;

  @override
  void initState() {
    _initScreenData();

    _tabController = TabController(vsync: this, length: 2);
    _tabController!.animation!.addListener(_handleTabSelection);

    super.initState();
  }

  /// Initializes data elements in screen.
  void _initScreenData() async {
    ContractProvidersViewModel? contractViewmodel;
    isLoading = true;

    try {
      contractViewmodel = await _contractController.getContractById(
        widget.contractId,
      );

      setState(() {
        contractProvidersViewModel = contractViewmodel;

        logoColor = Color(int.parse(contractProvidersViewModel!.iconColor!));

        if (contractProvidersViewModel!.providerList != null) {
          count = contractProvidersViewModel!.providerList!.length;
        }
      });

      sharedDataFromVault = await _contractController.getSharedDataFromVault(widget.contractId);
      sharedDataWithVault = await _contractController.getSharedDataWithVault(widget.contractId);

      setState(() => isLoading = false);
    } catch (e) {
      _errorHandler.handleError(e.toString(), false, context);
    }
  }

  /// Handles tab change event.
  void _handleTabSelection() {
    if (_tabController!.indexIsChanging && _currentIndex != _tabController!.index) {
      setState(() {
        _currentIndex = _tabController!.index;
        if (_currentIndex == 0) {
          screenId = ConnectionScreenIdentifier.myData;
        } else {
          screenId = ConnectionScreenIdentifier.sharedData;
        }
      });
    }
  }

  @override
  void dispose() {
    _tabController!.dispose();
    if (!mounted) return;
    super.dispose();
  }

  /// Sets tab bar.
  void _setUpTabBar(BuildContext context) {
    dataTabs.clear();
    dataTabs.add(Tab(text: I18n.of(context).connectionInfoTabMyDataHeader));
    dataTabs.add(Tab(text: I18n.of(context).connectionInfoTabSharedDataHeader));
  }

  /// Decrypts data.
  void decryptData(FieldViewModel fieldViewModel) async {
    isLoading = true;
    fieldViewModel = await _myDataController.getDataDetailsDecrypted(fieldViewModel);
    setState(() => isLoading = false);
  }

  /// When the cancel button is clicked on the select data screens.
  void _onShareCancel() {
    return;
  }

  /// When the share button is pushed from the select data screens, this method
  /// is fired and the selected attributes are shared.
  void _onShareFinish(List<int> attributeIds) async {
    var result = await singletonInjector<MyDataBLImpl>().shareAttributesWithClient(
      attributeIds,
      widget.contractId!,
      false,
    );

    // Refresh the outgoing shares.
    if (result) {
      Future.delayed(const Duration(seconds: 1), () async {
        var updatedShares = await _contractController.getSharedDataFromVault(
          widget.contractId,
        );

        setState(() => sharedDataFromVault = updatedShares);
      });
    }
  }

  /// Deletes connection.
  void _deleteConnection() async {
    try {
      await _contractController.deleteConnectionById(widget.contractId);
    } catch (e) {
      _errorHandler.handleError(e.toString(), false, context);
    }

    if (!mounted) return;

    if (widget.fromUI == HelperConstant.fromContractUI) {
      Navigator.pushNamed(
        context,
        '/dashboard',
        arguments: {'activeTab': 3, 'isActualVaultOwner': true},
      );
    } else if (widget.fromUI == HelperConstant.fromDataUI) {
      Navigator.pushNamed(
        context,
        '/dashboard',
        arguments: {'activeTab': 1, 'isActualVaultOwner': true},
      );
    } else {
      Navigator.pushNamed(
        context,
        '/dashboard',
        arguments: {'activeTab': 0, 'isActualVaultOwner': true},
      );
    }
  }

  /// Deletes shared attribute with other parties.
  void deleteSharedAttribute(String gatewayShareId, int attributeId) async {
    bool status = await _contractController.deleteOutgoingSharedAttribute(
      gatewayShareId,
      widget.contractId!,
      attributeId,
    );

    if (!mounted) return;

    if (status) {
      Navigator.pop(context);
      _initScreenData();
    }
  }

  @override
  Widget build(BuildContext context) {
    double width = MediaQuery.of(context).size.width;
    double height = MediaQuery.of(context).size.height;

    _setUpTabBar(context);

    return BottomUpSheetTemplate(
      backBtnText: I18n.of(context).contractsTitle,
      hasCloseBtn: false,
      addBtnCall: () {
        Navigator.pushNamed(
          context,
          '/data/select/groups',
          arguments: {
            'connectionId': widget.contractId,
            'onCancel': _onShareCancel,
            'onShare': _onShareFinish,
          },
        );
      },
      optionsBtnCall: () {
        Navigator.pushNamed(
          context,
          '/contracts/connection/delete',
          arguments: {
            'userName': contractProvidersViewModel!.requesterName,
            'dataCount': sharedDataFromVault.length,
            'deleteCallback': _deleteConnection,
          },
        );
      },
      isScrollable: true,
      page: Column(
        children: [
          Container(
            color: UIConstants.primaryColor,
            child: Column(
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              children: <Widget>[
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: <Widget>[
                    /// Logo.
                    Expanded(
                      flex: 2,
                      child: ProviderLogo(
                        isOrg: false,
                        logoColor: logoColor,
                        logoLetters: _initialsPickerUtil
                            .getInitials(
                              contractProvidersViewModel!.requesterName ?? '',
                            )
                            .toString(),
                      ),
                    ),

                    /// Title.
                    Expanded(
                      flex: 7,
                      child: Text(
                        contractProvidersViewModel!.requesterName ?? '',
                        maxLines: 1,
                        textAlign: TextAlign.start,
                        overflow: TextOverflow.ellipsis,
                        softWrap: true,
                        style: TextStyle(
                          fontWeight: FontWeight.w700,
                          color: UIConstants.grayDarkest100,
                          fontSize: width * 0.07,
                        ),
                      ),
                    ),
                  ],
                ),

                /// Stewardship info.
                widget.isSender || widget.isReceiver
                    ? Padding(
                        padding: EdgeInsets.only(top: height * 0.02),
                        child: GestureDetector(
                          onTap: () => widget.isReceiver
                              ? contractProvidersViewModel!.isReleased
                                  ? Navigator.pushNamed(
                                      context,
                                      '/stewardship/steward/access',
                                      arguments: {
                                        'connectionId': widget.contractId,
                                        'senderName': contractProvidersViewModel!.requesterName,
                                        'hasAccess': contractProvidersViewModel!.isReleased,
                                      },
                                    )
                                  : Navigator.pushNamed(
                                      context,
                                      '/stewardship/steward/authorize',
                                      arguments: {
                                        'senderName': contractProvidersViewModel!.requesterName,
                                        'connectionId': widget.contractId,
                                      },
                                    )
                              : Navigator.pushNamed(
                                  context,
                                  '/stewardship/owner/authorize',
                                  arguments: {
                                    'receiverName': contractProvidersViewModel!.requesterName,
                                    'connectionId': widget.contractId,
                                  },
                                ),
                          child: Container(
                            width: width,
                            padding: const EdgeInsets.all(16.0),
                            decoration: BoxDecoration(
                              color: widget.isReceiver ? UIConstants.pastelBlue : UIConstants.pastelPink,
                              borderRadius: BorderRadius.circular(8.0),
                            ),
                            child: Column(
                              mainAxisAlignment: MainAxisAlignment.spaceAround,
                              children: [
                                Row(
                                  mainAxisAlignment: MainAxisAlignment.start,
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  mainAxisSize: MainAxisSize.max,
                                  children: [
                                    Padding(
                                      padding: EdgeInsets.only(
                                        top: height * 0.005,
                                        right: 10.0,
                                      ),
                                      child: Image(
                                        image: AssetImage(
                                          widget.isReceiver ? 'assets/images/stewardship_blue_icon.png' : 'assets/images/stewardship_pink_icon.png',
                                        ),
                                      ),
                                    ),
                                    Flexible(
                                      child: Text(
                                        widget.isReceiver
                                            ? I18n.of(context).connectionOwnerNotifyTitle(
                                                userName: contractProvidersViewModel!.requesterName ?? '',
                                              )
                                            : I18n.of(context).connectionStewardNotifyTitle(
                                                userName: contractProvidersViewModel!.requesterName ?? '',
                                              ),
                                        softWrap: true,
                                        style: TextStyle(
                                          fontSize: width * 0.040,
                                          fontWeight: FontWeight.w600,
                                          fontStyle: FontStyle.normal,
                                          color: UIConstants.grayDarkest100,
                                          letterSpacing: 0,
                                        ),
                                      ),
                                    )
                                  ],
                                ),
                                Padding(
                                  padding: EdgeInsets.only(top: height * 0.01),
                                  child: Row(
                                    mainAxisAlignment: MainAxisAlignment.start,
                                    crossAxisAlignment: CrossAxisAlignment.center,
                                    children: [
                                      Flexible(
                                        child: Text(
                                          widget.isReceiver
                                              ? contractProvidersViewModel!.isReleased
                                                  ? I18n.of(context).connectionOwnerNotifyIsReleasedDescTxt(
                                                      userName: contractProvidersViewModel!.requesterName ?? '',
                                                    )
                                                  : I18n.of(context).connectionOwnerNotifyDescTxt(
                                                      userName: contractProvidersViewModel!.requesterName ?? '',
                                                    )
                                              : I18n.of(context).connectionStewardNotifyDescTxt(
                                                  userName: contractProvidersViewModel!.requesterName ?? '',
                                                ),
                                          softWrap: true,
                                          style: TextStyle(
                                            fontSize: width * 0.040,
                                            fontWeight: FontWeight.w400,
                                            fontStyle: FontStyle.normal,
                                            color: UIConstants.grayDarkest100,
                                            letterSpacing: 0,
                                          ),
                                        ),
                                      ),
                                    ],
                                  ),
                                ),
                                Padding(
                                  padding: EdgeInsets.only(top: height * 0.01),
                                  child: Row(
                                    mainAxisAlignment: MainAxisAlignment.start,
                                    crossAxisAlignment: CrossAxisAlignment.center,
                                    mainAxisSize: MainAxisSize.max,
                                    children: [
                                      Text(
                                        widget.isReceiver ? I18n.of(context).connectionOwnerNotifyLinkTxt : I18n.of(context).connectionStewardNotifyLinkTxt,
                                        softWrap: true,
                                        style: TextStyle(
                                          fontSize: width * 0.045,
                                          fontWeight: FontWeight.w400,
                                          fontStyle: FontStyle.normal,
                                          color: widget.isReceiver ? UIConstants.blue : UIConstants.rosyPink,
                                          letterSpacing: 0,
                                        ),
                                      )
                                    ],
                                  ),
                                ),
                              ],
                            ),
                          ),
                        ),
                      )
                    : Container(),

                /// Details Header.
                Padding(
                  padding: EdgeInsets.only(top: height * 0.02),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: <Widget>[
                      Text(
                        I18n.of(context).connectionInfoGeneralDetailsTxt,
                        maxLines: 1,
                        textAlign: TextAlign.start,
                        overflow: TextOverflow.ellipsis,
                        softWrap: true,
                        style: TextStyle(
                          fontFamily: UIConstants.subFontFamily,
                          color: UIConstants.gray100,
                          fontWeight: FontWeight.w400,
                          fontStyle: FontStyle.normal,
                          letterSpacing: 1,
                          fontSize: width * 0.045,
                        ),
                      ),
                    ],
                  ),
                ),

                /// Divider.
                const Divider(height: 28),

                /// Created date.
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: <Widget>[
                    /// Label.
                    Expanded(
                      flex: 5,
                      child: Text(
                        I18n.of(context).connectionInfoDateTxt,
                        maxLines: 1,
                        textAlign: TextAlign.start,
                        overflow: TextOverflow.ellipsis,
                        softWrap: true,
                        style: TextStyle(
                          fontFamily: UIConstants.subFontFamily,
                          color: UIConstants.gray100,
                          fontWeight: FontWeight.w400,
                          fontStyle: FontStyle.normal,
                          letterSpacing: 0,
                          fontSize: width * 0.040,
                        ),
                      ),
                    ),

                    /// Value.
                    Expanded(
                      flex: 5,
                      child: Text(
                        contractProvidersViewModel!.requestDate ?? '',
                        maxLines: 1,
                        textAlign: TextAlign.end,
                        overflow: TextOverflow.ellipsis,
                        softWrap: true,
                        style: TextStyle(
                          fontWeight: FontWeight.w700,
                          color: UIConstants.grayDarkest100,
                          fontSize: width * 0.048,
                        ),
                      ),
                    ),
                  ],
                ),
              ],
            ),
          ),

          /// Tab container.
          Container(
            margin: EdgeInsets.only(top: height * 0.02),
            height: height - height * 0.56,
            color: UIConstants.primaryColor,
            child: DefaultTabController(
              length: dataTabs.length,
              child: Column(
                children: [
                  /// Tab bar.
                  Container(
                    decoration: const BoxDecoration(
                      color: UIConstants.primaryColor,
                      border: Border(
                        bottom: BorderSide(
                          color: UIConstants.grayLight,
                          width: 1,
                        ),
                      ),
                    ),
                    child: TabsBar(
                      tabs: dataTabs,
                      tabController: _tabController!,
                      align: Alignment.center,
                    ),
                  ),

                  // Tab body view.
                  Expanded(
                    child: TabBarView(
                      controller: _tabController,
                      children: <Widget>[
                        // Data which I shared with the connected client.
                        sharedDataFromVault.isNotEmpty
                            ? ListView.builder(
                                padding: EdgeInsets.zero,
                                itemCount: sharedDataFromVault.length,
                                itemBuilder: (BuildContext context, int index) {
                                  var field = sharedDataFromVault[index];
                                  return Material(
                                    color: UIConstants.primaryColor,
                                    child: InkWell(
                                      splashColor: UIConstants.blackFaded,
                                      onTap: () => Navigator.pushNamed(
                                        context,
                                        '/contracts/connection/my-data/delete',
                                        arguments: {
                                          'userName': field.label ?? field.name,
                                          'dataCount': '0',
                                          'deleteCallback': () {
                                            deleteSharedAttribute(
                                              field.gatewayShareId!,
                                              field.attributeId!,
                                            );
                                          },
                                        },
                                      ),
                                      child: ProvidersListItem(
                                        title: field.label ?? field.name!,
                                        subTitle: field.providerName!,
                                        logo: field.logoUrl!,
                                      ),
                                    ),
                                  );
                                },
                              )
                            : Container(),
                        // Data the other client shares with me.
                        sharedDataWithVault.isNotEmpty
                            ? ListView.builder(
                                controller: ScrollController(),
                                padding: EdgeInsets.zero,
                                shrinkWrap: true,
                                itemCount: sharedDataWithVault.length,
                                scrollDirection: Axis.vertical,
                                itemBuilder: (BuildContext context, int index) {
                                  var field = sharedDataWithVault[index];
                                  return Column(children: <Widget>[
                                    IgnorePointer(
                                      ignoring: field.isHideValue,
                                      child: ExpansionTile(
                                        onExpansionChanged: (isExpanded) {
                                          if (isExpanded) {
                                            decryptData(field);
                                          }
                                        },
                                        title: ProvidersListItem(
                                          title: field.label ?? field.name!,
                                          subTitle: field.providerName ?? '',
                                          logo: field.logoUrl!,
                                          hasArrow: false,
                                        ),
                                        trailing: field.isHideValue ? const Icon(Icons.lock_outline) : const Icon(Icons.lock_open),
                                        childrenPadding: const EdgeInsets.only(bottom: 10),
                                        maintainState: true,
                                        tilePadding: EdgeInsets.only(right: width * 0.075),
                                        controlAffinity: ListTileControlAffinity.trailing,
                                        children: [
                                          const Divider(),
                                          Container(
                                            alignment: Alignment.center,
                                            child: Text(
                                              field.value ?? ' - ',
                                              maxLines: 1,
                                              textAlign: TextAlign.start,
                                              overflow: TextOverflow.ellipsis,
                                              softWrap: true,
                                              style: TextStyle(
                                                fontFamily: UIConstants.subFontFamily,
                                                color: UIConstants.black,
                                                fontWeight: FontWeight.w400,
                                                fontStyle: FontStyle.normal,
                                                letterSpacing: 1,
                                                fontSize: width * 0.055,
                                              ),
                                            ),
                                          )
                                        ],
                                      ),
                                    ),
                                    const Divider()
                                  ]);
                                },
                              )
                            : Container(),
                      ],
                    ),
                  ),
                ],
              ),
            ),
          ),
        ],
      ),
    );
  }

  void providerClicked(ContractDetailViewModel contractDetailViewModel) {
    Navigator.pushNamed(
      context,
      '/contracts/contract/info',
      arguments: contractDetailViewModel,
    );
  }
}
