import 'package:flutter/material.dart';
import 'package:schluss_beta_app/constants/ui_constants.dart';
import 'package:schluss_beta_app/controller/contract_controller.dart';
import 'package:schluss_beta_app/translations/i18n.dart';
import 'package:schluss_beta_app/ui/reused_widgets/group_header.dart';
import 'package:schluss_beta_app/ui/reused_widgets/mini_header_template.dart';
import 'package:schluss_beta_app/ui/reused_widgets/provider_logo.dart';
import 'package:schluss_beta_app/ui/reused_widgets/providers_list_item.dart';
import 'package:schluss_beta_app/util/error_handler_util.dart';
import 'package:schluss_beta_app/util/initials_picker_util.dart';
import 'package:schluss_beta_app/view_model/contract_detail_view_model.dart';
import 'package:schluss_beta_app/view_model/contract_providers_view_model.dart';

class ContractPage extends StatefulWidget {
  final int? contractId;
  final String? fromUI;

  const ContractPage({
    this.contractId,
    this.fromUI,
    Key? key,
  }) : super(key: key);

  @override
  ContractPageState createState() => ContractPageState();
}

class ContractPageState extends State<ContractPage> {
  ContractProvidersViewModel? contractProvidersViewModel = ContractProvidersViewModel();
  final ContractController _contractController = ContractController();
  final InitialsPickerUtil _initialsPickerUtil = InitialsPickerUtil();
  final ErrorHandlerUtil _errorHandler = ErrorHandlerUtil();

  var count = 0;
  Color? logoColor;

  @override
  void initState() {
    initScreenData();
    super.initState();
  }

  /// Initializes data elements in screen.
  void initScreenData() async {
    ContractProvidersViewModel? contractViewmodel;

    try {
      contractViewmodel = await _contractController.getContractById(widget.contractId);
    } catch (e) {
      _errorHandler.handleError(e.toString(), false, context);
    }

    setState(() {
      contractProvidersViewModel = contractViewmodel;

      if (contractProvidersViewModel!.providerList != null) {
        count = contractProvidersViewModel!.providerList!.length;
        logoColor = Color(int.parse(contractProvidersViewModel!.iconColor!));
      }
    });
  }

  @override
  Widget build(BuildContext context) {
    double width = MediaQuery.of(context).size.width;
    double height = MediaQuery.of(context).size.height;
    double topPadding = MediaQuery.of(context).padding.top;

    return Scaffold(
      body: Stack(
        children: <Widget>[
          Container(
            color: UIConstants.paleGray,
            child: ListView.builder(
              padding: EdgeInsets.only(top: height * 0.06),
              // +3 because we are showing three extra widgets. So we build top
              // container's when data looping.
              itemCount: count + 2,
              itemBuilder: (BuildContext context, int index) {
                if (index == 0) {
                  return buildTopContainer(height, width, topPadding);
                } else if (index == 1) {
                  return GroupHeader(
                    text: I18n.of(context).contractInfoGroupHeader.toUpperCase(),
                  );
                }
                List<ContractDetailViewModel>? list;
                list = contractProvidersViewModel!.providerList;
                // Here we have 2 extra widgets above the list.
                int numberOfExtraWidgets;
                numberOfExtraWidgets = 2;
                // Index of actual provider.
                index = index - numberOfExtraWidgets;

                return Material(
                  color: UIConstants.primaryColor,
                  child: InkWell(
                    splashColor: UIConstants.blackFaded,
                    onTap: () => providerClicked(list![index]),
                    child: Column(
                      children: <Widget>[
                        ProvidersListItem(
                          title: list![index].providerName!,
                          subTitle: list[index].providerDescription!,
                          logo: list[index].providerLogo!,
                          from: list[index].fromName,
                        ),
                        const Divider(height: 1),
                      ],
                    ),
                  ),
                );
              },
            ),
          ),

          /// Mini header with add button, options button and back button.
          MiniHeaderTemplate(
            optionsBtnCall: () => Navigator.pushNamed(
              context,
              '/contracts/contract/delete',
              arguments: {
                'contractId': widget.contractId,
                'fromUI': widget.fromUI,
              },
            ),
            hasBackBtn: true,
            backBtnText: I18n.of(context).contractsTitle,
          ),
        ],
      ),
    );
  }

  /// Details section.
  Container buildTopContainer(double height, double width, double topPadding) {
    return Container(
      margin: EdgeInsets.only(top: topPadding),
      padding: EdgeInsets.symmetric(
        horizontal: width * 0.05,
        vertical: width * 0.05,
      ),
      color: UIConstants.primaryColor,
      child: Column(
        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
        children: <Widget>[
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: <Widget>[
              /// Logo.
              Expanded(
                flex: 2,
                child: ProviderLogo(
                  isOrg: contractProvidersViewModel!.requesterType == 'org' ? true : false,
                  logoImg: contractProvidersViewModel!.logoUrl ?? '',
                  logoColor: logoColor,
                  logoLetters: _initialsPickerUtil.getInitials(contractProvidersViewModel!.requesterName ?? '').toString(),
                ),
              ),

              /// Title.
              Expanded(
                flex: 7,
                child: Text(
                  contractProvidersViewModel!.requesterName ?? '',
                  maxLines: 1,
                  textAlign: TextAlign.start,
                  overflow: TextOverflow.ellipsis,
                  softWrap: true,
                  style: TextStyle(
                    fontWeight: FontWeight.w700,
                    color: UIConstants.grayDarkest100,
                    fontSize: width * 0.07,
                  ),
                ),
              ),
            ],
          ),

          /// Details.
          Padding(
            padding: EdgeInsets.only(top: width * 0.04),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: <Widget>[
                Text(
                  I18n.of(context).connectionInfoGeneralDetailsTxt,
                  maxLines: 1,
                  textAlign: TextAlign.start,
                  overflow: TextOverflow.ellipsis,
                  softWrap: true,
                  style: TextStyle(
                    fontFamily: UIConstants.subFontFamily,
                    color: UIConstants.gray100,
                    fontWeight: FontWeight.w400,
                    fontStyle: FontStyle.normal,
                    letterSpacing: 1,
                    fontSize: width * 0.045,
                  ),
                ),
              ],
            ),
          ),

          /// Divider.
          const Divider(height: 28),

          /// Created date.
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: <Widget>[
              /// Label.
              Expanded(
                flex: 5,
                child: Text(
                  I18n.of(context).connectionInfoDateTxt,
                  maxLines: 1,
                  textAlign: TextAlign.start,
                  overflow: TextOverflow.ellipsis,
                  softWrap: true,
                  style: TextStyle(
                    fontFamily: UIConstants.subFontFamily,
                    color: UIConstants.gray100,
                    fontWeight: FontWeight.w400,
                    fontStyle: FontStyle.normal,
                    letterSpacing: 0,
                    fontSize: width * 0.040,
                  ),
                ),
              ),

              /// Value.
              Expanded(
                flex: 5,
                child: Text(
                  contractProvidersViewModel!.requestDate ?? '',
                  maxLines: 1,
                  textAlign: TextAlign.end,
                  overflow: TextOverflow.ellipsis,
                  softWrap: true,
                  style: TextStyle(
                    fontWeight: FontWeight.w700,
                    color: UIConstants.grayDarkest100,
                    fontSize: width * 0.048,
                  ),
                ),
              ),
            ],
          ),

          /// Divider.
          const Divider(height: 28),

          /// Term.
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: <Widget>[
              /// Label.
              Expanded(
                flex: 5,
                child: Text(
                  I18n.of(context).connectionInfoDurationTxt,
                  maxLines: 1,
                  textAlign: TextAlign.start,
                  overflow: TextOverflow.ellipsis,
                  softWrap: true,
                  style: TextStyle(
                    fontFamily: UIConstants.subFontFamily,
                    color: UIConstants.gray100,
                    fontWeight: FontWeight.w400,
                    fontStyle: FontStyle.normal,
                    letterSpacing: 0,
                    fontSize: width * 0.040,
                  ),
                ),
              ),

              /// Value.
              Expanded(
                flex: 5,
                child: Text(
                  I18n.of(context).dataReqDoneTermValueTxt,
                  maxLines: 1,
                  textAlign: TextAlign.end,
                  overflow: TextOverflow.ellipsis,
                  softWrap: true,
                  style: TextStyle(
                    fontWeight: FontWeight.w700,
                    color: UIConstants.grayDarkest100,
                    fontSize: width * 0.048,
                  ),
                ),
              ),
            ],
          ),
        ],
      ),
    );
  }

  void providerClicked(ContractDetailViewModel contractDetailViewModel) {
    Navigator.pushNamed(
      context,
      '/contracts/contract/info',
      arguments: contractDetailViewModel,
    );
  }
}
