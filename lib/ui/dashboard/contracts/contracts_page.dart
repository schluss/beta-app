import 'package:flutter/material.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:provider/provider.dart';
import 'package:schluss_beta_app/constants/helper_constants.dart';
import 'package:schluss_beta_app/constants/ui_constants.dart';
import 'package:schluss_beta_app/data/models/contract/contract_model.dart';
import 'package:schluss_beta_app/providers/impl/contract_data_provider.dart';
import 'package:schluss_beta_app/singleton_injector.dart';
import 'package:schluss_beta_app/translations/i18n.dart';
import 'package:schluss_beta_app/ui/dashboard/contracts/connection_provider_item.dart';
import 'package:schluss_beta_app/ui/dashboard/contracts/contract_provider_item.dart';
import 'package:schluss_beta_app/ui/reused_widgets/dashboard_page_template.dart';
import 'package:schluss_beta_app/ui/reused_widgets/empty_state_content_template.dart';
import 'package:schluss_beta_app/view_model/contract_recent_item.dart';

enum ConnectionScreenIdentifier { myData, sharedData }

class ContractsPage extends StatefulWidget {
  const ContractsPage({Key? key}) : super(key: key);

  @override
  ContractsPageState createState() => ContractsPageState();
}

class ContractsPageState extends State<ContractsPage> with SingleTickerProviderStateMixin {
  final ContractDataProvider _contractDataProvider = singletonInjector<ContractDataProvider>();

  @override
  void initState() {
    _getContracts();
    super.initState();
  }

  /// Gets recent and a-z contracts.
  void _getContracts() {
    _contractDataProvider.getContracts();
  }

  @override
  Widget build(BuildContext context) {
    double width = MediaQuery.of(context).size.width;
    double height = MediaQuery.of(context).size.height;

    /// Header.
    return DashboardPageTemplate(
      text: I18n.of(context).contractsTitle,
      appBody: buildBody(width, height),
      addBtnCall: () => Navigator.pushNamed(context, '/stewardship/owner/info'),
    );
  }

  /// Creates layout body containers.
  Column buildBody(double width, double height) {
    return Column(
      children: <Widget>[
        Expanded(
          child: ChangeNotifierProvider<ContractDataProvider?>(
            create: (context) => _contractDataProvider,
            child: Consumer<ContractDataProvider>(
              builder: (context, model, child) {
                if (model.isLoading) {
                  return _buildWaitingArea();
                } else {
                  if (model.contractMap.isNotEmpty) {
                    return _buildContractData(width);
                  } else {
                    return EmptyStateContentTemplate(
                      img: const AssetImage('assets/images/intro_4.png'),
                      text: I18n.of(context).contractsNoContractsTxt,
                    );
                  }
                }
              },
            ),
          ),
        ),
      ],
    );
  }

  /// Builds screen body waiting area.
  Container _buildWaitingArea() {
    return Container(
      color: UIConstants.paleGray,
      child: const Center(
        child: SpinKitThreeBounce(
          color: UIConstants.slateGray100,
          size: 16,
        ),
      ),
    );
  }

  /// Builds screen body tab area.
  Column _buildContractData(double width) {
    return Column(
      children: <Widget>[
        Expanded(
          child: Consumer<ContractDataProvider>(
            builder: (context, model, child) => Container(
              color: UIConstants.primaryColor,
              child: ListView(
                padding: EdgeInsets.zero,
                children: <Widget>[...buildRecentChildren(model.contractMap, width)],
              ),
            ),
          ),
        ),
      ],
    );
  }

  /// Creates recent tab child widget.
  List<Widget> buildRecentChildren(Map<String?, List<ContractItem>> dataMap, double width) {
    List<Widget> children;
    children = <Widget>[];
    dataMap.forEach((key, value) {
      for (var element in value) {
        // Loop each.
        if (element.type == ConnectionType.user) {
          children.add(
            Material(
              color: UIConstants.primaryColor,
              child: InkWell(
                splashColor: UIConstants.blackFaded,
                onTap: () {
                  Navigator.pushNamed(
                    context,
                    '/contracts/connection',
                    arguments: {
                      'selectedId': element.contractId,
                      'fromUI': HelperConstant.fromContractUI,
                      'isSender': element.stewardshipType == StewardshipType.sender,
                      'isReceiver': element.stewardshipType == StewardshipType.receiver,
                    },
                  );
                },
                child: ConnectionProviderItem(
                  element.providerDataItem,
                  iconColor: Color(int.parse(element.providerDataItem.iconColor!)),
                  sharedDatCount: element.providerDataItem.noOfShares,
                  isSender: element.stewardshipType == StewardshipType.sender,
                  isReceiver: element.stewardshipType == StewardshipType.receiver,
                  hasOptionBtn: false,
                ),
              ),
            ),
          );
        } else {
          children.add(
            Material(
              color: UIConstants.primaryColor,
              child: InkWell(
                splashColor: UIConstants.blackFaded,
                onTap: () {
                  Navigator.pushNamed(
                    context,
                    '/contracts/contract',
                    arguments: {
                      'selectedId': element.contractId,
                      'fromUI': HelperConstant.fromContractUI,
                    },
                  );
                },
                child: ContractProviderItem(
                  element.providerDataItem,
                  element.description,
                ),
              ),
            ),
          );
        }
      }
    });

    return children;
  }
}
