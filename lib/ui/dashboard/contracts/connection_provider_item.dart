import 'package:flutter/material.dart';
import 'package:schluss_beta_app/constants/ui_constants.dart';
import 'package:schluss_beta_app/translations/i18n.dart';
import 'package:schluss_beta_app/ui/reused_widgets/provider_logo.dart';
import 'package:schluss_beta_app/util/initials_picker_util.dart';
import 'package:schluss_beta_app/view_model/provider_data_model.dart';

class ConnectionProviderItem extends StatefulWidget {
  final ProviderDataModel? providerDataItem;
  final Color? iconColor;
  final String subTitle;
  final int? sharedDatCount;
  final bool isSender;
  final bool isReceiver;

  final bool hasOptionBtn;

  const ConnectionProviderItem(
    this.providerDataItem, {
    this.iconColor,
    this.subTitle = '',
    this.sharedDatCount,
    required this.isSender,
    required this.isReceiver,
    this.hasOptionBtn = true,
    Key? key,
  }) : super(key: key);

  @override
  ConnectionProviderItemState createState() => ConnectionProviderItemState();
}

class ConnectionProviderItemState extends State<ConnectionProviderItem> {
  final InitialsPickerUtil _initialsPickerUtil = InitialsPickerUtil();

  @override
  Widget build(BuildContext context) {
    double width = MediaQuery.of(context).size.width;
    double height = MediaQuery.of(context).size.height;

    return Container(
      decoration: BoxDecoration(
        border: Border(
          top: buildBorderSide(),
          bottom: buildBorderSide(),
        ),
      ),
      padding: EdgeInsets.symmetric(
        horizontal: width * 0.05,
        vertical: width * 0.03,
      ),
      child: Row(
        crossAxisAlignment: CrossAxisAlignment.center,
        children: <Widget>[
          /// Logo.
          Expanded(
            flex: 2,
            child: widget.providerDataItem?.imagePath != null
                ? ProviderLogo(
                    logoImg: widget.providerDataItem!.imagePath!,
                  )
                : ProviderLogo(
                    isOrg: false,
                    logoColor: Color(int.parse(widget.providerDataItem!.iconColor!)),
                    logoLetters: _initialsPickerUtil.getInitials(widget.providerDataItem!.title!).toString(),
                  ),
          ),
          Expanded(
            flex: 6,
            child: Column(
              mainAxisAlignment: MainAxisAlignment.start,
              mainAxisSize: MainAxisSize.max,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                /// Title.
                buildConstrainedBox(
                  context,
                  widget.providerDataItem!.title!,
                  TextStyle(
                    color: UIConstants.grayDarkest100,
                    fontWeight: FontWeight.w600,
                    fontStyle: FontStyle.normal,
                    letterSpacing: 0,
                    fontSize: width * 0.045,
                  ),
                ),

                /// Subtitle.
                widget.subTitle.isNotEmpty == true
                    ? buildConstrainedBox(
                        context,
                        widget.subTitle,
                        TextStyle(
                          fontFamily: UIConstants.subFontFamily,
                          color: UIConstants.gray100,
                          fontWeight: FontWeight.w400,
                          fontStyle: FontStyle.normal,
                          letterSpacing: 0,
                          fontSize: width * 0.042,
                        ),
                      )
                    : Container(),

                /// Details count.
                Row(
                  children: [
                    widget.sharedDatCount != null
                        ? Padding(
                            padding: EdgeInsets.only(top: width * 0.01),
                            child: Container(
                              height: height * 0.03,
                              padding: EdgeInsets.symmetric(horizontal: width * 0.025),
                              decoration: BoxDecoration(
                                borderRadius: BorderRadius.circular(6.0),
                                color: UIConstants.paleGray,
                              ),
                              child: Align(
                                alignment: Alignment.center,
                                child: buildConstrainedBox(
                                  context,
                                  '${widget.sharedDatCount != null ? widget.sharedDatCount.toString() : '0'} ${I18n.of(
                                    context,
                                  ).dataTitle.toLowerCase()}',
                                  TextStyle(
                                    color: UIConstants.gray100,
                                    fontWeight: FontWeight.w400,
                                    fontStyle: FontStyle.normal,
                                    letterSpacing: 0,
                                    fontSize: width * 0.045,
                                  ),
                                ),
                              ),
                            ),
                          )
                        : Container(),
                    widget.isSender || widget.isReceiver
                        ? Padding(
                            padding: EdgeInsets.only(top: width * 0.01, left: width * 0.02),
                            child: Container(
                              height: height * 0.03,
                              padding: EdgeInsets.symmetric(horizontal: width * 0.025),
                              decoration: BoxDecoration(
                                borderRadius: BorderRadius.circular(6.0),
                                color: widget.isReceiver ? UIConstants.pastelBlue : UIConstants.pastelPink,
                              ),
                              child: Align(
                                alignment: Alignment.center,
                                child: Image(
                                  image: AssetImage(
                                    widget.isReceiver ? 'assets/images/stewardship_blue_icon.png' : 'assets/images/stewardship_pink_icon.png',
                                  ),
                                  //width: width * 0.15,
                                ),
                              ),
                            ),
                          )
                        : Container(),
                  ],
                ),
              ],
            ),
          ),

          /// Arrow head.
          Expanded(
            flex: 1,
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.end,
              mainAxisAlignment: MainAxisAlignment.end,
              children: <Widget>[
                Container(
                  padding: EdgeInsets.only(left: width * 0.05),
                  child: Icon(
                    !widget.hasOptionBtn ? Icons.keyboard_arrow_right : Icons.more_horiz,
                    color: UIConstants.gray100,
                    size: width * 0.055,
                  ),
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }

  BorderSide buildBorderSide() => const BorderSide(
        color: UIConstants.grayLight,
        width: 0.5,
      );

  ConstrainedBox buildConstrainedBox(
    BuildContext context,
    String text,
    TextStyle textStyle,
  ) {
    double width = MediaQuery.of(context).size.width;

    return ConstrainedBox(
      constraints: BoxConstraints(maxWidth: width * 0.7),
      child: Text(
        text,
        softWrap: true,
        overflow: TextOverflow.ellipsis,
        style: textStyle,
      ),
    );
  }
}
