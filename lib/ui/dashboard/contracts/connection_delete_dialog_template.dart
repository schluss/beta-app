import 'package:flutter/material.dart';
import 'package:schluss_beta_app/constants/ui_constants.dart';
import 'package:schluss_beta_app/ui/reused_widgets/bottom_up_sheet_template.dart';
import 'package:schluss_beta_app/ui/reused_widgets/full_width_button.dart';
import 'package:schluss_beta_app/ui/reused_widgets/main_button.dart';

class ConnectionDeleteBottomUpSheetTemplate extends StatefulWidget {
  final String? title;
  final String? delBtnTxt;
  final String? whitishBtnTxt;
  final VoidCallback? deleteCallback;

  const ConnectionDeleteBottomUpSheetTemplate({
    this.title,
    this.delBtnTxt,
    this.whitishBtnTxt,
    this.deleteCallback,
    Key? key,
  }) : super(key: key);

  @override
  ConnectionDeleteBottomUpSheetTemplateState createState() => ConnectionDeleteBottomUpSheetTemplateState();
}

class ConnectionDeleteBottomUpSheetTemplateState extends State<ConnectionDeleteBottomUpSheetTemplate> {
  @override
  Widget build(BuildContext context) {
    double width = MediaQuery.of(context).size.width;

    return BottomUpSheetTemplate(
      page: Column(
        mainAxisAlignment: MainAxisAlignment.end,
        children: <Widget>[
          Text(
            widget.title!,
            maxLines: 1,
            textAlign: TextAlign.start,
            overflow: TextOverflow.ellipsis,
            softWrap: true,
            style: TextStyle(
              fontFamily: UIConstants.subFontFamily,
              color: UIConstants.gray100,
              fontWeight: FontWeight.w400,
              fontStyle: FontStyle.normal,
              letterSpacing: 0,
              fontSize: width * 0.040,
            ),
          ),
          FullWidthButton(
            text: widget.delBtnTxt!,
            call: widget.deleteCallback!,
          ),
          MainButton(
            type: BtnType.whitish,
            text: widget.whitishBtnTxt!,
            call: () => Navigator.pop(context),
          ),
        ],
      ),
      heightRatio: Theme.of(context).platform == TargetPlatform.iOS ? 0.60 : 0.60,
    );
  }
}
