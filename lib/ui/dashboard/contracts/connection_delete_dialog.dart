import 'package:flutter/material.dart';
import 'package:schluss_beta_app/translations/i18n.dart';
import 'package:schluss_beta_app/ui/dashboard/contracts/connection_delete_dialog_template.dart';

class ConnectionDeleteBottomUpSheet extends StatefulWidget {
  final String? userName;
  final int? dataCount;
  final VoidCallback? deleteCallback;

  const ConnectionDeleteBottomUpSheet({
    this.userName,
    this.dataCount,
    Key? key,
    this.deleteCallback,
  }) : super(key: key);

  @override
  ConnectionDeleteBottomUpSheetState createState() => ConnectionDeleteBottomUpSheetState();
}

class ConnectionDeleteBottomUpSheetState extends State<ConnectionDeleteBottomUpSheet> {
  @override
  Widget build(BuildContext context) {
    return ConnectionDeleteBottomUpSheetTemplate(
      title: '${widget.userName!} • ${widget.dataCount!} ${I18n.of(context).connectionDeleteBottomShtDataCountTitle}',
      delBtnTxt: I18n.of(context).connectionDelBottomShtDelBtnTxt,
      whitishBtnTxt: I18n.of(context).connectionDelBottomShtCancelBtnTxt,
      deleteCallback: widget.deleteCallback,
    );
  }
}
