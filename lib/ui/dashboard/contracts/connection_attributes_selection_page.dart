import 'package:flutter/material.dart';
import 'package:schluss_beta_app/constants/ui_constants.dart';
import 'package:schluss_beta_app/translations/i18n.dart';
import 'package:schluss_beta_app/ui/reused_widgets/data_list_item.dart';
import 'package:schluss_beta_app/ui/reused_widgets/group_header.dart';
import 'package:schluss_beta_app/ui/reused_widgets/mini_header_template.dart';
import 'package:schluss_beta_app/ui/reused_widgets/provider_logo.dart';
import 'package:schluss_beta_app/ui/reused_widgets/provider_retrieval_info.dart';
import 'package:schluss_beta_app/view_model/contract_detail_view_model.dart';

class ConnectionAttributesSelectionPage extends StatefulWidget {
  const ConnectionAttributesSelectionPage({
    Key? key,
  }) : super(key: key);

  @override
  ConnectionAttributesSelectionPageState createState() => ConnectionAttributesSelectionPageState();
}

class ConnectionAttributesSelectionPageState extends State<ConnectionAttributesSelectionPage> {
  ContractDetailViewModel? contractDetailViewModelDecrypted = ContractDetailViewModel();

  var count = 0;

  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    double width = MediaQuery.of(context).size.width;
    double height = MediaQuery.of(context).size.height;
    double topPadding = MediaQuery.of(context).padding.top;

    return Scaffold(
      backgroundColor: UIConstants.primaryColor,
      body: Stack(
        children: <Widget>[
          SingleChildScrollView(
            child: Container(
              height: height,
              width: width,
              color: UIConstants.primaryColor,
              child: ListView(
                padding: EdgeInsets.only(
                  top: height * 0.06 + topPadding,
                  bottom: height * 0.2,
                ),
                children: <Widget>[
                  ProviderRetrievalInfo(
                    title: I18n.of(context).providerInfoDateTxt,
                    subTitle: 'Date',
                    isSubTitleBold: true,
                  ),
                  Stack(
                    children: <Widget>[
                      ProviderRetrievalInfo(
                        title: I18n.of(context).providerInfoSrcTxt,
                        subTitle: '',
                        heightScale: 0.06,
                        isSubTitleBold: true,
                      ),
                      Align(
                        alignment: Alignment.centerRight,
                        child: Padding(
                          padding: EdgeInsets.only(right: width * 0.05),
                          child: const ProviderLogo(),
                        ),
                      ),
                    ],
                  ),
                  SizedBox(height: height * 0.012),

                  // Pass in the method below a [DataDetailViewModel] instance.
                  ...buildAttributes(
                    contractDetailViewModelDecrypted!,
                    width,
                    height,
                    context,
                  ),
                ],
              ),
            ),
          ),

          /// Mini title with back button.
          MiniHeaderTemplate(
            text: '',
            hasBackBtn: true,
            backBtnText: I18n.of(context).dataTitle,
          ),
        ],
      ),
    );
  }
}

/// Builds to show list of attributes values
List<Widget> buildAttributes(
  ContractDetailViewModel model,
  double width,
  double height,
  BuildContext context,
) {
  List<Widget> children;
  children = <Widget>[];

  if (model.dataMap != null) {
    // Parse map without groups.
    if (model.dataMap!.keys.length == 1 && model.dataMap!.keys.first!.isEmpty) {
      children.add(const GroupHeader(text: ''));
      for (var list in model.dataMap!.values) {
        {
          for (var element in list) {
            children.add(DataListItem(
              model: element,
              isDecryptionNeeded: true,
            ));
            children.add(const Divider(height: 1));
          }
        }
      }
    } else {
      // Parse map with groups.
      model.dataMap!.forEach((key, value) {
        children.add(GroupHeader(text: key!.toUpperCase()));

        for (var element in value) {
          children.add(DataListItem(
            model: element,
            isDecryptionNeeded: true,
          ));
        }
      });
    }
  }
  return children;
}
