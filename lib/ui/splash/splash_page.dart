import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:schluss_beta_app/constants/ui_constants.dart';
import 'package:schluss_beta_app/controller/account_controller.dart';
import 'package:schluss_beta_app/util/error_handler_util.dart';

class SplashPage extends StatefulWidget {
  const SplashPage({Key? key}) : super(key: key);

  @override
  SplashPageState createState() => SplashPageState();
}

class SplashPageState extends State<SplashPage> {
  final AccountController _accountController = AccountController();
  final ErrorHandlerUtil _errorHandler = ErrorHandlerUtil();

  late bool isAvailable;

  @override
  void initState() {
    super.initState();
    // Just to show little wait splash screen. If not needed we can remove it.
    Future.delayed(const Duration(milliseconds: 1000), checkVaultAvailability);
  }

  /// Checks existing vault availability.
  void checkVaultAvailability() async {
    isAvailable = false;

    try {
      isAvailable = await _accountController.validateVaultAvailability();
    } on Future catch (e) {
      _errorHandler.handleError(e.toString(), false, context);
    }

    if (isAvailable) {
      // Vault already created.
      var waitingTime = await _accountController.validateLoginAttempts();

      // If no  waiting time.
      if (!mounted) return;
      if (waitingTime == 0) {
        Navigator.pushReplacementNamed(context, '/account/login');
      } else if (waitingTime < 0) {
        Navigator.pushReplacementNamed(context, '/account/login/countdown');
      }
    } else {
      if (!mounted) return;
      Navigator.pushReplacementNamed(context, '/intro');
    }
  }

  @override
  Widget build(BuildContext context) {
    double width = MediaQuery.of(context).size.width;
    double height = MediaQuery.of(context).size.height;

    return Scaffold(
      backgroundColor: UIConstants.accentColor,
      body: SafeArea(
        child: SizedBox(
          height: height,
          width: width,
          child: Stack(
            children: <Widget>[
              Center(
                child: SvgPicture.asset(
                  'assets/images/logo.svg',
                  width: width * 0.25,
                ),
              ),
              /* BETA mention
              Padding(
                padding: EdgeInsets.only(bottom: height * 0.075),
                child: Align(
                  alignment: Alignment.bottomCenter,
                  child: Text(
                    I18n.of(context).splashPageBottomTxt.toUpperCase(),
                    textAlign: TextAlign.center,
                    style: TextStyle(
                      fontSize: width * 0.0575,
                      letterSpacing: 2,
                      color: UIConstants.primaryColor,
                      fontFamily: UIConstants.subFontFamily,
                      fontWeight: FontWeight.w600,
                    ),
                  ),
                ),
              )*/
            ],
          ),
        ),
      ),
    );
  }
}
