import 'package:flutter/material.dart';
import 'package:schluss_beta_app/constants/ui_constants.dart';
import 'package:schluss_beta_app/translations/i18n.dart';
import 'package:schluss_beta_app/ui/reused_widgets/backward_button.dart';
import 'package:schluss_beta_app/ui/reused_widgets/format_validation_notifier.dart';
import 'package:schluss_beta_app/ui/reused_widgets/keypad.dart';
import 'package:schluss_beta_app/ui/reused_widgets/pin_code.dart';
import 'package:schluss_beta_app/ui/reused_widgets/pin_eye.dart';
import 'package:schluss_beta_app/ui/reused_widgets/pin_screen_template.dart';
import 'package:schluss_beta_app/ui/reused_widgets/tap_tip.dart';

enum ValidationState { primary, error, success }

class PinCreationPage extends StatefulWidget {
  final bool isChangingPinCodeFlow;
  final String? oldPassword;

  const PinCreationPage({
    this.isChangingPinCodeFlow = false,
    this.oldPassword,
    Key? key,
  }) : super(key: key);

  @override
  PinCreationPageState createState() => PinCreationPageState();
}

class PinCreationPageState extends State<PinCreationPage> {
  final ValidationState _valid = ValidationState.success;
  final ValidationState _invalid = ValidationState.error;

  List<String> password = ['', '', '', '', ''];
  int inputIndex = 0;
  bool isPasswordVisible = false;
  ValidationState validationSameNum = ValidationState.primary;
  ValidationState validationSeqNum = ValidationState.primary;

  @override
  Widget build(BuildContext context) {
    double width = MediaQuery.of(context).size.width;
    double height = MediaQuery.of(context).size.height;

    return Scaffold(
      backgroundColor: UIConstants.primaryColor,
      body: SafeArea(
        child: SizedBox(
          width: width,
          height: height,
          child: Stack(
            children: [
              /// Back button.
              const BackwardButton(),

              /// Pin screen.
              PinScreenTemplate(
                /// Title text.
                text: I18n.of(context).regCreateTitlePart1,

                /// PIN.
                pin: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceAround,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: [
                    Expanded(child: getPasswordItemByIndex(width, 0)),
                    Expanded(child: getPasswordItemByIndex(width, 1)),
                    Expanded(child: getPasswordItemByIndex(width, 2)),
                    Expanded(child: getPasswordItemByIndex(width, 3)),
                    Expanded(child: getPasswordItemByIndex(width, 4)),
                  ],
                ),

                /// Validation messages.
                validationNotifier: Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    FormatValidationNotifier(
                      text: I18n.of(context).regCreateValidNoSameDigitTxt,
                      state: validationSameNum,
                    ),
                    FormatValidationNotifier(
                      text: I18n.of(context).regCreateValidNoSeqDigitsTxt,
                      state: validationSeqNum,
                    ),
                  ],
                ),

                /// Key pad.
                keyPad: KeyPad(
                  handleKeyOne: onOneClick,
                  handleKeyTwo: onTwoClick,
                  handleKeyThree: onThreeClick,
                  handleKeyFour: onFourClick,
                  handleKeyFive: onFiveClick,
                  handleKeySix: onSixClick,
                  handleKeySeven: onSevenClick,
                  handleKeyEight: onEightClick,
                  handleKeyNine: onNineClick,
                  handleKeyZero: onZeroClick,
                  handleDeleteKey: onDeleteNumber,
                ),

                /// Tooltip.
                tooltip: TapTip(
                  linkWidget: Row(
                    children: [
                      Text(
                        I18n.of(context).regCreateTitlePart2,
                        style: TextStyle(
                          fontSize: width * 0.05,
                          decoration: TextDecoration.underline,
                          color: UIConstants.accentColor,
                        ),
                      ),
                      Padding(
                        padding: EdgeInsets.only(bottom: width * 0.04),
                        child: SizedBox(
                          width: width * 0.0375,
                          height: width * 0.0375,
                          child: Image.asset('assets/images/info_icon.png'),
                        ),
                      ),
                    ],
                  ),
                  text: I18n.of(context).regCreateTooltipTxt,
                  hasCloseBtn: true,
                ),

                /// PIN eye.
                pinEye: GestureDetector(
                  onTap: () {
                    setState(() => isPasswordVisible = !isPasswordVisible);
                  },
                  child: PinEye(isVisible: isPasswordVisible),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }

  /// Shows dots or asterisks/digits.
  Widget getPasswordItemByIndex(double width, int index) {
    return password[index].isEmpty
        ? buildEmptyPlaceholder(width)
        : isPasswordVisible
            ? buildPasswordText(width, password[index])
            : buildStarImage(width);
  }

  /// Assigns values for keys.
  void onOneClick() => handleUserInput('1');

  void onTwoClick() => handleUserInput('2');

  void onThreeClick() => handleUserInput('3');

  void onFourClick() => handleUserInput('4');

  void onFiveClick() => handleUserInput('5');

  void onSixClick() => handleUserInput('6');

  void onSevenClick() => handleUserInput('7');

  void onEightClick() => handleUserInput('8');

  void onNineClick() => handleUserInput('9');

  void onZeroClick() => handleUserInput('0');

  void onDeleteNumber() => deleteDigit();

  /// Handles PIN.
  void handleUserInput(String number) {
    if (inputIndex != password.length) {
      password[inputIndex] = number;

      setState(() {});

      if (inputIndex != password.length) inputIndex++;

      if (inputIndex == password.length) validatePin();
    }
  }

  /// Validates PIN.
  void validatePin() {
    var countSameNum = 0;
    var countSeqAscNum = 0;
    var countSeqDesNum = 0;

    for (var i = 0; i < password.length - 1; i++) {
      setState(() {
        if (int.parse(password[i]) == int.parse(password[i + 1])) {
          countSameNum += 1;

          validationSameNum = (countSameNum == 4) ? _invalid : _valid;
          validationSeqNum = _valid;
        } else if ((int.parse(password[i + 1]) - 1) == int.parse(password[i])) {
          countSeqAscNum += 1;

          validationSameNum = _valid;
          validationSeqNum = (countSeqAscNum == 4) ? _invalid : _valid;
        } else if (int.parse(password[i + 1]) == (int.parse(password[i]) - 1)) {
          countSeqDesNum += 1;

          validationSameNum = _valid;
          validationSeqNum = (countSeqDesNum == 4) ? _invalid : _valid;
        } else {
          validationSameNum = _valid;
          validationSeqNum = _valid;
        }
      });
    }

    if (validationSameNum == _valid && validationSeqNum == _valid) {
      goToSubmitPageAfterDelay();
    }
  }

  /// Navigates to PIN confirming screen.
  void goToSubmitPageAfterDelay() {
    Future.delayed(
      const Duration(milliseconds: 500),
      () => Navigator.pushNamed(
        context,
        '/account/registration/confirm-pin',
        arguments: {
          'currentPinCode': password.join(''),
          'isChangingPinCodeFlow': widget.isChangingPinCodeFlow,
          'oldPassword': widget.oldPassword,
        },
      ),
    );
  }

  /// Deletes the last digit.
  void deleteDigit() {
    if (inputIndex >= 0) {
      if (inputIndex != 0) inputIndex--;

      password[inputIndex] = '';

      setState(() {
        validationSameNum = ValidationState.primary;
        validationSeqNum = ValidationState.primary;
      });
    }
  }
}
