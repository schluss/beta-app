import 'package:flutter/material.dart';
import 'package:logger/logger.dart';
import 'package:schluss_beta_app/constants/ui_constants.dart';
import 'package:schluss_beta_app/controller/account_controller.dart';
import 'package:schluss_beta_app/singleton_injector.dart';
import 'package:schluss_beta_app/translations/i18n.dart';
import 'package:schluss_beta_app/ui/reused_widgets/backward_button.dart';
import 'package:schluss_beta_app/ui/reused_widgets/keypad.dart';
import 'package:schluss_beta_app/ui/reused_widgets/matching_validation_notifier.dart';
import 'package:schluss_beta_app/ui/reused_widgets/pin_code.dart';
import 'package:schluss_beta_app/ui/reused_widgets/pin_eye.dart';
import 'package:schluss_beta_app/ui/reused_widgets/pin_screen_template.dart';
import 'package:schluss_beta_app/util/error_handler_util.dart';

enum SignInValidationState { primary, error, success }

class PinConfirmationPage extends StatefulWidget {
  final String? password;
  final bool? isChangingPinCodeFlow;
  final String? oldPassword;

  const PinConfirmationPage(
    this.password, {
    this.isChangingPinCodeFlow = false,
    this.oldPassword,
    Key? key,
  }) : super(key: key);

  @override
  PinConfirmationPageState createState() => PinConfirmationPageState();
}

class PinConfirmationPageState extends State<PinConfirmationPage> {
  final AccountController _accountController = AccountController();
  final ErrorHandlerUtil _errorHandler = ErrorHandlerUtil();
  final Logger _logger = singletonInjector<Logger>();

  List<String> password = ['', '', '', '', ''];
  int inputIndex = 0;
  bool isPasswordVisible = false;
  SignInValidationState isMatched = SignInValidationState.primary;

  @override
  Widget build(BuildContext context) {
    double width = MediaQuery.of(context).size.width;
    double height = MediaQuery.of(context).size.height;

    return Scaffold(
      backgroundColor: UIConstants.primaryColor,
      body: SafeArea(
        child: SizedBox(
          width: width,
          height: height,
          child: Stack(
            children: [
              /// Back button.
              const BackwardButton(),

              /// Pin screen.
              PinScreenTemplate(
                /// Title text.
                text: I18n.of(context).regSubmitTitle,

                /// PIN.
                pin: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceAround,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: [
                    Expanded(child: getPasswordItemByIndex(width, 0)),
                    Expanded(child: getPasswordItemByIndex(width, 1)),
                    Expanded(child: getPasswordItemByIndex(width, 2)),
                    Expanded(child: getPasswordItemByIndex(width, 3)),
                    Expanded(child: getPasswordItemByIndex(width, 4)),
                  ],
                ),

                /// Validation messages.
                validationNotifier: Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    MatchingValidationNotifier(
                      text: _getNotificationMsg(),
                      isValid: _checkNotificationValidationStatus()!,
                    ),
                  ],
                ),

                /// Key pad.
                keyPad: KeyPad(
                  handleKeyOne: onOneClick,
                  handleKeyTwo: onTwoClick,
                  handleKeyThree: onThreeClick,
                  handleKeyFour: onFourClick,
                  handleKeyFive: onFiveClick,
                  handleKeySix: onSixClick,
                  handleKeySeven: onSevenClick,
                  handleKeyEight: onEightClick,
                  handleKeyNine: onNineClick,
                  handleKeyZero: onZeroClick,
                  handleDeleteKey: onDeleteNumber,
                ),

                /// PIN eye.
                pinEye: GestureDetector(
                  onTap: () {
                    setState(() => isPasswordVisible = !isPasswordVisible);
                  },
                  child: PinEye(isVisible: isPasswordVisible),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }

  /// Shows dots or asterisks/digits.
  Widget getPasswordItemByIndex(double width, int index) {
    return password[index].isEmpty
        ? buildEmptyPlaceholder(width)
        : isPasswordVisible
            ? buildPasswordText(width, password[index])
            : buildStarImage(width);
  }

  /// Assigns values for keys.
  void onOneClick() => handleUserInput('1');

  void onTwoClick() => handleUserInput('2');

  void onThreeClick() => handleUserInput('3');

  void onFourClick() => handleUserInput('4');

  void onFiveClick() => handleUserInput('5');

  void onSixClick() => handleUserInput('6');

  void onSevenClick() => handleUserInput('7');

  void onEightClick() => handleUserInput('8');

  void onNineClick() => handleUserInput('9');

  void onZeroClick() => handleUserInput('0');

  /// Handles PIN.
  void handleUserInput(String number) {
    if (inputIndex != password.length) {
      password[inputIndex] = number;

      setState(() {});

      if (inputIndex != password.length) inputIndex++;

      if (inputIndex == password.length) validatePin();
    }
  }

  /// Validates PIN.
  void validatePin() {
    if (password.join('') == widget.password) {
      setState(() => isMatched = SignInValidationState.success);

      if (widget.isChangingPinCodeFlow!) {
        updateVault();
      } else {
        Future.delayed(const Duration(seconds: 1), () {
          Navigator.pushNamed(
            context,
            '/account/create',
            arguments: widget.password,
          );
        });
      }
    } else {
      setState(() => isMatched = SignInValidationState.error);
    }
  }

  /// Updates PIN.
  void updateVault() async {
    if (widget.password != null && widget.oldPassword != null) {
      late bool status;

      try {
        status = await _accountController.updateSchlussVaultKey(
          widget.password,
          widget.oldPassword,
        );
      } catch (e) {
        _errorHandler.handleError(e.toString(), true, context, 'password-change');
      }

      if (status) {
        _logger.i('Password is successfully changed!');

        Future.delayed(const Duration(seconds: 1), () {
          Navigator.pushNamed(context, '/account/change-pin/done');
        });
      }
    }
  }

  /// Deletes the last digit.
  void onDeleteNumber() {
    if (inputIndex >= 0) {
      if (inputIndex != 0) inputIndex--;

      password[inputIndex] = '';

      setState(() => isMatched = SignInValidationState.primary);
    }
  }

  /// Loads validation message.
  dynamic _getNotificationMsg() {
    var msg = '';

    if (isMatched == SignInValidationState.success) {
      msg = I18n.of(context).regSubmitValidTxt;
    } else if (isMatched == SignInValidationState.error) {
      msg = I18n.of(context).regSubmitInvalidTxt;
    }

    return msg;
  }

  /// Checks validation message status to show the notification.
  /// if [null]  - no message.
  /// if [true] / [false] - show the message.
  bool? _checkNotificationValidationStatus() {
    bool? status = false;

    if (isMatched == SignInValidationState.success) {
      status = true;
    } else if (isMatched == SignInValidationState.error) {
      status = false;
    }

    return status;
  }
}
