import 'package:flutter/material.dart';
import 'package:schluss_beta_app/constants/ui_constants.dart';

class VaultCreationContent extends StatelessWidget {
  final String? title;
  final String? mainText;
  final String? imagePath;
  final double imageHeightFactor;

  const VaultCreationContent({
    this.title,
    this.mainText,
    this.imagePath,
    this.imageHeightFactor = 0.36,
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    double height = MediaQuery.of(context).size.height;
    double width = MediaQuery.of(context).size.width;

    return SizedBox(
      height: height,
      width: width,
      child: Stack(
        alignment: Alignment.topCenter,
        children: <Widget>[
          SizedBox(
            height: height * 0.142,
          ),
          Padding(
            padding: EdgeInsets.only(
              top: height * 0.172,
              right: width * 0.075,
              left: width * 0.075,
              bottom: height * 0.125,
            ),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: <Widget>[
                Padding(
                  padding: EdgeInsets.only(
                    top: height * 0.02,
                    bottom: height * 0.02,
                  ),
                  child: Image(
                    image: AssetImage(imagePath!),
                    height: height * imageHeightFactor,
                  ),
                ),
                Text(
                  title!,
                  textAlign: TextAlign.center,
                  style: TextStyle(
                    color: UIConstants.grayDarkest100,
                    fontSize: width * 0.05,
                    fontWeight: FontWeight.bold,
                  ),
                ),
                Text(
                  mainText!,
                  textAlign: TextAlign.center,
                  style: TextStyle(
                    height: 1.4,
                    color: UIConstants.gray100,
                    fontSize: width * 0.048,
                  ),
                ),
                SizedBox(height: height * 0.083),
              ],
            ),
          )
        ],
      ),
    );
  }
}
