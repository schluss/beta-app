import 'package:dots_indicator/dots_indicator.dart';
import 'package:flutter/material.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:logger/logger.dart';
import 'package:schluss_beta_app/constants/ui_constants.dart';
import 'package:schluss_beta_app/controller/account_controller.dart';
import 'package:schluss_beta_app/singleton_injector.dart';
import 'package:schluss_beta_app/translations/i18n.dart';
import 'package:schluss_beta_app/ui/registration/vault_creation_content.dart';
import 'package:schluss_beta_app/ui/reused_widgets/main_button.dart';
import 'package:schluss_beta_app/ui/utils/clipper.dart';
import 'package:schluss_beta_app/util/error_handler_util.dart';

class VaultCreationPage extends StatefulWidget {
  final String? password;

  const VaultCreationPage({
    Key? key,
    this.password,
  }) : super(key: key);

  @override
  VaultCreationPageState createState() => VaultCreationPageState();
}

class VaultCreationPageState extends State<VaultCreationPage> {
  final _pageController = PageController(initialPage: 0);
  final AccountController _accountController = AccountController();
  final ErrorHandlerUtil _errorHandler = ErrorHandlerUtil();
  final Logger _logger = singletonInjector<Logger>();

  int _currentPage = 0;
  bool _isVaultGenerated = false;

  @override
  void initState() {
    super.initState();
    generateVault();
  }

  /// Generates the vault.
  void generateVault() async {
    if (widget.password != null) {
      late bool isGenerated;

      try {
        isGenerated = await _accountController.generateSchlussVault(
          widget.password!,
        );
      } on Future catch (e) {
        _errorHandler.handleError(e.toString(), true, context, 'vault-creation');
      }

      if (isGenerated) {
        _logger.i('Vault created successfully.');

        setState(() => _isVaultGenerated = true);
      }
    }
  }

  @override
  Widget build(BuildContext context) {
    double width = MediaQuery.of(context).size.width;
    double height = MediaQuery.of(context).size.height;

    return WillPopScope(
      onWillPop: () async => false, // Disables system back-button click event.
      child: Scaffold(
        backgroundColor: UIConstants.primaryColor,
        body: SizedBox(
          width: width,
          height: height,
          child: Stack(
            alignment: Alignment.topCenter,
            children: <Widget>[
              ClipPath(
                clipper: Clipper(),
                child: Container(
                  color: UIConstants.paleGray,
                  height: height * 0.162,
                ),
              ),
              Padding(
                padding: EdgeInsets.only(top: height * 0.04),
                child: Align(
                  alignment: Alignment.topCenter,
                  child: (_isVaultGenerated)
                      ? Row(
                          crossAxisAlignment: CrossAxisAlignment.center,
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: <Widget>[
                            Padding(
                              padding: const EdgeInsets.only(
                                right: UIConstants.minPadding,
                              ),
                              child: Icon(
                                Icons.check,
                                color: UIConstants.greenBlue,
                                size: width * 0.06,
                              ),
                            ),
                            Text(
                              I18n.of(context).vaultCreationDoneHeader,
                              textAlign: TextAlign.center,
                              style: TextStyle(
                                fontWeight: FontWeight.w600,
                                color: UIConstants.gray100,
                                fontSize: width * 0.05,
                              ),
                            ),
                          ],
                        )
                      : Row(
                          mainAxisAlignment: MainAxisAlignment.center,
                          crossAxisAlignment: CrossAxisAlignment.center,
                          children: <Widget>[
                            Text(
                              '${I18n.of(context).vaultCreationWaitingHeader} ',
                              textAlign: TextAlign.center,
                              style: TextStyle(
                                color: UIConstants.gray100,
                                fontWeight: FontWeight.w600,
                                fontSize: width * 0.05,
                              ),
                            ),
                            Padding(
                              padding: EdgeInsets.only(top: width * 0.025),
                              child: const SizedBox(
                                height: 8,
                                child: SpinKitThreeBounce(
                                  color: UIConstants.gray100,
                                  size: 8,
                                ),
                              ),
                            ),
                          ],
                        ),
                ),
              ),
              SizedBox(
                width: width,
                height: height,
                child: PageView(
                  controller: _pageController,
                  onPageChanged: _onPageViewChange,
                  children: <Widget>[
                    VaultCreationContent(
                      title: I18n.of(context).vaultCreation1Title,
                      mainText: I18n.of(context).vaultCreation1MainTxt,
                      imagePath: 'assets/images/vault_1.png',
                      imageHeightFactor: 0.34,
                    ),
                    VaultCreationContent(
                      title: I18n.of(context).vaultCreation2Title,
                      mainText: I18n.of(context).vaultCreation2MainTxt,
                      imagePath: 'assets/images/intro_2.png',
                      imageHeightFactor: 0.34,
                    ),
                    VaultCreationContent(
                      title: I18n.of(context).vaultCreation3Title,
                      mainText: I18n.of(context).vaultCreation3MainTxt,
                      imagePath: 'assets/images/vault_3.png',
                      imageHeightFactor: 0.34,
                    ),
                    VaultCreationContent(
                      title: I18n.of(context).vaultCreation4Title,
                      mainText: I18n.of(context).vaultCreation4MainTxt,
                      imagePath: 'assets/images/vault_4.png',
                      imageHeightFactor: 0.34,
                    ),
                  ],
                ),
              ),
              Padding(
                padding: EdgeInsets.only(bottom: height * 0.173),
                child: Align(
                  alignment: Alignment.bottomCenter,
                  // Used for indicate slider position, we need to highlight
                  // slider index manually please refer intro.dart class comment,
                  // why we use [DotsIndicator] & [SpinKitThreeBounce] both in app.
                  child: DotsIndicator(
                    dotsCount: 4,
                    position: _currentPage,
                    decorator: const DotsDecorator(
                      color: UIConstants.paleLilac,
                      activeColor: UIConstants.slateGray100,
                    ),
                  ),
                ),
              ),
              Padding(
                padding: EdgeInsets.only(
                  right: width * 0.075,
                  left: width * 0.075,
                  bottom: height * 0.05,
                ),
                child: Align(
                  alignment: Alignment.bottomCenter,
                  child: MainButton(
                    type: _isVaultGenerated ? BtnType.active : BtnType.awaited,
                    text: _isVaultGenerated
                        ? I18n.of(
                            context,
                          ).vaultCreationDoneBtnTxt
                        : I18n.of(context).vaultCreationWaitingBtnTxt,
                    call: () => _isVaultGenerated
                        ? Navigator.pushNamed(
                            context,
                            '/account/login',
                          )
                        : () {},
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }

  void _onPageViewChange(int page) {
    setState(() => _currentPage = page);
  }
}
