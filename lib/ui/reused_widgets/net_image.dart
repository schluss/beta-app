import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:schluss_beta_app/constants/ui_constants.dart';

class NetImage extends StatelessWidget {
  final String path;
  final double width;

  const NetImage({
    required this.path,
    required this.width,
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    double width = MediaQuery.of(context).size.width;

    return Align(
      alignment: Alignment.center,
      child: path.isNotEmpty
          ? path.endsWith('.svg')
              ? SizedBox(
                  width: width * width,
                  height: width * width,
                  child: SvgPicture.network(path),
                )
              : CachedNetworkImage(
                  imageUrl: path,
                  width: width * width,
                  height: width * width,
                  errorWidget: (context, url, error) => const Icon(
                    Icons.error_outline,
                    color: UIConstants.blackFaded,
                  ),
                )
          : Image(
              image: const AssetImage(
                'assets/images/media_placeholder_icon.png',
              ),
              width: width * width,
            ),
    );
  }
}
