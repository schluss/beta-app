import 'package:flutter/material.dart';
import 'package:schluss_beta_app/constants/ui_constants.dart';
import 'package:schluss_beta_app/translations/i18n.dart';
import 'package:schluss_beta_app/ui/dashboard/base.dart';
import 'package:schluss_beta_app/ui/reused_widgets/main_button.dart';
import 'package:schluss_beta_app/ui/reused_widgets/mini_closing_button.dart';
import 'package:schluss_beta_app/ui/reused_widgets/progress_bar.dart';
import 'package:schluss_beta_app/ui/reused_widgets/provider_logo.dart';
import 'package:schluss_beta_app/util/initials_picker_util.dart';

class NotificationCard extends StatefulWidget {
  final String logoImg;
  final String logoColor;
  final bool isUser;
  final String title;
  final String subtitle;
  final String doneDate;
  final String description;
  final num totalValue;
  final num progressiveValue;
  final String activeBtnText;
  final Function? activeBtnCall;
  final Function? closeBtnCall;

  const NotificationCard({
    this.logoImg = '',
    this.logoColor = '',
    this.isUser = false,
    this.title = '',
    this.subtitle = '',
    this.doneDate = '',
    this.description = '',
    this.totalValue = 0,
    this.progressiveValue = 0,
    this.activeBtnText = '',
    this.activeBtnCall,
    this.closeBtnCall,
    Key? key,
  }) : super(key: key);

  @override
  NotificationCardState createState() => NotificationCardState();
}

class NotificationCardState extends State<NotificationCard> {
  final InitialsPickerUtil _initialsPickerUtil = InitialsPickerUtil();

  var _progressValue = 0.0;
  var _progressValueText = '0%';

  @override
  void initState() {
    super.initState();

    if (widget.totalValue != 0) {
      getProgressValue();
    }
  }

  @override
  Widget build(BuildContext context) {
    double width = MediaQuery.of(context).size.width;

    return Container(
      padding: EdgeInsets.symmetric(horizontal: getPadding(context)),
      child: Card(
        elevation: 1,
        margin: EdgeInsets.only(top: width * 0.03, left: 0, right: 0),
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(16.0),
        ),
        child: Container(
          margin: EdgeInsets.all(width * 0.06),
          width: width,
          child: Stack(
            children: <Widget>[
              Column(
                crossAxisAlignment: CrossAxisAlignment.center,
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: <Widget>[
                  Row(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: <Widget>[
                      /// Logo.
                      !widget.isUser
                          ? widget.logoImg != ''
                              ? ProviderLogo(logoImg: widget.logoImg)
                              : Container()
                          : ProviderLogo(
                              isOrg: false,
                              logoColor: Color(int.parse(widget.logoColor)),
                              logoLetters: _initialsPickerUtil.getInitials(widget.title).toString(),
                            ),

                      /// Title/subtitle/done date.
                      Expanded(
                        flex: 7,
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: <Widget>[
                            SizedBox(
                              width: widget.closeBtnCall != null || widget.logoImg != '' || widget.logoColor != '' ? width * 0.63 : width * 0.78,
                              child: Text(
                                widget.title,
                                style: TextStyle(
                                  color: UIConstants.grayDarkest100,
                                  fontWeight: FontWeight.w600,
                                  fontSize: width * 0.045,
                                ),
                              ),
                            ),
                            widget.subtitle != ''
                                ? Text(
                                    widget.subtitle,
                                    style: TextStyle(
                                      color: UIConstants.slateGray100,
                                      fontSize: width * 0.045,
                                    ),
                                  )
                                : Container(),
                            widget.doneDate != ''
                                ? Text(
                                    widget.doneDate,
                                    style: TextStyle(
                                      color: UIConstants.slateGray100,
                                      fontSize: width * 0.045,
                                    ),
                                  )
                                : Container(),
                          ],
                        ),
                      ),

                      /// Mini close button.
                      widget.closeBtnCall != null
                          ? MiniClosingButton(
                              call: widget.closeBtnCall!,
                            )
                          : Container(),
                    ],
                  ),

                  /// Description.
                  widget.description != ''
                      ? Padding(
                          padding: widget.logoImg != ''
                              ? EdgeInsets.only(top: getPadding(context))
                              : EdgeInsets.only(
                                  top: getPadding(context) - 8.0,
                                ),
                          child: Text(
                            widget.description,
                            maxLines: 10,
                            overflow: TextOverflow.ellipsis,
                            style: TextStyle(
                              height: 1.4,
                              fontSize: width * 0.045,
                              color: UIConstants.gray100,
                            ),
                          ),
                        )
                      : Container(),

                  /// Progress bar.
                  widget.totalValue != 0
                      ? Padding(
                          padding: EdgeInsets.only(top: getPadding(context)),
                          child: Column(
                            children: <Widget>[
                              Row(
                                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                children: <Widget>[
                                  Text(
                                    I18n.of(context).progBarLabelTxt,
                                    style: TextStyle(
                                      fontFamily: UIConstants.subFontFamily,
                                      fontSize: width * 0.04,
                                      color: UIConstants.slateGray100,
                                    ),
                                  ),
                                  Text(
                                    _progressValueText,
                                    style: TextStyle(
                                      fontFamily: UIConstants.subFontFamily,
                                      fontSize: width * 0.04,
                                      color: UIConstants.slateGray100,
                                    ),
                                  )
                                ],
                              ),
                              Padding(
                                padding: const EdgeInsets.only(top: 4.0),
                                child: ProgressBar(value: _progressValue),
                              ),
                            ],
                          ),
                        )
                      : Container(),

                  /// Main button.
                  widget.activeBtnText != ''
                      ? Padding(
                          padding: EdgeInsets.only(top: getPadding(context)),
                          child: MainButton(
                            type: BtnType.active,
                            text: widget.activeBtnText,
                            call: widget.activeBtnCall,
                          ),
                        )
                      : Container(),
                ],
              ),
            ],
          ),
        ),
      ),
    );
  }

  /// Updates the progress value.
  void getProgressValue() {
    int completedCount;
    int total;
    completedCount = 0;
    total = widget.totalValue as int;

    try {
      completedCount = widget.progressiveValue as int;
    } catch (e) {
      completedCount = 0;
    }
    _progressValue = (completedCount / total);
    _progressValueText = '${(_progressValue * 100).toInt()}%';
  }
}
