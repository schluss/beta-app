import 'package:flutter/material.dart';
import 'package:schluss_beta_app/constants/ui_constants.dart';

class ProgressBar extends StatefulWidget {
  final double startValue;
  final double value;
  final int duration;

  const ProgressBar({
    required this.value,
    this.startValue = 0.0,
    this.duration = 1500,
    Key? key,
  }) : super(key: key);

  @override
  ProgressBarState createState() => ProgressBarState();
}

class ProgressBarState extends State<ProgressBar> with TickerProviderStateMixin {
  late AnimationController controller;
  late Animation<double> animation;

  @override
  void initState() {
    super.initState();
    setUpAnimation();
    controller.forward();
  }

  @override
  void dispose() {
    controller.stop();
    super.dispose();
  }

  @override
  void didUpdateWidget(ProgressBar oldWidget) {
    super.didUpdateWidget(oldWidget);
    controller.stop();
    setUpAnimation();
    controller.forward();
  }

  void setUpAnimation() {
    controller = AnimationController(
      duration: Duration(milliseconds: widget.duration),
      vsync: this,
    );

    animation = Tween(
      begin: widget.startValue,
      end: widget.value,
    ).animate(
      CurvedAnimation(
        parent: controller,
        curve: Curves.easeInOutQuart,
      ),
    )..addListener(
        () {
          setState(() {});
        },
      );
  }

  @override
  Widget build(BuildContext context) {
    return Center(
      child: Container(
        height: 3,
        decoration: BoxDecoration(borderRadius: BorderRadius.circular(1.5)),
        child: LinearProgressIndicator(
          value: animation.value,
          valueColor: const AlwaysStoppedAnimation<Color>(UIConstants.greenBlue),
          backgroundColor: UIConstants.grayLight,
        ),
      ),
    );
  }
}
