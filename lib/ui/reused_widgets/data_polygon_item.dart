import 'package:flutter/material.dart';
import 'package:flutter_polygon/flutter_polygon.dart';
import 'package:schluss_beta_app/constants/ui_constants.dart';

class DataPolygonItem extends StatelessWidget {
  final String logoImg;
  final Color logoColor;
  final String text;

  const DataPolygonItem({
    required this.logoImg,
    required this.logoColor,
    required this.text,
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    double width = MediaQuery.of(context).size.width;
    double height = MediaQuery.of(context).size.height;

    return Padding(
      padding: EdgeInsets.only(right: width * 0.02),
      child: GestureDetector(
        onTap: () {
          Navigator.pushNamed(
            context,
            '/data/data/info-new',
            arguments: {
              'logoImg': logoImg,
              'logoColor': logoColor,
              'label': text,
            },
          );
        },
        child: Column(
          mainAxisAlignment: MainAxisAlignment.start,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            Padding(
              padding: EdgeInsets.only(bottom: height * 0.005),
              child: SizedBox(
                height: height * 0.1,
                child: ClipPolygon(
                  sides: 6,
                  borderRadius: 8.0,
                  rotate: 0.0,
                  child: Container(
                    color: logoColor,
                    alignment: Alignment.center,
                    child: Image(
                      image: AssetImage(logoImg),
                      width: width * 0.1,
                    ),
                  ),
                ),
              ),
            ),
            Text(
              text,
              textAlign: TextAlign.center,
              maxLines: 2,
              overflow: TextOverflow.ellipsis,
              softWrap: true,
              style: TextStyle(
                color: UIConstants.slateGray100,
                fontWeight: FontWeight.w400,
                fontSize: width * 0.041,
              ),
            ),
          ],
        ),
      ),
    );
  }
}
