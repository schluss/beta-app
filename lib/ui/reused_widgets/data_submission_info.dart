import 'package:flutter/material.dart';
import 'package:schluss_beta_app/constants/ui_constants.dart';

class DataSubmissionInfo extends StatelessWidget {
  final String text;
  final String value;
  final double widthFactor;

  const DataSubmissionInfo({
    required this.text,
    required this.value,
    this.widthFactor = 0.425,
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    double width = MediaQuery.of(context).size.width;

    return Column(
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      crossAxisAlignment: CrossAxisAlignment.start,
      children: <Widget>[
        Padding(
          padding: const EdgeInsets.only(bottom: 4),
          child: Text(
            text,
            style: TextStyle(
              fontFamily: UIConstants.subFontFamily,
              fontWeight: FontWeight.w600,
              color: UIConstants.gray100,
              letterSpacing: 1.375,
              fontSize: width * 0.0325,
            ),
          ),
        ),
        ConstrainedBox(
          constraints: BoxConstraints(maxWidth: width * widthFactor),
          child: Text(
            value,
            overflow: TextOverflow.ellipsis,
            style: TextStyle(
              color: UIConstants.grayDarkest100,
              fontWeight: FontWeight.w600,
              fontSize: width * 0.045,
            ),
          ),
        ),
      ],
    );
  }
}
