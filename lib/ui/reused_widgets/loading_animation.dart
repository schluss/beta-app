import 'package:flutter/material.dart';
import 'package:schluss_beta_app/constants/ui_constants.dart';

class LoadingAnimation extends StatefulWidget {
  final double width;
  final double height;
  final double radius;

  const LoadingAnimation({
    required this.width,
    required this.height,
    this.radius = 0,
    Key? key,
  }) : super(key: key);

  @override
  LoadingRectangleState createState() => LoadingRectangleState();
}

class LoadingRectangleState extends State<LoadingAnimation> with SingleTickerProviderStateMixin {
  late AnimationController _controller;
  late Animation _animation;

  @override
  void initState() {
    super.initState();

    _controller = AnimationController(
      vsync: this,
      duration: const Duration(
        milliseconds: 1000,
      ),
    )..repeat(
        reverse: true,
      );

    _animation = Tween(
      begin: 0.0,
      end: 1.0,
    ).animate(_controller);
  }

  @override
  void dispose() {
    _controller.dispose();

    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return FadeTransition(
      opacity: _animation as Animation<double>,
      child: Container(
        width: widget.width,
        height: widget.height,
        decoration: BoxDecoration(
          borderRadius: BorderRadius.all(Radius.circular(widget.radius)),
          gradient: const LinearGradient(
            colors: [
              UIConstants.paleGray,
              UIConstants.grayLighter,
              UIConstants.paleGray,
            ],
            stops: [0, 0.4903333333333335, 1],
            begin: Alignment(-1.00, 0.00),
            end: Alignment(1.00, -0.00),
          ),
        ),
      ),
    );
  }
}
