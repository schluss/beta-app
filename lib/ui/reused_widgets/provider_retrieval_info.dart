import 'package:flutter/material.dart';
import 'package:schluss_beta_app/constants/ui_constants.dart';

class ProviderRetrievalInfo extends StatelessWidget {
  final String title;
  final String subTitle;
  final bool isTitleBold;
  final bool isSubTitleBold;
  final double heightScale;

  const ProviderRetrievalInfo({
    required this.title,
    required this.subTitle,
    this.isTitleBold = false,
    this.isSubTitleBold = false,
    this.heightScale = 0.1,
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    double width = MediaQuery.of(context).size.width;
    double height = MediaQuery.of(context).size.height;

    return Container(
      height: height * heightScale,
      width: width,
      color: UIConstants.primaryColor,
      child: Padding(
        padding: EdgeInsets.only(right: width * 0.075, left: width * 0.075),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            Text(
              title,
              style: TextStyle(
                fontFamily: UIConstants.subFontFamily,
                fontWeight: isTitleBold ? FontWeight.w600 : FontWeight.w400,
                color: UIConstants.gray100,
                fontStyle: FontStyle.normal,
                letterSpacing: 1.375,
                fontSize: width * 0.03,
              ),
            ),
            Text(
              subTitle,
              style: TextStyle(
                color: UIConstants.grayDarkest100,
                fontWeight: isSubTitleBold ? FontWeight.w600 : FontWeight.w400,
                fontStyle: FontStyle.normal,
                letterSpacing: 0,
                fontSize: width * 0.04,
              ),
            ),
          ],
        ),
      ),
    );
  }
}
