import 'package:flutter/material.dart';
import 'package:schluss_beta_app/constants/ui_constants.dart';

class DeleteKey extends StatefulWidget {
  final Color color;
  final Color pressedBtnColor;
  final Function call;

  const DeleteKey({
    this.color = UIConstants.gray100,
    this.pressedBtnColor = UIConstants.grayLight,
    required this.call,
    Key? key,
  }) : super(key: key);

  @override
  DeleteKeyState createState() => DeleteKeyState();
}

class DeleteKeyState extends State<DeleteKey> {
  var tappedDown = false;

  @override
  Widget build(BuildContext context) {
    double width = MediaQuery.of(context).size.width;
    double height = MediaQuery.of(context).size.height;

    return GestureDetector(
      onTapDown: (details) => {setState(() => tappedDown = true)},
      onTapUp: (details) {
        setState(() => tappedDown = false);
        widget.call();
      },
      child: Container(
        margin: EdgeInsets.only(bottom: height * 0.025),
        height: width * 0.2,
        width: width * 0.2,
        decoration: BoxDecoration(
          color: tappedDown ? widget.pressedBtnColor : null,
          shape: BoxShape.circle,
        ),
        child: Center(
          child: Image.asset(
            'assets/images/delete_icon.png',
            height: width * 0.065,
            color: widget.color,
          ),
        ),
      ),
    );
  }
}
