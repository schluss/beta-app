import 'package:flutter/material.dart';
import 'package:schluss_beta_app/constants/ui_constants.dart';

class UserShiftingHeaderTemplate extends StatelessWidget {
  final String text;

  const UserShiftingHeaderTemplate({
    required this.text,
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    double height = MediaQuery.of(context).size.height;
    double width = MediaQuery.of(context).size.width;

    var displayText = text;

    if (displayText.contains(' ')) {
      displayText = displayText.split(' ')[0];
    }

    // cut of the title text when it's too long
    if (displayText.length > 18) {
      displayText = '${displayText.substring(0, 16)}...';
    }

    return SliverAppBar(
      elevation: 0,
      pinned: true,
      automaticallyImplyLeading: false,
      expandedHeight: height * 0.1,
      backgroundColor: UIConstants.primaryColor,
      flexibleSpace: FlexibleSpaceBar(
        titlePadding: const EdgeInsetsDirectional.only(start: 18, bottom: 22),
        centerTitle: false,
        title: GestureDetector(
          onTap: () => Navigator.pushNamed(context, '/vault/switch'),
          child: Row(
            children: [
              Padding(
                padding: EdgeInsets.only(top: height * 0.005),
                child: Image(
                  image: const AssetImage('assets/images/schluss_logo_pink.png'),
                  width: width * 0.08,
                ),
              ),
              Padding(
                padding: EdgeInsets.only(left: width * 0.02),
                child: Text(
                  displayText,
                  textAlign: TextAlign.left,
                  style: TextStyle(
                    color: UIConstants.black,
                    fontSize: height * 0.025,
                    fontWeight: FontWeight.bold,
                  ),
                ),
              ),
              Icon(
                Icons.keyboard_arrow_down_outlined,
                color: UIConstants.black,
                size: height * 0.028,
              ),
            ],
          ),
        ),
      ),
      bottom: PreferredSize(
        preferredSize: const Size(0, 10),
        child: Container(),
      ),
    );
  }
}
