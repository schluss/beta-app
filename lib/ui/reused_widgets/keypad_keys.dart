import 'package:flutter/material.dart';

Widget buildTouchIDIcon(BuildContext context, Function call, {Color? color}) {
  double width = MediaQuery.of(context).size.width;

  return SizedBox(
    height: width * 0.125,
    width: width * 0.125,
  );

  // TODO: in near future implement [touchID].
//  return GestureDetector(
//     onTap: call,
//     child: Padding(
//       padding: EdgeInsets.only(bottom: width * 0.025),
//       child: Container(
//         height: width * 0.125,
//         width: width * 0.125,
//         decoration: const BoxDecoration(
//           shape: BoxShape.circle,
//         ),
//         child: Center(
//           child: Image.asset('assets/images/touch_id_icon.png', height: width * 0.07, color: color,),
//         ),
//       ),
//     ),
//   );
}
