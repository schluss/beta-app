import 'package:flutter/material.dart';
import 'package:flutter_widget_from_html_core/flutter_widget_from_html_core.dart';
import 'package:schluss_beta_app/constants/ui_constants.dart';
import 'package:schluss_beta_app/ui/reused_widgets/bottom_up_sheet_template.dart';
import 'package:schluss_beta_app/ui/reused_widgets/main_button.dart';
import 'package:schluss_beta_app/ui/reused_widgets/main_title.dart';

class UserFeaturePageContentsTemplate extends StatelessWidget {
  final String miniTitle;
  final bool isCentered;
  final Image? topImage;
  final Alignment titleAlign;
  final String title;
  final TextAlign titleTextAlign;
  final String para1TextPart1;
  final String para1TextPart2;
  final String para1TextPart3;
  final TextAlign para1TextAlign;
  final String para2Text;
  final bool hasDivider;
  final Widget? middleImage;
  final String cardHeaderText;
  final Widget? cardContent;
  final String linkText;
  final String outlineBtnText;
  final Function? outlineBtnCall;
  final String activeBtnText;
  final Function? activeBtnCall;
  final bool activeBtnDisabledOnClick;
  final double heightRatio;
  final bool isScrollable;

  const UserFeaturePageContentsTemplate({
    required this.miniTitle,
    this.isCentered = false,
    this.topImage,
    this.titleAlign = Alignment.centerLeft,
    this.title = '',
    this.titleTextAlign = TextAlign.left,
    required this.para1TextPart1,
    this.para1TextPart2 = '',
    this.para1TextPart3 = '',
    this.para1TextAlign = TextAlign.left,
    this.para2Text = '',
    this.hasDivider = false,
    this.middleImage,
    this.cardHeaderText = '',
    this.cardContent,
    this.linkText = '',
    this.outlineBtnText = '',
    this.outlineBtnCall,
    this.activeBtnText = '',
    this.activeBtnCall,
    this.activeBtnDisabledOnClick = false,
    this.heightRatio = 0.0,
    this.isScrollable = true,
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    double width = MediaQuery.of(context).size.width;
    double height = MediaQuery.of(context).size.height;

    return BottomUpSheetTemplate(
      heightRatio: heightRatio,
      miniTitle: miniTitle,
      isScrollable: true,
      page: Scaffold(
        backgroundColor: UIConstants.primaryColor,
        body: SingleChildScrollView(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            mainAxisSize: MainAxisSize.min,
            children: [
              Column(
                children: [
                  Column(
                    mainAxisAlignment: !isCentered ? MainAxisAlignment.start : MainAxisAlignment.center,
                    crossAxisAlignment: !isCentered ? CrossAxisAlignment.start : CrossAxisAlignment.center,
                    children: [
                      /// Top image.
                      if (topImage != null)
                        Padding(
                          padding: EdgeInsets.only(top: height * 0.03),
                          child: Align(
                            alignment: Alignment.center,
                            child: topImage,
                          ),
                        ),

                      /// Title.
                      if (title != '')
                        Padding(
                          padding: EdgeInsets.only(top: topImage == null ? height * 0.03 : height * 0.02),
                          child: Align(
                            alignment: titleAlign,
                            child: MainTitle(
                              text: title,
                              textHeight: 1.25,
                              textAlign: titleTextAlign,
                            ),
                          ),
                        ),

                      /// Description paragraph 1.
                      Padding(
                        padding: EdgeInsets.symmetric(vertical: height * 0.02),
                        child: RichText(
                          textAlign: para1TextAlign,
                          text: TextSpan(
                            style: TextStyle(
                              color: UIConstants.slateGray100,
                              fontFamily: UIConstants.defaultFontFamily,
                              fontSize: width * 0.04,
                            ),
                            children: <TextSpan>[
                              TextSpan(text: para1TextPart1),
                              TextSpan(
                                text: para1TextPart2,
                                style: const TextStyle(
                                  fontWeight: FontWeight.bold,
                                ),
                              ),
                              TextSpan(text: para1TextPart3),
                            ],
                          ),
                        ),
                      ),

                      /// Description paragraph 2.
                      if (para2Text != '')
                        Padding(
                          padding: EdgeInsets.only(
                            bottom: height * 0.02,
                          ),
                          child: Text(
                            para2Text,
                            style: TextStyle(
                              color: UIConstants.slateGray100,
                              fontSize: width * 0.04,
                            ),
                          ),
                        ),

                      /// Divider line.
                      if (hasDivider) const Divider(height: 1),

                      /// Middle image.
                      if (middleImage != null)
                        Padding(
                          padding: EdgeInsets.symmetric(vertical: height * 0.02),
                          child: middleImage,
                        ),

                      /// Card.
                      if (cardHeaderText != '')
                        Card(
                          elevation: 0,
                          margin: const EdgeInsets.all(0.0),
                          shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(8.0),
                            side: const BorderSide(
                              color: UIConstants.grayLight,
                              width: 1.0,
                            ),
                          ),
                          child: ClipPath(
                            clipper: ShapeBorderClipper(
                              shape: RoundedRectangleBorder(
                                borderRadius: BorderRadius.circular(8.0),
                              ),
                            ),
                            child: Column(
                              children: [
                                Container(
                                  width: width,
                                  color: UIConstants.grayLight,
                                  child: Padding(
                                    padding: EdgeInsets.symmetric(
                                      vertical: height * 0.01,
                                      horizontal: width * 0.04,
                                    ),
                                    child: Text(
                                      cardHeaderText,
                                      style: TextStyle(
                                        color: UIConstants.gray100,
                                        fontSize: width * 0.04,
                                      ),
                                    ),
                                  ),
                                ),
                                Padding(
                                  padding: EdgeInsets.symmetric(
                                    vertical: width * 0.025,
                                    horizontal: width * 0.04,
                                  ),
                                  child: cardContent,
                                ),
                              ],
                            ),
                          ),
                        ),

                      Padding(
                        padding: EdgeInsets.only(
                          top: height * 0.04,
                        ),
                        child: HtmlWidget(
                          '<center>$linkText</center>',
                          onTapUrl: (url) {
                            return false;
                          },
                          textStyle: TextStyle(
                            fontSize: width * 0.04,
                            color: UIConstants.gray100,
                          ),
                        ),
                      ),
                    ],
                  ),
                ],
              ),
            ],
          ),
        ),
        bottomNavigationBar: Container(
          color: UIConstants.primaryColor,
          child: Column(
            mainAxisSize: MainAxisSize.min,
            children: [
              /// Cancel button.
              if (outlineBtnText != '')
                Padding(
                  padding: EdgeInsets.only(top: height * 0.03),
                  child: Align(
                    alignment: Alignment.bottomCenter,
                    child: SizedBox(
                      child: MainButton(
                        type: BtnType.outline,
                        text: outlineBtnText,
                        call: outlineBtnCall,
                      ),
                    ),
                  ),
                ),

              /// Main button.
              if (activeBtnText != '')
                Padding(
                  padding: EdgeInsets.only(
                    top: height * 0.03,
                    bottom: height * 0.025,
                  ),
                  child: Align(
                    alignment: Alignment.bottomCenter,
                    child: MainButton(
                      type: BtnType.active,
                      text: activeBtnText,
                      call: activeBtnCall,
                      isDisabled: activeBtnDisabledOnClick,
                    ),
                  ),
                ),
            ],
          ),
        ),
      ),
    );
  }
}
