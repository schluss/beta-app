import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:schluss_beta_app/constants/ui_constants.dart';

class MainTitle extends StatelessWidget {
  final String text;
  final TextAlign textAlign;
  final double textSize;
  final double? textHeight;
  final Color textColor;

  const MainTitle({
    required this.text,
    this.textAlign = TextAlign.center,
    this.textSize = 0.065,
    this.textHeight,
    this.textColor = UIConstants.grayDarkest100,
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    double width = MediaQuery.of(context).size.width;

    return Text(
      text,
      textAlign: textAlign,
      style: TextStyle(
        fontWeight: FontWeight.bold,
        color: textColor,
        fontSize: width * textSize,
        height: textHeight,
      ),
    );
  }
}
