import 'package:flutter/material.dart';
import 'package:open_file_safe/open_file_safe.dart';
import 'package:schluss_beta_app/constants/ui_constants.dart';
import 'package:schluss_beta_app/controller/my_data_controller.dart';
import 'package:schluss_beta_app/translations/i18n.dart';
import 'package:schluss_beta_app/ui/reused_widgets/provider_retrieval_info.dart';
import 'package:schluss_beta_app/ui/reused_widgets/tap_tip.dart';
import 'package:schluss_beta_app/view_model/field_view_model.dart';

class DataListItem extends StatefulWidget {
  final FieldViewModel model;

  // TODO: [fieldViewModelDecrypted!.value] loaded in here should always be in
  //  encrypted state. Show a waiting box while decrypting and show content
  //  (or start file generation when it is a file) when decrypted.
  final bool isDecryptionNeeded;

  const DataListItem({
    required this.model,
    this.isDecryptionNeeded = false,
    Key? key,
  }) : super(key: key);

  @override
  DataListItemState createState() => DataListItemState();
}

class DataListItemState extends State<DataListItem> {
  final MyDataController _myDataController = MyDataController();
  FieldViewModel? fieldViewModelDecrypted = FieldViewModel();
  bool isLoading = true;

  @override
  void initState() {
    super.initState();

    initData();
  }

  void initData() async {
    if (widget.isDecryptionNeeded && widget.model.type != 'file') {
      // Run the decryption operation in such a way it is cancelled when
      // the widget is disposed before operation completes.
      fieldViewModelDecrypted = await _myDataController.getDataDetailsDecrypted(widget.model);
      if (!mounted) return;

      setState(() {
        isLoading = false;
      });
    } else {
      fieldViewModelDecrypted = widget.model;
      setState(() {
        isLoading = false;
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    Widget widgetScreen;
    double width = MediaQuery.of(context).size.width;
    double height = MediaQuery.of(context).size.height;

    switch (widget.model.type) {
      case 'file':
        if (widget.model.displayValue != null) {
          widgetScreen = GestureDetector(
            onTap: () => _onDownloadClicked(widget.model),
            child: Stack(
              children: <Widget>[
                ProviderRetrievalInfo(
                  title: widget.model.label!.toUpperCase(),
                  subTitle: widget.model.displayValue ??
                      I18n.of(
                        context,
                      ).dataListItemOpenFileTxt,
                ),
                Container(
                  height: height * 0.1,
                  alignment: Alignment.centerRight,
                  padding: EdgeInsets.only(right: width * 0.075),
                  child: Image(
                    image: const AssetImage('assets/images/download_icon.png'),
                    width: width * 0.1,
                  ),
                ),
              ],
            ),
          );
        } else {
          widgetScreen = Stack(
            children: <Widget>[
              ProviderRetrievalInfo(
                title: widget.model.label!.toUpperCase(),
                subTitle: '-',
              ),
            ],
          );
        }
        break;

      case 'list':
        if (!isLoading && fieldViewModelDecrypted!.value != null) {
          widgetScreen = GestureDetector(
            onTap: () {
              Navigator.pushNamed(
                context,
                '/data/data/info',
                arguments: widget.model,
              );
            },
            child: Stack(
              children: <Widget>[
                ProviderRetrievalInfo(
                  title: widget.model.label!.toUpperCase(),
                  subTitle: widget.model.displayValue ??
                      I18n.of(
                        context,
                      ).dataListItemShowDataTxt,
                ),
                Container(
                  height: height * 0.1,
                  alignment: Alignment.centerRight,
                  padding: EdgeInsets.only(right: width * 0.075),
                  child: Image(
                    image: const AssetImage('assets/images/open_link_icon.png'),
                    width: width * 0.1,
                  ),
                ),
              ],
            ),
          );
        } else {
          widgetScreen = Stack(
            children: <Widget>[
              ProviderRetrievalInfo(
                title: widget.model.label!.toUpperCase(),
                subTitle: '-',
              ),
            ],
          );
        }
        break;

      default:
        var value = "-";

        if (fieldViewModelDecrypted!.isVC && fieldViewModelDecrypted!.displayValue != null) {
          value = fieldViewModelDecrypted!.displayValue!;
        } else if (fieldViewModelDecrypted!.value != null) {
          value = fieldViewModelDecrypted!.value!;
        }

        widgetScreen = Stack(
          children: <Widget>[
            ProviderRetrievalInfo(
              title: widget.model.label!.toUpperCase(),
              subTitle: value,
            ),
            if (fieldViewModelDecrypted!.isVC)
              Container(
                height: height * 0.1,
                alignment: Alignment.topRight,
                padding: EdgeInsets.only(
                  top: width * 0.05,
                  right: width * 0.075,
                ),
                child: TapTip(
                  linkWidget: Image(
                    image: const AssetImage(
                      'assets/images/verified_icon.png',
                    ),
                    width: width * 0.06,
                  ),
                  tipDistance: 14.0,
                  text: I18n.of(context).dataVerifiedTxt,
                  color: UIConstants.greenBlue,
                ),
              ),
          ],
        );
    }

    return widgetScreen;
  }

  /// On pressed download file icon event.
  void _onDownloadClicked(FieldViewModel data) async {
    MyDataController myDataController = MyDataController();
    if (widget.isDecryptionNeeded) {
      data.isDecrypted = false;
    } else {
      data.isDecrypted = true;
    }

    String filePath = await myDataController.generateFile(data);
    await OpenFile.open(filePath);
  }
}
