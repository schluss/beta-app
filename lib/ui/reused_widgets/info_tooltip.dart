import 'package:flutter/material.dart';
import 'package:schluss_beta_app/constants/ui_constants.dart';
import 'package:super_tooltip/super_tooltip.dart';

class InfoTooltip extends StatelessWidget {
  final String linkText;
  final String text;

  const InfoTooltip({
    required this.linkText,
    required this.text,
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    double width = MediaQuery.of(context).size.width;

    return GestureDetector(
      onTap: () => buildSuperTooltip(context).show(context),
      child: Row(
        children: [
          Text(
            linkText,
            style: TextStyle(
              fontSize: width * 0.05,
              decoration: TextDecoration.underline,
              color: UIConstants.accentColor,
            ),
          ),
          Padding(
            padding: EdgeInsets.only(bottom: width * 0.04),
            child: SizedBox(
              width: width * 0.0375,
              height: width * 0.0375,
              child: Image.asset('assets/images/info_icon.png'),
            ),
          ),
        ],
      ),
    );
  }

  SuperTooltip buildSuperTooltip(BuildContext context) {
    RenderBox renderBox = context.findRenderObject() as RenderBox;
    final RenderBox? overlay = Overlay.of(
      context,
    ).context.findRenderObject() as RenderBox?;
    var targetGlobalCenter = renderBox.localToGlobal(
      renderBox.size.topCenter(Offset.zero),
      ancestor: overlay,
    );

    return SuperTooltip(
      backgroundColor: UIConstants.grayDarkest100,
      borderRadius: 5,
      showCloseButton: ShowCloseButton.inside,
      closeButtonColor: UIConstants.primaryColor,
      maxWidth: MediaQuery.of(context).size.width * 0.7,
      borderColor: UIConstants.grayDarkest100,
      arrowTipDistance: 10,
      arrowLength: 10,
      touchThrougArea: Rect.fromCircle(
        center: targetGlobalCenter,
        radius: double.infinity,
      ),
      outsideBackgroundColor: UIConstants.transparent,
      hasShadow: false,
      closeButtonSize: MediaQuery.of(context).size.width * 0.06,
      popupDirection: TooltipDirection.down,
      content: Material(
        color: UIConstants.grayDarkest100,
        child: Padding(
          padding: const EdgeInsets.all(8.0),
          child: Text(
            text,
            style: TextStyle(
              color: UIConstants.primaryColor,
              fontSize: MediaQuery.of(context).size.width * 0.045,
            ),
            softWrap: true,
          ),
        ),
      ),
    );
  }
}
