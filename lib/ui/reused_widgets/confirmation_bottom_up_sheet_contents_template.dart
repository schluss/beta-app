import 'package:flutter/material.dart';
import 'package:schluss_beta_app/constants/ui_constants.dart';
import 'package:schluss_beta_app/translations/i18n.dart';
import 'package:schluss_beta_app/ui/reused_widgets/bottom_up_sheet_template.dart';
import 'package:schluss_beta_app/ui/reused_widgets/main_button.dart';
import 'package:schluss_beta_app/ui/reused_widgets/main_title.dart';

class ConfirmationBottomUpSheetContentsTemplate extends StatelessWidget {
  final Image? topIcon;
  final String title;
  final String? cardHeaderText;
  final Widget? cardContent;
  final String mainText;
  final String activeBtnText;
  final Function activeBtnCall;
  final int popCount;

  const ConfirmationBottomUpSheetContentsTemplate({
    this.topIcon,
    required this.title,
    this.cardHeaderText = '',
    this.cardContent,
    required this.mainText,
    required this.activeBtnText,
    required this.activeBtnCall,
    this.popCount = 1,
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    int count = 0;
    double width = MediaQuery.of(context).size.width;
    double height = MediaQuery.of(context).size.height;

    return BottomUpSheetTemplate(
      page: Scaffold(
        body: Container(
          height: height,
          color: UIConstants.primaryColor,
          child: Column(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            mainAxisSize: MainAxisSize.min,
            children: [
              Column(
                children: [
                  /// Delete icon.
                  Align(
                    alignment: Alignment.centerLeft,
                    child: Container(
                      decoration: const BoxDecoration(
                        shape: BoxShape.circle,
                        color: UIConstants.paleGray,
                      ),
                      padding: EdgeInsets.all(width * 0.05),
                      child: topIcon ??
                          Image(
                            image: const AssetImage('assets/images/bin_icon.png'),
                            height: width * 0.084,
                          ),
                    ),
                  ),

                  /// Title.
                  Padding(
                    padding: EdgeInsets.symmetric(vertical: height * 0.03),
                    child: Align(
                      alignment: Alignment.centerLeft,
                      child: MainTitle(
                        text: title,
                        textHeight: 1.25,
                        textAlign: TextAlign.left,
                      ),
                    ),
                  ),

                  /// Card.
                  cardHeaderText != ''
                      ? Card(
                          elevation: 0,
                          margin: const EdgeInsets.all(0.0),
                          shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(8.0),
                            side: const BorderSide(
                              color: UIConstants.grayLight,
                              width: 1.0,
                            ),
                          ),
                          child: ClipPath(
                            clipper: ShapeBorderClipper(
                              shape: RoundedRectangleBorder(
                                borderRadius: BorderRadius.circular(8.0),
                              ),
                            ),
                            child: Column(
                              children: [
                                Container(
                                  width: width,
                                  color: UIConstants.grayLight,
                                  child: Padding(
                                    padding: EdgeInsets.symmetric(
                                      vertical: height * 0.01,
                                      horizontal: width * 0.04,
                                    ),
                                    child: Text(
                                      cardHeaderText!,
                                      style: TextStyle(
                                        color: UIConstants.gray100,
                                        fontSize: width * 0.05,
                                      ),
                                    ),
                                  ),
                                ),
                                Padding(
                                  padding: EdgeInsets.symmetric(
                                    vertical: width * 0.025,
                                    horizontal: width * 0.04,
                                  ),
                                  child: cardContent,
                                ),
                              ],
                            ),
                          ),
                        )
                      : Container(),

                  /// Description.
                  Text(
                    mainText,
                    style: TextStyle(
                      color: UIConstants.slateGray100,
                      fontSize: width * 0.0475,
                    ),
                  ),
                ],
              ),
              Column(
                mainAxisAlignment: MainAxisAlignment.end,
                children: [
                  /// Cancel button.
                  Padding(
                    padding: EdgeInsets.only(bottom: height * 0.03),
                    child: Align(
                      alignment: Alignment.bottomCenter,
                      child: MainButton(
                        type: BtnType.outline,
                        text: I18n.of(context).btnNoGoBackTxt,
                        call: () => Navigator.of(context).popUntil(
                          (_) => count++ >= popCount,
                        ),
                      ),
                    ),
                  ),

                  /// Main button.
                  Padding(
                    padding: EdgeInsets.only(bottom: height * 0.025),
                    child: Align(
                      alignment: Alignment.bottomCenter,
                      child: MainButton(
                        isDisabled: true,
                        type: BtnType.active,
                        text: activeBtnText,
                        call: activeBtnCall,
                      ),
                    ),
                  ),
                ],
              ),
            ],
          ),
        ),
      ),
      heightRatio: 0.15,
    );
  }
}
