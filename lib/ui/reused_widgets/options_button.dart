import 'package:flutter/material.dart';
import 'package:schluss_beta_app/constants/ui_constants.dart';

class OptionsButton extends StatelessWidget {
  final Function? call;

  const OptionsButton({
    this.call,
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.only(top: 8, right: 12),
      child: Container(
        decoration: const BoxDecoration(
          color: UIConstants.paleGray,
          borderRadius: BorderRadius.all(
            Radius.circular(8),
          ),
        ),
        child: IconButton(
          iconSize: 32,
          icon: const Icon(
            Icons.more_horiz,
            color: UIConstants.gray100,
          ),
          onPressed: call as void Function()?,
        ),
      ),
    );
  }
}
