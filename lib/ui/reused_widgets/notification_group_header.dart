import 'package:flutter/material.dart';
import 'package:schluss_beta_app/constants/ui_constants.dart';
import 'package:schluss_beta_app/ui/dashboard/base.dart';

import 'package:schluss_beta_app/ui/reused_widgets/mini_closing_button.dart';

class NotificationGroupHeader extends StatelessWidget {
  final String text;
  final Function? closeBtnCall;

  const NotificationGroupHeader({
    this.text = '',
    this.closeBtnCall,
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    double width = MediaQuery.of(context).size.width;

    return Padding(
      padding: EdgeInsets.only(
        top: getPadding(context),
        left: getPadding(context),
        right: getPadding(context),
      ),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        crossAxisAlignment: CrossAxisAlignment.end,
        children: <Widget>[
          Text(
            text,
            style: TextStyle(
              color: UIConstants.gray100,
              fontSize: width * 0.04,
            ),
          ),
          closeBtnCall != null
              ? MiniClosingButton(
                  color: UIConstants.grayLight,
                  call: closeBtnCall!,
                )
              : Container(),
        ],
      ),
    );
  }
}
