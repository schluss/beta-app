import 'package:flutter/material.dart';
import 'package:flutter_switch/flutter_switch.dart';
import 'package:schluss_beta_app/constants/ui_constants.dart';

class SwitchButton extends StatelessWidget {
  final bool isSwitchOn;
  final Function? callback;

  const SwitchButton({
    required this.isSwitchOn,
    this.callback,
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    double width = MediaQuery.of(context).size.width;
    double height = MediaQuery.of(context).size.height;

    return FlutterSwitch(
      width: width * 0.18,
      height: height * 0.0475,
      toggleSize: height * 0.04,
      value: isSwitchOn,
      borderRadius: 30.0,
      padding: 3.0,
      toggleColor: UIConstants.primaryColor,
      activeColor: UIConstants.accentColor,
      inactiveColor: UIConstants.paleLilac,
      showOnOff: false,
      onToggle: (val) => callback!(val),
    );
  }
}
