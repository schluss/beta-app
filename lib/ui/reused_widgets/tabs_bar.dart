import 'package:flutter/material.dart';
import 'package:schluss_beta_app/constants/ui_constants.dart';

class TabsBar extends StatelessWidget {
  final List<Tab> tabs;
  final TabController tabController;
  final Alignment align;

  const TabsBar({
    required this.tabs,
    required this.tabController,
    this.align = Alignment.center,
    Key? key,
  }) : super(key: key);

  @override

  Widget build(BuildContext context) {
    return Align(
      alignment: align,
      child: TabBar(
        isScrollable: true,
        controller: tabController,
        indicatorWeight: 1,
        labelColor: UIConstants.grayDarkest100,
        unselectedLabelColor: UIConstants.gray100,
        indicatorColor: UIConstants.grayDarkest100,
        labelStyle: TextStyle(
          fontFamily: UIConstants.defaultFontFamily,
          fontSize: MediaQuery.of(context).size.width * 0.0425,
        ),
        tabs: getTabs(context),
        indicatorSize: TabBarIndicatorSize.label,
      ),
    );
  }

  List<Widget> getTabs(BuildContext context) {
    List<Widget> children = <Widget>[];

    for (int i = 0; i < tabs.length; i++) {
      children.add(buildTabContainer(index: i, context: context));
    }
    return children;
  }

  SizedBox buildTabContainer({
    required BuildContext context,
    required int index,
  }) {
    double height = MediaQuery.of(context).size.height;
    double width = MediaQuery.of(context).size.width;

    return SizedBox(
      height: height * 0.05,
      child: SizedBox(
        width: width * 0.4,
        child: Tab(text: tabs[index].text),
      ),
    );
  }
}
