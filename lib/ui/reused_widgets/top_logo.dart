import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:schluss_beta_app/constants/ui_constants.dart';

class TopLogo extends StatelessWidget {
  final String imgPath;
  final double imgWidth;
  final bool? isLogoPink;
  final double? topPadding;

  const TopLogo({
    required this.imgPath,
    required this.imgWidth,
    this.isLogoPink = true,
    this.topPadding = 0.0,
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: EdgeInsets.only(top: topPadding!),
      child: Align(
        alignment: Alignment.topCenter,
        child: SvgPicture.asset(
          imgPath,
          width: imgWidth,
          colorFilter: ColorFilter.mode(isLogoPink == true ? UIConstants.accentColor : UIConstants.primaryColor, BlendMode.srcIn),
        ),
      ),
    );
  }
}
