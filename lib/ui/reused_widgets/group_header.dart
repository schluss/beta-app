import 'package:flutter/material.dart';
import 'package:schluss_beta_app/constants/ui_constants.dart';

class GroupHeader extends StatelessWidget {
  final String text;
  final double leftPadding;

  const GroupHeader({
    required this.text,
    this.leftPadding = 0.075,
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    double width = MediaQuery.of(context).size.width;
    double height = MediaQuery.of(context).size.height;

    return Container(
      width: width,
      padding: EdgeInsets.only(
        left: width * leftPadding,
        top: height * 0.0125,
        bottom: height * 0.0125,
        right: height * 0.008,
      ),
      decoration: const BoxDecoration(
        color: UIConstants.paleGray,
        border: Border(
          top: BorderSide(color: UIConstants.paleLilac),
          bottom: BorderSide(color: UIConstants.paleLilac),
        ),
      ),
      child: Text(
        text,
        maxLines: 1,
        style: TextStyle(
          fontFamily: UIConstants.subFontFamily,
          color: UIConstants.slateGray100,
          fontSize: width * 0.03,
          fontWeight: FontWeight.w600,
          fontStyle: FontStyle.normal,
          letterSpacing: 1,
        ),
      ),
    );
  }
}
