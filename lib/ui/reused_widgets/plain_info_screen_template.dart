import 'package:flutter/material.dart';
import 'package:schluss_beta_app/constants/ui_constants.dart';
import 'package:schluss_beta_app/ui/reused_widgets/backward_button.dart';
import 'package:schluss_beta_app/ui/reused_widgets/main_title.dart';

class PlainInfoScreenTemplate extends StatelessWidget {
  final AssetImage img;
  final String title;
  final String mainText;
  final Column dynamicContent;
  final int backBtnPopCount;

  const PlainInfoScreenTemplate({
    required this.img,
    required this.title,
    required this.mainText,
    required this.dynamicContent,
    this.backBtnPopCount = 1,
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    double width = MediaQuery.of(context).size.width;
    double height = MediaQuery.of(context).size.height;

    return SafeArea(
      child: Scaffold(
        backgroundColor: UIConstants.paleGray,
        body: Stack(
          children: <Widget>[
            /// Back button.
            BackwardButton(popCount: backBtnPopCount),
            Center(
              child: Padding(
                padding: EdgeInsets.only(
                  left: width * 0.1,
                  right: width * 0.1,
                  bottom: height * 0.0625,
                ),
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    /// Image.
                    Image(
                      image: img,
                      width: width * 0.2,
                    ),

                    /// Main title.
                    Padding(
                      padding: EdgeInsets.symmetric(
                        horizontal: width * 0.05,
                        vertical: width * 0.04,
                      ),
                      child: MainTitle(text: title),
                    ),

                    /// Description.
                    Text(
                      mainText,
                      maxLines: 5,
                      overflow: TextOverflow.ellipsis,
                      textAlign: TextAlign.center,
                      style: TextStyle(
                        color: UIConstants.slateGray100,
                        fontSize: width * 0.048,
                      ),
                    )
                  ],
                ),
              ),
            ),

            /// Page content.
            Padding(
              padding: EdgeInsets.only(
                right: width * 0.075,
                left: width * 0.075,
                bottom: height * 0.05,
              ),
              child: Align(
                alignment: Alignment.bottomCenter,
                child: dynamicContent,
              ),
            )
          ],
        ),
      ),
    );
  }
}
