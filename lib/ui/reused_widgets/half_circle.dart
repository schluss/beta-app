import 'dart:math' as math;

import 'package:flutter/material.dart';
import 'package:schluss_beta_app/constants/ui_constants.dart';

class HalfCircle extends StatelessWidget {
  final double diameter;
  final Color color;

  const HalfCircle({
    this.diameter = 0.06,
    this.color = UIConstants.grayLight,
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    double width = MediaQuery.of(context).size.width;

    return CustomPaint(
      painter: ArcPainter(color),
      size: Size(width * diameter, width * diameter),
    );
  }
}

class ArcPainter extends CustomPainter {
  final Color color;

  ArcPainter(this.color);

  @override
  void paint(Canvas canvas, Size size) {
    Paint paint = Paint()..color = color;
    canvas.drawArc(
      Rect.fromCenter(
        center: Offset(size.height / 2, size.width),
        height: size.height,
        width: size.width,
      ),
      math.pi,
      math.pi,
      false,
      paint,
    );
  }

  @override
  bool shouldRepaint(CustomPainter oldDelegate) => false;
}
