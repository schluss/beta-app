import 'package:flutter/material.dart';
import 'package:schluss_beta_app/constants/ui_constants.dart';
import 'package:schluss_beta_app/ui/reused_widgets/add_button.dart';
import 'package:schluss_beta_app/ui/reused_widgets/backward_button.dart';

class HeaderTemplate extends StatelessWidget {
  final String text;
  final Function? addBtnCall;
  final Function? backBtnCall;

  const HeaderTemplate({
    required this.text,
    this.addBtnCall,
    this.backBtnCall,
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    double height = MediaQuery.of(context).size.height;
    double width = MediaQuery.of(context).size.width;

    return SliverAppBar(
      elevation: 0,
      pinned: true,
      automaticallyImplyLeading: false,
      expandedHeight: height * 0.1,
      backgroundColor: UIConstants.primaryColor,
      flexibleSpace: Padding(
        padding: const EdgeInsets.only(bottom: 5),
        child: FlexibleSpaceBar(
          /// Title.
          centerTitle: true,
          title: Align(
            alignment: Alignment.bottomCenter,
            child: Text(
              text,
              textAlign: TextAlign.center,
              style: TextStyle(
                color: UIConstants.black,
                fontSize: height * 0.025,
                fontWeight: FontWeight.bold,
              ),
            ),
          ),
        ),
      ),
      actions: <Widget>[
        /// Back button.
        backBtnCall != null
            ? Align(
                alignment: Alignment.topLeft,
                child: Padding(
                  padding: EdgeInsets.only(
                    right: addBtnCall != null ? width * 0.458 : width * 0.60,
                  ),
                  child: const BackwardButton(),
                ),
              )
            : Container(),

        /// Add button.
        addBtnCall != null
            ? Padding(
                padding: EdgeInsets.only(right: width * 0.01),
                child: AddButton(
                  call: addBtnCall!,
                ),
              )
            : Container(),
      ],
      bottom: PreferredSize(
        preferredSize: const Size(0, 10),
        child: Container(),
      ),
    );
  }
}
