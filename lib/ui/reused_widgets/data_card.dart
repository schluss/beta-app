import 'package:flutter/material.dart';
import 'package:flutter_polygon/flutter_polygon.dart';
import 'package:schluss_beta_app/constants/ui_constants.dart';
import 'package:schluss_beta_app/translations/i18n.dart';

class DataCard extends StatelessWidget {
  final String logoImg;
  final Color logoColor;
  final String title;
  final String count;

  const DataCard({
    required this.logoImg,
    required this.logoColor,
    required this.title,
    required this.count,
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    double width = MediaQuery.of(context).size.width;
    double height = MediaQuery.of(context).size.height;

    return GestureDetector(
      onTap: () {
        Navigator.pushNamed(
          context,
          '/data/data-new',
          arguments: {
            'logoImg': logoImg,
            'logoColor': logoColor,
            'title': title,
          },
        );
      },
      child: Card(
        semanticContainer: false,
        margin: EdgeInsets.all(width * 0.02),
        color: UIConstants.primaryColor,
        child: Container(
          margin: const EdgeInsets.all(0.0),
          padding: EdgeInsets.symmetric(
            horizontal: width * 0.025,
            vertical: width * 0.01,
          ),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              SizedBox(
                height: height * 0.08,
                child: ClipPolygon(
                  sides: 6,
                  borderRadius: 8.0,
                  rotate: 0.0,
                  child: Container(
                    color: logoColor,
                    alignment: Alignment.center,
                    child: Image(
                      image: AssetImage(logoImg),
                      width: width * 0.06,
                    ),
                  ),
                ),
              ),
              Padding(
                padding: EdgeInsets.only(left: width * 0.01),
                child: Text(
                  title,
                  textAlign: TextAlign.start,
                  style: TextStyle(
                    color: UIConstants.grayDarkest100,
                    fontWeight: FontWeight.w600,
                    fontSize: width * 0.043,
                  ),
                ),
              ),
              Padding(
                padding: EdgeInsets.only(left: width * 0.01),
                child: Text(
                  '$count ${I18n.of(context).dataCategoryAttributesSubtitle}',
                  textAlign: TextAlign.start,
                  style: TextStyle(
                    color: UIConstants.slateGray100,
                    fontWeight: FontWeight.w400,
                    fontSize: width * 0.043,
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
