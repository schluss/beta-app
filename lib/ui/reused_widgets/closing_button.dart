import 'package:flutter/material.dart';
import 'package:schluss_beta_app/constants/ui_constants.dart';

class ClosingButton extends StatelessWidget {
  final Color backgroundColor;
  final Color iconColor;
  final Function call;

  const ClosingButton({
    this.backgroundColor = UIConstants.primaryColor,
    this.iconColor = UIConstants.accentColor,
    required this.call,
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    double width = MediaQuery.of(context).size.width;
    double height = MediaQuery.of(context).size.height;

    return Align(
      alignment: Alignment.topRight,
      child: GestureDetector(
        onTap: call as void Function()?,
        child: Container(
          margin: EdgeInsets.only(
            top: height * 0.02,
          ),
          color: backgroundColor,
          child: Container(
            width: width * 0.095,
            height: width * 0.095,
            alignment: Alignment.center,
            decoration: BoxDecoration(
              shape: BoxShape.circle,
              color: backgroundColor,
              border: Border.all(
                color: UIConstants.paleLilac,
                width: 0.5,
              ),
            ),
            child: Icon(
              Icons.close,
              color: iconColor,
              size: width * 0.05,
            ),
          ),
        ),
      ),
    );
  }
}
