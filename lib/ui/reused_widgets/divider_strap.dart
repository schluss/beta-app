import 'package:flutter/material.dart';
import 'package:schluss_beta_app/constants/ui_constants.dart';

class DividerStrap extends StatelessWidget {
  final bool hasBorder;
  final String textPart1;
  final String? textPart2;
  final TextAlign textAlign;

  const DividerStrap({
    this.hasBorder = true,
    this.textPart1 = '',
    this.textPart2 = '',
    this.textAlign = TextAlign.center,
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    double width = MediaQuery.of(context).size.width;
    double height = MediaQuery.of(context).size.height;

    return Container(
      width: width,
      decoration: BoxDecoration(
        color: UIConstants.paleGray,
        border: hasBorder
            ? const Border(
                top: BorderSide(color: UIConstants.paleLilac),
                bottom: BorderSide(color: UIConstants.paleLilac),
              )
            : null,
      ),
      child: Padding(
        padding: const EdgeInsets.only(top: 4.0),
        child: Column(
          children: [
            textPart1 != ''
                ? Padding(
                    padding: const EdgeInsets.symmetric(
                      vertical: 4.0,
                      horizontal: 20.0,
                    ),
                    child: Text(
                      textPart1,
                      textAlign: textAlign,
                      style: TextStyle(
                        color: UIConstants.gray100,
                        fontSize: height * 0.015,
                        fontFamily: UIConstants.subFontFamily,
                      ),
                    ),
                  )
                : Container(),
            textPart2 != ''
                ? Padding(
                    padding: const EdgeInsets.only(top: 0.0),
                    child: Text(
                      textPart2!,
                      textAlign: textAlign,
                      style: TextStyle(
                        color: UIConstants.gray100,
                        fontSize: height * 0.015,
                        fontFamily: UIConstants.subFontFamily,
                      ),
                    ),
                  )
                : const Padding(
                    padding: EdgeInsets.only(top: 4.0),
                  ),
          ],
        ),
      ),
    );
  }
}
