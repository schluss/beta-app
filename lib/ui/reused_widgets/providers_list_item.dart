import 'package:flutter/material.dart';
import 'package:schluss_beta_app/constants/ui_constants.dart';
import 'package:schluss_beta_app/ui/reused_widgets/provider_logo.dart';

class ProvidersListItem extends StatelessWidget {
  final String logo;

  /// Checks icon is not needed for request complete screen.
  final bool isCheckedEnabled;
  final bool isChecked;

  final String title;
  final String subTitle;

  // To display a small tip, indicating the item is going to be retrieved from inside the other vault
  final String? from;

  final bool hasLeftPadding;
  final bool hasArrow;

  const ProvidersListItem({
    this.logo = '',
    this.isChecked = false,
    this.isCheckedEnabled = true,
    required this.title,
    this.subTitle = '',
    this.from,
    this.hasLeftPadding = true,
    this.hasArrow = true,
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    double width = MediaQuery.of(context).size.width;

    return Padding(
      padding: EdgeInsets.only(
        left: hasLeftPadding ? width * 0.075 : 0,
        right: width * 0.075,
        top: width * 0.025,
        bottom: width * 0.025,
      ),
      child: Container(
        alignment: Alignment.center,
        height: MediaQuery.of(context).size.height * 0.095,
        child: Row(
          children: [
            // Logo
            logo.isNotEmpty
                ? ProviderLogo(
                    logoImg: logo,
                    isChecked: isChecked,
                    isCheckedEnabled: isCheckedEnabled,
                  )
                : Container(),

            // Titles
            Column(
              mainAxisAlignment: MainAxisAlignment.center,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                // Title
                ConstrainedBox(
                  constraints: BoxConstraints(maxWidth: width * 0.6),
                  child: Text(
                    title,
                    softWrap: true,
                    overflow: TextOverflow.ellipsis,
                    style: TextStyle(
                      color: UIConstants.grayDarkest100,
                      fontWeight: FontWeight.w600,
                      fontStyle: FontStyle.normal,
                      letterSpacing: 0,
                      fontSize: MediaQuery.of(context).size.width * 0.045,
                    ),
                  ),
                ),

                // Subtitle
                subTitle.isNotEmpty
                    ? ConstrainedBox(
                        constraints: BoxConstraints(maxWidth: width * 0.6),
                        child: Text(
                          subTitle,
                          maxLines: 1,
                          softWrap: true,
                          overflow: TextOverflow.ellipsis,
                          style: TextStyle(
                            fontFamily: UIConstants.subFontFamily,
                            color: UIConstants.gray100,
                            fontWeight: FontWeight.w400,
                            fontStyle: FontStyle.normal,
                            letterSpacing: 0,
                            fontSize: MediaQuery.of(context).size.width * 0.041,
                          ),
                        ),
                      )
                    : Container(),

                // When to display a small tip indicating this data will be retrieved from the other vault the user is in to
                from != null
                    ? ConstrainedBox(
                        constraints: BoxConstraints(maxWidth: width * 0.6),
                        child: Container(
                          margin: const EdgeInsets.only(
                            top: 3,
                          ),
                          padding: const EdgeInsets.only(
                            left: 8,
                            right: 8,
                          ),
                          decoration: BoxDecoration(
                            color: UIConstants.grayDarkest100,
                            borderRadius: BorderRadius.circular(8.0),
                          ),
                          child: Text(
                            from!,
                            style: const TextStyle(color: Colors.white),
                          ),
                        ),
                      )
                    : Container(),
              ],
            ),
            const Spacer(),

            // Action
            if (hasArrow)
              Icon(
                Icons.keyboard_arrow_right,
                color: UIConstants.gray100,
                size: MediaQuery.of(context).size.width * 0.06,
              ),
          ],
        ),
      ),
    );
  }
}
