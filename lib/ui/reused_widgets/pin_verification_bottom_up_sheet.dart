import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:schluss_beta_app/constants/ui_constants.dart';
import 'package:schluss_beta_app/controller/account_controller.dart';
import 'package:schluss_beta_app/controller/plugin_controller.dart';
import 'package:schluss_beta_app/translations/i18n.dart';
import 'package:schluss_beta_app/ui/reused_widgets/bottom_up_sheet_template.dart';
import 'package:schluss_beta_app/ui/reused_widgets/keypad.dart';
import 'package:schluss_beta_app/ui/reused_widgets/matching_validation_notifier.dart';
import 'package:schluss_beta_app/ui/reused_widgets/pin_code.dart';
import 'package:schluss_beta_app/ui/reused_widgets/pin_screen_template.dart';
import 'package:schluss_beta_app/util/common_util.dart';
import 'package:schluss_beta_app/util/error_handler_util.dart';
import 'package:schluss_beta_app/view_model/request_view_model.dart';

enum ValidationState { primary, error, success }

enum ProcessType {
  submitDataRequest,
  changePin,
  exportData,
  grantOwnership,
  grantDataSharing,
  deleteAccount,
}

class PinVerificationBottomUpSheet extends StatefulWidget {
  final ProcessType? process;
  final RequestViewModel? requestModel;
  final PluginController? pluginController;

  const PinVerificationBottomUpSheet({
    this.process,
    this.requestModel,
    this.pluginController,
    Key? key,
  }) : super(key: key);

  @override
  PinVerificationBottomUpSheetState createState() => PinVerificationBottomUpSheetState();
}

class PinVerificationBottomUpSheetState extends State<PinVerificationBottomUpSheet> {
  final AccountController _accountController = AccountController();
  final CommonUtil _commonUtil = CommonUtil();
  final ErrorHandlerUtil _errorHandler = ErrorHandlerUtil();

  List<String> password = ['', '', '', '', ''];
  int inputIndex = 0;
  bool _isLoading = false;
  ValidationState isValid = ValidationState.primary;
  late bool internetStatus;

  @override
  Widget build(BuildContext context) {
    double width = MediaQuery.of(context).size.width;
    double height = MediaQuery.of(context).size.height;

    return BottomUpSheetTemplate(
      isHeaderNeeded: false,
      page: Stack(
        children: [
          PinScreenTemplate(
            /// Title text.
            text: (widget.process == ProcessType.changePin || widget.process == ProcessType.exportData || widget.process == ProcessType.deleteAccount)
                ? I18n.of(context).pinBottomShtTitle
                : (_isLoading)
                    ? I18n.of(context).pinBottomShtLoadingTitle
                    : I18n.of(context).pinBottomShtSubmitTitle,

            /// PIN / Loading area.
            pin: !_isLoading
                ? Row(
                    mainAxisAlignment: MainAxisAlignment.spaceAround,
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: [
                      Expanded(child: getPasswordItemByIndex(width, 0)),
                      Expanded(child: getPasswordItemByIndex(width, 1)),
                      Expanded(child: getPasswordItemByIndex(width, 2)),
                      Expanded(child: getPasswordItemByIndex(width, 3)),
                      Expanded(child: getPasswordItemByIndex(width, 4)),
                    ],
                  )
                : Container(),

            /// Validation messages.
            validationNotifier: Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                MatchingValidationNotifier(
                  text: _getNotificationMsg(),
                  isValid: _checkNotificationValidationStatus(),
                  invalidColor: UIConstants.primaryColor,
                ),
              ],
            ),

            /// Key pad / Loading area.
            keyPad: !_isLoading
                ? KeyPad(
                    handleKeyOne: onOneClick,
                    handleKeyTwo: onTwoClick,
                    handleKeyThree: onThreeClick,
                    handleKeyFour: onFourClick,
                    handleKeyFive: onFiveClick,
                    handleKeySix: onSixClick,
                    handleKeySeven: onSevenClick,
                    handleKeyEight: onEightClick,
                    handleKeyNine: onNineClick,
                    handleKeyZero: onZeroClick,
                    handleDeleteKey: onDeleteNumber,
                    paddingRatio: 0.00,
                    digitColor: UIConstants.primaryColor,
                    buttonColor: UIConstants.rosyPink,
                    pressedButtonColor: UIConstants.rosyPinkLight,
                    delButtonColor: UIConstants.primaryColor,
                  )
                : buildLoader(width, height),
            hasCloseBtn: true,
            marginRatio: 0.0,
            logoPaddingRatio: 0.01,
            isLogoPink: false,
            textPaddingRatio: 0.01,
            textColor: UIConstants.primaryColor,
            pinPaddingRatio: 0.01,
            validationNotifierPaddingRatio: 0.01,
            keypadPaddingRatio: 0.33,
          ),
        ],
      ),
      color: UIConstants.accentColor,
      btnCloseColor: UIConstants.primaryColor,
    );
  }

  /// Shows dots or asterisks/digits.
  Widget getPasswordItemByIndex(double width, int index) {
    return password[index].isEmpty
        ? buildEmptyPlaceholder(
            width,
            color: UIConstants.primaryColor.withOpacity(0.2),
          )
        : buildStarImage(width, color: UIConstants.primaryColor);
  }

  /// Assigns values for keys.
  void onOneClick() => handleUserInput('1');

  void onTwoClick() => handleUserInput('2');

  void onThreeClick() => handleUserInput('3');

  void onFourClick() => handleUserInput('4');

  void onFiveClick() => handleUserInput('5');

  void onSixClick() => handleUserInput('6');

  void onSevenClick() => handleUserInput('7');

  void onEightClick() => handleUserInput('8');

  void onNineClick() => handleUserInput('9');

  void onZeroClick() => handleUserInput('0');

  /// Handles PIN.
  Future<void> handleUserInput(String number) async {
    if (inputIndex != password.length) {
      password[inputIndex] = number;

      setState(() => isValid = ValidationState.primary);

      if (inputIndex != password.length) inputIndex++;

      if (inputIndex == password.length) checkValidity(password.join(''));
    }
  }

  /// Validates PIN.
  void checkValidity(String password) async {
    if (password.isEmpty) {
      return;
    }
    var isPinCorrect = await _accountController.validatePin(password);

    if (isPinCorrect) {
      setState(() => isValid = ValidationState.success);

      // Check the flow is sending the payload or changing the existing pin code.
      if (!mounted) return;
      if (widget.process == ProcessType.changePin) {
        Navigator.pushReplacementNamed(
          context,
          '/account/change-pin',
          arguments: password,
        );
      } else if (widget.process == ProcessType.exportData) {
        Navigator.pushReplacementNamed(
          context,
          '/account/export-data',
          arguments: password,
        );
      } else if (widget.process == ProcessType.submitDataRequest) {
        // Check internet availability.
        internetStatus = await _commonUtil.checkInternetConnectivity();
        if (!mounted) return;
        if (internetStatus) {
          setState(() => _isLoading = true);

          await widget.pluginController!
              .submitPayload(
            widget.requestModel,
          )
              .then(
            (value) {
              Navigator.pushNamed(
                context,
                '/data-request/submit',
                arguments: widget.requestModel,
              );
            },
          ).catchError((onError) {
            _errorHandler.handleError(onError, true, context);
          });
        } else {
          _errorHandler.handleError(
            'no-internet',
            true,
            context,
            'no-internet',
          );
        }
      } else if (widget.process == ProcessType.grantOwnership) {
        Navigator.pop(context, true);

        // TODO: need to be changed, after integrating the back-end process.
        // Navigator.pushNamed(
        //   context,
        //   '/stewardship/passed',
        //   arguments: {
        //     'stewardForename': 'Kees',
        //     'stewardSurname': 'Janssen',
        //   },
        // )

      } else if (widget.process == ProcessType.grantDataSharing) {
        Navigator.pop(context, true);
      } else if (widget.process == ProcessType.deleteAccount) {
        Navigator.pushNamed(context, '/account/deletion/confirm');
      }
    } else {
      resetPin();

      setState(() => isValid = ValidationState.error);
    }
  }

  /// Deletes all five digits.
  void resetPin() {
    for (var i = inputIndex; i >= 0; i--) {
      onDeleteNumber();
    }
  }

  /// Deletes the last digit.
  void onDeleteNumber() {
    if (inputIndex >= 0) {
      if (inputIndex != 0) inputIndex--;

      password[inputIndex] = '';

      setState(() => isValid = ValidationState.primary);
    }
  }

  /// Builds loading dots animation.
  Container buildLoader(double width, double height) {
    return Container(
      margin: EdgeInsets.only(bottom: height * 0.05),
      padding: EdgeInsets.symmetric(horizontal: width * 0.075),
      child: Center(
        child: Padding(
          padding: EdgeInsets.only(bottom: height / 6),
          child: const SpinKitThreeBounce(
            color: UIConstants.primaryColor,
            size: 25,
          ),
        ),
      ),
    );
  }

  /// Loads validation message.
  dynamic _getNotificationMsg() {
    var msg = '';

    if (isValid == ValidationState.error) {
      msg = I18n.of(context).pinBottomShtInvalidTxt;
    }

    return msg;
  }

  /// Checks validation message status to show the notification.
  /// If [false] - show the message.
  bool _checkNotificationValidationStatus() {
    bool status = true;

    if (isValid == ValidationState.error) {
      status = false;
    }
    return status;
  }
}
