import 'package:flutter/material.dart';
import 'package:schluss_beta_app/constants/ui_constants.dart';
import 'package:schluss_beta_app/ui/registration/pin_creation_page.dart';

class FormatValidationNotifier extends StatelessWidget {
  final String text;
  final ValidationState state;

  const FormatValidationNotifier({
    required this.text,
    required this.state,
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    double width = MediaQuery.of(context).size.width;

    return Container(
      margin: EdgeInsets.symmetric(horizontal: width * 0.025),
      padding: EdgeInsets.symmetric(
        horizontal: width * 0.04,
        vertical: width * 0.025,
      ),
      alignment: Alignment.centerLeft,
      decoration: BoxDecoration(
        color: _getBackgroundColor(),
        borderRadius: const BorderRadius.all(Radius.circular(8)),
      ),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          if (state != ValidationState.primary)
            Padding(
              padding: const EdgeInsets.only(right: 6),
              child: _getImageByStatus(width),
            ),
          ConstrainedBox(
            constraints: BoxConstraints(maxWidth: width * 0.35),
            child: Text(
              text,
              maxLines: 2,
              overflow: TextOverflow.ellipsis,
              style: _getTextStyleByStatus(width),
            ),
          )
        ],
      ),
    );
  }

  Color _getBackgroundColor() {
    var color = UIConstants.paleGray;
    if (state == ValidationState.error) {
      color = UIConstants.red.withOpacity(0.1);
    } else if (state == ValidationState.success) {
      color = UIConstants.greenLightest;
    }
    return color;
  }

  TextStyle _getTextStyleByStatus(double width) {
    var style = TextStyle(
      color: UIConstants.gray100,
      fontFamily: UIConstants.subFontFamily,
      fontWeight: FontWeight.w600,
      fontSize: width * 0.04,
    );
    if (state == ValidationState.error) {
      style = TextStyle(
        color: UIConstants.red,
        fontFamily: UIConstants.subFontFamily,
        fontWeight: FontWeight.w600,
        fontSize: width * 0.04,
      );
    } else if (state == ValidationState.success) {
      style = TextStyle(
        fontFamily: UIConstants.subFontFamily,
        color: UIConstants.greenDark,
        fontWeight: FontWeight.w600,
        fontSize: width * 0.04,
      );
    }
    return style;
  }

  Image _getImageByStatus(double width) {
    var image = Image.asset(
      'assets/images/white_cross_icon.png',
      color: UIConstants.red,
      height: width * 0.03,
    );
    if (state == ValidationState.success) {
      image = Image.asset(
        'assets/images/white_check_icon.png',
        color: UIConstants.greenDark,
        height: width * 0.03,
      );
    }
    return image;
  }
}
