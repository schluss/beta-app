import 'package:flutter/material.dart';
import 'package:schluss_beta_app/constants/ui_constants.dart';

class AnimatedCircleIcon extends StatelessWidget {
  final Widget img;
  final bool isShadowPink;
  final bool isError;

  const AnimatedCircleIcon({
    required this.img,
    this.isShadowPink = false,
    this.isError = false,
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    double width = MediaQuery.of(context).size.width;

    return Container(
      width: width * 0.25,
      height: width * 0.25,
      decoration: BoxDecoration(
        shape: BoxShape.circle,
        color: UIConstants.primaryColor,
        boxShadow: (isError)
            ? [
                const BoxShadow(
                  color: UIConstants.slateGray40,
                  offset: Offset(0, 5),
                  blurRadius: 10,
                  spreadRadius: -6,
                ),
              ]
            : [
                BoxShadow(
                  color: isShadowPink ? UIConstants.watermelonDark : UIConstants.slateGray40,
                  offset: const Offset(0, 10),
                  blurRadius: 20,
                  spreadRadius: 0,
                ),
              ],
      ),
      child: Align(
        alignment: Alignment.center,
        child: img,
      ),
    );
  }
}
