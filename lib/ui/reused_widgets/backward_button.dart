import 'package:flutter/material.dart';
import 'package:schluss_beta_app/constants/ui_constants.dart';
import 'package:schluss_beta_app/translations/i18n.dart';

class BackwardButton extends StatelessWidget {
  final String text;
  final Color color;
  final int popCount;
  final Function? call;

  const BackwardButton({
    this.text = '',
    this.color = UIConstants.gray100,
    this.popCount = 1,
    this.call,
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    double width = MediaQuery.of(context).size.width;
    double height = MediaQuery.of(context).size.height;

    int count = 0;

    return Align(
      alignment: Alignment.topLeft,
      child: GestureDetector(
        onTap: call == null
            ? () => Navigator.of(
                  context,
                ).popUntil((_) => count++ >= popCount)
            : null,
        child: Container(
          margin: EdgeInsets.only(
            top: height * 0.02,
            left: height * 0.02,
          ),
          width: width * 0.4,
          color: UIConstants.transparent,
          child: Row(
            crossAxisAlignment: CrossAxisAlignment.center,
            mainAxisAlignment: MainAxisAlignment.start,
            children: <Widget>[
              Icon(
                Icons.keyboard_arrow_left,
                color: color,
                size: width * 0.06,
              ),
              Container(
                margin: EdgeInsets.only(right: height * 0.02),
                child: Text(
                  text.isEmpty ? I18n.of(context).btnBackTxt : text,
                  textAlign: TextAlign.center,
                  style: TextStyle(
                    color: color,
                    fontFamily: UIConstants.subFontFamily,
                    fontSize: width * 0.042,
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
