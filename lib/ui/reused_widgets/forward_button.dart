import 'package:flutter/material.dart';
import 'package:schluss_beta_app/constants/ui_constants.dart';

class ForwardButton extends StatelessWidget {
  final String text;
  final Color color;
  final Function call;

  const ForwardButton({
    this.text = '',
    this.color = UIConstants.gray100,
    required this.call,
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    double width = MediaQuery.of(context).size.width;
    double height = MediaQuery.of(context).size.height;

    return Align(
      alignment: Alignment.topRight,
      child: GestureDetector(
        onTap: () => {call()},
        child: Container(
          margin: EdgeInsets.only(
            top: height * 0.02,
            right: height * 0.02,
          ),
          width: width * 0.4,
          color: UIConstants.transparent,
          child: Row(
            crossAxisAlignment: CrossAxisAlignment.center,
            mainAxisAlignment: MainAxisAlignment.end,
            children: <Widget>[
              Container(
                margin: EdgeInsets.only(left: height * 0.02),
                child: Text(
                  text,
                  textAlign: TextAlign.right,
                  style: TextStyle(
                    color: color,
                    fontFamily: UIConstants.subFontFamily,
                    fontSize: width * 0.042,
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
