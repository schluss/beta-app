import 'package:flutter/material.dart';
import 'package:schluss_beta_app/constants/theme.dart';
import 'package:schluss_beta_app/constants/ui_constants.dart';
import 'package:schluss_beta_app/ui/reused_widgets/add_button.dart';
import 'package:schluss_beta_app/ui/reused_widgets/backward_button.dart';
import 'package:schluss_beta_app/ui/reused_widgets/closing_button.dart';
import 'package:schluss_beta_app/ui/reused_widgets/options_button.dart';

class MiniHeaderTemplate extends StatelessWidget {
  final String? text;
  final bool hasBackBtn;
  final String? backBtnText;
  final Function? backBtnCall;
  final Function? closeBtnCall;
  final Function? optionsBtnCall;
  final Function? addBtnCall;
  final Widget? forwardBtn;
  final Color backgroundColor;

  const MiniHeaderTemplate({
    this.text,
    this.hasBackBtn = false,
    this.backBtnText,
    this.backBtnCall,
    this.closeBtnCall,
    this.addBtnCall,
    this.optionsBtnCall,
    this.forwardBtn,
    this.backgroundColor = UIConstants.primaryColor,
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    double width = MediaQuery.of(context).size.width;
    double height = MediaQuery.of(context).size.height;
    double topPadding = MediaQuery.of(context).padding.top;

    return Container(
      margin: EdgeInsets.only(top: topPadding),
      height: height * 0.085,
      decoration: BoxDecoration(
        color: backgroundColor,
        borderRadius: mainBorderRadius,
      ),
      alignment: Alignment.center,
      child: Stack(
        children: <Widget>[
          /// Title.
          Align(
            alignment: Alignment.topCenter,
            child: Container(
              margin: EdgeInsets.only(top: height * 0.04),
              color: UIConstants.transparent,
              child: ConstrainedBox(
                constraints: BoxConstraints(
                  maxWidth: width * 0.55,
                ),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: <Widget>[
                    text != null
                        ? Flexible(
                            child: Text(
                              text!,
                              maxLines: 1,
                              overflow: TextOverflow.ellipsis,
                              textAlign: TextAlign.center,
                              style: TextStyle(
                                color: UIConstants.grayDark,
                                fontSize: width * 0.04,
                                fontWeight: FontWeight.w700,
                                fontStyle: FontStyle.normal,
                              ),
                            ),
                          )
                        : Container(),
                  ],
                ),
              ),
            ),
          ),

          /// Back button or close button.
          hasBackBtn
              ? Padding(
                  padding: EdgeInsets.only(top: height * 0.01),
                  child: BackwardButton(
                    text: backBtnText ?? '',
                    call: backBtnCall,
                  ),
                )
              : Padding(
                  padding: EdgeInsets.only(
                    top: height * 0.01,
                    right: height * 0.03,
                  ),
                  child: ClosingButton(
                    call: closeBtnCall ?? () => Navigator.pop(context),
                  ),
                ),

          /// Add button.
          addBtnCall != null
              ? Align(
                  alignment: Alignment.topRight,
                  child: Padding(
                    padding: EdgeInsets.only(
                      right: optionsBtnCall != null ? width * 0.16 : width * 0.01,
                    ),
                    child: AddButton(
                      call: addBtnCall!,
                    ),
                  ),
                )
              : Container(),

          /// Options button.
          optionsBtnCall != null
              ? Align(
                  alignment: Alignment.topRight,
                  child: Padding(
                    padding: EdgeInsets.only(right: width * 0.01),
                    child: OptionsButton(
                      call: optionsBtnCall,
                    ),
                  ),
                )
              : Container(),

          forwardBtn != null
              ? Align(
                  alignment: Alignment.topRight,
                  child: Padding(
                    padding: EdgeInsets.only(
                      top: height * 0.01,
                      right: width * 0.02,
                    ),
                    child: forwardBtn,
                  ))
              : Container(),
        ],
      ),
    );
  }
}
