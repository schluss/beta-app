import 'package:flutter/material.dart';
import 'package:schluss_beta_app/constants/ui_constants.dart';

class FullWidthButton extends StatelessWidget {
  final String text;
  final Function call;

  const FullWidthButton({
    required this.text,
    required this.call,
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    double width = MediaQuery.of(context).size.width;
    double height = MediaQuery.of(context).size.height;

    return Container(
      color: UIConstants.primaryColor,
      child: Padding(
        padding: EdgeInsets.only(top: height * 0.0325),
        child: Material(
          color: UIConstants.primaryColor,
          child: InkWell(
            splashColor: UIConstants.blackFaded,
            onTap: call as void Function()?,
            child: Container(
              decoration: const BoxDecoration(
                border: Border(
                  top: BorderSide(color: UIConstants.paleLilac),
                  bottom: BorderSide(color: UIConstants.paleLilac),
                ),
              ),
              height: height * 0.085,
              child: Center(
                child: Text(
                  text,
                  style: TextStyle(
                    color: UIConstants.accentColor,
                    fontSize: width * 0.045,
                  ),
                ),
              ),
            ),
          ),
        ),
      ),
    );
  }
}
