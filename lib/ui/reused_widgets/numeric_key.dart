import 'package:flutter/material.dart';
import 'package:schluss_beta_app/constants/ui_constants.dart';

class NumericKey extends StatefulWidget {
  final String text;
  final Function call;
  final Color digitColor;
  final Color pressedButtonColor;
  final Color buttonColor;

  const NumericKey({
    Key? key,
    required this.text,
    required this.call,
    this.digitColor = UIConstants.accentColor,
    this.buttonColor = UIConstants.paleGray,
    this.pressedButtonColor = UIConstants.grayLight,
  }) : super(key: key);

  @override
  NumericKeyState createState() => NumericKeyState();
}

class NumericKeyState extends State<NumericKey> {
  var tappedDown = false;

  @override
  Widget build(BuildContext context) {
    double width = MediaQuery.of(context).size.width;
    double height = MediaQuery.of(context).size.height;

    return GestureDetector(
      onTapDown: (details) => {setState(() => tappedDown = true)},
      onTapUp: (details) {
        setState(() => tappedDown = false);
        widget.call();
      },
      child: Container(
        margin: EdgeInsets.only(bottom: height * 0.025),
        height: width * 0.2,
        width: width * 0.2,
        decoration: BoxDecoration(
          color: tappedDown ? widget.pressedButtonColor : widget.buttonColor,
          shape: BoxShape.circle,
        ),
        child: Center(
          child: Text(
            widget.text,
            textAlign: TextAlign.center,
            style: TextStyle(
              color: widget.digitColor,
              fontSize: width * 0.08,
            ),
          ),
        ),
      ),
    );
  }
}
