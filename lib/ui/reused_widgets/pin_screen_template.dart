import 'package:flutter/material.dart';
import 'package:schluss_beta_app/constants/ui_constants.dart';
import 'package:schluss_beta_app/ui/reused_widgets/closing_button.dart';
import 'package:schluss_beta_app/ui/reused_widgets/top_logo.dart';

class PinScreenTemplate extends StatelessWidget {
  final String text;
  final bool? hasCloseBtn;
  final Widget pin;
  final Widget validationNotifier;
  final Widget keyPad;
  final double marginRatio;
  final double logoPaddingRatio;
  final bool? isLogoPink;
  final double textPaddingRatio;
  final Color textColor;
  final Widget? tooltip;
  final double pinPaddingRatio;
  final Widget? pinEye;
  final double validationNotifierPaddingRatio;
  final double keypadPaddingRatio;

  const PinScreenTemplate({
    required this.text,
    required this.pin,
    required this.validationNotifier,
    required this.keyPad,
    this.hasCloseBtn = false,
    this.marginRatio = 0.025,
    this.logoPaddingRatio = 0.05,
    this.isLogoPink = true,
    this.textPaddingRatio = 0.025,
    this.textColor = UIConstants.gray100,
    this.tooltip,
    this.pinPaddingRatio = 0.025,
    this.pinEye,
    this.validationNotifierPaddingRatio = 0.025,
    this.keypadPaddingRatio = 0.38,
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    double width = MediaQuery.of(context).size.width;
    double height = MediaQuery.of(context).size.height;
    double topPadding = MediaQuery.of(context).padding.top;

    return Stack(
      children: [
        Container(
          alignment: Alignment.topCenter,
          margin: EdgeInsets.only(top: topPadding),
          child: Stack(
            children: [
              Positioned(
                child: Column(
                  children: [
                    Stack(
                      children: [
                        /// Close button.
                        if (hasCloseBtn == true)
                          ClosingButton(
                            backgroundColor: UIConstants.accentColor,
                            iconColor: UIConstants.primaryColor,
                            call: () => Navigator.pop(context),
                          ),

                        /// Logo.
                        Align(
                          alignment: Alignment.topCenter,
                          child: TopLogo(
                            imgPath: 'assets/images/logo.svg',
                            imgWidth: width / 7,
                            isLogoPink: isLogoPink,
                            topPadding: hasCloseBtn == true ? height * 0.05 : height * logoPaddingRatio,
                          ),
                        ),
                      ],
                    ),

                    /// Title.
                    Padding(
                      padding: EdgeInsets.only(top: hasCloseBtn == true ? height * 0.02 : height * textPaddingRatio),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          Text(
                            text,
                            style: TextStyle(
                              fontSize: width * 0.05,
                              color: textColor,
                            ),
                          ),
                          if (tooltip != null) tooltip!,
                        ],
                      ),
                    ),

                    /// PIN.
                    Padding(
                      padding: EdgeInsets.only(
                        top: height * pinPaddingRatio,
                      ),
                      child: Stack(
                        alignment: Alignment.center,
                        children: [
                          SizedBox(
                            width: width * 0.5,
                            height: width * 0.125,
                            child: pin,
                          ),

                          // PIN eye for making PIN visible.
                          if (pinEye != null) pinEye!,
                        ],
                      ),
                    ),

                    /// Validation messages.
                    Padding(
                      padding: EdgeInsets.only(
                        top: height * validationNotifierPaddingRatio,
                      ),
                      child: validationNotifier,
                    ),
                  ],
                ),
              ),
            ],
          ),
        ),

        /// Key pad.
        Padding(
          padding: EdgeInsets.only(
            top: height * keypadPaddingRatio,
          ),
          child: keyPad,
        ),
      ],
    );
  }
}
