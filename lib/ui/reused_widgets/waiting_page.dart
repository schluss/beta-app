import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:schluss_beta_app/constants/ui_constants.dart';
import 'package:schluss_beta_app/translations/i18n.dart';

class WaitingPage extends StatelessWidget {
  final Function? call;

  const WaitingPage({
    this.call,
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    double width = MediaQuery.of(context).size.width;
    double height = MediaQuery.of(context).size.height;

    return GestureDetector(
      onTap: call as void Function()?,
      child: Container(
        color: UIConstants.blackFaded,
        child: Stack(
          children: <Widget>[
            Align(
              alignment: Alignment.centerRight,
              child: Image(
                image: const AssetImage('assets/images/card_bg.png'),
                height: width * 0.97,
              ),
            ),
            Align(
              alignment: Alignment.center,
              child: SvgPicture.asset(
                'assets/images/logo.svg',
                colorFilter: const ColorFilter.mode(UIConstants.accentColor, BlendMode.srcIn),
                width: width * 0.4,
              ),
            ),
            Align(
              alignment: Alignment.topCenter,
              child: Padding(
                  padding: EdgeInsets.only(
                    top: height * 0.8,
                  ),
                  child: Text(
                    I18n.of(context).waitingPgTxt,
                    textAlign: TextAlign.center,
                    style: const TextStyle(
                      fontSize: 17.0,
                      color: UIConstants.grayDarkest70,
                      fontWeight: FontWeight.w600,
                    ),
                  )),
            ),
          ],
        ),
      ),
    );
  }
}
