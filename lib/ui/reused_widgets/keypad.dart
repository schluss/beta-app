import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:schluss_beta_app/constants/ui_constants.dart';
import 'package:schluss_beta_app/ui/reused_widgets/delete_key.dart';
import 'package:schluss_beta_app/ui/reused_widgets/keypad_keys.dart';
import 'package:schluss_beta_app/ui/reused_widgets/numeric_key.dart';

class KeyPad extends StatelessWidget {
  final Function handleKeyOne;
  final Function handleKeyTwo;
  final Function handleKeyThree;
  final Function handleKeyFour;
  final Function handleKeyFive;
  final Function handleKeySix;
  final Function handleKeySeven;
  final Function handleKeyEight;
  final Function handleKeyNine;
  final Function handleKeyZero;
  final Function handleDeleteKey;
  final double paddingRatio;
  final Color digitColor;
  final Color pressedButtonColor;
  final Color buttonColor;
  final Color delButtonColor;

  const KeyPad({
    Key? key,
    required this.handleKeyOne,
    required this.handleKeyTwo,
    required this.handleKeyThree,
    required this.handleKeyFour,
    required this.handleKeyFive,
    required this.handleKeySix,
    required this.handleKeySeven,
    required this.handleKeyEight,
    required this.handleKeyNine,
    required this.handleKeyZero,
    required this.handleDeleteKey,
    this.paddingRatio = 0.075,
    this.digitColor = UIConstants.accentColor,
    this.buttonColor = UIConstants.paleGray,
    this.pressedButtonColor = UIConstants.grayLight,
    this.delButtonColor = UIConstants.gray100,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    double width = MediaQuery.of(context).size.width;
    double height = MediaQuery.of(context).size.height;

    return Container(
      margin: EdgeInsets.only(bottom: height * 0.05),
      padding: EdgeInsets.symmetric(horizontal: width * paddingRatio),
      child: Center(
        child: Table(
          defaultVerticalAlignment: TableCellVerticalAlignment.middle,
          children: [
            TableRow(
              children: [
                NumericKey(
                  text: '1',
                  call: handleKeyOne,
                  digitColor: digitColor,
                  buttonColor: buttonColor,
                  pressedButtonColor: pressedButtonColor,
                ),
                NumericKey(
                  text: '2',
                  call: handleKeyTwo,
                  digitColor: digitColor,
                  buttonColor: buttonColor,
                  pressedButtonColor: pressedButtonColor,
                ),
                NumericKey(
                  text: '3',
                  call: handleKeyThree,
                  digitColor: digitColor,
                  buttonColor: buttonColor,
                  pressedButtonColor: pressedButtonColor,
                ),
              ],
            ),
            TableRow(
              children: [
                NumericKey(
                  text: '4',
                  call: handleKeyFour,
                  digitColor: digitColor,
                  buttonColor: buttonColor,
                  pressedButtonColor: pressedButtonColor,
                ),
                NumericKey(
                  text: '5',
                  call: handleKeyFive,
                  digitColor: digitColor,
                  buttonColor: buttonColor,
                  pressedButtonColor: pressedButtonColor,
                ),
                NumericKey(
                  text: '6',
                  call: handleKeySix,
                  digitColor: digitColor,
                  buttonColor: buttonColor,
                  pressedButtonColor: pressedButtonColor,
                ),
              ],
            ),
            TableRow(
              children: [
                NumericKey(
                  text: '7',
                  call: handleKeySeven,
                  digitColor: digitColor,
                  buttonColor: buttonColor,
                  pressedButtonColor: pressedButtonColor,
                ),
                NumericKey(
                  text: '8',
                  call: handleKeyEight,
                  digitColor: digitColor,
                  buttonColor: buttonColor,
                  pressedButtonColor: pressedButtonColor,
                ),
                NumericKey(
                  text: '9',
                  call: handleKeyNine,
                  digitColor: digitColor,
                  buttonColor: buttonColor,
                  pressedButtonColor: pressedButtonColor,
                ),
              ],
            ),
            TableRow(
              children: [
                buildTouchIDIcon(context, () {}),
                NumericKey(
                  text: '0',
                  call: handleKeyZero,
                  digitColor: digitColor,
                  buttonColor: buttonColor,
                  pressedButtonColor: pressedButtonColor,
                ),
                DeleteKey(
                  call: handleDeleteKey,
                  color: delButtonColor,
                  pressedBtnColor: pressedButtonColor,
                ),
              ],
            ),
          ],
        ),
      ),
    );
  }
}
