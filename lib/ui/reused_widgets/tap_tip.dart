import 'package:flutter/material.dart';
import 'package:schluss_beta_app/constants/ui_constants.dart';
import 'package:super_tooltip/super_tooltip.dart';

class TapTip extends StatelessWidget {
  final Widget linkWidget;
  final double tipDistance;
  final TooltipDirection direction;
  final bool hasCloseBtn;
  final String text;
  final Color textColor;
  final Color color;

  const TapTip({
    required this.linkWidget,
    this.tipDistance = 10.0,
    this.direction = TooltipDirection.down,
    this.hasCloseBtn = false,
    required this.text,
    this.textColor = UIConstants.primaryColor,
    this.color = UIConstants.grayDarkest100,
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () => buildSuperTooltip(context).show(context),
      child: linkWidget,
    );
  }

  SuperTooltip buildSuperTooltip(BuildContext context) {
    double width = MediaQuery.of(context).size.width;

    return SuperTooltip(
      popupDirection: direction,
      arrowTipDistance: tipDistance,
      arrowLength: 10.0,
      borderRadius: 8.0,
      borderColor: color,
      maxWidth: width * 0.85,
      backgroundColor: color,
      showCloseButton: hasCloseBtn ? ShowCloseButton.inside : ShowCloseButton.none,
      closeButtonSize: width * 0.06,
      closeButtonColor: textColor,
      outsideBackgroundColor: UIConstants.transparent,
      hasShadow: false,
      content: Material(
        color: color,
        child: Padding(
          padding: EdgeInsets.only(
            top: hasCloseBtn ? 16.0 : 8.0,
            left: 8.0,
            bottom: 8.0,
            right: 8.0,
          ),
          child: Text(
            text,
            style: TextStyle(
              color: textColor,
              fontSize: width * 0.045,
            ),
            softWrap: true,
          ),
        ),
      ),
    );
  }
}
