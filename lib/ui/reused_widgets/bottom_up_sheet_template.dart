import 'dart:ui';

import 'package:flutter/material.dart';
import 'package:schluss_beta_app/constants/theme.dart';
import 'package:schluss_beta_app/constants/ui_constants.dart';
import 'package:schluss_beta_app/ui/reused_widgets/add_button.dart';
import 'package:schluss_beta_app/ui/reused_widgets/backward_button.dart';
import 'package:schluss_beta_app/ui/reused_widgets/closing_button.dart';
import 'package:schluss_beta_app/ui/reused_widgets/options_button.dart';
import 'package:schluss_beta_app/ui/reused_widgets/top_logo.dart';

class BottomUpSheetTemplate extends StatelessWidget {
  /// Ratios for calculate the height of the bottom-up sheet.
  /// An optional parameter and default value is 1.0.
  final double heightRatio;

  /// An additional amounts for the height of the bottom-up sheet,
  /// which is loaded from an another bottom-up sheet.
  /// An optional parameter and default value is 0.00.
  final double heightAddition;

  /// Backgrounds colour of the disabled area of the bottom-up sheet.
  /// An optional parameter and default value is [UIConstants.grayShadow].
  final Color shadowColor;

  /// Backgrounds colour of the bottom-up sheet.
  /// An optional parameter and default value is [UIConstants.primaryColor].
  final Color color;

  final bool? isHeaderNeeded;

  final bool? hasBackBtn;
  final String? backBtnText;
  final Function? backBtnCall;

  final bool? hasLogo;
  final bool? isLogoPink;

  final String? miniTitle;
  final String? title;

  final Function? addBtnCall;
  final Function? optionsBtnCall;

  /// Colour of the close-button.
  /// An optional parameter and default value is [UIConstants.accentColor].
  final bool? hasCloseBtn;
  final Color btnCloseColor;
  final Function? closeBtnCall;

  /// Bottom-up sheet is scrollable or not.
  /// A optional parameter.
  final bool isScrollable;

  /// Contents of the bottom-up sheet.
  /// A compulsory widget.
  final Widget page;

  const BottomUpSheetTemplate({
    this.heightRatio = 0.0,
    this.heightAddition = 0.0,
    this.color = UIConstants.primaryColor,
    this.shadowColor = UIConstants.grayShadow,
    this.isHeaderNeeded = true,
    this.hasBackBtn = false,
    this.backBtnText,
    this.backBtnCall,
    this.hasLogo = false,
    this.isLogoPink = true,
    this.miniTitle,
    this.title,
    this.addBtnCall,
    this.optionsBtnCall,
    this.hasCloseBtn = true,
    this.btnCloseColor = UIConstants.accentColor,
    this.closeBtnCall,
    this.isScrollable = false,
    required this.page,
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    double width = MediaQuery.of(context).size.width;
    double height = MediaQuery.of(context).size.height;

    return GestureDetector(
      // Closes when swipe down.
      onVerticalDragUpdate: (details) {
        int sensitivity = 8;
        if (details.delta.dy > sensitivity) Navigator.pop(context);
      },
      child: SafeArea(
        child: Container(
          color: UIConstants.transparentDark, // Background color.
          child: ClipRect(
            child: BackdropFilter(
              filter: ImageFilter.blur(sigmaX: 4, sigmaY: 4), // Blur the background.
              child: Container(
                margin: EdgeInsets.only(top: height * heightRatio),
                decoration: BoxDecoration(
                  color: color,
                  border: Border.all(color: color, width: 0),
                  borderRadius: mainBorderRadius,
                ),
                padding: EdgeInsets.only(
                  left: width * 0.02,
                  top: height * 0.01,
                  right: width * 0.02,
                ),
                child: Stack(
                  children: [
                    /// Header.
                    ///
                    /// This top section is static and not scrollable.
                    /// Each widget of this is optional.
                    if (isHeaderNeeded == true)
                      Stack(
                        children: [
                          /// Back button.
                          hasBackBtn! || backBtnText != null
                              ? BackwardButton(
                                  text: backBtnText ?? '',
                                  call: backBtnCall,
                                )
                              : Container(),

                          /// Logo or mini title / title.
                          hasLogo == true
                              ? TopLogo(
                                  imgPath: 'assets/images/logo.svg',
                                  imgWidth: width / 7,
                                  isLogoPink: isLogoPink,
                                  topPadding: height * 0.05,
                                )
                              : miniTitle != null || title != null
                                  ? Align(
                                      alignment: Alignment.topCenter,
                                      child: Container(
                                        margin: EdgeInsets.only(top: height * 0.03),
                                        color: UIConstants.transparent,
                                        child: ConstrainedBox(
                                          constraints: BoxConstraints(maxWidth: width * 0.55),
                                          child: Row(
                                            mainAxisAlignment: MainAxisAlignment.center,
                                            crossAxisAlignment: CrossAxisAlignment.center,
                                            children: <Widget>[
                                              Flexible(
                                                child: Text(
                                                  miniTitle != null ? miniTitle! : title!,
                                                  maxLines: 1,
                                                  overflow: TextOverflow.ellipsis,
                                                  textAlign: TextAlign.center,
                                                  style: TextStyle(
                                                    color: UIConstants.grayDark,
                                                    fontSize: miniTitle != null ? width * 0.04 : width * 0.06,
                                                    fontWeight: FontWeight.w700,
                                                    fontStyle: FontStyle.normal,
                                                  ),
                                                ),
                                              )
                                            ],
                                          ),
                                        ),
                                      ),
                                    )
                                  : Container(),

                          /// Close button.
                          hasCloseBtn! || closeBtnCall != null
                              ? Padding(
                                  padding: EdgeInsets.only(right: height * 0.02),
                                  child: ClosingButton(
                                    backgroundColor: color,
                                    iconColor: btnCloseColor,
                                    call: closeBtnCall ?? () => Navigator.pop(context),
                                  ),
                                )
                              : Padding(
                                  padding: EdgeInsets.only(
                                    top: height * 0.01,
                                    right: width * 0.005,
                                  ),
                                  child: Stack(
                                    children: [
                                      /// Add button.
                                      addBtnCall != null
                                          ? Align(
                                              alignment: Alignment.topRight,
                                              child: Padding(
                                                padding: EdgeInsets.only(
                                                  right: optionsBtnCall != null ? width * 0.16 : width * 0.01,
                                                ),
                                                child: AddButton(
                                                  call: addBtnCall!,
                                                ),
                                              ),
                                            )
                                          : Container(),

                                      /// Options button.
                                      optionsBtnCall != null
                                          ? Align(
                                              alignment: Alignment.topRight,
                                              child: Padding(
                                                padding: EdgeInsets.only(right: width * 0.01),
                                                child: OptionsButton(
                                                  call: optionsBtnCall,
                                                ),
                                              ),
                                            )
                                          : Container(),
                                    ],
                                  ),
                                ),
                        ],
                      ),

                    /// Page content.
                    Padding(
                      padding: EdgeInsets.only(
                        top: isHeaderNeeded == true ? height * 0.08 : height * 0.0,
                        left: width * 0.04,
                        right: width * 0.04,
                        bottom: height * 0.02,
                      ),
                      child: LayoutBuilder(
                        builder: (
                          BuildContext context,
                          BoxConstraints constraints,
                        ) =>
                            SingleChildScrollView(
                          physics: !isScrollable ? const NeverScrollableScrollPhysics() : null,
                          child: ConstrainedBox(
                            constraints: BoxConstraints(minHeight: constraints.maxHeight),
                            child: IntrinsicHeight(
                              child: Column(
                                mainAxisAlignment: MainAxisAlignment.start,
                                children: [Expanded(child: page)],
                              ),
                            ),
                          ),
                        ),
                      ),
                    ),
                  ],
                ),
              ),
            ),
          ),
        ),
      ),
    );
  }
}
