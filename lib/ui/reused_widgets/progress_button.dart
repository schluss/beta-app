import 'package:flutter/material.dart';
import 'package:schluss_beta_app/constants/ui_constants.dart';

class ProgressButton extends StatelessWidget {
  final String text;
  final double value;

  const ProgressButton({
    required this.text,
    required this.value,
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    double height = MediaQuery.of(context).size.width * 0.15;

    return SizedBox(
      height: height,
      child: ClipRRect(
        borderRadius: BorderRadius.circular(8),
        child: Stack(
          children: <Widget>[
            SizedBox.expand(
              child: LinearProgressIndicator(
                value: value,
                backgroundColor: UIConstants.grayLightest,
                valueColor: const AlwaysStoppedAnimation<Color>(
                  UIConstants.paleLilac,
                ),
              ),
            ),
            Center(
              child: Text(
                text,
                textAlign: TextAlign.center,
                style: TextStyle(
                  color: UIConstants.slateGray100,
                  fontSize: MediaQuery.of(context).size.width * 0.045,
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
