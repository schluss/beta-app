import 'package:flutter/material.dart';
import 'package:schluss_beta_app/constants/ui_constants.dart';
import 'package:schluss_beta_app/translations/i18n.dart';
import 'package:schluss_beta_app/ui/reused_widgets/provider_logo.dart';
import 'package:schluss_beta_app/util/color_picker_util.dart';
import 'package:schluss_beta_app/util/initials_picker_util.dart';

class DataAttributeInfoFullCard extends StatelessWidget {
  const DataAttributeInfoFullCard({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    double width = MediaQuery.of(context).size.width;
    double height = MediaQuery.of(context).size.height;

    final ColorPickerUtil colorPickerUtil = ColorPickerUtil();
    final InitialsPickerUtil initialsPickerUtil = InitialsPickerUtil();

    return Card(
      elevation: 1,
      margin: EdgeInsets.only(
        right: 0,
        bottom: height * 0.02,
        left: 0,
      ),
      color: UIConstants.primaryColor,
      child: Container(
        margin: const EdgeInsets.all(0.0),
        padding: EdgeInsets.symmetric(vertical: height * 0.015),
        width: width,
        child: Column(
          children: [
            ListView(
              physics: const NeverScrollableScrollPhysics(),
              shrinkWrap: true,
              padding: EdgeInsets.symmetric(horizontal: width * 0.05),
              children: <Widget>[
                Padding(
                  padding: EdgeInsets.only(bottom: height * 0.01),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.start,
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: [
                      Text(
                        I18n.of(context).dataSharePartiesTxt,
                        textAlign: TextAlign.end,
                        style: TextStyle(
                          color: UIConstants.gray100,
                          fontWeight: FontWeight.w400,
                          fontSize: width * 0.044,
                        ),
                      ),
                    ],
                  ),
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: [
                    Expanded(
                      flex: 1,
                      child: ProviderLogo(
                        isOrg: false,
                        logoColor: colorPickerUtil.pickColor(),
                        logoLetters: initialsPickerUtil.getInitials('NS').toString(),
                        logoFontSizeFactor: 0.055,
                      ),
                    ),
                    Expanded(
                      flex: 6,
                      child: Padding(
                        padding: EdgeInsets.only(left: width * 0.02),
                        child: Text(
                          'NS',
                          textAlign: TextAlign.start,
                          style: TextStyle(
                            color: UIConstants.grayDarkest100,
                            fontWeight: FontWeight.w600,
                            fontSize: width * 0.045,
                          ),
                        ),
                      ),
                    ),
                    Expanded(
                      flex: 3,
                      child: Text(
                        I18n.of(context).allAttributesTxt,
                        textAlign: TextAlign.end,
                        style: TextStyle(
                          color: UIConstants.gray100,
                          fontWeight: FontWeight.w400,
                          fontSize: width * 0.044,
                        ),
                      ),
                    ),
                  ],
                ),
                const Divider(height: 0.01),
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: [
                    Expanded(
                      flex: 1,
                      child: ProviderLogo(
                        isOrg: false,
                        logoColor: colorPickerUtil.pickColor(),
                        logoLetters: initialsPickerUtil.getInitials('Volksbank').toString(),
                        logoFontSizeFactor: 0.055,
                      ),
                    ),
                    Expanded(
                      flex: 6,
                      child: Padding(
                        padding: EdgeInsets.only(left: width * 0.02),
                        child: Text(
                          'Volksbank',
                          textAlign: TextAlign.start,
                          style: TextStyle(
                            color: UIConstants.grayDarkest100,
                            fontWeight: FontWeight.w600,
                            fontSize: width * 0.045,
                          ),
                        ),
                      ),
                    ),
                    Expanded(
                      flex: 3,
                      child: Text(
                        I18n.of(context).allAttributesTxt,
                        textAlign: TextAlign.end,
                        style: TextStyle(
                          color: UIConstants.gray100,
                          fontWeight: FontWeight.w400,
                          fontSize: width * 0.044,
                        ),
                      ),
                    ),
                  ],
                ),
                const Divider(height: 0.01),
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: [
                    Expanded(
                      flex: 1,
                      child: ProviderLogo(
                        isOrg: false,
                        logoColor: colorPickerUtil.pickColor(),
                        logoLetters: initialsPickerUtil.getInitials('Anna Peters').toString(),
                        logoFontSizeFactor: 0.055,
                      ),
                    ),
                    Expanded(
                      flex: 6,
                      child: Padding(
                        padding: EdgeInsets.only(left: width * 0.02),
                        child: Text(
                          'Anna Peters',
                          textAlign: TextAlign.start,
                          style: TextStyle(
                            color: UIConstants.grayDarkest100,
                            fontWeight: FontWeight.w600,
                            fontSize: width * 0.045,
                          ),
                        ),
                      ),
                    ),
                    Expanded(
                      flex: 3,
                      child: Text(
                        '1 ${I18n.of(context).attributeTxt}',
                        textAlign: TextAlign.end,
                        style: TextStyle(
                          color: UIConstants.gray100,
                          fontWeight: FontWeight.w400,
                          fontSize: width * 0.044,
                        ),
                      ),
                    ),
                  ],
                ),
              ],
            ),
          ],
        ),
      ),
    );
  }
}
