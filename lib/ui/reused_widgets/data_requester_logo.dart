import 'package:flutter/cupertino.dart';
import 'package:schluss_beta_app/constants/ui_constants.dart';
import 'package:schluss_beta_app/ui/reused_widgets/net_image.dart';

class DataRequesterLogo extends StatelessWidget {
  final bool isAssetImg;
  final String img;
  final bool hasMargin;
  final bool hasBorder;

  const DataRequesterLogo({
    required this.img,
    this.isAssetImg = true,
    this.hasMargin = true,
    this.hasBorder = true,
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    double width = MediaQuery.of(context).size.width;

    return Container(
      margin: (hasMargin) ? EdgeInsets.only(right: width * 0.03) : null,
      width: width * 0.12,
      height: width * 0.12,
      decoration: BoxDecoration(
        shape: BoxShape.circle,
        border: (hasBorder)
            ? Border.all(
                color: UIConstants.grayLight,
                width: 1,
              )
            : null,
        color: UIConstants.primaryColor,
      ),
      alignment: Alignment.center,
      child: isAssetImg
          ? Image(
              image: AssetImage(img),
              width: 25,
              height: 25,
            )
          : NetImage(
              path: img,
              width: 0.075,
            ),
    );
  }
}
