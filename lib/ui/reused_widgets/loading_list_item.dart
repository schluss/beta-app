import 'package:flutter/material.dart';
import 'package:schluss_beta_app/constants/ui_constants.dart';
import 'package:schluss_beta_app/ui/reused_widgets/loading_animation.dart';

class LoadingListItem extends StatefulWidget {
  final bool isIconVisible;
  final double topItemWidthFactor;
  final double topItemHeightFactor;
  final double bottomItemWidthFactor;
  final double bottomItemHeightFactor;

  const LoadingListItem({
    this.isIconVisible = true,
    this.topItemWidthFactor = 0.5,
    this.topItemHeightFactor = 0.0525,
    this.bottomItemWidthFactor = 0.3,
    this.bottomItemHeightFactor = 0.035,
    Key? key,
  }) : super(key: key);

  @override
  LoadingLitItemState createState() => LoadingLitItemState();
}

class LoadingLitItemState extends State<LoadingListItem> with SingleTickerProviderStateMixin {
  late AnimationController _controller;
  late Animation _animation;

  @override
  void initState() {
    super.initState();

    _controller = AnimationController(
      vsync: this,
      duration: const Duration(
        milliseconds: 1000,
      ),
    )..repeat(
        reverse: true,
      );

    _animation = Tween(
      begin: 0.0,
      end: 1.0,
    ).animate(_controller);
  }

  @override
  void dispose() {
    _controller.dispose();

    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    double width = MediaQuery.of(context).size.width;

    return Row(
      children: <Widget>[
        if (widget.isIconVisible)
          FadeTransition(
            opacity: _animation as Animation<double>,
            child: Container(
              margin: EdgeInsets.only(right: width * 0.03),
              width: width * 0.12,
              height: width * 0.12,
              decoration: const BoxDecoration(
                shape: BoxShape.circle,
                gradient: LinearGradient(
                  colors: [
                    UIConstants.paleGray,
                    UIConstants.grayLighter,
                    UIConstants.paleGray,
                  ],
                  stops: [0, 0.4903333333333335, 1],
                  begin: Alignment(-1.00, 0.00),
                  end: Alignment(1.00, -0.00),
                ),
              ),
              alignment: Alignment.center,
            ),
          ),
        Column(
          mainAxisSize: MainAxisSize.min,
          crossAxisAlignment: CrossAxisAlignment.start,
          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
          children: <Widget>[
            LoadingAnimation(
              width: width * widget.topItemWidthFactor,
              height: width * widget.topItemHeightFactor,
            ),
            Container(height: width * 0.02),
            LoadingAnimation(
              width: width * widget.bottomItemWidthFactor,
              height: width * widget.bottomItemHeightFactor,
            ),
          ],
        )
      ],
    );
  }
}
