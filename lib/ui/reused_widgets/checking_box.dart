import 'package:flutter/material.dart';
import 'package:schluss_beta_app/constants/ui_constants.dart';

class CheckingBox extends StatelessWidget {
  final bool noneSelected;
  final bool allSelected;
  final bool someSelected;
  final Function? callback;

  const CheckingBox({
    this.noneSelected = false,
    this.allSelected = true,
    this.someSelected = false,
    this.callback,
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    double width = MediaQuery.of(context).size.width;

    return InkWell(
      child: Align(
        alignment: Alignment.center,
        child: Container(
          width: width * 0.05,
          height: width * 0.05,
          alignment: Alignment.center,
          decoration: BoxDecoration(
            shape: BoxShape.rectangle,
            border: Border.all(
              color: noneSelected ? UIConstants.grayDarkest40 : UIConstants.accentColor,
              width: 1,
            ),
            borderRadius: BorderRadius.circular(4.0),
            color: noneSelected ? UIConstants.primaryColor : UIConstants.accentColor,
          ),
          child: !noneSelected || allSelected || someSelected
              ? Image.asset(
                  allSelected ? 'assets/images/green_check_icon.png' : 'assets/images/dash_check_icon.png',
                  color: UIConstants.primaryColor,
                  width: width * 0.05,
                )
              : Container(),
        ),
      ),
      onTap: () {},
    );
  }
}
