/*
 * @author Asanka Anthony
 * @email aanthony@yukon.lk
 * @create date 2020-11-03 12:38:57
 * @modify date 2020-11-03 12:38:57
 * 
 * Refactoring
 *  by: Pasindu Jayanath (pasindu@yukon.lk).
 *  on: 2021-06-16.
 */

import 'package:flutter/cupertino.dart';
import 'package:schluss_beta_app/constants/ui_constants.dart';
import 'package:schluss_beta_app/ui/reused_widgets/main_button.dart';

class EmptyStateContentTemplate extends StatelessWidget {
  final AssetImage img;
  final String text;
  final MainButton? mainButton;

  const EmptyStateContentTemplate({
    required this.img,
    required this.text,
    this.mainButton,
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    double width = MediaQuery.of(context).size.width;
    double height = MediaQuery.of(context).size.height;

    return SingleChildScrollView(
      physics: const NeverScrollableScrollPhysics(),
      child: Container(
        color: UIConstants.paleGray,
        height: height,
        child: Align(
          alignment: Alignment.topCenter,
          child: SizedBox(
            height: height * 0.69,
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                /// Image.
                Image(
                  image: img,
                  width: width * 0.42,
                ),
                Padding(
                  padding: EdgeInsets.only(
                    top: width * 0.08,
                    left: width * 0.18,
                    right: width * 0.18,
                  ),

                  /// Description text.
                  child: Text(
                    text,
                    textAlign: TextAlign.center,
                    style: TextStyle(
                      color: UIConstants.slateGray100,
                      fontSize: width * 0.046,
                    ),
                  ),
                ),

                /// Main button.
                mainButton != null
                    ? Padding(
                        padding: EdgeInsets.only(
                          top: width * 0.25,
                          left: width * 0.18,
                          right: width * 0.18,
                        ),
                        child: mainButton,
                      )
                    : Container(),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
