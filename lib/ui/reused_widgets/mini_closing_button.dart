import 'package:flutter/material.dart';
import 'package:schluss_beta_app/constants/ui_constants.dart';

class MiniClosingButton extends StatelessWidget {
  final Color? color;
  final Function call;

  const MiniClosingButton({
    this.color = UIConstants.paleGray,
    required this.call,
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: call as void Function()?,
      child: Align(
        alignment: Alignment.topRight,
        child: Container(
          decoration: BoxDecoration(
            shape: BoxShape.circle,
            color: color,
          ),
          padding: const EdgeInsets.all(6),
          child: Icon(
            Icons.close,
            color: UIConstants.gray100,
            size: MediaQuery.of(context).size.width * 0.04,
          ),
        ),
      ),
    );
  }
}
