import 'dart:ui';

import 'package:flutter/cupertino.dart';
import 'package:schluss_beta_app/constants/ui_constants.dart';
import 'package:schluss_beta_app/translations/i18n.dart';

class BottomUpActionSheetLoader {
  /// Loads CupertinoModalPopup which hosts a CupertinoActionSheet.
  static void loadActionSheet(
    BuildContext context, {
    required String title,
    required dynamic buttonList,
    String btnCancelText = '',
  }) {
    showCupertinoModalPopup<void>(
      context: context,
      barrierColor: UIConstants.transparentDark,
      barrierDismissible: true,
      filter: ImageFilter.blur(sigmaX: 6.0, sigmaY: 6.0),
      builder: (BuildContext context) => BottomUpActionSheetTemplate(
        title: title,
        btnList: buttonList,
        btnCancelText: btnCancelText,
      ),
    );
  }
}

class ActionSheetButton {
  String text;
  dynamic call;

  ActionSheetButton({
    required this.text,
    required this.call,
  });
}

class BottomUpActionSheetTemplate extends StatefulWidget {
  final String title;
  final dynamic btnList;
  final String btnCancelText;

  const BottomUpActionSheetTemplate({
    required this.title,
    required this.btnList,
    this.btnCancelText = '',
    Key? key,
  }) : super(key: key);

  @override
  State<BottomUpActionSheetTemplate> createState() => _BottomUpActionSheetTemplateState();
}

class _BottomUpActionSheetTemplateState extends State<BottomUpActionSheetTemplate> {
  List<ActionSheetButton> actionButton = [];

  @override
  void initState() {
    actionButton = widget.btnList;

    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    double width = MediaQuery.of(context).size.width;

    return WillPopScope(
      onWillPop: () async {
        Navigator.pop(context);
        return true;
      },
      child: CupertinoActionSheet(
        title: Text(
          widget.title,
          textAlign: TextAlign.center,
          style: TextStyle(
            fontSize: width * 0.05,
            fontWeight: FontWeight.w400,
            fontStyle: FontStyle.normal,
            fontFamily: UIConstants.subFontFamily,
            color: UIConstants.gray100,
          ),
        ),
        actions: List<Widget>.generate(
          actionButton.length,
          (int index) => CupertinoActionSheetAction(
            isDestructiveAction: false,
            isDefaultAction: false,
            onPressed: actionButton[index].call,
            child: Text(
              actionButton[index].text,
              textAlign: TextAlign.center,
              style: TextStyle(
                fontSize: width * 0.05,
                fontWeight: FontWeight.w400,
                fontStyle: FontStyle.normal,
                fontFamily: UIConstants.btnFontFamily,
                color: UIConstants.grayShadowDark,
              ),
            ),
          ),
        ),
        cancelButton: CupertinoActionSheetAction(
          isDestructiveAction: false,
          child: Text(
            I18n.of(context).btnCancelTxt,
            textAlign: TextAlign.center,
            style: TextStyle(
              fontSize: width * 0.05,
              fontWeight: FontWeight.w400,
              fontStyle: FontStyle.normal,
              fontFamily: UIConstants.btnFontFamily,
              color: UIConstants.slateGray100,
            ),
          ),
          onPressed: () {
            Navigator.pop(context);
          },
        ),
      ),
    );
  }
}
