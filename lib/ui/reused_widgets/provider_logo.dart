import 'package:flutter/material.dart';
import 'package:schluss_beta_app/constants/ui_constants.dart';
import 'package:schluss_beta_app/ui/reused_widgets/net_image.dart';

class ProviderLogo extends StatelessWidget {
  final bool isOrg;
  final String logoImg;
  final Color? logoColor;
  final String logoLetters;
  final double logoFontSizeFactor;
  final bool isChecked;
  final bool isCheckedEnabled;

  const ProviderLogo({
    this.isOrg = true,
    this.logoImg = '',
    this.logoColor,
    this.logoLetters = '',
    this.logoFontSizeFactor = 0.06,
    this.isChecked = false,
    this.isCheckedEnabled = true,
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    double width = MediaQuery.of(context).size.width;

    return SizedBox(
      width: width * 0.17,
      height: width * 0.14,
      child: Stack(
        children: <Widget>[
          Align(
            alignment: Alignment.centerLeft,
            child: Container(
              width: width * 0.13,
              height: width * 0.13,
              alignment: Alignment.center,
              decoration: BoxDecoration(
                shape: BoxShape.circle,
                border: Border.all(
                  color: isOrg == true ? UIConstants.grayLight : logoColor ?? UIConstants.grayLight,
                  width: 1,
                ),
                color: isOrg == true ? UIConstants.paleGray : logoColor,
              ),
              child: isOrg == true
                  ? NetImage(
                      path: logoImg,
                      width: 0.074,
                    )
                  : Text(
                      logoLetters,
                      textAlign: TextAlign.center,
                      style: TextStyle(
                        color: UIConstants.primaryColor,
                        backgroundColor: UIConstants.transparent,
                        fontSize: width * logoFontSizeFactor,
                        fontWeight: FontWeight.w600,
                        fontStyle: FontStyle.normal,
                        fontFamily: UIConstants.defaultFontFamily,
                      ),
                    ),
            ),
          ),

          /// Check mark.
          if (isChecked && isCheckedEnabled)
            Padding(
              padding: EdgeInsets.only(right: width * 0.035),
              child: Align(
                alignment: Alignment.topRight,
                child: SizedBox(
                  width: width * 0.055,
                  height: width * 0.055,
                  child: const Image(
                    image: AssetImage(
                      'assets/images/white_check_in_green_icon.png',
                    ),
                  ),
                ),
              ),
            )
        ],
      ),
    );
  }
}
