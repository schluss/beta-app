import 'package:flutter/material.dart';
import 'package:logger/logger.dart';
import 'package:schluss_beta_app/constants/ui_constants.dart';
import 'package:schluss_beta_app/controller/logger_controller.dart';
import 'package:schluss_beta_app/singleton_injector.dart';
import 'package:schluss_beta_app/translations/i18n.dart';
import 'package:schluss_beta_app/ui/reused_widgets/main_button.dart';
import 'package:schluss_beta_app/ui/reused_widgets/plain_info_screen_template.dart';
import 'package:schluss_beta_app/util/error_handler_util.dart';
import 'package:schluss_beta_app/util/logger_util.dart';

class ErrorPage extends StatefulWidget {
  final String errorText;
  final String description;

  const ErrorPage({
    required this.errorText,
    required this.description,
    Key? key,
  }) : super(key: key);

  @override
  ErrorPageState createState() => ErrorPageState();
}

class ErrorPageState extends State<ErrorPage> {
  final LoggerController _loggerController = LoggerController();
  final ErrorHandlerUtil _errorHandler = ErrorHandlerUtil();
  final LoggerUtil _loggerUtil = LoggerUtil();
  final Logger _logger = singletonInjector<Logger>();

  bool isSendingLogInitiated = false;
  bool isLogSent = false;

  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    double width = MediaQuery.of(context).size.width;
    double height = MediaQuery.of(context).size.height;

    return PlainInfoScreenTemplate(
      img: const AssetImage('assets/images/error_icon.png'),
      title: widget.errorText,
      mainText: widget.description,
      dynamicContent: Column(
        mainAxisAlignment: MainAxisAlignment.end,
        children: <Widget>[
          if (!isSendingLogInitiated && !isLogSent)
            Padding(
              padding: EdgeInsets.only(bottom: height * 0.03),
              child: MainButton(
                  type: BtnType.outline,
                  text: I18n.of(context).errHandlerIgnoreBtnTxt,
                  call: () {
                    if (Navigator.canPop(context)) {
                      Navigator.pop(context);
                    } else {
                      Navigator.pushNamed(
                        context,
                        '/dashboard',
                        arguments: {'activeTab': 0, 'isActualVaultOwner': true},
                      );
                    }
                  }),
            ),
          if (isLogSent)
            Padding(
              padding: EdgeInsets.only(bottom: height * 0.05),
              child: buildLogSentMessage(width),
            ),
          _buildMainButton()!
        ],
      ),
    );
  }

  Row buildLogSentMessage(double width) {
    return Row(
      crossAxisAlignment: CrossAxisAlignment.center,
      mainAxisAlignment: MainAxisAlignment.center,
      children: <Widget>[
        Padding(
          padding: const EdgeInsets.only(right: UIConstants.minPadding),
          child: Icon(
            Icons.check,
            color: UIConstants.greenBlue,
            size: width * 0.0525,
          ),
        ),
        Text(
          I18n.of(context).errHandlerSentTxt,
          textAlign: TextAlign.center,
          style: TextStyle(
            fontWeight: FontWeight.w600,
            color: UIConstants.gray100,
            fontSize: width * 0.045,
          ),
        ),
      ],
    );
  }

  MainButton? _buildMainButton() {
    MainButton? button;

    if (!isSendingLogInitiated && !isLogSent) {
      button = MainButton(
        type: BtnType.active,
        text: I18n.of(context).errHandlerBtnTxt,
        call: () {
          setState(() => isSendingLogInitiated = true);
          _sendLog();
        },
      );
    } else if (isSendingLogInitiated && !isLogSent) {
      button = MainButton(
        type: BtnType.awaited,
        text: I18n.of(context).btnSendTxt,
        call: () {},
      );
    } else if (isLogSent) {
      button = MainButton(
        type: BtnType.active,
        text: I18n.of(context).btnGoBackTxt,
        call: () {
          // TODO: screen remove validation not implemented in the app. May need
          //  to implement generic pop function with validation.
          if (Navigator.canPop(context)) {
            Navigator.pop(context);
          } else {
            Navigator.pushNamed(
              context,
              '/dashboard',
              arguments: {'activeTab': 0, 'isActualVaultOwner': true},
            );
          }
        },
      );
    }
    return button;
  }

  Future _sendLog() async {
    bool? response;

    try {
      response = await _loggerController.sendErrorLogFile();
    } catch (e) {
      _errorHandler.handleError(e.toString(), true, context, 'logger');
    }

    if (response == true) {
      try {
        // Delete log file, if log is sent to the server.
        await _loggerUtil.deleteLogFile();
      } catch (e) {
        _logger.e(e);
        if (!mounted) return;
        _errorHandler.handleError(e.toString(), true, context);
      }

      return setState(() => isLogSent = true);
    }
  }
}
