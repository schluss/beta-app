import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';

class NetworkImg extends StatefulWidget {
  final String? imgPath;
  final double imgWidth;

  const NetworkImg(
    this.imgPath,
    this.imgWidth, {
    Key? key,
  }) : super(key: key);

  @override
  NetworkImgState createState() => NetworkImgState();
}

class NetworkImgState extends State<NetworkImg> {
  @override
  Widget build(BuildContext context) {
    double width = MediaQuery.of(context).size.width;

    return Align(
      alignment: Alignment.center,
      child: widget.imgPath != null && widget.imgPath!.isNotEmpty
          ? widget.imgPath!.endsWith('.svg')
              ? SizedBox(
                  width: width * widget.imgWidth,
                  height: width * widget.imgWidth,
                  child: SvgPicture.network(widget.imgPath!),
                )
              : Image(
                  image: CachedNetworkImageProvider(
                    widget.imgPath!,
                  ),
                  width: width * widget.imgWidth,
                  height: width * widget.imgWidth,
                )
          : Image(
              image: const AssetImage(
                'assets/images/media_placeholder_icon.png',
              ),
              width: width * widget.imgWidth,
            ),
    );
  }
}
