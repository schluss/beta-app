import 'package:flutter/material.dart';
import 'package:schluss_beta_app/constants/ui_constants.dart';

class PinEye extends StatelessWidget {
  final bool isVisible;

  const PinEye({
    required this.isVisible,
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    double width = MediaQuery.of(context).size.width;

    return Container(
      alignment: Alignment.centerRight,
      width: width * 0.8,
      height: width * 0.125,
      color: UIConstants.transparent,
      child: Padding(
        padding: isVisible
            ? EdgeInsets.all(
                width * 0.03,
              )
            : EdgeInsets.all(
                width * 0.033,
              ),
        child: Image.asset(
          isVisible ? 'assets/images/eye_cross_icon.png' : 'assets/images/eye_open_icon.png',
          color: UIConstants.gray100,
          width: isVisible ? width * 0.062 : width * 0.055,
        ),
      ),
    );
  }
}
