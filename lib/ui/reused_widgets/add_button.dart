import 'package:flutter/material.dart';
import 'package:schluss_beta_app/constants/ui_constants.dart';

class AddButton extends StatelessWidget {
  final Function call;

  const AddButton({
    required this.call,
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.only(top: 12, right: 12),
      child: Container(
        decoration: const BoxDecoration(
          color: UIConstants.transparent,
          borderRadius: BorderRadius.all(
            Radius.circular(8),
          ),
        ),
        child: IconButton(
          iconSize: 32,
          icon: const Icon(
            Icons.add,
            color: UIConstants.accentColor,
          ),
          onPressed: call as void Function()?,
        ),
      ),
    );
  }
}
