import 'package:flutter/material.dart';
import 'package:schluss_beta_app/constants/ui_constants.dart';

/// Builds notification message on pin code input screens.
class MatchingValidationNotifier extends StatelessWidget {
  final String text;
  final bool isValid;
  final Color validTextColor;
  final Color invalidTextColor;
  final Color validColor;
  final Color invalidColor;

  const MatchingValidationNotifier({
    required this.text,
    required this.isValid,
    this.validTextColor = UIConstants.greenDark,
    this.invalidTextColor = UIConstants.red,
    this.validColor = UIConstants.greenLightest,
    this.invalidColor = UIConstants.red,
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    double width = MediaQuery.of(context).size.width;
    Container container;

    if (text == '') {
      container = Container();
    } else {
      container = Container(
        width: width * 0.77,
        padding: EdgeInsets.symmetric(
          horizontal: width * 0.04,
          vertical: width * 0.023,
        ),
        alignment: Alignment.centerLeft,
        decoration: BoxDecoration(
          color: isValid
              ? validColor
              : invalidColor == UIConstants.red
                  ? invalidColor.withOpacity(0.1)
                  : invalidColor,
          borderRadius: const BorderRadius.all(Radius.circular(8)),
        ),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: <Widget>[
            FittedBox(
              child: ConstrainedBox(
                constraints: BoxConstraints(maxWidth: width * 0.60),
                child: Text(
                  text,
                  maxLines: 2,
                  overflow: TextOverflow.ellipsis,
                  style: TextStyle(
                    fontFamily: UIConstants.subFontFamily,
                    color: isValid ? validTextColor : invalidTextColor,
                    fontWeight: FontWeight.w600,
                    fontSize: width * 0.038,
                  ),
                ),
              ),
            ),
            Padding(
              padding: const EdgeInsets.only(left: 6),
              child: Image.asset(
                isValid ? 'assets/images/smiley_face_icon.png' : 'assets/images/sad_face_icon.png',
                width: width * 0.05,
              ),
            ),
          ],
        ),
      );
    }
    return container;
  }
}
