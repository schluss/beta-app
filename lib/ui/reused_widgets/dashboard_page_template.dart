import 'package:flutter/material.dart';
import 'package:schluss_beta_app/constants/ui_constants.dart';
import 'package:schluss_beta_app/ui/reused_widgets/header_template.dart';

class DashboardPageTemplate extends StatelessWidget {
  final String text;
  final Function? addBtnCall;
  final Function? backBtnCall;
  final PreferredSize? appBar;
  final double topPadding;
  final Widget appBody;

  const DashboardPageTemplate({
    required this.text,
    this.addBtnCall,
    this.backBtnCall,
    this.appBar,
    this.topPadding = 0.0,
    required this.appBody,
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: appBar,
      resizeToAvoidBottomInset: false,
      body: GestureDetector(
        onTap: () {
          FocusScope.of(context).requestFocus(FocusNode());
        },
        child: Container(
          color: UIConstants.primaryColor,
          child: Padding(
            padding: EdgeInsets.only(top: topPadding),
            child: NestedScrollView(
              headerSliverBuilder: (
                BuildContext context,
                bool innerBoxIsScrolled,
              ) {
                return [
                  HeaderTemplate(
                    text: text,
                    addBtnCall: addBtnCall,
                    backBtnCall: backBtnCall,
                  ),
                ];
              },
              body: appBody,
            ),
          ),
        ),
      ),
    );
  }
}
