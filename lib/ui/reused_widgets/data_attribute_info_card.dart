import 'package:flutter/material.dart';
import 'package:schluss_beta_app/constants/ui_constants.dart';
import 'package:schluss_beta_app/translations/i18n.dart';
import 'package:schluss_beta_app/ui/reused_widgets/provider_logo.dart';
import 'package:schluss_beta_app/util/color_picker_util.dart';
import 'package:schluss_beta_app/util/initials_picker_util.dart';

class DataAttributeInfoCard extends StatelessWidget {
  final bool isDefault;
  final bool isShown;

  const DataAttributeInfoCard({
    this.isDefault = false,
    this.isShown = true,
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    double width = MediaQuery.of(context).size.width;
    double height = MediaQuery.of(context).size.height;

    final ColorPickerUtil colorPickerUtil = ColorPickerUtil();
    final InitialsPickerUtil initialsPickerUtil = InitialsPickerUtil();

    return Container(
      padding: EdgeInsets.symmetric(horizontal: width * 0.06),
      child: Card(
        elevation: 1,
        margin: EdgeInsets.only(
          right: 0,
          bottom: height * 0.02,
          left: 0,
        ),
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(8.0),
        ),
        color: UIConstants.primaryColor,
        child: Container(
          // color: UIConstants.gray100,
          margin: const EdgeInsets.all(0.0),
          padding: EdgeInsets.symmetric(vertical: height * 0.015),
          width: width,
          child: Column(
            children: [
              ListView(
                physics: const NeverScrollableScrollPhysics(),
                shrinkWrap: true,
                padding: const EdgeInsets.all(0.0),
                children: <Widget>[
                  Padding(
                    padding: EdgeInsets.only(
                      right: width * 0.05,
                      bottom: width * 0.01,
                      left: width * 0.05,
                    ),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: [
                        Expanded(
                          flex: 2,
                          child: ProviderLogo(
                            isOrg: false,
                            logoColor: colorPickerUtil.pickColor(),
                            logoLetters: initialsPickerUtil.getInitials('Belastingdienst').toString(),
                          ),
                        ),
                        Expanded(
                          flex: 4,
                          child: Column(
                            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              Padding(
                                padding: EdgeInsets.only(left: width * 0.01),
                                child: Text(
                                  'Belastingdienst',
                                  textAlign: TextAlign.start,
                                  style: TextStyle(
                                    color: UIConstants.grayDarkest100,
                                    fontWeight: FontWeight.w600,
                                    fontSize: width * 0.045,
                                  ),
                                ),
                              ),
                              Padding(
                                padding: EdgeInsets.only(left: width * 0.01),
                                child: Text(
                                  I18n.of(context).dataDefaultSrcTxt,
                                  textAlign: TextAlign.start,
                                  style: TextStyle(
                                    color: UIConstants.gray100,
                                    fontWeight: FontWeight.w400,
                                    fontSize: width * 0.043,
                                  ),
                                ),
                              ),
                            ],
                          ),
                        ),
                        Expanded(
                          flex: 4,
                          child: Text(
                            '12 februari 2021',
                            textAlign: TextAlign.end,
                            style: TextStyle(
                              color: UIConstants.gray100,
                              fontWeight: FontWeight.w400,
                              fontSize: width * 0.044,
                            ),
                          ),
                        ),
                      ],
                    ),
                  ),
                  const Divider(),
                  Padding(
                    padding: EdgeInsets.symmetric(
                      horizontal: width * 0.05,
                      vertical: height * 0.001,
                    ),
                    child: InkWell(
                      splashColor: UIConstants.blackFaded,
                      onTap: null,
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.spaceAround,
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Padding(
                            padding: EdgeInsets.only(bottom: height * 0.0025),
                            child: Text(
                              I18n.of(context).foreNameTxt,
                              textAlign: TextAlign.start,
                              style: TextStyle(
                                color: UIConstants.gray100,
                                fontWeight: FontWeight.w400,
                                fontSize: width * 0.044,
                              ),
                            ),
                          ),
                          Text(
                            'Michiel',
                            textAlign: TextAlign.start,
                            style: TextStyle(
                              color: UIConstants.grayDarkest100,
                              fontWeight: FontWeight.w600,
                              fontSize: width * 0.046,
                            ),
                          ),
                        ],
                      ),
                    ),
                  ),
                  const Divider(),
                  Padding(
                    padding: EdgeInsets.symmetric(
                      horizontal: width * 0.05,
                      vertical: height * 0.001,
                    ),
                    child: InkWell(
                      splashColor: UIConstants.blackFaded,
                      onTap: null,
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.spaceAround,
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Padding(
                            padding: EdgeInsets.only(bottom: height * 0.0025),
                            child: Text(
                              I18n.of(context).initialsTxt,
                              textAlign: TextAlign.start,
                              style: TextStyle(
                                color: UIConstants.gray100,
                                fontWeight: FontWeight.w400,
                                fontSize: width * 0.044,
                              ),
                            ),
                          ),
                          Text(
                            'M.A.P.',
                            textAlign: TextAlign.start,
                            style: TextStyle(
                              color: UIConstants.grayDarkest100,
                              fontWeight: FontWeight.w600,
                              fontSize: width * 0.046,
                            ),
                          ),
                        ],
                      ),
                    ),
                  ),
                  const Divider(),
                  Padding(
                    padding: EdgeInsets.symmetric(
                      horizontal: width * 0.05,
                      vertical: height * 0.001,
                    ),
                    child: InkWell(
                      splashColor: UIConstants.blackFaded,
                      onTap: null,
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.spaceAround,
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Padding(
                            padding: EdgeInsets.only(bottom: height * 0.0025),
                            child: Text(
                              I18n.of(context).initialsTxt,
                              textAlign: TextAlign.start,
                              style: TextStyle(
                                color: UIConstants.gray100,
                                fontWeight: FontWeight.w400,
                                fontSize: width * 0.044,
                              ),
                            ),
                          ),
                          Text(
                            'Wandelaar',
                            textAlign: TextAlign.start,
                            style: TextStyle(
                              color: UIConstants.grayDarkest100,
                              fontWeight: FontWeight.w600,
                              fontSize: width * 0.046,
                            ),
                          ),
                        ],
                      ),
                    ),
                  ),
                  isDefault && !isShown ? const Divider() : Container(),
                  isDefault && !isShown
                      ? Padding(
                          padding: EdgeInsets.symmetric(horizontal: width * 0.05),
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            crossAxisAlignment: CrossAxisAlignment.center,
                            children: [
                              Expanded(
                                flex: 9,
                                child: Column(
                                  mainAxisAlignment: MainAxisAlignment.spaceAround,
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: [
                                    Text(
                                      I18n.of(context).dataUploadBottomShtTileHeader,
                                      textAlign: TextAlign.start,
                                      style: TextStyle(
                                        color: UIConstants.gray100,
                                        fontWeight: FontWeight.w400,
                                        fontSize: width * 0.044,
                                      ),
                                    ),
                                    Padding(
                                      padding: EdgeInsets.only(
                                        top: height * 0.0025,
                                        bottom: height * 0.008,
                                      ),
                                      child: Text(
                                        'ID_Kaart_scan.pdf',
                                        textAlign: TextAlign.start,
                                        style: TextStyle(
                                          color: UIConstants.grayDarkest100,
                                          fontWeight: FontWeight.w600,
                                          fontSize: width * 0.046,
                                        ),
                                      ),
                                    ),
                                  ],
                                ),
                              ),
                              Expanded(
                                flex: 1,
                                child: Image(
                                  image: const AssetImage('assets/images/download_icon.png'),
                                  width: width * 0.1,
                                ),
                              ),
                            ],
                          ),
                        )
                      : Container(),
                  !isDefault && isShown ? const Divider() : Container(),
                  !isDefault && isShown
                      ? Padding(
                          padding: EdgeInsets.symmetric(
                            horizontal: width * 0.05,
                            vertical: height * 0.02,
                          ),
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.center,
                            crossAxisAlignment: CrossAxisAlignment.center,
                            children: [
                              Expanded(
                                flex: 10,
                                child: Text(
                                  I18n.of(context).dataSetDefaultTxt,
                                  textAlign: TextAlign.center,
                                  style: TextStyle(
                                    color: UIConstants.accentColor,
                                    fontWeight: FontWeight.w600,
                                    fontSize: width * 0.046,
                                  ),
                                ),
                              ),
                            ],
                          ),
                        )
                      : Container(),
                ],
              ),
            ],
          ),
        ),
      ),
    );
  }
}
