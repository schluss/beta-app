import 'package:flutter/material.dart';
import 'package:schluss_beta_app/constants/theme.dart';
import 'package:schluss_beta_app/constants/ui_constants.dart';
import 'package:schluss_beta_app/ui/reused_widgets/add_button.dart';
import 'package:schluss_beta_app/ui/reused_widgets/backward_button.dart';
import 'package:schluss_beta_app/ui/reused_widgets/closing_button.dart';
import 'package:schluss_beta_app/ui/reused_widgets/options_button.dart';

class IconHeaderTemplate extends StatelessWidget {
  final String logo;
  final String text;
  final bool hasBackBtn;
  final String? backBtnText;
  final Function? closeBtnCall;
  final Function? addBtnCall;
  final Function? optionsBtnCall;

  const IconHeaderTemplate({
    required this.logo,
    required this.text,
    this.hasBackBtn = false,
    this.backBtnText,
    this.closeBtnCall,
    this.addBtnCall,
    this.optionsBtnCall,
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    double width = MediaQuery.of(context).size.width;
    double height = MediaQuery.of(context).size.height;
    double topPadding = MediaQuery.of(context).padding.top;

    return Container(
      margin: EdgeInsets.only(top: topPadding),
      padding: EdgeInsets.only(bottom: height * 0.02),
      height: height * 0.12,
      decoration: const BoxDecoration(
        color: UIConstants.primaryColor,
        borderRadius: mainBorderRadius,
      ),
      alignment: Alignment.center,
      child: Stack(
        children: <Widget>[
          /// Icon.
          Align(
            alignment: Alignment.topCenter,
            child: Container(
              margin: EdgeInsets.only(top: height * 0.02, bottom: height * 0.04),
              color: UIConstants.transparent,
              child: Row(
                mainAxisAlignment: MainAxisAlignment.center,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: <Widget>[
                  Image(
                    image: AssetImage(logo),
                    width: width * 0.08,
                  ),
                ],
              ),
            ),
          ),

          /// text.
          Align(
            alignment: Alignment.bottomCenter,
            child: Container(
              margin: EdgeInsets.only(top: height * 0.02),
              color: UIConstants.transparent,
              child: ConstrainedBox(
                constraints: BoxConstraints(
                  maxWidth: width * 0.55,
                ),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: <Widget>[
                    Flexible(
                      child: Text(
                        text,
                        maxLines: 1,
                        overflow: TextOverflow.ellipsis,
                        textAlign: TextAlign.center,
                        style: TextStyle(
                          color: UIConstants.black,
                          fontSize: height * 0.025,
                          fontWeight: FontWeight.bold,
                        ),
                      ),
                    ),
                  ],
                ),
              ),
            ),
          ),

          /// Back button or close button.
          hasBackBtn
              ? BackwardButton(
                  text: backBtnText ?? '',
                )
              : ClosingButton(
                  call: closeBtnCall ?? () => Navigator.pop(context),
                ),

          /// Add button.
          addBtnCall != null
              ? Align(
                  alignment: Alignment.topRight,
                  child: Padding(
                    padding: EdgeInsets.only(
                      right: optionsBtnCall != null ? width * 0.16 : width * 0.01,
                    ),
                    child: AddButton(
                      call: addBtnCall!,
                    ),
                  ),
                )
              : Container(),

          /// Options button.
          optionsBtnCall != null
              ? Align(
                  alignment: Alignment.topRight,
                  child: Padding(
                    padding: EdgeInsets.only(right: width * 0.01),
                    child: OptionsButton(
                      call: optionsBtnCall,
                    ),
                  ),
                )
              : Container(),
        ],
      ),
    );
  }
}
