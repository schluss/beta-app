import 'package:flutter/material.dart';
import 'package:schluss_beta_app/constants/ui_constants.dart';

Widget buildPasswordText(double width, String number) => Center(
      child: FittedBox(
        child: Text(
          number,
          textAlign: TextAlign.center,
          style: TextStyle(
            color: UIConstants.accentColor,
            fontSize: width * 0.1,
          ),
        ),
      ),
    );

Container buildEmptyPlaceholder(
  double width, {
  Color color = UIConstants.paleLilac,
}) {
  return Container(
    height: width * 0.02,
    width: width * 0.02,
    decoration: BoxDecoration(
      color: color,
      shape: BoxShape.circle,
    ),
  );
}

Image buildStarImage(
  double width, {
  Color color = UIConstants.accentColor,
}) {
  return Image.asset(
    'assets/images/pink_asterisk_icon.png',
    color: color,
    width: width * 0.06,
    height: width * 0.06,
  );
}
