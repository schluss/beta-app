import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:schluss_beta_app/constants/ui_constants.dart';
import 'package:schluss_beta_app/ui/utils/clipper.dart';

class IntroImageSection extends StatelessWidget {
  final String? imagePath;
  final double? imageWidth;

  const IntroImageSection({
    this.imagePath,
    this.imageWidth,
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    double height = MediaQuery.of(context).size.height;

    return ClipPath(
      clipper: Clipper(),
      child: Container(
        color: UIConstants.paleGray,
        height: height * 0.500,
        child: Stack(
          children: <Widget>[
            SizedBox(
              height: height * 7.14,
            ),

            /// Logo.
            Padding(
              padding: EdgeInsets.only(top: height * 0.1),
              child: Align(
                alignment: Alignment.center,
                child: Image(
                  image: AssetImage(imagePath!),
                  width: imageWidth,
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
