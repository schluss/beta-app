import 'package:flutter/material.dart';
import 'package:flutter_widget_from_html_core/flutter_widget_from_html_core.dart';
import 'package:schluss_beta_app/constants/ui_constants.dart';
import 'package:schluss_beta_app/translations/i18n.dart';
import 'package:schluss_beta_app/ui/reused_widgets/top_logo.dart';

class PromiseContent extends StatelessWidget {
  const PromiseContent({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    double width = MediaQuery.of(context).size.width;
    double height = MediaQuery.of(context).size.height;

    return Column(
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      children: <Widget>[
        /// Logo.
        TopLogo(
          imgPath: 'assets/images/logo.svg',
          imgWidth: width * 0.14,
          topPadding: 0.0,
        ),

        /// Title.
        Align(
          alignment: Alignment.topCenter,
          child: Text(
            I18n.of(context).promiseTitle,
            style: TextStyle(
              color: UIConstants.grayDarkest100,
              fontSize: width * 0.06,
              fontWeight: FontWeight.bold,
            ),
          ),
        ),

        /// Card.
        SizedBox(
          height: height * 0.575,
          child: Card(
            elevation: 1,
            child: Container(
              height: height,
              width: width,
              padding: EdgeInsets.only(
                top: height * 0.04,
                bottom: height * 0.03,
                left: height * 0.03,
                right: height * 0.03,
              ),
              child: Column(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: <Widget>[
                  /// Text with bullet points.
                  buildRow(context, I18n.of(context).promiseBullet1Txt),
                  buildRow(context, I18n.of(context).promiseBullet2Txt),
                  buildRow(context, I18n.of(context).promiseBullet3Txt),
                  buildRow(context, I18n.of(context).promiseBullet4Txt),

                  /// Bottom logo list.
                  Row(
                    mainAxisSize: MainAxisSize.max,
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: <Widget>[
                      Padding(
                        padding: EdgeInsets.only(
                          right: width * 0.02,
                          top: 8,
                        ),
                        child: buildImage(
                          'assets/images/npa_logo.png',
                          width,
                        ),
                      ),
                      Padding(
                        padding: EdgeInsets.only(
                          right: width * 0.02,
                          top: 8,
                        ),
                        child: buildImage(
                          'assets/images/mdo_logo.png',
                          width,
                        ),
                      ),
                    ],
                  ),
                ],
              ),
            ),
          ),
        ),

        /// Terms & condition link.
        Align(
          alignment: Alignment.topCenter,
          child: HtmlWidget(
            I18n.of(context).promiseLinkTxt,
            textStyle: TextStyle(
              color: UIConstants.gray100,
              fontFamily: UIConstants.subFontFamily,
              fontSize: width * 0.035,
            ),
            onTapUrl: (url) {
              // Pop-up terms and conditions bottom-up sheet.
              Navigator.pushNamed(context, '/terms-and-conditions');
              return false;
            },
          ),
        ),
      ],
    );
  }

  /// Builds text with bullet points.
  Row buildRow(BuildContext context, String text) {
    double width = MediaQuery.of(context).size.width;

    return Row(
      mainAxisSize: MainAxisSize.min,
      mainAxisAlignment: MainAxisAlignment.start,
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Padding(
          padding: EdgeInsets.only(right: width * 0.03),
          child: Icon(
            Icons.check,
            size: width * 0.055,
            color: UIConstants.accentColor,
          ),
        ),
        Flexible(
          child: Text(
            text,
            softWrap: true,
            style: TextStyle(
              fontSize: width * 0.0425,
              fontWeight: FontWeight.w400,
              fontStyle: FontStyle.normal,
              letterSpacing: 0,
              height: 1.4,
              color: UIConstants.slateGray100,
            ),
          ),
        ),
      ],
    );
  }

  /// Builds image with properties.
  Image buildImage(String path, double width) {
    return Image(
      image: AssetImage(path),
      height: width * 0.18,
      width: width * 0.215,
    );
  }
}
