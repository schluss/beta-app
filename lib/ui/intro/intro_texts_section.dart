import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:schluss_beta_app/constants/ui_constants.dart';

class IntroTextsSection extends StatelessWidget {
  final String? title;
  final String? mainText;

  const IntroTextsSection({
    this.title,
    this.mainText,
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    double width = MediaQuery.of(context).size.width;
    double height = MediaQuery.of(context).size.height;

    return Container(
      color: UIConstants.primaryColor,
      height: MediaQuery.of(context).size.height / 2,
      padding: EdgeInsets.only(
        top: height * 0.0384,
        right: width * 0.075,
        left: width * 0.075,
        bottom: height * 0.055,
      ),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: <Widget>[
          /// Main text.
          Text(
            title!,
            textAlign: TextAlign.center,
            style: TextStyle(
              height: 1.2,
              color: UIConstants.grayDarkest100,
              fontSize: width * 0.07,
              fontWeight: FontWeight.bold,
            ),
          ),

          /// Description.
          Text(
            mainText!,
            textAlign: TextAlign.center,
            style: TextStyle(
              color: UIConstants.slateGray100,
              fontSize: width * 0.048,
            ),
          ),
          SizedBox(
            width: width,
            height: width * 0.25,
          ),
        ],
      ),
    );
  }
}
