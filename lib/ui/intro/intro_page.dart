import 'package:dots_indicator/dots_indicator.dart';
import 'package:flutter/material.dart';
import 'package:schluss_beta_app/constants/ui_constants.dart';
import 'package:schluss_beta_app/translations/i18n.dart';
import 'package:schluss_beta_app/ui/intro/intro_content.dart';
import 'package:schluss_beta_app/ui/reused_widgets/main_button.dart';
import 'package:schluss_beta_app/ui/reused_widgets/top_logo.dart';

/// Needs to highlight slider index.
///
/// [SpinKitThreeBounce] - uses for indicating loading status, three dots
/// are continuously indicating one after the another automatically.
/// [DotsIndicator] - uses for indicate slider position, we need to highlight
/// slider index manually. Ref: [https://schluss.atlassian.net/browse/BETA-1668].
class IntroPage extends StatefulWidget {
  const IntroPage({Key? key}) : super(key: key);

  @override
  IntroPageState createState() => IntroPageState();
}

class IntroPageState extends State<IntroPage> {
  final _pageController = PageController(initialPage: 0); // Default slide.

  /// Sets current slider as first.
  int _currentPage = 0;
  String _logoPath = 'assets/images/logo_with_schluss.svg';
  double _logoWidth = 0;

  @override
  Widget build(BuildContext context) {
    double width = MediaQuery.of(context).size.width;
    double height = MediaQuery.of(context).size.height;
    double topPadding = MediaQuery.of(context).padding.top;

    return Scaffold(
      backgroundColor: UIConstants.primaryColor,
      body: Stack(
        children: <Widget>[
          SizedBox(
            width: width,
            height: height,
            child: PageView(
              controller: _pageController,
              onPageChanged: _onPageViewChange,
              children: <Widget>[
                /// Slide 1.
                IntroContent(
                  imagePath: 'assets/images/intro_1.png',
                  imageSizeFactor: 0.34,
                  title: I18n.of(context).intro1Title,
                  mainText: I18n.of(context).intro1MainTxt,
                ),

                /// Slide 2.
                IntroContent(
                  imagePath: 'assets/images/intro_2.png',
                  imageSizeFactor: 0.32,
                  title: I18n.of(context).intro2Title,
                  mainText: I18n.of(context).intro2MainTxt,
                ),

                /// Slide 3.
                IntroContent(
                  imagePath: 'assets/images/intro_3.png',
                  imageSizeFactor: 0.20,
                  title: I18n.of(context).intro3Title,
                  mainText: I18n.of(context).intro3MainTxt,
                ),

                /// Slide 4.
                IntroContent(
                  imagePath: 'assets/images/intro_4.png',
                  imageSizeFactor: 0.20,
                  title: I18n.of(context).intro4Title,
                  mainText: I18n.of(context).intro4MainTxt,
                ),
              ],
            ),
          ),

          /// Top logo.
          TopLogo(
            imgPath: _logoPath,
            imgWidth: (_logoWidth == 0) ? height * 0.25 : _logoWidth,
            topPadding: topPadding + height * 0.05,
          ),

          /// Dots indicator.
          Padding(
            padding: EdgeInsets.only(bottom: height * 0.16),
            child: Align(
              alignment: Alignment.bottomCenter,
              child: Padding(
                padding: EdgeInsets.only(top: height * 0.02),

                /// Uses for indicate slider position, we need to highlight slider
                /// index manually. Refer class comment, why we use [DotsIndicator]
                /// & [SpinKitThreeBounce] both in app.
                child: DotsIndicator(
                  dotsCount: 4,
                  position: _currentPage,
                  decorator: const DotsDecorator(
                    color: UIConstants.paleLilac,
                    activeColor: UIConstants.slateGray100,
                  ),
                ),
              ),
            ),
          ),

          /// Main button.
          Padding(
            padding: EdgeInsets.only(
              right: width * 0.075,
              left: width * 0.075,
              bottom: height * 0.05,
            ),
            child: Align(
              alignment: Alignment.bottomCenter,
              child: MainButton(
                type: BtnType.active,
                text: I18n.of(context).introBtnTxt,
                call: () => Navigator.pushNamed(context, '/promise'),
              ),
            ),
          ),
        ],
      ),
    );
  }

  /// Changes top section contents as the slide.
  ///
  /// Only first slide has Schluss text with the logo.
  /// Other slides only have the logo.
  void _onPageViewChange(int page) {
    setState(() => _currentPage = page);

    // If first slide.
    if (page == 0) {
      _logoPath = 'assets/images/logo_with_schluss.svg';
      _logoWidth = MediaQuery.of(context).size.height * 0.25;
    } else {
      _logoPath = 'assets/images/logo.svg';
      _logoWidth = MediaQuery.of(context).size.width * 0.14;
    }
  }
}
