import 'package:flutter/material.dart';
import 'package:schluss_beta_app/constants/ui_constants.dart';
import 'package:schluss_beta_app/translations/i18n.dart';
import 'package:schluss_beta_app/ui/reused_widgets/bottom_up_sheet_template.dart';

class TermsAndConditionsBottomUpSheet extends StatelessWidget {
  const TermsAndConditionsBottomUpSheet({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    double width = MediaQuery.of(context).size.width;
    double height = MediaQuery.of(context).size.height;

    int extraItems = 2;

    return BottomUpSheetTemplate(
      hasLogo: true,
      page: Stack(
        children: [
          /// Title.
          Padding(
            padding: EdgeInsets.only(top: height * 0.03),
            child: Align(
              alignment: Alignment.topCenter,
              child: Text(
                I18n.of(context).termsTitle,
                style: TextStyle(
                  color: UIConstants.grayDarkest100,
                  fontSize: width * 0.06,
                  fontWeight: FontWeight.bold,
                ),
              ),
            ),
          ),

          /// Scrolling section.
          Padding(
            padding: EdgeInsets.only(top: height * 0.1),
            child: Scaffold(
              backgroundColor: UIConstants.primaryColor,
              body: ListView.builder(
                itemBuilder: (BuildContext context, int index) {
                  if (index == 0) {
                    /// Main description.
                    return SizedBox(
                      height: height * 0.13,
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.start,
                        children: [
                          Align(
                            alignment: Alignment.topLeft,
                            child: Text(
                              I18n.of(context).termsMainTxt,
                              style: TextStyle(
                                color: UIConstants.grayDarkest100,
                                fontSize: width * 0.043,
                              ),
                              textAlign: TextAlign.start,
                            ),
                          ),
                        ],
                      ),
                    );
                  } else if (index == 1) {
                    return const Divider(height: 1);
                  }
                  index = index - extraItems;
                  return const Column(
                    children: [
                      EntryItem(),
                      Divider(height: 1),
                    ],
                  );
                },
                itemCount: 1 + extraItems,
              ),
            ),
          ),
        ],
      ),
    );
  }
}

/// Lists view item headers.
class EntryItem extends StatelessWidget {
  const EntryItem({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    double width = MediaQuery.of(context).size.width;

    return Column(
      children: [
        const Divider(height: 1),
        buildTitleExpansionTheme(
          context,
          width,
          I18n.of(context).termsTile2Header,
          'assets/images/idea_icon.png',
          buildChildren(context, width),
        )
      ],
    );
  }

  /// Lists view item header.
  Theme buildTitleExpansionTheme(
    BuildContext context,
    double width,
    String title,
    String imagePath,
    List<Widget> children,
  ) {
    return Theme(
      data: Theme.of(context).copyWith(
        dividerColor: UIConstants.transparent,
        colorScheme: ColorScheme.fromSwatch().copyWith(
          secondary: UIConstants.gray100,
        ),
      ),
      child: ExpansionTile(
        backgroundColor: UIConstants.primaryColor,
        leading: Padding(
          padding: const EdgeInsets.symmetric(
            horizontal: 0.0,
            vertical: 6.0,
          ),
          child: Image(
            image: AssetImage(imagePath),
            width: 30,
          ),
        ),
        title: Padding(
          padding: const EdgeInsets.symmetric(vertical: 8.0),
          child: Text(
            title,
            style: TextStyle(
              fontSize: width * 0.043,
              color: UIConstants.grayDarkest100,
              fontWeight: FontWeight.w600,
            ),
          ),
        ),
        children: children,
      ),
    );
  }

  /// Lists view item.
  List<Widget> buildChildren(BuildContext context, double width) {
    List<Widget> children;
    children = <Widget>[];
    children.add(
      Container(
        color: UIConstants.paleGray,
        child: Padding(
          padding: getMainEdgeInsets(width, width * 0.06),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              buildCheckedItemsColumn(
                width,
                context,
                <Widget>[
                  buildRow(context, I18n.of(context).termsBullet2Txt),
                  buildRow(context, I18n.of(context).termsBullet3Txt),
                  buildRow(context, I18n.of(context).termsBullet4Txt),
                  buildRow(context, I18n.of(context).termsBullet5Txt),
                ],
              ),
              buildWhyText(context, width),
              Padding(
                padding: EdgeInsets.only(bottom: width * 0.03),
                child: getMainTextStyle(
                  I18n.of(context).termsMainPara3Txt,
                  width,
                ),
              ),
              Padding(
                padding: EdgeInsets.only(bottom: width * 0.02),
                child: getSecondaryTextStyle(
                  I18n.of(context).termsSubPara2Txt,
                  width,
                ),
              )
            ],
          ),
        ),
      ),
    );
    return children;
  }

  /// Builds checked items.
  Container buildCheckedItemsColumn(
    double width,
    BuildContext context,
    List<Widget> rows,
  ) {
    return Container(
      width: width,
      padding: EdgeInsets.symmetric(
        vertical: width * 0.02,
        horizontal: width * 0.0475,
      ),
      decoration: BoxDecoration(
        color: UIConstants.primaryColor,
        border: Border.all(color: UIConstants.grayLight),
        borderRadius: const BorderRadius.all(Radius.circular(8)),
      ),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: rows,
      ),
    );
  }

  /// Builds green dark check bullets list view.
  Padding buildRow(BuildContext context, String text) {
    double width = MediaQuery.of(context).size.width;

    return Padding(
      padding: EdgeInsets.symmetric(vertical: width * 0.015),
      child: Row(
        mainAxisSize: MainAxisSize.min,
        mainAxisAlignment: MainAxisAlignment.start,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Padding(
            padding: EdgeInsets.only(right: width * 0.03),
            child: SizedBox(
              width: width * 0.05,
              height: width * 0.05,
              child: const Image(
                image: AssetImage('assets/images/white_check_in_green_icon.png'),
              ),
            ),
          ),
          Flexible(
            child: Text(
              text,
              softWrap: true,
              style: TextStyle(
                fontSize: width * 0.043,
                fontStyle: FontStyle.normal,
                color: UIConstants.grayDarkest100,
              ),
            ),
          ),
        ],
      ),
    );
  }
}

/// Builds 'Why' text section.
Padding buildWhyText(BuildContext context, double width) {
  return Padding(
    padding: EdgeInsets.only(top: width * 0.06, bottom: width * 0.01),
    child: Text(
      I18n.of(context).termsSectionHeader.toUpperCase(),
      style: TextStyle(
        letterSpacing: 1,
        color: UIConstants.gray100,
        fontFamily: UIConstants.subFontFamily,
        fontWeight: FontWeight.w600,
        fontSize: width * 0.034,
      ),
    ),
  );
}

Text getMainTextStyle(String text, double width) {
  return Text(
    text,
    style: TextStyle(
      color: UIConstants.grayDarkest100,
      fontSize: width * 0.043,
    ),
  );
}

Text getSecondaryTextStyle(String text, double width) {
  return Text(
    text,
    style: TextStyle(
      color: UIConstants.gray100,
      fontFamily: UIConstants.subFontFamily,
      height: 1.7,
      fontSize: width * 0.0335,
    ),
  );
}

EdgeInsets getMainEdgeInsets(double width, double topInsets) {
  return EdgeInsets.only(
    left: width * 0.06,
    right: width * 0.06,
    top: topInsets,
    bottom: width * 0.075,
  );
}
