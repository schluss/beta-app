import 'package:flutter/material.dart';
import 'package:schluss_beta_app/constants/ui_constants.dart';

import 'package:schluss_beta_app/ui/intro/intro_image_section.dart';
import 'package:schluss_beta_app/ui/intro/intro_texts_section.dart';

class IntroContent extends StatelessWidget {
  final String? imagePath;
  final double? imageSizeFactor;
  final String? title;
  final String? mainText;

  const IntroContent({
    this.imagePath,
    this.imageSizeFactor,
    this.title,
    this.mainText,
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: UIConstants.primaryColor,
      body: SizedBox(
        width: MediaQuery.of(context).size.width,
        child: Column(
          mainAxisSize: MainAxisSize.max,
          children: <Widget>[
            /// Image section.
            IntroImageSection(
              imagePath: imagePath,
              imageWidth: MediaQuery.of(context).size.height * imageSizeFactor!,
            ),

            /// Bottom section.
            IntroTextsSection(
              title: title,
              mainText: mainText,
            ),
          ],
        ),
      ),
    );
  }
}
