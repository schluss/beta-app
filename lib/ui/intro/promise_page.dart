import 'package:flutter/material.dart';
import 'package:schluss_beta_app/constants/ui_constants.dart';
import 'package:schluss_beta_app/translations/i18n.dart';
import 'package:schluss_beta_app/ui/reused_widgets/main_button.dart';
import 'package:schluss_beta_app/ui/utils/clipper.dart';

import 'package:schluss_beta_app/ui/intro/promise_content.dart';

class PromisePage extends StatelessWidget {
  const PromisePage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    double width = MediaQuery.of(context).size.width;
    double height = MediaQuery.of(context).size.height;
    double topPadding = MediaQuery.of(context).padding.top;

    return Scaffold(
      backgroundColor: UIConstants.primaryColor,
      body: SizedBox(
        height: height,
        width: width,
        child: Stack(
          children: <Widget>[
            ClipPath(
              clipper: Clipper(),
              child: Container(
                color: UIConstants.paleGray,
                height: height / 2,
              ),
            ),
            Padding(
              padding: EdgeInsets.only(
                top: topPadding + height * 0.05,
                right: width * 0.075,
                left: width * 0.075,
                bottom: height * 0.18,
              ),

              /// Contents of the screen.
              child: const PromiseContent(),
            ),

            /// Main button.
            Column(
              mainAxisAlignment: MainAxisAlignment.end,
              children: [
                Padding(
                  padding: EdgeInsets.only(
                    right: width * 0.075,
                    left: width * 0.075,
                    bottom: height * 0.05,
                  ),
                  child: MainButton(
                    type: BtnType.active,
                    text: I18n.of(context).btnNextTxt,
                    call: () => Navigator.pushReplacementNamed(
                      context,
                      '/account/registration/create-pin',
                    ),
                  ),
                ),
              ],
            ),
          ],
        ),
      ),
    );
  }
}
