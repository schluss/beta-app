import 'dart:async';
import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:schluss_beta_app/business/interface/plugin_base.dart';
import 'package:schluss_beta_app/plugins/bkr/ui/web_view_loading.dart';

class PluginBKR implements PluginBase {
  final StreamController<int> _streamController = StreamController.broadcast();

  @override
  void runPlugin(context, Function callBack, String siteUrl, String trigger, String pluginName) {
    Locale locale;
    locale = Localizations.localeOf(context);

    Navigator.push(
      context,
      MaterialPageRoute(
        builder: (context) => WebViewLoading(
          callBack,
          siteUrl,
          trigger,
          locale,
          pluginName: pluginName,
        ),
      ),
    );
  }

//Extract data from xml file
  @override
  Future<Map<String, dynamic>> getExtractedData(String jsonData) async {
    _streamController.add(100); // temporary

    // here we recieved a json encoded string (filePath), to be converted back to a Map<String,dynamic> to convert them to a ResponseAttributeModel
    _streamController.add(60);

    Map<String, dynamic>? attributes;
    attributes = jsonDecode(jsonData);
    _streamController.add(100);
    return Future.value(attributes);
  }

  Stream<int> getProgress() {
    return _streamController.stream; // return stream controller to the main app
  }
}
