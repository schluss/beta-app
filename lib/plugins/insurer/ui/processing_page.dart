import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;

class ProcessingPage extends StatefulWidget {
  final String downloadUrl;

  const ProcessingPage(this.downloadUrl, {Key? key}) : super(key: key);

  @override
  ProcessingPageState createState() => ProcessingPageState();
}

class ProcessingPageState extends State<ProcessingPage> {
  @override
  void initState() {
    super.initState();

    process();
  }

  void process() async {
    // download the whishlist
    var response = await http.get(Uri.parse(widget.downloadUrl));

    if (response.statusCode == 200) {
      // send back the response body while closing this page
      Navigator.pop(context, response.body);
    } else {
      // some error here!
    }
  }

  @override
  Widget build(BuildContext context) {
    //TODO: Might be nice to show a waiting screen
    return const Text('Ophalen...');
  }
}
