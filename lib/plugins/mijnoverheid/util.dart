import 'dart:io';

import 'package:flutter_inappwebview/flutter_inappwebview.dart' as in_app;
import 'package:logger/logger.dart';
import 'package:path_provider/path_provider.dart';
import 'package:requests/requests.dart';

Logger _logger = Logger();

class Util {
  static const String _fileName = 'verzekeringsbericht.pdf';

  // Download and store the PDF file
  static Future<String> downloadAndSaveFile(
    String downloadUrl,
    List<in_app.Cookie> cookies,
  ) async {
    // prepare and format cookies
    Map<String, String?> cookiesMap;
    cookiesMap = {};

    for (var element in cookies) {
      cookiesMap[element.name] = element.value;
    }

    // Prepare request
    String hostname;
    hostname = Requests.getHostname(downloadUrl);
    await Requests.setStoredCookies(hostname, cookiesMap as Map<String, String>);

    // Do the request and get the response
    var response = await Requests.get(downloadUrl);

    _logger.d('UWV plugin - response statuscode: ${response.statusCode}');

    List<int> body;
    body = response.bytes();

    // Store result as a file
    String savePath;
    savePath = await getSavePath(_fileName); // Getting file saving location.

    File f;
    f = File(await getSavePath(_fileName));
    await f.writeAsBytes(body);

    _logger.d('UWV plugin - written to: $savePath');

    return savePath;
  }

  // Make the file saving location
  static Future<String> getSavePath(String fileName) async {
    var dir = await getApplicationDocumentsDirectory();
    return '${dir.path}/$fileName';
  }
}
