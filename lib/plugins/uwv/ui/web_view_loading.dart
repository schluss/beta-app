import 'dart:async';

import 'package:flutter/material.dart';
import 'package:schluss_beta_app/constants/ui_constants.dart';
import 'package:schluss_beta_app/plugins/reused_widgets/loading_indicator.dart';
import 'package:schluss_beta_app/plugins/reused_widgets/mini_header_template.dart';
import 'package:schluss_beta_app/plugins/uwv/ui/web_view_body.dart';
import 'package:schluss_beta_app/translations/i18n.dart';

class WebViewLoading extends StatefulWidget {
  final Function callBack; // function to callback main application
  final String siteUrl;
  final String trigger;
  final String? pluginName;
  final Locale locale;
  final Function(String userName, String password)? storeCredentialCallback;
  final String? userName;
  final String? password;

  const WebViewLoading(
    this.callBack,
    this.siteUrl,
    this.trigger,
    this.locale, {
    Key? key,
    this.pluginName,
    this.storeCredentialCallback,
    this.userName,
    this.password,
  }) : super(key: key);

  @override
  WebViewLoadingState createState() => WebViewLoadingState();
}

class WebViewLoadingState extends State<WebViewLoading> {
  String _url = '';
  bool _isLoading = true;
  // change this variable to show waiting text
  final bool _isWaiting = false;

  late Timer _timer;
  WebViewLoadingState() {
    // set timmer but not work with inappbrowser widget
    _timer = Timer(const Duration(seconds: 3), () {
      setState(() {
        _isLoading = false;
      });
    });
  }

  void _urlChanged(url) {
    setState(() {
      _url = url; // set url change
    });
  }

  void _callBackMethod(String data, BuildContext context) {
    widget.callBack(widget.pluginName, data, context); //callback to main application
  }

  @override
  Widget build(BuildContext context) {
    _url = widget.siteUrl;
    var width = MediaQuery.of(context).size.width;
    var height = MediaQuery.of(context).size.height;
    var loadingText = I18n.of(context).uwvLoadingTxt;
    return Scaffold(
      backgroundColor: Colors.white,
      resizeToAvoidBottomInset: true,
      body: Stack(
        children: <Widget>[
          Container(
            padding: EdgeInsets.only(
              top: MediaQuery.of(context).padding.top + height * 0.1,
            ),
            height: height,
            width: width,
            color: UIConstants.primaryColor,
            child: SizedBox(
              height: height * 0.9,
              width: width,
              child: !_isLoading && !_isWaiting
                  ? WebViewBody(
                      _url,
                      _urlChanged,
                      _callBackMethod,
                      widget.locale,
                    )
                  : Padding(
                      padding: EdgeInsets.only(
                        left: width * 0.07,
                        right: width * 0.07,
                        bottom: height * 0.25,
                      ),
                      child: LoadingIndicator(
                        widget.locale,
                        text: _isLoading ? loadingText : I18n.of(context).pluginProcessingTxt,
                      ),
                    ),
            ),
          ),
          Stack(
            children: <Widget>[
              Align(
                  alignment: Alignment.topCenter,
                  child: Container(
                    height: height * 0.1,
                    color: UIConstants.paleLilac,
                  )),
              Padding(
                padding: EdgeInsets.only(top: MediaQuery.of(context).padding.top),
                child: MiniHeaderTemplate(
                  iconPath: 'assets/images/lock_icon.png',
                  title: _url,
                  locale: widget.locale,
                ),
              ),
            ],
          ),
        ],
      ),
    );
  }

  @override
  void dispose() {
    _timer.cancel();
    super.dispose();
  }
}
