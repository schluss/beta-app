import 'package:flutter/material.dart';
import 'package:logger/logger.dart';
import 'package:schluss_beta_app/constants/ui_constants.dart';
import 'package:schluss_beta_app/translations/i18n.dart';

import 'loading_indicator.dart';
import 'mini_header_template.dart';

Locale? _locale;
late BuildContext _context;

class WaitOverlay {
  static void hide() {
    try {
      if (Navigator.of(_context, rootNavigator: true).canPop()) {
        Navigator.of(_context, rootNavigator: true).pop();
      } else {
        Logger().w('System will not be able to close the Screen. please Review the Navigator Flow.');
      }
    } catch (e) {
      Logger().e(e);
    }
  }

  static void show(BuildContext context, Locale? locale, {String? text}) {
    _context = context;
    var width = MediaQuery.of(_context).size.width;
    var height = MediaQuery.of(_context).size.height;

    text ??= I18n.of(context).uwvLoadingTxt;

    Dialog alert;
    alert = Dialog(
        insetPadding: EdgeInsets.zero,
        child: Scaffold(
          backgroundColor: Colors.white,
          body: Stack(
            children: <Widget>[
              Container(
                padding: EdgeInsets.only(
                  top: MediaQuery.of(context).padding.top + height * 0.1,
                ),
                height: height,
                width: width,
                color: UIConstants.primaryColor,
                child: Padding(
                  padding: EdgeInsets.only(
                    left: width * 0.07,
                    right: width * 0.07,
                    bottom: height * 0.25,
                  ),
                  child: LoadingIndicator(_locale, text: text),
                ),
              ),
              Stack(
                children: <Widget>[
                  Container(
                    color: UIConstants.paleLilac,
                    child: MiniHeaderTemplate(locale: locale),
                  ),
                ],
              ),
            ],
          ),
        ));

    showDialog(
      barrierDismissible: false,
      context: _context,
      builder: (BuildContext context) {
        return alert;
      },
    );
  }
}
