import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:schluss_beta_app/constants/ui_constants.dart';
import 'package:schluss_beta_app/plugins/reused_widgets/auth_warning_list.dart';
import 'package:schluss_beta_app/plugins/reused_widgets/backward_button.dart';
import 'package:schluss_beta_app/plugins/reused_widgets/closing_button.dart';
import 'package:schluss_beta_app/plugins/reused_widgets/main_button.dart';
import 'package:url_launcher/url_launcher.dart';

late BuildContext ctx;

class AuthSMSWarning extends StatelessWidget {
  final Locale locale;

  const AuthSMSWarning(this.locale, {Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    ctx = context;
    var width = MediaQuery.of(context).size.width;
    var height = MediaQuery.of(context).size.height;

    return Scaffold(
      backgroundColor: Colors.white,
      body: Stack(
        children: <Widget>[
          Container(
            padding: EdgeInsets.only(
              top: MediaQuery.of(context).padding.top + height * 0.1,
            ),
            height: height,
            width: width,
            color: UIConstants.primaryColor,
            child: SizedBox(
              height: height * 0.9,
              width: width,
              child: Padding(
                  padding: EdgeInsets.only(
                    left: width * 0.07,
                    right: width * 0.07,
                    bottom: 0,
                  ),
                  child: ListView(children: [
                    const SizedBox(height: 20),
                    Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        Image.asset(
                          'assets/images/digid_and_sms.png',
                          width: width * 0.4,
                        ),
                      ],
                    ),
                    const SizedBox(height: 20),
                    Text(
                      'Oeps! Je sms-controle staat uit',
                      style: TextStyle(
                        color: UIConstants.grayDark,
                        fontSize: width * 0.05,
                        fontWeight: FontWeight.bold,
                      ),
                      textAlign: TextAlign.center,
                    ),
                    const SizedBox(height: 20),
                    Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: [
                        Text(
                          'Dat heb je nodig voor inloggen met DigiD bij UWV.',
                          style: TextStyle(
                            color: UIConstants.gray100,
                            fontSize: width * 0.05,
                          ),
                          textAlign: TextAlign.left,
                        ),
                      ],
                    ),
                    const SizedBox(height: 20),
                    AuhtWarningList([
                      AuhtWarningListItem(
                        '1',
                        'Ga naar',
                        child: RichText(
                          text: TextSpan(
                            text: '',
                            style: TextStyle(
                              color: UIConstants.gray100,
                              fontSize: width * 0.05,
                            ),
                            children: [
                              TextSpan(
                                text: 'www.digid.nl',
                                style: const TextStyle(
                                  decoration: TextDecoration.underline,
                                ),
                                recognizer: TapGestureRecognizer()
                                  ..onTap = () async {
                                    const url = 'https://www.digid.nl';
                                    if (await canLaunch(url)) await launch(url, forceWebView: false, forceSafariVC: false);
                                  },
                              ),
                            ],
                          ),
                        ),
                      ),
                      const AuhtWarningListItem('2', 'Klik op Mijn DigiD'),
                      const AuhtWarningListItem('3', 'Log in'),
                      const AuhtWarningListItem('4', 'Ga naar \'sms-controle\' en klik op \'sms-controle activeren\''),
                    ]),
                    const SizedBox(height: 20),
                    Padding(
                      padding: EdgeInsets.only(
                        right: width * 0.075,
                        left: width * 0.075,
                        bottom: 0,
                      ),
                      child: Align(
                        alignment: Alignment.bottomCenter,
                        child: MainButton('Opnieuw inloggen bij UWV', triggerBack),
                      ),
                    )
                  ])),
            ),
          ),
          // header
          Stack(
            children: <Widget>[
              Align(
                  alignment: Alignment.topCenter,
                  child: Container(
                    height: height * 0.1,
                    color: UIConstants.paleLilac,
                  )),
              Padding(
                  padding: EdgeInsets.only(top: MediaQuery.of(context).padding.top),
                  child: Container(
                    decoration: const BoxDecoration(
                      color: Colors.white,
                      borderRadius: BorderRadius.only(
                        topLeft: Radius.circular(20),
                        topRight: Radius.circular(20),
                      ),
                    ),
                    height: height * 0.1,
                    alignment: Alignment.center,
                    child: Stack(
                      children: <Widget>[
                        // back arrow
                        Padding(
                          padding: EdgeInsets.only(left: height * 0.02),
                          child: Align(
                            alignment: Alignment.centerRight,
                            child: BackwardButton(
                              locale: locale,
                              call: triggerBack,
                            ),
                          ),
                        ),
                        // circle close button
                        Padding(
                          padding: EdgeInsets.only(right: height * 0.02),
                          child: Align(
                            alignment: Alignment.centerRight,
                            child: ClosingButton(
                              call: triggerBack,
                            ),
                          ),
                        ),
                      ],
                    ),
                  )),
            ],
          ),
        ],
      ),
    );
  }

  /*back button trigger */
  void triggerBack() {
    Navigator.pop(ctx);
  }
}
