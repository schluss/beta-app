import 'package:flutter/material.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:schluss_beta_app/constants/ui_constants.dart';

/*
 * preview until page loaing 
 */
class LoadingIndicator extends StatelessWidget {
  final Locale? locale;
  final String? text;
  const LoadingIndicator(this.locale, {Key? key, this.text}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    var width = MediaQuery.of(context).size.width;
    var height = MediaQuery.of(context).size.height;
    return Column(
      mainAxisAlignment: MainAxisAlignment.center,
      crossAxisAlignment: CrossAxisAlignment.center,
      children: <Widget>[
        Text(
          text!,
          style: TextStyle(color: UIConstants.gray100, fontSize: width * 0.05),
          textAlign: TextAlign.center,
        ),
        Padding(
          padding: EdgeInsets.only(top: height * 0.02),
          child: const SizedBox(
            height: 16,
            child: SpinKitThreeBounce(
              color: UIConstants.paleLilac,
              size: 16,
            ),
          ),
        )
      ],
    );
  }
}
