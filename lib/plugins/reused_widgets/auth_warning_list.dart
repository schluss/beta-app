import 'package:flutter/material.dart';
import 'package:schluss_beta_app/constants/ui_constants.dart';

class AuhtWarningList extends StatelessWidget {
  const AuhtWarningList(this.children, {Key? key}) : super(key: key);
  final List<Widget> children;

  @override
  Widget build(BuildContext context) {
    var widgetList = <Widget>[];

    for (var element in children) {
      // Add list item
      widgetList.add(element);
      // Add space between items
      widgetList.add(const SizedBox(height: 15.0));
    }

    return Column(children: widgetList);
  }
}

class AuhtWarningListItem extends StatelessWidget {
  const AuhtWarningListItem(this.leading, this.text, {Key? key, this.child}) : super(key: key);
  final String leading;
  final String text;
  final Widget? child;

  @override
  Widget build(BuildContext context) {
    var width = MediaQuery.of(context).size.width;

    return Row(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: <Widget>[
        Padding(
          padding: const EdgeInsets.only(left: 10, right: 15),
          child: Text(
            leading,
            style: TextStyle(
              color: const Color.fromRGBO(165, 175, 198, 1),
              fontSize: width * 0.05,
            ),
          ),
        ),
        Expanded(
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            mainAxisAlignment: MainAxisAlignment.start,
            children: [
              Text(
                text,
                style: TextStyle(
                  color: UIConstants.gray100,
                  fontSize: width * 0.05,
                ),
              ),
              child ?? const Column() // Short form: child != null ? child : Column()
            ],
          ),
        ),
      ],
    );
  }
}
