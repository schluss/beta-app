import 'package:flutter/material.dart';
import 'package:schluss_beta_app/constants/ui_constants.dart';
import 'package:schluss_beta_app/translations/i18n.dart';

class BackwardButton extends StatelessWidget {
  final Locale? locale;
  final Color color;
  final Function? call;

  const BackwardButton({
    Key? key,
    this.locale,
    this.color = UIConstants.gray100,
    this.call,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    var width = MediaQuery.of(context).size.width;

    return Align(
      alignment: Alignment.centerLeft,
      child: GestureDetector(
        onTap: () => call ?? Navigator.pop(context),
        child: Row(
          children: <Widget>[
            Icon(
              Icons.keyboard_arrow_left,
              color: color,
              size: width * 0.06,
            ),
            Text(
              I18n.of(context).btnBackTxt,
              style: TextStyle(
                color: color,
                fontFamily: 'Graphik',
                fontSize: width * 0.042,
              ),
            ),
          ],
        ),
      ),
    );
  }
}
