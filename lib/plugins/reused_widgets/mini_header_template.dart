import 'package:flutter/material.dart';
import 'package:schluss_beta_app/constants/ui_constants.dart';
import 'package:schluss_beta_app/plugins/reused_widgets/backward_button.dart';
import 'package:schluss_beta_app/plugins/reused_widgets/closing_button.dart';

class MiniHeaderTemplate extends StatelessWidget {
  final String? iconPath;
  final String? title;
  final Color titleColor;
  final Locale? locale;
  final bool isDividerVisible;
  final Function? backBtnCall;
  final Function? closeBtnCall;

  const MiniHeaderTemplate({
    Key? key,
    this.iconPath,
    this.title = '',
    this.titleColor = UIConstants.accentColor,
    this.locale,
    this.isDividerVisible = true,
    this.backBtnCall,
    this.closeBtnCall,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    var width = MediaQuery.of(context).size.width;
    var height = MediaQuery.of(context).size.height;

    return Container(
      decoration: const BoxDecoration(
        color: UIConstants.primaryColor,
        borderRadius: BorderRadius.only(
          topLeft: Radius.circular(20),
          topRight: Radius.circular(20),
        ),
      ),
      height: height * 0.08,
      alignment: Alignment.center,
      child: Stack(
        children: <Widget>[
          /// Title.
          Align(
            alignment: Alignment.center,
            child: Container(
              margin: EdgeInsets.only(left: height * 0.01),
              child: ConstrainedBox(
                constraints: BoxConstraints(
                  maxWidth: width * 0.615,
                ),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: <Widget>[
                    (iconPath != null)
                        ? Image.asset(
                            iconPath!,
                            height: width * 0.045,
                          )
                        : Container(),
                    Flexible(
                      child: Text(
                        title!,
                        maxLines: 1,
                        overflow: TextOverflow.ellipsis,
                        textAlign: TextAlign.left,
                        style: TextStyle(
                          color: titleColor,
                          fontSize: width * 0.04,
                          fontWeight: FontWeight.w700,
                          fontStyle: FontStyle.normal,
                        ),
                      ),
                    ),
                  ],
                ),
              ),
            ),
          ),

          /// Back button.
          Padding(
            padding: EdgeInsets.only(left: height * 0.01),
            child: BackwardButton(
              locale: locale,
              call: backBtnCall,
            ),
          ),

          /// Close button.
          Padding(
            padding: EdgeInsets.only(right: height * 0.02),
            child: ClosingButton(
              call: closeBtnCall,
            ),
          ),

          /// Divider.
          isDividerVisible
              ? const Align(
                  alignment: Alignment.bottomCenter,
                  child: Divider(height: 1),
                )
              : Container(),
        ],
      ),
    );
  }
}
