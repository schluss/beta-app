import 'dart:async';

import 'package:flutter/material.dart';
import 'package:schluss_beta_app/business/interface/plugin_base.dart';
import 'package:schluss_beta_app/plugins/me/ui/personal_info_input_page.dart';

class PluginMe implements PluginBase {
  final StreamController<int> _streamController = StreamController.broadcast();
  Map<String, String> fields = {
    'Street': 'Straat',
    'HouseNumber': 'Huisnummer',
    'PostalCode': 'Postcode',
    'City': 'Plaats',
  };

  /// Initiates the plug-in for getting user input values.
  @override
  void runPlugin(context, Function callBack, String siteUrl, String trigger, String pluginName) {
    var locale = Localizations.localeOf(context); // Sets localization.

    Navigator.push(
      context,
      MaterialPageRoute(
        builder: (context) => PersonalInfoInputPage(locale, fields, callBack, pluginName),
      ),
    );
  }

  /// Extracts data and sets into a map of attributes.
  @override
  Future<Map<String, dynamic>> getExtractedData(String pluginData) async {
    var attributes = <String, String>{};
    var inputData = pluginData.split(',');

    // Maps fields and input values.
    var index = 0;
    for (var entry in fields.entries) {
      attributes[entry.key] = inputData[index];
      index++;
    }

    _streamController.add(100);

    return Future.value(attributes);
  }

  /// Gets the progress value of the process.
  Stream<int> getProgress() {
    return _streamController.stream; // Returns stream controller to the main app.
  }
}
