import 'package:flutter/material.dart';
import 'package:schluss_beta_app/constants/ui_constants.dart';
import 'package:schluss_beta_app/plugins/me/ui/page_header.dart';
import 'package:schluss_beta_app/plugins/me/ui/user_info.dart';
import 'package:schluss_beta_app/translations/i18n.dart';
import 'package:schluss_beta_app/ui/reused_widgets/main_button.dart';

class PersonalInfoInputPage extends StatefulWidget {
  final Locale locale;
  final Map<String, String> attributes;
  final Function callBack;
  final String? pluginName;

  const PersonalInfoInputPage(this.locale, this.attributes, this.callBack, this.pluginName, {Key? key}) : super(key: key);

  @override
  PersonalInfoInputPageState createState() => PersonalInfoInputPageState();
}

class PersonalInfoInputPageState extends State<PersonalInfoInputPage> {
  Map<String, TextEditingController> textEditingControllers = {};
  String inputData = '';
  bool isEdited = false;

  @override
  void initState() {
    super.initState();

    // convert attributelist to texteditingcontrollerlist
    for (var att in widget.attributes.entries) {
      textEditingControllers[att.key] = TextEditingController();
    }
  }

  @override
  void dispose() {
    super.dispose();
    // dispose textEditingControllers to prevent memory leaks
    textEditingControllers.forEach((key, value) {
      value.dispose();
    });
  }

  @override
  Widget build(BuildContext context) {
    var width = MediaQuery.of(context).size.width;
    var height = MediaQuery.of(context).size.height;

    return Scaffold(
      backgroundColor: UIConstants.primaryColor,
      body: Stack(
        children: <Widget>[
          SingleChildScrollView(
            child: Container(
              height: height,
              width: width,
              color: UIConstants.primaryColor,
              child: ListView(
                padding: EdgeInsets.only(
                  top: height * 0.1 + MediaQuery.of(context).padding.top,
                  bottom: height * 0.4,
                ),
                children: <Widget>[
                  SizedBox(height: height * 0.02),
                  Stack(
                    children: <Widget>[
                      UserInfo(
                        I18n.of(context).providerInfoSrcTxt,
                        widget.pluginName,
                        heightScale: 0.064,
                        isTitleBold: true,
                        isSubTitleBold: true,
                        isEditable: false,
                      ),
                    ],
                  ),
                  SizedBox(height: height * 0.025),
                  ...buildChildren(
                    textEditingControllers,
                    width,
                    height,
                    context,
                  ),
                ],
              ),
            ),
          ),
          Stack(
            children: <Widget>[
              Align(
                alignment: Alignment.topCenter,
                child: Container(
                  height: height * 0.1,
                  color: UIConstants.paleLilac,
                ),
              ),
              PageHeader(
                widget.locale,
                'Gegevens invoeren',
              ),
            ],
          ),
        ],
      ),
      bottomNavigationBar: Container(
        color: UIConstants.primaryColor,
        child: Column(
          mainAxisSize: MainAxisSize.min,
          children: [
            /// Main button.
            Padding(
              padding: EdgeInsets.only(
                top: height * 0.03,
                bottom: height * 0.025,
                left: width * 0.05,
                right: width * 0.05,
              ),
              child: Align(
                alignment: Alignment.bottomCenter,
                child: MainButton(
                  type: BtnType.active,
                  text: 'Opslaan',
                  call: () async {
                    _sendBackData();
                  },
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }

  /// Builds Children providers.
  List<Widget> buildChildren(Map<String, TextEditingController> fields, double width, double height, BuildContext context) {
    List<Widget> children = <Widget>[];

    for (var field in fields.entries) {
      children.add(
        UserInfo(
          widget.attributes[field.key],
          '',
          call: field.value,
        ),
      );
    }

    return children;
  }

  /// Sends data back to the main app.
  void _sendBackData() {
    var values = StringBuffer();

    for (var field in textEditingControllers.entries) {
      values.write('${field.value.text},');
    }

    widget.callBack(widget.pluginName, values.toString(), context);
  }
}
