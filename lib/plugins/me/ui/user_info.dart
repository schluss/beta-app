import 'package:flutter/material.dart';
import 'package:schluss_beta_app/constants/ui_constants.dart';

class UserInfo extends StatefulWidget {
  final String? title;
  final String? subTitle;
  final double heightScale;
  final bool isSubTitleBold;
  final bool isTitleBold;
  final bool isEditable;
  final TextEditingController? call;

  const UserInfo(
    this.title,
    this.subTitle, {
    Key? key,
    this.heightScale = 0.1,
    this.isSubTitleBold = false,
    this.isTitleBold = false,
    this.isEditable = true,
    this.call,
  }) : super(key: key);

  @override
  UserInfoState createState() => UserInfoState();
}

class UserInfoState extends State<UserInfo> {
  late FocusScopeNode currentFocus;

  void unfocus() {
    currentFocus = FocusScope.of(context);

    if (!currentFocus.hasPrimaryFocus) {
      currentFocus.unfocus();
    }
  }

  @override
  Widget build(BuildContext context) {
    var width = MediaQuery.of(context).size.width;
    var height = MediaQuery.of(context).size.height;

    return GestureDetector(
      onTap: unfocus,
      child: Container(
        height: height * widget.heightScale,
        width: width,
        color: UIConstants.primaryColor,
        child: Padding(
          padding: EdgeInsets.symmetric(horizontal: width * 0.075),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              Padding(
                padding: const EdgeInsets.only(left: 2.0, bottom: 4.0),
                child: Text(
                  widget.title ?? '',
                  style: TextStyle(
                    fontFamily: UIConstants.subFontFamily,
                    fontWeight: widget.isTitleBold ? FontWeight.w600 : FontWeight.w400,
                    fontStyle: FontStyle.normal,
                    color: UIConstants.grayDarkest100,
                    letterSpacing: 1.375,
                    fontSize: width * 0.03,
                  ),
                ),
              ),
              widget.isEditable
                  ? TextField(
                      decoration: InputDecoration(
                        border: const OutlineInputBorder(
                          borderRadius: BorderRadius.all(Radius.circular(4)),
                          borderSide: BorderSide(width: 1),
                        ),
                        enabledBorder: const OutlineInputBorder(
                          borderSide: BorderSide(color: UIConstants.paleLilac),
                        ),
                        focusedBorder: const OutlineInputBorder(
                          borderSide: BorderSide(color: UIConstants.paleGray),
                        ),
                        errorBorder: const OutlineInputBorder(
                          borderSide: BorderSide(color: Colors.redAccent),
                        ),
                        focusedErrorBorder: const OutlineInputBorder(
                          borderSide: BorderSide(color: Colors.orangeAccent),
                        ),
                        disabledBorder: const OutlineInputBorder(
                          borderSide: BorderSide(color: Colors.white),
                        ),
                        contentPadding: const EdgeInsets.all(10.0),
                        isDense: true,
                        hintText: widget.subTitle,
                        hintStyle: TextStyle(
                          fontFamily: UIConstants.subFontFamily,
                          fontWeight: FontWeight.w300,
                          fontStyle: FontStyle.normal,
                          color: UIConstants.paleGray,
                          letterSpacing: 1.00,
                          fontSize: width * 0.04,
                          decoration: TextDecoration.none,
                        ),
                      ),
                      style: TextStyle(
                        fontFamily: UIConstants.subFontFamily,
                        fontWeight: FontWeight.w300,
                        fontStyle: FontStyle.normal,
                        color: UIConstants.grayDarkest100,
                        letterSpacing: 1.00,
                        fontSize: width * 0.04,
                        decoration: TextDecoration.none,
                      ),
                      keyboardType: TextInputType.visiblePassword,
                      cursorColor: UIConstants.black,
                      controller: widget.call,
                    )
                  : Text(
                      widget.subTitle!,
                      style: TextStyle(
                        color: UIConstants.black,
                        fontWeight: widget.isSubTitleBold ? FontWeight.w600 : FontWeight.w400,
                        fontStyle: FontStyle.normal,
                        letterSpacing: 0,
                        fontSize: width * 0.04,
                      ),
                    ),
            ],
          ),
        ),
      ),
    );
  }
}
