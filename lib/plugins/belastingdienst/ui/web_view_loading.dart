import 'dart:async';

import 'package:flutter/material.dart';
import 'package:schluss_beta_app/constants/ui_constants.dart';
import 'package:schluss_beta_app/plugins/belastingdienst/ui/web_view_body.dart';
import 'package:schluss_beta_app/plugins/reused_widgets/loading_indicator.dart';
import 'package:schluss_beta_app/plugins/reused_widgets/mini_header_template.dart';
import 'package:schluss_beta_app/translations/i18n.dart';

class WebViewLoading extends StatefulWidget {
  final Function callBack; // function to callback main application
  final String initialUrl;
  final String trigger;
  final String? pluginName;
  final Locale locale;

  const WebViewLoading(this.callBack, this.initialUrl, this.trigger, this.locale, this.pluginName, {Key? key}) : super(key: key);

  @override
  WebViewLoadingState createState() => WebViewLoadingState();
}

class WebViewLoadingState extends State<WebViewLoading> {
  var _url = '';
  bool _isLoading = true;
  late Timer _timer;

  WebViewLoadingState() {
    // set timer but not work with inappbrowser widget
    _timer = Timer(const Duration(seconds: 3), () {
      setState(() {
        _isLoading = false;
      });
    });
  }

  void _urlChanged(url) {
    setState(() {
      _url = url; // set url change
    });
  }

  void _callBackMethod(String status, BuildContext context) {
    widget.callBack(widget.pluginName, status, context); //callback to main application
  }

  @override
  Widget build(BuildContext context) {
    _url = widget.initialUrl;
    var width = MediaQuery.of(context).size.width;
    var height = MediaQuery.of(context).size.height;
    var loadingText = I18n.of(context).belastingdienstLoadingTxt;

    return Scaffold(
      backgroundColor: Colors.white,
      resizeToAvoidBottomInset: true,
      body: Stack(
        children: <Widget>[
          Container(
            padding: EdgeInsets.only(
              top: MediaQuery.of(context).padding.top + height * 0.1,
            ),
            height: height,
            width: width,
            color: UIConstants.primaryColor,
            child: SizedBox(
              height: height * 0.9,
              width: width,
              child: !_isLoading
                  ? WebViewBody(_url, _urlChanged, _callBackMethod, widget.trigger, widget.locale)
                  : Padding(
                      padding: EdgeInsets.only(
                        left: width * 0.07,
                        right: width * 0.07,
                        bottom: height * 0.25,
                      ),
                      child: LoadingIndicator(
                        widget.locale,
                        text: _isLoading ? loadingText : I18n.of(context).pluginProcessingTxt,
                      ),
                    ),
            ),
          ),
          Stack(
            children: <Widget>[
              Align(
                  alignment: Alignment.topCenter,
                  child: Container(
                    height: height * 0.1,
                    color: UIConstants.paleLilac,
                  )),
              Padding(
                padding: EdgeInsets.only(
                  top: MediaQuery.of(context).padding.top,
                ),
                child: MiniHeaderTemplate(
                  iconPath: 'assets/images/lock_icon.png',
                  title: _url,
                  locale: widget.locale,
                ),
              ),
            ],
          ),
        ],
      ),
    );
  }

  @override
  void dispose() {
    _timer.cancel();
    super.dispose();
  }
}
