import 'dart:async';
import 'dart:io';

import 'package:flutter/material.dart';
import 'package:schluss_beta_app/business/interface/plugin_base.dart';
import 'package:schluss_beta_app/plugins/belastingdienst/template.dart';
import 'package:schluss_beta_app/plugins/belastingdienst/ui/web_view_loading.dart';

class PluginBelasting implements PluginBase {
  final StreamController<int> _streamController = StreamController.broadcast();

  @override
  void runPlugin(context, Function callBack, String siteUrl, String trigger, String pluginName) {
    var locale = Localizations.localeOf(context);

    Navigator.push(
      context,
      MaterialPageRoute(
        builder: (context) => WebViewLoading(callBack, siteUrl, trigger, locale, pluginName),
      ),
    );
  }

//Extract data from xml file
  @override
  Future<Map<String, dynamic>> getExtractedData(String filePath) async {
    // get file as string
    File file = File(filePath);
    String xmlStr = await file.readAsString();

    // parse attributes
    Map<String, dynamic> attributes = await Template.parseXML(xmlStr, _streamController);

    // delete file
    file.deleteSync(recursive: true);

    // return attributes
    return Future.value(attributes);
  }

  Stream<int> getProgress() {
    return _streamController.stream; // return stream controller to the main app
  }
}
