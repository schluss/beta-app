import 'dart:async';
import 'dart:convert';

import 'package:logger/logger.dart';
import 'package:xml/xml.dart';

Logger _logger = Logger();

class Template {
  static Future<Map<String, dynamic>> parseXML(String xmlString, StreamController<int> streamController) async {
    Map<String, dynamic> attributes;
    attributes = <String, dynamic>{};
    XmlDocument doc;

    try {
      doc = XmlDocument.parse(xmlString);

      // TaxYear
      attributes['TaxYear'] = extractElement(doc, ['h:Belastingjaar', 'Belastingjaar']);

      streamController.add(5);

      // Watermark
      attributes['Watermark'] = extractElement(doc, ['h:Watermerk', 'Watermerk']);
      streamController.add(10);
      // Initials
      attributes['Initials'] = getInitials(extractElement(doc, ['h:NaamAangever', 'NaamAangever']));
      streamController.add(15);
      // Surname
      attributes['Surname'] = getLastName(extractElement(doc, ['h:NaamAangever', 'NaamAangever']));
      streamController.add(20);
      // DateOfBirth
      attributes['DateOfBirth'] = extractElement(doc, ['h:GebDatumAangever', 'GebDatumAangever']);
      streamController.add(25);
      // Accountnumber
      attributes['Accountnumber'] = extractElement(doc, ['h:BankrekNrBRGAangever', 'BankrekNrBRGAangever']);
      streamController.add(30);
      // ProvisionalBalance
      attributes['ProvisionalBalance'] = extractElement(doc, ['h:SaldoVA', 'SaldoVA']);
      streamController.add(35);
      // ProvisionalMeans
      attributes['ProvisionalMeans'] = extractElement(doc, ['h:Middel', 'Middel']);
      streamController.add(40);
      // ProvisionalDeclarationNumber
      attributes['ProvisionalDeclarationNumber'] = extractElement(doc, ['h:AanslagnrVA', 'AanslagnrVA']);
      streamController.add(45);
      // ProvisionalDate
      attributes['ProvisionalDate'] = extractElement(doc, ['h:DagtekeningVA', 'DagtekeningVA']);
      streamController.add(50);
      // Incomes
      attributes['Incomes'] = getIncome(doc);
      streamController.add(60);
      // Assets
      attributes['Assets'] = getAssets(doc);
      streamController.add(72);
      // Debts
      attributes['Debts'] = getDebts(doc);
      streamController.add(80);
      // RealEstates
      attributes['RealEstates'] = getRealEstate(doc);
      streamController.add(95);
      // Insurances
      attributes['Insurances'] = getInsurances(doc);
      streamController.add(100);
      return Future.value(attributes);
    } catch (e) {
      return attributes;
    }
  }

  static String extractElement(XmlDocument doc, List<String> searchItems) {
    try {
      for (var search in searchItems) {
        var elements = doc.findAllElements(search);

        if (elements.isEmpty) {
          _logger.d('plugin-belastingdienst - Attribute not found: $search');
          continue;
        }

        _logger.d('plugin-belastingdienst - Attribute found: $search');
        return elements.first.innerText;
      }

      return '';
    } catch (e) {
      _logger.d('plugin-belastingdienst - Attribute not found: ${searchItems.join(', ')}');
      return '';
    }
  }

  static String extractChild(XmlElement element, List<String> searchItems) {
    try {
      for (var search in searchItems) {
        var child = element.getElement(search);

        if (child == null) {
          _logger.d('plugin-belastingdienst - Could not get value for: ${element.name}.$search');
          continue;
        }

        _logger.d('plugin-belastingdienst - Value found for: ${element.name}$search');
        return child.innerText;
      }

      return '';
    } catch (e) {
      _logger.d('plugin-belastingdienst - Could not get value for:${searchItems.join(', ')}');
      return '';
    }
  }

  static Iterable<XmlElement>? findAllElements(XmlDocument doc, List<String> searchItems) {
    Iterable<XmlElement>? elements;

    try {
      for (var i = 0; i < searchItems.length; i++) {
        elements = doc.findAllElements(searchItems[i]);

        if (elements.isEmpty) {
          continue;
        } else {
          _logger.d('plugin-belastingdienst - Value found for: ${searchItems[i]}');
          break;
        }
      }

      return elements;
    } catch (e) {
      _logger.d('plugin-belastingdienst - Could not get value for:${searchItems.join(', ')}');
      return <XmlElement>[];
    }
  }

  static String getDebts(XmlDocument doc) {
    var attributes = <Map<String, dynamic>>[];

    List<XmlElement> items;
    items = <XmlElement>[];

    // RekSchuld
    items.addAll(findAllElements(doc, ['h:RekSchuld', 'RekSchuld'])!);

    for (var element in items) {
      var attribute = <String, dynamic>{};

      // Description
      attribute['Description'] = extractChild(element, ['h:LabelBankprodRekSchuld', 'LabelBankprodRekSchuld']);

      // ContractNumber
      attribute['ContractNumber'] = extractChild(element, ['h:RekNrRekSchuld', 'RekNrRekSchuld']);

      // Supplier
      attribute['Supplier'] = extractChild(element, ['h:RSINBronBank', 'RSINBronBank']);

      // TypeOfAccount
      attribute['TypeOfAccount'] = extractChild(element, ['h:GezBelangRekSchuld', 'GezBelangRekSchuld']);

      // RecordType
      attribute['RecordType'] = extractChild(element, ['h:RecordTypeRekSchuld', 'RecordTypeRekSchuld']);

      // IsMortgage
      var hypothecairSchuldInd = extractChild(element, ['h:HypothecairSchuldInd', 'HypothecairSchuldInd']);

      if (hypothecairSchuldInd == '01') {
        attribute['IsMortgage'] = 1;
      } else if (hypothecairSchuldInd == '00') {
        attribute['IsMortgage'] = 0;
      } else {
        attribute['IsMortgage'] = 'Unknown';
      }

      // Type
      var annuitairSchuldInd = extractChild(element, ['h:AnnuitairSchuldInd', 'AnnuitairSchuldInd']);

      if (annuitairSchuldInd == '00') {
        attribute['Type'] = 'NonRepaying';
      } else if (hypothecairSchuldInd == '01') {
        attribute['Type'] = 'Repaying';
      } else {
        attribute['Type'] = 'Unknown';
      }

      // Get some details about the debt 'start'
      var begin = element.getElement('h:BeginstandSchuld');
      begin ??= element.getElement('BeginstandSchuld');

      if (begin != null) {
        // StartValue
        attribute['StartValue'] = extractChild(element, ['h:SaldoRekSchuldEuro', 'SaldoRekSchuldEuro']);
      }

      //items.addAll(findAllElementsInElement(element, ['h:BeginstandSchuld', 'BeginstandSchuld']));

      /*
      // LoanPartValue
      var loanPartValues = <String, dynamic>{};

      // LoanSum
      loanPartValues['LoanSum'] = extractChild(element, ['h:StartbedragLeningSchuld', 'StartbedragLeningSchuld']);

      // StartFiscalYear
      loanPartValues['StartFiscalYear'] = extractChild(element, ['h:StartbedragLeningSchuld', 'StartbedragLeningSchuld']);

      // EndFiscalYear
      loanPartValues['EndFiscalYear'] = extractChild(element, ['h:StartbedragLeningSchuld', 'StartbedragLeningSchuld']);

      // EWS
      loanPartValues['EWS'] = extractChild(element, ['h:StartbedragLeningSchuld', 'StartbedragLeningSchuld']);

       // CalculatedAnnuity
      loanPartValues['CalculatedAnnuity'] = extractChild(element, ['h:StartbedragLeningSchuld', 'StartbedragLeningSchuld']);     

      attribute['LoanPartValue'] = jsonEncode(loanPartValues);
      */

      // add map to list
      attributes.add(attribute);
    }

    // convert to string and return
    return jsonEncode(attributes);
  }

  static String getAssets(XmlDocument doc) {
    var attributes = <Map<String, dynamic>>[];

    List<XmlElement> items;
    items = <XmlElement>[];

    // BankrekBezit
    items.addAll(findAllElements(doc, ['h:BankrekBezit', 'BankrekBezit'])!);

    // EffectenrekBezit
    items.addAll(findAllElements(doc, ['h:EffectenrekBezit', 'EffectenrekBezit'])!);

    if (items.isEmpty) {
      _logger.d('plugin-belastingdienst - Could not retrieve Assets, no elements named BankrekBezit and EffectenrekBezit found');
    } else {
      for (var element in items) {
        var attribute = <String, dynamic>{};

        // AccountNumber
        attribute['AccountNumber'] = extractChild(element, ['h:RekNr', 'RekNr']);

        // AccountNumberAddition
        attribute['AccountNumberAddition'] = extractChild(element, ['h:ProductIdEffectenrek', 'ProductIdEffectenrek']);

        // Supplier
        attribute['Supplier'] = extractChild(element, ['h:NaamBank', 'NaamBank']);

        // Description
        attribute['Description'] = extractChild(element, ['h:LabelBankprod', 'LabelBankprod']);

        // Owners
        attribute['Owners'] = extractChild(element, ['h:GezBelang', 'GezBelang']);

        // Type
        var type = extractChild(element, ['h:SoortSaldo', 'SoortSaldo']);
        var recordType = extractChild(element, ['h:Recordtype', 'Recordtype']);

        if (type == '01') {
          attribute['Type'] = 'MutualFundGreen';
        } else if (type == '11' || type == '12' || type == '31') {
          attribute['Type'] = 'MutualFund';
        } else if (type == '21' || type == '22' || type == '23' || type == '24' || type == '25' || type == '26' || type == '29') {
          attribute['Type'] = 'SavingsAccount';
        } else if (type == '41' && (recordType == '5' || recordType == '05')) {
          attribute['Type'] = 'SavingsAccount';
        } else if (type == '41' && (recordType == '7' || recordType == '07')) {
          attribute['Type'] = 'MutualFund';
        } else {
          attribute['Type'] = 'MiscellaneousAsset';
        }

        attribute['TypeOrigValue'] = type;

        // RecordType
        attribute['RecordType'] = recordType;

        // Value
        attribute['Value'] = extractChild(element, ['h:BSaldo', 'BSaldo']);

        // add map to list
        attributes.add(attribute);
      }
    }

    // BankrekBuitenlIndBezit
    items.clear();
    items.addAll(findAllElements(doc, ['h:BankrekBuitenlIndBezit', 'BankrekBuitenlIndBezit'])!);

    if (items.isEmpty) {
      _logger.d('plugin-belastingdienst - Could not retrieve Assets, no elements named BankrekBuitenlIndBezit found');
    } else {
      for (var element in items) {
        var attribute = <String, dynamic>{};

        // AccountNumber
        attribute['AccountNumber'] = extractChild(element, ['h:RekNrBuitenland', 'RekNrBuitenland']);

        // Description
        attribute['Description'] = extractChild(element, ['h:NaamBankBuitenland', 'NaamBankBuitenland']);

        // Code
        attribute['Code'] = extractChild(element, ['h:CodeSoortRenteBuitenland', 'CodeSoortRenteBuitenland']);

        // Currency
        attribute['Currency'] = extractChild(element, ['h:LandcodeBuitenland', 'LandcodeBuitenland']);

        // add map to list
        attributes.add(attribute);
      }
    }

    // PremiedepotsBezit
    items.clear();
    items.addAll(findAllElements(doc, ['h:PremiedepotsBezit', 'PremiedepotsBezit'])!);

    if (items.isEmpty) {
      _logger.d('plugin-belastingdienst - Could not retrieve Assets, no elements named PremiedepotsBezit found');
    } else {
      for (var element in items) {
        var attribute = <String, dynamic>{};

        // Type
        attribute['Type'] = extractChild(element, ['h:LabelPremiedepot', 'LabelPremiedepot']);

        // AccountNumber
        attribute['AccountNumber'] = extractChild(element, ['h:PolisnrPremiedepot', 'PolisnrPremiedepot']);

        // Value
        attribute['Value'] = extractChild(element, ['h:WaardePremiedepot', 'WaardePremiedepot']);

        // add map to list
        attributes.add(attribute);
      }
    }

    // convert to string and return
    return jsonEncode(attributes);
  }

  // build income list string
  // Inkomsten Werk Woning
  static String getIncome(XmlDocument doc) {
    var attributes = <Map<String, dynamic>>[];

    List<XmlElement> items;
    items = <XmlElement>[];

    // InkTegenwArbeid
    items.addAll(findAllElements(doc, ['h:InkTegenwArbeid', 'InkTegenwArbeid'])!);

    // InkVrArbeid
    items.addAll(findAllElements(doc, ['h:InkVrArbeid', 'InkVrArbeid'])!);

    if (items.isEmpty) {
      _logger.d('plugin-belastingdienst - Could not retrieve Incomes, no elements named InkTegenwArbeid and InkVrArbeid found');
      return '';
    }

    for (var element in items) {
      var attribute = <String, dynamic>{};

      // Employer
      attribute['Employer'] = extractChild(element, ['h:NaamWerkgeverTegenwArbeid', 'NaamWerkgeverTegenwArbeid']);

      // Value
      attribute['Value'] = extractChild(element, ['h:LoonTegenwArbeid', 'LoonTegenwArbeid']);

      // SIVCode
      attribute['SIVCode'] = extractChild(element, ['h:SIVCode', 'SIVCode']);

      //TODO Type (calculation on SIVCode)

      // LBTCode
      attribute['LBTCode'] = extractChild(element, ['h:LBTCode', 'LBTCode']);

      // ZVWCode
      attribute['ZVWCode'] = extractChild(element, ['h:ZVWCode', 'ZVWCode']);

      // BijdrZVW
      attribute['BijdrZVW'] = extractChild(element, ['h:BijdrZVW', 'BijdrZVW']);

      // IngehoudenHeffingZVW
      attribute['IngehoudenHeffingZVW'] = extractChild(element, ['h:IngehoudenHeffingZVW', 'IngehoudenHeffingZVW']);

      // IngehoudenLHTegenwArbeid
      attribute['IngehoudenLHTegenwArbeid'] = extractChild(element, ['h:IngehoudenLHTegenwArbeid', 'IngehoudenLHTegenwArbeid']);

      // VerArbeidsKortingTegenwArbeid
      attribute['VerArbeidsKortingTegenwArbeid'] = extractChild(element, ['h:VerArbeidsKortingTegenwArbeid', 'VerArbeidsKortingTegenwArbeid']);

      // ToegLevenslKortTegenwArbeid
      attribute['ToegLevenslKortTegenwArbeid'] = extractChild(element, ['h:ToegLevenslKortTegenwArbeid', 'ToegLevenslKortTegenwArbeid']);

      // TravelAllowance
      attribute['TravelAllowance'] = extractChild(element, ['h:Reiskostenvergoeding', 'Reiskostenvergoeding']);

      // Year
      attribute['TravelAllowance'] = extractChild(element, ['h:Belastingjaar', 'Belastingjaar']);

      // add map to list
      attributes.add(attribute);
    }

    // convert to string and return
    return jsonEncode(attributes);
  }

  // build income list string
  // Inkomsten Werk Woning
  static String getRealEstate(XmlDocument doc) {
    var attributes = <Map<String, dynamic>>[];

    List<XmlElement> items = <XmlElement>[];

    // EWTweedeWoningOnroerendeZaken
    items.addAll(findAllElements(doc, ['h:EWTweedeWoningOnroerendeZaken', 'EWTweedeWoningOnroerendeZaken'])!);

    if (items.isEmpty) {
      _logger.d('plugin-belastingdienst - Could not retrieve Incomes, no elements named EWTweedeWoningOnroerendeZaken found');
      return '';
    }

    for (var element in items) {
      var attribute = <String, dynamic>{};

      // WOZValue
      attribute['WOZValue'] = extractChild(element, ['h:WOZWaarde', 'WOZWaarde']);

      // PostalCode
      attribute['PostalCode'] = getPostalCode(extractChild(element, ['h:WOZAdres', 'WOZAdres']));

      // HouseNumber
      attribute['HouseNumber'] = getHouseNumber(extractChild(element, ['h:WOZAdres', 'WOZAdres']));

      // Suffix
      attribute['Suffix'] = getHouseNumberSuffix(extractChild(element, ['h:WOZAdres', 'WOZAdres']));

      // OccupancyStartDate
      attribute['OccupancyStartDate'] = extractChild(element, ['h:BPerSituatieBewoning', 'BPerSituatieBewoning']);

      // OccupancyEndDate
      attribute['OccupancyEndDate'] = extractChild(element, ['h:EPerSituatieBewoning', 'EPerSituatieBewoning']);

      // TypeOfHabitation
      var situation = extractChild(element, ['h:SituatieBewoning', 'SituatieBewoning']);
      var qualification = extractChild(element, ['h:Voorkwalificatie', 'Voorkwalificatie']);

      attribute['TypeOfHabitation'] = situation;

      if (qualification == '01' && situation == '01') {
        attribute['Usage'] = 'MainResidence';
      } else if (qualification == '01' && situation == '02') {
        attribute['Usage'] = 'MainResidenceNotOccupied';
      } else if (qualification == '01' && situation == '03') {
        attribute['Usage'] = 'MainResidenceRented';
      } else if (qualification == '01' && situation == '04') {
        attribute['Usage'] = 'Undefined';
      } else if (qualification == '01' && situation == '05') {
        attribute['Usage'] = 'Undefined';
      } else if (qualification == '01' && situation == '07') {
        attribute['Usage'] = 'Undefined';
      } else if (qualification == '02') {
        attribute['Usage'] = 'NotOwnHome';
      } else if (qualification == '04') {
        attribute['Usage'] = 'NotOwnHome';
      }

      // PreQualification
      attribute['PreQualification'] = qualification;

      // add map to list
      attributes.add(attribute);
    }

    // convert to string and return
    return jsonEncode(attributes);
  }

  // build insurances list string
  static String getInsurances(XmlDocument doc) {
    var attributes = <Map<String, dynamic>>[];

    List<XmlElement> items = <XmlElement>[];

    // PremiesLijfrente
    items.addAll(findAllElements(doc, ['h:PremiesLijfrente', 'PremiesLijfrente'])!);

    if (items.isEmpty) {
      _logger.d('plugin-belastingdienst - Could not retrieve Incomes, no elements named PremiesLijfrente found');
      return '';
    }

    for (var element in items) {
      var attribute = <String, dynamic>{};

      // Supplier
      attribute['Supplier'] = extractChild(element, ['h:LabelLijfrente', 'LabelLijfrente']);

      // ContractNumber
      attribute['ContractNumber'] = extractChild(element, ['h:PolisnrRekNr', 'PolisnrRekNr']);

      // Payment
      attribute['Payment'] = extractChild(element, ['h:BetPremiesLijfrente', 'BetPremiesLijfrente']);

      // Type
      attribute['Type'] = extractChild(element, ['h:CodeRenseignementLijfrente', 'CodeRenseignementLijfrente']);

      // add map to list
      attributes.add(attribute);
    }

    // convert to string and return
    return jsonEncode(attributes);
  }

  static String getPostalCode(String input) {
    if (input.length < 6) {
      return '';
    }

    return input.substring(0, 6);
  }

  static String getHouseNumber(String input) {
    if (input.length < 6) {
      return '';
    }

    // when only housenumber, for 1234AB123, housenumber = 123
    if (!input.contains('-')) {
      return input.substring(6);
    }

    // when housenumber contains a suffix, input is like 1234AB123-a. Then suffix = a
    else {
      return input.substring(6, input.indexOf('-'));
    }
  }

  static String getHouseNumberSuffix(String input) {
    if (!input.contains('-')) {
      return '';
    }

    return input.substring(input.indexOf('-'));
  }

  // get initials part of from full name
  static String getInitials(String fullName) {
    List<String> byspace = fullName.split(" ");
    byspace.removeLast(); // remove last name
    return (byspace.join(". ")); // combine and add dots between the initials
  }

  // get lastname part of full name
  static String getLastName(String fullName) {
    List<String> byspace = fullName.split(" ");
    return byspace.last; // get last name
  }
}
