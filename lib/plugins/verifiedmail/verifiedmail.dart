import 'dart:async';
import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:schluss_beta_app/business/interface/plugin_base.dart';
import 'package:schluss_beta_app/plugins/verifiedmail/ui/intro_page.dart';

Function? storeSecureFunc;
Function? getSecureFunc;

class PluginVerifiedMail implements PluginBase {
  static late Function getEnvVar;
  static late Function storeInSecureStore;
  static late Function getFromSecureStore;

  final StreamController<int> _streamController = StreamController.broadcast();

  // run plugin
  @override
  void runPlugin(context, Function callBack, String siteUrl, String trigger, String pluginName) async {
    var locale = Localizations.localeOf(context); // Sets localization.

    await Navigator.push(
      context,
      MaterialPageRoute(
        builder: (context) => IntroPage(
          locale,
          callBack,
          pluginName: pluginName,
        ),
      ),
    );
  }

  /// Extracts data and sets into a map of attributes.
  @override
  Future<Map<String, dynamic>> getExtractedData(String pluginData) async {
    // set progress to 100%
    _streamController.add(100);

    return Future.value(jsonDecode(pluginData));
  }

  /// Gets the progress value of the process.
  Stream<int> getProgress() {
    return _streamController.stream; // Returns stream controller to the main app.
  }
}
