import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:logger/logger.dart';
import 'package:schluss_beta_app/constants/ui_constants.dart';
import 'package:schluss_beta_app/plugins/reused_widgets/closing_button.dart';
import 'package:schluss_beta_app/plugins/reused_widgets/main_button.dart';
import 'package:schluss_beta_app/plugins/verifiedmail/model/mail_validation_model.dart';
import 'package:schluss_beta_app/plugins/verifiedmail/utils/mail_validator.dart';
import 'package:schluss_beta_app/translations/i18n.dart';

BuildContext? ctx;
Logger logger = Logger();

enum ValidationState { input, inputValid, waiting, checkInbox, error }

class MailValidationPage extends StatefulWidget {
  final Locale locale;
  final Function callBack;
  final String? pluginName;

  const MailValidationPage(this.locale, this.callBack, {Key? key, this.pluginName}) : super(key: key);

  @override
  MailValidationPagePageState createState() => MailValidationPagePageState();
}

class MailValidationPagePageState extends State<MailValidationPage> {
  final textController = TextEditingController();
  ValidationState validationState = ValidationState.input;
  String? message;

  double? width;
  double? height;

  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    ctx = context;

    width = MediaQuery.of(context).size.width;
    height = MediaQuery.of(context).size.height;

    return Scaffold(
        resizeToAvoidBottomInset: true,
        body: SingleChildScrollView(
            child: SizedBox(
                height: height,
                width: width,
                child: Stack(
                  children: [
                    // content
                    Container(
                      height: height,
                      width: width,
                      color: UIConstants.primaryColor,
                      child: Padding(
                          padding: EdgeInsets.only(
                            left: width! * 0.07,
                            right: width! * 0.07,
                            bottom: 0,
                          ),
                          // The different content blocks depending on ValidationState
                          child: buildContentView()),
                    ),

                    // header
                    Stack(
                      children: <Widget>[
                        Align(
                            alignment: Alignment.topCenter,
                            child: Container(
                              height: height! * 0.1,
                              color: UIConstants.paleLilac,
                            )),
                        Padding(
                            padding: EdgeInsets.only(top: MediaQuery.of(context).padding.top),
                            child: Container(
                              decoration: const BoxDecoration(
                                color: Colors.white,
                                borderRadius: BorderRadius.only(
                                  topLeft: Radius.circular(20),
                                  topRight: Radius.circular(20),
                                ),
                              ),
                              height: height! * 0.1,
                              alignment: Alignment.center,
                              child: Stack(
                                children: <Widget>[
                                  // circle close button
                                  Padding(
                                    padding: EdgeInsets.only(right: height! * 0.02),
                                    child: const Align(
                                      alignment: Alignment.centerRight,
                                      child: ClosingButton(),
                                    ),
                                  ),
                                ],
                              ),
                            )),
                      ],
                    ),
                  ],
                ))));
  }

  @override
  void dispose() {
    textController.dispose();
    super.dispose();
  }

  // handle the mail validation and initiate the verification process
  void handleMailValidation() async {
    try {
      // only start the validation process when the email address is valid
      if (validationState != ValidationState.inputValid) {
        logger.i('plugin-verifiedmail: user tried to start validation with invalid mail address');
        return;
      }

      var mailAddress = textController.text;

      setState(() {
        validationState = ValidationState.waiting;
        message = I18n.of(context).verifiedMailValidatePrepare;
      });

      // initiate
      var model = MailValidationModel();

      var result = await model.getIdentity();

      if (result == null || result['did']['id'] == null) {
        logger.w('plugin-verifiedmail: createIdentity failed');

        setState(() {
          validationState = ValidationState.error;
          message = I18n.of(context).verifiedMailPrepareFailed;
        });
        return;
      }

      setState(() => message = '${I18n.of(context).verifiedMailValidateSendingMail} $mailAddress...');

      await Future.delayed(const Duration(seconds: 1)); // wait a little to show the message

      var requestId = await model.startRequest(result['did']['id'], mailAddress);

      if (requestId == '') {
        setState(() {
          validationState = ValidationState.error;
          message = I18n.of(context).verifiedMailValidateSendingMailFailed;
        });
      }

      setState(() {
        validationState = ValidationState.checkInbox;
        message = I18n.of(context).verifiedMailValidateMailSent;
      });

      // wait for user to click on the validation link
      var mailVc = await model.awaitRequest(requestId);

      widget.callBack(widget.pluginName, jsonEncode(mailVc), context);
    } catch (e) {
      setState(() {
        validationState = ValidationState.error;
        message = e.toString();
      });
    }
  }

  Widget buildContentView() {
    switch (validationState) {
      case ValidationState.input:
      case ValidationState.inputValid:
        return buildMailInputView();

      case ValidationState.waiting:
        return buildMailWaitingView();

      case ValidationState.checkInbox:
        return buildMailCheckInboxView();

      case ValidationState.error:
      default:
        return buildMailErrorView();
    }
  }

  // Block 1: Input e-mail address
  Widget buildMailInputView() {
    return Stack(children: [
      Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          Image.asset(
            'assets/images/email_verified.png',
            width: width! * 0.3,
          ),
          const SizedBox(height: 20),
          Text(
            I18n.of(context).verifiedMailValidateWhatIsYourMail,
            style: TextStyle(
              color: UIConstants.grayDark,
              fontSize: width! * 0.05,
              fontWeight: FontWeight.bold,
            ),
            textAlign: TextAlign.center,
          ),
          const SizedBox(height: 20),
          Padding(
            padding: const EdgeInsets.all(16.0),
            child: TextField(
              controller: textController,
              autofocus: true,
              keyboardType: TextInputType.emailAddress,
              // scrollPadding: const EdgeInsets.only(bottom:40),
              onChanged: (text) {
                validateMailAddress(text);
              },
            ),
          ),
        ],
      ),
      // Bottom button
      Column(mainAxisAlignment: MainAxisAlignment.end, children: [
        Padding(
          padding: EdgeInsets.only(
            right: width! * 0.075,
            left: width! * 0.075,
            bottom: height! * 0.05,
          ),
          child: Align(
            alignment: Alignment.bottomCenter,
            child: MainButton(
              I18n.of(context).verifiedMailValidateValidateYourMail,
              () => handleMailValidation(),

              backgroundColor: (validationState == ValidationState.inputValid) ? null : UIConstants.paleLilac,
              textColor: (validationState == ValidationState.inputValid) ? null : UIConstants.gray50, // .staleGrey,
            ),
          ),
        )
      ])
    ]);
  }

  void validateMailAddress(String text) {
    var isMailValid = MailValidator.validate(text);

    if (isMailValid) {
      setState(() => validationState = ValidationState.inputValid);
    }
    // when user changes textinput from a valid email to an invalid again, refresh back to 'input' state
    else if (!isMailValid && validationState == ValidationState.inputValid) {
      setState(() => validationState = ValidationState.input);
    }
  }

  // While waiting
  Widget buildMailWaitingView() {
    return Stack(children: [
      Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          const Center(
            child: SizedBox(
              width: 100,
              height: 100,
              child: CircularProgressIndicator(
                strokeWidth: 10,
                color: UIConstants.accentColor,
              ),
            ),
          ),
          const SizedBox(height: 40),
          Text(
            message ?? 'Please wait',
            style: TextStyle(
              color: UIConstants.gray50,
              fontSize: width! * 0.05,
              fontWeight: FontWeight.normal,
            ),
            textAlign: TextAlign.center,
          ),
        ],
      ),
    ]);
  }

  // Message to check your inbox
  Widget buildMailCheckInboxView() {
    return Stack(children: [
      Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          Center(
            child: Image.asset(
              'assets/images/new_message.png',
              width: width! * 0.3,
            ),
          ),
          const SizedBox(height: 40),
          Text(
            message!,
            style: TextStyle(
              color: UIConstants.grayDark,
              fontSize: width! * 0.05,
              fontWeight: FontWeight.bold,
            ),
          ),
          const SizedBox(height: 20),
          Text(
            I18n.of(context).verifiedMailValidateCheckMailLink,
            style: TextStyle(
              color: UIConstants.gray50,
              fontSize: width! * 0.05,
            ),
            textAlign: TextAlign.center,
          ),
        ],
      ),
    ]);
  }

  // Show an error
  Widget buildMailErrorView() {
    return Stack(children: [
      Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          Image.asset(
            'assets/images/error_icon.png',
            width: width! * 0.3,
          ),
          const SizedBox(height: 20),
          Center(
            child: Text(
              I18n.of(context).verifiedMailValidateGenericErrorTitle,
              style: TextStyle(
                color: UIConstants.grayDark,
                fontSize: width! * 0.05,
                fontWeight: FontWeight.bold,
              ),
              textAlign: TextAlign.center,
            ),
          ),
          const SizedBox(height: 20),
          Text(
            message!,
            style: TextStyle(
              color: UIConstants.gray50,
              fontSize: width! * 0.05,
            ),
            textAlign: TextAlign.left,
          ),
        ],
      ),
      // Bottom button
      Column(mainAxisAlignment: MainAxisAlignment.end, children: [
        Padding(
          padding: EdgeInsets.only(
            right: width! * 0.075,
            left: width! * 0.075,
            bottom: height! * 0.05,
          ),
          child: Align(
            alignment: Alignment.bottomCenter,
            child: MainButton(I18n.of(context).verifiedMailValidateStartOver, () => Navigator.pop(context)),
          ),
        )
      ])
    ]);
  }
}
