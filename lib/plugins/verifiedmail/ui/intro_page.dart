import 'package:flutter/material.dart';
import 'package:schluss_beta_app/constants/ui_constants.dart';
import 'package:schluss_beta_app/plugins/reused_widgets/backward_button.dart';
import 'package:schluss_beta_app/plugins/reused_widgets/closing_button.dart';
import 'package:schluss_beta_app/plugins/reused_widgets/main_button.dart';
import 'package:schluss_beta_app/plugins/verifiedmail/ui/mail_validation_page.dart';
import 'package:schluss_beta_app/plugins/verifiedmail/ui/reused_widgets/content_list.dart';
import 'package:schluss_beta_app/plugins/verifiedmail/utils/animated_route.dart';
import 'package:schluss_beta_app/translations/i18n.dart';

BuildContext? ctx;

class IntroPage extends StatelessWidget {
  final Locale locale;
  final Function callback;
  final String? pluginName;

  const IntroPage(this.locale, this.callback, {Key? key, this.pluginName}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    ctx = context;

    var width = MediaQuery.of(context).size.width;
    var height = MediaQuery.of(context).size.height;

    return Scaffold(
        backgroundColor: Colors.white,
        body: SizedBox(
          height: height,
          width: width,
          child: Stack(
            children: [
              // content
              Container(
                color: UIConstants.primaryColor,
                child: Padding(
                  padding: EdgeInsets.only(
                    top: height * 0.15,
                    left: width * 0.07,
                    right: width * 0.07,
                    bottom: 0,
                  ),
                  child: Column(
                    children: [
                      //SizedBox(height: 20),
                      Image.asset(
                        'assets/images/email_verified.png',
                        width: width * 0.4,
                      ),
                      const SizedBox(height: 20),
                      Text(
                        I18n.of(context).verifiedMailIntroTitle,
                        style: TextStyle(
                          color: UIConstants.grayDark,
                          fontSize: width * 0.05,
                          fontWeight: FontWeight.bold,
                        ),
                        textAlign: TextAlign.center,
                      ),
                      const SizedBox(height: 20),
                      Column(
                        mainAxisAlignment: MainAxisAlignment.center,
                        crossAxisAlignment: CrossAxisAlignment.center,
                        children: [
                          Text(
                            I18n.of(context).verifiedMailIntroDescription,
                            style: TextStyle(
                              color: UIConstants.gray100,
                              fontSize: width * 0.05,
                            ),
                            textAlign: TextAlign.left,
                          ),
                        ],
                      ),
                      const SizedBox(height: 20),
                      ContentList([
                        ContentListItem('1', I18n.of(context).verifiedMailIntroStep1),
                        ContentListItem('2', I18n.of(context).verifiedMailIntroStep2),
                        ContentListItem('3', I18n.of(context).verifiedMailIntroStep3),
                      ]),
                      const SizedBox(height: 20),
                    ],
                  ),
                ),
              ),

              /*Container(
                padding: EdgeInsets.only(
                  top: MediaQuery.of(context).padding.top + height * 0.1,
                ),
                height: height,
                width: width,
                color: UIConstants.primaryColor,
                child: 
              ),*/

              Column(mainAxisAlignment: MainAxisAlignment.end, children: [
                Padding(
                  padding: EdgeInsets.only(
                    right: width * 0.075,
                    left: width * 0.075,
                    bottom: height * 0.05,
                  ),
                  child: Align(
                    alignment: Alignment.bottomCenter,
                    child: MainButton(
                        I18n.of(context).verifiedMailIntroStart,
                        () async => {
                              await Navigator.push(
                                context,
                                AnimatedRoute(
                                  AnimationDirection.forward,
                                  builder: (context) => MailValidationPage(locale, callback, pluginName: pluginName),
                                ),
                              ),
                            }),
                  ),
                )
              ]),

              // header
              Stack(
                children: <Widget>[
                  Align(
                      alignment: Alignment.topCenter,
                      child: Container(
                        height: height * 0.1,
                        color: UIConstants.paleLilac,
                      )),
                  Padding(
                      padding: EdgeInsets.only(top: MediaQuery.of(context).padding.top),
                      child: Container(
                        decoration: const BoxDecoration(
                          color: Colors.white,
                          borderRadius: BorderRadius.only(
                            topLeft: Radius.circular(20),
                            topRight: Radius.circular(20),
                          ),
                        ),
                        height: height * 0.1,
                        alignment: Alignment.center,
                        child: Stack(
                          children: <Widget>[
                            // back arrow
                            Padding(
                              padding: EdgeInsets.only(left: height * 0.02),
                              child: Align(
                                alignment: Alignment.centerRight,
                                child: BackwardButton(
                                  locale: locale,
                                ),
                              ),
                            ),
                            // circle close button
                            Padding(
                              padding: EdgeInsets.only(right: height * 0.02),
                              child: const Align(
                                alignment: Alignment.centerRight,
                                child: ClosingButton(),
                              ),
                            ),
                          ],
                        ),
                      )),
                ],
              ),
            ],
          ),
        ));
  }
}
