/*
 * @author Manuja Jayawardana
 * @email mjayawardana@yukon.lk
 * @create date 2020-05-23
 */
class UrlConstants {
  /// Data requests.

  /// Tokens position after splitting "/".
  static const int dataRequestTokenIndex = 2;

  /// Settings url position after splitting "/".
  static const int dataRequestConfigUrlIndex = 3;

  /// Requests scope position after splitting "/".
  static const int dataRequestScopeIndex = 4;

  /// Data sharing.

  /// Data shares QR code location position after splitting "/".
  static const int dataShareQRLocationIndex = 6;

  /// Data shares QR code token position after splitting "/".
  static const int dataShareQRTokenIndex = 7;
}
