import 'package:flutter/material.dart';

class UIConstants {
  /// Fonts.
  static const defaultFontFamily = 'Walsheim';
  static const subFontFamily = 'Graphik';
  static const btnFontFamily = 'GTWalsheimMedium';

  /// Colors.
  static const accentColor = Color.fromRGBO(250, 74, 105, 1.0);
  static const watermelonDark = Color.fromRGBO(250, 74, 105, 0.3);
  static const watermelon = Color.fromRGBO(250, 74, 105, 0.64);
  static const rosyPinkDark = Color(0xa3fb637e);
  static const rosyPink = Color.fromRGBO(251, 99, 126, 1.0);
  static const rosyPinkLight = Color.fromRGBO(251, 123, 146, 1.0);
  static const primaryColor = Colors.white;
  static const transparent = Colors.transparent;
  static const black = Colors.black;
  static const blackFaded = Colors.black12;
  static const grayDarkest20 = Color.fromRGBO(33, 42, 61, 0.2);
  static const grayDarkest40 = Color.fromRGBO(33, 42, 61, 0.4);
  static const grayDarkest48 = Color.fromRGBO(33, 42, 61, 0.48);
  static const grayDarkest70 = Color.fromRGBO(33, 42, 61, 0.7);
  static const grayDarkest100 = Color.fromRGBO(33, 42, 61, 1.0);
  static const grayDark = Color.fromRGBO(49, 48, 49, 1.0);
  static const grayShadowDark = Color(0xFF363636);
  static const grayShadow = Color(0xFF737373);
  static const bottomSheetGrayShadow = Color.fromRGBO(43, 45, 53, 0.5);
  static const slateGray40 = Color.fromRGBO(88, 96, 113, 0.4);
  static const slateGray90 = Color.fromRGBO(88, 96, 113, 0.9);
  static const slateGray100 = Color.fromRGBO(88, 96, 113, 1.0);
  static const gray50 = Color.fromRGBO(124, 133, 154, 0.5);
  static const gray100 = Color.fromRGBO(124, 133, 154, 1.0);
  static const paleLilac = Color.fromRGBO(226, 225, 233, 1.0);
  static const grayLight = Color.fromRGBO(239, 239, 241, 1.0);
  static const grayLighter = Color(0xffefeff1);
  static const grayLightest = Color.fromRGBO(240, 240, 245, 1.0);
  static const paleGray = Color.fromRGBO(247, 247, 249, 1.0);
  static const transparentWhite = Color.fromRGBO(249, 249, 249, 0.78);
  static const transparentDark = Color.fromRGBO(43, 45, 53, 0.6);
  static const greenDark = Color.fromRGBO(40, 129, 112, 1.0);
  static const greenBlue = Color(0xff02b073);
  static const greenLight = Color.fromRGBO(0, 242, 157, 1.0);
  static const greenLightest = Color.fromRGBO(229, 247, 241, 1.0);
  static const red = Color.fromRGBO(229, 50, 50, 1.0);
  static const orange = Color.fromRGBO(255, 140, 0, 1.0);
  static const yellow = Color.fromRGBO(255, 182, 0, 1.0);
  static const green = Color.fromRGBO(0, 184, 119, 1.0);
  static const blue = Color.fromRGBO(42, 147, 238, 1.0);
  static const turquoise = Color.fromRGBO(5, 159, 180, 1.0);
  static const purple = Color.fromRGBO(112, 79, 239, 1.0);
  static const pastelBlue = Color.fromRGBO(224, 241, 255, 1.0);
  static const pastelGreen = Color.fromRGBO(215, 247, 237, 1.0);
  static const pastelTurquoise = Color.fromRGBO(216, 245, 246, 1.0);
  static const pastelYellow = Color.fromRGBO(255, 248, 224, 1.0);
  static const pastelOrange = Color.fromRGBO(255, 239, 224, 1.0);
  static const pastelPink = Color.fromRGBO(255, 224, 230, 1.0);
  static const pastelPurple = Color.fromRGBO(231, 224, 255, 1.0);
  static const pastelGrey = Color.fromRGBO(225, 226, 233, 1.0);

  /// Paddings.
  static const minPadding = 4.0;
}
