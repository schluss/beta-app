//TODO : Update status codes with a list if multiple response codes available
class HttpResponseCodeConstants {
  static const responseOk = [200, 201];
  static const responseBadRequest = 400;
  static const responseUnauthorized = 401;
  static const responseForbidden = 403;
  static const responseNotFound = 404;
  static const responseNotAcceptable = 406;
  static const responseInternalServerError = 500;
  static const responseNotImplemented = 501;
  static const responseBadGateway = 502;
  static const responseServiceUnavailable = 503;
  static const responseGatewayTimeout = 504;
  static const responseHttpVersionNotSupported = 505;
}
