import 'package:flutter/material.dart';
import 'package:schluss_beta_app/constants/ui_constants.dart';

final appTheme = ThemeData(
  primaryColor: UIConstants.primaryColor,
  fontFamily: UIConstants.defaultFontFamily,
  textTheme: const TextTheme(
    titleLarge: TextStyle(
      fontFamily: UIConstants.defaultFontFamily,
      fontWeight: FontWeight.w700,
      fontSize: 24,
      color: UIConstants.black,
    ),
  ),
  colorScheme: ColorScheme.fromSwatch().copyWith(
    secondary: UIConstants.accentColor,
  ),
);

const mainBorderRadius = BorderRadius.only(
  topLeft: Radius.circular(20),
  topRight: Radius.circular(20),
);
