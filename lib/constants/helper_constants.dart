import 'package:flutter/material.dart';

class HelperConstant {
  /// Maps key for grouping time-related data. Key is used in i18n.dart file to translate.
  static const String groupTodayMapKey = 'groupNameToday';

  /// Maps keys for grouping time related data. Key is used in i18n.dart file to translate.
  static const String groupYesterdayMapKey = 'groupNameYesterday';

  /// Maps key for grouping time related data. Key is used in i18n.dart file to translate.
  static const String groupLastWeekMapKey = 'groupNameLastWeek';

  /// Maps key for grouping time related data. Key is used in i18n.dart file to translate.
  static const String groupLastMonthMapKey = 'groupNameLastMonth';

  /// Maps key for grouping time related data. Key is used in i18n.dart file to translate.
  static const String groupMonthAgoMapKey = 'groupNameMonthAgo';

  /// Maps key for grouping time related data. Key is used in i18n.dart file to translate.
  static const String groupYearAgoMapKey = 'groupNameYearAgo';

  /// Maps key for category name -gegevens for local files uploaded. Key is used in i18n.dart file to translate.
  static const String dataTextMapKey = 'dataText';

  /// Numbers of seconds before the session expires when there is no interaction with the app.
  static const int focusInactiveTimeSec = 300;
  static const String fromContractUI = 'Contract';
  static const String fromDataUI = 'Data';
  static const String fromHomeUI = 'Home';

  /// Rights to left animation.
  static const Offset rightDirection = Offset(1, 0);

  /// Lefts to right animation.
  static const Offset leftDirection = Offset(-1, 0);

  /// Bottoms to up navigation.
  static const Offset bottomToTopDirection = Offset(0, 1);

  /// Tops to bottom animation.
  static const Offset topToBottomDirection = Offset(0, -1);
}
