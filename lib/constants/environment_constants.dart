/// Constants to be able to easy specification and handling the app environment mode.
class EnvironmentConstants {
  /// Whether the apps is in [development] mode.
  static const String development = 'development';

  /// Whether the apps is in [production] mode.
  static const String production = 'production';

  /// Casts to constant from a given string. When invalid input is given,
  /// the default constant of [development] is returned.
  static String fromString(String? toCastFrom) {
    switch (toCastFrom) {
      case production:
        return production;
      case development:
      default:
        return development;
    }
  }
}
