/*
 * @author Manuja Jayawardana
 * @email mjayawardana@yukon.lk
 * @create date 2020-07-07
 */

/// Data models for [LoginAttempt] table.
class LoginAttempt {
  static const String _storeName = 'LoginAttempt';

  int? id;
  int loginAttemptCount;
  int? loginAttemptTimestamp;

  static String getStoreName() => _storeName;

  LoginAttempt({
    this.id,
    this.loginAttemptCount = 0,
    this.loginAttemptTimestamp,
  });

  /// Converts [AppDataModel] to a JSON/Map.
  Map<String, dynamic> toJson() => {
        'id': id,
        'loginAttemptCount': loginAttemptCount,
        'loginAttemptTimestamp': loginAttemptTimestamp,
      };

  /// Generates [AppDataModel] from a JSON/Map.
  factory LoginAttempt.fromJson(Map<String, dynamic> json) {
    return LoginAttempt(
      id: json['id'],
      loginAttemptCount: json['loginAttemptCount'],
      loginAttemptTimestamp: json['loginAttemptTimestamp'],
    );
  }
}
