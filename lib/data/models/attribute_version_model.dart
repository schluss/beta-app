import 'package:json_annotation/json_annotation.dart';

part 'attribute_version_model.g.dart';

/// Data models for [AttributeVersion] table.
@JsonSerializable()
class AttributeVersionModel {
  static const String _storeName = 'AttributeVersion';

  final int? id;
  final int? attributeId;
  final int? version;

  /// MillisecondsSinceEpoch.
  final int? timestamp;

  /// The paths/filenames where the attribute value is stored on disk.
  final String? localPath;

  /// When also stores online, this contains the path.
  final String? onlineStoragePath;

  /// When stores online, this contains the provider used for storing online.
  final String? onlineStorageProvider;

  static String getStoreName() => _storeName;

  AttributeVersionModel({
    this.id,
    this.attributeId,
    this.version,
    this.timestamp,
    this.localPath,
    this.onlineStoragePath,
    this.onlineStorageProvider,
  });

  factory AttributeVersionModel.fromJson(Map<String, dynamic> json) => _$AttributeVersionModelFromJson(
        json,
      );

  Map<String, dynamic> toJson() => _$AttributeVersionModelToJson(this);
}
