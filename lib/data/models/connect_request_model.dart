/// Connects request related data model.
///
/// This models is for the [ConnectRequest] table in the database.
class ConnectRequestModel {
  static const String _storeName = 'ConnectRequest';

  final int? id;
  final String? token;
  final String? objectId;
  final String? location;
  final int? timestamp;

  static String getStoreName() => _storeName;

  ConnectRequestModel({
    this.id,
    this.token,
    this.objectId,
    this.location,
    this.timestamp,
  });

  /// Converts [ConnectRequestModel] to a JSON/Map.
  Map<String, dynamic> toJson() => {
        'id': id,
        'token': token,
        'objectId': objectId,
        'location': location,
        'timestamp': timestamp,
      };

  /// Generates [ConnectRequestModel] from a JSON/Map.
  factory ConnectRequestModel.fromJson(Map<String, dynamic> json) {
    return ConnectRequestModel(
      id: json['id'],
      token: json['token'],
      objectId: json['objectId'],
      location: json['location'],
      timestamp: json['timestamp'],
    );
  }
}
