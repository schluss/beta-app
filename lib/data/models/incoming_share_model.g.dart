// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'incoming_share_model.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

IncomingShareModel _$IncomingShareModelFromJson(Map<String, dynamic> json) =>
    IncomingShareModel(
      id: json['id'] as int?,
      contractId: json['contractId'] as int?,
      category: json['category'] as String?,
      group: json['group'] as String?,
      name: json['name'] as String?,
      label: json['label'] as String?,
      logoUrl: json['logoUrl'] as String?,
      displayValue: json['displayValue'] as String?,
      providerName: json['providerName'] as String?,
      type: json['type'] as String?,
      mediaType: json['mediaType'] as String?,
      gatewayShareId: json['gatewayShareId'] as String?,
      gatewayPath: json['gatewayPath'] as String?,
      gatewayAuthToken: json['gatewayAuthToken'] as String?,
      gatewayAttributeKey: json['gatewayAttributeKey'] as String?,
      version: json['version'] as int?,
      timestamp: json['timestamp'] as int?,
      attrKeyEnc: json['attrKeyEnc'] as String?,
      localPath: json['localPath'] as String?,
      isVC: json['isVC'] as bool?,
      isHideValue: json['isHideValue'] as bool? ?? false,
    );

Map<String, dynamic> _$IncomingShareModelToJson(IncomingShareModel instance) =>
    <String, dynamic>{
      'id': instance.id,
      'contractId': instance.contractId,
      'category': instance.category,
      'group': instance.group,
      'name': instance.name,
      'label': instance.label,
      'logoUrl': instance.logoUrl,
      'type': instance.type,
      'version': instance.version,
      'displayValue': instance.displayValue,
      'providerName': instance.providerName,
      'mediaType': instance.mediaType,
      'gatewayShareId': instance.gatewayShareId,
      'gatewayPath': instance.gatewayPath,
      'gatewayAuthToken': instance.gatewayAuthToken,
      'gatewayAttributeKey': instance.gatewayAttributeKey,
      'attrKeyEnc': instance.attrKeyEnc,
      'localPath': instance.localPath,
      'isVC': instance.isVC,
      'timestamp': instance.timestamp,
      'isHideValue': instance.isHideValue,
    };
