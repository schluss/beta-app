// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'task_model.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

TaskModel _$TaskModelFromJson(Map<String, dynamic> json) => TaskModel(
      name: json['name'] as String?,
      params: json['params'] as String?,
      status: $enumDecodeNullable(_$TaskStatusEnumMap, json['status']) ??
          TaskStatus.pending,
    )
      ..id = json['id'] as int?
      ..retryCount = json['retryCount'] as int
      ..created = json['created'] as int?;

Map<String, dynamic> _$TaskModelToJson(TaskModel instance) => <String, dynamic>{
      'id': instance.id,
      'name': instance.name,
      'params': instance.params,
      'status': _$TaskStatusEnumMap[instance.status]!,
      'retryCount': instance.retryCount,
      'created': instance.created,
    };

const _$TaskStatusEnumMap = {
  TaskStatus.pending: 'pending',
  TaskStatus.executing: 'executing',
  TaskStatus.retry: 'retry',
  TaskStatus.failed: 'failed',
};
