import 'package:json_annotation/json_annotation.dart';

part 'task_model.g.dart';

/// Status of a task.
enum TaskStatus {
  /// The tasks is waiting to be executed.
  @JsonValue('pending')
  pending,

  /// Tasks is currently being executed.
  @JsonValue('executing')
  executing,

  /// The tasks failed, but is going to be tried again.
  @JsonValue('retry')
  retry,

  /// The tasks failed permanently.
  @JsonValue('failed')
  failed
}

/// Data models for the task table.
@JsonSerializable()
class TaskModel {
  static const String _storeName = 'Task';

  int? id;
  final String? name;
  final String? params;
  TaskStatus status;
  int retryCount = 0;

  /// MillisecondsSinceEpoch.
  int? created;

  static String getStoreName() => _storeName;

  TaskModel({
    this.name,
    this.params,
    this.status = TaskStatus.pending,
  });

  factory TaskModel.fromJson(
    Map<String, dynamic> json,
  ) =>
      _$TaskModelFromJson(
        json,
      );

  Map<String, dynamic> toJson() => _$TaskModelToJson(this);
}
