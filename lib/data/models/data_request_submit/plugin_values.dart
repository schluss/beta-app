import 'package:json_annotation/json_annotation.dart';

part 'plugin_values.g.dart';

/// Reals values.
@JsonSerializable()
class PluginValues {
  String? name;
  String? value;

  PluginValues({this.name, this.value});

  @override
  String toString() {
    return 'PluginValues{name: $name, value: $value}';
  }

  factory PluginValues.fromJson(Map<String, dynamic> json) => _$PluginValuesFromJson(json);
  Map<String, dynamic> toJson() => _$PluginValuesToJson(this);
  @override
  bool operator ==(Object other) => identical(this, other) || other is PluginValues && runtimeType == other.runtimeType && name == other.name && value == other.value;

  @override
  int get hashCode => name.hashCode ^ value.hashCode;
}
