import 'package:json_annotation/json_annotation.dart';
import 'package:schluss_beta_app/data/models/data_request_submit/contract_plugin.dart';

part 'contract_package.g.dart';

/// Roots package for sending payload.
@JsonSerializable()
class ContractPackage {
  String? requester;

  /// Schluss.
  String? provider;
  String? scope;

  /// [ISO 8601].
  String? date;
  List<ContractPlugin>? shares;

  ContractPackage({
    this.requester,
    this.provider,
    this.scope,
    this.date,
    this.shares,
  });
  factory ContractPackage.fromJson(Map<String, dynamic> json) => _$ContractPackageFromJson(
        json,
      );
  Map<String, dynamic> toJson() => _$ContractPackageToJson(this);
  @override
  String toString() {
    return 'ContractPackage{requester: $requester, provider: $provider, scope: $scope, date: $date, shares: $shares}';
  }

  @override
  bool operator ==(Object other) =>
      identical(
        this,
        other,
      ) ||
      other is ContractPackage && runtimeType == other.runtimeType && requester == other.requester && provider == other.provider && scope == other.scope && date == other.date && shares == other.shares;

  @override
  int get hashCode => requester.hashCode ^ provider.hashCode ^ scope.hashCode ^ date.hashCode ^ shares.hashCode;
}
