// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'contract_package.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

ContractPackage _$ContractPackageFromJson(Map<String, dynamic> json) =>
    ContractPackage(
      requester: json['requester'] as String?,
      provider: json['provider'] as String?,
      scope: json['scope'] as String?,
      date: json['date'] as String?,
      shares: (json['shares'] as List<dynamic>?)
          ?.map((e) => ContractPlugin.fromJson(e as Map<String, dynamic>))
          .toList(),
    );

Map<String, dynamic> _$ContractPackageToJson(ContractPackage instance) =>
    <String, dynamic>{
      'requester': instance.requester,
      'provider': instance.provider,
      'scope': instance.scope,
      'date': instance.date,
      'shares': instance.shares,
    };
