// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'plugin_values.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

PluginValues _$PluginValuesFromJson(Map<String, dynamic> json) => PluginValues(
      name: json['name'] as String?,
      value: json['value'] as String?,
    );

Map<String, dynamic> _$PluginValuesToJson(PluginValues instance) =>
    <String, dynamic>{
      'name': instance.name,
      'value': instance.value,
    };
