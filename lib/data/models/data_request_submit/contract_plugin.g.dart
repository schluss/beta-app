// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'contract_plugin.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

ContractPlugin _$ContractPluginFromJson(Map<String, dynamic> json) =>
    ContractPlugin(
      plugin: json['plugin'] as String?,
      attributes: (json['attributes'] as List<dynamic>?)
          ?.map((e) => PluginValues.fromJson(e as Map<String, dynamic>))
          .toList(),
    );

Map<String, dynamic> _$ContractPluginToJson(ContractPlugin instance) =>
    <String, dynamic>{
      'plugin': instance.plugin,
      'attributes': instance.attributes,
    };
