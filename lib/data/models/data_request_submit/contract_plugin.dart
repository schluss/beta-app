import 'package:json_annotation/json_annotation.dart';
import 'package:schluss_beta_app/data/models/data_request_submit/plugin_values.dart';

part 'contract_plugin.g.dart';

/// Plugins information.
@JsonSerializable()
class ContractPlugin {
  String? plugin;
  List<PluginValues>? attributes;

  ContractPlugin({this.plugin, this.attributes});

  factory ContractPlugin.fromJson(Map<String, dynamic> json) => _$ContractPluginFromJson(json);
  Map<String, dynamic> toJson() => _$ContractPluginToJson(this);

  @override
  String toString() {
    return 'ContractPlugin{pluginName: $plugin, shares: $attributes}';
  }

  @override
  bool operator ==(
    Object other,
  ) =>
      identical(
        this,
        other,
      ) ||
      other is ContractPlugin && runtimeType == other.runtimeType && plugin == other.plugin && attributes == other.attributes;

  @override
  int get hashCode => plugin.hashCode ^ attributes.hashCode;
}
