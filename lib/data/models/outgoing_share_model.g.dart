// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'outgoing_share_model.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

OutgoingShareModel _$OutgoingShareModelFromJson(Map<String, dynamic> json) =>
    OutgoingShareModel(
      id: json['id'] as int?,
      contractId: json['contractId'] as int?,
      attributeId: json['attributeId'] as int?,
      timestamp: json['timestamp'] as int?,
      gatewayShareId: json['gatewayShareId'] as String?,
    );

Map<String, dynamic> _$OutgoingShareModelToJson(OutgoingShareModel instance) =>
    <String, dynamic>{
      'id': instance.id,
      'contractId': instance.contractId,
      'attributeId': instance.attributeId,
      'timestamp': instance.timestamp,
      'gatewayShareId': instance.gatewayShareId,
    };
