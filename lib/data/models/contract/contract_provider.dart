/*
 * @author Asanka Anthony
 * @email aanthony@yukon.lk
 * @create date 2020-06-22 15:39:29
 * @modify date 2020-06-22 15:39:29
 */
import 'package:json_annotation/json_annotation.dart';
import 'package:schluss_beta_app/data/models/attribute_version_model.dart';

part 'contract_provider.g.dart';

@JsonSerializable()
class ContractProvider {
  /// The name of the provider that provided the data.
  String? provider;

  /// Providers config.
  String? configUrl;
  String? logoUrl;
  String? request;

  // For keeping track if the attributes are from the current user, or another user (and therefore from the IncomingShares table instead of the Attributes tabel)
  int connectionId;

  /// Actual encrypted data.
  List<AttributeVersionModel>? attributes;

  ContractProvider({
    this.connectionId = 0,
    this.provider,
    this.configUrl,
    this.logoUrl,
    this.request,
    this.attributes,
  });

  factory ContractProvider.fromJson(Map<String, dynamic> json) => _$ContractProviderFromJson(
        json,
      );

  Map<String, dynamic> toJson() => _$ContractProviderToJson(this);

  @override
  String toString() {
    return 'ContractProvider{provider: $provider, config: $configUrl, logoURL: $logoUrl, name: $request, attributes: $attributes,}';
  }
}
