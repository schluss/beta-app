// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'contract_requester.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

Requester _$RequesterFromJson(Map<String, dynamic> json) => Requester(
      name: json['name'] as String?,
      logoUrl: json['logoUrl'] as String?,
      configUrl: json['configUrl'] as String?,
      configVersion: json['configVersion'] as String?,
    );

Map<String, dynamic> _$RequesterToJson(Requester instance) => <String, dynamic>{
      'name': instance.name,
      'logoUrl': instance.logoUrl,
      'configUrl': instance.configUrl,
      'configVersion': instance.configVersion,
    };
