import 'package:json_annotation/json_annotation.dart';
import 'package:schluss_beta_app/data/models/contract/contract_provider.dart';
import 'package:schluss_beta_app/data/models/contract/contract_requester.dart';
import 'package:schluss_beta_app/ui/dashboard/stewardship/owner_define_access_timing_page_content.dart';

part 'contract_model.g.dart';

/// Types of connection.
enum ConnectionType {
  @JsonValue('user')
  user,

  @JsonValue('org')
  organization
}

/// Defines stewardship connection types.
enum StewardshipType {
  // Owner: The person who created the stewardship connection and appointed
  // someone to be their steward. Notification of steward's connection in
  // owner's vault is represents by PINK color.
  @JsonValue('sender')
  sender,

  // Steward: the person who gets access to the shared data of the owner after a
  // certain trigger moment. Notification of owner's connection in steward's
  // vault is represents by BLUE color.
  @JsonValue('receiver')
  receiver,
}

/// Representations of a Connection.
@JsonSerializable(explicitToJson: true)
class ContractModel {
  static const String _storeName = 'contract';

  /// The internals generated identifier.
  int? id;

  /// The globals used identifier, format: [UUIDv4].
  String? contractId; //uuid v4.

  /// Scopes of the contract, in case of a v1 data request.
  String? scope;

  /// Creations date.
  int? date;

  String? version;

  String? iconColor;

  String? description;

  /// With a V1 data requests, these are the shared attributes with the organization.
  List<ContractProvider>? shares;

  /// Details about the connected person or organization.
  Requester? requester;

  /// [true] - show notifications / [false] - hide notifications.
  bool? notification = true;

  /// The clients type, can be 'user' when it is another Schluss user,
  /// or 'org' when it's an organization.
  ConnectionType? type;

  /// When it's a specials king of connection, this contains that specific type.
  StewardshipType? stewardshipType;

  /// When the releases trigger has been fired, this will become true.
  bool? isReleased = false;

  /// The uniques gateway identifier to be able to reach the client.
  String? clientKey;

  /// The clients' public key.
  String? clientPublicKey;

  /// The gateways URL of the client.
  String? gatewayUrl;

  /// Shares data accessing type ( Immediately / After Death)
  DataAccessType? dataAccessType;

  /// Trigger Key from Stewardship process
  String? triggerKey;

  static String getStoreName() => _storeName;
  ContractModel({
    this.id,
    this.scope,
    this.date,
    this.version,
    this.iconColor,
    this.shares,
    this.requester,
    this.contractId,
    this.description,
    this.notification,
    this.type,
    this.stewardshipType,
    this.isReleased,
    this.clientKey,
    this.clientPublicKey,
    this.gatewayUrl,
    this.dataAccessType,
    this.triggerKey,
  });

  factory ContractModel.fromJson(
    Map<String, dynamic> json,
  ) =>
      _$ContractModelFromJson(
        json,
      );

  Map<String, dynamic> toJson() => _$ContractModelToJson(this);
}
