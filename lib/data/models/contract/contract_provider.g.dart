// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'contract_provider.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

ContractProvider _$ContractProviderFromJson(Map<String, dynamic> json) =>
    ContractProvider(
      connectionId: json['connectionId'] as int? ?? 0,
      provider: json['provider'] as String?,
      configUrl: json['configUrl'] as String?,
      logoUrl: json['logoUrl'] as String?,
      request: json['request'] as String?,
      attributes: (json['attributes'] as List<dynamic>?)
          ?.map(
              (e) => AttributeVersionModel.fromJson(e as Map<String, dynamic>))
          .toList(),
    );

Map<String, dynamic> _$ContractProviderToJson(ContractProvider instance) =>
    <String, dynamic>{
      'provider': instance.provider,
      'configUrl': instance.configUrl,
      'logoUrl': instance.logoUrl,
      'request': instance.request,
      'connectionId': instance.connectionId,
      'attributes': instance.attributes,
    };
