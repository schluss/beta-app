// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'contract_model.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

ContractModel _$ContractModelFromJson(Map<String, dynamic> json) =>
    ContractModel(
      id: json['id'] as int?,
      scope: json['scope'] as String?,
      date: json['date'] as int?,
      version: json['version'] as String?,
      iconColor: json['iconColor'] as String?,
      shares: (json['shares'] as List<dynamic>?)
          ?.map((e) => ContractProvider.fromJson(e as Map<String, dynamic>))
          .toList(),
      requester: json['requester'] == null
          ? null
          : Requester.fromJson(json['requester'] as Map<String, dynamic>),
      contractId: json['contractId'] as String?,
      description: json['description'] as String?,
      notification: json['notification'] as bool?,
      type: $enumDecodeNullable(_$ConnectionTypeEnumMap, json['type']),
      stewardshipType: $enumDecodeNullable(
          _$StewardshipTypeEnumMap, json['stewardshipType']),
      isReleased: json['isReleased'] as bool?,
      clientKey: json['clientKey'] as String?,
      clientPublicKey: json['clientPublicKey'] as String?,
      gatewayUrl: json['gatewayUrl'] as String?,
      dataAccessType:
          $enumDecodeNullable(_$DataAccessTypeEnumMap, json['dataAccessType']),
      triggerKey: json['triggerKey'] as String?,
    );

Map<String, dynamic> _$ContractModelToJson(ContractModel instance) =>
    <String, dynamic>{
      'id': instance.id,
      'contractId': instance.contractId,
      'scope': instance.scope,
      'date': instance.date,
      'version': instance.version,
      'iconColor': instance.iconColor,
      'description': instance.description,
      'shares': instance.shares?.map((e) => e.toJson()).toList(),
      'requester': instance.requester?.toJson(),
      'notification': instance.notification,
      'type': _$ConnectionTypeEnumMap[instance.type],
      'stewardshipType': _$StewardshipTypeEnumMap[instance.stewardshipType],
      'isReleased': instance.isReleased,
      'clientKey': instance.clientKey,
      'clientPublicKey': instance.clientPublicKey,
      'gatewayUrl': instance.gatewayUrl,
      'dataAccessType': _$DataAccessTypeEnumMap[instance.dataAccessType],
      'triggerKey': instance.triggerKey,
    };

const _$ConnectionTypeEnumMap = {
  ConnectionType.user: 'user',
  ConnectionType.organization: 'org',
};

const _$StewardshipTypeEnumMap = {
  StewardshipType.sender: 'sender',
  StewardshipType.receiver: 'receiver',
};

const _$DataAccessTypeEnumMap = {
  DataAccessType.immediately: 'immediately',
  DataAccessType.afterPassed: 'afterPassed',
};
