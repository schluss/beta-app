/*
 * @author Asanka Anthony
 * @email aanthony@yukon.lk
 * @create date 2020-06-25 12:11:08
 * @modify date 2020-06-25 12:11:08
 */

import 'package:json_annotation/json_annotation.dart';

part 'contract_requester.g.dart';

@JsonSerializable()
class Requester {
  String? name;
  String? logoUrl;
  String? configUrl;
  String? configVersion;

  Requester({this.name, this.logoUrl, this.configUrl, this.configVersion});
  factory Requester.fromJson(Map<String, dynamic> json) => _$RequesterFromJson(
        json,
      );

  Map<String, dynamic> toJson() => _$RequesterToJson(this);
}
