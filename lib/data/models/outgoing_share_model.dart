import 'package:json_annotation/json_annotation.dart';

part 'outgoing_share_model.g.dart';

/// Data models for [OutgoingShareModel] table.
@JsonSerializable()
class OutgoingShareModel {
  static const String _storeName = 'OutgoingShare';

  final int? id;
  final int? contractId;
  final int? attributeId;

  /// MillisecondsSinceEpoch.
  int? timestamp;

  /// UUIDv4.
  final String? gatewayShareId;

  static String getStoreName() => _storeName;

  OutgoingShareModel({
    this.id,
    this.contractId,
    this.attributeId,
    this.timestamp,
    this.gatewayShareId,
  });

  factory OutgoingShareModel.fromJson(
    Map<String, dynamic> json,
  ) =>
      _$OutgoingShareModelFromJson(
        json,
      );

  Map<String, dynamic> toJson() => _$OutgoingShareModelToJson(this);
}
