/*
 * @author Manuja Jayawardana
 * @email mjayawardana@yukon.lk
 * @create date 2020-05-23
 */
import 'package:schluss_beta_app/view_model/request_view_model.dart';

class RequestDataModel {
  static const String _storeName = 'requests';

  final int? id;
  final int connectionId;
  final String? name;
  final String? logoUrl;
  final String? importName;
  final String? importDescription;
  final String? token;
  final String? scope;
  final String? settingsUrl;

  /// P - Pending , C - Complete.
  final String? status;
  final String? label;

  /// MillisecondsSinceEpoch.
  final int? requestDate;

  /// Completed providers (Index table Id) list in table.
  String? providerIdList;

  /// Apps config.json raw data (Json encoded).
  final String? appConfigRawData;

  /// Providers config.json raw data list (Json encoded).
  final List<dynamic>? providerConfigRawData;
  static String getStoreName() => _storeName;

  RequestDataModel(
    this.connectionId, {
    this.id,
    this.name,
    this.logoUrl,
    this.importName,
    this.importDescription,
    this.token,
    this.scope,
    this.settingsUrl,
    this.status,
    this.label,
    this.requestDate,
    this.providerIdList,
    this.appConfigRawData,
    this.providerConfigRawData,
  });

  /// Converts [RequestViewModel] to JSON/Map.
  Map<String, dynamic> toJson() => {
        'id': id,
        'connectionId': connectionId,
        'name': name,
        'logoUrl': logoUrl,
        'importName': importName,
        'importDescription': importDescription,
        'token': token,
        'scope': scope,
        'settings_url': settingsUrl,
        'status': status,
        'label': label,
        'request_date': requestDate,
        'providers': providerIdList,
        'appConfigRawData': appConfigRawData,
        'providerConfigRawData': providerConfigRawData,
      };

  /// Generates [RequestDataModel] from [RequestViewModel].
  factory RequestDataModel.fromViewModel(int connectionId, RequestViewModel model) {
    return RequestDataModel(
      connectionId,
      id: model.id,
      name: model.name,
      logoUrl: model.logoUrl,
      importName: model.importName,
      importDescription: model.importDescription,
      token: model.token,
      scope: model.scope,
      settingsUrl: model.settingsUrl,
      status: model.status,
      label: model.label,
      requestDate: model.requestDate,
      appConfigRawData: model.appConfigRawData,
      providerConfigRawData: model.providerConfigRawData,
    );
  }
}
