/*
 * @author Manuja Jayawardana
 * @email mjayawardana@yukon.lk
 * @create date 2020-07-07
 */

/// Applications related data model.
///
/// This models is for the [APP] table in the database.
class AppDataModel {
  static const String _storeName = 'APP';

  final int? id;
  final String? publicKey;

  /// MillisecondsSinceEpoch.
  final int? timestamp;
  final int? loginAttemptCount;
  final int? loginAttemptTimestamp;

  static String getStoreName() => _storeName;

  AppDataModel({
    this.id,
    this.publicKey,
    this.timestamp,
    this.loginAttemptCount,
    this.loginAttemptTimestamp,
  });

  /// Converts [AppDataModel] to a JSON/Map.
  Map<String, dynamic> toJson() => {
        'id': id,
        'publicKey': publicKey,
        'timestamp': timestamp,
        'loginAttemptCount': loginAttemptCount,
        'loginAttemptTimestamp': loginAttemptTimestamp,
      };

  /// Generates [AppDataModel] from a JSON/Map.
  factory AppDataModel.fromJson(Map<String, dynamic> json) {
    return AppDataModel(
      id: json['id'],
      publicKey: json['publicKey'],
      timestamp: json['timestamp'],
      loginAttemptCount: json['loginAttemptCount'],
      loginAttemptTimestamp: json['loginAttemptTimestamp'],
    );
  }
}
