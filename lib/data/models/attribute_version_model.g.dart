// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'attribute_version_model.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

AttributeVersionModel _$AttributeVersionModelFromJson(
        Map<String, dynamic> json) =>
    AttributeVersionModel(
      id: json['id'] as int?,
      attributeId: json['attributeId'] as int?,
      version: json['version'] as int?,
      timestamp: json['timestamp'] as int?,
      localPath: json['localPath'] as String?,
      onlineStoragePath: json['onlineStoragePath'] as String?,
      onlineStorageProvider: json['onlineStorageProvider'] as String?,
    );

Map<String, dynamic> _$AttributeVersionModelToJson(
        AttributeVersionModel instance) =>
    <String, dynamic>{
      'id': instance.id,
      'attributeId': instance.attributeId,
      'version': instance.version,
      'timestamp': instance.timestamp,
      'localPath': instance.localPath,
      'onlineStoragePath': instance.onlineStoragePath,
      'onlineStorageProvider': instance.onlineStorageProvider,
    };
