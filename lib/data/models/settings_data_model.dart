/*
 * @author Manuja Jayawardana
 * @email mjayawardana@yukon.lk
 * @create date 2020-08-25
 */

/// Data model for [Settings] table.
class SettingsDataModel {
  static const String _storeName = 'settings';

  final int? id;

  /// These modes are for enabling and disabling modes of the app.
  ///
  /// For enabling, make this true.
  /// For disabling (activating mode) make this false.
  final bool stewardshipMode;
  final bool dataSharingMode;
  final bool testMode;

  /// This is for showing the data request guideline bottom-up sheet or not.
  ///
  /// For showing it, make this [true], otherwise make it [false].
  final bool? tourDataRequestOpen;

  final String iconColor;

  static String getStoreName() => _storeName;

  SettingsDataModel({
    this.id,
    this.stewardshipMode = false,
    this.dataSharingMode = false,
    this.testMode = false,
    this.tourDataRequestOpen,
    required this.iconColor,
  });

  /// Converts [SettingsDataModel] to a JSON/Map.
  Map<String, dynamic> toJson() => {
        'id': id,
        'stewardshipMode': stewardshipMode,
        'dataSharingMode': dataSharingMode,
        'testMode': testMode,
        'tourDataRequestOpen': tourDataRequestOpen,
        'iconColor': iconColor,
      };

  /// Generates [SettingsDataModel] from a JSON/Map.
  factory SettingsDataModel.fromJson(Map<String, dynamic> json) {
    return SettingsDataModel(
      id: json['id'],
      stewardshipMode: json['stewardshipMode'],
      dataSharingMode: json['dataSharingMode'],
      testMode: json['testMode'],
      tourDataRequestOpen: json['tourDataRequestOpen'],
      iconColor: json['iconColor'],
    );
  }
}
