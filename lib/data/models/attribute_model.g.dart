// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'attribute_model.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

AttributeModel _$AttributeModelFromJson(Map<String, dynamic> json) =>
    AttributeModel(
      id: json['id'] as int?,
      label: json['label'] as String?,
      name: json['name'] as String?,
      description: json['description'] as String?,
      category: json['category'] as String?,
      group: json['group'] as String?,
      logoUrl: json['logoUrl'] as String?,
      providerName: json['providerName'] as String?,
      type: json['type'] as String?,
      timestamp: json['timestamp'] as int?,
      attrKeyEnc: json['attrKeyEnc'] as String?,
      gatewayLinkKey: json['gatewayLinkKey'] as String?,
      gatewayLinkOwnerKey: json['gatewayLinkOwnerKey'] as String?,
      gatewayLinkObjectId: json['gatewayLinkObjectId'] as String?,
      displayValue: json['displayValue'] as String?,
      mediaType: json['mediaType'] as String?,
    );

Map<String, dynamic> _$AttributeModelToJson(AttributeModel instance) =>
    <String, dynamic>{
      'id': instance.id,
      'name': instance.name,
      'label': instance.label,
      'description': instance.description,
      'category': instance.category,
      'group': instance.group,
      'providerName': instance.providerName,
      'logoUrl': instance.logoUrl,
      'type': instance.type,
      'attrKeyEnc': instance.attrKeyEnc,
      'gatewayLinkKey': instance.gatewayLinkKey,
      'gatewayLinkOwnerKey': instance.gatewayLinkOwnerKey,
      'gatewayLinkObjectId': instance.gatewayLinkObjectId,
      'timestamp': instance.timestamp,
      'displayValue': instance.displayValue,
      'mediaType': instance.mediaType,
    };
