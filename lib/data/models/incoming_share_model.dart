import 'package:json_annotation/json_annotation.dart';

part 'incoming_share_model.g.dart';

/// Data model for [IncomingShareModel] table.
@JsonSerializable()
class IncomingShareModel {
  static const String _storeName = 'IncomingShare';

  final int? id;
  final int? contractId;
  final String? category;
  final String? group;
  final String? name;
  final String? label;
  final String? logoUrl;
  final String? type;
  final int? version;
  final String? displayValue;
  final String? providerName;
  final String? mediaType;

  /// Uniques ID of the gateway link, this will be the same on both clients,
  /// so we know which incoming share to update when we recieve an update.
  final String? gatewayShareId;

  /// Full paths to the [gateway/linkId/shareId].
  final String? gatewayPath;

  /// The requires authorization token to be able to retrieve the encrypted
  /// attribute value when accessing [gatewayPath].
  final String? gatewayAuthToken;

  /// The attributes decryption key, required to be able to decrypt the retrieved
  /// attribute value from [gatewayPath].
  final String? gatewayAttributeKey;

  /// The internally used attribute encryption key.
  final String? attrKeyEnc;

  /// Contains the UUID filename of the locally stored encrypted attribute value.
  final String? localPath;

  final bool? isVC;

  /// MillisecondsSinceEpoch.
  final int? timestamp;

  /// Hides incoming share attribute.
  final bool isHideValue;

  static String getStoreName() => _storeName;

  IncomingShareModel({
    this.id,
    this.contractId,
    this.category,
    this.group,
    this.name,
    this.label,
    this.logoUrl,
    this.displayValue,
    this.providerName,
    this.type,
    this.mediaType,
    this.gatewayShareId,
    this.gatewayPath,
    this.gatewayAuthToken,
    this.gatewayAttributeKey,
    this.version,
    this.timestamp,
    this.attrKeyEnc,
    this.localPath,
    this.isVC,
    this.isHideValue = false,
  });

  factory IncomingShareModel.fromJson(Map<String, dynamic> json) => _$IncomingShareModelFromJson(
        json,
      );

  Map<String, dynamic> toJson() => _$IncomingShareModelToJson(this);
}
