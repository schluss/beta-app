import 'package:json_annotation/json_annotation.dart';

part 'attribute_model.g.dart';

// TODO: create a 'typed' type see [ContractModel].
/*
/// Type of attribute
enum AttributeType {
  @JsonValue('int')
  int,

  @JsonValue('string')
  string,

  @JsonValue('boolean')
  boolean,

  @JsonValue('file')
  file,
}
*/

/// Data models for 'Attribute' table.
@JsonSerializable()
class AttributeModel {
  static const String _storeName = 'Attribute';

  final int? id;
  final String? name;
  final String? label;
  final String? description;
  final String? category;
  final String? group;
  final String? providerName;
  final String? logoUrl;
  final String? type;

  /// Contains the encrypted attribute value key.
  String? attrKeyEnc;

  /// When this attributes is shared, it's metadata and sharing information
  /// is stored in the gateway of the user. These fields keep track of the gateway link.

  /// Uniques acces key, to be able to gain access to the gateway link of the attribute.
  final String? gatewayLinkKey;

  /// Uniques key which proves ownership of the attribute.
  final String? gatewayLinkOwnerKey;

  /// Uniques objectId, which identifies the link on the gateway.
  final String? gatewayLinkObjectId;

  /// This is for defining whether local file uploading or not.
  ///
  /// [true] - local file uploaded, [false] - received from 3rd party provider.

  // final bool? isLocal;

  /// MillisecondsSinceEpoch.
  int? timestamp;

  /// For some specifics type attributes initially another value is displayed
  /// instead of the real value. This applies to files (where it's the filename) and lists.
  final String? displayValue;

  /// Defines the media type when it's a file (formerly known as mimetype).
  final String? mediaType;

  static String getStoreName() => _storeName;

  AttributeModel({
    this.id,
    this.label,
    this.name,
    this.description,
    this.category,
    this.group,
    this.logoUrl,
    this.providerName,
    this.type,
    this.timestamp,
    this.attrKeyEnc,
    this.gatewayLinkKey,
    this.gatewayLinkOwnerKey,
    this.gatewayLinkObjectId,
    this.displayValue,
    this.mediaType,
    //this.isVC = false,
  });

  factory AttributeModel.fromJson(
    Map<String, dynamic> json,
  ) =>
      _$AttributeModelFromJson(
        json,
      );

  Map<String, dynamic> toJson() => _$AttributeModelToJson(this);
}
