import 'package:schluss_beta_app/business/impl/notification_bl_impl.dart';

/// Data models for notification table.
class NotificationModel {
  static const String _storeName = 'Notification';

  final int? id;
  final int connectionId;
  final String? title;
  final String? description;
  final String? routeName;

  /// When the route needs to have params, this contains a JSON encoded object.
  final String? routeParams;
  final bool? isClosable;

  /// A-Active , I-Inactive , R-Removed.
  final String? status;

  /// T-Tips , C-Completed , P-Pending.
  final String? type;

  /// MillisecondsSinceEpoch.
  final int? timestamp;

  static String getStoreName() => _storeName;

  NotificationModel(
    this.connectionId, {
    this.id,
    this.title,
    this.description,
    this.routeName,
    this.routeParams,
    this.isClosable,
    this.timestamp,
    this.status,
    this.type,
  });

  /// Converts [NotificationModel] to a JSON/Map.
  Map<String, dynamic> toJson() => {
        'id': id,
        'connectionId': connectionId,
        'title': title,
        'description': description,
        'routeName': routeName,
        'routeParams': routeParams,
        'isClosable': isClosable,
        'timestamp': timestamp,
        'status': status,
        'type': type,
      };

  /// Generates [NotificationModel] from a JSON/Map.
  factory NotificationModel.fromJson(Map<String, dynamic> json) {
    return NotificationModel(
      json['connectionId'],
      id: json['id'],
      title: json['title'],
      description: json['description'],
      routeName: json['routeName'],
      routeParams: json['routeParams'],
      isClosable: json['isClosable'],
      timestamp: json['timestamp'],
      status: json['status'] == NotificationState.active
          ? 'A'
          : json['status'] == NotificationState.inactive
              ? 'I'
              : 'R',
      type: json['type'] == NotificationType.tip ? 'T' : null,
    );
  }
}
