/*
 * @author Manuja Jayawardana
 * @email mjayawardana@yukon.lk
 * @create date 2020-05-23
 */

/// Enums for filter types.
enum FilterType { and, or }

abstract class CommonDAO {
  Future<Map<String, dynamic>> store(var storeName, Map<String, dynamic> model);
  Future<Map<String, dynamic>> storeTransaction(var storeName, Map<String, dynamic> model, var transaction);
  Future<Map<String, dynamic>?> update(var storeName, int id, Map<String, dynamic> model);
  Future<bool> updateFirst(var storeName, Map<String, dynamic> model);
  Future<List<Map<String, dynamic>>> getAll(var storeName);
  Future<List<Map<String, dynamic>>> getFilteredData(var storeName, Map<String, dynamic> filterVal, FilterType filterType, {Map<String, bool>? sortMap});
  Future<Map<String, dynamic>?> getDataFromId(var storeName, int id);
  Future<List<Map<String, dynamic>>> getSortedData(var storeName, Map<String, bool> sortMap);
  Future<bool> deleteById(var storeName, int? id);
  Future<bool> deleteDatabase();
  Future getDatabase();
  Future<int> getRecordCount(
    String storeName, {
    Map<String, dynamic>? filterVal,
    FilterType? filterType,
  });
}
