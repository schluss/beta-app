import 'dart:async';

import 'package:schluss_beta_app/data/interface/common_dao.dart';
import 'package:schluss_beta_app/data/models/contract/contract_model.dart';
import 'package:schluss_beta_app/singleton_injector.dart';
import 'package:sembast/utils/value_utils.dart';

class ContractStoreDAOImpl {
  final CommonDAO _commonDAOImpl = singletonInjector<CommonDAO>();

  Future<ContractModel?> getById(int contractId) async {
    var value = await _commonDAOImpl.getDataFromId(
      ContractModel.getStoreName(),
      contractId,
    );

    if (value == null) {
      return null;
    }

    // Make the map editable (by default an immutable map is returned).
    var editableRecord = cloneMap(value);

    // Add the [id] to the result.
    editableRecord['id'] = contractId;

    // Cast to [ContractModel].
    var model = ContractModel.fromJson(editableRecord);

    return model;
  }

  Future<ContractModel?> getByClientKey(String clientKey) async {
    Map<String, dynamic> filterMap = {
      'clientKey': clientKey,
    };

    var contracts = await _commonDAOImpl.getFilteredData(
      ContractModel.getStoreName(),
      filterMap,
      FilterType.and,
    );

    if (contracts.isEmpty) {
      return null;
    }

    // Make the map editable (by default an immutable map is returned).
    var editableRecord = cloneMap(contracts.last);

    // Cast to [ContractModel].
    var model = ContractModel.fromJson(editableRecord);

    return model;
  }

  /// Updates Contract Store
  Future<Map<String, dynamic>?> updateContractData(
    int contractId,
    Map<String, dynamic> keyValues,
  ) async {
    return _commonDAOImpl.update(
      ContractModel.getStoreName(),
      contractId,
      keyValues,
    );
  }
}
