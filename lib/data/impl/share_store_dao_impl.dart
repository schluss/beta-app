import 'dart:async';

import 'package:logger/logger.dart';
import 'package:schluss_beta_app/data/interface/common_dao.dart';
import 'package:schluss_beta_app/data/models/incoming_share_model.dart';
import 'package:schluss_beta_app/data/models/outgoing_share_model.dart';
import 'package:schluss_beta_app/singleton_injector.dart';
import 'package:sembast/utils/value_utils.dart';

class ShareStoreDAOImpl {
  final CommonDAO _commonDAOImpl = singletonInjector<CommonDAO>();
  final Logger _logger = singletonInjector<Logger>();

  /// Adds a new outgoing share.
  Future<int> addOutgoingShare(OutgoingShareModel model) async {
    model.timestamp ??= DateTime.now().millisecondsSinceEpoch;

    var result = await _commonDAOImpl.store(
      OutgoingShareModel.getStoreName(),
      model.toJson(),
    );

    return result['id'];
  }

  /// Gets all outgoing shares by given [attributeId]
  Future<List<OutgoingShareModel>> getOutgoingByAttribute(int attributeId) async {
    List<OutgoingShareModel> items = [];

    Map<String, dynamic> filterMap = {
      'attributeId': attributeId,
    };

    var shares = await _commonDAOImpl.getFilteredData(
      OutgoingShareModel.getStoreName(),
      filterMap,
      FilterType.and,
    );

    if (shares.isEmpty) {
      return items;
    }

    for (var share in shares) {
      // Make the map editable (by default an immutable map is returned).
      var item = cloneMap(share);

      // Cast to [OutgoingShareModel].
      var model = OutgoingShareModel.fromJson(item);

      items.add(model);
    }

    return items;
  }

  Future<IncomingShareModel?> getIncomingByGatewayShareId(String gatewayShareId) async {
    Map<String, dynamic> filterMap = {
      'gatewayShareId': gatewayShareId,
    };

    var shares = await _commonDAOImpl.getFilteredData(
      IncomingShareModel.getStoreName(),
      filterMap,
      FilterType.and,
    );

    if (shares.isEmpty) {
      return null;
    }

    // Make the map editable (by default an immutable map is returned).
    var editableRecord = cloneMap(shares.last);

    // Cast to [ContractModel].
    var model = IncomingShareModel.fromJson(editableRecord);

    return model;
  }

  /// Deletes outgoing share attribute from data store.
  Future<bool> deleteOutgoingShareAttribute(
    String gatewayShareId,
    int contractId,
  ) async {
    _logger.d('delete Outgoing Shared Attribute');
    Map<String, dynamic> filterMap = {
      'gatewayShareId': gatewayShareId,
      'contractId': contractId,
    };

    List<Map<String, dynamic>> dataList = await _commonDAOImpl.getFilteredData(
      OutgoingShareModel.getStoreName(),
      filterMap,
      FilterType.and,
    );

    // TODO: need to validate what happens if multiple records found.
    if (dataList.isNotEmpty && dataList.length == 1) {
      await _commonDAOImpl.deleteById(
        OutgoingShareModel.getStoreName(),
        dataList.first['id'],
      );
      return true;
    } else {
      throw Exception('Shared Attribute not found with provided filters');
    }
    // return false;
  }

  /// Deletes incoming share attribute from data store.
  Future<bool> deleteIncomingShareAttribute(IncomingShareModel incomingShareModel) async {
    if (incomingShareModel.id != null) {
      await _commonDAOImpl.deleteById(
        IncomingShareModel.getStoreName(),
        incomingShareModel.id,
      );
    } else {
      throw Exception('Shared Attribute not found with provided filters');
    }
    return false;
  }
}
