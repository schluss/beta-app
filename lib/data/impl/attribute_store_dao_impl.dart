import 'dart:async';
import 'dart:io';

import 'package:path_provider/path_provider.dart';
import 'package:schluss_beta_app/data/interface/common_dao.dart';
import 'package:schluss_beta_app/data/models/attribute_model.dart';
import 'package:schluss_beta_app/data/models/attribute_version_model.dart';
import 'package:schluss_beta_app/services/impl/task_service.dart';
import 'package:schluss_beta_app/singleton_injector.dart';
import 'package:schluss_beta_app/util/crypto_util.dart';
import 'package:schluss_beta_app/view_model/field_view_model.dart';
import 'package:sembast/utils/value_utils.dart';
import 'package:session_next/session_next.dart';
import 'package:uuid/uuid.dart';

class AttributeStoreDAOImpl {
  final CommonDAO _commonDAOImpl = singletonInjector<CommonDAO>();
  final Uuid uuid = singletonInjector<Uuid>();
  final _taskService = singletonInjector<TaskService>();

  Future<Map<String, dynamic>?> updateAttributeMetadata(
    int attributeId,
    Map<String, dynamic> keyValues,
  ) async {
    return _commonDAOImpl.update(
      AttributeModel.getStoreName(),
      attributeId,
      keyValues,
    );
  }

  Future<Map<String, dynamic>?> updateAttributeVersionMetadata(
    int attributeVersionId,
    Map<String, dynamic> keyValues,
  ) async {
    return _commonDAOImpl.update(
      AttributeVersionModel.getStoreName(),
      attributeVersionId,
      keyValues,
    );
  }

  Future<AttributeModel?> storeAttributeFromModel(
    AttributeModel model,
    String value,
  ) async {
    AttributeModel? attributeModel;
    String attributeRandomKey;

    // Check if the attribute already exists.
    if (model.id == null) {
      attributeModel = await findAttribute(
        model.name!,
        model.category!,
        model.providerName!,
      );
    } else {
      // Update the attribute metadata.
      attributeModel = model;
    }

    // When the attribute does not exist yet, we create a new one.
    if (attributeModel == null) {
      attributeModel = model;

      // Generate a random attribute encryption key.
      attributeRandomKey = CryptoUtil.generateRandomKey();

      // Encrypt the random attribute encryption key with the users' [publicKey].
      String attributeKeyEncrypted = await CryptoUtil.encrypt(
        attributeRandomKey,
        SessionNext().get<String>('publicKey'),
      );

      // Add the encrypted attribute encryption key to the attribute.
      model.attrKeyEnc = attributeKeyEncrypted;

      model.timestamp = DateTime.now().millisecondsSinceEpoch;

      Map attributeMap = await _commonDAOImpl.store(
        AttributeModel.getStoreName(),
        attributeModel.toJson(),
      );

      if (attributeMap.isEmpty) {
        throw ('Could not store the attribute model');
      }

      attributeModel = AttributeModel.fromJson(
        attributeMap as Map<String, dynamic>,
      );
    }

    // Otherwise, get and decrypt the [attributeRandomKey].
    else {
      attributeRandomKey = await CryptoUtil.decrypt(
        attributeModel.attrKeyEnc!,
        privateKey: SessionNext().get<String>('privateKey'),
      );
    }

    // Store the attribute value.
    var attributeValueFilename = await storeAttributeValue(
      value,
      attributeRandomKey,
    );

    if (attributeValueFilename == null) {
      throw ('Could not store attribute value');
    }

    // Store the attribute Version.
    var attributeVersionModel = await _storeAttributeVersion(
      attributeModel.id!,
      attributeValueFilename,
    );

    if (attributeVersionModel == null) {
      throw ('Could not store the attribute version');
    }

    return attributeModel;
  }

  /// Stores an attribute.
  /// Deprecated.
  @Deprecated(
    'Use storeAttributeFromModel instead. This one uses FieldViewModel which is not good practice',
  )
  Future<AttributeModel?> storeAttribute(
    String dataProviderName,
    String logoUrl,
    FieldViewModel field,
  ) async {
    AttributeModel? attributeModel;

    String attributeRandomKey;

    // Check if the attribute already exists.
    attributeModel = await findAttribute(
      field.name!,
      field.category!,
      dataProviderName,
    );

    // When the attribute does not exist yet, we create a new one.
    if (attributeModel == null) {
      // Generate a random attribute encryption key.
      attributeRandomKey = CryptoUtil.generateRandomKey();

      // Encrypt the random attribute encryption key with the users' [publicKey].
      String attributeKeyEncrypted = await CryptoUtil.encrypt(
        attributeRandomKey,
        SessionNext().get<String>('publicKey'),
      );

      attributeModel = AttributeModel(
        name: field.name,
        label: field.label,
        description: field.description,
        category: field.category,
        providerName: dataProviderName,
        logoUrl: logoUrl,
        type: field.type,
        timestamp: DateTime.now().millisecondsSinceEpoch,
        displayValue: field.displayValue,
        attrKeyEnc: attributeKeyEncrypted,
      );

      Map attributeMap = await _commonDAOImpl.store(
        AttributeModel.getStoreName(),
        attributeModel.toJson(),
      );

      if (attributeMap.isEmpty) {
        throw ('Could not store the attribute model');
      }

      attributeModel = AttributeModel.fromJson(
        attributeMap as Map<String, dynamic>,
      );
    }

    // Otherwise, get and decrypt the [attributeRandomKey].
    else {
      attributeRandomKey = await CryptoUtil.decrypt(
        attributeModel.attrKeyEnc!,
        privateKey: SessionNext().get<String>('privateKey'),
      );
    }

    // Store the attribute value.
    var attributeValueFilename = await storeAttributeValue(
      field.value!,
      attributeRandomKey,
    );

    if (attributeValueFilename == null) {
      throw ('Could not store attribute value');
    }

    // Store the attribute version.
    var attributeVersionModel = await _storeAttributeVersion(
      attributeModel.id!,
      attributeValueFilename,
    );

    if (attributeVersionModel == null) {
      throw ('Could not store the attribute version');
    }

    return attributeModel;
  }

  /// [findNextAttributeVersion(attributeId, {attributeCompareValue})]
  /// Gets the current latest version of the attribute
  /// if [attributeCompareValue] is set:
  /// -> get the value of the latest version, and compare it with [attributeCompareValue]
  /// if same: return this version number.
  // TODO: re-implement the comparison check where we look at the latest stored
  //  version, compare it to the value we want to store. If the same: we should
  //  not store a duplicate. Store a version of an attribute, if online mode is
  //  enabled it is also stored on in the external storage.
  Future<AttributeVersionModel?> _storeAttributeVersion(int attributeId, String attributeValueLocation) async {
    var version = await _findLatestAttributeVersionNumber(attributeId);

    // Add one to the latest version found, because we're storing a newer version.
    version += 1;

    var attributeVersionModel = AttributeVersionModel(
      attributeId: attributeId,
      version: version,
      timestamp: DateTime.now().millisecondsSinceEpoch,
      localPath: attributeValueLocation,
    );

    // Store the attribute version.
    Map attributeVersionMap = await _commonDAOImpl.store(
      AttributeVersionModel.getStoreName(),
      attributeVersionModel.toJson(),
    );

    attributeVersionModel = AttributeVersionModel.fromJson(
      attributeVersionMap as Map<String, dynamic>,
    );

    // Sync the attribute to the external storage.
    _taskService.add('SyncAttributeTask', {
      'id': attributeVersionModel.id,
    });

    return attributeVersionModel;
  }

  Future<AttributeVersionModel?> getAttributeVersion(
    int attributeVersionId,
  ) async {
    var attrVersion = await _commonDAOImpl.getDataFromId(
      AttributeVersionModel.getStoreName(),
      attributeVersionId,
    );

    if (attrVersion == null) {
      return null;
    }

    var model = AttributeVersionModel.fromJson(cloneMap(attrVersion));

    return model;
  }

  /// Gets's the latest (highest) stored version number for an attribute.
  /// When no stored versions are found, it returns 0.
  Future<int> _findLatestAttributeVersionNumber(int attributeId) async {
    var lastVersionModel = await findLatestAttributeVersion(attributeId);

    if (lastVersionModel == null || lastVersionModel.version == null) {
      return 0;
    }

    return lastVersionModel.version!;
  }

  /// Gets's the latest (highest) stored version for an attribute. When no stored
  /// versions are found, it returns [null].
  Future<AttributeVersionModel?> findLatestAttributeVersion(
    int attributeId,
  ) async {
    // Try to get the latest version, if there are stored versions for this attribute already.
    var attributeVersionList = await _commonDAOImpl.getFilteredData(
      AttributeVersionModel.getStoreName(),
      {'attributeId': attributeId},
      FilterType.and,
      sortMap: {'version': true},
    );

    if (attributeVersionList.isEmpty) {
      return null;
    }

    // When existing attribute versions are available.
    var lastVersionModel = AttributeVersionModel.fromJson(
      attributeVersionList.last,
    );

    return lastVersionModel;
  }

  /// Gets all attribute versions for given [attributeId].
  Future<List<AttributeVersionModel>> getAttributeVersions(
    int attributeId,
  ) async {
    var items = <AttributeVersionModel>[];

    // Try to get the latest version, if there are stored versions for this attribute already.
    var attributeVersionList = await _commonDAOImpl.getFilteredData(
      AttributeVersionModel.getStoreName(),
      {'attributeId': attributeId},
      FilterType.and,
      sortMap: {'version': true},
    );

    if (attributeVersionList.isEmpty) {
      return items;
    }

    for (var v in attributeVersionList) {
      items.add(AttributeVersionModel.fromJson(v));
    }

    return items;
  }

  /// Stores the value of an attribute.
  Future<String?> storeAttributeValue(
    String value,
    String encryptionKey,
  ) async {
    // Encrypt the value.
    var attrValueEnc = await CryptoUtil.encryptSymmetric(value, encryptionKey);

    // Generate a unique filename based on a [UUID] v4.
    var attrHash = uuid.v4();

    // Store as file.
    String filePath = await _getAppDirectory();
    File f = File('$filePath/$attrHash.${CryptoUtil.encryptedFileExtension}');
    await f.writeAsString(attrValueEnc);

    return attrHash;
  }

  /// Finds an attribute by the given name, category and data provider.
  /// In case multiple results are found, the first one is returned.
  Future<AttributeModel?> findAttribute(
    String name,
    String category,
    String providerName,
  ) async {
    var attributeFilterMap = <String, dynamic>{
      'name': name,
      'category': category,
      'providerName': providerName,
    };

    var attributeList = await _commonDAOImpl.getFilteredData(
      AttributeModel.getStoreName(),
      attributeFilterMap,
      FilterType.and,
    );

    if (attributeList.isEmpty) {
      return null;
    }

    var attributeModel = AttributeModel.fromJson(attributeList.first);

    return attributeModel;
  }

  Future<AttributeModel?> getAttribute(int attributeId) async {
    var attribute = await _commonDAOImpl.getDataFromId(
      AttributeModel.getStoreName(),
      attributeId,
    );

    if (attribute == null || attribute.isEmpty) {
      return null;
    }

    AttributeModel attributeModel = AttributeModel.fromJson(attribute);
    return attributeModel;
  }

  /// Gets the value of an attribute. When no version given, it returns
  /// the value of the  latest version.
  Future<String?> getAttributeValue(
    int attributeId, {
    String? attrKeyEnc,
    String? localPath,
    int? version,
    bool doNotDecrypt = false,
  }) async {
    if (version != null) {
      throw ('searching a specific version of an attribute value is not implemented yet');
    }

    if (attrKeyEnc == null) {
      // Get the attribute, to have the decryption key.
      var attributeModel = await getAttribute(attributeId);

      if (attributeModel == null) {
        return null;
      }

      attrKeyEnc = attributeModel.attrKeyEnc;
    }

    if (localPath == null) {
      // Get the latest version.
      var versionModel = await findLatestAttributeVersion(attributeId);

      if (versionModel == null) {
        return null;
      }

      localPath = versionModel.localPath;
    }

    // Get the file value.
    var attrValue = await getAttributeValueFromDirectory(
      localPath!,
      attrKeyEnc!,
      doNotDecrypt: doNotDecrypt,
    );

    return attrValue;
  }

  /// Decrypts value in local path.
  Future<String?> getAttributeValueFromDirectory(
    String localPath,
    String attrKeyEnc, {
    bool doNotDecrypt = false,
  }) async {
    // Get the file.
    String filePath = await _getAppDirectory();
    File f = File('$filePath/$localPath.${CryptoUtil.encryptedFileExtension}');

    var attrValue = await f.readAsString();

    // When the value needs to be returned in decrypted form.
    if (!doNotDecrypt) {
      String? attrKeyDec;
      // Decrypt the attribute key.
      attrKeyDec = await CryptoUtil.decrypt(
        attrKeyEnc,
        privateKey: SessionNext().get<String>('privateKey'),
      );

      // Decrypt the attribute.
      attrValue = await CryptoUtil.decryptSymmetric(attrValue, attrKeyDec);
    }
    return attrValue;
  }

  Future<bool> deleteAttribute(int attributeId) async {
    // Unlink and unshare.
    // TODO: remove all online things. Get all attribute versions.
    var versions = await getAttributeVersions(attributeId);

    for (var version in versions) {
      // Delete attribute version and value.
      await deleteAttributeVersion(version.id!);
    }

    // Delete attribute.
    return _commonDAOImpl.deleteById(
      AttributeModel.getStoreName(),
      attributeId,
    );
  }

  /// Deletes an attribute value by given hash.
  Future<bool> deleteAttributeValue(String attrHash) async {
    var basePath = await _getAppDirectory();
    var path = '$basePath/$attrHash.${CryptoUtil.encryptedFileExtension}';

    if (await File(path).exists()) {
      await File(path).delete();
    }

    return true;
  }

  /// Deletes an attribute version by given [id], if this attribute is stored online,
  /// it's also deleted from there.
  Future<bool> deleteAttributeVersion(int attributeVersionId) async {
    var model = await getAttributeVersion(attributeVersionId);

    await _commonDAOImpl.deleteById(
      AttributeVersionModel.getStoreName(),
      attributeVersionId,
    );

    // Delete the online stored value of the attribute.
    if (model != null && model.onlineStoragePath != null) {
      _taskService.add('SyncAttributeRemoveTask', {
        'id': model.id!,
        'onlineStoragePath': model.onlineStoragePath!,
      });
    }

    return true;
  }

  /// Returns application directory path.
  Future<String> _getAppDirectory() async {
    var dir = await getApplicationDocumentsDirectory();
    return dir.path;
  }
}
