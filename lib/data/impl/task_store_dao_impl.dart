import 'dart:async';

import 'package:schluss_beta_app/data/models/task_model.dart';
import 'package:schluss_beta_app/database_config.dart';
import 'package:schluss_beta_app/singleton_injector.dart';
import 'package:sembast/sembast.dart';
import 'package:sembast/utils/value_utils.dart';

class TaskStoreDAOImpl {
  final String storeName = 'tasks';
  final AppDatabaseConfig _dbConfig = singletonInjector<AppDatabaseConfig>();

  Future<int> add(TaskModel model) async {
    var db = await getDatabase();
    var dataStore = intMapStoreFactory.store(storeName);

    model.created ??= DateTime.now().millisecondsSinceEpoch;

    // Store the record and adds the [id] to the model.
    model.id = await dataStore.add(db!, model.toJson());

    return model.id!;
  }

  Future<bool> delete(int taskId) async {
    var db = await getDatabase();
    StoreRef<int?, Map<String, Object?>> dataStore = intMapStoreFactory.store(
      storeName,
    );

    var result = await dataStore.record(taskId).delete(db!);

    // When result not [null], deletion succeeded, so return a [true].
    return result != null ? true : false;
  }

  Future<TaskModel?> getById(int taskId) async {
    var db = await getDatabase();

    StoreRef<int?, Map<String, Object?>> dataStore = intMapStoreFactory.store(
      storeName,
    );
    var value = await dataStore.record(taskId).get(db!);

    if (value == null) {
      return null;
    }

    // Make the map editable (by default an immutable map is returned).
    var editableRecord = cloneMap(value);

    // Add the [id] to the result.
    editableRecord['id'] = taskId;

    // Cast to [TaskModel].
    var model = TaskModel.fromJson(editableRecord);

    return model;
  }

  Future<TaskModel?> getNext(String status) async {
    var db = await getDatabase();

    var finder = Finder(filter: Filter.equals('status', status));

    StoreRef<int?, Map<String, Object?>> dataStore = intMapStoreFactory.store(
      storeName,
    );
    var record = await dataStore.findFirst(db!, finder: finder);

    if (record == null) {
      return null;
    }

    return _recordToModel(record);
  }

  TaskModel _recordToModel(RecordSnapshot<int?, Map<String, Object?>> record) {
    // Make the map editable (by default an immutable map is returned).
    var editableRecord = cloneMap(record.value);

    // Add the [id] to the result.
    editableRecord['id'] = record.key;

    // Cast to [TaskModel].
    var model = TaskModel.fromJson(editableRecord);

    return model;
  }

  Future<bool> update(TaskModel model) async {
    var db = await getDatabase();

    StoreRef<int?, Map<String, Object?>> dataStore = intMapStoreFactory.store(
      storeName,
    );

    var record = dataStore.record(model.id);

    await record.put(db!, model.toJson(), merge: true);

    return true;
  }

  Future<List<TaskModel>> getAsList({String? status}) async {
    List<TaskModel> resultList = [];

    var db = await getDatabase();

    var dataStore = intMapStoreFactory.store(storeName);

    // Get the all notifications.
    if (status == null) {
      var records = await dataStore.find(db!);

      for (var record in records) {
        resultList.add(_recordToModel(record));
      }

      // Get filtered result set.
    } else {
      var records = await dataStore.find(db!,
          finder: Finder(
              filter: Filter.and([
            Filter.matchesRegExp(
              'status',
              RegExp(
                status,
                caseSensitive: false,
              ),
            ),
          ])));

      for (var record in records) {
        resultList.add(_recordToModel(record));
      }
    }

    return resultList;
  }

  /// Deletes all tasks by dropping the whole tasks store.
  Future<void> deleteAll() async {
    var db = await getDatabase();

    var dataStore = intMapStoreFactory.store(storeName);
    await dataStore.drop(db!);
  }

  Future<Database?> getDatabase() async {
    return _dbConfig.getInstance();
  }

  Future<bool> deleteDatabase() async {
    await _dbConfig.deleteDatabase();

    return Future.value(true);
  }
}
