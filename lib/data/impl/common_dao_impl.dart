import 'dart:async';

import 'package:schluss_beta_app/data/interface/common_dao.dart';
import 'package:schluss_beta_app/database_config.dart';
import 'package:schluss_beta_app/singleton_injector.dart';
import 'package:sembast/sembast.dart';

class CommonDAOImpl implements CommonDAO {
  /// Gets a record by [id].
  @override
  Future<Map<String, dynamic>?> getDataFromId(var storeName, int id) async {
    var db = await getDatabase();

    StoreRef<int?, Map<String, Object?>> dataStore = intMapStoreFactory.store(
      storeName,
    );
    var value = await dataStore.record(id).get(db);

    return value;
  }

  /// Stores a new record.
  @override
  Future<Map<String, dynamic>> store(
    var storeName,
    Map<String, dynamic> model,
  ) async {
    var db = await getDatabase();
    var dataStore = intMapStoreFactory.store(storeName);

    // Store the record.
    var id = await dataStore.add(db, model);

    // Update the record to add the [id] as a field to it.
    var record = dataStore.record(id);

    await record.put(db, {'id': id}, merge: true);

    model.update('id', (value) => id);

    return model;
  }

  /// Stores a new record in a transaction.
  ///
  /// Only use this method if you are doing transaction.
  /// Note : Make sure not to use database related tasks ( Store / Read / Update)
  /// until transaction is finished. It will result a deadlock in database.
  /// [transaction] parameter mandatory if this method executed inside a database
  /// transaction, unless it will result a deadlock.
  @override
  Future<Map<String, dynamic>> storeTransaction(
    var storeName,
    Map<String, dynamic> model,
    var transaction,
  ) async {
    dynamic db;

    // Validate if transaction on database available. If available transaction
    // will be use as a database.
    if (transaction == null) {
      throw Exception('Transaction not initialized');
    }

    db = transaction;

    var dataStore = intMapStoreFactory.store(storeName);

    // Store the record.
    var id = await dataStore.add(db, model);

    // Update the record to add the [id] as a field to it.
    var record = dataStore.record(id);

    await record.put(db, {'id': id}, merge: true);

    model.update('id', (value) => id);

    return model;
  }

  /// Update an existing record by [id]
  @override
  Future<Map<String, dynamic>?> update(var storeName, int id, Map<String, dynamic> model) async {
    var db = await getDatabase();

    var dataStore = intMapStoreFactory.store(storeName);

    var record = dataStore.record(id);

    var ret = await record.put(db!, model, merge: true);

    return ret;
  }

  /// Update First found record
  @override
  Future<bool> updateFirst(var storeName, Map<String, dynamic> model) async {
    var db = await getDatabase();

    var dataStore = intMapStoreFactory.store(storeName);

    // Find the first record matching the finder
    var record = await dataStore.findFirst(db);

    if (record == null) {
      return false;
    }

    // Get the record id
    var recordId = record.key;

    await update(storeName, recordId, model);

    return true;
  }

  /// Gets all records in a table.
  @override
  Future<List<Map<String, dynamic>>> getAll(var storeName) async {
    var db = await getDatabase();

    var dataStore = intMapStoreFactory.store(storeName);
    var records = (await dataStore.find(db));

    return records.map((result) => result.value).toList();
  }

  /// Gets and filters records using a key-value Map.
  ///
  /// Key : column names.
  /// Value : arguments for Where field.
  /// Optional parameter : Sorting List (sorted by list order).
  @override
  Future<List<Map<String, dynamic>>> getFilteredData(
    var storeName,
    Map<String, dynamic> filterVal,
    FilterType filterType, {
    Map<String, bool>? sortMap,
  }) async {
    var db = await getDatabase();
    var dataStore = intMapStoreFactory.store(storeName);
    Finder finder;
    List<Filter> filterList;
    List<SortOrder> sortList;

    filterList = getFilteringList(filterVal);
    sortList = getSortingList(sortMap);

    if (filterType == FilterType.or) {
      finder = Finder(filter: Filter.or(filterList), sortOrders: sortList);
    } else {
      finder = Finder(filter: Filter.and(filterList), sortOrders: sortList);
    }

    var records = await dataStore.find(db, finder: finder);

    return records.map((result) {
      // Add the [id] to the record.
      if (!result.value.containsKey('id')) {
        result.value['id'] = result.key;
      }

      return result.value;
    }).toList();
  }

  /// Gets and sorts records using a key-value Map.
  ///
  /// Key : column names.
  /// Value : [true] - ascending , [false] - descending.
  @override
  Future<List<Map<String, dynamic>>> getSortedData(
    var storeName,
    Map<String, bool> sortMap,
  ) async {
    var db = await getDatabase();
    List<SortOrder> sortList;
    sortList = [];
    Finder finder;

    var dataStore = intMapStoreFactory.store(storeName);

    sortList = getSortingList(sortMap);

    finder = Finder(sortOrders: sortList);

    var records = await dataStore.find(db, finder: finder);

    return records.map((result) => result.value).toList();
  }

  /// Gets filtering criteria List.
  ///
  /// Map template {filter_column : filter_argument}.
  List<Filter> getFilteringList(Map<String, dynamic> filterVal) {
    List<Filter> filterList;
    filterList = [];

    for (var key in filterVal.keys) {
      dynamic filterValue = filterVal[key];

      if (filterValue is bool || filterValue is int) {
        filterList.add(Filter.equals(key, filterValue));
      } else if (filterValue is List<Object>) {
        filterList.add(Filter.inList(key, filterValue));
      } else {
        // String values search case insensitive with [Regex].
        filterList.add(
          Filter.matchesRegExp(
            key,
            RegExp(filterValue, caseSensitive: false),
          ),
        );
      }
    }
    return filterList;
  }

  /// Gets sorting list.
  List<SortOrder> getSortingList(Map<String, bool>? sortMap) {
    List<SortOrder> sortList;
    sortList = [];

    if (sortMap != null) {
      for (var key in sortMap.keys) {
        sortList.add(SortOrder(key, sortMap[key]!));
      }
    }
    return sortList;
  }

  /// Counts the amount of records in [storeName].
  @override
  Future<int> getRecordCount(
    String storeName, {
    Map<String, dynamic>? filterVal,
    FilterType? filterType,
  }) async {
    var db = await getDatabase();
    var dataStore = intMapStoreFactory.store(storeName);
    var count = 0;

    if (filterVal != null && filterType != null) {
      List<Filter> filterList = getFilteringList(filterVal);
      count = await dataStore.count(
        db,
        filter: filterType == FilterType.or
            ? Filter.or(filterList)
            : Filter.and(
                filterList,
              ),
      );
    } else {
      count = await dataStore.count(db);
    }

    return count;
  }

  /// Deletes a record by [id].
  @override
  Future<bool> deleteById(storeName, int? id) async {
    var db = await getDatabase();
    StoreRef<int?, Map<String, Object?>> dataStore = intMapStoreFactory.store(
      storeName,
    );

    var result = await dataStore.record(id).delete(db);

    // When result not [null], deletion succeeded, so return a [true].
    if (result != null) {
      return true;
    }
    return false;
  }

  /// Deletes the database.
  @override
  Future<bool> deleteDatabase() async {
    await singletonInjector<AppDatabaseConfig>().deleteDatabase();

    return Future.value(true);
  }

  /// Gets the database instance.
  @override
  Future getDatabase() async {
    var dbConfig = singletonInjector<AppDatabaseConfig>();
    return dbConfig.getInstance();
  }
}
