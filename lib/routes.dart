import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:schluss_beta_app/constants/helper_constants.dart';
import 'package:schluss_beta_app/controller/plugin_controller.dart';
import 'package:schluss_beta_app/ui/dashboard/account/account_deletion_confirmation_bottom_up_sheet.dart';
import 'package:schluss_beta_app/ui/dashboard/account/account_deletion_final_confirmation_bottom_up_sheet.dart';
import 'package:schluss_beta_app/ui/dashboard/account/data_exporting_bottom_up_sheet.dart';
import 'package:schluss_beta_app/ui/dashboard/account/data_exporting_confirmation_bottom_up_sheet.dart';
import 'package:schluss_beta_app/ui/dashboard/account/data_exporting_done_page.dart';
import 'package:schluss_beta_app/ui/dashboard/account/feedback_bottom_up_sheet.dart';
import 'package:schluss_beta_app/ui/dashboard/account/feedback_done_page.dart';
import 'package:schluss_beta_app/ui/dashboard/account/pin_changing_done_page.dart';
import 'package:schluss_beta_app/ui/dashboard/account/promise_bottom_up_sheet.dart';
import 'package:schluss_beta_app/ui/dashboard/account/vaults_switching_bottom_up_sheet.dart';
import 'package:schluss_beta_app/ui/dashboard/base.dart';
import 'package:schluss_beta_app/ui/dashboard/contracts/connection_delete_dialog.dart';
import 'package:schluss_beta_app/ui/dashboard/contracts/connection_my_data_delete_dialog.dart';
import 'package:schluss_beta_app/ui/dashboard/contracts/connection_page.dart';
import 'package:schluss_beta_app/ui/dashboard/contracts/connection_shared_data_delete_dialog.dart';
import 'package:schluss_beta_app/ui/dashboard/contracts/contract_deletion_confirmation_bottom_up_sheet.dart';
import 'package:schluss_beta_app/ui/dashboard/contracts/contract_info.dart';
import 'package:schluss_beta_app/ui/dashboard/contracts/contract_page.dart';
import 'package:schluss_beta_app/ui/dashboard/data/data_deletion_confirmation_bottom_up_sheet.dart';
import 'package:schluss_beta_app/ui/dashboard/data/data_deletion_error_bottom_up_sheet.dart';
import 'package:schluss_beta_app/ui/dashboard/data/data_details_list_item_page.dart';
import 'package:schluss_beta_app/ui/dashboard/data/data_details_page.dart';
import 'package:schluss_beta_app/ui/dashboard/data/data_select_attributes_page.dart';
import 'package:schluss_beta_app/ui/dashboard/data/data_select_groups_page.dart';
import 'package:schluss_beta_app/ui/dashboard/data/data_select_summary_attributes_page.dart';
import 'package:schluss_beta_app/ui/dashboard/data/data_select_summary_page.dart';
import 'package:schluss_beta_app/ui/dashboard/data/data_upload_options_dialog.dart';
import 'package:schluss_beta_app/ui/dashboard/data/data_uploading_bottom_up_sheet.dart';
import 'package:schluss_beta_app/ui/dashboard/data/new_data_attribute_detail_page.dart';
import 'package:schluss_beta_app/ui/dashboard/data/new_data_category_detail_page.dart';
import 'package:schluss_beta_app/ui/dashboard/data_request/data_provider_info_page.dart';
import 'package:schluss_beta_app/ui/dashboard/data_request/data_request_done_page.dart';
import 'package:schluss_beta_app/ui/dashboard/data_request/data_request_guideline_bottom_up_sheet.dart';
import 'package:schluss_beta_app/ui/dashboard/data_request/data_request_page.dart';
import 'package:schluss_beta_app/ui/dashboard/data_request/data_request_retrieval_page.dart';
import 'package:schluss_beta_app/ui/dashboard/identification/add_identity.dart';
import 'package:schluss_beta_app/ui/dashboard/qr/qr.dart';
import 'package:schluss_beta_app/ui/dashboard/stewardship/add_identity_bottom_sheet_content.dart';
import 'package:schluss_beta_app/ui/dashboard/stewardship/cancellation_page_content.dart';
import 'package:schluss_beta_app/ui/dashboard/stewardship/identity_addition_guideline_bottom_up_sheet.dart';
import 'package:schluss_beta_app/ui/dashboard/stewardship/owner_agreement_page_content.dart';
import 'package:schluss_beta_app/ui/dashboard/stewardship/owner_authorize_page_content.dart';
import 'package:schluss_beta_app/ui/dashboard/stewardship/owner_define_access_timing_page_content.dart';
import 'package:schluss_beta_app/ui/dashboard/stewardship/owner_info_page_content.dart';
import 'package:schluss_beta_app/ui/dashboard/stewardship/owner_qr_code_page_content.dart';
import 'package:schluss_beta_app/ui/dashboard/stewardship/owner_remove_authorization_page_content.dart';
import 'package:schluss_beta_app/ui/dashboard/stewardship/ownership_sent_page_content.dart';
import 'package:schluss_beta_app/ui/dashboard/stewardship/steward_access_page_content.dart';
import 'package:schluss_beta_app/ui/dashboard/stewardship/steward_agreement_page_content.dart';
import 'package:schluss_beta_app/ui/dashboard/stewardship/steward_authorize_page_content.dart';
import 'package:schluss_beta_app/ui/dashboard/stewardship/stewardship_received_page_content.dart';
import 'package:schluss_beta_app/ui/dashboard/stewardship/vault_switching_info_page_content.dart';
import 'package:schluss_beta_app/ui/dashboard/stewardship/waiting_page_content.dart';
import 'package:schluss_beta_app/ui/intro/intro_page.dart';
import 'package:schluss_beta_app/ui/intro/promise_page.dart';
import 'package:schluss_beta_app/ui/intro/terms_and_conditions_bottom_up_sheet.dart';
import 'package:schluss_beta_app/ui/login/login_countdown_page.dart';
import 'package:schluss_beta_app/ui/login/login_page.dart';
import 'package:schluss_beta_app/ui/registration/pin_confirmation_page.dart';
import 'package:schluss_beta_app/ui/registration/pin_creation_page.dart';
import 'package:schluss_beta_app/ui/registration/vault_creation_page.dart';
import 'package:schluss_beta_app/ui/reused_widgets/error_page.dart';
import 'package:schluss_beta_app/ui/reused_widgets/pin_verification_bottom_up_sheet.dart';
import 'package:schluss_beta_app/ui/splash/splash_page.dart';
import 'package:schluss_beta_app/util/slide_route_transition.dart.dart';
import 'package:schluss_beta_app/view_model/contract_detail_view_model.dart';
import 'package:schluss_beta_app/view_model/contract_recent_item.dart';
import 'package:schluss_beta_app/view_model/field_view_model.dart';
import 'package:schluss_beta_app/view_model/gateway_viewmodel.dart';
import 'package:schluss_beta_app/view_model/my_data_view_model.dart';
import 'package:schluss_beta_app/view_model/request_view_model.dart';

Route<dynamic> generateRoute(RouteSettings settings) {
  switch (settings.name) {

    // Splash page.
    case '/':
      return SlideRouteTransition(widget: const SplashPage());

    // Intro page.
    case '/intro':
      return SlideRouteTransition(widget: const IntroPage());

    // Promise page.
    case '/promise':
      return SlideRouteTransition(widget: const PromisePage());

    // Term & conditions bottom-up sheet.
    case '/terms-and-conditions':
      return SlideRouteTransition(
        widget: const TermsAndConditionsBottomUpSheet(),
        animationDirection: HelperConstant.bottomToTopDirection,
      );

    // Registration - creating PIN page.
    case '/account/registration/create-pin':
      return SlideRouteTransition(widget: const PinCreationPage());

    // Changing PIN page.
    case '/account/change-pin':
      String? oldPassword = settings.arguments as String?;
      return SlideRouteTransition(
        widget: PinCreationPage(
          isChangingPinCodeFlow: true,
          oldPassword: oldPassword,
        ),
      );

    //  Registration - confirming PIN page.
    case '/account/registration/confirm-pin':
      Map map = settings.arguments as Map<dynamic, dynamic>;
      String? password = map['currentPinCode'];
      bool? isChangingPinCodeFlow = map['isChangingPinCodeFlow'];
      String? oldPassword = map['oldPassword'];
      return SlideRouteTransition(
        widget: PinConfirmationPage(
          password,
          isChangingPinCodeFlow: isChangingPinCodeFlow,
          oldPassword: oldPassword,
        ),
      );

    // Vault creation page.
    case '/account/create':
      String? password = settings.arguments as String?;
      return SlideRouteTransition(widget: VaultCreationPage(password: password));

    // Changing PIN done page.
    case '/account/change-pin/done':
      return SlideRouteTransition(widget: const PinChangingDonePage());

    // Log-in page.
    case '/account/login':
      return SlideRouteTransition(widget: const LoginPage());

    // Log-in countdown page.
    case '/account/login/countdown':
      return SlideRouteTransition(widget: const LoginCountdownPage());

    // Dashboard pages.
    case '/dashboard':
      Map map = settings.arguments as Map<dynamic, dynamic>;
      int? activeTab = map['activeTab'];
      return SlideRouteTransition(
        widget: Base(
          selectedIndexPage: activeTab ?? 0,
        ),
      );

    // Promise bottom-up sheet.
    case '/account/promise':
      return SlideRouteTransition(
        widget: const PromiseBottomUpSheet(),
        animationDirection: HelperConstant.bottomToTopDirection,
      );

    // PIN verification bottom-up sheet.
    case '/account/verify':
      Map map = settings.arguments as Map<dynamic, dynamic>;
      ProcessType? processType = map['processType'];
      RequestViewModel? requestModel = map['requestModel'];
      PluginController? pluginController = map['pluginController'];
      return SlideRouteTransition(
        widget: PinVerificationBottomUpSheet(
          process: processType,
          requestModel: requestModel,
          pluginController: pluginController,
        ),
        animationDirection: HelperConstant.bottomToTopDirection,
      );

    // Feedback-problem bottom-up sheet.
    case '/account/feedback/problem':
      return SlideRouteTransition(
        widget: const FeedbackBottomUpSheet(tabType: TabType.issue),
        animationDirection: HelperConstant.bottomToTopDirection,
      );

    // Feedback-generic bottom-up sheet.
    case '/account/feedback/idea':
      return SlideRouteTransition(
        widget: const FeedbackBottomUpSheet(tabType: TabType.generic),
        animationDirection: HelperConstant.bottomToTopDirection,
      );

    // Feedback-generic dashboard bottom-up sheet.
    case '/account/feedback/genericDashboard':
      return SlideRouteTransition(
        widget: const FeedbackBottomUpSheet(tabType: TabType.genericDashboard),
        animationDirection: HelperConstant.bottomToTopDirection,
      );

    // Feedback sent page.
    case '/account/feedback/done':
      return SlideRouteTransition(widget: const FeedbackDonePage());

    // Data exporting confirmation bottom-up sheet.
    case '/account/export-data/confirm':
      return SlideRouteTransition(
        widget: const DataExportingConfirmationBottomUpSheet(),
        animationDirection: HelperConstant.bottomToTopDirection,
      );

    // Data exporting bottom-up sheet.
    case '/account/export-data':
      String? password = settings.arguments as String?;
      return SlideRouteTransition(
        widget: DataExportingPageBottomUpSheet(password: password),
        animationDirection: HelperConstant.bottomToTopDirection,
      );

    // Data exporting done page.
    case '/account/export-data/done':
      String? filepath = settings.arguments as String?;
      return SlideRouteTransition(widget: DataExportingDonePage(filepath));

    // Account deletion confirmation bottom-up sheet.
    case '/account/deletion/confirm':
      return SlideRouteTransition(
        widget: const AccountDeletionConfirmationBottomUpSheet(),
        animationDirection: HelperConstant.bottomToTopDirection,
      );

    // Account deletion final confirmation bottom-up sheet.
    case '/account/deletion/confirm-final':
      return SlideRouteTransition(
        widget: const AccountDeletionFinalConfirmationBottomUpSheet(),
        animationDirection: HelperConstant.bottomToTopDirection,
      );

    // Add identity.
    case '/identity/add':
      return SlideRouteTransition(
        widget: const AddIdentity(),
        animationDirection: HelperConstant.bottomToTopDirection,
      );

    // Stewardship - info.
    case '/stewardship/owner/info':
      return SlideRouteTransition(
        widget: const OwnerInfoPageContent(),
        animationDirection: HelperConstant.bottomToTopDirection,
      );

    // Stewardship - loads identity addition guideline bottom-up sheet.
    case '/stewardship/identity-addition-guideline':
      return SlideRouteTransition(
        widget: const IdentityAdditionGuidelineBottomUpSheet(),
        animationDirection: HelperConstant.bottomToTopDirection,
      );

    // Stewardship - loads add identity bottom-up sheet.
    case '/stewardship/add-identity':
      return SlideRouteTransition(
        widget: const AddIdentityBottomUpSheet(),
        animationDirection: HelperConstant.bottomToTopDirection,
      );

    // Stewardship - owner defines access timing.
    case '/stewardship/owner/define-access-timing':
      return SlideRouteTransition(
        widget: const OwnerDefineAccessTimingPageContent(),
        animationDirection: HelperConstant.bottomToTopDirection,
      );

    // Stewardship - owner shares the QR code.
    case '/stewardship/owner/qr-code':
      Map map = settings.arguments as Map<dynamic, dynamic>;
      String? qrCodeLink = map['qrData'];
      List<int>? sharedAttributesIds = map['sharedAttributesIds'];
      DataAccessType? accessType = map['dataAccessType'];
      return SlideRouteTransition(
        widget: OwnerQrCodePageContent(
          qrCodeLink: qrCodeLink,
          sharedAttributesIds: sharedAttributesIds,
          dataAccessType: accessType,
        ),
        animationDirection: HelperConstant.bottomToTopDirection,
      );

    // Stewardship - awaits for sharing.
    case '/stewardship/awaiting':
      //TODO : may needs this validation through out the App Routing to check arguments availble
      Map map = settings.arguments != null ? settings.arguments as Map<dynamic, dynamic> : {};
      List<int>? sharedAttributesIds = map['sharedAttributesIds'] ?? [];
      DataAccessType? accessType = map['dataAccessType'];
      return SlideRouteTransition(
        widget: WaitingPageContent(
          sharedAttributesIds: sharedAttributesIds,
          dataAccessType: accessType,
        ),
      );

    // Stewardship - owner's agreement.
    case '/stewardship/owner/agreement':
      Map map = settings.arguments as Map<dynamic, dynamic>;
      GatewayViewModel? gatewayViewModel = map['gatewayViewModel'];
      return SlideRouteTransition(
        widget: OwnerAgreementPageContent(
          gatewayViewModel: gatewayViewModel,
        ),
        animationDirection: HelperConstant.bottomToTopDirection,
      );

    // Stewardship - steward sees cancellation.
    case '/stewardship/steward/cancellation':
      Map map = settings.arguments as Map<dynamic, dynamic>;
      String senderSurname = map['senderSurname'];
      return SlideRouteTransition(
        widget: CancellationPageContent(
          senderSurname: senderSurname,
        ),
        animationDirection: HelperConstant.bottomToTopDirection,
      );

    // Stewardship - stewards's agreement.
    case '/stewardship/steward/agreement':
      Map map = settings.arguments as Map<dynamic, dynamic>;
      GatewayViewModel? gatewayViewModel = map['gatewayViewModel'];
      return SlideRouteTransition(
        widget: StewardAgreementPageContent(
          gatewayViewModel: gatewayViewModel,
        ),
        animationDirection: HelperConstant.bottomToTopDirection,
      );

    // Stewardship - owner sends authorization.
    case '/stewardship/owner/sent':
      Map map = settings.arguments as Map<dynamic, dynamic>;
      String? receiverForename = map['receiverForename'];
      String? receiverSurname = map['receiverSurname'];
      return SlideRouteTransition(
        widget: OwnershipSentPageContent(
          receiverForename: receiverForename,
          receiverSurname: receiverSurname,
        ),
        animationDirection: HelperConstant.bottomToTopDirection,
      );

    // Stewardship - steward receives authorization.
    case '/stewardship/steward/received':
      Map map = settings.arguments as Map<dynamic, dynamic>;
      String? senderForename = map['senderForename'];
      String? senderSurname = map['senderSurname'];
      return SlideRouteTransition(
        widget: StewardshipReceivedPageContent(
          senderForename: senderForename,
          senderSurname: senderSurname,
        ),
        animationDirection: HelperConstant.bottomToTopDirection,
      );

    // Stewardship - owner authorizes.
    case '/stewardship/owner/authorize':
      Map map = settings.arguments as Map<dynamic, dynamic>;
      String? receiverName = map['receiverName'];
      int connectionId = map['connectionId'];
      return SlideRouteTransition(
        widget: OwnerAuthorizePageContent(
          receiverName: receiverName,
          connectionId: connectionId,
        ),
        animationDirection: HelperConstant.bottomToTopDirection,
      );

    // Stewardship - owner removes authorization.
    case '/stewardship/owner/remove':
      Map map = settings.arguments as Map<dynamic, dynamic>;
      String? receiverName = map['receiverName'];
      int connectionId = map['connectionId'];
      return SlideRouteTransition(
        widget: OwnerRemoveAuthorizationPageContent(
          connectionId: connectionId,
          receiverName: receiverName,
        ),
        animationDirection: HelperConstant.bottomToTopDirection,
      );

    // Stewardship - steward authorizes.
    case '/stewardship/steward/authorize':
      Map map = settings.arguments as Map<dynamic, dynamic>;
      String? senderName = map['senderName'];
      int connectionId = map['connectionId'];
      return SlideRouteTransition(
        widget: StewardAuthorizePageContent(
          senderName: senderName ?? '',
          connectionId: connectionId,
        ),
        animationDirection: HelperConstant.bottomToTopDirection,
      );

    // Stewardship - steward accesses.
    case '/stewardship/steward/access':
      Map map = settings.arguments as Map<dynamic, dynamic>;
      int connectionId = map['connectionId'];
      String? senderName = map['senderName'];
      bool? hasAccess = map['hasAccess'];
      return SlideRouteTransition(
        widget: StewardAccessPageContent(
          connectionId: connectionId,
          senderName: senderName,
          hasAccess: hasAccess ?? false,
        ),
        animationDirection: HelperConstant.bottomToTopDirection,
      );

    case '/vault/switch':
      return SlideRouteTransition(
        widget: const VaultsSwitchingBottomUpSheet(),
        animationDirection: HelperConstant.bottomToTopDirection,
      );

    // Stewardship - owner's agreement.
    case '/vault/switch/info':
      Map map = settings.arguments as Map<dynamic, dynamic>;
      int? contractId = map['contractId'];
      String? name = map['name'];
      return SlideRouteTransition(
        widget: VaultSwitchingInfoPageContent(contractId: contractId, name: name),
        animationDirection: HelperConstant.bottomToTopDirection,
      );

    // Data uploading bottom-up sheet.
    case 'data/upload':
      return SlideRouteTransition(
        widget: const DataUploadingBottomUpSheet(),
        animationDirection: HelperConstant.bottomToTopDirection,
      );

    // Data uploading options bottom-up sheet.
    case 'data/upload/options':
      return SlideRouteTransition(
        widget: const DataUploadingOptionsBottomUpSheet(),
        animationDirection: HelperConstant.bottomToTopDirection,
      );

    // QR code scanning page.
    case '/qr-scan':
      return SlideRouteTransition(widget: const QRPage());

    // Data request guideline bottom-up sheet.
    case '/data-request/guideline':
      return SlideRouteTransition(
        widget: DataRequestGuidelineBottomUpSheet(),
        animationDirection: HelperConstant.bottomToTopDirection,
      );

    /// Data request page.
    // When requestModel is provided, an existing data request is loaded.
    // When configUrl is provided, a data request will be created on the fly
    case '/data-request':
      Map map = settings.arguments as Map<dynamic, dynamic>;
      return SlideRouteTransition(
        widget: DataRequestPage(
          requestModel: map['model'],
          configUrl: map['configUrl'],
          isTourDataRequestOpen: map['isOpen'],
        ),
        animationDirection: HelperConstant.bottomToTopDirection,
      );

    // Data provider info page.
    case '/data-request/provider':
      RequestViewModel? model = settings.arguments as RequestViewModel?;
      return SlideRouteTransition(
        widget: DataProviderInfoPage(requestModel: model),
      );

    // Data retrieving page.
    case '/data-request/retrieve':
      Map map = settings.arguments as Map<dynamic, dynamic>;
      RequestViewModel model = map['model'];
      String pluginData = map['pluginData'];
      return SlideRouteTransition(
        widget: DataRequestRetrievalPage(
          requestModel: model,
          pluginData: pluginData,
        ),
      );

    // Data submitting page.
    case '/data-request/submit':
      RequestViewModel? model = settings.arguments as RequestViewModel?;
      return SlideRouteTransition(
        widget: DataRequestDonePage(requestViewModel: model),
      );

    // Data item page.
    case '/data/data':
      MyDataViewModel model = settings.arguments as MyDataViewModel;
      return SlideRouteTransition(
        widget: DataDetailsPage(model),
      );

    // Data category item page.
    case '/data/data-new':
      Map map = settings.arguments as Map<dynamic, dynamic>;
      String logoImg = map['logoImg'];
      Color logoColor = map['logoColor'];
      String title = map['title'];
      return SlideRouteTransition(
        widget: DataCategoryDetailPage(
          logoPath: logoImg,
          logoColour: logoColor,
          title: title,
        ),
      );

    // Data item info page.
    case '/data/data/info':
      FieldViewModel? model = settings.arguments as FieldViewModel?;
      return SlideRouteTransition(
        widget: DataDetailsListItemPage(model: model),
        animationDirection: HelperConstant.bottomToTopDirection,
      );

    // Data attribute info page.
    case '/data/data/info-new':
      Map map = settings.arguments as Map<dynamic, dynamic>;
      String logoImg = map['logoImg'];
      Color logoColor = map['logoColor'];
      String label = map['label'];
      return SlideRouteTransition(
        widget: DataAttributeDetailPage(
          logoPath: logoImg,
          logoColour: logoColor,
          title: label,
        ),
      );

    // Data deletion confirmation bottom-up sheet.
    case '/data/data/delete':
      Map map = settings.arguments as Map<dynamic, dynamic>;
      MyDataViewModel? dataViewModel = map['dataViewModel'];
      return SlideRouteTransition(
        widget: DataDeletionConfirmationBottomUpSheet(
          dataViewModel: dataViewModel,
        ),
      );

    // Data deletion error bottom-up sheet.
    case '/data/data/delete/error':
      Map map = settings.arguments as Map<dynamic, dynamic>;
      List<ContractItem>? contractList = map['contractList'];
      return SlideRouteTransition(
        widget: DataDeletionErrorBottomUpSheet(
          contractList: contractList,
        ),
      );

    // Contract page.
    case '/contracts/contract':
      Map map = settings.arguments as Map<dynamic, dynamic>;
      int selectedId = map['selectedId'];
      String fromUI = map['fromUI'];
      return SlideRouteTransition(
        widget: ContractPage(
          contractId: selectedId,
          fromUI: fromUI,
        ),
      );

    // Connection page.
    case '/contracts/connection':
      Map map = settings.arguments as Map<dynamic, dynamic>;
      int selectedId = map['selectedId'];
      String fromUI = map['fromUI'];
      String? userName = map['userName'] ?? '';
      String? sharedDatCount = map['sharedDatCount'];
      bool isSender = map['isSender'] ?? false;
      bool isReceiver = map['isReceiver'] ?? false;
      return SlideRouteTransition(
        widget: ConnectionPage(
          contractId: selectedId,
          fromUI: fromUI,
          userName: userName,
          sharedDatCount: sharedDatCount,
          isSender: isSender,
          isReceiver: isReceiver,
        ),
      );

    // Connection data sources selection page.
    case '/data/select/groups':
      Map arguments = settings.arguments as Map<dynamic, dynamic>;
      return SlideRouteTransition(
        widget: DataSelectGroupsPage(
          connectionId: arguments['connectionId'] ?? 0,
          onCancel: arguments['onCancel'],
          onShare: arguments['onShare'],
        ),
      );

    // Connection data attributes selection page.
    case '/data/select/attributes':
      Map arguments = settings.arguments as Map<dynamic, dynamic>;
      return SlideRouteTransition(
        widget: DataSelectAttributesPage(
          connectionId: arguments['connectionId'],
          onCancel: arguments['onCancel'],
          onShare: arguments['onShare'],
          provider: arguments['provider'],
          group: arguments['group'],
          logoUrl: arguments['logoUrl'],
          selectedAttributes: arguments['selectedAttributes'],
        ),
      );

    // Connection data attributes selection page.
    case '/data/select/summary':
      Map arguments = settings.arguments as Map<dynamic, dynamic>;
      return SlideRouteTransition(
        widget: DataSelectSummaryPage(
          connectionId: arguments['connectionId'],
          onCancel: arguments['onCancel'],
          onShare: arguments['onShare'],
          selectedAttributes: arguments['selectedAttributes'],
        ),
      );

    // Connection data attributes selection page.
    case '/data/select/summary/attributes':
      Map arguments = settings.arguments as Map<dynamic, dynamic>;
      return SlideRouteTransition(
        widget: DataSelectSummaryAttributesPage(
          connectionId: arguments['connectionId'],
          provider: arguments['provider'],
          group: arguments['group'],
          logoUrl: arguments['logoUrl'],
          selectedAttributes: arguments['selectedAttributes'],
        ),
      );

    // Connection deletion option bottom-up sheet.
    case '/contracts/connection/delete':
      Map map = settings.arguments as Map<dynamic, dynamic>;
      String? userName = map['userName'];
      int? dataCount = map['dataCount'];
      VoidCallback deleteCallback = map['deleteCallback'];
      return SlideRouteTransition(
        widget: ConnectionDeleteBottomUpSheet(
          userName: userName,
          dataCount: dataCount,
          deleteCallback: deleteCallback,
        ),
        animationDirection: HelperConstant.bottomToTopDirection,
      );

    // Connection my data deletion option bottom-up sheet.
    case '/contracts/connection/my-data/delete':
      Map map = settings.arguments as Map<dynamic, dynamic>;
      String? userName = map['userName'];
      String? dataCount = map['dataCount'];
      VoidCallback deleteCallback = map['deleteCallback'];
      return SlideRouteTransition(
        widget: ConnectionMyDataDeleteBottomUpSheet(
          userName: userName,
          dataCount: dataCount,
          deleteCallback: deleteCallback,
        ),
        animationDirection: HelperConstant.bottomToTopDirection,
      );

    // Connection shared data deletion option bottom-up sheet.
    case '/contracts/connection/shared-data/delete':
      Map map = settings.arguments as Map<dynamic, dynamic>;
      String? userName = map['userName'];
      String? dataCount = map['dataCount'];
      return SlideRouteTransition(
        widget: ConnectionSharedDataDeleteBottomUpSheet(
          userName: userName,
          dataCount: dataCount,
        ),
        animationDirection: HelperConstant.bottomToTopDirection,
      );

    // Contract info page.
    case '/contracts/contract/info':
      ContractDetailViewModel? detailViewModel = settings.arguments as ContractDetailViewModel?;
      return SlideRouteTransition(
        widget: ContractInfo(contractDetailViewModel: detailViewModel),
      );

    // Contract deletion confirmation bottom-up sheet.
    case '/contracts/contract/delete':
      Map map = settings.arguments as Map<dynamic, dynamic>;
      int? contractId = map['contractId'];
      String? fromUI = map['fromUI'];
      return SlideRouteTransition(
        widget: ContractDeletionConfirmationBottomUpSheet(
          contractId: contractId,
          fromUI: fromUI,
        ),
        animationDirection: HelperConstant.bottomToTopDirection,
      );

    // Error showing page.
    case '/error':
      Map map = settings.arguments as Map<dynamic, dynamic>;
      String? errorMsg = map['errorMsg'];
      String? errorDescription = map['errorDescription'];
      return SlideRouteTransition(
        widget: ErrorPage(
          errorText: errorMsg!,
          description: errorDescription!,
        ),
      );

    // Default - Intro page.
    default:
      return SlideRouteTransition(
        widget: const IntroPage(),
      );
  }
}
