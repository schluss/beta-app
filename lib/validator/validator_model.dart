/*
 * @author Asanka Anthony
 * @email aanthony@yukon.lk
 * @create date 2020-05-27 18:05:01
 */

/// Shares response validation status.
class ValidatorModel {
  bool repetitionStatus;
  bool additionStatus;
  bool isPincodeValid;

  ValidatorModel({this.repetitionStatus = true, this.additionStatus = true, this.isPincodeValid = false});

  @override
  String toString() {
    return 'ValidatorModel{repetitionStatus: $repetitionStatus, additionStatus: $additionStatus, isPincodeValid: $isPincodeValid,}';
  }
}
