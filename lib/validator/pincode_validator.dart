/*
 * @author Asanka Anthony
 * @email aanthony@yukon.lk
 * @create date 2020-05-08 18:05:01
 * @modify date 2020-05-27 18:05:01
 */

import 'dart:core';

import 'package:logger/logger.dart';
import 'package:schluss_beta_app/validator/common_validation.dart';
import 'package:schluss_beta_app/validator/validator_model.dart';

/// Rules:
/// Lengths of 5 digits: A pin code must always be exact 5 digits long.
/// No repeating: a digit can exist twice in a pin code but not next to each other.
/// OK: 10480 not ok: 10048 / 00000.
/// No sequences: only two numbers may be in sequence.
/// OK: 12548 not ok: 12345 / 12300 / 98765.

/// UPDATED ::::
/// The updated restrictions will be as follows, the user should not be able to type a pin code with:
/// five of the same number; like: 11111, 77777, 00000
/// if so: the ‘no repeat’ error must be triggered
/// five numbers counting up or downwards, like: 12345 / 87654 / 43210
/// if so: the 'no addition' error must be triggered.
class PincodeValidator {
  final String pincode;
  final int pincodeLength;
  PincodeValidator(
    this.pincode, {
    this.pincodeLength = 5,
  });

  /// Creates instance of small, easy to use and extensible logger which prints beautiful logs.
  final _logger = Logger();

  Future<ValidatorModel> validate() async {
    ValidatorModel validatorModel = ValidatorModel();

    if (!CommonValidation().isNumeric(pincode)) {
      return Future.error('Pincode contains invalid characters');
    }

    if (pincode.trim().length == pincodeLength) {
      // Pincode only check length =5.
      // Extract digit from pin code string.
      List<String> list = pincode.split('');

      _validateDuplicatedDigits(list, validatorModel);

      if (_validateIncrementedSequence(list, validatorModel) &&
          _validateDecrementedSequence(
            list,
            validatorModel,
          )) {
        validatorModel.additionStatus = true;
      } else {
        validatorModel.additionStatus = false;
      }

      if (pincodeLength == pincode.length && validatorModel.additionStatus && validatorModel.repetitionStatus) {
        validatorModel.isPincodeValid = true;
      } else {
        validatorModel.isPincodeValid = false;
      }
    } else {
      // Pincode length != 5.
      validatorModel.additionStatus = true;
      validatorModel.repetitionStatus = true;
      validatorModel.isPincodeValid = true;
      validatorModel.isPincodeValid = false;
    }
    _logger.i('Pincode validation done!');

    return Future.value(validatorModel);
  }

  // Validates repeated digits.
  void _validateDuplicatedDigits(
    List<String> pincodeList,
    ValidatorModel validatorModel,
  ) {
    // Get first pin code value.
    String firstDigit = pincodeList[0];
    validatorModel.repetitionStatus = false;

    for (var i = 0; i < pincodeList.length; i++) {
      if (firstDigit != pincodeList[i]) {
        validatorModel.repetitionStatus = true;
      }
    }
  }

  /// Checks incremented[+] sequence in digits.
  /// Not allowed: 12345
  bool _validateIncrementedSequence(
    List<String> pincodeList,
    ValidatorModel validatorModel,
  ) {
    for (var i = 0; i < pincodeList.length - 1; i++) {
      int currentDigit = int.parse(pincodeList[i]);
      // Next digit.
      int nextDigit = int.parse(pincodeList[i + 1]);

      if (currentDigit + 1 != nextDigit) {
        // If first digit not repeated valid pin code.
        return true;
      }
    }
    //  Validation failed.
    return false;
  }

  /// Checks incremented[+] sequence in digits.
  /// Not allowed: 54321
  bool _validateDecrementedSequence(
    List<String> pincodeList,
    ValidatorModel validatorModel,
  ) {
    for (var i = 1; i < pincodeList.length; i++) {
      int currentDigit = int.parse(pincodeList[i]);
      // Next digit.
      int prvDigit = int.parse(pincodeList[i - 1]);

      if (currentDigit != prvDigit - 1) {
        // If first digit not repeated valid pin code.
        return true;
      }
    }
    //  Validation failed.
    return false;
  }

  @override
  String toString() {
    return 'PincodeValidator(pincodeLength: $pincodeLength, pin code: $pincode)';
  }
}
