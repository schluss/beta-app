/*
 * @author Asanka Anthony
 * @email aanthony@yukon.lk
 * @create date 2020-05-09 23:41:00
 * @modify date 2020-05-09 23:41:00
 */
/// Adds common schluss validations.
class CommonValidation {
  /// Checks string contains only numbers.
  bool isNumeric(String? str) {
    if (str == null) {
      return false;
    }
    return double.tryParse(str) != null;
  }
}
