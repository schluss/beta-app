import 'package:logger/logger.dart';
import 'package:schluss_beta_app/business/impl/notification_bl_impl.dart';
import 'package:schluss_beta_app/singleton_injector.dart';
import 'package:schluss_beta_app/view_model/notification_view_model.dart';

class NotificationController {
  final NotificationBLImpl _notificationBL = singletonInjector<NotificationBLImpl>();
  final Logger _logger = singletonInjector<Logger>();

  /// Gets tips.
  Future<List<NotificationViewModel>> getTips({int connectionId = 0}) async {
    List<NotificationViewModel> tipsList = [];

    try {
      tipsList = await _notificationBL.getNotifications(
        notificationType: NotificationType.tip,
        connectionId: connectionId,
      );
    } catch (e) {
      _logger.e(e);
    }

    return Future.value(tipsList);
  }
}
