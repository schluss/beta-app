import 'dart:async';

import 'package:flutter/material.dart';
import 'package:one_context/one_context.dart';
import 'package:schluss_beta_app/business/impl/account_bl_impl.dart';
import 'package:schluss_beta_app/controller/contract_controller.dart';
import 'package:schluss_beta_app/services/impl/task_service.dart';
import 'package:schluss_beta_app/singleton_injector.dart';
import 'package:schluss_beta_app/view_model/login_result_view_model.dart';
import 'package:schluss_beta_app/view_model/settings_view_model.dart';
import 'package:schluss_beta_app/view_model/vault_switch_item_view_model.dart';
import 'package:session_next/session_next.dart';

class AccountController {
  final AccountBLImpl _accountBL = singletonInjector<AccountBLImpl>();

  final ContractController _contractController = ContractController();

  /// Generates the Schluss vault.
  Future<bool> generateSchlussVault(String password) async {
    return await _accountBL.generateSchlussVault(password);
  }

  /// Updates Schluss vault with new pin code.
  Future<bool> updateSchlussVaultKey(
    String? password,
    String? oldPassword,
  ) async {
    return await _accountBL.updateSchlussVaultKey(password, oldPassword);
  }

  /// Checks if the provided pin code is correct
  Future<bool> validatePin(String pincode) async {
    return _accountBL.validatePin(pincode);
  }

  /// Logs in the user
  Future<bool> validateLogin(String pincode) async {
    LoginResultViewModel? loginResult = await _accountBL.validateLogin(pincode);

    if (loginResult == null) {
      return false;
    }

    SessionNext().set<String>('privateKey', loginResult.privateKey);
    SessionNext().set<String>('publicKey', loginResult.publicKey);
    SessionNext().set<String>('randomKey', loginResult.randomKey);

    // From here, the session is set!

    // load switch connections (can only be done from here, because the crypto keys are needed from the session)
    SessionNext().set<List<VaultSwitchItemViewModel>>('switchConnections', await _contractController.getStewardshipAssignedConnections());
    // Start processing background tasks.
    singletonInjector<TaskService>().processAndRetryFailedTasks();

    return true;
  }

  /// Validates vault availability.
  Future<bool> validateVaultAvailability() async {
    return Future.value(await _accountBL.validateVaultAvailability());
  }

  /// Validates login attempts.
  Future<int> validateLoginAttempts() async {
    return Future.value(await _accountBL.validateLoginAttempts());
  }

  /// Removes all vault data.
  Future<bool> deleteVault() async {
    bool status = await _accountBL.deleteVault();

    // Remove all user session parameters.
    if (status) {
      SessionNext().destroy();
    }

    // Reset all the stored singletons.
    await resetSingletonInjector();

    return Future.value(status);
  }

  /// Logs out from the Schluss Vault.
  void logout() async {
    // Remove session.
    SessionNext().destroy();

    // Navigate to logout screen with calling [pushNamedAndRemoveUntil]
    // method - user will not be able to navigate back to the account
    // page with [pushNamed] method the user was able to navigate back to
    // the account page which should not be possible.
    OneContext().pushNamedAndRemoveUntil(
      '/account/login',
      (Route<dynamic> route) => false,
    );

    // Reset all the stored singletons.
    await resetSingletonInjector();
  }

  /// Gets settings.
  Future<SettingsViewModel> getSettings() async {
    return Future.value(await (_accountBL.getSettings()));
  }

  /// Gets the global ENV mode setting.
  Future<String> getEnvMode() async {
    return Future.value(await _accountBL.getEnvMode());
  }

  /// Changes stewardship mode from the UI.
  Future<bool> changeStewardshipMode(bool? isStewardshipModeOn) async {
    return Future.value(await _accountBL.changeStewardshipMode(
      isStewardshipModeOn,
    ));
  }

  /// Changes data sharing mode from the UI.
  Future<bool> changeDataSharingMode(bool? isDataSharingModeOn) async {
    return Future.value(await _accountBL.changeDataSharingMode(
      isDataSharingModeOn,
    ));
  }

  /// Changes test-mode from the UI.
  Future<bool> changeTestMode(bool? isTestModeOn) async {
    return Future.value(await _accountBL.changeTestMode(isTestModeOn));
  }

  /// Changes vault owner status in settings store.
  Future<bool> changeVaultOwner(int? contractId) async {
    return Future.value(await _accountBL.changeVaultOwner(
      contractId,
    ));
  }

  /// Changes tour_data_request_on_open mode from the UI.
  Future<bool> changeTourDataRequestOpen(bool isTourDataRequestOpen) async {
    return Future.value(await _accountBL.changeTourDataRequestOpen(
      isTourDataRequestOpen,
    ));
  }

  /// Exports vault data to a zip file.
  Future<String> exportVaultData(String? passphrase) {
    return _accountBL.exportVaultData(passphrase);
  }

  /// Returns whether the user is in its own vault or not
  bool isInOwnVault() {
    return _accountBL.isInOwnVault();
  }

  /// Returns the full name of the vault the user is currently in
  String getCurrentVaultName() {
    return _accountBL.getCurrentVaultName();
  }

  /// Validates stewardship mode.
  Future<bool> isStewardshipModeOn() async {
    return _accountBL.isStewardshipModeOn();
  }

  /// Validates data sharing mode.
  Future<bool> isDataSharingModeOn() async {
    return _accountBL.isDataSharingModeOn();
  }

  /// Validates test mode.
  Future<bool> isTestModeOn() async {
    return _accountBL.isTestModeOn();
  }

  Future<void> createIdentityTooltip() async {
    return _accountBL.createVerifyIdentityTip();
  }
}
