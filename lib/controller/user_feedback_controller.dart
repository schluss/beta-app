import 'dart:convert';
import 'dart:io';

import 'package:flutter_dotenv/flutter_dotenv.dart';
import 'package:intl/intl.dart';
import 'package:logger/logger.dart';
import 'package:schluss_beta_app/services/impl/payload_service_impl.dart';
import 'package:schluss_beta_app/singleton_injector.dart';
import 'package:schluss_beta_app/util/logger_util.dart';
import 'package:schluss_beta_app/util/package_info_util.dart';
import 'package:schluss_beta_app/view_model/settings_view_model.dart';

class UserFeedbackController {
  final Logger _logger = singletonInjector<Logger>();
  final LoggerUtil _loggerUtil = LoggerUtil();

  SettingsViewModel? settings;

  /// Sets current date time of the device.
  Map<String, String> _getNowDateTime() {
    return {
      'dateTime': DateFormat('yyyy-MM-dd HH:mm:ss').format(DateTime.now()),
    };
  }

  /// Sets the platform of the device.
  Map<String, String> _getPlatform() {
    return {'platform': Platform.operatingSystem};
  }

  /// Sets the version of the app into a map.
  Map<String, String> _getVersion(versionNo) {
    return {'version': versionNo};
  }

  /// Sets feedback text into a map.
  Map<String, String?> _getFeedbackType(feedbackType) {
    return {'type': feedbackType};
  }

  /// Sets feedback text into a map.
  Map<String, String?> _getFeedbackContent(feedbackText) {
    return {'feedback': feedbackText};
  }

  /// Sets error logs into a map.
  Future<Map> _getLogs() async {
    List<String> loggedErrors;
    Map errorLogs;

    loggedErrors = await _loggerUtil.readLogFileAsLines();

    errorLogs = {'logs': Map.fromIterable(loggedErrors, value: (v) => v[0])};

    return errorLogs;
  }

  /// Combines body data.
  Future<String> _getFeedbackData(
    String versionNo,
    String? feedbackType,
    String? feedbackText,
  ) async {
    Map dateTime = _getNowDateTime();
    Map platform = _getPlatform();
    Map version = _getVersion(versionNo);
    Map feedbackTypeText = _getFeedbackType(feedbackType);
    Map feedbackContent = _getFeedbackContent(feedbackText);
    Map feedbackInfo;
    Map logs;

    logs = await _getLogs();

    feedbackInfo = {
      ...dateTime,
      ...platform,
      ...version,
      ...feedbackTypeText,
      ...feedbackContent,
      ...logs,
    };

    return json.encode(feedbackInfo);
  }

  /// Sends feedback data with or without logs.
  Future sendUserFeedback(
    String? feedbackType,
    String? feedbackText,
  ) async {
    String? endpoint;
    String? token;
    String version;
    String feedbackData;

    endpoint = dotenv.env['FEEDBACK_API_URL'];
    token = dotenv.env['FEEDBACK_API_KEY'];
    version = await PackageInfoUtil.getVersion();

    feedbackData = await _getFeedbackData(
      version,
      feedbackType,
      feedbackText,
    );
    _logger.i(feedbackData);

    return await PayloadServiceImpl().submitPayloadData(
      endpoint!,
      token,
      feedbackData,
    );
  }
}
