import 'package:logger/logger.dart';
import 'package:schluss_beta_app/business/impl/contract_manager_bl_impl.dart';
import 'package:schluss_beta_app/singleton_injector.dart';
import 'package:schluss_beta_app/view_model/contract_detail_view_model.dart';
import 'package:schluss_beta_app/view_model/contract_providers_view_model.dart';
import 'package:schluss_beta_app/view_model/field_view_model.dart';
import 'package:schluss_beta_app/view_model/vault_switch_item_view_model.dart';

class ContractController {
  final ContractManagerBLImpl _contractManagerBL = singletonInjector<ContractManagerBLImpl>();
  final Logger _logger = singletonInjector<Logger>();

  /// Decrypts contract details.
  Future<ContractDetailViewModel> getContractDetailsDecrypted(
    ContractDetailViewModel? contractDetailViewModel,
  ) async {
    return await _contractManagerBL.getContractDetailsDecrypted(
      contractDetailViewModel,
    );
  }

  /// Hides finished contract notifications.
  Future<void> hideContractNotifications() async {
    try {
      await _contractManagerBL.hideContractNotifications();
    } catch (e) {
      _logger.e('Error Occurred $e');
    }
  }

  /// Removes contract by [contractId].
  Future<dynamic> deleteContractById(int? contractId) async {
    return _contractManagerBL.deleteContractById(contractId);
  }

  /// Removes connection by [contractId].
  Future<dynamic> deleteConnectionById(int? contractId) async {
    return _contractManagerBL.deleteConnectionById(contractId);
  }

  /// Removes connection by [clientKey].
  Future<void> deleteConnectionByClientKey(String? clientKey) async {
    return _contractManagerBL.deleteConnectionByClientKey(clientKey);
  }

  /// Gets contracts by ID.
  Future<ContractProvidersViewModel?> getContractById(int? contractId) async {
    return await _contractManagerBL.getContractById(contractId);
  }

  /// Gets shared data with vault.
  Future<List<FieldViewModel>> getSharedDataWithVault(int? contractId) async {
    return await _contractManagerBL.getSharedDataWithVault(contractId!);
  }

  /// Gets shared data from vault.
  Future<List<FieldViewModel>> getSharedDataFromVault(int? contractId) async {
    return await _contractManagerBL.getSharedDataFromVault(contractId!);
  }

  /// Deletes shared attribute from vault.
  Future<bool> deleteOutgoingSharedAttribute(String gatewayShareId, int contractId, int attributeId) async {
    return await _contractManagerBL.deleteOutgoingShare(
      gatewayShareId,
      contractId,
      attributeId,
    );
  }

  Future<bool> deleteStewardship(int connectionId) async {
    return _contractManagerBL.deleteStewardship(connectionId);
  }

  /// Deletes shared attribute with vault.
  Future<bool> deleteIncomingSharedAttribute(String gatewayShareId) async {
    return await _contractManagerBL.deleteIncomingShare(gatewayShareId);
  }

  /// Gets stewardship connection with this vault.
  Future<List<VaultSwitchItemViewModel>> getStewardshipAssignedConnections() async {
    return await _contractManagerBL.getStewardshipAssignedConnections();
  }
}
