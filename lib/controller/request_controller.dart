import 'package:logger/logger.dart';
import 'package:schluss_beta_app/business/impl/request_manager_bl_impl.dart';
import 'package:schluss_beta_app/singleton_injector.dart';
import 'package:schluss_beta_app/view_model/request_view_model.dart';

class RequestController {
  final RequestManagerBLImpl _requestManagerBL = singletonInjector<RequestManagerBLImpl>();
  final Logger _logger = singletonInjector<Logger>();

  /// Gets the pending request list.
  Future<List<RequestViewModel>> getPendingRequests() async {
    List<RequestViewModel>? pendingRequestList = [];
    try {
      pendingRequestList = await _requestManagerBL.getPendingRequestList();

      return Future.value(pendingRequestList);
    } catch (e) {
      _logger.e('Error occurred: $e');

      return Future.value(pendingRequestList);
    }
  }

  /// Removes the request.
  ///
  /// Currently updates request status to deleted.
  Future<void> removeRequest(RequestViewModel model) async {
    try {
      await _requestManagerBL.removeRequest(model);
    } catch (e) {
      _logger.e('Error occurred: $e');

      return Future.error(e);
    }
  }

  /// Completes the store-only no-contracts data request.
  Future completeLocalRequest(RequestViewModel? model) async {
    return _requestManagerBL.completeLocalRequest(model);
  }
}
