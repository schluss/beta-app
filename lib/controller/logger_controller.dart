import 'dart:async';
import 'dart:convert';
import 'dart:io';

import 'package:flutter_dotenv/flutter_dotenv.dart';
import 'package:intl/intl.dart';
import 'package:logger/logger.dart';
import 'package:schluss_beta_app/services/impl/payload_service_impl.dart';
import 'package:schluss_beta_app/singleton_injector.dart';
import 'package:schluss_beta_app/util/device_info_util.dart';
import 'package:schluss_beta_app/util/logger_util.dart';
import 'package:schluss_beta_app/util/package_info_util.dart';

class LoggerController {
  final Logger _logger = singletonInjector<Logger>();
  final LoggerUtil _loggerUtil = LoggerUtil();
  final DeviceInfoUtil _deviceInfoUtil = DeviceInfoUtil();

  /// Sets current [dateTime] of the device.
  Map<String, String> getNowDateTime() {
    return {
      'dateTime': DateFormat('yyyy-MM-dd HH:mm:ss').format(DateTime.now()),
    };
  }

  /// Sets the platform of the device.
  Map<String, String> getPlatform() {
    return {'platform': Platform.operatingSystem};
  }

  /// Sets the version of the app into a map.
  Map<String, String> getVersion(versionNo) {
    return {'version': versionNo};
  }

  /// Gets device info.
  Future<Map<String, dynamic>?> getDeviceInfo() async {
    return await _deviceInfoUtil.getDeviceInfo();
  }

  /// Sets error logs into a map.
  Future<Map> getErrorLogs() async {
    List<String> loggedErrors;
    Map errorLogs;

    loggedErrors = await _loggerUtil.readLogFileAsLines();

    errorLogs = {'logs': Map.fromIterable(loggedErrors, value: (v) => v[0])};

    return errorLogs;
  }

  /// Combines body data.
  Future<String> getLoggerData(String versionNo) async {
    Map dateTime = getNowDateTime();
    Map platform = getPlatform();
    Map version = getVersion(versionNo);
    var deviceInfo = await getDeviceInfo() ?? <String, dynamic>{};
    Map errorLogs;
    Map logInfo;

    errorLogs = await getErrorLogs();

    logInfo = {
      ...dateTime,
      ...platform,
      ...version,
      ...deviceInfo,
      ...errorLogs,
    };

    return json.encode(logInfo);
  }

  /// Sends the saved log file contents to the server.
  Future<bool> sendErrorLogFile() async {
    String? endpoint;
    String? token;
    String version;
    String loggerData;

    endpoint = dotenv.env['LOG_API_URL'];
    token = dotenv.env['LOG_API_KEY'];
    version = await PackageInfoUtil.getVersion();

    loggerData = await getLoggerData(version); // Gets body data.
    _logger.i(loggerData);

    bool status = await PayloadServiceImpl().submitPayloadData(
      endpoint!,
      token,
      loggerData,
    );

    return status;
  }
}
