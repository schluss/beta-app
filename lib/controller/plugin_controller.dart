import 'package:flutter/cupertino.dart';
import 'package:logger/logger.dart';
import 'package:schluss_beta_app/business/impl/request_manager_bl_impl.dart';
import 'package:schluss_beta_app/business/interface/plugin_manager_bl.dart';
import 'package:schluss_beta_app/singleton_injector.dart';
import 'package:schluss_beta_app/util/common_util.dart';
import 'package:schluss_beta_app/util/error_handler_util.dart';
import 'package:schluss_beta_app/view_model/provider_config_view_model.dart';
import 'package:schluss_beta_app/view_model/request_view_model.dart';
import 'package:session_next/session_next.dart';

class PluginController {
  final Logger _logger = singletonInjector<Logger>();
  final CommonUtil _commonUtil = CommonUtil();
  final ErrorHandlerUtil _errorHandler = ErrorHandlerUtil();
  RequestViewModel? requestModel;
  ProviderConfigViewModel? model;
  late PluginManagerBL plugin;

  /// Executes plugin dynamically using [singletonInjector] named class creation.
  ///
  /// Each plugin should have different implementations. Depend on the name
  /// implementation class will executed.
  // TODO: for testing need to review and decide which method to keep.

  // TODO: need to change the architecture as plugin-me. For keeping the process
  //  of the plug-ins generic; user-input field values return back to
  //  [callbackFromPlugin] and again pass them back to the plugin side.
  void executePluginDynamic(BuildContext context, RequestViewModel requestModel, [bool mounted = true]) async {
    try {
      int? providerIndex;

      providerIndex = requestModel.getCurrentSelectedIndex();
      model = requestModel.getProviderList().elementAt(providerIndex!);
      this.requestModel = requestModel;

      // Pause the session.
      SessionNext().pause();

      // Check internet availability.
      var internetStatus = await _commonUtil.checkInternetConnectivity();

      if (!mounted) return;

      if (!internetStatus) {
        _errorHandler.handleError(
          'no-internet',
          true,
          context,
          'no-internet',
        );

        return;
      }

      // Plugin executes dynamically depend on plugin name in settings URL.
      plugin = singletonInjector.get(instanceName: model!.plugin);
      plugin.executePlugin(
        context,
        requestModel,
        providerIndex,
        callbackFromPlugin,
      );
    } catch (e) {
      _logger.e('Plugin Not Found: $e');
    }
  }

  /// Callbacks method to get response from the plugin after download/retrieval complete.
  void callbackFromPlugin(
    String pluginName,
    String pluginData,
    BuildContext context,
  ) async {
    _logger.i('Received callback with data from plugin $pluginName');

    if (requestModel != null && pluginData != '') {
      Navigator.of(context).pushNamedAndRemoveUntil(
        '/data-request/retrieve',
        ModalRoute.withName('/data-request'),
        arguments: {
          'model': requestModel,
          'pluginData': pluginData,
        },
      );
    }
  }

  /// Gets extracted data from plugin using downloaded file path.
  Future<RequestViewModel> getExtractedContent(
    RequestViewModel requestModel,
    String? pluginData,
  ) async {
    int? providerIndex;

    providerIndex = requestModel.getCurrentSelectedIndex();
    model = requestModel.getProviderList().elementAt(providerIndex!);

    plugin = singletonInjector.get(instanceName: model!.plugin);

    // Execute the plugin dynamically depend on the plugin name.
    var result = await plugin.getData(
      requestModel,
      pluginData!,
    );

    // Resume session
    SessionNext().resume();

    return result;
  }

  /// Submits the payload.
  Future<bool> submitPayload(RequestViewModel? model) {
    return RequestManagerBLImpl().submitPayload(model);
  }

  /// Returns progress value.
  Stream<int> getProgressValue(RequestViewModel requestViewModel) {
    model = requestViewModel.getProviderList().elementAt(
          requestViewModel.getCurrentSelectedIndex()!,
        );

    plugin = singletonInjector.get(instanceName: model!.plugin);

    return plugin.getProgressValue(requestViewModel);
  }
}
