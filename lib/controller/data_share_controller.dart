import 'package:schluss_beta_app/business/impl/data_share_bl_impl.dart';
import 'package:schluss_beta_app/business/impl/gateway_bl_impl.dart';
import 'package:schluss_beta_app/singleton_injector.dart';
import 'package:schluss_beta_app/view_model/gateway_viewmodel.dart';

class DataShareController {
  final DataShareBLImpl _dataShareBL = singletonInjector<DataShareBLImpl>();
  final GatewayBLImpl _gatewayBL = singletonInjector<GatewayBLImpl>();

  Future<String> generateQRLink(String endpointUrl) async {
    return await _dataShareBL.generateQRLink(endpointUrl);
  }

  Future<GatewayViewModel?> processQRData(String? qrData) async {
    return await _dataShareBL.processQRData(qrData);
  }

  Future<bool> validateSessionId(int? sessionId) async {
    return await _dataShareBL.validateSessionId(sessionId);
  }

  Future<bool> acceptingDataShareRequest(
    GatewayViewModel? gatewayViewModel,
  ) async {
    return await _dataShareBL.acceptingDataShareRequest(
      gatewayViewModel,
    );
  }

  Future<bool> acceptingDataSendRequest(GatewayViewModel? gatewayViewModel) async {
    return await _dataShareBL.acceptingDataSendRequest(gatewayViewModel);
  }

  Future<GatewayViewModel?> processDataAcceptResponse(GatewayViewModel? gatewayViewModel) async {
    return await _dataShareBL.processDataAcceptResponse(gatewayViewModel);
  }

  Future<bool> releaseTrigger(int connectionId) {
    return _gatewayBL.releaseTrigger(connectionId);
  }
}
