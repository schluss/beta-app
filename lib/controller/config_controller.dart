import 'dart:convert';

import 'package:logger/logger.dart';
import 'package:schluss_beta_app/business/impl/qr_config_bl_impl.dart';
import 'package:schluss_beta_app/business/impl/request_manager_bl_impl.dart';
import 'package:schluss_beta_app/constants/url_constants.dart';
import 'package:schluss_beta_app/services/impl/payload_service_impl.dart';
import 'package:schluss_beta_app/singleton_injector.dart';
import 'package:schluss_beta_app/view_model/data_request_config_view_model.dart';
import 'package:schluss_beta_app/view_model/request_view_model.dart';

class ConfigurationController {
  final QRConfigurationBLImpl _configBL = singletonInjector<QRConfigurationBLImpl>();
  final RequestManagerBLImpl _requestManagerBL = singletonInjector<RequestManagerBLImpl>();
  final Logger logger = singletonInjector<Logger>();

  /// Processes the configuration link first time and stores the request.
  Future<RequestViewModel> processConfigurationLink(String configUrl) async {
    DataRequestConfigViewModel dataRequest;
    RequestViewModel? requestModel;
    RequestViewModel? requestViewModel;

    Map<String, String> post;
    List<String> pathsList;
    String scope;
    String settingsUrl;
    String token;
    // When scan same QR twice or more.
    bool isDuplicated = false;

    try {
      pathsList = Uri.parse(configUrl).pathSegments;

      scope = pathsList.elementAt(UrlConstants.dataRequestScopeIndex);
      settingsUrl = pathsList.elementAt(UrlConstants.dataRequestConfigUrlIndex);
      token = pathsList.elementAt(UrlConstants.dataRequestTokenIndex);
      // Find same QR scan before.
      requestModel = requestViewModel = await _requestManagerBL.findDuplicateQR(token, scope, settingsUrl);

      // Check QR code duplicate or not.
      if (requestViewModel != null) {
        // Set the duplicate status.
        isDuplicated = true;
      }

      dataRequest = await _configBL.processConfigData(configUrl, scope, settingsUrl);
    } catch (e) {
      return Future.error(e);
    }

    if (!isDuplicated) {
      try {
        requestModel = RequestViewModel.fromUrl(configUrl, dataRequest);
        requestModel = await _requestManagerBL.storeRequest(requestModel);
        // Compare request config fields with provider export fields
        // & remove if request fields not available in provider export fields.
        // [https://schluss.atlassian.net/browse/BETA-2226].
        await _configBL.matchWithProviderConfig(dataRequest);
        // Set providers.
        requestModel.setProviderList(dataRequest.providers);
      } catch (e) {
        return Future.error(e);
      }
    } else {
      // Exiting request already have configurations so we can setup providers here.
      // Set providers.
      requestModel!.setProviderList(dataRequest.providers);
    }

    // Send status update to api, if there is one
    if (dataRequest.statusUrl != '') {
      try {
        post = {
          'state': 'connecting',
        };
        await PayloadServiceImpl().updateStatus(dataRequest.statusUrl!, jsonEncode(post), requestModel.token);
      } catch (e) {
        return Future.error(e);
      }
    }

    if (requestModel.id != null) {
      return Future.value(requestModel);
    } else {
      return Future.error(Exception('Request could not be stored'));
    }
  }

  /// Retrieves [DataRequestConfigViewModel] from processing [RequestViewModel] settings URL.
  Future<DataRequestConfigViewModel> retrieveConfiguration(RequestViewModel? model) async {
    try {
      return await _configBL.retrieveConfigData(model);
    } catch (e) {
      logger.e(e);
      return Future.error(Exception('Retrieving configuration was unsuccessful'));
    }
  }

  /// Checks and (pre)loads the attribute values.
  Future<void> preLoadDataIfAlreadyExists(RequestViewModel? requestViewModel) async {
    try {
      await _configBL.preLoadDataIfAlreadyExists(requestViewModel);
    } catch (e) {
      return Future.error(e);
    }
  }
}
