import 'package:logger/logger.dart';
import 'package:schluss_beta_app/business/impl/my_data_bl_impl.dart';
import 'package:schluss_beta_app/singleton_injector.dart';
import 'package:schluss_beta_app/view_model/contract_recent_item.dart';
import 'package:schluss_beta_app/view_model/field_view_model.dart';
import 'package:schluss_beta_app/view_model/my_data_view_model.dart';

class MyDataController {
  final MyDataBLImpl _myDataBL = singletonInjector<MyDataBLImpl>();
  final Logger _logger = singletonInjector<Logger>();

  Future<List<FieldViewModel>> getData() async {
    List<FieldViewModel> dataList = [];

    try {
      dataList = await _myDataBL.getData();
    } catch (e) {
      _logger.e(e);
    }

    if (dataList.isEmpty) {
      _logger.i('No Data found.');
    }
    return Future.value(dataList);
  }

  /// Gets recent data.
  Future<List<MyDataViewModel>> getRecentData() async {
    List<MyDataViewModel> dataList = [];

    try {
      dataList = await _myDataBL.getStoredData();
    } catch (e) {
      _logger.e(e);
    }

    if (dataList.isEmpty) {
      _logger.i('No Recent Data found.');
    }
    return Future.value(dataList);
  }

  /// Gets all attributes inside [provider] and [group].
  Future<List<FieldViewModel>> getByProviderAndGroup(
    String provider,
    String group,
  ) async {
    List<FieldViewModel> dataList = [];

    try {
      dataList = await _myDataBL.getByProviderAndGroup(provider, group);
    } catch (e) {
      _logger.e(e);
    }

    if (dataList.isEmpty) {
      _logger.i('Attributes found');
    }
    return Future.value(dataList);
  }

  /// Uploads data.
  Future<bool> uploadData(String filePath) async {
    _logger.i('Uploading Data ');

    bool status;

    try {
      status = await _myDataBL.uploadData(filePath);
    } catch (e) {
      status = false;
      _logger.e(e);
    }

    return Future.value(status);
  }

  /// Generates file and returns file path.
  Future<String> generateFile(FieldViewModel data) async {
    try {
      return await _myDataBL.generateFile(data);
    } catch (e) {
      _logger.e(e);
      return Future.error(e);
    }
  }

  /// Validates data attributes with contracts.
  Future<List<ContractItem>> validateDataWithContracts(
    MyDataViewModel? dataViewModel,
  ) async {
    List<ContractItem> list = [];

    try {
      list = await _myDataBL.validateDataWithContracts(dataViewModel);
    } catch (e) {
      _logger.e(e);
      return Future.error(e);
    }

    return Future.value(list);
  }

  /// Deletes attribute data and pending request data.
  Future<bool> deleteData(MyDataViewModel? dataViewModel) async {
    bool status = false;

    try {
      status = await _myDataBL.deleteData(dataViewModel);
    } catch (e) {
      _logger.e(e);
      return Future.error(e);
    }

    return Future.value(status);
  }

  /// Decrypts data details.
  Future<FieldViewModel> getDataDetailsDecrypted(
    FieldViewModel fieldViewModel,
  ) async {
    return await _myDataBL.getDataDetailsDecrypted(fieldViewModel);
  }

  /// Gets attribute value.
  Future<String> getPersonalInfoValue(
    String provider,
    String attributeName,
  ) async {
    String attributeValue = await _myDataBL.getPersonalInfoValue(
      provider,
      attributeName,
    );

    return attributeValue;
  }
}
