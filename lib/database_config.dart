/*
 * @author Manuja Jayawardana
 * @email mjayawardana@yukon.lk
 * @create date 2020-05-23
 */
import 'package:logger/logger.dart';
import 'package:path/path.dart';
import 'package:path_provider/path_provider.dart';
import 'package:schluss_beta_app/singleton_injector.dart';
import 'package:schluss_beta_app/util/database_encrypt_encoder_util.dart';
import 'package:sembast/sembast.dart';
import 'package:sembast/sembast_io.dart';

class AppDatabaseConfig extends DatabaseConfig {
  Future<bool> initiateDatabase(String password) async {
    return _initiateDatabase('schluss.db', password: password);
  }
}

/*
class OtherDatabaseConfig extends DatabaseConfig {
  Future<bool> initiateDatabase() async {
    return _initiateDatabase('tasks.db');
  }
}
*/

abstract class DatabaseConfig {
  Database? _db;
  String _dbFilename = 'app.db';

  /// Checks the status of database initializing.
  bool isInitialized() {
    return (_db != null);
  }

  /// Initializes the database client.
  Future<bool> _initiateDatabase(
    String dbFilename, {
    String password = '',
  }) async {
    _dbFilename = dbFilename;

    if (!isInitialized()) {
      var dbPath = await getDatabaseFilePath();

      SembastCodec? codec;

      if (password != '') {
        // Initialize the encryption codec with a user password.
        codec = getEncryptSembastCodec(password: password);
      }

      // Open the database.
      _db = await databaseFactoryIo.openDatabase(dbPath, codec: codec);

      return true;
    } else {
      return false;
    }
  }

  Future<void> closeDatabase() async {
    if (!isInitialized()) {
      return;
    }

    await _db!.close();

    return;
  }

  /// Deletes the database.
  Future<void> deleteDatabase() async {
    await closeDatabase();

    var dbPath = await getDatabaseFilePath();

    await databaseFactoryIo.deleteDatabase(dbPath);

    _db = null;

    return;
  }

  /// Gets the path of the database file.
  Future<String> getDatabaseFilePath() async {
    // Get the application documents directory.
    var dir = await getApplicationDocumentsDirectory();

    // Make sure it exists.
    await dir.create(recursive: true);

    return join(dir.path, _dbFilename);
  }

  /// Gets the instance of the database.
  Future<Database?> getInstance() async {
    if (isInitialized()) {
      return _db;
    } else {
      singletonInjector<Logger>().e('Database is not initialized');

      return null;
    }
  }
}
